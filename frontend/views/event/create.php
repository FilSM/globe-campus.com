<?php

use kartik\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\event\Event */

$this->title = Yii::t($model->tableName(), 'Create a new '.$model->modelTitle(1, false));
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-create">

    <?php if(!$isModal): ?>
    <?= Html::pageHeader(Html::encode($this->title));?>
    <?php endif; ?>

    <?= $this->render('_form', [
        'model' => $model,
        'eventProfileModel' => $eventProfileModel,
        'eventContactModel' => $eventContactModel,
        'eventResursModel' => $eventResursModel,
        'fromClientModel' => $fromClientModel,
        'eventResursList' => $eventResursList,
        'clientList' => $clientList,
        'managerList' => $managerList,
        'participantList' => $participantList,
        'isAdmin' => $isAdmin,
        'isModal' => $isModal,
    ]) ?>

</div>