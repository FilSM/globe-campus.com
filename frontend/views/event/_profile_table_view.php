<?php

use kartik\helpers\Html;

use common\models\event\EventProfile;
use common\components\FSMHelper;
?>

<?php 
    ob_start();
    ob_implicit_flush(false);
?>

<table class="table table-bordered table-striped margin-b-none">
    <thead>
        <tr>
            <th style="text-align: center; width: 90px;">#</th>
            <th><?= $model[0]->getAttributeLabel('manager_name'); ?></th>
            <th style="text-align: center; width: 100px;"><?= $model[0]->getAttributeLabel('accepted_at'); ?></th>
            <th style="text-align: center; width: 120px;"><?= Yii::t('common', 'Actions'); ?></th>
        </tr>
    </thead>
    <tbody class="form-managers-body">
        <?php foreach ($model as $index => $eventProfile): 
            if(empty($eventProfile->id)){
                continue;
            }
            ?>
            <tr class="form-managers-item">
                <td style="text-align: center;"><?= $index + 1; ?></td>
                <td>
                    <?= Html::a($eventProfile->profile->name, ['/user/'.$eventProfile->profile->user_id], ['target' => '_blank']).
                        (!empty($eventProfile->comment) ? '<br/>'.$eventProfile->comment : ''); 
                    ?>
                </td>
                <td style="text-align: center;">
                    <?= !empty($eventProfile->accepted_at) ? date('d-M-Y H:i:s', strtotime($eventProfile->accepted_at)) : Html::icon('remove', ['class' => 'text-danger']) ; ?>
                </td>
                <td style="text-align: center;">
                    <?= FSMHelper::vButton($eventProfile->id, [
                        'label' => null,
                        'title' => Yii::t('event', 'Refuse'),
                        'controller' => 'event',
                        'action' => 'refuse-event',
                        'class' => 'danger',
                        'icon' => 'thumbs-down',
                        'modal' => true,
                        'idName' => 'idProfile',
                        'options' => [
                            'disabled' => empty($eventProfile->accepted_at)
                        ]
                    ]).'&nbsp;'.
                    FSMHelper::aButton($eventProfile->id, [
                        'label' => null,
                        'title' => Yii::t('event', 'Accept'),
                        'controller' => 'event',
                        'action' => 'accept-event',
                        'class' => 'success',
                        'icon' => 'thumbs-up',
                        'idName' => 'idProfile',
                        'options' => [
                            'disabled' => !empty($eventProfile->accepted_at)
                        ]
                        //'modal' => true,
                    ]); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php
    $body = ob_get_contents();
    ob_get_clean(); 

    $panelContent = [
        'heading' => EventProfile::modelTitle(2),
        'preBody' => '<div class="panel-body">',
        'body' => $body,
        'postBody' => '</div>',
    ];
    echo Html::panel(
        $panelContent, 
        'success', 
        [
            'id' => "panel-manager-data",
        ]
    );
?>