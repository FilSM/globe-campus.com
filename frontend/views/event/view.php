<?php
namespace common\models\event;

use Yii;
use yii\widgets\Pjax;

use kartik\helpers\Html;

use common\components\FSMAccessHelper;
/* @var $this yii\web\View */
/* @var $model common\models\event\Event */
$isModal = !empty($isModal);
if(!$isModal){
    $this->title = $model->modelTitle() . ': '. (!empty($model->event_type) ? $model->eventTypeList[$model->event_type].' ' : '') . date('d-m-Y, H:i', strtotime($model->event_datetime));
    if(FSMAccessHelper::checkRoute('/event/index')){
        $this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
    }    
    $this->params['breadcrumbs'][] = $this->title;
}
?>

<?php if($isModal) : Pjax::begin(Yii::$app->params['PjaxModalOptions']); endif; ?>
<div class="event-view">
   
    <?php if(!$isModal): ?>
    <?= Html::pageHeader(Html::encode($this->title));?>
    <?php endif; ?>    

    <?= $this->render('_view', [
        'model' => $model,
        'eventProfileModel' => $eventProfileModel,
        'eventContactModel' => $eventContactModel,
        'eventResursModel' => $eventResursModel,
        'clientModel' => $clientModel,
        'isAdmin' => $isAdmin,
        'isModal' => $isModal,
    ]);
    ?>
            
</div>
<?php if($isModal) : Pjax::end(); endif; ?>