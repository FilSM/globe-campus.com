<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\event\Event */

$this->title = Yii::t($model->tableName(), 'Update a '.$model->modelTitle(1, false)) . ': ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Edit');
?>
<div class="event-update">

    <?= Html::pageHeader(Html::encode($this->title)); ?>

    <?= $this->render('_form', [
        'model' => $model,
        'eventProfileModel' => $eventProfileModel,
        'eventContactModel' => $eventContactModel,
        'eventResursModel' => $eventResursModel,
        'fromClientModel' => $fromClientModel,
        'eventResursList' => $eventResursList,
        'clientList' => $clientList,
        'managerList' => $managerList,
        'participantList' => $participantList,
        'isAdmin' => $isAdmin,
        'isModal' => $isModal,        
    ]) ?>

</div>