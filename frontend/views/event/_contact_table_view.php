<?php

use kartik\helpers\Html;

use common\models\event\EventContact;
use common\components\FSMHelper;
?>

<?php 
    ob_start();
    ob_implicit_flush(false);
?>

<table class="table table-bordered table-striped margin-b-none">
    <thead>
        <tr>
            <th style="text-align: center; width: 90px;">#</th>
            <th><?= $model[0]->getAttributeLabel('name'); ?></th>
            <th style="text-align: center; width: 100px;"><?= $model[0]->getAttributeLabel('accepted_at'); ?></th>
            <th style="text-align: center; width: 120px;"><?= Yii::t('common', 'Actions'); ?></th>
        </tr>
    </thead>
    <tbody class="form-contacts-body">
        <?php foreach ($model as $index => $eventContact): 
            if(empty($eventContact->id)){
                continue;
            }
            ?>
            <tr class="form-contacts-item">
                <td style="text-align: center;"><?= $index + 1; ?></td>
                <td>
                    <?= $eventContact->clientContactNamePosition.(!empty($eventContact->comment) ? '<br/>'.$eventContact->comment : ''); ?>
                </td>
                <td style="text-align: center;">
                    <?= !empty($eventContact->accepted_at) ? date('d-M-Y H:i:s', strtotime($eventContact->accepted_at)) : Html::icon('remove', ['class' => 'text-danger']) ; ?>
                </td>
                <td style="text-align: center;">
                    <?= FSMHelper::vButton($eventContact->id, [
                        'label' => null,
                        'title' => Yii::t('event', 'Refuse'),
                        'controller' => 'event',
                        'action' => 'refuse-event-contact',
                        'class' => 'danger',
                        'icon' => 'thumbs-down',
                        'modal' => true,
                        'idName' => 'idContact',
                        'options' => [
                            'disabled' => empty($eventContact->accepted_at)
                        ]
                    ]).'&nbsp;'.
                    FSMHelper::aButton($eventContact->id, [
                        'label' => null,
                        'title' => Yii::t('event', 'Accept'),
                        'controller' => 'event',
                        'action' => 'accept-event-contact',
                        'class' => 'success',
                        'icon' => 'thumbs-up',
                        'idName' => 'idContact',
                        'options' => [
                            'disabled' => !empty($eventContact->accepted_at)
                        ]
                        //'modal' => true,
                    ]); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php
    $body = ob_get_contents();
    ob_get_clean(); 

    $panelContent = [
        'heading' => EventContact::modelTitle(2),
        'preBody' => '<div class="panel-body">',
        'body' => $body,
        'postBody' => '</div>',
    ];
    echo Html::panel(
        $panelContent, 
        'success', 
        [
            'id' => "panel-contact-data",
        ]
    );
?>