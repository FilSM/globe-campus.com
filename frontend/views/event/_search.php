<?php

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\event\search\EventSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'parent_event_id') ?>

    <?= $form->field($model, 'manager_id') ?>

    <?= $form->field($model, 'event_type') ?>

    <?php //echo $form->field($model, 'amount') ?>

    <?php //echo $form->field($model, 'status') ?>

    <?php //echo $form->field($model, 'event_date') ?>

    <?php //echo $form->field($model, 'event_time') ?>

    <?php //echo $form->field($model, 'event_datetime') ?>

    <?php //echo $form->field($model, 'next_event_type') ?>

    <?php //echo $form->field($model, 'next_date') ?>

    <?php //echo $form->field($model, 'next_time') ?>

    <?php //echo $form->field($model, 'next_datetime') ?>

    <?php //echo $form->field($model, 'comment') ?>

    <?php //echo $form->field($model, 'result_comment') ?>

    <?php //echo $form->field($model, 'deleted')->widget(SwitchInput::class, [
//             'pluginOptions' => [
//                 'onText' => Yii::t('common', 'Yes'),
//                 'offText' => Yii::t('common', 'No'),
//             ],
//         ]) ?>

    <?php //echo $form->field($model, 'create_time') ?>

    <?php //echo $form->field($model, 'create_user_id') ?>

    <?php //echo $form->field($model, 'update_time') ?>

    <?php //echo $form->field($model, 'update_user_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('common', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('common', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>