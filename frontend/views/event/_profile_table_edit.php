<?php
use yii\helpers\Url;

use kartik\helpers\Html;
use kartik\widgets\Select2;

use common\components\FSMHtml;
use common\widgets\dynamicform\DynamicFormWidget;
use common\models\event\EventProfile;

$isModal = !empty($isModal);
?>

<?php 
    ob_start();
    ob_implicit_flush(false);
?>

<?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_wrapper_profile',
    'widgetBody' => '.form-profiles-body',
    'widgetItem' => '.form-profiles-item',
    'min' => 1,
    'insertButton' => '.add-item',
    'deleteButton' => '.delete-item',
    'model' => $model[0],
    'formId' => $form->id,
    'formFields' => [
        'profile_name',
    ],
]); ?>

<table class="table table-bordered table-striped margin-b-none">
    <thead>
        <tr>
            <th><?= $model[0]->getAttributeLabel('manager_name'); ?></th>
            <th style="width: 90px; text-align: center"><?= Yii::t('kvgrid', 'Actions'); ?></th>
        </tr>
    </thead>
    <tbody class="form-profiles-body">
        <?php foreach ($model as $index => $eventProfile): ?>
            <tr class="form-profiles-item">
                <td>
                    <?= Html::activeHiddenInput($eventProfile, "[{$index}]id"); ?>
                    <?= $form->field($eventProfile, "[{$index}]profile_id", [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                    ])->widget(Select2::class, [
                        'data' => $managerList, 
                        'options' => [
                            'placeholder' => '...',
                            'class' => 'profile-item-select',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
                        ],
                    ])->label(false); ?>
                </td>
                <td class="text-center vcenter">
                    <button type="button" class="delete-item btn btn-danger btn-xs"><?= Html::icon('minus');?></button>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="5"><button type="button" class="add-item btn btn-success btn-sm"><?= Html::icon('plus');?> <?= Yii::t('event', 'Add participant'); ?></button></td>
        </tr>
    </tfoot>
</table>

<?php DynamicFormWidget::end(); ?>

<?php
    $body = ob_get_contents();
    ob_get_clean(); 

    $panelContent = [
        'heading' => EventProfile::modelTitle(2),
        'preBody' => '<div class="panel-body">',
        'body' => $body,
        'postBody' => '</div>',
    ];
    echo FSMHtml::panel(
        $panelContent, 
        'default', 
        [
            'id' => "panel-profile-data",
        ]
    );
?>