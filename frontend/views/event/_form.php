<?php
namespace common\models;

use Yii;
use yii\widgets\Pjax;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;
use kartik\widgets\TimePicker;
use kartik\widgets\DateTimePicker;
use kartik\widgets\TouchSpin;
use kartik\checkbox\CheckboxX;
use kartik\widgets\DatePicker;

use common\widgets\EnumInput;

/* @var $this yii\web\View */
/* @var $model common\models\event\Event */
/* @var $form yii\widgets\ActiveForm */
$isModal = !empty($isModal);
?>

<div class="event-form">
    <?php if($isModal) : Pjax::begin(Yii::$app->params['PjaxModalOptions']); endif; ?>

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'id' => $model->tableName().'-form',
        'formConfig' => [
            'labelSpan' => 3,
        ],
        'fieldConfig' => [
            'showHints' => true,
        ],
        'options' => [
            'data-pjax' => $isModal,
        ],
    ]); ?>

    <?php 
        if($model->id){
            echo '<p>';
            if ($model->parent_event_id) {
                echo Html::a($model->getAttributeLabel('parent_event_id'), ['event/update', 'id' => $model->parent_event_id, 'client_id' => (!empty($fromClientModel) ? $fromClientModel->id : null)], ['class' => 'btn btn-info']);
            } else {
                echo Html::a($model->getAttributeLabel('parent_event_id'), ['#'], ['class' => 'btn btn-default', 'disabled' => true]);
            }
            echo '<span stile="padding-left: 10px;"> </span>';
            if ($nextEvent = $model->nextEvent) {
                echo Html::a(Yii::t('event', 'Next event'), ['event/update', 'id' => $nextEvent->id, 'client_id' => (!empty($fromClientModel) ? $fromClientModel->id : null)], ['class' => 'btn btn-info']);
            } else {
                echo Html::a(Yii::t('event', 'Next event'), ['#'], ['class' => 'btn btn-default', 'disabled' => true]);
            }
            echo '</p>';
        }
    ?>
    
    <?= Html::activeHiddenInput($model, 'abonent_id'); ?>
    
    <?= $form->field($model, 'event_type')->widget(EnumInput::class, [
            'type' => EnumInput::TYPE_RADIOBUTTON,
            'options' => [
                'translate' => $model->eventTypeList,
            ],
        ]); 
    ?>
    
    <?= $form->field($model, 'event_type_other')->textInput([
        'maxlength' => 50,
    ]); ?>
    
    <?= $form->field($model, 'client_id')->widget(Select2::class, [
            'data' => $clientList,
            'options' => [
                'id' => 'client-id', 
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => !$model->isAttributeRequired('client_id'),
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],            
        ]); 
    ?>
    
    <?= $form->field($model, 'name')->textInput([
        'maxlength' => 255,
    ]); ?>
    
    <?= $form->field($model, 'address')->textInput([
        'maxlength' => 255,
    ]); ?>
                
    <?= $form->field($model, 'event_datetime', [
        //'template' => '{label}<div class="well well-sm col-md-9" style="margin-left: 15px; background-color: #fff; width:245px">{input}</div>{error}{hint}',
        /*
        'options' => [
            'class' => 'col-md-3'
        ],
         * 
         */
    ])->widget(DateTimePicker::class, [
        //'type' => DateTimePicker::TYPE_INLINE,
        'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
        'options' => [
            'placeholder' => Yii::t('event', 'Enter date & time of the event start'),
        ],
        'pluginOptions' => \yii\helpers\ArrayHelper::merge(Yii::$app->params['DatePickerPluginOptions'], ['format' => 'dd-mm-yyyy, hh:ii']),
    ]); ?>
                
    <?= $form->field($model, 'event_datetime_finish', [
    ])->widget(DateTimePicker::class, [
        //'type' => DateTimePicker::TYPE_INLINE,
        'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
        'options' => [
            'placeholder' => Yii::t('event', 'Enter date & time of the event finish'),
        ],
        'pluginOptions' => \yii\helpers\ArrayHelper::merge(Yii::$app->params['DatePickerPluginOptions'], ['format' => 'dd-mm-yyyy, hh:ii']),
    ]); ?>

    <?= $form->field($model, 'periodicity')->widget(EnumInput::class, [
        'type' => EnumInput::TYPE_RADIOBUTTON,
        'options' => [
            'translate' => $model->periodicityList,
        ],
    ]);?>
    
    <div id="periodicity-data">
    <?php 
        $periodicalDayList = [];
        foreach (range(1, 31) as $key => $value) {
            $periodicalDayList[$key+1] = $value;
        }
        echo $form->field($model, 'periodical_day')->widget(EnumInput::class, [
            'type' => EnumInput::TYPE_SELECT2,
            'data' => $periodicalDayList,
            'clientOptions' => [
                'pluginOptions' => [
                    'minimumResultsForSearch' => 'Infinity',
                ],
            ],
        ])->hint(Yii::t('event', 'When using the "Weekly" periodicity, enter the serial number of the day of the week here.'));
    ?>

    <?php /*= $form->field($model, 'periodical_last_date')->widget(DatePicker::class, [
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'pickerIcon' => '<i class="glyphicon glyphicon-calendar kv-dp-icon"></i>',
            'removeButton' => false,
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
        ]); 
     * 
     */
    ?>
    
    <?= $form->field($model, 'periodical_finish_date')->widget(DatePicker::class, [
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'pickerIcon' => '<i class="glyphicon glyphicon-calendar kv-dp-icon"></i>',
            'removeButton' => false,
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
        ]); 
    ?>
    </div>

    <?= $form->field($model, 'manager_id')->widget(Select2::class, [
        'data' => $managerList,
        'options' => [
            'id' => 'manager_id',
            'placeholder' => '...',
        ],
        'pluginOptions' => [
            'allowClear' => !$model->isAttributeRequired('manager_id')
        ],           
    ]); ?>
    
    <?= $form->field($model, 'send_email')->widget(CheckboxX::class, [
            'pluginOptions' => ['threeState' => false]
        ]);
    ?>
            
    <?php
        echo Html::beginTag('div', [
            'id' => 'person-data-container',
            'class' => 'form-group',
        ]);
        echo Html::beginTag('div', [
            'class' => 'col-md-3',
        ]);
        echo Html::endTag('div');
        echo Html::beginTag('div', [
            'class' => 'col-md-9',
        ]);
    ?>

    <?= $this->render('_profile_table_edit', [
        'form' => $form,
        'model' => $eventProfileModel,
        'managerList' => $participantList,
        'isModal' => $isModal,
    ]) ?>

    <?php
        echo Html::endTag('div');
        echo Html::endTag('div');           
    ?>  
    
    <?php
        echo Html::beginTag('div', [
            'id' => 'contact-data-container',
            'class' => 'form-group',
        ]);
        echo Html::beginTag('div', [
            'class' => 'col-md-3',
        ]);
        echo Html::endTag('div');
        echo Html::beginTag('div', [
            'class' => 'col-md-9',
        ]);
    ?>
    
    <?= $this->render('_contact_table_edit', [
        'form' => $form,
        'model' => $eventContactModel,
        'isModal' => $isModal,
    ]) ?>

    <?php
        echo Html::endTag('div');
        echo Html::endTag('div');           
    ?>  
    
    <?php
        echo Html::beginTag('div', [
            'id' => 'resurs-data-container',
            'class' => 'form-group',
        ]);
        echo Html::beginTag('div', [
            'class' => 'col-md-3',
        ]);
        echo Html::endTag('div');
        echo Html::beginTag('div', [
            'class' => 'col-md-9',
        ]);
    ?>
    
    <?= $this->render('_resurs_table_edit', [
        'form' => $form,
        'model' => $eventResursModel,
        'eventResursList' => $eventResursList,
        'isModal' => $isModal,
    ]) ?>

    <?php
        echo Html::endTag('div');
        echo Html::endTag('div');           
    ?> 
    
    <?php 
        //if(empty(Yii::$app->params['ENABLE_GOOGLE_CALENDAR'])) : 
            echo Html::beginTag('div', [
                'class' => 'form-group',
            ]);
            echo Html::label(Yii::t('event', 'Remind').'?', 'cbx-remind',
                [
                    'class' => 'control-label col-md-3',
                ]
            );
            echo Html::beginTag('div', [
                'id' => 'cbx-remind-container',
                'class' => 'col-md-9',
            ]);

            echo SwitchInput::widget([
                'name' => 'remind',
                'value' => (!empty($model->remind_day) || !empty($model->remind_hours)),
                'options' => ['id' => 'cbx-remind'],
                'pluginOptions' => [
                    'onText' => Yii::t('common', 'Yes'),
                    'offText' => Yii::t('common', 'No'),
                    'handleWidth' => (!$isModal ? 'auto' : '45')
                ],
            ]);

            echo '<div class="help-block"></div>';
            echo Html::endTag('div');
            echo Html::endTag('div');  
        //endif;
    ?> 

    <?= Html::beginTag('div', [
        'id' => 'remind-data-container',
        'style' => (empty($model->id) ? 'display: none;' : null),
    ]);?>
        
    <?= $form->field($model, 'remind_day')->widget(TouchSpin::class, [
        'options' => ['placeholder' => 'Adjust ...'],
        'pluginOptions' => [
            'min' => 0,
            'max' => 5,
            'step' => 1,
        ]
    ]); ?>
    
    <?= $form->field($model, 'remind_hours')->widget(TimePicker::class, [
        'pluginOptions' => [
            'showMeridian' => false,
            'minuteStep' => 15,
            'defaultTime' => false,
        ]
    ]); ?>
    
    <?= Html::endTag('div'); ?>
    
    <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>

    <div id="event-status">
        <?= $form->field($model, 'status')->widget(EnumInput::class, [
                'type' => EnumInput::TYPE_RADIOBUTTON,
                'options' => [
                    'translate' => $model->eventStatusList,
                ],
            ]); 
        ?>
    </div>
    
    <div id="next-event-group" class="not-planned" style="display: none;">
        <?= $form->field($model, 'next_event_type', [
            'options' => [
                'class' => 'form-group next-event-control',
                ]
            ])->widget(EnumInput::class, [
                'type' => EnumInput::TYPE_RADIOBUTTON,
                'options' => [
                    'translate' => $model->eventTypeList,
                ],
            ]); 
        ?>

        <?= $form->field($model, 'next_datetime', [
            'options' => [
                'class' => 'form-group next-event-control',
                ]
            ])->widget(DateTimePicker::class, [
                'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
                'options' => [
                    'placeholder' => Yii::t('event', 'Enter date & time of the next event'),
                ],
                'pluginOptions' => \yii\helpers\ArrayHelper::merge(Yii::$app->params['DatePickerPluginOptions'], ['format' => 'dd-M-yyyy hh:ii']),
            ]);
        ?>    
    </div>
    
    <div id="event-result-comment-group" class="not-planned" style="display: none;">
        <?= $form->field($model, 'result_comment', ['options' => ['class' => 'form-group next-event-control']])->textarea(['rows' => 3]) ?>
    </div>

    
    <?php if(!empty($model->id) && !empty($isAdmin)){
        echo $form->field($model, 'deleted')->widget(SwitchInput::class, [
            'pluginOptions' => [
                'onText' => Yii::t('common', 'Yes'),
                'offText' => Yii::t('common', 'No'),
            ],
        ]);
    } ?>
    
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10" style="text-align: right;">
            <?= $model->SubmitButton; ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <?php if($isModal) : Pjax::end(); endif; ?>
</div>