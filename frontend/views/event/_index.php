<?php
namespace common\models;

use Yii;
//use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\helpers\Html;
use kartik\grid\GridView;

use common\components\FSMAccessHelper;
use common\models\user\FSMUser;
use common\models\event\Event;

/* @var $this yii\web\View */
/* @var $searchModel common\models\event\search\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<?php if(FSMAccessHelper::can('createEvent')): ?>
<p>
    <?= Html::a(Html::icon('plus').'&nbsp;'.$searchModel->modelTitle(), ['create', 'client_id' => (!empty($clientModel) ? $clientModel->id : null)], ['class' => 'btn btn-success']); ?>
</p>
<?php endif; ?>

<?php
$columns = [
    //['class' => '\kartik\grid\SerialColumn'],

    [
        'attribute' => 'id',
        'width' => '75px',
        'hAlign' => 'center',
        'vAlign' => 'middle',
    ],        
    [
        'attribute' => 'event_type',
        'headerOptions' => ['class'=>'td-mw-150'],
        'value' => function ($model) {
            static $eventTypeList = null;
            if(!$eventTypeList){
                $eventTypeList = $model->getEventTypeList();
            }
            if(!empty($model->event_type)){
                if($model->event_type == Event::EVENT_TYPE_OTHER){
                    $result = !empty($model->event_type_other) ? $model->event_type_other : $eventTypeList[$model->event_type];
                }else{
                    $result = $eventTypeList[$model->event_type];
                }
            }else{
                $result = null;
            }
            return $result;
        },
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $searchModel->getEventTypeList(),
        'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
        'filterInputOptions' => ['placeholder' => Yii::t('common', 'Select').' '.$searchModel->getAttributelabel('event_type')],
    ],
    [
        'attribute' => 'status',
        'headerOptions' => ['class'=>'td-mw-150'],
        'value' => function ($model) {
            static $eventStatusList = null;
            if(!$eventStatusList){
                $eventStatusList = $model->getEventStatusList();
            }
            return !empty($model->status) ? $eventStatusList[$model->status] : null;
        },
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $searchModel->getEventStatusList(),
        'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
        'filterInputOptions' => ['placeholder' => Yii::t('common', 'Select').' '.$searchModel->getAttributelabel('status')],
    ],
    [
        'attribute' => 'manager_id',
        'headerOptions' => ['class'=>'td-mw-200'],
        'value' => function ($model) {
            return !empty($model->manager_id) ? Html::a($model->manager_name, ['/user/profile/show', 'id' => $model->manager->user->id], ['target' => '_blank', 'data-pjax' => 0]) : null;
        },
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $managerList,
        'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
        'filterInputOptions' => ['placeholder' => Yii::t('common', 'Select').' '.Yii::t('user', 'Manager')],
    ],
    [
        'attribute'=>'event_date',
        'headerOptions' => ['class'=>'td-mw-75'],
        'value' => function ($model) {
            return !empty($model->event_date) ? date('d-M-Y', strtotime($model->event_date)) : null;
        },
        'filterType' => GridView::FILTER_DATE,
        'filterWidgetOptions' => [
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
            'pickerButton' => false,
            'layout' => '{input}{remove}',
            'buttonOptions' => [],
        ],
    ],     
    [
        'attribute'=>'event_time',
        'headerOptions' => ['class'=>'td-mw-75'],     
        'value' => function ($model) {
            return !empty($model->event_time) ? date('H:i', strtotime($model->event_time)) : null;
        },                
    ],
    // 'event_datetime',
    [
        'attribute'=>'event_date_finish',
        'headerOptions' => ['class'=>'td-mw-75'],
        'value' => function ($model) {
            return !empty($model->event_date_finish) ? date('d-M-Y', strtotime($model->event_date_finish)) : null;
        },
        'filterType' => GridView::FILTER_DATE,
        'filterWidgetOptions' => [
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
            'pickerButton' => false,
            'layout' => '{input}{remove}',
            'buttonOptions' => [],
        ],
    ],    
    [
        'attribute'=>'event_time_finish',
        'headerOptions' => ['class'=>'td-mw-75'],     
        'value' => function ($model) {
            return !empty($model->event_time_finish) ? date('H:i', strtotime($model->event_time_finish)) : null;
        },                
    ],
    [
        'attribute' => "deleted",   
        'vAlign' => 'middle',
        'class' => '\kartik\grid\BooleanColumn',
        'trueLabel' => 'Yes', 
        'falseLabel' => 'No',
        'width' => '100px',
        'visible' => $isAdmin,
    ],
    [
        'attribute' => 'create_time',
        'width' => '150px',
        'headerOptions' => ['class'=>'td-mw-100'],
        'value' => function ($model) {
            return isset($model->create_time) ? date('d-M-Y H:i:s', strtotime($model->create_time)) : null;
        },                         
    ],
    [
        'attribute' => 'create_user_id',
        'headerOptions' => ['class'=>'td-mw-100'],
        'value' => function ($model) {
            return isset($model->create_user_id) ? Html::a($model->user_name, ['/user/profile/show', 'id' => $model->create_user_id], ['target' => '_blank', 'data-pjax' => 0]) : null;
        },                         
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $userList,
        'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
        'filterInputOptions' => ['placeholder' => '...'],
        'format' => 'raw',
    ],
    // 'amount',
    // 'next_datetime',
    // 'comment:ntext',
    // 'result_comment:ntext',
    // 'parent_event_id',
    // 'update_time',
    // 'update_user_id',                    
    [
        'class' => '\common\components\FSMActionColumn',
        'headerOptions' => ['class' => 'td-mw-150'],
        'dropdown' => true,
        'checkPermission' => true,
        'linkedObj' => !empty($clientModel->id) ? [
            ['fieldName' => 'client_id', 'id' => $clientModel->id],
        ] : null,  
    ],                    
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'responsive' => false,
    //'striped' => false,
    'hover' => true,      
    'floatHeader' => true,
    'pjax' => true,
    'columns' => $columns,
]);
?>