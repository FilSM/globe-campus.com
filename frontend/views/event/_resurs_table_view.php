<?php

use kartik\helpers\Html;

use common\models\event\EventResurs;
use common\components\FSMHelper;
?>

<?php 
    ob_start();
    ob_implicit_flush(false);
?>

<table class="table table-bordered table-striped margin-b-none">
    <thead>
        <tr>
            <th style="text-align: center; width: 90px;">#</th>
            <th><?= $model[0]->getAttributeLabel('resurs_name'); ?></th>
        </tr>
    </thead>
    <tbody class="form-resurs-body">
        <?php foreach ($model as $index => $eventResurs): 
            if(empty($eventResurs->id)){
                continue;
            }
            ?>
            <tr class="form-resurs-item">
                <td style="text-align: center;"><?= $index + 1; ?></td>
                <td>
                    <?= Html::badge($eventResurs->eventResurs->name, ['style' => "background-color: {$eventResurs->eventResurs->color_code};"]); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php
    $body = ob_get_contents();
    ob_get_clean(); 

    $panelContent = [
        'heading' => EventResurs::modelTitle(2),
        'preBody' => '<div class="panel-body">',
        'body' => $body,
        'postBody' => '</div>',
    ];
    echo Html::panel(
        $panelContent, 
        'success', 
        [
            'id' => "panel-contact-data",
        ]
    );
?>