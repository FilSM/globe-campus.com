<?php
namespace common\models;

use Yii;
//use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\JsExpression;
use yii\helpers\Url;

use kartik\helpers\Html;
use kartik\grid\GridView;

use common\components\FSMAccessHelper;
use common\components\FSMHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\event\search\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('event', 'All events');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">
    <?php /*
    <?= Html::pageHeader(Html::encode($this->title));?>
     * 
     */ ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if(FSMAccessHelper::can('createEvent')): ?>
    <p>
        <?= FSMHelper::vButton(null, [
            'label' => $eventModel::modelTitle(),
            'title' => Yii::t('event', 'Add new event'),
            'action' => 'create',
            'class' => 'success',
            'icon' => 'plus',
            'modal' => true,
            'options' => [
                'data-pjax' => 0,
            ]
        ]); ?>
    </p>
    <?php endif; ?>
    
    <div id="calendar" class="full-calendar">
        <?= \yii2fullcalendar\yii2fullcalendar::widget([
            'options' => [
                'id' => 'calendar',
                'lang' => \Yii::$app->language,
            //... more options to be defined here!
            ],
            'clientOptions' => [
                //'locale' => 'lv',
                'firstDay' => 1,
                'weekNumbers' => true,
                'weekNumbersWithinDays' => true,
                'selectable'  => true,
                'defaultView' => 'basicWeek',
                'header' => ['right' => 'month, basicWeek, basicDay'],
                'timeFormat' => 'H:mm',
                'themeSystem' => 'bootstrap3',
                //'eventLimit' => 1,
                'allDaySlot' => false,
                'slotLabelFormat' => 'H:mm',
                'displayEventEnd' => true,
                'minTime' => '09:00',
                'maxTime' => '18:00',
                /*
                'businessHours' => [
                    'start' => '09:00',
                    'end' => '17:00',
                ],
                 * 
                 */
                //'hiddenDays' => [0, 6],
                'weekends' => false,
                'height' => 'auto',
                /*
                'eventResize' => new JsExpression("
                    function(event, delta, revertFunc, jsEvent, ui, view) {
                        console.log(event);
                    }
                "),
                 * 
                 */
                'dayRender' => new JsExpression("
                    function(date, cell) {
                        var url = appUrl + '/event/create?date=' + date.format();
                        var \$cell = \$(cell);
                        \$cell.attr('value', url);
                        \$cell.attr('header-title', lajax.t('Add new event'));
                        \$cell.addClass('show-modal-button');
                        
                        \$cell.on('dblclick', function() {
                            /*
                            var \$day = \$(this);
                            if(\$day.hasClass('show-modal-button')){
                                \$day.trigger('click.modal');
                            }
                            */
                            alert('double click!');
                        });
                        /*
                        \$cell.bind('dblclick',function(){
                            alert(date);
                        })
                        */
                    }
                "),       
                'dayClick' => new JsExpression("
                    function(date, jsEvent, view) {
                        \$('#calendar').fullCalendar('gotoDate', date);
                        //alert('Clicked on: ' + date.format());
                        //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                        //alert('Current view: ' + view.name);

                        // change the day's background color just for fun
                        //$(this).css('background-color', 'red');
                    }
                "),                
                'eventRender' => new JsExpression("
                    function(event, element, view) {
                        var \$event = \$(element);
                        if(event.id > 0){
                            \$event.attr('target', '_blank');
                        }
                        var elementText = $(element).html();
                        if(!empty(event.nonstandard.description)){
                            elementText = elementText + event.nonstandard.description;
                        }
                        if(!empty(event.nonstandard.partOur)){
                            if(!empty(event.nonstandard.description)){
                                elementText = elementText + '<br/>';
                            }
                            elementText = elementText + event.nonstandard.partOur;
                        }
                        if(!empty(event.nonstandard.partClient)){
                            if(!empty(event.nonstandard.client)){
                                if(!empty(elementText)){
                                    elementText = elementText + '<br/>';
                                }
                                elementText = elementText + event.nonstandard.client;
                            }
                            if(!empty(elementText)){
                                elementText = elementText + '<br/>';
                            }
                            elementText = elementText + event.nonstandard.partClient;
                        }
                        //$(element).html(elementText);
                        $(element).html(event.nonstandard.html);

                        /*
                        element.qtip({
                            content: event.nonstandard.partArr, // This is a non-standard, custom attribute!
                            position: {
                                my: 'top left',
                                at: 'bottom left'
                            }
                        });
                        */
                    }
                "),
                /*
                'eventClick' => new JsExpression("
                    function(calEvent, jsEvent, view) {
                        //alert('Event: ' + calEvent.title);
                        //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                        //alert('View: ' + view.name);

                        // change the border color just for fun
                        //$(this).css('border-color', 'red');
                    }
                "),
                 * 
                 */
            ],        
            'events' => Url::to(['/event/ajax-events']),
        ]);
        ?>  
    </div>
    
    <?php /*
    <div id="g-calendar" class="full-calendar">
        <?= \yii2fullcalendar\yii2fullcalendar::widget([
            'googleCalendar' => true,
            'options' => [
                'id' => 'g-calendar',
                'lang' => \Yii::$app->language,
            //... more options to be defined here!
            ],
            'clientOptions' => [
                'googleCalendarApiKey' => Yii::$app->params['googleCalendarApiKey'],
                'events' => [
                    'commonGoogleCalendarId' => Yii::$app->params['commonGoogleCalendarId'],
                    'className' => 'gcal-event',
                ],
                //'locale' => 'lv',
                'firstDay' => 1,
                'weekNumbers' => true,
                'weekNumbersWithinDays' => true,
                'selectable'  => true,
                'defaultView' => 'basicWeek',
                'header' => ['right' => 'month, basicWeek, basicDay'],
                'timeFormat' => 'H:mm',
                'themeSystem' => 'bootstrap3',
                'allDaySlot' => false,
                'slotLabelFormat' => 'H:mm',
                'displayEventEnd' => true,
                'minTime' => '09:00',
                'maxTime' => '18:00',
                //'weekends' => false,
                'height' => 'auto',
                'dayClick' => new JsExpression("
                    function(date, jsEvent, view) {
                        \$('#calendar').fullCalendar('gotoDate', date);
                        console.log('Clicked on: ' + date.format());
                        //alert('Clicked on: ' + date.format());
                        //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                        //alert('Current view: ' + view.name);

                        // change the day's background color just for fun
                        //$(this).css('background-color', 'red');
                    }
                "),                
            ],            
        ]);
        ?>  
    </div>
     * 
     */?>

</div>