<?php

use kartik\helpers\Html;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\client\Agreement */

$this->title = $model->modelTitle() . ': '. $model->eventTypeList[$model->event_type] . ' ' . date('d-m-Y, H:i', strtotime($model->event_datetime));
$this->params['breadcrumbs'][] = ['label' => $clientModel->modelTitle(2), 'url' => ['client/index']];
$this->params['breadcrumbs'][] = ['label' => $clientModel->name, 'url' => ['client/view', 'id' => $clientModel->id]];
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index', 'client_id' => $clientModel->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row client-event-view">
    
    <div class="col-md-2">
        <?= $this->render('@frontend/views/client/client/_menu', [
            'client' => $clientModel,
            'activeItem' => $model->tableName(),
        ])
        ?>
    </div>
    
    <div class="col-md-10">

        <?php /*= Html::pageHeader(Html::encode($this->title)); */?>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?php 
            $body = $this->render('../_view', [
                'model' => $model,
                'eventProfileModel' => $eventProfileModel,
                'eventContactModel' => $eventContactModel,
                'eventResursModel' => $eventResursModel,
                'clientModel' => $clientModel,
                'isAdmin' => $isAdmin,
                'isModal' => $isModal,
            ]); 

            $panelContent = [
                'heading' => Html::encode($this->title),
                'preBody' => '<div class="panel-body">',
                'body' => $body,
                'postBody' => '</div>',
            ];
            echo Html::panel(
                $panelContent, 
                'primary', 
                [
                    'id' => "panel-client-event-data",
                ]
            );        
        ?>
    </div>

</div>