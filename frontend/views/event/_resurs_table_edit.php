<?php
use yii\helpers\Url;
use yii\widgets\MaskedInput;

use kartik\helpers\Html;
use kartik\widgets\Select2;

use common\components\FSMHtml;
use common\widgets\dynamicform\DynamicFormWidget;
use common\models\event\EventResurs;

$isModal = !empty($isModal);
?>

<?php 
    ob_start();
    ob_implicit_flush(false);
?>

<?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_wrapper_resurs',
    'widgetBody' => '.form-resurss-body',
    'widgetItem' => '.form-resurss-item',
    'min' => 1,
    'insertButton' => '.add-item',
    'deleteButton' => '.delete-item',
    'model' => $model[0],
    'formId' => $form->id,
    'formFields' => [
        'name',
        'short_name',
        'email',
    ],
]); ?>

<table class="table table-bordered table-striped margin-b-none">
    <thead>
        <tr>
            <th><?= $model[0]->getAttributeLabel('name'); ?></th>
            <th style="width: 90px; text-align: center"><?= Yii::t('kvgrid', 'Actions'); ?></th>
        </tr>
    </thead>
    <tbody class="form-resurss-body">
        <?php foreach ($model as $index => $eventResurs): ?>
            <tr class="form-resurss-item">
                <td>
                    <?= Html::activeHiddenInput($eventResurs, "[{$index}]id"); ?>
                    <?= $form->field($eventResurs, "[{$index}]event_resurs_id", [    
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                        /*
                        'options' => [
                            'class' => 'form-group',
                            //'style' => 'text-align: center;',
                        ],
                         * 
                         */
                    ])->widget(Select2::class, [
                        'data' => $eventResursList, 
                        //'disabled' => true,
                        'options' => [
                            'placeholder' => '...',
                            'class' => 'event-resurs-item-select',
                            'value' => (!empty($eventResurs->event_resurs_id) ? $eventResurs->event_resurs_id : null),
                        ],
                        'pluginOptions' => [
                            'allowClear' => !$eventResurs->isAttributeRequired('event_resurs_id'),
                            'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
                        ],
                        'addon' => [
                            'prepend' => $eventResurs->getModalButtonContent([
                                'formId' => $form->id,
                                'controller' => 'event-resurs',
                            ]),
                        ],                          
                    ])->label(false); ?>                  
                </td>
                <td class="text-center vcenter">
                    <button type="button" class="delete-item btn btn-danger btn-xs"><?= Html::icon('minus');?></button>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="5"><button type="button" class="add-item btn btn-success btn-sm"><?= Html::icon('plus');?> <?= Yii::t('event', 'Add resurs'); ?></button></td>
        </tr>
    </tfoot>
</table>

<?php DynamicFormWidget::end(); ?>

<?php
    $body = ob_get_contents();
    ob_get_clean(); 

    $panelContent = [
        'heading' => EventResurs::modelTitle(2),
        'preBody' => '<div class="panel-body">',
        'body' => $body,
        'postBody' => '</div>',
    ];
    echo FSMHtml::panel(
        $panelContent, 
        'default', 
        [
            'id' => "panel-resurs-data",
        ]
    );
?>