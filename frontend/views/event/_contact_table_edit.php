<?php
use yii\helpers\Url;
use yii\widgets\MaskedInput;

use kartik\helpers\Html;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;

use common\components\FSMHtml;
use common\widgets\dynamicform\DynamicFormWidget;
use common\models\event\EventContact;

$isModal = !empty($isModal);
?>

<?php 
    ob_start();
    ob_implicit_flush(false);
?>

<?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_wrapper_contact',
    'widgetBody' => '.form-contacts-body',
    'widgetItem' => '.form-contacts-item',
    'min' => 1,
    'insertButton' => '.add-item',
    'deleteButton' => '.delete-item',
    'model' => $model[0],
    'formId' => $form->id,
    'formFields' => [
        'name',
        'short_name',
        'email',
    ],
]); ?>

<table class="table table-bordered table-striped margin-b-none">
    <thead>
        <tr>
            <th><?= $model[0]->getAttributeLabel('name'); ?></th>
            <th><?= $model[0]->getAttributeLabel('short_name'); ?></th>
            <th style="width: 90px; text-align: center"><?= Yii::t('kvgrid', 'Actions'); ?></th>
        </tr>
    </thead>
    <tbody class="form-contacts-body">
        <?php foreach ($model as $index => $eventContact): ?>
            <tr class="form-contacts-item">
                <td class="col-md-10">
                    <?= Html::activeHiddenInput($eventContact, "[{$index}]id"); ?>
                    
                    <?= $form->field($eventContact, "[{$index}]contact_id", [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                    ])->widget(DepDrop::class, [
                        'type' => DepDrop::TYPE_SELECT2,
                        'data' => empty($eventContact->contact_id) ? null :
                                [
                                    $eventContact->contact_id => $eventContact->clientContact->first_name . ' ' . $eventContact->clientContact->last_name .
                                    (!empty($eventContact->clientContact->position_id) ? ' ( ' . $eventContact->clientContact->position->name . ' )' : '')
                                ],
                        'options' => [
                            'class' => 'contact-item-select',
                        ],                        
                        'select2Options' => [
                            'pluginOptions' => [
                                'allowClear' => false,
                                //'allowClear' => !$eventContact->isAttributeRequired('contact_id'),
                                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
                            ],
                        ],
                        'pluginOptions' => [
                            'depends' => ['client-id'],
                            //'initDepends' => ['client-id'],
                            //'initialize' => true,
                            //'initialize' => !empty($eventContact->contact_id) && empty($model[1]->id),
                            'url' => Url::to(['/client/ajax-get-client-person-list']),
                            'placeholder' => '...',
                        ]
                    ])->label(false);
                    ?>                    
                    
                    <?= $form->field($eventContact, "[{$index}]name", [    
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                        'options' => [
                            //'id' => "contact-{$index}-name",
                            'class' => 'form-group',
                            'style' => 'margin-top: 0px;',
                        ],
                    ])->textInput([
                        'maxlength' => 100,
                        'class' => 'contact-item-input', 
                    ])->label(false); ?>                    
                </td>
                <td class="col-md-10">
                    <?= $form->field($eventContact, "[{$index}]short_name", [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                    ])->textInput([
                        'maxlength' => 5,
                    ])->label(false); ?>                    
                </td>
                <td class="col-md-2 text-center vcenter">
                    <button type="button" class="delete-item btn btn-danger btn-xs"><?= Html::icon('minus');?></button>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="5"><button type="button" class="add-item btn btn-success btn-sm"><?= Html::icon('plus');?> <?= Yii::t('event', 'Add participant'); ?></button></td>
        </tr>
    </tfoot>
</table>

<?php DynamicFormWidget::end(); ?>

<?php
    $body = ob_get_contents();
    ob_get_clean(); 

    $panelContent = [
        'heading' => EventContact::modelTitle(2),
        'preBody' => '<div class="panel-body">',
        'body' => $body,
        'postBody' => '</div>',
    ];
    echo FSMHtml::panel(
        $panelContent, 
        'default', 
        [
            'id' => "panel-contact-data",
        ]
    );
?>