(function ($) {

    //$(document).ready(function () {

        var form = $('.event-form');
        var currentType = form.find('[name="Event[event_type]"]').val();
        var currentEventStatus = form.find('[name="Event[status]"]').val();
        
        if(in_array(currentType, ['call', 'email', 'sms'])){
            form.find('div.field-event-address').hide();
            form.find('div.field-event-event_datetime_finish').hide();
            
            if(in_array(currentType, ['email', 'sms'])){
                form.find('div.field-event-send_email').hide();
                form.find('#person-data-container').hide();
            }
            form.find('#contact-data-container').hide();
            form.find('#resurs-data-container').hide();
        }
        
        if(currentType != 'other'){
            form.find('.field-event-event_type_other').hide()
        }else{
            form.find('.field-event-event_type_other').show()
        }

        switch (currentEventStatus) {
            default:
            case 'planned':
                form.find('.not-planned').hide();
                form.find('.field-agreement-id').hide();
                break;
            case 'positive':
                form.find('#next-event-group').show();
                form.find('#event-result-comment-group').show();
                form.find('.field-agreement-id').hide();
                break;
            case 'negative':
                form.find('#next-event-group').hide();
                form.find('#event-result-comment-group').show();
                form.find('.field-agreement-id').hide();
                break;
            case 'last':
                form.find('.field-agreement-id').show();
                form.find('#event-result-comment-group').show();                
                break;
        }
        
        form.find('#event-event_type button').click(function () {
            var $this = $(this);
            if($this.hasClass( "active" )){
                return false;
            }
            var currentType = $(this).val();
            switch (currentType) {
                case 'call':
                case 'email':
                case 'sms':
                    form.find('div.field-event-address').hide('slow');
                    form.find('div.field-event-event_datetime_finish').hide('slow');
                    if(in_array(currentType, ['email', 'sms'])){
                        form.find('div.field-event-send_email').hide('slow');
                        form.find('#person-data-container').hide('slow');
                    }else if(in_array(currentType, ['call'])){
                        form.find('div.field-event-send_email').show('slow');
                        form.find('#person-data-container').show('slow');
                    }
                    form.find('#contact-data-container').hide('slow');
                    form.find('#resurs-data-container').hide('slow');
                    
                    form.find('div.field-client-id').show('slow');
                    break;
                    
                //case 'bus_trip':
                case 'vacation':
                    form.find('div.field-event-address').show('slow');
                    form.find('div.field-event-event_datetime_finish').show('slow');

                    form.find('div.field-client-id').hide('slow');
                    form.find('div.field-event-send_email').hide('slow');
                    form.find('#person-data-container').hide('slow');
                    form.find('#contact-data-container').hide('slow');
                    form.find('#resurs-data-container').hide('slow');
                    break;
                
                default:
                    form.find('div.field-client-id').show('slow');
                    form.find('div.field-event-address').show('slow');
                    form.find('div.field-event-event_datetime_finish').show('slow');
                    form.find('div.field-event-send_email').show('slow');
                    form.find('#person-data-container').show('slow');
                    form.find('#contact-data-container').show('slow');
                    form.find('#resurs-data-container').show('slow');
                    break;
            }
            
            if(currentType != 'other'){
                form.find('div.field-event-event_type_other').hide();
                form.find('#event-event_type_other').val('');
            }else{
                form.find('div.field-event-event_type_other').show('slow');
            }            
        });
        
        form.find('#event-status button').click(function () {
            var $this = $(this);
            if($this.hasClass( "active" )){
                return false;
            }
            var eventStatus = $(this).val();
            var currentEventType = form.find('[name="Event[event_type]"]').val();

            form.find('#agreement-id').val('').change();
            
            switch (eventStatus) {
                case 'planned':
                default:
                    form.find('.not-planned').hide("slow");
                    form.find('.field-agreement-id').hide("slow");
                    break;
                case 'positive':
                    form.find('#event-next_event_type button[value=' + currentEventType + ']').click();
                    form.find('#next-event-group').show("slow");
                    form.find('.field-agreement-id').hide("slow");
                    form.find('#event-result-comment-group').show("slow");
                    break;
                case 'negative':
                    form.find('#next-event-group').hide("slow");
                    form.find('.field-agreement-id').hide("slow");
                    form.find('#event-result-comment-group').show("slow");
                    break;
                case 'last':
                    form.find('#next-event-group').hide("slow");
                    form.find('.field-agreement-id').show("slow");
                    form.find('#event-result-comment-group').show("slow");
                    break;
            }
        });
        
        form.find('#client-id').on('change', checkClientInputState).change();
        
        function checkClientInputState(init){
            var value = form.find('#client-id').val();
            var selectList = form.find('#contact-data-container').find('.contact-item-select');
            if (value){
                selectList.closest('.form-group').show();
            }else{
                selectList.closest('.form-group').hide();
                selectList.val('').change();
            }
        }
        
        form.find('.dynamicform_wrapper_contact').bind("afterInsert", 
            function(e) {
                checkClientInputState(true);
            }
        );

        form.find('#cbx-remind').on('init.bootstrapSwitch switchChange.bootstrapSwitch', function(){
            var state = (arguments.length == 1 ? arguments[0] : (arguments[1] != undefined ? arguments[1] : null));
            switch (state) {
                case false:
                default:
                    form.find('#remind-data-container').hide('slow');
                    form.find('#event-remind_day').val('');
                    form.find('#event-remind_hours').val('');
                    break;
                case true:
                    form.find('#remind-data-container').show('slow');
                    break;
            }            
        });
        
        var currentValue = form.find('[name="Event[periodicity]"]').val();
        switchPeriodicityType(currentValue);
        
        function switchPeriodicityType(newType){
            switch (newType) {
                case 'none':
                case 'day':
                    form.find('#periodicity-data').hide("slow");
                    form.find('#periodicity-data input').val('');
                    break;
                default:
                    form.find('#periodicity-data').show("slow");
                    break;
            }
        }

        form.find('#event-periodicity button').click(function () {
            var newType = $(this).val();
            switchPeriodicityType(newType);
        });
    //});

})(window.jQuery);     