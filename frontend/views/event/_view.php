<?php
namespace common\models\event;

use Yii;

use kartik\helpers\Html;
use kartik\detail\DetailView;

use common\components\FSMAccessHelper;

/* @var $this yii\web\View */
/* @var $model common\models\event\Event */
$isModal = !empty($isModal);
?>
<p>
    <?php if(!empty($clientModel)): ?> 
    <?= $model->getBackButton().'&nbsp;'; ?>
    <?php endif; ?>    
    <?php if(FSMAccessHelper::can('updateEvent', $model) || $model->canUpdate()): ?>
    <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id, 'client_id' => (!empty($clientModel) ? $clientModel->id : null)], ['class' => 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(FSMAccessHelper::can('deleteEvent', $model) || $model->canDelete()): ?>
    <?= \common\components\FSMBtnDialog::button(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id, 'client_id' => (!empty($clientModel) ? $clientModel->id : null)], [
        'class' => 'btn btn-danger',
    ]); ?>         
    <?php endif; ?>
</p>

<p>
    <?php if($model->id) :
        if ($previousEvent = $model->previousEvent) {
            echo Html::a($model->getAttributeLabel('parent_event_id'), ['event/view', 'id' => $previousEvent->id, 'client_id' => (!empty($clientModel) ? $clientModel->id : null)], ['class' => 'btn btn-info']);
        } else {
            echo Html::a($model->getAttributeLabel('parent_event_id'), ['#'], ['class' => 'btn btn-default', 'disabled' => true]);
        }
        echo '<span stile="padding-left: 10px;"> </span>';
        if ($nextEvent = $model->nextEvent) {
            echo Html::a(Yii::t('event', 'Next event'), ['event/view', 'id' => $nextEvent->id, 'client_id' => (!empty($clientModel) ? $clientModel->id : null)], ['class' => 'btn btn-info']);
        } else {
            echo Html::a(Yii::t('event', 'Next event'), ['#'], ['class' => 'btn btn-default', 'disabled' => true]);
        }
    endif;?>    
</p>

<?php
    $attributes = [
        'id',
        [
            'attribute' => 'event_type',
            'value' => !empty($model->event_type) ? $model->eventTypeList[$model->event_type] : null,
        ],
        [
            'attribute' => 'status',
            'value' => isset($model->status) ? $model->eventStatusList[$model->status] : null,
        ],
        'name',
        [
            'attribute' => 'address',
            'visible' => !empty($model->address),
        ],
        [
            'attribute' => 'client_id',
            'value' => !empty($model->client_id) ? 
                Html::a($model->client->name, ['/client/view', 'id' => $model->client_id], ['target' => '_blank']) : null,
            'format' => 'raw',
        ],            
        [
            'attribute' => 'manager_id',
            'value' => isset($model->manager_id) ?  Html::a($model->manager->name, ['/user/profile/show', 'id' => $model->manager->user->id], ['target' => '_blank']) : null,
            'format' => 'raw',
        ],                            
        [
            'label' => Yii::t('event', 'Event start'),
            'value' => (isset($model->event_date) ? date('d-M-Y', strtotime($model->event_date)) : null).
                (isset($model->event_time) ? ' '.date('H:i', strtotime($model->event_time)) : ''),
        ],                            
        [
            'label' => Yii::t('event', 'Event finish'),
            'value' => (isset($model->event_date_finish) ? date('d-M-Y', strtotime($model->event_date_finish)) : null).
                (isset($model->event_time_finish) ? ' '.date('H:i', strtotime($model->event_time_finish)) : ''),
        ],
        [
            'attribute' => 'send_email',
            'format' => 'boolean',
        ],
        /*
        [
            'attribute' => 'remind_day',
            'value' => isset($model->remind_day) ? $model->remind_day : null,
            'visible' => !empty($model->remind_day),
        ],
        [
            'attribute' => 'remind_hours',
            'value' => isset($model->remind_hours) ? date('H:i', strtotime($model->remind_hours)) : null,
            'visible' => !empty($model->remind_day),
        ],
         * 
         */
        [
            'attribute' => 'remind_datetime',
            'value' => isset($model->remind_datetime) ? date('d-m-Y, H:i', strtotime($model->remind_datetime)) : null,
            'visible' => !empty($model->remind_datetime),
        ],
        [
            'attribute' => 'comment',
            'visible' => !empty($model->comment),
            'format' => 'ntext',
        ],
        [
            'attribute' => 'next_date',
            'value' => isset($model->next_date) ? date('d-M-Y', strtotime($model->next_date)) : null,
            'visible' => !empty($model->next_date),
        ],
        [
            'attribute' => 'next_time',
            'value' => isset($model->next_time) ? date('H:i', strtotime($model->next_time)) : null,
            'visible' => !empty($model->next_time),
        ],
        //'next_datetime',
        [
            'attribute' => 'result_comment',
            'visible' => !empty($model->result_comment),
            'format' => 'ntext',
        ],                         
        [
            'attribute' => 'periodicity',
            'value' => isset($model->periodicity) ? $model->periodicityList[$model->periodicity] : null,
            'visible' => ($model->periodicity != $model::PERIODICITY_NONE),
        ],                            
        [
            'attribute'=>'periodical_last_date',
            'value' => !empty($model->periodical_last_date) ? date('d-M-Y', strtotime($model->periodical_last_date)) : null,
            'visible' => ($model->periodicity != $model::PERIODICITY_NONE),
        ],
        [
            'attribute'=>'periodical_next_date',
            'value' => !empty($model->periodical_next_date) ? date('d-M-Y', strtotime($model->periodical_next_date)) : null,
            'visible' => ($model->periodicity != $model::PERIODICITY_NONE),
        ],                            
        [
            'attribute'=>'periodical_finish_date',
            'value' => !empty($model->periodical_finish_date) ? date('d-M-Y', strtotime($model->periodical_finish_date)) : null,
            'visible' => ($model->periodicity != $model::PERIODICITY_NONE),
        ],
        //'create_time',
        //'create_user_id',
        //'update_time',
        //'update_user_id',
        [
            'attribute' => 'deleted',
            'format' => 'boolean',
            'visible' => $isAdmin,
        ],
        [
            'attribute' => 'create_time',
            'width' => '150px',
            'headerOptions' => ['class'=>'td-mw-100'],
            'value' => isset($model->create_time) ? date('d-M-Y H:i:s', strtotime($model->create_time)) : null,
        ],
        [
            'attribute' => 'create_user_id',
            'headerOptions' => ['class'=>'td-mw-100'],
            'value' => isset($model->create_user_id) ? Html::a($model->createUser->profile->name, ['/user/profile/show', 'id' => $model->create_user_id], ['target' => '_blank', 'data-pjax' => 0]) : null,
            'format' => 'raw',
        ],            
    ];
?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => $attributes,
]); ?>

<?php if(!empty($eventProfileModel[0]->id)) :
    echo $this->render('_profile_table_view', [
        'model' => $eventProfileModel,
    ]); 
endif; ?>

<?php if(!empty($eventContactModel[0]->id)) :
    echo $this->render('_contact_table_view', [
        'model' => $eventContactModel,
    ]); 
endif; ?>

<?php if(!empty($eventResursModel[0]->id)) :
    echo $this->render('_resurs_table_view', [
        'model' => $eventResursModel,
    ]); 
endif; ?>