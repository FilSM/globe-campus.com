<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\event\search\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('event', 'All events');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">
    <?php /*
    <?= Html::pageHeader(Html::encode($this->title));?>
     * 
     */ ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <?= $this->render('_index', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'clientModel' => $clientModel,
        'managerList' => $managerList,
        'userList' => $userList,
        'isAdmin' => $isAdmin,
    ])
    ?>

</div>