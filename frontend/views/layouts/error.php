<?php

namespace common\models;

use Yii;
use yii\helpers\Url;

use kartik\helpers\Html;

use frontend\assets\AppAsset;

AppAsset::register($this);
$this->registerLinkTag(['rel' => 'shortcut icon', 'type' => 'image/png', 'href' => Url::home() . 'favicon.ico?v=' . time()]);

$user = Yii::$app->user->identity;
$isGuest = !isset($user) || $user->isGuest;
$envProd = (integer)YII_ENV_PROD;
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        
        <link href="https://fonts.googleapis.com/css?family=Montserrat|Ubuntu" rel="stylesheet">

        <?php $this->head() ?>
    </head>
    
    <body id="frontend-body" class="<?= $isGuest ? 'guest' : 'user'; ?>">
        <?php $this->beginBody() ?>
        
        <?= $content ?>

        <?php $this->endBody(); ?>
        
    </body>
</html>

<?php $this->endPage() ?>
