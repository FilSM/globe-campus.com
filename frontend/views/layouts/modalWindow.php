<?php

use yii\bootstrap\Modal;
use yii\jui\Draggable;

Draggable::begin();
Draggable::end();

$bsVersion = Yii::$app->params['bsVersion'];
$isBs4 = !empty($bsVersion) && (strpos(Yii::$app->params['bsVersion'], '3.') === false);
$modalClass = ($isBs4 ? 'yii\bootstrap4\Modal' : 'yii\bootstrap\Modal');
$modalOpts = [
    'size' => Modal::SIZE_LARGE,
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE],
    'closeButton' => ['id' => 'close-button'],
    'options' => [
        'id' => 'modal-source-window',
        'class' => 'modal-size-xl fade',
        'tabindex' => false // important for Select2 to work properly
    ],
];
if ($isBs4) {
    //$hdr = Yii::t('kvdynagrid', 'Personalize Grid Configuration');
    //$modalOpts['title'] = $hdr;
    $modalOpts['title'] = '<span id="modalHeaderTitle"></span>';
} else {
    $modalOpts['header'] = '<span id="modalHeaderTitle"></span>';
    $modalOpts['headerOptions'] = [
        'id' => 'modalHeader',
        'class' => 'modal-header type-primary bootstrap-dialog-draggable',
    ];
}

echo '<div id="modal-window-container">';

$modalClass::begin($modalOpts);
echo '<div id="modalContent"></div>';
$modalClass::end();

echo '</div>';