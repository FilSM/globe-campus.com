<?php

/* @var $this \yii\web\View */
/* @var $content string */

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

//use kartik\nav\NavX;
use kartik\widgets\AlertBlock;
use kartik\widgets\Select2;
use kartik\helpers\Html;

use common\components\FSMNavX;
use common\components\FSMAccessHelper;
use common\models\user\FSMUser;
use common\models\bill\Bill;
use common\models\bill\Expense;
use common\models\client\Client;
use common\models\client\Project;
use common\models\client\Agreement;

//use common\widgets\languageSelection\FSMLanguageSelection;
use frontend\assets\AppAsset;

AppAsset::register($this);
$this->registerLinkTag(['rel' => 'shortcut icon', 'type' => 'image/png', 'href' => Url::home().'favicon.png?v='. time()]);

$user = Yii::$app->user->identity;
$isGuest = !isset($user) || $user->isGuest;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body id="frontend-body" class="<?= $isGuest ? 'guest' : 'user';?>">
	<?php $this->beginBody() ?>
        <div class="wrap">
        <?php
            NavBar::begin(
            [
                'brandLabel' => Yii::$app->params['brandLabel'].' v.'.Yii::$app->params['brandVersion'],
                'brandUrl' => Yii::$app->urlManager->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                    //'class' => 'navbar-inverse',
                    'style' => 'z-index: 1003;'
                ],
            ]);
            
            $menuLeftItems = [
	        /*
                FSMLanguageSelection::widget([
                    //'language' => ['lv', 'en-us', 'ru'],
                    //'languageParam' => 'language',
                    //'container' => 'div', // li for navbar, div for sidebar or footer example
                    'classContainer' =>  'btn btn-default dropdown-toggle', // btn btn-default dropdown-toggle
                    'btnOptions' => [
                        'id' => 'language-selection',
                    ],
                    'menuOptions' => [
                        'id' => 'language-selection-menu'
                    ]
                ]),
		*/
                ['label' => Html::icon('home').'&nbsp;'.Yii::t('yii', 'Home'), 
                    'url' => ['/site/index'],
                    'visible' => $isGuest,
                ],
                [
                    'label' => Html::icon('log-in').'&nbsp;'.Yii::t('common', 'Login'),
                    'url' => ['/user/security/login'],
		    'visible' => $isGuest,
                ],

                
                ['label' => Yii::t('event', 'Calendar'), 
                    'url' => ['/event/calendar'],
                    'items' => [
                        [
                            'label' => Yii::t('bill', 'Add new event'), 
                            'url' => ['/event/create'],
                            'visible' => FSMAccessHelper::can('createEvent'),
                        ],
                        [
                            'label' => '<li class="divider"></li>', 
                            'visible' => FSMAccessHelper::can('createEvent'),
                        ],                        
                        [
                            'label' => Yii::t('bill', 'All events'), 
                            'url' => ['/event'],
                            'visible' => FSMAccessHelper::checkRoute('/event/*'),
                        ],
                    ],
                    'visible' => !$isGuest && FSMAccessHelper::checkRoute('/event/*'),
                ],
                
                ['label' => Project::modelTitle(2), 
                    'url' => ['/project'],
                    'items' => [
                        [
                            'label' => Yii::t('client', 'Add new project'), 
                            'url' => ['/project/create'],
                            'visible' => FSMAccessHelper::can('createProject'),
                        ],
                        [
                            'label' => '<li class="divider"></li>', 
                            'visible' => FSMAccessHelper::can('createProject'),
                        ],
                        [
                            'label' => Yii::t('client', 'All projects'), 
                            'url' => ['/project'],
                            'visible' => FSMAccessHelper::checkRoute('/project/*'),
                        ],
                    ],
                    'visible' => !$isGuest && FSMAccessHelper::checkRoute('/project/*'),
                ],
                
                ['label' => Client::modelTitle(2), 
                    'url' => ['/client'],
                    'items' => [
                        [
                            'label' => Yii::t('client', 'Add new client'), 
                            'url' => ['/client/create'],
                            'visible' => FSMAccessHelper::can('createClient'),
                        ],
                        [
                            'label' => '<li class="divider"></li>', 
                            'visible' => FSMAccessHelper::can('createClient'),
                        ],
                        [
                            'label' => Yii::t('client', 'All clients'), 
                            'url' => ['/client'],
                            'visible' => FSMAccessHelper::checkRoute('/client/*'),
                        ],
                        [
                            'label' => Yii::t('client', 'My clients'), 
                            'url' => ['/client', 'manager_id' => Yii::$app->user->identity ? (Yii::$app->user->identity->profile ? Yii::$app->user->identity->profile->id : null) : null],
                            'visible' => FSMAccessHelper::checkRoute('/client/*'),
                        ],                        
                        [
                            'label' => Yii::t('client', 'Our clients'), 
                            'url' => ['/client', 'our_clients' => true],
                            'visible' => FSMAccessHelper::checkRoute('/client/*'),
                        ],                        
                        [
                            'label' => Yii::t('client', 'External clients'), 
                            'url' => ['/client', 'our_clients' => false],
                            'visible' => FSMAccessHelper::checkRoute('/client/*'),
                        ],    
                    ],
                    'visible' => !$isGuest && FSMAccessHelper::checkRoute('/client/*'),
                ],
                
                ['label' => Agreement::modelTitle(2), 
                    'url' => ['/agreement'],
                    'items' => [
                        [
                            'label' => Yii::t('client', 'Add new agreement'), 
                            'url' => ['/agreement/create'],
                            'visible' => FSMAccessHelper::can('createAgreement'),
                        ],
                        [
                            'label' => '<li class="divider"></li>', 
                            'visible' => FSMAccessHelper::can('createAgreement'),
                        ],
                        [
                            'label' => Yii::t('client', 'All agreements'), 
                            'url' => ['/agreement'],
                            'visible' => FSMAccessHelper::checkRoute('/agreement/*'),
                        ],
                        [
                            'label' => \common\models\client\AgreementPayment::modelTitle(2), 
                            'url' => ['/agreement-payment'],
                            'visible' => FSMAccessHelper::checkRoute('/agreement-payment/*'),
                        ],
                    ],
                    'visible' => !$isGuest && FSMAccessHelper::checkRoute('/agreement/*'),
                ],
                
                ['label' => Bill::modelTitle(2), 
                    'url' => ['/bill'],
                    'items' => [
                        [
                            'label' => Yii::t('bill', 'Add new invoice'), 
                            'url' => ['/bill/create'],
                            'visible' => FSMAccessHelper::can('createBill'),
                        ],
                        [
                            'label' => '<li class="divider"></li>', 
                            'visible' => FSMAccessHelper::can('createBill'),
                        ],                        
                        [
                            'label' => Yii::t('bill', 'All invoices'), 
                            'url' => ['/bill'],
                            'visible' => FSMAccessHelper::checkRoute('/bill/*'),
                        ],
                        [
                            'label' => \common\models\bill\BillPayment::modelTitle(2), 
                            'url' => ['/bill-payment'],
                            'visible' => FSMAccessHelper::checkRoute('/bill-payment/*'),
                        ],
                        [
                            'label' => \common\models\bill\TaxPayment::modelTitle(2), 
                            'url' => ['/tax-payment'],
                            'visible' => FSMAccessHelper::checkRoute('/tax-payment/*'),
                        ],
                        [
                            'label' => \common\models\bill\Convention::modelTitle(2), 
                            'url' => ['/convention'],
                            'visible' => FSMAccessHelper::checkRoute('/convention/*'),
                        ],
                        [
                            'label' => '<li class="divider"></li>', 
                            'visible' => 
                                FSMAccessHelper::checkRoute('/bill-template/*') ||
			    	FSMAccessHelper::checkRoute('/payment-order/*') ||
			    	FSMAccessHelper::checkRoute('/payment-confirm/*'),
                        ],
                        [
                            'label' => \common\models\bill\BillTemplate::modelTitle(2),
                            'url' => ['/bill-template'],
                            'visible' => FSMAccessHelper::checkRoute('/bill-template/*'),
                        ],                          
                        [
                            'label' => Yii::t('bill', 'Export / Import'), 
                            'items' => [
                                [
                                    'label' => \common\models\bill\PaymentOrder::modelTitle(2),
                                    'url' => ['/payment-order/index'],
                                    'visible' => FSMAccessHelper::checkRoute('/payment-order/*'),
                                ],
                                [
                                    'label' => \common\models\bill\PaymentConfirm::modelTitle(2), 
                                    'url' => ['/payment-confirm/index'],
                                    'visible' => FSMAccessHelper::checkRoute('/payment-confirm/*'),
                                ],
                            ],
                            'visible' => FSMAccessHelper::checkRoute('/payment-order/*') || FSMAccessHelper::checkRoute('/payment-confirm/*'),
                        ],
                    ],
                    'visible' => !$isGuest && FSMAccessHelper::checkRoute('/bill/*'),
                ],
                ['label' => Expense::modelTitle(2), 
                    'url' => ['/expense'],
                    'items' => [
                        [
                            'label' => Yii::t('bill', 'Add new expense'), 
                            'url' => ['/expense/create'],
                            'visible' => FSMAccessHelper::can('createExpense'),
                        ],
                        [
                            'label' => '<li class="divider"></li>', 
                            'visible' => FSMAccessHelper::can('createExpense'),
                        ],                        
                        [
                            'label' => Yii::t('bill', 'All expenses'), 
                            'url' => ['/expense'],
                            'visible' => FSMAccessHelper::checkRoute('/expense/*'),
                        ],
                    ],
                    'visible' => !$isGuest && FSMAccessHelper::checkRoute('/expense/*'),
                ],
                                
                ['label' => Yii::t('report', 'Reports'), 
                    'items' => [
                        [
                            'label' => Yii::t('report', 'Debtors/Creditors'), 
                            'url' => ['/base-report/debitor-creditor'],
                            'visible' => FSMAccessHelper::checkRoute('/base-report/debitor-creditor'),
                        ],
                        [
                            'label' => Yii::t('report', 'EBITDA'),
                            'items' => [
                                [
                                    'label' => Yii::t('report', 'by Projects'), 
                                    'url' => ['/base-report/ebitda'],
                                    'visible' => FSMAccessHelper::checkRoute('/base-report/ebitda'),
                                ],
                                [
                                    'label' => Yii::t('report', 'by Clients'), 
                                    'url' => ['/base-report/ebitda-client-base'],
                                    'visible' => FSMAccessHelper::checkRoute('/base-report/ebitda-client-base'),
                                ],
                                [
                                    'label' => Yii::t('report', 'by Clients (PLUS)'), 
                                    'url' => ['/base-report/ebitda-client-plus'],
                                    'visible' => FSMAccessHelper::checkRoute('/base-report/ebitda-client-plus'),
                                ],
                            ],
                            'visible' => 
                                FSMAccessHelper::checkRoute('/base-report/ebitda') || 
                                FSMAccessHelper::checkRoute('/base-report/ebitda-client-base') || 
                                FSMAccessHelper::checkRoute('/base-report/ebitda-client-plus'),
                        ],
                        [
                            'label' => Yii::t('report', 'VAT Report'), 
                            'url' => ['/base-report/vat'],
                            'visible' => FSMAccessHelper::checkRoute('/base-report/vat'),
                        ],
                        [
                            'label' => Yii::t('report', 'Bank statements'), 
                            'url' => ['/client-bank/report'],
                            'visible' => FSMAccessHelper::checkRoute('/client-bank/report'),
                        ],
                        [
                            'label' => Yii::t('report', 'Payment of invoices'), 
                            'url' => ['/base-report/payment'],
                            'visible' => FSMAccessHelper::checkRoute('/base-report/payment'),
                        ],
                        [
                            'label' => '<li class="divider"></li>', 
                            'visible' => FSMAccessHelper::checkRoute('/dashboard/*'),
                        ],
                        [
                            'label' => Yii::t('common', 'New documents'), 
                            'url' => ['/dashboard'],
                            'visible' => FSMAccessHelper::checkRoute('/dashboard/*'),
                        ],                    
                    ],
                    'visible' => !$isGuest && (FSMAccessHelper::checkRoute('/base-report') || FSMAccessHelper::checkRoute('/base-report/*')),
                ],
                /*
                ['label' => Yii::t('common', 'About us'), 
                    'url' => ['/site/about'],
                    'visible' => $isGuest,
                ],
                ['label' => Yii::t('common', 'Contacts'), 
                    'url' => ['/site/contact'],
                    'visible' => $isGuest,
                ],
                 * 
                 */
            ];

            $menuRightItems = [];
            
            if(!$isGuest){
                $countUserAbonent = Yii::$app->session->get('user_abonent_id');
                if(isset($countUserAbonent) && (count($countUserAbonent) > 1)){
                    $dataAbonent = array_combine(Yii::$app->session->get('user_abonent_id'), Yii::$app->session->get('user_abonent_name'));
                    asort($dataAbonent);
                    $currentAbonentId = (int)Yii::$app->session->get('user_current_abonent_id');
                    $selectAbonent = Select2::widget([
                        'name' => 'abonent_id',
                        'data' => $dataAbonent,
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'value' => $currentAbonentId,
                        'options' => [
                            'placeholder' => 'Select a abonent...',
                        ],
                        'pluginEvents' => [
                            "change" => "function() {
                                var form = $('#switch-abonent-form');
                                form.submit();
                            }",
                        ]
                    ]);
                    $swithAbonentForm = 
                        Html::beginForm(['/abonent/switch-abonent'], 'post', ['class' => 'navbar-form', 'id' => 'switch-abonent-form', 'style' => 'min-width: 200px;'])
                        . $selectAbonent
                        . Html::endForm();
                    $menuRightItems = ArrayHelper::merge($menuRightItems, [
                        'label' => '<li>'.$swithAbonentForm.'</li>',
                        //'visible' => count(Yii::$app->session->get('user_abonent_id')) > 1,
                    ]);
                }
                
                $gravatar_id = $user->profile ? $user->profile->gravatar_id : null;
                if(!$gravatar_id){
                    $linkOptions = [
                        'title' => Yii::t('user', 'Profile') . ' (' . Yii::$app->user->identity->username . ')', 
                        'class' => 'glyphicon glyphicon-user',
                        'style' => 'font-size: 1.7em;'
                    ];
                }else{
                    $linkOptions = [
                        //'title' => Yii::t('user', 'Profile') . ' (' . Yii::$app->user->identity->username . ')', 
                        'class' => 'gravatar',
                        //'style' => 'font-size: 1.5em;'
                    ];                    
                }
                $menuRightItems = ArrayHelper::merge($menuRightItems, [
                    ['label' => $gravatar_id ? "<img src='http://gravatar.com/avatar/{$gravatar_id}>?s=28' alt='{$user->username}'/> ".($user->profile ? $user->profile->name : '') : '',
                        'items' => [
                            [
                                'label' => 
                                    '<li>' 
                                        . Html::beginForm(['/user/admin/switch'], 'post', ['class' => 'navbar-form'])
                                            . Html::submitButton(Html::icon('user'). '&nbsp;' . Yii::t('user', 'Back to original user'),
                                                ['class' => 'btn']
                                            ) 
                                        . Html::endForm() 
                                    . '</li>',
                                'visible' => Yii::$app->session->has(\dektrium\user\controllers\AdminController::ORIGINAL_USER_SESSION_KEY),
                            ],
                            [
                                'label' => Yii::t('user', 'Users'), 
                                'url' => ['/user/admin/index'],
                                'visible' => FSMUser::getIsSystemAdmin() || FSMAccessHelper::can('administrateUser'),
                            ],
                            [
                                'label' => '<li class="divider"></li>', 
                                'visible' => FSMUser::getIsSystemAdmin() || Yii::$app->session->has(\dektrium\user\controllers\AdminController::ORIGINAL_USER_SESSION_KEY),
                            ],
                            [
                                'label' => Yii::t('user', 'Profile'), 
                                'url' => ['/user/profile/show', 'id' => (isset($user) ? $user->id : null)],
                            ],
                            [
                                'label' => Yii::t('fsmuser', 'My company'), 
                                'url' => ['/client/view', 'id' => (isset($user, $user->firstClient) ? $user->firstClient->id : null)],
                                'linkOptions' => [
                                    'target' => '_blank', 
                                ],                                
                                'visible' => (isset($user, $user->firstClient) && FSMAccessHelper::can('viewClient') ? true : false)
                            ],
                            '<li class="divider"></li>',
                            [
                                'label' => Html::icon('cog').'&nbsp;'.Yii::t('common', 'Admin'),
                                'url' => ['/backend'],
                                'visible' => Yii::$app->user->can('showBackend'),
                            ],
                            [
                                'label' => Html::icon('cog').'&nbsp;'.Yii::t('admin', 'Run Daily procedure'),
                                'url' => ['/daemon/daily'],
                                'linkOptions' => ['target' => '_blank', 'data-pjax' => 0],
                                'visible' => FSMUser::getIsPortalAdmin(),
                            ],
                            [
                                'label' => Html::icon('cog').'&nbsp;'.Yii::t('admin', 'Run Frequent procedure'),
                                'url' => ['/daemon/frequent'],
                                'linkOptions' => ['target' => '_blank', 'data-pjax' => 0],
                                'visible' => FSMUser::getIsPortalAdmin(),
                            ],
                            [
                                'label' => Html::icon('log-out').'&nbsp;'.Yii::t('common', 'Log Out'),
                                'url' => ['/user/security/logout'],
                                'linkOptions' => [
                                    //'title' => Yii::t('common', 'Log Out'), 
                                    //'class' => 'glyphicon glyphicon-log-out',
                                    'data-method' => 'post'
                                ],
                            ],
                        ],
                        'linkOptions' => $linkOptions,
                    ],
                ]);
            } 
            
            echo FSMNavX::widget([
                'options' => ['class' => 'navbar-nav navbar-left'],
                'items' => $menuLeftItems,
                'activateParents' => true,
                'encodeLabels' => false
            ]);
            
            echo FSMNavX::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuRightItems,
                'activateParents' => true,
                'encodeLabels' => false
            ]);            
            
            NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]); ?>

            <?= AlertBlock::widget([
                    'delay' => 0,
                    //'delay' => 5000,
                ]); 
            ?>  

            <?php /*echo Alert::widget();*/ ?>
            <?= $content ?>
        </div>
    </div>
    <?php if(!empty(Yii::$app->params['SHOW_COPYRIGHT'])) : ?>
    <footer class="footer" style="padding-top: 0;">
        <div class="container">
            <p class="pull-left" style="padding-top: 20px;"><?= '&copy; '.Yii::$app->params['brandOwner']; ?></p>
        </div>
    </footer>
    <?php endif;?>
    
    <?= $this->render('modalWindow'); ?>
    <?= $this->render('sessionTimer'); ?>
    
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
