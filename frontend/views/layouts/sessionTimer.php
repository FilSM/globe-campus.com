<?php

use yii\web\JsExpression;

use kartik\dialog\Dialog;

    echo Dialog::widget([
        'overrideYiiConfirm' => false,
        'libName' => 'dialogSessionTimer', // a custom lib name
        'options' => [// customized BootstrapDialog options
            'id' => 'modal-timer',
            'size' => Dialog::SIZE_SMALL,
            'type' => Dialog::TYPE_WARNING,
            'title' => Yii::t('common', 'Session expired'),
            'closable' => false,
            'draggable' => true,
            'minute_txt' => Yii::t('common', 'minute'),
            'message' =>
            '<p class="text-center" style="font-weight: bold;">' .
            Yii::t('common', 'Your session will end in <span id="time_to_logout" style="color: red; font-size: 1.5em">{showMinutes}</span>' .
                    ' <span id="time_to_logout_txt">{minutes, plural, =0{minutes} =1{minute} =2{minutes} =3{minutes} =4{minutes} other{minutes}}</span>',
                    [
                        'showMinutes' => Yii::$app->params['dialogTimer.showMinutes'],
                        'minutes' => Yii::$app->params['dialogTimer.showMinutes'],
                    ]
            ) .
            '</p>' .
            '<p class="text-center">' .
            Yii::t('common', 'Do you want to continue the session?') .
            '</p>',
            'buttons' => [
                [
                    'id' => 'timer-no',
                    'label' => Yii::t('common', 'No'),
                    'action' => new JsExpression("function(dialog) {
                            $.post(
                                logoutUrl, 
                                'json'
                            );                            
                            //window.location.assign(logoutUrl);
                        }")
                ],
                [
                    'id' => 'timer-yes',
                    'label' => Yii::t('common', 'Yes'),
                    'action' => new JsExpression("function(dialog) {
                            dialog.close();
                            location.reload();
                        }")
                ],
            ]
        ]
    ]);
?>
