<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

use common\models\mainclass\FSMBaseModel;

$this->title = '404';
$this->params['show-page-title'] = false;

$imagesUrl = Url::to('@web/images');
?>

<section class="section section-md bg-default text-center mt-5 mb-5">
    <div class="container">
        <h2 class="content-header text-center mb-5">
            <?= Yii::t('yii', 'The requested page was not found.')?>
        </h2>
        <?= Html::img($imagesUrl . '/404.webp', ['style' => 'max-width: 100%;']) ?>
        <p class="mt-5"></p>
        <?= Html::a(Html::button(Yii::t('front', 'Back'), ['class' => 'btn btn-raised btn-info waves-effect']), FSMBaseModel::getBackUrl()) ?>
    </div>
</section>
