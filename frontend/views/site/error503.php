<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

use common\models\mainclass\FSMBaseModel;

$this->title = '503';
$this->params['show-page-title'] = false;

$imagesUrl = Url::to('@web/images');
?>

<section class="section section-md bg-default text-center mt-5 mb-5">
    <div class="container">
        <h2 class="text-spacing-20">
            <?= Yii::t('front', 'Service is Temporarily Unavailable')?>
        </h2>
        <?= Html::img($imagesUrl . '/503.webp', ['style' => 'width: 25%;']) ?>
        <p class="heading-5 mw-650 block-center text-default">
            <?= Yii::t('front', 'Sorry, we\'re offline right now to make our site even better. Please, come back later and check what we\'ve been up to.')?>
        </p>
        <?= Html::a(Html::button(Yii::t('front', 'Back'), ['class' => 'btn btn-raised btn-info waves-effect']), FSMBaseModel::getBackUrl()) ?>
    </div>
</section>
