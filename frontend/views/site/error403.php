<?php

use yii\helpers\Html;
use yii\helpers\Url;

use common\models\mainclass\FSMBaseModel;

$this->title = '403';
$this->params['show-page-title'] = false;

$imagesUrl = Url::to('@web/images');
?>

<section class="section section-md bg-default text-center mt-5 mb-5">
    <div class="container">
        <h2 class="content-header text-center mb-5">
            <?= Yii::t('yii', 'You are not allowed to perform this action.')?>
        </h2>
        <?= Html::img($imagesUrl . '/403.webp', ['style' => 'max-width: 100%;']) ?>
        <p class="mt-5"></p>
        <?= Html::a(Html::button(Yii::t('front', 'Back'), ['class' => 'btn btn-raised btn-info waves-effect']), FSMBaseModel::getBackUrl()) ?>
    </div>
</section>
