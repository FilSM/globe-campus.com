<?php

use kartik\helpers\Html;
use kartik\detail\DetailView;

use common\components\FSMAccessHelper;

/* @var $this yii\web\View */
/* @var $model common\models\bill\Convention */

$this->title = $model->modelTitle().': '.$model->conventionTypeList[$model->doc_type] .' #'. $model->doc_number;
if(FSMAccessHelper::checkRoute('/convention/index')){
    $this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="convention-doc-view">

    <?= Html::pageHeader(Html::encode($this->title)); ?>

    <div class='col-md-12' style="margin-bottom: 5px;">
        <?php if(FSMAccessHelper::can('updateConvention', $model)): ?>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>
        <?php if(FSMAccessHelper::can('deleteConvention', $model)): ?>
        <?= \common\components\FSMBtnDialog::button(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'id' => 'btn-dialog-selected',
            'class' => 'btn btn-danger',
        ]); ?>
        <?php endif; ?>
    </div> 
    <div class='col-md-12'>
        <?php 
            $attributes = [
                [
                    'attribute' => 'id',
                    'visible' => $isAdmin,
                ], 
                [
                    'attribute' => 'doc_type',
                    'value' => isset($model->doc_type) ? $model->conventionTypeList[$model->doc_type] : null,
                ],
                [
                    'attribute' => 'doc_number',
                    'valueColOptions' => ['style' => 'font-weight: bold; font-size: 150%;'],
                    'format'=>'raw',
                ],                         
                [
                    'attribute' => 'doc_date',
                    'value' => date('d-M-Y', strtotime($model->doc_date)),
                    'valueColOptions' => ['style' => 'font-weight: bold; font-size: 150%;'],
                    'format'=>'raw',
                ],           
                
                'comment:ntext',
                [
                    'attribute' => 'deleted',
                    'format' => 'boolean',
                    'visible' => $isAdmin,
                ],
            ]
        ?>
        <?php
            $panelContent = [
                'heading' => Yii::t('common', 'Common data'),
                'preBody' => '<div class="list-group-item">',
                'body' => DetailView::widget([
                    'model' => $model,
                    'attributes' => $attributes,
                ]),
                'postBody' => '</div>',
            ];                        
            echo Html::panel(
                $panelContent, 
                'primary', 
                [
                    'id' => "common-data",
                ]
            );                        
        ?>   
    </div>
    
    <div class='col-md-12'>
        <div class='col-md-4' style="padding-left: 0;">
            <?php 
                $attributes = [
                    [
                        'label' => Yii::t('client', 'Full name'),
                        'value' => !empty($firstClientModel->id) ? 
                            (FSMAccessHelper::can('viewClient', $firstClientModel) ?
                                Html::a($firstClientModel->name, ['/client/view', 'id' => $firstClientModel->id], ['target' => '_blank']) :
                                $firstClientModel->name
                            )
                             : null,
                        'format'=>'raw',
                        'valueColOptions' => ['style' => 'font-weight: bold; font-size: 150%;'],
                    ],
                    [
                        'label' => Yii::t('client', 'Reg.number'),
                        'value' => !empty($firstClientModel->id) ? $firstClientModel->reg_number : null,
                    ],
                    [
                        'label' => Yii::t('client', 'VAT number'),
                        'value' => !empty($firstClientModel->id) ? $firstClientModel->vat_number : null,
                    ],
                    [
                        'label' => Yii::t('client', 'Legal address'),
                        'value' => !empty($firstClientModel->id) ? $firstClientModel->fullLegalAddress : null,
                    ],
                ]
            ?>
            <?php
                $panelContent = [
                    'heading' => Yii::t('bill', 'First party data'),
                    'preBody' => '<div class="list-group-item">',
                    'body' => DetailView::widget([
                        'model' => $firstClientModel,
                        'attributes' => $attributes,
                    ]),
                    'postBody' => '</div>',
                ];                        
                echo Html::panel(
                    $panelContent, 
                    'success', 
                    [
                        'id' => "first-party-data",
                    ]
                );                        
            ?>   
        </div>
        
        <div class='col-md-4' style="padding-left: 0; padding-right: 0;">
            <?php 
                $attributes = [
                    [
                        'label' => Yii::t('client', 'Full name'),
                        'value' => !empty($secondClientModel->id) ? 
                            (FSMAccessHelper::can('viewClient', $secondClientModel) ?
                                Html::a($secondClientModel->name, ['/client/view', 'id' => $secondClientModel->id], ['target' => '_blank']) :
                                $secondClientModel->name
                            )
                            : null,
                        'format'=>'raw',
                        'valueColOptions' => ['style' => 'font-weight: bold; font-size: 150%;'],
                    ],
                    [
                        'label' => Yii::t('client', 'Reg.number'),
                        'value' => !empty($secondClientModel->id) ? $secondClientModel->reg_number : null,
                    ],
                    [
                        'label' => Yii::t('client', 'VAT number'),
                        'value' => !empty($secondClientModel->id) ? $secondClientModel->vat_number : null,
                    ],
                    [
                        'label' => Yii::t('client', 'Legal address'),
                        'value' => !empty($secondClientModel->id) ? $secondClientModel->fullLegalAddress : null,
                    ],
                ]
            ?>
            <?php
                $panelContent = [
                    'heading' => Yii::t('bill', 'Second party data'),
                    'preBody' => '<div class="list-group-item">',
                    'body' => DetailView::widget([
                        'model' => $secondClientModel,
                        'attributes' => $attributes,
                    ]),
                    'postBody' => '</div>',
                ];                        
                echo Html::panel(
                    $panelContent, 
                    'success', 
                    [
                        'id' => "second-party-data",
                    ]
                );                        
            ?>   
        </div>
        
        <div class='col-md-4' style="padding-right: 0;">
            <?php 
                $attributes = [
                    [
                        'label' => Yii::t('client', 'Full name'),
                        'value' => !empty($thirdClientModel->id) ? 
                            (FSMAccessHelper::can('viewClient', $thirdClientModel) ?
                                Html::a($thirdClientModel->name, ['/client/view', 'id' => $thirdClientModel->id], ['target' => '_blank']) :
                                $thirdClientModel->name
                            )
                            : null,
                        'format'=>'raw',
                        'valueColOptions' => ['style' => 'font-weight: bold; font-size: 150%;'],
                    ],
                    [
                        'label' => Yii::t('client', 'Reg.number'),
                        'value' => !empty($thirdClientModel->id) ? $thirdClientModel->reg_number : null,
                    ],
                    [
                        'label' => Yii::t('client', 'VAT number'),
                        'value' => !empty($thirdClientModel->id) ? $thirdClientModel->vat_number : null,
                    ],
                    [
                        'label' => Yii::t('client', 'Legal address'),
                        'value' => !empty($thirdClientModel->id) ? $thirdClientModel->fullLegalAddress : null,
                    ],
                ]
            ?>
            <?php
                $panelContent = [
                    'heading' => Yii::t('bill', 'Third party data'),
                    'preBody' => '<div class="list-group-item">',
                    'body' => DetailView::widget([
                        'model' => $thirdClientModel,
                        'attributes' => $attributes,
                    ]),
                    'postBody' => '</div>',
                ];                        
                echo Html::panel(
                    $panelContent, 
                    'success', 
                    [
                        'id' => "third-party-data",
                    ]
                );                        
            ?>   
        </div>
    </div>
    
    <div class='col-md-12'>
        <?= $this->render('_attachment_table', [
            'model' => $filesModel,
        ]) ?>         
    </div>
</div>