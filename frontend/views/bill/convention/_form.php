<?php
namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;
use kartik\widgets\FileInput;
use kartik\widgets\DatePicker;
use kartik\icons\Icon;

use common\widgets\EnumInput;
use common\components\FSMAccessHelper;

/* @var $this yii\web\View */
/* @var $model common\models\bill\Convention */
/* @var $form yii\widgets\ActiveForm */

Icon::map($this);
$isModal = !empty($isModal);
$isAdmin = !empty($isAdmin);
?>

<div class="convention-doc-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => $model->tableName().'-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>

    <?= Html::activeHiddenInput($model, 'abonent_id'); ?>
    
    <?= $form->field($model, 'doc_type')->widget(EnumInput::class, [
            'type' => EnumInput::TYPE_RADIOBUTTON,
            'options' => [
                'translate' => $model->conventionTypeList,
            ],
        ]); 
    ?>  
    
    <?= $form->field($model, 'first_client_id')->widget(Select2::class, [
            'data' => $clientList,
            'options' => [
                //'id' => 'first-client-id', 
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => true,
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],            
            'addon' => [
                'prepend' => $model::getModalButtonContent([
                    'formId' => $form->id,
                    'prefix' => 'first-',
                    'controller' => 'client',
                ]),
            ],        
        ]); 
    ?>
    
    <?= $form->field($model, 'second_client_id')->widget(Select2::class, [
            'data' => $clientList,
            'options' => [
                //'id' => 'second-client-id', 
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => true,
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],            
            'addon' => [
                'prepend' => $model::getModalButtonContent([
                    'formId' => $form->id,
                    'prefix' => 'second-',
                    'controller' => 'client',
                ]), 
            ],        
        ]); 
    ?>    
    
    <?= $form->field($model, 'third_client_id')->widget(Select2::class, [
            'data' => $clientList,
            'options' => [
                //'id' => 'third-client-id', 
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => true,
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],            
            'addon' => [
                'prepend' => $model::getModalButtonContent([
                    'formId' => $form->id,
                    'prefix' => 'third-',
                    'controller' => 'client',
                ]), 
            ],        
        ]); 
    ?>

    <?= $form->field($model, 'doc_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'doc_date')->widget(DatePicker::class, [
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
        ]); 
    ?>
    
    <?php
    if(!empty($filesModel)){
        $preview = $previewConfig = [];
        foreach ($filesModel as $key => $file) {
            if(empty($file->id)){
                continue;
            }
            $preview[] = $file->uploadedFileUrl;
            $ext = pathinfo($file->filename, PATHINFO_EXTENSION);
            switch ($ext) {
                default:
                case 'pdf':
                    $type = 'pdf';
                    break;
                case 'doc':
                case 'docx':
                case 'edoc':
                case 'bdoc':
                case 'xls':
                case 'xlsx':
                    $type = 'gdocs';
                    break;
                case 'zip':
                    $type = 'object';
                    break;
            }
            $previewConfig[] = [
                'type' => $type,
                'size' => $file->filesize, 
                'caption' => $file->filename, 
                'url' => Url::to(['/convention/attachment-delete', 'deleted_id' => $file->id]), 
                'key' => 101 + $key,
                'downloadUrl' => $file->uploadedFileUrl,
            ];
        }
    
        echo $form->field($filesModel[0], 'file[]')->widget(FileInput::class, [
            'language' =>  strtolower(substr(Yii::$app->language, 0, 2)),
            'sortThumbs' => true,
            'options' => [
                'id' => 'bill-files',
                'multiple' => true,
            ],
            'pluginOptions' => [
                'allowedFileExtensions' => ['png', 'jpg', 'jpeg', 'pdf', 'doc', 'docx', 'edoc', 'bdoc', 'asice', 'xls', 'xlsx', 'ppt', 'zip'],
                'uploadAsync' => false,
                'maxFileSize' => 20000,
                'showRemove' => false,
                'showUpload' => false,
                'overwriteInitial' => false,
                'initialPreview' => $preview,
                'initialPreviewAsData' => true,
                'initialPreviewFileType' => 'pdf',
                'initialPreviewConfig' => $previewConfig,
                'initialPreviewShowDelete' => true,            
                
                'preferIconicPreview' => true, // this will force thumbnails to display icons for following file extensions
                'previewFileIcon' => Icon::show('file'),
                'allowedPreviewTypes' => null, // set to empty, null or false to disable preview for all types                
                'previewFileIconSettings' => [ // configure your icon file extensions
                    'png' => Icon::show('file-image', ['class' => 'text-info']),
                    'jpg' => Icon::show('file-image', ['class' => 'text-info']),
                    'jpeg' => Icon::show('file-image', ['class' => 'text-info']),
                    'pdf' => Icon::show('file-pdf', ['class' => 'text-danger']),
                    'doc' => Icon::show('file-word', ['class' => 'text-primary']),
                    'docx' => Icon::show('file-word', ['class' => 'text-primary']),
                    'edoc' => Icon::show('file-word', ['class' => 'text-primary']),
                    'bdoc' => Icon::show('file-word', ['class' => 'text-primary']),
                    'xls' => Icon::show('file-excel', ['class' => 'text-success']),
                    'xlsx' => Icon::show('file-excel', ['class' => 'text-success']),
                    'zip' => Icon::show('file-archive', ['class' => 'text-muted']),
                ],
            ]
        ])->label(Yii::t('files', 'Attachment'))->hint('Allowed file extensions: .png .jpg .jpeg .pdf .doc .docx .edoc .bdoc .xls .xlsx .zip'); 
    }
    ?>
    
    <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>

    <?php if(!empty($model->id) && !empty($isAdmin)){
        echo $form->field($model, 'deleted')->widget(SwitchInput::class, [
            'pluginOptions' => [
                'onText' => Yii::t('common', 'Yes'),
                'offText' => Yii::t('common', 'No'),
            ],
        ]);
    } ?>
    
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10" style="text-align: right;">
            <?= $model->SubmitButton; ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>