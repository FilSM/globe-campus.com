<?php
namespace common\models;

use Yii;
//use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\grid\GridView;

use kartik\helpers\Html;
use kartik\grid\GridView;

use common\components\FSMAccessHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\bill\search\Convention */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $searchModel->modelTitle(2);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="convention-doc-index">
    
    <?= Html::pageHeader(Html::encode($this->title));?>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if(FSMAccessHelper::can('createConvention')): ?>
    <p>
        <?= Html::a(Html::icon('plus').'&nbsp;'.$searchModel->modelTitle(), ['create'], ['class' => 'btn btn-success']); ?>
    </p>
    <?php endif; ?>    
    
    <?php
        $columns = [
            //['class' => '\kartik\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'width' => '75px',
                'hAlign' => 'center',
            ],
            [
                'attribute' => 'doc_type',
                'headerOptions' => ['class'=>'td-mw-100'],
                'value' => function ($model) {
                    return isset($model->doc_type) ? $model->ConventionTypeList[$model->doc_type] : null;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $searchModel->ConventionTypeList,
                'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                'filterInputOptions' => ['placeholder' => '...'],
                'format'=>'raw',
            ],            
            [
                'attribute'=>'doc_number',
                'width' => '150px',
                'headerOptions' => ['class'=>'td-mw-100'],
                'value' => function ($model) {
                    return !empty($model->doc_number) ? 
                        (FSMAccessHelper::can('viewConvention', $model) ?
                            Html::a($model->doc_number, ['/convention/view', 'id' => $model->id], ['target' => '_blank', 'data-pjax' => 0]) :
                            $model->doc_number
                        ) : null;
                },                         
                'format'=>'raw',
            ],                          
            [
                'attribute' => 'doc_date',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class'=>'td-mw-75'],
                'value' => function ($model) {
                    return isset($model->doc_date) ? date('d-M-Y', strtotime($model->doc_date)) : null;
                },
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [
                    'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
                    'pickerButton' => false,
                    'layout' => '{input}{remove}',
                    'buttonOptions' => [],
                ],                            
                'format'=>'raw',        
            ],                         
            [
                'attribute'=>'first_client_name',
                //'headerOptions' => ['class'=>'td-mw-100'],
                'contentOptions' => [
                    'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                ],
                'value' => function ($model) {
                    return 
                        '<div style="overflow-x: auto;">'.
                        (!empty($model->first_client_name) ? 
                        (FSMAccessHelper::can('viewClient', $model->firstClient) ?
                            Html::a($model->first_client_name, ['/client/view', 'id' => $model->first_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                            $model->first_client_name
                        )
                        : null).
                        '</div>';
                },                           
                'format'=>'raw',
            ],                          
            [
                'attribute'=>'second_client_name',
                //'headerOptions' => ['class'=>'td-mw-100'],
                'contentOptions' => [
                    'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                ],
                'value' => function ($model) {
                    return  
                        '<div style="overflow-x: auto;">'.
                        (!empty($model->second_client_name) ?  
                        (FSMAccessHelper::can('viewClient', $model->secondClient) ?
                            Html::a($model->second_client_name, ['/client/view', 'id' => $model->second_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                            $model->second_client_name
                        )
                        : null).
                        '</div>';
                },
                'format'=>'raw',
            ],
            [
                'attribute'=>'third_client_name',
                //'headerOptions' => ['class'=>'td-mw-100'],
                'contentOptions' => [
                    'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                ],
                'value' => function ($model) {
                    return  
                        '<div style="overflow-x: auto;">'.
                        (!empty($model->third_client_name) ?  
                        (FSMAccessHelper::can('viewClient', $model->thirdClient) ?
                            Html::a($model->third_client_name, ['/client/view', 'id' => $model->third_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                            $model->third_client_name
                        )
                        : null).
                        '</div>';
                },
                'format'=>'raw',
            ],

            [
                'attribute' => "deleted",   
                'vAlign' => 'middle',
                'class' => '\kartik\grid\BooleanColumn',
                'trueLabel' => 'Yes', 
                'falseLabel' => 'No',
                'width' => '100px',
                //'visible' => ($isAdmin || FSMAccessHelper::can('showDeletedItems')),
            ],
            [
                'class' => '\common\components\FSMActionColumn',
                'headerOptions' => ['class'=>'td-mw-125'],
                'dropdown' => true,
                'checkPermission' => true,
            ],
        ];
    ?>    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsive' => false,
        //'striped' => false,
        'hover' => true,      
        'floatHeader' => true,
        'pjax' => true,
        'columns' => $columns,
    ]); ?>
</div>