<?php

use kartik\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\bill\Convention */

$this->title = Yii::t($model->tableName(), 'Create a new '.$model->modelTitle(1, false));
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="convention-doc-create">

    <?= Html::pageHeader(Html::encode($this->title)); ?>
    

    <?= $this->render('_form', [
        'model' => $model,
        'firstClientModel' => $firstClientModel,
        'secondClientModel' => $secondClientModel,
        'thirdClientModel' => $thirdClientModel,
        'filesModel' => $filesModel,
        'clientList' => $clientList,
        'isAdmin' => $isAdmin,
    ]) ?>

</div>