<?php

use kartik\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\bill\BillConfirm */

$this->title = Yii::t($model->tableName(), 'Create a new '.$model->modelTitle(1, false));
$this->params['breadcrumbs'][] = ['label' => $paymentConfirmModel->modelTitle(2), 'url' => ['/payment-confirm/index']];
$this->params['breadcrumbs'][] = ['label' => $paymentConfirmModel->name, 'url' => ['/payment-confirm/view', 'id' => $paymentConfirmModel->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bill-confirm-create">

    <?php if(!$isModal): ?>
    <?= Html::pageHeader(Html::encode($this->title));?>
    <?php endif; ?>

    <?= $this->render('_form_manual', [
        'model' => $model,
        'paymentConfirmModel' => $paymentConfirmModel,
        'confirmLinkModel' => $confirmLinkModel,
        'billList' => $billList,
        'agreementList' => $agreementList,
        'expenseList' => $expenseList,
        'purchaseList' => $purchaseList,
        'valutaList' => $valutaList,
        'clientList' => null,
        'isAdmin' => !empty($isAdmin),
        'isModal' => $isModal,
    ]) ?>

</div>