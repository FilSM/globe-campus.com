<?php

use yii\helpers\Url;
//use yii\widgets\MaskedInput;
use yii\helpers\ArrayHelper;

use kartik\helpers\Html;
use kartik\widgets\Select2;
use kartik\depdrop\DepDrop;

use common\widgets\dynamicform\DynamicFormWidget;
use common\widgets\MaskedInput;
use common\components\FSMHtml;
use common\models\bill\BillConfirmLink;

$isModal = !empty($isModal);
?>

<?php
    echo Html::beginTag('div', [
        'class' => 'form-group',
    ]);
    echo Html::label(Yii::t('bill', 'Documents'), null, ['class' => 'control-label has-star col-md-3']);
    echo Html::beginTag('div', [
        'class' => 'col-md-9',
    ]);
?>

<?php
    ob_start();
    ob_implicit_flush(false);
?>

<?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_wrapper',
    'widgetBody' => '.form-docs-body',
    'widgetItem' => '.form-docs-item',
    'min' => 1,
    'insertButton' => '.add-item',
    'deleteButton' => '.delete-item',
    'model' => $model[0],
    'formId' => $form->id,
    'formFields' => [
        'doc_type',
        'doc_id',
        'vat',
        'summa',
    ],
]); ?>

<table class="table table-bordered table-striped margin-b-none">
    <thead>
        <tr>
            <th class="required" style="width: 30%;"><?= $model[0]->getAttributeLabel('doc_type'); ?></th>
            <th class="required" style="width: 50%;"><?= $model[0]->getAttributeLabel('doc_id'); ?></th>
            <th style="width: 10%; text-align: right;"><?= $model[0]->getAttributeLabel('vat'); ?></th>
            <th class="required" style="width: 10%; text-align: right;"><?= $model[0]->getAttributeLabel('summa'); ?></th>
            <th style="width: 90px; text-align: center"><?= Yii::t('kvgrid', 'Actions'); ?></th>
        </tr>
    </thead>
    <tbody class="form-docs-body">
        <?php 
        $total = 0;
        foreach ($model as $index => $doc): 
            $total += $doc->summa;
            $list = [];
            switch ($doc->doc_type) {
                case BillConfirmLink::DOC_TYPE_BILL:
                    $list = $billList;
                    break;
                case BillConfirmLink::DOC_TYPE_AGREEMENT:
                    $list = $agreementList;
                    break;
                case BillConfirmLink::DOC_TYPE_EXPENSE:
                    $list = $expenseList;
                    break;
                case BillConfirmLink::DOC_TYPE_DIRECT:
                    $list = $purchaseList;
                    break;
            }
        ?>
            <tr class="form-docs-item">
                <td>
                    <?= Html::activeHiddenInput($doc, "[{$index}]id"); ?>
                    
                    <?= $form->field($doc, "[{$index}]doc_type", [    
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                        'options' => [
                            //'id' => "doc-{$index}-doc_type",
                            'class' => 'form-group',
                        ],
                    ])->widget(Select2::class, [
                        'data' => ArrayHelper::merge(['0' => ''] ,BillConfirmLink::getDocTypeList()), 
                        'options' => [
                            'placeholder' => '...',
                            'id' => "doc-{$index}-doc_type",
                            'class' => 'doc_type-item-select',
                            'value' => (!empty($doc->doc_type) ? $doc->doc_type : null),
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
                        ],
                    ])->label(false); ?>
                </td>                
                <td>
                    <?= $form->field($doc, "[{$index}]doc_id", [    
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                        'options' => [
                            'id' => "doc-{$index}-doc_id",
                            'class' => 'form-group',
                        ],
                    ])->widget(DepDrop::class, [
                        'type' => DepDrop::TYPE_SELECT2,
                        'data' => $list,
                        'options' => [
                            'class' => 'doc-doc_id',
                            'placeholder' => '...',
                        ],  
                        'select2Options' => [
                            'pluginOptions' => [
                                'allowClear' => !$doc->isAttributeRequired('doc_id'),
                                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
                            ],            
                            'addon' => [
                                'prepend' => [
                                    'content' => 
                                        $doc->getModalButton([
                                            'formId' => $form->id,
                                            'controller' => $doc->doc_type,
                                            'params' => [
                                                'source' => 'bill-confirm',
                                                'payment_confirm_id' => $paymentConfirmModel->id,
                                                'bill_confirm_id' => $billConfirmModel->id,
                                            ],
                                        ]).
                                        Html::a(Html::icon('eye-open'), 
                                            Url::to(["/{$doc->doc_type}/view", 'id' => $doc->doc_id]),
                                            [
                                                'id' => "btn-{$index}-view-doc", 
                                                'class'=>'btn btn-info btn-view-doc',
                                                'target' => '_blank',
                                                'data-pjax' => 0,
                                                'style' => empty($doc->doc_id) ? 'display: none;' : '',
                                            ]
                                        ),
                                    'asButton' => true, 
                                ],
                            ],
                        ],
                        'pluginOptions' => [
                            'depends' => ["billconfirm-{$index}-id", "doc-{$index}-doc_type"],
                            //'initialize' => !empty($doc->doc_id),
                            //'initDepends' => ["billconfirm-{$index}-id", "doc-{$index}-doc_type"],
                            'url' => Url::to([
                                '/bill-confirm/ajax-get-doc-list', 
                                'payment_confirm_id' => $paymentConfirmModel->id, 
                                'bill_confirm_id' => $billConfirmModel->id,
                            ]),
                            'placeholder' => '...',
                        ],                        
                    ])->label(false);?>                    
                </td>
                <td>
                    <?= $form->field($doc, "[{$index}]vat", [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                    ])->widget(MaskedInput::class, [
                        //'mask' => '[-]9{1,10}[.9{1,2}]',
                        'options' => [
                            'id' => "doc-{$index}-vat",
                            'class' => 'form-control number-field doc-vat',
                            'style' => 'text-align: right;',
                            'readonly' => $doc->doc_type != BillConfirmLink::DOC_TYPE_DIRECT,
                        ],
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'rightAlign' => false,
                            'digits' => 3,
                            'allowMinus' => true,
                        ],                                    
                    ])->label(false); ?>
                </td>
                <td>
                    <?= $form->field($doc, "[{$index}]summa", [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                    ])->widget(MaskedInput::class, [
                        //'mask' => '[-]9{1,10}[.9{1,2}]',
                        'options' => [
                            'id' => "doc-{$index}-summa",
                            'class' => 'form-control number-field doc-summa',
                            'style' => 'text-align: right;',
                        ],
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'rightAlign' => false,
                            'digits' => 3,
                            'allowMinus' => true,
                        ],                                    
                    ])->label(false); ?>
                </td>
                <td class="text-center vcenter">
                    <button type="button" class="delete-item btn btn-danger btn-xs"><?= Html::icon('minus');?></button>
                </td>
            </tr>

        <?php endforeach; ?>
            
    </tbody>
    <tfoot>
        <tr id='total-row'>
            <td colspan="3"></td>                
            <td>
                <?= Html::input('text', 'total', number_format($total, 2, '.', ''), [
                    'id' => 'total-input', 
                    'class' => 'form-control', 
                    'style' => 'text-align: right;', 
                    'readonly' => true,
                ]); ?>
            </td>
            <td></td>                
        </tr>
        <tr>
            <td colspan="10"><button type="button" class="add-item btn btn-success btn-sm"><?= Html::icon('plus');?> <?= Yii::t('doc', 'Add doc'); ?></button></td>
        </tr>
    </tfoot>
</table>

<?php DynamicFormWidget::end(); ?>

<?php
    $body = ob_get_contents();
    ob_get_clean(); 

    //echo $body;
    $panelContent = [
        //'heading' => Yii::t('bill', 'Documents'),
        'preBody' => '<div class="panel-body">',
        'body' => $body,
        'postBody' => '</div>',
    ];
    echo FSMHtml::panel(
        $panelContent,
        'default', 
        [
            'id' => "panel-doc-data",
        ]
    );
?>

<?php
    echo Html::endTag('div');
    echo Html::endTag('div');      
?>
