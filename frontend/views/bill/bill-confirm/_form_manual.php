<?php
namespace common\models;

use Yii;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;

use common\widgets\EnumInput;

/* @var $this yii\web\View */
/* @var $model common\models\bill\BillConfirm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bill-confirm-form">
    <?php if($isModal) : Pjax::begin(Yii::$app->params['PjaxModalOptions']); endif; ?>

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => $model->tableName().'-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'formConfig' => [
            'labelSpan' => 3,
        ],        
        'options' => [
            'data-pjax' => $isModal,
        ],          
    ]); ?>

    <?= Html::activeHiddenInput($model, 'payment_confirm_id'); ?>
    <?= Html::hiddenInput('main_client_id', $paymentConfirmModel->client_id, ['id' => 'billconfirm-main_client_id']); ?>
    <?= Html::activeHiddenInput($model, 'second_client_id'); ?>
    <?= Html::activeHiddenInput($model, 'id'); ?>
    <?= Html::activeHiddenInput($model, 'direction'); ?>
    <?= Html::activeHiddenInput($model, 'currency'); ?>
    <?= Html::activeHiddenInput($model, 'manual_input', ['value' => 1]); ?>

    <?= $this->render('_doc_table_edit', [
        'form' => $form,
        'model' => $confirmLinkModel,
        'paymentConfirmModel' => $paymentConfirmModel,
        'billConfirmModel' => $model,
        'billList' => $billList,
        'agreementList' => $agreementList,
        'expenseList' => $expenseList,
        'purchaseList' => $purchaseList,
        'isModal' => $isModal,
    ]); ?>    
    
    <?= $form->field($model, 'first_client_account', [
        'options' => [
            'id' => 'first-client-account-static-text',
            'class' => 'form-group',
        ],
    ])->textInput(['readonly' => true]); ?>

    <?= $form->field($model, 'second_client_name', [
        'options' => [
            'id' => 'second-client-name-static-text',
            'class' => 'form-group',
        ],
    ])->textInput(['readonly' => true]); ?>

    <?= $form->field($model, 'second_client_reg_number', [
        'options' => [
            'id' => 'second-client-reg-number-static-text',
            'class' => 'form-group',
        ],
    ])->textInput(['readonly' => true]); ?>

    <?= $form->field($model, 'second_client_account', [
        'options' => [
            'id' => 'second-client-account-static-text',
            'class' => 'form-group',
        ],
    ])->textInput(['readonly' => true]); ?>

    <?= $form->field($model, 'direction')->widget(EnumInput::class, [
            'type' => EnumInput::TYPE_RADIOBUTTON,
            'options' => [
                'translate' => $model->directionList,
            ],
        ]); 
    ?>
    
    <?= $form->field($model, 'doc_date')->widget(DatePicker::class, [
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
            'options' => [
                'class' => 'rate-changer-date',
                'data-currency_input_id' => 'valuta-id',
                'data-rate_input_id' => 'rate',
            ],
        ]); 
    ?>    
    
    <?= $form->field($model, 'doc_number')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'bank_ref')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'summa', [
        'addon' => [
            'append' => [
                ['content' => 
                    MaskedInput::widget([
                        'model' => $model,
                        'attribute' => 'rate',
                        //'mask' => '9{1,10}[.9{1,4}]',
                        'options' => [
                            'id' => 'rate', 
                            'placeholder' => 'Rate',
                            'class' => 'form-control number-field',
                            'style' => 'text-align: right; min-width: 90px;'
                        ],
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'rightAlign' => false,
                            'digits' => 4,
                            'allowMinus' => false,
                        ],                                    
                    ]),
                    'asButton' => true,
                ],
                ['content' => 
                    Select2::widget([
                        'model' => $model,
                        'attribute' => 'valuta_id',
                        'data' => $valutaList, 
                        'options' => [
                            'id' => 'valuta-id', 
                            'class' => 'rate-changer-currency',
                            'placeholder' => '...',
                            'data-date_input_id' => 'billconfirm-doc_date',
                            'data-rate_input_id' => 'rate',
                        ],   
                        'pluginOptions' => [
                            'allowClear' => !$model->isAttributeRequired('valuta_id'),
                        ],            
                        'size' => 'control-width-90',
                    ]),
                    'asButton' => true,
                ],
            ],
        ],
        //'labelOptions' => ['class' => 'col-md-3'],
    ])->widget(MaskedInput::class, [
        //'mask' => '9{1,10}[.9{1,2}]',
        'options' => [
            'class' => 'form-control number-field',
        ],
        'clientOptions' => [
            'alias' => 'decimal',
            'rightAlign' => false,
            'digits' => 2,
            'allowMinus' => false,
        ],                                    
    ])->textInput([
        'readonly' => true,
    ])->label($model->getAttributeLabel('summa').' / '.$model->getAttributeLabel('rate').' / '.$model->getAttributeLabel('valuta_id'));
    ?>
    
    <?= $form->field($model, 'comment')->textarea(['rows' => 3, 'readonly' => !empty($model->id) && empty($model->manual_input)]) ?>

    <div class="form-group <?php if($isModal) : echo 'modal-button-group'; endif; ?>">
        <div class="col-md-offset-3 col-md-9" style="text-align: right;">
            <?= $model->SaveButton; ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <?php if($isModal) : Pjax::end(); endif; ?>

</div>

<?php
$this->registerJs(
    "var form = $('#bill-form');
    form.find('#rate').change(function(){
        form.yiiActiveForm('validateAttribute', 'bill-total');                    
    });",
    \yii\web\View::POS_END
);
?>