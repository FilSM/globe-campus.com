<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\bill\BillConfirm */

$this->title = Yii::t($model->tableName(), 'Update a '.$model->modelTitle(1, false)) . ': ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => $paymentConfirmModel->modelTitle(2), 'url' => ['/payment-confirm/index']];
$this->params['breadcrumbs'][] = ['label' => $paymentConfirmModel->name, 'url' => ['/payment-confirm/view', 'id' => $paymentConfirmModel->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Edit');
?>
<div class="bill-confirm-update">

    <?php if(!$isModal): ?>
    <?= Html::pageHeader(Html::encode($this->title));?>
    <?php endif; ?>

    <?= $this->render((empty($model->manual_input) ? '_form' : '_form_manual'), [
        'model' => $model,
        'paymentConfirmModel' => $paymentConfirmModel,
        'confirmLinkModel' => $confirmLinkModel,
        'billList' => $billList,
        'agreementList' => $agreementList,
        'expenseList' => $expenseList,
        'purchaseList' => $purchaseList,
        'valutaList' => $valutaList,
        'clientList' => $clientList,
        'isAdmin' => !empty($isAdmin),
        'isModal' => $isModal,
    ]) ?>

</div>