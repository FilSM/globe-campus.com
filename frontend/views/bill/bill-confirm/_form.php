<?php
namespace common\models;

use Yii;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;

use common\widgets\EnumInput;

/* @var $this yii\web\View */
/* @var $model common\models\bill\BillConfirm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bill-confirm-form">
    <?php if($isModal) : Pjax::begin(Yii::$app->params['PjaxModalOptions']); endif; ?>

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => $model->tableName().'-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'formConfig' => [
            'labelSpan' => 3,
        ],        
        'options' => [
            'data-pjax' => $isModal,
        ],          
    ]); ?>

    <?= Html::activeHiddenInput($model, 'id'); ?>
    <?= Html::activeHiddenInput($model, 'payment_confirm_id'); ?>
    <?= Html::hiddenInput('main_client_id', $paymentConfirmModel->client_id, ['id' => 'billconfirm-main_client_id']); ?>    
    <?= Html::activeHiddenInput($model, 'direction'); ?>
    <?= Html::activeHiddenInput($model, 'currency'); ?>
    <?= Html::activeHiddenInput($model, 'rate'); ?>
    <?= Html::activeHiddenInput($model, 'valuta_id'); ?>
    <?= Html::activeHiddenInput($model, 'manual_input', ['value' => 0]); ?>

    <?= $this->render('_doc_table_edit', [
        'form' => $form,
        'model' => $confirmLinkModel,
        'paymentConfirmModel' => $paymentConfirmModel,
        'billConfirmModel' => $model,
        'billList' => $billList,
        'agreementList' => $agreementList,
        'expenseList' => $expenseList,
        'purchaseList' => $purchaseList,
        'isModal' => $isModal,
    ]); ?>    
    
    <?= $form->field($model, 'first_client_account', [
        'options' => [
            'class' => 'form-group static-input',
        ],
        'staticValue' => (isset($model->first_client_account) ? $model->first_client_account : ''),
    ])->staticInput(['class' => 'form-control', 'disabled' => true])
    ->label(Yii::t('bill', ($model->direction == 'C' ? 'Recipient' : 'Payer'))); ?>

    <?php if(!empty($model->second_client_id)) : ?>
        <?= Html::activeHiddenInput($model, 'second_client_id'); ?>
        <?= $form->field($model, 'second_client_name', [
            'options' => [
                'class' => 'form-group static-input',
            ],
            'staticValue' => (isset($model->second_client_name) ? $model->second_client_name : ''),
        ])->staticInput(['class' => 'form-control', 'disabled' => true])
        ->label(Yii::t('bill', ($model->direction == 'C' ? 'Payer name' : 'Recipient name'))); ?>

        <?= $form->field($model, 'second_client_reg_number', [
            'options' => [
                'class' => 'form-group static-input',
            ],
            'staticValue' => (isset($model->second_client_reg_number) ? $model->second_client_reg_number : ''),
        ])->staticInput(['class' => 'form-control', 'disabled' => true])
        ->label(Yii::t('bill', ($model->direction == 'C' ? 'Payer reg.number' : 'Recipient reg.number'))); ?>

    <?php else: 
    
        echo $form->field($model, 'second_client_id')->widget(Select2::class, [
            'data' => $clientList,
            'options' => [
                //'id' => 'first-client-id', 
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => true,
            ],            
            'addon' => [
                'prepend' => $model::getModalButtonContent([
                    'formId' => $form->id,
                    'controller' => 'client',
                ]),
            ],        
        ]); 
    endif;
    ?>
    
    <?= $form->field($model, 'second_client_account', [
        'options' => [
            'class' => 'form-group static-input',
        ],
        'staticValue' => (isset($model->second_client_account) && ($model->second_client_account != 'NULL') ? $model->second_client_account : ''),
    ])->staticInput(['class' => 'form-control', 'disabled' => true])
    ->label(Yii::t('bill', ($model->direction == 'C' ? 'Payer account' : 'Recipient account'))); ?>

    <?= $form->field($model, 'doc_date', [
        'options' => [
            'class' => 'form-group static-input',
        ],
        'staticValue' => (isset($model->doc_date) ? $model->doc_date : ''),
    ])->staticInput(['class' => 'form-control', 'disabled' => true]); ?>

    <?= $form->field($model, 'doc_number', [
        'options' => [
            'class' => 'form-group static-input',
        ],
        'staticValue' => (isset($model->doc_number) ? $model->doc_number : ''),
    ])->staticInput(['class' => 'form-control', 'disabled' => true]); ?>

    <?= $form->field($model, 'direction', [
        'options' => [
            'class' => 'form-group static-input',
        ],
        'staticValue' => (isset($model->direction) ? $model->directionList[$model->direction] : ''),
    ])->staticInput(['class' => 'form-control', 'disabled' => true]); ?>

    <?= $form->field($model, 'summa', [
        'options' => [
            'class' => 'form-group static-input',
        ],
        'staticValue' => (isset($model->summa) ? number_format($model->summa, 2, '.', ' ').(!empty($model->currency) ? ' '.$model->currency : '') : ''),
    ])->staticInput(['class' => 'form-control', 'disabled' => true]); ?>
    
    <?= $form->field($model, 'comment')->textarea(['rows' => 3, 'disabled' => !empty($model->id) && empty($model->manual_input)]) ?>

    <div class="form-group <?php if($isModal) : echo 'modal-button-group'; endif; ?>">
        <div class="col-md-offset-3 col-md-9" style="text-align: right;">
            <?= $model->SaveButton; ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <?php if($isModal) : Pjax::end(); endif; ?>

</div>