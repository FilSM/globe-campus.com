<?php
namespace common\models;

use Yii;
use yii\widgets\Pjax;
use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;

use common\widgets\EnumInput;

/* @var $this yii\web\View */
/* @var $model common\models\bill\PaymentOrder */
/* @var $form yii\widgets\ActiveForm */
$isModal = !empty($isModal);
$isAdmin = !empty($isAdmin);
?>

<div class="payment-order-form">
    <?php if($isModal) : Pjax::begin(Yii::$app->params['PjaxModalOptions']); endif; ?>    
    <?php if(!isset($fromPayment)) : ?>
    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => $model->tableName().'-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'options' => [
            'data-pjax' => $isModal,
        ],
    ]); ?>
    <?php endif; ?>

    <?= Html::activeHiddenInput($model, 'abonent_id'); ?>
    
    <?= $form->field($model, "bank_id")->widget(Select2::class, [
        'data' => $bankList, 
        'options' => [
            'placeholder' => '...',
        ],
        'pluginOptions' => [
            'allowClear' => true,
            'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
        ],
        'addon' => [
            'prepend' => $model->getModalButtonContent([
                'formId' => $form->id,
                'controller' => 'bank',
                'isModal' => $isModal,
            ]),
        ],                          
    ]); ?>
    
    <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pay_date')->widget(DatePicker::class, [
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
        ]); 
    ?>
    
    <?php if(!empty($model->id) && !empty($isAdmin)) :
        echo $form->field($model, 'status')->widget(EnumInput::class, [
            'type' => EnumInput::TYPE_RADIOBUTTON,
            'options' => [
                'translate' => $model->exportStateList,
            ],
        ]);
    else: ?>
        <?= Html::activeHiddenInput($model, 'status'); ?>
    <?php endif; ?>
    
    <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>

    <?php if(!isset($fromPayment)) : ?>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10" style="text-align: right;">
            <?= $model->SubmitButton; ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>
    <?php endif; ?>

    <?php if(!isset($fromPayment)) : ?>    
    <?php ActiveForm::end(); ?>
    <?php endif; ?>
    <?php if($isModal) : Pjax::end(); endif; ?>

</div>