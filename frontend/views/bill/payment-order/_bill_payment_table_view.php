<?php

use yii\widgets\Pjax;

use kartik\helpers\Html;
use kartik\grid\GridView;

use common\components\FSMAccessHelper;
use common\models\bill\BillPayment;
?>

<?php 
    ob_start();
    ob_implicit_flush(false);
?>

<div class="bill-payment-index">
    <?= GridView::widget([
        'responsive' => false,
        //'striped' => false,
        'hover' => true,
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'pjax' => true, 
        'pjaxSettings' => [
            'enablePushState' => false,
            'enableReplaceState' => false,
        ],
        'columns' => [
            ['class' => '\kartik\grid\SerialColumn'],
            [
                'attribute'=>'bill_number',
                'headerOptions' => ['class'=>'td-mw-100'],
                'value' => function ($model) {
                    return !empty($model->bill_id) ? Html::a($model->bill_number, ['/bill/view', 'id' => $model->bill_id], ['target' => '_blank', 'data-pjax' => 0,]) : null;
                },                         
                'format'=>'raw',
            ],  
            [
                'attribute'=>'first_client_id',
                'contentOptions' => [
                    'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                ],
                'value' => function ($model) {
                    $firstClient = $model->bill->firstClient;
                    return 
                        '<div style="overflow-x: auto;">'.
                            (FSMAccessHelper::can('viewClient', $firstClient) ?
                                Html::a($firstClient->name, ['/client/view', 'id' => $firstClient->id], ['target' => '_blank', 'data-pjax' => 0]) :
                                $firstClient->name
                            ).
                        '</div>';
                },
                'format'=>'raw',
            ], 
            [
                'attribute' => 'paid_date',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class'=>'td-mw-75'],
                'width' => '150px',
                'value' => function ($model) {
                    return isset($model->paid_date) ? date('d-M-Y', strtotime($model->paid_date)) : null;
                },
            ],                          
            [
                'attribute' => 'summa_eur',
                'hAlign' => 'right',
                'width' => '150px',
                'headerOptions' => ['style'=>'text-align: center;'],
                'mergeHeader' => true,
                'value' => function ($model) {
                    return isset($model->summa_eur) ? $model->summa_eur : null;
                },
                'format' => ['decimal', 2],
            ],  
                        /*
            [
                'attribute' => 'confirmed',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class'=>'td-mw-75'],
                'width' => '150px',
                'value' => function ($model) {
                    return isset($model->confirmed) ? date('d-M-Y', strtotime($model->confirmed)) : null;
                },
            ],
                         * 
                         */
            [
                'attribute' => "combine",   
                'vAlign' => 'middle',
                'class' => '\kartik\grid\BooleanColumn',
                'trueLabel' => 'Yes', 
                'falseLabel' => 'No',
                'width' => '100px',
            ],                        
            [
                'class' => '\common\components\FSMActionColumn',
                'headerOptions' => ['class'=>'td-mw-125'],
                'checkCanDo' => true,
                //'dropdown' => true,
                //'isDropdownActionColumn' => true,
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function ($params) { 
                        return BillPayment::getButtonUpdate($params);
                    },
                ], 
                'controller' => 'bill-payment',
                'linkedObj' => [
                    ['fieldName' => 'payment_order_id', 'id' => (!empty($linkedModel->id) ? $linkedModel->id : null)],
                ],                
            ],                        
        ],
    ]); ?>
</div>

<?php
    $body = ob_get_contents();
    ob_get_clean(); 

    $panelContent = [
        'heading' => Yii::t('bill', 'Invoice list'),
        'preBody' => '<div class="panel-body">',
        'body' => $body,
        'postBody' => '</div>',
    ];
    echo Html::panel(
        $panelContent, 
        'success', 
        [
            'id' => "panel-bill-payment-data",
        ]
    );
?>