<?php

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\bill\search\TaxPaymentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tax-payment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'paid_date') ?>

    <?= $form->field($model, 'from_client_id') ?>

    <?php //echo $form->field($model, 'to_client_id') ?>

    <?php //echo $form->field($model, 'summa') ?>

    <?php //echo $form->field($model, 'valuta_id') ?>

    <?php //echo $form->field($model, 'rate') ?>

    <?php //echo $form->field($model, 'summa_eur') ?>

    <?php //echo $form->field($model, 'confirmed')->widget(SwitchInput::class, [
//             'pluginOptions' => [
//                 'onText' => Yii::t('common', 'Yes'),
//                 'offText' => Yii::t('common', 'No'),
//             ],
//         ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('common', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('common', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>