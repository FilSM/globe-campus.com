<?php
namespace common\models;

use Yii;
use yii\widgets\Pjax;
use yii\widgets\MaskedInput;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\bill\TaxPayment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tax-payment-form">
    <?php if($isModal) : Pjax::begin(Yii::$app->params['PjaxModalOptions']); endif; ?>

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => $model->tableName().'-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>
    
    <?= Html::activeHiddenInput($model, 'abonent_id'); ?>
    
    <?= $form->field($model, 'from_client_id')->widget(Select2::class, [
            'data' => $clientList,
            'options' => [
                'id' => 'from-client-id',
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => !$model->isAttributeRequired('from_client_id'),
            ],            
        ]);
    ?>
    
    <?= $form->field($model, 'to_client_id')->widget(Select2::class, [
            'data' => $clientList,
            'options' => [
                'id' => 'to-client-id',
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => !$model->isAttributeRequired('to_client_id'),
            ],            
        ]);
    ?>

    <?= $form->field($model, 'paid_date')->widget(DatePicker::class, [
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
            'options' => [
                'class' => 'rate-changer-date',
                'data-currency_input_id' => 'valuta-id',
                'data-rate_input_id' => 'rate',
            ],
            'pluginEvents' => [
                "change" => "function() {
                    var form = $('#{$model->tableName()}' + '-form');
                    var data = form.data('yiiActiveForm');
                    $.each(data.attributes, function() {
                        this.status = 3;
                    });
                    form.yiiActiveForm('validate');
                }",
            ],          
        ]); 
    ?>
    
    <?= $form->field($model, 'summa', [
        'addon' => [
            'append' => [
                ['content' => 
                    MaskedInput::widget([
                        'model' => $model,
                        'attribute' => 'rate',
                        //'mask' => '9{1,10}[.9{1,4}]',
                        'options' => [
                            'id' => 'rate', 
                            'placeholder' => 'Rate',
                            'class' => 'form-control number-field',
                            'style' => 'text-align: right; min-width: 90px;'
                        ],
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'rightAlign' => false,
                            'digits' => 4,
                            'allowMinus' => false,
                        ],                                    
                    ]),
                    'asButton' => true,
                ],
                ['content' => 
                    Select2::widget([
                        'model' => $model,
                        'attribute' => 'valuta_id',
                        'data' => $valutaList, 
                        'options' => [
                            'id' => 'valuta-id', 
                            'class' => 'rate-changer-currency',
                            'placeholder' => '...',
                            'data-date_input_id' => 'taxpayment-paid_date',
                            'data-rate_input_id' => 'rate',
                        ],
                        'pluginOptions' => [
                            'allowClear' => !$model->isAttributeRequired('valuta_id'),
                        ],            
                        'size' => 'control-width-90',
                    ]),
                    'asButton' => true,
                ],
            ],
        ],
        //'labelOptions' => ['class' => 'col-md-3'],
    ])->widget(MaskedInput::class, [
        //'mask' => '9{1,10}[.9{1,2}]',
        'options' => [
            'class' => 'form-control number-field',
            //'readonly' => true,
        ],
        'clientOptions' => [
            'alias' => 'decimal',
            'rightAlign' => false,
            'digits' => 2,
            'allowMinus' => false,
        ],                                    
    ])->label($model->getAttributeLabel('summa').' / '.$model->getAttributeLabel('rate').' / '.$model->getAttributeLabel('valuta_id'));
    ?>
    
    <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>

    <?php if(!empty($model->id) && !empty($isAdmin)){
        echo $form->field($model, 'confirmed')->widget(DatePicker::class, [
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
        ]); 
    } ?>    
    
    <div class="form-group <?php if($isModal) : echo 'modal-button-group'; endif; ?>">
        <div class="col-md-offset-2 col-md-10" style="text-align: right;">
            <?= $model->saveButton; ?>
            <?= $model->cancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <?php if($isModal) : Pjax::end(); endif; ?>

</div>

<?php
$this->registerJs(
    "var form = $('#tax_payment-form');
    form.find('#rate').change(function(){
        form.yiiActiveForm('validateAttribute', 'taxpayment-summa');                    
    });",
    \yii\web\View::POS_END
);
?>