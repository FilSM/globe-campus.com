<?php

use kartik\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\bill\TaxPayment */

$this->title = Yii::t($model->tableName(), 'Create a new '.$model->modelTitle(1, false));
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$isModal = !empty($isModal);
?>
<div class="tax-payment-create">

    <?= Html::pageHeader(Html::encode($this->title)); ?>
    
    <?= $this->render('_form', [
        'model' => $model,
        'clientList' => $clientList,
        'valutaList' => $valutaList,
        'isAdmin' => !empty($isAdmin),
        'isModal' => $isModal, 
    ]) ?>

</div>