<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\bill\TaxPayment */

$this->title = Yii::t($model->tableName(), 'Update a '.$model->modelTitle(1, false)) . ': #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Edit');

$isModal = !empty($isModal);
?>
<div class="tax-payment-update">

    <?php if(!$isModal): ?>
    <?= Html::pageHeader(Html::encode($this->title));?>
    <?php endif; ?>

    <?= $this->render('_form', [
        'model' => $model,
        'clientList' => $clientList,
        'valutaList' => $valutaList,
        'isAdmin' => !empty($isAdmin),
        'isModal' => $isModal,        
    ]) ?>

</div>