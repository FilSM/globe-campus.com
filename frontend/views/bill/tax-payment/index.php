<?php
namespace common\models;

use Yii;
//use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\grid\GridView;

use kartik\helpers\Html;
use kartik\grid\GridView;

use common\models\bill\TaxPayment;
use common\components\FSMAccessHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\bill\search\TaxPaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $searchModel->modelTitle(2);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tax-payment-index">

    <?= Html::pageHeader(Html::encode($this->title)); ?>
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if(FSMAccessHelper::can('createTaxPayment')): ?>
    <p>
        <?= Html::a(Html::icon('plus').'&nbsp;'.$searchModel->modelTitle(), ['create'], ['class' => 'btn btn-success']); ?>
    </p>
    <?php endif; ?>
    
    <?php 
    $columns = [
        //['class' => '\kartik\grid\SerialColumn'],
        [
            'attribute' => 'id',
            'width' => '75px',
            'hAlign' => 'center',
            //'pageSummary' => $searchModel->getAttributeLabel('total'),
        ],
        [
            'attribute'=>'from_client_id',
            'contentOptions' => [
                'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
            ],
            'value' => function ($model) {
                return 
                    '<div style="overflow-x: auto;">'.
                    (!empty($model->from_client_name) ? 
                        Html::a($model->from_client_name, ['/client/view', 'id' => $model->from_client_id], ['target' => '_blank', 'data-pjax' => 0])
                        : null
                    ).
                    '</div>';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $clientList,
            'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
            'filterInputOptions' => [
                'id' => 'taxpayment-from_client_name',
                'placeholder' => '...',
            ],                            
            'format'=>'raw',
        ],                    
        [
            'attribute'=>'to_client_id',
            'contentOptions' => [
                'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
            ],
            'value' => function ($model) {
                return 
                    '<div style="overflow-x: auto;">'.
                    (!empty($model->to_client_name) ? 
                        Html::a($model->to_client_name, ['/client/view', 'id' => $model->to_client_id], ['target' => '_blank', 'data-pjax' => 0])
                        : null
                    ).
                    '</div>';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $clientList,
            'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
            'filterInputOptions' => [
                'id' => 'taxpayment-to_client_name',
                'placeholder' => '...',
            ],                            
            'format'=>'raw',
        ], 
        [
            'attribute' => 'paid_date',
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'width' => '150px',
            'headerOptions' => ['class'=>'td-mw-75'],
            'value' => function ($model) {
                return isset($model->paid_date) ? date('d-M-Y', strtotime($model->paid_date)) : null;
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'pluginOptions' => \yii\helpers\ArrayHelper::merge(
                    Yii::$app->params['DatePickerPluginOptions'], 
                    [
                        'locale' => [
                            'firstDay' => 1,
                            'format' => 'd-m-Y',
                            'cancelLabel' => Yii::t('common', 'Clear'),
                        ],
                        //'opens'=>'left',
                        //'startDate' => date('d-m-Y'),
                    ]),
                'convertFormat' => true,
                //'presetDropdown' => true,
                'hideInput' => true,
                'pluginEvents' => [
                    'cancel.daterangepicker' => 'function() {
                        //console.log("cancel.daterangepicker"); 
                        var gridFilters = $("table tr.filters");
                        var daterangeinput = gridFilters.find("#taxpaymentsearch-paid_date");
                        daterangeinput.val("").change();
                    }',
                ],
            ],
            'format'=>'raw',        
        ],                     
        [
            'attribute' => 'summa_eur',
            'hAlign' => 'right',
            'headerOptions' => ['style'=>'text-align: center;'],
            'mergeHeader' => true,
            'value' => function ($model) {
                return isset($model->summa_eur) ? $model->summa_eur : null;
            },
            'format' => ['decimal', 2],
            //'pageSummary' => true,                 
        ],        
        [
            'class' => '\common\components\FSMActionColumn',
            'headerOptions' => ['class'=>'td-mw-125'],
            'checkCanDo' => true,
            'dropdown' => true,
            'template' => '{confirm} {view} {update} {delete}',
            'checkCanDo' => true,
            'buttons' => [
                'confirm' => function ($params) { 
                    return TaxPayment::getButtonConfirm($params);
                },
                'update' => function ($params) { 
                    return TaxPayment::getButtonUpdate($params);
                },
            ], 
        ],
    ];
    ?>
    
    <?= GridView::widget([
        'id' => 'grid-view',
        /*
        'tableOptions' => [
            'id' => 'bill-payment-list',
        ],
         * 
         */
        'responsive' => false,
        'striped' => true,
        'hover' => true,
        'bordered' => true,
        'condensed' => true,
        'persistResize' => false,
        'floatHeader' => true,
        'autoXlFormat' => true,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        //'showPageSummary' => true,
        'pjax' => true,
        'columns' => $columns,
    ]); ?> 
</div>