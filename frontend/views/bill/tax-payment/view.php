<?php

use kartik\helpers\Html;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\bill\TaxPayment */

$this->title = $model->modelTitle() .' #'. $model->id;
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tax-payment-view">

    <?= Html::pageHeader(Html::encode($this->title)); ?>

    <p>
        <?php if($model->canUpdate()){
            echo Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
        } 
        ?>
        <?php if($model->canDelete()){
            echo\common\components\FSMBtnDialog::button(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
                'id' => 'btn-dialog-selected',
                'class' => 'btn btn-danger',
            ]); 
        } 
        ?>            
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'from_client_id',
                'value' => !empty($model->from_client_id) ? 
                    Html::a($model->fromClient->name, ['/client/view', 'id' => $model->from_client_id], ['target' => '_blank'])
                    : null,
                'format' => 'raw',
            ],              
            [
                'attribute' => 'to_client_id',
                'value' => !empty($model->to_client_id) ? 
                    Html::a($model->toClient->name, ['/bank/view', 'id' => $model->to_client_id], ['target' => '_blank'])
                    : null,
                'format' => 'raw',
            ],
            [
                'attribute' => 'paid_date',
                'value' => isset($model->paid_date) ? date('d-M-Y', strtotime($model->paid_date)) : null,
            ],     
            [
                'attribute' => 'summa_eur',
                'value' => isset($model->summa_eur) ? $model->summa_eur : null,
                'format' => ['decimal', 2],
            ],     
            'comment',
        ],
    ]) ?>

</div>