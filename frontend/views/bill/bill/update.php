<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\bill\Bill */

$this->title = Yii::t($model->tableName(), 'Update a '.$model->billDocTypeList[$model->doc_type]).': #' . $model->doc_number;
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->doc_number, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Edit');
?>
<div class="bill-update">

    <?= Html::pageHeader(Html::encode($this->title)); ?>

    <?= $this->render('_form', [
        'model' => $model,
        'abonentModel' => $abonentModel,
        'agreementModel' => $agreementModel,
        'firstClientModel' => $firstClientModel,
        'secondClientModel' => $secondClientModel,
        'filesModel' => $filesModel,
        'projectList' => $projectList,
        'agreementList' => $agreementList,
        'clientRoleList' => $clientRoleList,
        'valutaList' => $valutaList,
        'managerList' => $managerList,
        'billProductModel' => $billProductModel,
        'billProductProjectModel' => $billProductProjectModel,
        'billFirstPersonModel' => $billFirstPersonModel,
        'billSecondPersonModel' => $billSecondPersonModel,
        'productList' => $productList,
        'measureList' => $measureList,
        'languageList' => $languageList,
        'isAdmin' => $isAdmin,
        'hideDocType' => !empty($hideDocType),
        'isModal' => false,
    ]) ?>

</div>