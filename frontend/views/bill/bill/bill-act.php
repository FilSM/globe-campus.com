<?php

use common\models\client\Agreement;
use common\models\client\ClientContact;
?>
<body>
    <table border=0 cellspacing=0 cellpadding=0 width=720>
        <tbody>
            <tr>
                <td colspan="3" align="center">
                    <p>
                        <b>
                            CERTIFICATE OF ACCEPTANCE
                        </b>
                    </p>
                    <p>
                        <b>
                            to the COOPERATION CONTRACT No <?= strtoupper($agreementModel->number);?> dated <?= date('Y-m-d', strtotime($agreementModel->signing_date));?>
                        </b>
                    </p>
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td colspan="3">
                    <p>
                        <?= date('F jS, Y', strtotime($model->doc_date));?>
                    </p>
                </td>
            </tr>

            <tr>
                <td width="160">
                    <p>
                        &nbsp;
                    </p>
                    <p>
                        <b>
                            <?= $firstClientRoleModel->name ?? ''; ?>:
                        </b>
                    </p>
                </td>
                <td colspan="2">
                    &nbsp;                    
                </td>
            </tr>

            <tr>
                <td width="160">
                    <p>
                        Name:
                    </p>
                </td>
                <td colspan="2">
                    <p>
                        <?= $firstClientModel->name; ?>
                    </p>
                </td>
            </tr>

            <tr>
                <td width="160">
                    <p>
                        Registration number:
                    </p>
                </td>
                <td colspan="2">
                    <p>
                        <?= $firstClientModel->reg_number ?? ''; ?>
                    </p>
                </td>
            </tr>

            <tr>
                <td width="160">
                    <p>
                        Registered address:
                    </p>
                </td>
                <td colspan="2">
                    <p>
                        <?= $firstClientModel->fullLegalAddress; ?>
                    </p>
                </td>
            </tr>

            <?php foreach ($firstClientPersonModel as $key => $person): ?>
            <tr>
                <td width=160 valign=top>
                    <p>
                        <?= ($key == 0 ? 'Represented by:' : '')?>
                    </p>
                </td>
                <td width=275 valign=top>
                    <p>
                        <?= $person->clientContact->position->name ?? ''; ?>
                    </p>
                </td>
                <td width=285 valign=top>
                    <p>
                        <?= $person->clientContact->first_name.' '.$person->clientContact->last_name; ?>
                    </p>
                </td>
            </tr>

            <tr>
                <td width=160 valign=top>
                    <p>
                        &nbsp;
                    </p>
                </td>
                <td width=275 valign=top>
                    <p>
                        Acting on the basis of:
                    </p>
                </td>
                <td width=285 valign=top>
                    <p>
                        <?= (isset($person->clientContact->acting_on_basis) ? $person->clientContact->actingTypeList[$person->clientContact->acting_on_basis] : null)?>
                        <?php if($person->clientContact->acting_on_basis == ClientContact::ACTING_TYPE_ATTORNEY): ?>
                            dated 
                            <?= !empty($person->clientContact->term_from) ? date('d.m.Y', strtotime($person->clientContact->term_from)) : ''; ?>
                        <?php endif;?>
                    </p>
                </td>
            </tr>
            <?php endforeach;?>

            <tr>
                <td width="160">
                    <p>
                        from one side, and
                    </p>
                </td>
                <td width="275">
                    <p>
                        &nbsp;
                    </p>
                </td>
                <td width="285">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
    
    <table border=0 cellspacing=0 cellpadding=0 width=720>
        <tbody>
            <tr>
                <td width="160">
                    <p>
                        &nbsp;
                    </p>
                    <p>
                        <b>
                            <?= $secondClientRoleModel->name ?? ''; ?>:
                        </b>
                    </p>
                </td>
                <td colspan="2">
                    &nbsp;                    
                </td>
            </tr>

            <tr>
                <td width="160">
                    <p>
                        Name:
                    </p>
                </td>
                <td colspan="2">
                    <p>
                        <?= $secondClientModel->name; ?>
                    </p>
                </td>
            </tr>

            <tr>
                <td width="160">
                    <p>
                        Registration number:
                    </p>
                </td>
                <td colspan="2">
                    <p>
                        <?= $secondClientModel->reg_number ?? ''; ?>
                    </p>
                </td>
            </tr>

            <tr>
                <td width="160">
                    <p>Registered address:</p>
                </td>
                <td colspan="2">
                    <p>
                        <?= $secondClientModel->fullLegalAddress; ?>
                    </p>
                </td>
            </tr>

            <?php foreach ($secondClientPersonModel as $key => $person): ?>
            <tr>
                <td width=160 valign=top>
                    <p>
                        <?= ($key == 0 ? 'Represented by:' : '')?>
                    </p>
                </td>
                <td width=274 valign=top>
                    <p>
                        <?= $person->clientContact->position->name ?? ''; ?>
                    </p>
                </td>
                <td width=285 valign=top>
                    <p>
                        <?= $person->clientContact->first_name.' '.$person->clientContact->last_name; ?>
                    </p>
                </td>
            </tr>

            <tr>
                <td width=160 valign=top>
                    <p>
                        &nbsp;
                    </p>
                </td>
                <td width=274 valign=top>
                    <p>
                        Acting on the basis of:
                    </p>
                </td>
                <td width=285 valign=top>
                    <p>
                        <?= (isset($person->clientContact->acting_on_basis) ? $person->clientContact->actingTypeList[$person->clientContact->acting_on_basis] : null)?>
                        <?php if($person->clientContact->acting_on_basis == ClientContact::ACTING_TYPE_ATTORNEY): ?>
                            dated 
                            <?= !empty($person->clientContact->term_from) ? date('d.m.Y', strtotime($person->clientContact->term_from)) : ''; ?>
                        <?php endif;?>
                    </p>
                </td>
            </tr>
            <?php endforeach;?>

            <tr>
                <td width="160">
                    <p>
                        from the other side, 
                    </p>
                </td>
                <td width="275">
                    <p>
                        &nbsp;
                    </p>
                </td>
                <td width="285">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>
        </tbody>
    </table>

    <table border=0 cellspacing=0 cellpadding=0 width=720>
        <tbody>
            <tr>
                <td align="justify">
                    The Advisor and the Client hereinafter also separately referred to as the Party, and jointly as the Parties,
                </td>
            </tr>
            <tr>
                <td>
                    WHEREAS:
                </td>
            </tr>
            <tr>
                <td align="justify">
                    - The Parties on <?= date('Y-m-d', strtotime($agreementModel->signing_date));?> concluded a Cooperation Contract No. <?= strtoupper($agreementModel->number);?> (hereinafter - Contract);
                </td>
            </tr>
            <tr>
                <td align="justify">
                    - The Advisor has fulfilled its obligations under the Contract,
                </td>
            </tr>
            <tr>
                <td align="justify">
                    the Parties state the following:
                </td>
            </tr>
            <tr>
                <td align="justify">
                    1. The Parties hereby approve that the Advisor has fulfilled its obligations under the Contract and the Advisor is entitled to the remuneration prescribed in the Contract. The Client has no objections against the fulfilment of the Contract performed by the Advisor. 
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="justify">
                    2. This certificate of acceptance is prepared on one page in English language. All countersigned copies of this certificate of acceptance shall be deemed valid and executable. 
                </td>
            </tr>
        </tbody>
    </table>
    
    <p>
        &nbsp;
    </p>

    <table border=0 cellspacing=0 cellpadding=0>
        <tbody>
            <tr>
                <td width=350 align="center">
                    <?php foreach ($firstClientPersonModel as $key => $person): ?>
                        <hr>
                        <p>
                            <b>
                                <?= $person->clientContact->first_name.' '.$person->clientContact->last_name; ?>
                            </b>
                        </p>
                        <p>
                            <?= $person->clientContact->position->name ?? ''; ?>
                        </p>
                        <?php if(($key == 0) && (count($firstClientPersonModel) > 1)) :?>
                            <br/>
                        <?php endif;?>
                    <?php endforeach;?>
                </td>
                <td width=20>
                    <p>
                        &nbsp;
                    </p>
                </td>
                <td width=350 align="center">
                    <?php foreach ($secondClientPersonModel as $key => $person): ?>
                        <hr>
                        <p>
                            <b>
                                <?= $person->clientContact->first_name.' '.$person->clientContact->last_name; ?>
                            </b>
                        </p>
                        <p>
                            <?= $person->clientContact->position->name ?? ''; ?>
                        </p>
                        <?php if(($key == 0) && (count($secondClientPersonModel) > 1)) :?>
                            <br/>
                        <?php endif;?>
                    <?php endforeach;?>
                </td>
            </tr>
        </tbody>
    </table>
</body>