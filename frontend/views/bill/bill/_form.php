<?php
namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;
use kartik\icons\Icon;
use kartik\dialog\Dialog;

use common\components\FSMAccessHelper;
use common\widgets\EnumInput;
use common\models\bill\Bill;

/* @var $this yii\web\View */
/* @var $model common\models\bill\Bill */
/* @var $form yii\widgets\ActiveForm */

Icon::map($this);
$isModal = !empty($isModal);
$isAdmin = !empty($isAdmin);
?>
  
<?= Dialog::widget([
    'libName' => 'krajeeDialogWarning',
    'options' => [
        'draggable' => true, 
        'type' => Dialog::TYPE_WARNING,
        'title' => Yii::t('common', 'Confirmation'),
    ],
]);?> 

<div class="bill-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => $model->tableName().'-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>

    <?= Html::activeHiddenInput($model, 'id', ['id' => 'bill-id', 'value' => $model->id]); ?>
    <?= Html::activeHiddenInput($model, 'reserved_id', ['id' => 'bill-reserved-id', 'value' => $model->reserved_id]); ?>
    <?= Html::activeHiddenInput($model, 'bill_template_id'); ?>
    
    <?php if(in_array($model->doc_type, [
                Bill::BILL_DOC_TYPE_CRBILL, 
                Bill::BILL_DOC_TYPE_DEBT, 
                Bill::BILL_DOC_TYPE_CESSION
            ]) || 
            !empty($hideDocType)) :
        echo Html::activeHiddenInput($model, 'doc_type');
    else: 
        echo $form->field($model, 'doc_type')->widget(EnumInput::class, [
                'type' => EnumInput::TYPE_RADIOBUTTON,
                'data' => isset($availableDocTypeList) ? $availableDocTypeList : null,
                'options' => [
                    'translate' => $model->billDocTypeList,
                ],
                'without' => [Bill::BILL_DOC_TYPE_CRBILL],
            ]); 
    endif; ?>

    <?= $form->field($abonentModel, 'project_id', [
        'template' => '<div class="col-md-2"></div><div class="col-md-10">{input}</div>{error}{hint}',
        'options' => [
            'id' => 'project-static-text',
            'class' => 'form-group static-input',
            'style' => 'display: '.((isset($abonentModel->project_id) && ($abonentModel->project->vat_taxable == 0)) ? 'block' : 'none').'; font-size: x-large; color: red; text-shadow: 1px 1px 2px grey;',
        ],
        'staticValue' => ((isset($abonentModel->project_id) && ($abonentModel->project->vat_taxable == 0)) ? YII::t('javascript', 'This project is non taxable and you will not have the opportunity to charge VAT') : ''),
    ])->staticInput()->label(false); ?>

    <?= $form->field($model, 'agreement_id')->widget(Select2::class, [
        'data' => $agreementList,
        'options' => [
            'id' => 'agreement-id',
            'placeholder' => '...',
        ],
        'pluginOptions' => [
            'allowClear' => !$model->isAttributeRequired('agreement_id'),
            'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
        ],
        'addon' => in_array($model->doc_type, [
                Bill::BILL_DOC_TYPE_CRBILL, 
                Bill::BILL_DOC_TYPE_DEBT, 
                Bill::BILL_DOC_TYPE_CESSION
            ]) || !FSMAccessHelper::can('createAgreement') ? 
            null : 
            [
            'prepend' => [
                'content' => 
                $model->getModalButton([
                    'formId' => $form->id,
                    'controller' => 'agreement',
                    'addNewBtnTitle' => Yii::t('bill', 'Add new agreement'),
                    /*
                    'parent' => (!empty($abonentModel->project_id) ? 
                        [
                            'field_name' => 'project_id',
                            'id' => $abonentModel->project_id
                        ] :
                        null),
                     * 
                     */
                ]).
                Html::a(Html::icon('eye-open'), 
                    Url::to(['/agreement/view', 'id' => $model->agreement_id]),
                    [
                        'id' => 'btn-view-agreement', 
                        'class'=>'btn btn-info',
                        'target' => '_blank',
                        'style' => empty($model->agreement_id) ? 'display: none;' : '',
                        'title' => Yii::t('client', 'View agreement data'),
                    ]
                ),
                'asButton' => true, 
            ],
        ],  
        'disabled' => in_array($model->doc_type, [
            Bill::BILL_DOC_TYPE_CRBILL, 
            Bill::BILL_DOC_TYPE_DEBT, 
            Bill::BILL_DOC_TYPE_CESSION 
        ]),
    ]);?>

    <?= $form->field($model, 'e_signing')->widget(SwitchInput::class, [
        'options' => [
            'id' => 'e-signing',
        ],        
        'pluginOptions' => [
            'onText' => Yii::t('common', 'Yes'),
            'offText' => Yii::t('common', 'No'),
            'handleWidth' => (!$isModal ? 'auto' : '45')
        ],
        /*
        'pluginEvents' => [
            "switchChange.bootstrapSwitch" => "function() { console.log('switchChange'); }",
        ],
         * 
         */
    ]); ?>
        
    <fieldset id="client-data" style="margin-bottom: 10px;">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-10">
                <div class="col-md-6 double-line-top double-line-bottom first-client-data" style="background: aliceblue;">
                    <div class="col-md-12" style="padding: 0;">
                        <div class="field-group-title"><h3 id="first-party-title"><?= Yii::t('bill', 'First party data'); ?></h3></div>
                    </div>

                    <div class="col-md-12" style="padding: 0;">
                        <?= Html::activeHiddenInput($firstClientModel, 'id', ['id' => 'first-client-id']); ?>

                        <div class="form-group static-input">
                            <?= Html::label(Yii::t('bill', 'Party role'), 'first-client-address', ['class' => 'control-label col-md-3']); ?>
                            <div class="col-md-9">
                                <?= Html::textInput('first_client_role', 
                                    (!empty($agreementModel->first_client_role_id) ? $agreementModel->firstClientRole->name : null), [
                                    'id' => 'first-client-role', 
                                    'class' => 'form-control', 
                                    'disabled' => true,
                                ]); ?>
                            </div>
                        </div>                        
                        
                        <?= $form->field($firstClientModel, 'name',[
                            'options' => [
                                'class' => 'form-group static-input',
                            ],
                            'horizontalCssClasses' => [
                                'label' => 'col-md-3',
                                'wrapper' => 'col-md-9',
                            ], 
                            'template' => '{label} <div class="col-md-9">{input}</div>',
                            'addon' => !FSMAccessHelper::can('viewClient') ? 
                                null : [
                                'prepend' => [
                                    'content' => 
                                        Html::a(Html::icon('eye-open'), 
                                            Url::to(['/client/view', 'id' => $firstClientModel->id]),
                                            [
                                                'id' => 'btn-view-first-client', 
                                                'class'=>'btn btn-info',
                                                'target' => '_blank',
                                                'title' => Yii::t('client', 'View client data'),
                                                'disabled' => empty($firstClientModel->id),
                                            ]
                                        ),
                                    'asButton' => true, 
                                ],
                            ],
                        ])->textInput([
                            'id' => 'first-client-name', 
                            'disabled' => true,
                            'style' => 'font-weight: bold; font-size: x-large;',
                        ])->label(Yii::t('client', 'Full name')); 
                        ?>    
                        
                        <?= $form->field($firstClientModel, 'reg_number',[
                            'options' => [
                                'class' => 'form-group static-input',
                            ],
                            'horizontalCssClasses' => [
                                'label' => 'col-md-3',
                                'wrapper' => 'col-md-9',
                            ], 
                            'template' => '{label} <div class="col-md-9">{input}</div>',
                        ])->textInput([
                            'id' => 'first-client-reg',
                            'disabled' => true,
                        ])->label(Yii::t('client', 'Reg.number')); 
                        ?>
                        
                        <?= $form->field($firstClientModel, 'vat_number',[
                            'options' => [
                                'class' => 'form-group static-input',
                            ],
                            'horizontalCssClasses' => [
                                'label' => 'col-md-3',
                                'wrapper' => 'col-md-9',
                            ], 
                            'template' => '{label} <div class="col-md-9">{input}</div>',
                        ])->textInput([
                            'id' => 'first-client-vat',
                            'disabled' => true,
                        ])->label(Yii::t('client', 'VAT number')); 
                        ?>

                        <?= $form->field($firstClientModel, 'legal_address',[
                            'options' => [
                                'class' => 'form-group static-input',
                            ],
                            'horizontalCssClasses' => [
                                'label' => 'col-md-3',
                                'wrapper' => 'col-md-9',
                            ], 
                            'template' => '{label} <div class="col-md-9">{input}</div>',
                        ])->textInput([
                            'id' => 'first-client-address',
                            'value' => $firstClientModel->fullLegalAddress,
                            'disabled' => true,
                        ])->label(Yii::t('client', 'Legal address')); 
                        ?>

                        <?= $form->field($model, 'first_client_bank_id',[
                            'horizontalCssClasses' => [
                                'label' => 'col-md-3',
                                //'offset' => 'col-sm-offset-4',
                                'wrapper' => 'col-md-9',
                                //'error' => '',
                                //'hint' => '',
                            ],                            
                        ])->widget(DepDrop::class, [
                            'type' => DepDrop::TYPE_SELECT2,
                            'data' => empty($model->first_client_bank_id) ? null : [$model->first_client_bank_id => $model->firstClientBank->bank->name . ' | ' . $model->firstClientBank->account . (!empty($model->firstClientBank->name) ? ' ( '.$model->firstClientBank->name . ' )' : '')],
                            'select2Options' => [
                                'pluginOptions' => [
                                    'allowClear' => !$model->isAttributeRequired('first_client_bank_id'),
                                    'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
                                ],
                            ],
                            'pluginOptions' => [
                                'depends' => ['first-client-id'],
                                'initDepends' => ['first-client-id'],
                                'initialize' => !empty($model->id),
                                'url' => Url::to(['/client/ajax-get-client-account-list', 'selected' => $model->first_client_bank_id]),
                                'placeholder' => '...',
                            ],
                            'pluginEvents' => [
                                "depdrop:afterChange" => "function() {
                                    var form = $('#{$model->tableName()}' + '-form');
                                    checkRequiredInputs(form);
                                }",
                            ],
                         ])->label(Yii::t('bill', 'Bank account')); ?>
                        
                        <?= $this->render('_person_table_edit', [
                            'form' => $form,
                            'model' => $billFirstPersonModel,
                            'invoice' => $model,
                            'isModal' => $isModal,
                            'depends' => 'first-client-id',
                            'panel_person_id' => 'panel-first-person-data',
                            'docDate' => $model->doc_date ?? null,
                        ]) ?>

                    </div>    
                </div>

                <div class="col-md-6 double-line-top double-line-bottom second-client-data" style="background: antiquewhite;">
                    <div class="col-md-12" style="padding: 0;">
                        <div class="field-group-title"><h3 id="second-party-title"><?= Yii::t('bill', 'Second party data'); ?></h3></div>
                    </div>

                    <div class="col-md-12" style="padding: 0;">
                        <?= Html::activeHiddenInput($secondClientModel, 'id', ['id' => 'second-client-id']); ?>
                        
                        <div class="form-group static-input">
                            <?= Html::label(Yii::t('bill', 'Party role'), 'second-client-role', ['class' => 'control-label col-md-3']); ?>
                            <div class="col-md-9">
                                <?= Html::textInput('second_client_role', 
                                    (!empty($agreementModel->second_client_role_id) ? $agreementModel->secondClientRole->name : null), [
                                    'id' => 'second-client-role', 
                                    'class' => 'form-control', 
                                    'disabled' => true,
                                ]); ?>
                            </div>
                        </div>                        

                        <?= $form->field($secondClientModel, 'name',[
                            'options' => [
                                'class' => 'form-group static-input',
                            ],
                            'horizontalCssClasses' => [
                                'label' => 'col-md-3',
                                'wrapper' => 'col-md-9',
                            ],  
                            'template' => '{label} <div class="col-md-9">{input}</div>',
                            'addon' => !FSMAccessHelper::can('viewClient') ? 
                                null : [
                                'prepend' => [
                                    'content' => 
                                    Html::a(Html::icon('eye-open'), 
                                        Url::to(['/client/view', 'id' => $secondClientModel->id]),
                                        [
                                            'id' => 'btn-view-second-client', 
                                            'class'=>'btn btn-info',
                                            'target' => '_blank',
                                            'title' => Yii::t('client', 'View client data'),
                                            'disabled' => empty($secondClientModel->id),
                                        ]
                                    ),
                                    'asButton' => true, 
                                ],
                            ],
                        ])->textInput([
                            'id' => 'second-client-name', 
                            //'value' => (!empty($secondClientModel->id) ? $secondClientModel->name : null),
                            'disabled' => true,
                            'style' => 'font-weight: bold; font-size: x-large;',
                        ])->label(Yii::t('client', 'Full name')); 
                        ?>
                        
                        <?= $form->field($secondClientModel, 'reg_number',[
                            'options' => [
                                'class' => 'form-group static-input',
                            ],
                            'horizontalCssClasses' => [
                                'label' => 'col-md-3',
                                'wrapper' => 'col-md-9',
                            ], 
                            'template' => '{label} <div class="col-md-9">{input}</div>',
                        ])->textInput([
                            'id' => 'second-client-reg',
                            'disabled' => true,
                        ])->label(Yii::t('client', 'Reg.number')); 
                        ?>
                        
                        <?= $form->field($secondClientModel, 'vat_number',[
                            'options' => [
                                'class' => 'form-group static-input',
                            ],
                            'horizontalCssClasses' => [
                                'label' => 'col-md-3',
                                'wrapper' => 'col-md-9',
                            ], 
                            'template' => '{label} <div class="col-md-9">{input}</div>',
                        ])->textInput([
                            'id' => 'second-client-vat',
                            'disabled' => true,
                        ])->label(Yii::t('client', 'VAT number')); 
                        ?>

                        <?= $form->field($secondClientModel, 'legal_address',[
                            'options' => [
                                'class' => 'form-group static-input',
                            ],
                            'horizontalCssClasses' => [
                                'label' => 'col-md-3',
                                'wrapper' => 'col-md-9',
                            ], 
                            'template' => '{label} <div class="col-md-9">{input}</div>',
                        ])->textInput([
                            'id' => 'second-client-address',
                            'value' => $secondClientModel->fullLegalAddress,
                            'disabled' => true,
                        ])->label(Yii::t('client', 'Legal address')); 
                        ?>

                        <?= $form->field($model, 'second_client_bank_id',[
                            'horizontalCssClasses' => [
                                'label' => 'col-md-3',
                                //'offset' => 'col-sm-offset-4',
                                'wrapper' => 'col-md-9',
                                //'error' => '',
                                //'hint' => '',
                            ],                            
                        ])->widget(DepDrop::class, [
                            'type' => DepDrop::TYPE_SELECT2,
                            'data' => empty($model->second_client_bank_id) ? null : [$model->second_client_bank_id => $model->secondClientBank->bank->name . ' | ' . $model->secondClientBank->account . (!empty($model->secondClientBank->name) ? ' ( '.$model->secondClientBank->name . ' )' : '')],
                            'select2Options' => [
                                'pluginOptions' => [
                                    'allowClear' => !$model->isAttributeRequired('second_client_bank_id'),
                                    'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
                                ],
                            ],
                            'pluginOptions' => [
                                'depends' => ['second-client-id'],
                                'initDepends' => ['second-client-id'],
                                'initialize' => !empty($model->id),            
                                'url' => Url::to(['/client/ajax-get-client-account-list', 'selected' => $model->second_client_bank_id]),
                                'placeholder' => '...',
                            ],
                            'pluginEvents' => [
                                "depdrop:afterChange" => "function() {
                                    var form = $('#{$model->tableName()}' + '-form');
                                    checkRequiredInputs(form);
                                }",
                            ],
                         ])->label(Yii::t('bill', 'Bank account')); ?>
                        
                        <?= $this->render('_person_table_edit', [
                            'form' => $form,
                            'model' => $billSecondPersonModel,
                            'invoice' => $model,
                            'isModal' => $isModal,
                            'depends' => 'second-client-id',
                            'panel_person_id' => 'panel-second-person-data',
                            'docDate' => $model->doc_date ?? null,
                        ]) ?>
                    </div>    
                </div>            
            </div>
        </div>
    </fieldset>
    
    <?= $form->field($model, 'doc_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'doc_date')->widget(DatePicker::class, [
            'options' => [
                'class' => 'rate-changer-date',
                'data-currency_input_id' => 'valuta-id',
                'data-rate_input_id' => 'rate',
                'data-deferment_payment' => Bill::BILL_DEFAULT_PAYMENT_DAYS,
            ],
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
            'pluginEvents' => [
                "changeDate" => "function() {
                    var form = $('#{$model->tableName()}' + '-form');
                    var \$this = form.find('#bill-doc_date');
                    var defermentPayment = \$this.data('deferment_payment');
                    var docDate = \$this.val();
                    var docDateInt = strtotime(docDate);
                    //var docDateInt = Date.parse( docDate.replace(/-/g, ' ') ) / 1000;
                    var dueDateInt = strtotime('+' + defermentPayment + ' days', docDateInt); 
                    var dueDate = date('d-m-Y', dueDateInt);
                    form.find('#bill-pay_date').val(dueDate);            
                }",
            ],        
        ]); 
    ?>
    
    <?= $form->field($model, 'pay_date')->widget(DatePicker::class, [
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
        ]); 
    ?>
    
    <?= $form->field($model, 'services_period_from', [
                //'template' => '{label} <div class="input-group col-md-9">{input}{error}{hint}</div>',
            ])->widget(DatePicker::class, [
            'type' => DatePicker::TYPE_RANGE,
            'attribute' => 'services_period_from',
            'attribute2' => 'services_period_till',
            'options' => [
                'placeholder' => Yii::t('common', 'Start date'),
            ],
            'options2' => [
                'placeholder' => Yii::t('common', 'End date'),
            ],
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
        ])->label(Yii::t('bill', 'Service period')); 
    ?>

    <?= $form->field($abonentModel, 'project_id')->widget(Select2::class, [
        'data' => $projectList,
        'options' => [
            'id' => 'project-id',
            'placeholder' => '...',
        ],
        'pluginOptions' => [
            'allowClear' => !$abonentModel->isAttributeRequired('project_id'),
            'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
        ],
        'addon' => in_array($model->doc_type, [
                Bill::BILL_DOC_TYPE_CRBILL, 
                Bill::BILL_DOC_TYPE_DEBT, 
                Bill::BILL_DOC_TYPE_CESSION
            ]) || !FSMAccessHelper::can('createProject') ? 
            null : 
            [
            'prepend' => $model::getModalButtonContent([
                'formId' => $form->id,
                'controller' => 'project',
                'addNewBtnTitle' => Yii::t('bill', 'Add new project'),
            ]),
        ],     
        'disabled' => in_array($model->doc_type, [
            Bill::BILL_DOC_TYPE_CRBILL, 
            Bill::BILL_DOC_TYPE_DEBT, 
            Bill::BILL_DOC_TYPE_CESSION
        ]),
    ])->hint(Yii::t('bill', 'Empty project fields in the Invoice products table will be replaced with this value'));
    ?>

    <?= $form->field($model, 'according_contract')->widget(SwitchInput::class, [
        'options' => [
            'id' => 'according-contract',
        ],        
        'pluginOptions' => [
            'onText' => Yii::t('common', 'Yes'),
            'offText' => Yii::t('common', 'No'),
            'handleWidth' => (!$isModal ? 'auto' : '45')
        ],
        /*
        'pluginEvents' => [
            "switchChange.bootstrapSwitch" => "function() { console.log('switchChange'); }",
        ],
         * 
         */
    ]); ?>
    
    <?php
        echo Html::beginTag('div', [
            'id' => 'product-data-container',
            'class' => 'form-group',
        ]);
        echo Html::beginTag('div', [
            'class' => 'col-md-2',
        ]);
        echo Html::endTag('div');
        echo Html::beginTag('div', [
            'class' => 'col-md-10',
        ]);
    ?>

    <?= $this->render('_product_table_edit', [
        'form' => $form,
        'model' => $billProductModel,
        'billProductProjectModel' => $billProductProjectModel,
        'invoiceModel' => $model,
        'productList' => $productList,
        'measureList' => $measureList,
        'projectList' => $projectList,
        'isModal' => $isModal,
        'useProjects' => 1,
    ]) ?>

    <?php
        echo Html::endTag('div');
        echo Html::endTag('div');           
    ?> 

    <?php
        echo Html::beginTag('div', [
            'id' => 'summa-data-container',
        ]);
        
        echo $form->field($model, 'summa')/*->textInput([
            'readonly' => true,
        ])*/->widget(MaskedInput::class, [
            'mask' => '[-]9{1,10}[.9{1,2}]',
            'options' => [
                'class' => 'form-control number-field',
                'readonly' => true,
            ],
        ]);
        
        echo $form->field($model, 'vat')/*->textInput([
            'readonly' => true,
        ])*/->widget(MaskedInput::class, [
            'mask' => '[-]9{1,10}[.9{1,2}]',
            'options' => [
                'class' => 'form-control number-field',
                'readonly' => true,
            ],
        ]);
        
        
        if(!$model->according_contract) :
            //echo Html::activeHiddenInput($model, 'summa');
            //echo Html::activeHiddenInput($model, 'vat');
        else :
            
        endif;
        echo Html::endTag('div');           
    ?>     
    
    <?= $form->field($model, 'total', [
        'addon' => [
            'append' => [
                ['content' => 
                    MaskedInput::widget([
                        'model' => $model,
                        'attribute' => 'rate',
                        //'mask' => '[-]9{1,10}[.9{1,4}]',
                        'options' => [
                            'id' => 'rate', 
                            'placeholder' => 'Rate',
                            'class' => 'form-control number-field',
                            'style' => 'text-align: right; min-width: 90px;'
                        ],
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'rightAlign' => false,
                            'digits' => 4,
                            'allowMinus' => true,
                        ],                                    
                    ]),
                    'asButton' => true,
                ],
                ['content' => 
                    Select2::widget([
                        'model' => $model,
                        'attribute' => 'valuta_id',
                        'data' => $valutaList, 
                        'options' => [
                            'id' => 'valuta-id', 
                            'class' => 'rate-changer-currency',
                            'placeholder' => '...',
                            'data-date_input_id' => 'bill-doc_date',
                            'data-rate_input_id' => 'rate',
                        ],                        
                        'pluginOptions' => [
                            'allowClear' => !$model->isAttributeRequired('valuta_id'),
                            'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
                        ],            
                        'size' => 'control-width-90',
                    ]),
                    'asButton' => true,
                ],
            ],
        ],
        //'labelOptions' => ['class' => 'col-md-3'],
    ])->textInput([
        'readonly' => true,
    ])->label($model->getAttributeLabel('total').' / '.$model->getAttributeLabel('rate').' / '.$model->getAttributeLabel('valuta_id'));
    ?>
    
    <div id="waybill-data">
        <?= $form->field($model, 'loading_address')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'unloading_address')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'carrier')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'transport')->textInput(['maxlength' => true]) ?>
    </div>

    <?= $form->field($model, 'manager_id')->widget(Select2::class, [
        'data' => $managerList,
        'options' => [
            'placeholder' => '...',
        ],   
        'pluginOptions' => [
            'allowClear' => !$model->isAttributeRequired('manager_id'),
            'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
        ],        
    ]); ?>
    
    <?= $form->field($model, 'language_id')->widget(Select2::class, [
        'data' => $languageList,
        'options' => [
            'placeholder' => '...',
        ],
        'pluginOptions' => [
            'allowClear' => boolval(!$model->isAttributeRequired('language_id')),
            'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
        ],            
    ]); ?>

    <?php if(!empty($model->id) && !empty($isAdmin)) :
        echo $form->field($model, 'status')->widget(EnumInput::class, [
                'type' => EnumInput::TYPE_RADIOBUTTON,
                'options' => [
                    'translate' => $model->billStatusList,
                ],
                'without' => (!empty(Yii::$app->session->get('user_current_abonent_workflow')) ? [Bill::BILL_STATUS_NEW, Bill::BILL_STATUS_READY] : []),
            ]); 

        echo $form->field($model, 'pay_status')->widget(EnumInput::class, [
                'type' => EnumInput::TYPE_RADIOBUTTON,
                'options' => [
                    'translate' => $model->billPayStatusList,
                ],
                'without' => [Bill::BILL_PAY_STATUS_DELAYED],
            ]); 

        echo $form->field($model, 'mail_status')->widget(EnumInput::class, [
                'type' => EnumInput::TYPE_RADIOBUTTON,
                'options' => [
                    'translate' => $model->billMailStatusList,
                ],
            ]);

        echo $form->field($model, 'transfer_status')->widget(EnumInput::class, [
                'type' => EnumInput::TYPE_RADIOBUTTON,
                'options' => [
                    'translate' => $model->billTransferStatusList,
                ],
            ]);
        
        echo $form->field($model, 'delayed')->widget(SwitchInput::class, [
            'pluginOptions' => [
                'onText' => Yii::t('common', 'Yes'),
                'offText' => Yii::t('common', 'No'),
                'handleWidth' => (!$isModal ? 'auto' : '45')
            ],
        ]);
    else: ?>
        <?= Html::activeHiddenInput($model, 'status'); ?>
        <?= Html::activeHiddenInput($model, 'pay_status'); ?>
        <?= Html::activeHiddenInput($model, 'mail_status'); ?>
        <?= Html::activeHiddenInput($model, 'transfer_status'); ?>
        <?= Html::activeHiddenInput($model, 'delayed'); ?>
    <?php endif; ?>

    <?= $form->field($model, 'has_act')->widget(SwitchInput::class, [
        'pluginOptions' => [
            'onText' => Yii::t('common', 'Yes'),
            'offText' => Yii::t('common', 'No'),
            'handleWidth' => (!$isModal ? 'auto' : '45')
        ],
        'pluginEvents' => [
            "switchChange.bootstrapSwitch" => "function() { console.log('\"has_act\" switchChange'); }",
        ],
    ]); ?>

    <?= $form->field($model, 'not_expenses')->widget(SwitchInput::class, [
        'pluginOptions' => [
            'onText' => Yii::t('common', 'Yes'),
            'offText' => Yii::t('common', 'No'),
            'handleWidth' => (!$isModal ? 'auto' : '45')
        ],
    ]); ?>
    
    <?= $form->field($model, 'justification')->textInput(['maxlength' => true]) ?>        
    
    <?= $form->field($model, 'place_service')->textInput(['maxlength' => true]) ?>        
    
    <?= $form->field($model, 'comment_special')->textarea(['rows' => 3]) ?>
    
    <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>
        
    <?php
    if(!empty($filesModel)){
        $preview = $previewConfig = [];
        foreach ($filesModel as $key => $file) {
            if(empty($file->id)){
                continue;
            }
            $preview[] = $file->uploadedFileUrl;
            $ext = pathinfo($file->filename, PATHINFO_EXTENSION);
            switch ($ext) {
                default:
                case 'pdf':
                    $type = 'pdf';
                    break;
                case 'doc':
                case 'docx':
                case 'edoc':
                case 'bdoc':
                case 'xls':
                case 'xlsx':
                case 'asice':
                case 'sce':
                    $type = 'gdocs';
                    break;
                case 'zip':
                    $type = 'object';
                    break;
            }
            $previewConfig[] = [
                'type' => $type,
                'size' => $file->filesize, 
                'caption' => $file->filename, 
                'url' => Url::to(['/bill/attachment-delete', 'deleted_id' => $file->id]), 
                'key' => 101 + $key,
                'downloadUrl' => $file->uploadedFileUrl,
            ];
        }
    
        echo $form->field($filesModel[0], 'file[]')->widget(FileInput::class, [
            'language' =>  strtolower(substr(Yii::$app->language, 0, 2)),
            'sortThumbs' => true,
            'options' => [
                'id' => 'bill-files',
                'multiple' => true,
            ],
            'pluginOptions' => [
                'allowedFileExtensions' => ['png', 'jpg', 'jpeg', 'pdf', 'doc', 'docx', 'edoc', 'bdoc', 'asice', 'xls', 'xlsx', 'sce', 'zip'],
                'uploadAsync' => false,
                'maxFileSize' => 20000,
                'showRemove' => false,
                'showUpload' => false,
                'overwriteInitial' => false,
                
                'initialPreview' => $preview,
                'initialPreviewAsData' => true,
                'initialPreviewFileType' => 'pdf',
                'initialPreviewConfig' => $previewConfig,
                'initialPreviewShowDelete' => true,            
                
                'preferIconicPreview' => true, // this will force thumbnails to display icons for following file extensions
                'previewFileIcon' => Icon::show('file'),
                'allowedPreviewTypes' => null, // set to empty, null or false to disable preview for all types  
                'previewFileIconSettings' => [ // configure your icon file extensions
                    'png' => Icon::show('file-image', ['class' => 'text-info']),
                    'jpg' => Icon::show('file-image', ['class' => 'text-info']),
                    'jpeg' => Icon::show('file-image', ['class' => 'text-info']),
                    'pdf' => Icon::show('file-pdf', ['class' => 'text-danger']),
                    'doc' => Icon::show('file-word', ['class' => 'text-primary']),
                    'docx' => Icon::show('file-word', ['class' => 'text-primary']),
                    'edoc' => Icon::show('file-word', ['class' => 'text-primary']),
                    'bdoc' => Icon::show('file-word', ['class' => 'text-primary']),
                    'xls' => Icon::show('file-excel', ['class' => 'text-success']),
                    'xlsx' => Icon::show('file-excel', ['class' => 'text-success']),
                    'zip' => Icon::show('file-archive', ['class' => 'text-muted']),
                ],                
            ]
        ])->label(Yii::t('files', 'Attachment'))->hint('Allowed file extensions: .png .jpg .jpeg .pdf .doc .docx .edoc .bdoc .xls .xlsx .asice .sce .zip'); 
    }
    ?>
    
    <?php if(!empty($model->id) && FSMAccessHelper::can('blockBill')){
        echo $form->field($model, 'blocked')->widget(SwitchInput::class, [
            'pluginOptions' => [
                'onText' => Yii::t('common', 'Yes'),
                'offText' => Yii::t('common', 'No'),
                'handleWidth' => (!$isModal ? 'auto' : '45')
            ],
        ]);
    } else {
        echo Html::activeHiddenInput($model, 'blocked');
    }?>
    
    <?php if(!empty($model->id) && !empty($isAdmin)){
        echo $form->field($model, 'deleted')->widget(SwitchInput::class, [
            'pluginOptions' => [
                'onText' => Yii::t('common', 'Yes'),
                'offText' => Yii::t('common', 'No'),
                'handleWidth' => (!$isModal ? 'auto' : '45')
            ],
        ]);
    } ?>

    <div class="form-group">
        <div class="col-md-offset-9 col-md-3" style="text-align: right;">
            <?= $model->SubmitButton; ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>