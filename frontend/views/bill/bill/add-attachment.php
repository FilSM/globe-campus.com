<?php
namespace common\models;

use Yii;
use yii\widgets\Pjax;
use yii\helpers\Url;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $model common\models\bill\Bill */
/* @var $form yii\widgets\ActiveForm */

Icon::map($this);
$isModal = !empty($isModal);
$isAdmin = !empty($isAdmin);
?>

<div class="bill-form">
    <?php if($isModal) : Pjax::begin(Yii::$app->params['PjaxModalOptions']); endif; ?>

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => $model->tableName().'-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'options' => [
            'data-pjax' => $isModal,    
        ],         
    ]); ?>

    <?= Html::activeHiddenInput($model, 'id', ['id' => 'bill-id', 'value' => $model->id]); ?>
        
    <?php
    if(!empty($filesModel)){
        $preview = $previewConfig = [];
        foreach ($filesModel as $key => $file) {
            if(empty($file->id)){
                continue;
            }
            $preview[] = $file->uploadedFileUrl;
            $ext = pathinfo($file->filename, PATHINFO_EXTENSION);
            switch ($ext) {
                default:
                case 'pdf':
                    $type = 'pdf';
                    break;
                case 'doc':
                case 'docx':
                case 'xls':
                case 'xlsx':
                case 'ppt':
                case 'pptx':
                case 'asice':
                case 'sce':
                    $type = 'gdocs';
                    break;
                case 'zip':
                    $type = 'object';
                    break;
            }
            $previewConfig[] = [
                'type' => $type,
                'size' => $file->filesize, 
                'caption' => $file->filename, 
                'url' => Url::to(['/bill/attachment-delete', 'deleted_id' => $file->id]), 
                'key' => 101 + $key,
                'downloadUrl' => $file->uploadedFileUrl,
            ];
        }
    
        echo $form->field($filesModel[0], 'file[]')->widget(FileInput::class, [
            'language' =>  strtolower(substr(Yii::$app->language, 0, 2)),
            'sortThumbs' => true,
            'options' => [
                'id' => 'bill-files',
                'multiple' => true,
            ],
            'pluginOptions' => [
                'allowedFileExtensions' => ['png', 'jpg', 'jpeg', 'pdf', 'doc', 'docx', 'edoc', 'bdoc', 'asice', 'xls', 'xlsx', 'sce', 'zip'],
                'uploadAsync' => false,
                'maxFileSize' => 20000,
                'showRemove' => false,
                'showUpload' => false,
                'overwriteInitial' => false,
                
                'initialPreview' => $preview,
                'initialPreviewAsData' => true,
                'initialPreviewFileType' => 'pdf',
                'initialPreviewConfig' => $previewConfig,
                'initialPreviewShowDelete' => true,            
                
                'preferIconicPreview' => true, // this will force thumbnails to display icons for following file extensions
                'previewFileIcon' => Icon::show('file'),
                'allowedPreviewTypes' => null, // set to empty, null or false to disable preview for all types  
                'previewFileIconSettings' => [ // configure your icon file extensions
                    'pdf' => Icon::show('file-pdf', ['class' => 'text-danger']),
                    'doc' => Icon::show('file-word', ['class' => 'text-primary']),
                    'docx' => Icon::show('file-word', ['class' => 'text-primary']),
                    'xls' => Icon::show('file-excel', ['class' => 'text-success']),
                    'xlsx' => Icon::show('file-excel', ['class' => 'text-success']),
                    'ppt' => Icon::show('file-powerpoint', ['class' => 'text-danger']),
                    'pptx' => Icon::show('file-powerpoint', ['class' => 'text-danger']),
                    'png' => Icon::show('file-image', ['class' => 'text-info']),
                    'jpg' => Icon::show('file-image', ['class' => 'text-info']),
                    'jpeg' => Icon::show('file-image', ['class' => 'text-info']),
                    'zip' => Icon::show('file-archive', ['class' => 'text-muted']),
                ],                
            ]
        ])->label(Yii::t('files', 'Attachment'))->hint('Allowed file extensions: .png .jpg .jpeg .pdf .doc .docx .xls .xlsx .asice .sce .zip'); 
    }
    ?>
    
    <div class="form-group">
        <div class="col-md-offset-9 col-md-3" style="text-align: right;">
            <?= $model->saveButton; ?>
            <?= $model->cancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <?php if($isModal) : Pjax::end(); endif; ?>

</div>