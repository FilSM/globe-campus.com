<?php
namespace common\models;

use Yii;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\bill\BillConfirm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bill-confirm-form">
    <?php if($isModal) : Pjax::begin(Yii::$app->params['PjaxModalOptions']); endif; ?>

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => $model->tableName().'-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'formConfig' => [
            'labelSpan' => 3,
        ],        
        'options' => [
            'data-pjax' => $isModal,
        ],          
    ]); ?>

    <?= $form->field($model, 'doc_number', [
        'options' => [
            'id' => 'bill-static-text',
            'class' => 'form-group static-input',
        ],
        'staticValue' => (isset($model->id) ? $model->doc_number : ''),
    ])->staticInput(['class' => 'form-control', 'disabled' => true]); ?>

    <?= $form->field($historyModel, 'create_time', [
        'options' => [
            'id' => 'create-time-static-text',
            'class' => 'form-group static-input',
        ],
        'staticValue' => (isset($historyModel->create_time) ? date('d-M-Y H:i:s', strtotime($historyModel->create_time)) : date('d-M-Y H:i:s')),
    ])->staticInput(['class' => 'form-control', 'disabled' => true]); ?>

    <?= $form->field($historyModel, 'create_user_id', [
        'options' => [
            'id' => 'create-user-static-text',
            'class' => 'form-group static-input',
        ],
        'staticValue' => (isset($historyModel->create_user_id) ? $historyModel->createUserProfile->name : Yii::$app->user->identity->profile->name),
    ])->staticInput(['class' => 'form-control', 'disabled' => true]); ?>
    
    <?= $form->field($model, 'summa', [
        'options' => [
            'id' => 'summa-static-text',
            'class' => 'form-group static-input',
        ],
        'staticValue' => (isset($model->summa) ? $model->summa : ''),
    ])->staticInput(['class' => 'form-control', 'disabled' => true])->label(Yii::t('bill', 'Last payment summa')); ?>
    
    <?= $form->field($model, 'paid_date')->widget(DatePicker::class, [
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
        ]); 
    ?>    
    
    <div class="form-group <?php if($isModal) : echo 'modal-button-group'; endif; ?>">
        <div class="col-md-offset-3 col-md-9" style="text-align: right;">
            <?= $model->SaveButton; ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <?php if($isModal) : Pjax::end(); endif; ?>

</div>