<?php

use kartik\helpers\Html;

use common\models\bill\BillProduct;
?>

<?php 
    ob_start();
    ob_implicit_flush(false);
?>

<table class="table table-bordered table-striped margin-b-none">
    <thead>
        <tr>
            <th style="text-align: center;">#</th>
            <th style="width: 20%;"><?= $model[0]->getAttributeLabel('product_name'); ?></th>
            <th style="width: 11%; text-align: center;"><?= $model[0]->getAttributeLabel('measure_id'); ?></th>
            <th style="width: 11%; text-align: right;"><?= $model[0]->getAttributeLabel('amount'); ?></th>
            <th style="width: 11%; text-align: right;"><?= $model[0]->getAttributeLabel('price'); ?></th>
            <th style="width: 11%; text-align: right;"><?= $model[0]->getAttributeLabel('vat'); ?></th>
            <th style="width: 5%; text-align: center;"><?= $model[0]->getAttributeLabel('revers'); ?></th>
            <th style="width: 11%; text-align: right;"><?= $model[0]->getAttributeLabel('summa'); ?></th>
            <th style="width: 11%; text-align: right;"><?= $model[0]->getAttributeLabel('summa_vat'); ?></th>
            <th style="width: 16%; text-align: right;"><?= $model[0]->getAttributeLabel('total'); ?></th>
        </tr>
    </thead>
    <tbody class="table-products-body">
        <?php foreach ($model as $index => $billProduct): 
            if(empty($billProduct->id)){
                continue;
            }
            ?>
            <tr class="table-products-item">
                <td style="text-align: center;"><?= $index + 1; ?></td>
                <td>
                    <?= $billProduct->productName.(!empty($billProduct->comment) ? '<br/>'.$billProduct->comment : ''); ?>
                </td>
                <td style="text-align: center;">
                    <?= $billProduct->measureName; ?>
                </td>
                <td style="text-align: right;">
                    <?= $billProduct->amount; ?>
                </td>
                <td style="text-align: right;">
                    <?= number_format($billProduct->price, 3, '.', ' '); ?>
                </td>
                <td style="text-align: right;">
                    <?= $billProduct->vat; ?>
                </td>
                <td style="text-align: center;">
                    <?= $billProduct->revers ? Yii::t('common', 'Yes') : ''; ?>
                </td>
                <td style="text-align: right;">
                    <?= number_format($billProduct->summa, 2, '.', ' '); ?>
                </td>
                <td style="text-align: right;">
                    <?= number_format($billProduct->summa_vat, 2, '.', ' '); ?>
                </td>
                <td style="text-align: right;">
                    <?= number_format($billProduct->total, 2, '.', ' '); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        <tr class="table-products-summa">
            <td colspan="7" style="font-weight: bold; font-size: 150%; text-align: right;"><?= $bill->getAttributeLabel('summa'); ?></td>
            <td colspan="3" style="text-align: right; font-weight: bold; font-size: 150%;"><?= number_format($bill->summa, 2, '.', ' '); ?></td>
        </tr>            
        <tr class="table-products-vat">
            <td colspan="7" style="font-weight: bold; font-size: 150%; text-align: right;"><?= $bill->getAttributeLabel('vat'); ?></td>
            <td colspan="3" style="text-align: right; font-weight: bold; font-size: 150%;"><?= number_format($bill->vat, 2, '.', ' '); ?></td>
        </tr>            
        <tr class="table-products-total">
            <td colspan="7" style="font-weight: bold; font-size: 150%; text-align: right;"><?= $bill->getAttributeLabel('total').(isset($bill->valuta_id) ? ', '.$bill->valuta->name : ''); ?></td>
            <td colspan="3" style="text-align: right; font-weight: bold; font-size: 150%;"><?= number_format($bill->total, 2, '.', ' '); ?></td>
        </tr>            
    </tbody>
</table>

<?php
    $body = ob_get_contents();
    ob_get_clean(); 

    $panelContent = [
        'heading' => BillProduct::modelTitle(2),
        'preBody' => '<div class="panel-body">',
        'body' => $body,
        'postBody' => '</div>',
    ];
    echo Html::panel(
        $panelContent, 
        'default', 
        [
            'id' => "panel-product-data",
        ]
    );
?>