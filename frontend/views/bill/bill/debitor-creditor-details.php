<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\bill\Bill */
$this->title = $reportTitle;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="debitor-creditor-details">

    <?= Html::pageHeader(Html::encode($this->title)); ?>
    
    <?= $this->render('debitor-creditor-index', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'isAdmin' => $isAdmin,
        'tillDate' => $tillDate,
    ]);?>

    <?= $this->render('@frontend/views/client/agreement-payment/debitor-creditor-index', [
        'dataProvider' => $dataAgrPaymentProvider,
        'searchModel' => $searchAgrPaymentModel,
        'bankList' => $bankList,
        'isAdmin' => $isAdmin,
    ]);?>

</div>