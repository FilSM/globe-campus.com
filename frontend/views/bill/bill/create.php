<?php

use kartik\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\bill\Bill */

$this->title = Yii::t($model->tableName(), 'Create a new '.$model->modelTitle(1, false));
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$isModal = !empty($isModal);
?>
<div class="bill-create">

    <?php if(!$isModal): ?>
    <?= Html::pageHeader(Html::encode($this->title));?>
    <?php endif; ?>  

    <?= $this->render('_form', [
        'model' => $model,
        'abonentModel' => $abonentModel,
        'agreementModel' => $agreementModel,
        'firstClientModel' => $firstClientModel,
        'secondClientModel' => $secondClientModel,
        'filesModel' => $filesModel,
        'projectList' => $projectList,
        'agreementList' => $agreementList,
        'clientRoleList' => $clientRoleList,
        'valutaList' => $valutaList,
        'managerList' => $managerList,
        'billProductModel' => $billProductModel,
        'billProductProjectModel' => $billProductProjectModel,
        'billFirstPersonModel' => $billFirstPersonModel,
        'billSecondPersonModel' => $billSecondPersonModel,
        'productList' => $productList,
        'measureList' => $measureList,
        'languageList' => $languageList,
        'isAdmin' => $isAdmin,
        'availableDocTypeList' => isset($availableDocTypeList) ? $availableDocTypeList : null,
        'hideDocType' => !empty($hideDocType),
        'isModal' => $isModal,
    ]) ?>

</div>