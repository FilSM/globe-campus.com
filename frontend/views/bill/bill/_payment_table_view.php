<?php

use kartik\helpers\Html;
use kartik\grid\GridView;

use common\components\FSMAccessHelper;
use common\models\bill\BillPayment;
?>

<?php 
    ob_start();
    ob_implicit_flush(false);
?>

<div class="bill-payment-index">
    <?php 
    $columns = [
        ['class' => '\kartik\grid\SerialColumn'],
        /*
        [
            'attribute' => 'id',
            'width' => '75px',
            'hAlign' => 'center',
            //'pageSummary' => $searchModel->getAttributeLabel('total'),
        ],
         * 
         */
        [
            'attribute' => 'payment_order_number',
            'contentOptions' => [
                'style'=>'max-width:100px; min-height:75px; word-wrap: break-word;'
            ],
            'value' => function ($model) {
                return 
                    '<div style="overflow-x: auto;">'.
                    (!empty($model->payment_order_number) ? (FSMAccessHelper::can('viewBill', $model) ?
                        Html::a($model->payment_order_number, ['/payment-order/view', 'id' => $model->payment_order_id], ['target' => '_blank', 'data-pjax' => 0]) :
                        $model->payment_order_number
                    )
                    : null).
                    '</div>';
            },                        
            'format' => 'raw',
        ],
        [
            'attribute'=>'from_bank_id',
            'contentOptions' => [
                'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
            ],
            'value' => function ($model) {
                return 
                    '<div style="overflow-x: auto;">'.
                    (!empty($model->from_bank_name) ? 
                        Html::a($model->from_bank_name, ['/bank/view', 'id' => $model->from_bank_id], ['target' => '_blank', 'data-pjax' => 0])
                        : null
                    ).
                    '</div>';
            },
            'format'=>'raw',
        ],                    
        [
            'attribute'=>'to_bank_id',
            'contentOptions' => [
                'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
            ],
            'value' => function ($model) {
                return 
                    '<div style="overflow-x: auto;">'.
                    (!empty($model->to_bank_name) ? 
                        Html::a($model->to_bank_name, ['/bank/view', 'id' => $model->to_bank_id], ['target' => '_blank', 'data-pjax' => 0])
                        : null
                    ).
                    '</div>';
            },
            'format'=>'raw',
        ], 
        [
            'attribute' => 'paid_date',
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'headerOptions' => ['class'=>'td-mw-75'],
            'width' => '100px',
            'value' => function ($model) {
                return isset($model->paid_date) ? date('d-M-Y', strtotime($model->paid_date)) : null;
            },
        ],                    
        [
            'attribute' => 'summa_origin',
            'label' => $billdModel->getAttributeLabel('summa'),//.' '.$billdModel->valuta->name,
            'hAlign' => 'right',
            'headerOptions' => ['style'=>'text-align: center;'],
            'mergeHeader' => true,
            'value' => function ($model) {
                return isset($model->summa_origin) ? number_format($model->summa_origin, 2).' '.$model->valuta->name : null;
            },
            //'format' => ['decimal', 2],
            //'pageSummary' => true,                 
        ],                     
        [
            'attribute' => 'summa_eur',
            'hAlign' => 'right',
            'headerOptions' => ['style'=>'text-align: center;'],
            'mergeHeader' => true,
            'value' => function ($model) {
                return isset($model->summa_eur) ? $model->summa_eur : null;
            },
            'format' => ['decimal', 2],
            'pageSummary' => true,                 
        ],
        [
            'class' => '\common\components\FSMActionColumn',
            'headerOptions' => ['class'=>'td-mw-125'],
            'dropdown' => true,
            'template' => '{confirm} {update} {delete}',
            'checkCanDo' => true,
            'buttons' => [
                'confirm' => function ($params) { 
                    return BillPayment::getButtonConfirm($params);
                },
                'update' => function ($params) { 
                    return BillPayment::getButtonUpdate($params);
                },
            ], 
            'controller' => 'bill-payment',
            'linkedObj' => [
                ['fieldName' => 'bill_id', 'id' => (!empty($billdModel->id) ? $billdModel->id : null)],
            ],             
        ],
    ];
    ?>

    <?= GridView::widget([
        'id' => 'grid-view',
        'tableOptions' => [
            'id' => 'bill-payment-list',
        ],
        'responsive' => false,
        'striped' => true,
        'hover' => true,
        'bordered' => true,
        'condensed' => true,
        'persistResize' => false,
        'autoXlFormat' => true,
        'dataProvider' => $dataProvider,
        'showPageSummary' => true,
        'pjax' => true,
        'columns' => $columns,
    ]); ?>  
</div>

<?php
    $body = ob_get_contents();
    ob_get_clean(); 

    $panelContent = [
        'heading' => BillPayment::modelTitle(2),
        'preBody' => '<div class="panel-body">',
        'body' => $body,
        'postBody' => '</div>',
    ];
    echo Html::panel(
        $panelContent, 
        'warning', 
        [
            'id' => "panel-payment-data",
        ]
    );
?>