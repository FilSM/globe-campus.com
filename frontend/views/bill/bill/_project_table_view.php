<?php

use kartik\helpers\Html;

use common\components\FSMHtml;
use common\models\bill\BillProductProject;
?>

<?php 
    ob_start();
    ob_implicit_flush(false);
?>

<table class="table table-bordered table-striped margin-b-none">
    <thead>
        <tr>
            <th class="text-center" style="width: 5%;">#</th>
            <th style="width: 40%;"><?= $billProductModel[0]->getAttributeLabel('product_id').' / '.$billProductModel[0]->getAttributeLabel('comment'); ?></th>
            <th style="width: 35%;"><?= $billProductProjectModel[0]->getAttributeLabel('project_id'); ?></th>
            <th class="text-right" style="width: 10%;"><?= $billProductModel[0]->getAttributeLabel('summa'); ?></th>
        </tr>
    </thead>
    <tbody class="table-object-body">
        <?php 
        $summa = 0;
        foreach ($billProductModel as $index => $billProduct): 
            $productObject = null;
            foreach ($billProductProjectModel as $billProductProject) {
                if($billProductProject->bill_product_id == $billProduct->id){
                    $productObject = $billProductProject;
                    break;
                }
            }
            if(!($billProductProject = $productObject)){
                continue;
            };
            
            $summa += $billProduct->summa;
        ?>
            <tr class="table-object-item">
                <td class="text-center"><?= $index + 1; ?></td>
                <td>
                    <?= $billProduct->productName.(!empty($billProduct->comment) ? '<br/>'.$billProduct->comment : ''); ?>
                </td>
                <td class="text-right">
                    <?= !empty($billProductProject->project) ? $billProductProject->project->name : ''; ?>
                </td>
                <td class="text-right">
                    <?= number_format($billProduct->summa, 2, '.', ' '); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        <tr class="table-products-summa">
            <td colspan="2"></td>
            <td class="text-right" style="font-weight: bold; font-size: 150%;"><?= $bill->getAttributeLabel('summa'); ?></td>
            <td class="text-right" style="font-weight: bold; font-size: 150%;"><?= number_format($summa, 2, '.', ' '); ?></td>
        </tr>            
    </tbody>
</table>

<?php
    $body = ob_get_contents();
    ob_get_clean(); 

    $panelContent = [
        'heading' => BillProductProject::modelTitle(2),
        'preBody' => '<div class="panel-body">',
        'body' => $body,
        'postBody' => '</div>',
    ];
    echo FSMHtml::panel(
        $panelContent, 
        'default', 
        [
            'id' => "panel-object-data",
            'bsVersion' => 3,
        ]
    );
?>