<?php
//namespace common\models;

//use Yii;
//use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\grid\GridView;
use yii\helpers\Url;

use kartik\helpers\Html;
use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;
use kartik\export\ExportMenu;

use common\models\bill\Bill;
use common\components\FSMHelper;
use common\components\FSMAccessHelper;
use common\components\FSMExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel common\models\bill\search\BillSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = !empty($reportTitle) ? $reportTitle : $searchModel->modelTitle(2);
$this->params['breadcrumbs'][] = $this->title;

if(!empty($_GET['BillSearch']['agreement_id']) && !empty($_GET['BillSearch']['doc_type'])){
    if(is_array($_GET['BillSearch']['doc_type'])){
        if(count($_GET['BillSearch']['doc_type']) == 1){
            $showWriteOnBasisBtn = $_GET['BillSearch']['doc_type'][0] == Bill::BILL_DOC_TYPE_AVANS;
        }else{
            $showWriteOnBasisBtn = false;
        }
    }else{
        $showWriteOnBasisBtn = $_GET['BillSearch']['doc_type'] == Bill::BILL_DOC_TYPE_AVANS;
    }
}else{
    $showWriteOnBasisBtn = false;
}

if(!empty($_GET['BillSearch']['status']) && !empty($_GET['BillSearch']['second_client_id'])){
    if(is_array($_GET['BillSearch']['status'])){
        if(count($_GET['BillSearch']['status']) == 1){
            $showSendPaymentBtn = in_array($_GET['BillSearch']['status'][0], [Bill::BILL_STATUS_PREP_PAYMENT, Bill::BILL_STATUS_PAID]);
        }else{
            $showSendPaymentBtn = false;
        }
    }else{
        $showSendPaymentBtn = in_array($_GET['BillSearch']['status'], [Bill::BILL_STATUS_PREP_PAYMENT, Bill::BILL_STATUS_PAID]);
    }
}else{
    $showSendPaymentBtn = false;
}

$exportMode = !empty($_POST['export_type']);
$forDetail = !empty($forDetail);
?>
<div id="bill-index">

    <div id="page-content">
        <?php
            if(!$forDetail): 
                echo $this->render('_search', [
                    'model' => $searchModel,
                    'projectList' => $projectList,
                    'agreementList' => $agreementList,
                    'clientList' => $clientList,
                    'managerList' => $managerList,
                ]);
            
            endif;
        ?>        
        <?php
            $columns = [
                /*
                [
                    'class' => '\common\components\FSMActionColumn',
                    'headerOptions' => ['class'=>'td-mw-125'],
                    'dropdown' => true,
                    'dropdownDefaultBtn' => 'view-pdf',
                    'template' => '{view-pdf} {view} {update} {delete}',
                    'buttons' => [
                        'view-pdf' => function (array $params) { 
                            return Bill::getButtonPrint($params);
                        },
                    ],
                    'controller' => 'bill',
                ],
                 * 
                 */
                
                [
                    'class' => '\kartik\grid\CheckboxColumn',
                    'width' => '25px',
                    //'visible' => $showWriteOnBasisBtn || $showSendPaymentBtn,
                ],
                
                //['class' => '\kartik\grid\SerialColumn'],

                [
                    'attribute' => 'id',
                    'width' => '75px',
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                ],
                /*
                [
                    'attribute' => 'doc_type',
                    'headerOptions' => ['class'=>'td-mw-150'],
                    'value' => function ($model) {
                        return isset($model->doc_type) ? $model->billDocTypeList[$model->doc_type] : null;
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => $searchModel->billDocTypeList,
                    'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                    'filterInputOptions' => ['id' => 'doc-type-search', 'placeholder' => '...'],
                    'visible' => $isAdmin,
                ],     
                 * 
                 */  
                [
                    'attribute'=>'doc_type',
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'value' => function ($model) {
                        return isset($model->doc_type) ? $model->billDocTypeList[$model->doc_type] : null;
                    },                         
                    'format'=>'raw',
                    'visible' => $exportMode,                        
                ],                      
                [
                    'attribute'=>'doc_number',
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'value' => function ($model) {
                        return !empty($model->doc_number) ? $model->doc_number : null;
                    },                         
                    'format'=>'raw',
                    'visible' => $exportMode,                        
                ],                 
                [
                    'attribute'=>'doc_number',
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'value' => function ($model) {
                        return !empty($model->doc_number) ? 
                            (isset($model->doc_type) ? $model->billDocTypeList[$model->doc_type].'<br/>' : null).
                                (FSMAccessHelper::can('viewBill', $model) ?
                                    Html::a($model->doc_number, ['/bill/view', 'id' => $model->id], ['target' => '_blank', 'data-pjax' => 0]) :
                                    $model->doc_number
                                )
                            : null;
                    },                         
                    'format'=>'raw',
                    'hiddenFromExport' => true,
                ],                      
                [
                    'attribute' => 'status',
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'value' => function ($model) {
                        return $model->statusHTML;
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => $searchModel->billStatusList,
                    'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                    'filterInputOptions' => [
                        'placeholder' => '...',
                        //'multiple' => true,
                    ],
                    'format'=>'raw',
                ],                         
                [
                    'header' => Yii::t('common', 'Progress'),
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'mergeHeader' => true,
                    'value' => function ($model) {
                        return in_array($model->doc_type, [
                            Bill::BILL_DOC_TYPE_AVANS,
                            Bill::BILL_DOC_TYPE_BILL,
                            Bill::BILL_DOC_TYPE_INVOICE,
                            Bill::BILL_DOC_TYPE_CESSION,
                            ]) ? $model->getProgressButtons() : '&nbsp;';
                    },
                    'format' => 'raw',
                    'visible' => FSMAccessHelper::can('changeBillStatus'),
                    'hiddenFromExport' => true,
                ],                        
                [
                    'attribute'=>'first_client_role_name',
                    'label' => Yii::t('agreement', 'First party role'),
                    'headerOptions' => ['class'=>'td-mw-200'],
                    'value' => function ($model) {
                        return !empty($model->first_client_role_name) ? $model->first_client_role_name : '';
                    },
                    'visible' => $exportMode,
                ],                          
                [
                    'attribute'=>'first_client_name',
                    'headerOptions' => ['class'=>'td-mw-200'],
                    'value' => function ($model) {
                        return !empty($model->first_client_name) ? $model->first_client_name : null;
                    },                         
                    'format'=>'raw',
                    'visible' => $exportMode,
                ],                             
                [
                    'attribute'=>'first_client_regno',
                    'hAlign' => GridView::ALIGN_RIGHT,
                    'value' => function ($model) {
                        return !empty($model->first_client_regno) ? $model->first_client_regno : null;
                    },                         
                    'visible' => $exportMode,
                ],                          
                [
                    'attribute'=>'first_client_id',
                    'headerOptions' => ['class'=>'td-mw-150'],
                    'contentOptions' => [
                        'style'=>'max-width:150px; word-wrap: break-word;'
                    ],
                    'value' => function ($model) {
                        return 
                            '<div style="overflow-x: auto;">'.
                            (!empty($model->first_client_name) ? 
                            (!empty($model->first_client_role_name) ? $model->first_client_role_name.'<br/>' : '') . 
                                (FSMAccessHelper::can('viewClient', $model->firstClient) ?
                                    Html::a($model->first_client_name, ['/client/view', 'id' => $model->first_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                    $model->first_client_name
                                )
                            : null).
                            '</div>';
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => $clientList,
                    'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                    'filterInputOptions' => [
                        'id' => 'billsearch-first_client_name',
                        'placeholder' => '...',
                    ],                            
                    'format'=>'raw',
                    'hiddenFromExport' => true,
                ],              
                [
                    'attribute'=>'second_client_role_name',
                    'label' => Yii::t('agreement', 'Second party role'),
                    'headerOptions' => ['class'=>'td-mw-200'],
                    'value' => function ($model) {
                        return !empty($model->second_client_role_name) ? $model->second_client_role_name : '';
                    },                         
                    'visible' => $exportMode,
                ],       
                [
                    'attribute'=>'second_client_name',
                    'headerOptions' => ['class'=>'td-mw-200'],
                    'value' => function ($model) {
                        return !empty($model->second_client_name) ? $model->second_client_name : null;
                    },                         
                    'visible' => $exportMode,
                ],                             
                [
                    'attribute'=>'second_client_regno',
                    'hAlign' => GridView::ALIGN_RIGHT,
                    'value' => function ($model) {
                        return !empty($model->second_client_regno) ? $model->second_client_regno : null;
                    },                
                    'visible' => $exportMode,
                ],                         
                [
                    'attribute'=>'second_client_id',
                    'headerOptions' => ['class'=>'td-mw-150'],
                    'contentOptions' => [
                        'style'=>'max-width:150px; word-wrap: break-word;'
                    ],
                    'value' => function ($model) {
                        return  
                            '<div style="overflow-x: auto;">'.
                            (!empty($model->second_client_name) ?  
                            (!empty($model->second_client_role_name) ? $model->second_client_role_name.'<br/>' : '') . 
                                (FSMAccessHelper::can('viewClient', $model->secondClient) ?
                                    Html::a($model->second_client_name, ['/client/view', 'id' => $model->second_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                    $model->second_client_name
                                )
                            : null).
                            '</div>';
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => $clientList,
                    'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                    'filterInputOptions' => [
                        'placeholder' => '...',
                    ],
                    'format'=>'raw',
                    'hiddenFromExport' => true,                            
                ],       
                [
                    'attribute' => 'summa',
                    'hAlign' => 'right',
                    'headerOptions' => ['style'=>'text-align: center;'],
                    'mergeHeader' => true,
                    'xlFormat' => '###0.00',
                    'exportMenuStyle' => [
                        'numberFormat' => ['formatCode' => '###0.00'],
                    ],
                    'format' => ['decimal', 2],
                    'pageSummary' => true,    
                    'visible' => $exportMode,
                ],                              
                [
                    'attribute' => 'vat',
                    'hAlign' => 'right',
                    'headerOptions' => ['style'=>'text-align: center;'],
                    'mergeHeader' => true,
                    'xlFormat' => '###0.00',
                    'exportMenuStyle' => [
                        'numberFormat' => ['formatCode' => '###0.00'],
                    ],
                    'format' => ['decimal', 2],
                    'pageSummary' => true,    
                    'visible' => $exportMode,
                ],                              
                            /*
                [
                    'attribute' => 'summa',
                    'hAlign' => 'right',
                ],                         
                [
                    'attribute' => 'vat',
                    'hAlign' => 'right',
                ],     
                             * 
                             */
                [
                    'attribute'=>'total',
                    'hAlign' => 'right',
                    'headerOptions' => ['class'=>'td-mw-150'],
                    'xlFormat' => '###0.00',
                    'exportMenuStyle' => [
                        'numberFormat' => ['formatCode' => '###0.00'],
                    ],  
                    'format' => ['decimal', 2],
                    'visible' => $exportMode,
                ],       
                [
                    'attribute'=>'valuta_id',
                    'headerOptions' => ['class'=>'td-mw-150'],
                    'value' => function ($model) {
                        return isset($model->valuta_id) ? $model->valuta->name : null;
                    },                         
                    'visible' => $exportMode,
                ],                             
                [
                    'attribute' => 'total',
                    'hAlign' => 'right',
                    'vAlign' => 'middle',
                    'mergeHeader' => true,
                    'headerOptions' => ['style'=>'text-align: center;'],
                    //'label' => Yii::t('bill', 'Sum'),
                    'value' => function ($model) {
                        $total = (($model->pay_status != Bill::BILL_PAY_STATUS_PART) ? $model->total : $model->billNotPaidSumma);
                        return 
                        '<span class="cell-total" '
                            . 'data-sum="'.$total.'" '
                            . 'data-valuta="'.($model->valuta_id ?? '').'">'.
                            number_format($model->total, 2, '.', ' ')
                        . '</span>'
                        . (isset($model->valuta_id) ? '&nbsp;<span>'.$model->valuta->name.'</span>' :'');
                    },
                    'hiddenFromExport' => true,
                    'format' => 'raw',
                ],                         
                // 'delayed:datetime',
                [
                    'attribute' => 'doc_date',
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'headerOptions' => ['class'=>'td-mw-75'],
                    'value' => function ($model) use ($exportMode) {
                        if($exportMode){
                            return date('d.m.Y', strtotime($model->doc_date));
                        }else{
                            return isset($model->doc_date) && ($model->doc_date == date('Y-m-d')) ? 
                                Html::a(Html::badge(Yii::t('common', 'Today'), ['class' => 'badge-success']), 
                                    Url::to(['/bill/index']).'?BillSearch[doc_date]='.$model->doc_date, 
                                    ['target' => '_blank', 'data-pjax' => 0]) 
                                : date('d-M-Y', strtotime($model->doc_date));
                        }
                    },
                    'filterType' => GridView::FILTER_DATE_RANGE,
                    'filterWidgetOptions' => [
                        'pluginOptions' => \yii\helpers\ArrayHelper::merge(
                            Yii::$app->params['DatePickerPluginOptions'], 
                            [
                                'locale' => [
                                    'firstDay' => 1,
                                    'format' => 'd-m-Y',
                                    'cancelLabel' => Yii::t('common', 'Clear'),
                                ],
                                //'opens'=>'left',
                                //'startDate' => date('d-m-Y'),
                            ]),
                        'convertFormat' => true,
                        //'presetDropdown' => true,
                        'hideInput' => true,
                        'pluginEvents' => [
                            'cancel.daterangepicker' => 'function() {
                                //console.log("cancel.daterangepicker"); 
                                var gridFilters = $("table tr.filters");
                                var daterangeinput = gridFilters.find("#billsearch-doc_date");
                                daterangeinput.val("").change();
                            }',
                        ],
                    ],
                    'format'=>'raw',  
                ],                         
                [
                    'attribute' => 'pay_date',
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'headerOptions' => ['class'=>'td-mw-75'],
                    'value' => function ($model) use ($exportMode) {
                        if($exportMode){
                            return date('d.m.Y', strtotime($model->pay_date));
                        }else{
                            $class = 'badge-success';
                            if(in_array($model->status, [
                                Bill::BILL_STATUS_CANCELED,
                                Bill::BILL_STATUS_COMPLETE,
                                ]))
                            {
                                $badge = '';
                            }else{
                                $firstDate = new DateTime(date('Y-m-d'));
                                $secondDate = new DateTime($model->pay_date);
                                $dateDiff = date_diff($firstDate, $secondDate, true)->days;

                                $firstDate = $firstDate->format('Y-m-d');
                                $secondDate = $secondDate->format('Y-m-d');
                                if($firstDate > $secondDate){
                                    $dateDiff = -1 * $dateDiff;
                                    $class = 'badge-danger';
                                }elseif($dateDiff >= 0){
                                    $class = 'badge-success';
                                }

                                $badge = isset($model->pay_date) && ($model->pay_date == date('Y-m-d')) ? 
                                    '' : Html::badge($dateDiff, ['class' => $class]);
                            }
                            return isset($model->pay_date) && ($model->pay_date == date('Y-m-d')) ? 
                                Html::a(Html::badge(Yii::t('common', 'Today'), ['class' => $class]) , 
                                    Url::to(['/bill/index']).'?BillSearch[pay_date]='.$model->pay_date, 
                                    ['target' => '_blank', 'data-pjax' => 0]) 
                                : date('d-M-Y', strtotime($model->pay_date)).(!empty($badge) ? '<br/>' : '').$badge;
                        }
                    },
                            /*
                    'filterType' => GridView::FILTER_DATE,
                    'filterWidgetOptions' => [
                        'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
                        'pickerButton' => false,
                        'layout' => '{input}{remove}',
                        'buttonOptions' => [],
                    ],
                             * 
                             */
                    'filterType' => GridView::FILTER_DATE_RANGE,
                    'filterWidgetOptions' => [
                        'pluginOptions' => \yii\helpers\ArrayHelper::merge(
                            Yii::$app->params['DatePickerPluginOptions'], 
                            [
                                'locale' => [
                                    'firstDay' => 1,
                                    'format' => 'd-m-Y',
                                    'cancelLabel' => Yii::t('common', 'Clear'),
                                ],
                                //'opens'=>'left',
                                //'startDate' => date('d-m-Y'),
                            ]),
                        'convertFormat' => true,
                        //'presetDropdown' => true,
                        'hideInput' => true,
                        'pluginEvents' => [
                            'cancel.daterangepicker' => 'function() {
                                //console.log("cancel.daterangepicker"); 
                                var gridFilters = $("table tr.filters");
                                var daterangeinput = gridFilters.find("#billsearch-pay_date");
                                daterangeinput.val("").change();
                            }',
                        ],
                    ],
                    'format'=>'raw',
                ],
                [
                    'attribute' => 'mail_status',
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'value' => function ($model) {
                        return $model->mailStatusHTML;
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => $searchModel->billMailStatusList,
                    'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                    'filterInputOptions' => [
                        'placeholder' => '...',
                        //'multiple' => true,
                    ],
                    'format'=>'raw',
                ],                         
                [
                    'attribute' => 'transfer_status',
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'value' => function ($model) {
                        return $model->transferStatusHTML;
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => $searchModel->billTransferStatusList,
                    'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                    'filterInputOptions' => [
                        'placeholder' => '...',
                        //'multiple' => true,
                    ],
                    'format'=>'raw',
		    'visible' => !empty(Yii::$app->params['ENABLE_BILL_TRANSFER']),
                ],                         
                [
                    'attribute' => 'paid_date',
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'headerOptions' => ['class'=>'td-mw-75'],
                    'value' => function ($model) {
                        return isset($model->paid_date) ? date('d-M-Y', strtotime($model->paid_date)) : null;
                    },
                    'filterType' => GridView::FILTER_DATE_RANGE,
                    'filterWidgetOptions' => [
                        'pluginOptions' => \yii\helpers\ArrayHelper::merge(
                            Yii::$app->params['DatePickerPluginOptions'], 
                            [
                                'locale' => [
                                    'firstDay' => 1,
                                    'format' => 'd-m-Y',
                                    'cancelLabel' => Yii::t('common', 'Clear'),
                                ],
                                //'opens'=>'left',
                                //'startDate' => date('d-m-Y'),
                            ]),
                        'convertFormat' => true,
                        //'presetDropdown' => true,
                        'hideInput' => true,
                        'pluginEvents' => [
                            'cancel.daterangepicker' => 'function() {
                                //console.log("cancel.daterangepicker"); 
                                var gridFilters = $("table tr.filters");
                                var daterangeinput = gridFilters.find("#billsearch-paid_date");
                                daterangeinput.val("").change();
                            }',
                        ],
                    ],
                ],                                          
                [
                    'attribute' => 'pay_status',
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'value' => function ($model) {
                        return $model->payStatusHTML;
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => $searchModel->billPayStatusList,
                    'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                    'filterInputOptions' => [
                        'placeholder' => '...',
                        //'multiple' => true,
                    ],
                    'format'=>'raw',
                ],
                [
                    'class' => '\common\components\FSMActionColumn',
                    'header' => Yii::t('common', 'Options'),
                    'headerOptions' => ['class'=>'td-mw-125'],
                    'dropdown' => true,
                    'pullLeft' => false,
                    'dropdownButton' => [
                        'class' => 'btn btn-default',
                        'label' => Yii::t('common', 'Options'),
                    ],
                    'isDropdownActionColumn' => true,
                    'dropdownDefaultBtn' => 'pay',
                    'template' => '{pay} {write-on-basis} {credit-invoice} {mutual-settlement} {cession} {debt-relief} {cancel}',
                    'buttons' => $searchModel->getOptionsActionButtons('xs'),
                    'visible' => FSMAccessHelper::can('updateBill'),
                    'hiddenFromExport' => true,
                ],
                [
                    'attribute' => "blocked",   
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'class' => '\common\components\FSMBooleanColumn',
                    'trueLabel' => 'Yes', 
                    'falseLabel' => 'No',
                    'width' => '100px',
                    'value' => function ($model) {
                        return $model->cellButtonBlock;
                    },
                            
                    'visible' => ($isAdmin || FSMAccessHelper::can('blockBill')) && !$forDetail,
                    'format'=>'raw',
                    'hiddenFromExport' => true,
                ],                            
                [
                    'attribute' => "has_act",   
                    'vAlign' => 'middle',
                    'class' => '\common\components\FSMBooleanColumn',
                    'trueLabel' => 'Yes', 
                    'falseLabel' => 'No',
                    'width' => '75px',
                    'value' => function ($model) {
                        $params = [
                            'url' => Url::to(['view-act-pdf', 'id' => $model->id]), 
                            'model' => $model, 
                            'isBtn' => true,
                        ];
                        return Bill::getButtonActPrint($params);
                    },
                ],                            
                [
                    'attribute' => "deleted",   
                    'vAlign' => 'middle',
                    'class' => '\kartik\grid\BooleanColumn',
                    'trueLabel' => 'Yes', 
                    'falseLabel' => 'No',
                    'width' => '75px',
                    'visible' => ($isAdmin || FSMAccessHelper::can('showDeletedItems') || $exportMode) && !$forDetail,
                ],
                [
                    'class' => '\common\components\FSMActionColumn',
                    'headerOptions' => ['class'=>'td-mw-125'],
                    'dropdown' => true,
                    'dropdownDefaultBtn' => 'view-attachment',
                    'checkPermission' => true,
                    'template' => !empty(Yii::$app->params['ENABLE_BILL_TRANSFER']) ?
		    	'{view-pdf} {view-attachment} {ajax-add-attachment} {view} {send-other-abonent} {receive-other-abonent} {send-mail-client} {send-mail-agent} {copy} {update} {delete}' :
		    	'{view-pdf} {view-attachment} {ajax-add-attachment} {view} {send-mail-client} {send-mail-agent} {copy} {update} {delete}',
                    'buttons' => [
                        'ajax-add-attachment' => function (array $params) { 
                            return Bill::getButtonAddAttachment($params);
                        },
                        'view-attachment' => function (array $params) { 
                            return Bill::getButtonViewAttachment($params);
                        },
                        'view-pdf' => function (array $params) { 
                            return Bill::getButtonPrint($params);
                        },
                        'send-other-abonent' => function (array $params) { 
                            return Bill::getButtonSendOtherAbonent($params);
                        },
                        'receive-other-abonent' => function (array $params) { 
                            return Bill::getButtonReceiveOtherAbonent($params);
                        },
                        'send-mail-client' => function (array $params) { 
                            return Bill::getButtonSendMailClient($params);
                        },
                        'send-mail-agent' => function (array $params) { 
                            return Bill::getButtonSendMailAgent($params);
                        },
                        'copy' => function (array $params) { 
                            return Bill::getButtonCopy($params);
                        },
                    ],
                    'controller' => 'bill',
                    'visible' => !$forDetail,                                
                ],
            ];
        ?>
        
        <?php
            $panelBefore = $panelAfter = '';
            if(!$forDetail): 
                $searchForm = '';
                /*$this->render('_search', [
                        'model' => $searchModel,
                        'projectList' => $projectList,
                        'agreementList' => $agreementList,
                        'clientList' => $clientList,
                        'managerList' => $managerList,
                    ]);
                 * 
                 */

                $panelBefore = '<div class="btn-group">';
                if (FSMAccessHelper::can('createBill')):
                    $panelBefore .= Html::a(Html::icon('plus') . '&nbsp;' . $searchModel->modelTitle(), ['create'], ['class' => 'btn btn-success', 'data-pjax' => 0]);
                endif;
                
                if($showWriteOnBasisBtn && FSMAccessHelper::can('createBill')) :
                    $panelBefore .= \common\components\FSMBtnMultiAction::aButton(
                            Yii::t('bill', 'Write out on this basis'), 
                            Url::to(['bill-write-on-basis-many']),
                            [
                                'model' => $searchModel,
                                'grid' => 'grid-view',
                                'icon' => 'share',
                                'confirm' => '',
                            ],
                            [
                                'id' => 'bill-write-on-basis-many',
                                'class' => 'btn btn-info',
                                'disabled' => true,
                            ]
                    );           
                endif;
                
                if($showSendPaymentBtn) :
                    $panelBefore .= FSMHelper::vButton(null, [
                        'label' => Yii::t('bill', 'Add to payment order'),
                        'title' => Yii::t('bill', 'Add to payment order'),
                        'controller' => 'bill-payment',
                        'action' => 'ajax-create-many',
                        'icon' => 'share',
                        'class' => 'info',
                        'modal' => true,
                        'options' => [
                            'id' => 'bill-payments-create-many',
                            'disabled' => true,
                        ],
                    ]);
                endif;                
                
                $panelBefore .= FSMHelper::vButton(null, [
                    'label' => Yii::t('common', 'Search'),
                    //'title' => Yii::t('common', 'Search'),
                    'icon' => 'search',
                    'class' => 'default search-toggle-button',
                ]);
                
                $panelBefore .= '</div>';
                $panelBefore .= '<div class="total-selected"><span>'.Yii::t('bill', 'Total selected').': </span><span id="total-selected-sum">0.00</span></div>';
                $panelBefore .= $searchForm;
                
                $panelAfter = '<fieldset id="background-legend-fieldset">';
                $panelAfter .= '<table class="kv-grid-table" style="width: 20%;"><tr>';
                $panelAfter .= 
                    '<td>'.Yii::t('bill', 'Background legend').': </td>'.
                    '<td class="bill-row-blocked" style="width: 33%; text-align: center;">'.Yii::t('bill', 'Blocked').'</td>';
                $panelAfter .= '</tr></table>';
                $panelAfter .= '</fieldset>';
            endif;
        ?>
        
        <?= DynaGrid::widget([
            'columns' => $columns,
            'storage' => DynaGrid::TYPE_DB,
            //'theme' => 'simple-striped',
            'allowThemeSetting' => false,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'gridOptions' => [
                //'panel'=>['heading'=>'<h3 class="panel-title">Library</h3>'],
                //'panel' => false,
                'panel' => [
                    'heading' => '<h3 class="panel-title">' . Html::encode($this->title) . '</h3>',
                    'type' => 'default',
                    'before' => !$forDetail ? $panelBefore : null,
                    'after' => !$forDetail ? $panelAfter : null,
                ],
/*
                'panelTemplate' => 
                    '<div class="panel {type}">
                        {panelHeading}
                        {panelBefore}
                        {items}
                        {panelAfter}
                        {panelFooter}
                    </div>',
*/
                'id' => 'grid-view',
                'tableOptions' => [
                    'id' => 'bill-list',
                ],
                'responsive' => false,
                'condensed' => true,
                //'striped' => false,
                'hover' => true,
                'dataProvider' => $dataProvider,
                'filterModel' => !$forDetail ? $searchModel : null,
                'pjax' => true,
                //'export' => false,
                'floatHeader' => true,
                'toolbar' =>  [
                    //['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                    ['content' => 
                        FSMExportMenu::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => $columns,
                            'showColumnSelector' => false,
                            'showConfirmAlert' => false,
                            'dropdownOptions' => [
                                'label' => Yii::t('kvgrid', 'Export'),
                                'menuOptions' => [
                                    'class' => 'pull-right',
                                ],
                            ],
                            'target' => ExportMenu::TARGET_SELF,
                            'exportConfig' => [
                                ExportMenu::FORMAT_TEXT => false,
                                ExportMenu::FORMAT_HTML => false,
                                ExportMenu::FORMAT_CSV => false,
                                ExportMenu::FORMAT_TEXT => false,
                                ExportMenu::FORMAT_PDF => false,
                                ExportMenu::FORMAT_EXCEL => false,
                                ExportMenu::FORMAT_EXCEL_X => ['label' => Yii::t('kvgrid', 'Excel'), 'alertMsg' => ''],
                            ],
                            'pjaxContainerId' => 'bill-index',
                            'exportFormView' => '@vendor/kartik-v/yii2-export/views/_form',
                        ]).'{dynagrid}'
                    ],
                    //'{export}',
                ],
                'rowOptions' => function ($model, $key, $index, $grid) {
                    if($model['blocked'] == 1){
                        return ['class' => 'bill-row-blocked'];
                    }else{
                        return [];
                    }
                }, 
            ],
            'options' => ['id' => 'dynagrid-bill'], // a unique identifier is important
        ]);
        ?>

    </div>    
</div>