<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\bill\Bill */
$this->title = $reportTitle;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vat-details">

    <?= Html::pageHeader(Html::encode($this->title)); ?>
    
    <?= $this->render('vat-index', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'reportTitle' => $reportTitle,
        'client_id' => $client_id,
        'isAdmin' => $isAdmin,
    ]);?>

    <?php if(!empty($direction) && ($direction == 'out')) : ?>
    <?= $this->render('@frontend/views/bill/expense/ebitda-index', [
        'dataProvider' => $dataExpenseProvider,
        'searchModel' => $searchExpenseModel,
        'client_id' => $client_id,
        'isAdmin' => $isAdmin,
    ]);?>
    <?php endif; ?>

</div>