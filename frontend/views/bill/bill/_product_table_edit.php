<?php
use yii\helpers\Url;
//use yii\widgets\MaskedInput;

use kartik\helpers\Html;
use kartik\widgets\Select2;
use kartik\checkbox\CheckboxX;

use common\widgets\dynamicform\DynamicFormWidget;
use common\widgets\MaskedInput;
use common\models\bill\BillProduct;
use common\models\bill\BillProductProject;
use common\components\FSMHtml;

$isModal = !empty($isModal);
?>

<?php
    ob_start();
    ob_implicit_flush(false);
?>

<?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_wrapper',
    'widgetBody' => '.form-products-body',
    'widgetItem' => '.form-products-item',
    'min' => 1,
    'insertButton' => '.add-item',
    'deleteButton' => '.delete-item',
    'model' => $model[0],
    'formId' => $form->id,
    'formFields' => [
        'product_id',
        'product_name',
        'project_id',
        'measure_id',
        'amount',
        'price',
        'vat',
        'revers',
        'summa',
        'summa_vat',
        'total',
    ],
]); ?>

<table class="table table-bordered table-striped margin-b-none">
    <thead>
        <tr>
            <th class="required" style="width: 25%;"><?= $model[0]->getAttributeLabel('product_name'); ?></th>
            <th class="text-center" style="width: 15%;"><?= $model[0]->getAttributeLabel('measure_id'); ?></th>
            <th class="text-right" style="width: 9%;"><?= $model[0]->getAttributeLabel('amount'); ?></th>
            <th class="text-right" style="width: 9%;"><?= $model[0]->getAttributeLabel('price'); ?></th>
            <th class="text-right" style="width: 9%;"><?= $model[0]->getAttributeLabel('vat'); ?></th>
            <th class="text-center" style="width: 5%;"><?= $model[0]->getAttributeLabel('revers'); ?></th>
            <th class="text-right" style="width: 9%;"><?= $model[0]->getAttributeLabel('summa'); ?></th>
            <th class="text-right" style="width: 9%;"><?= $model[0]->getAttributeLabel('summa_vat'); ?></th>
            <th class="text-right" style="width: 9%;"><?= $model[0]->getAttributeLabel('total'); ?></th>
            <th class="text-center" style="width: 90px;"><?= Yii::t('kvgrid', 'Actions'); ?></th>
        </tr>
    </thead>
    <tbody class="form-products-body">
        <?php 
        foreach ($model as $index => $billProduct):
            $productProject = null;
            foreach ($billProductProjectModel as $billProductProject) {
                if(in_array($billProduct->id, [
                    $billProductProject->bill_product_id,
                    $billProductProject->product_id,
                ])){
                    $productProject = $billProductProject;
                    break;
                }
            }
            $billProductProject = $productProject ?? new BillProductProject();
        ?>
            <tr class="form-products-item">
                <td>
                    <?= Html::activeHiddenInput($billProduct, "[{$index}]id"); ?>
                    <?= Html::activeHiddenInput($billProductProject, "[{$index}]id");?>
                    
                    <?= $form->field($billProduct, "[{$index}]product_id", [    
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                        'options' => [
                            'id' => "product-{$index}-product_id",
                            'class' => 'form-group',
                            'style' => 'display: none;',
                        ],
                    ])->widget(Select2::class, [
                        'data' => $productList, 
                        'options' => [
                            'placeholder' => '...',
                            'class' => 'product-item-select',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
                        ],
                        'addon' => [
                            'prepend' => $billProduct->getModalButtonContent([
                                'formId' => $form->id,
                                'controller' => 'product',
                            ]),
                        ],                          
                    ])->label(false); ?>
                    
                    <?= $form->field($billProduct, "[{$index}]product_name", [    
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                        'options' => [
                            //'id' => "product-{$index}-product_name",
                            'class' => 'form-group',
                            'style' => 'display: none; margin-top: 0px;',
                        ],
                    ])->textInput([
                        'class' => 'product-item-input', 
                    ])->label(false); ?>
                    
                    <?php if(!empty($useProjects)) :
                        echo $form->field($billProductProject, "[{$index}]project_id", [    
                            'horizontalCssClasses' => [
                                'wrapper' => 'col-md-12',
                            ],
                            'options' => [
                                'id' => "product-{$index}-project_id",
                                'class' => 'form-group',
                            ],
                        ])->widget(Select2::class, [
                            'data' => $projectList, 
                            'options' => [
                                'placeholder' => Yii::t('bill', 'Project'),
                                'class' => 'project-item-select',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
                            ],
                            'addon' => [
                                'prepend' => $billProductProject->getModalButtonContent([
                                    'formId' => $form->id,
                                    'controller' => 'project',
                                ]),
                            ],
                        ])->label(false);
                    endif; ?>
                    
                    <?= $form->field($billProduct, "[{$index}]comment", [    
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                        'options' => [
                            //'id' => "product-{$index}-comment",
                            'class' => 'form-group',
                            'style' => 'margin-top: 0px;',
                        ],
                    ])->textarea(['rows' => 1, 'maxlength' => 255, 'style' => 'resize: vertical;', 'placeholder' => Yii::t('common', 'Comment')])->label(false); ?>
                    
                </td>
                <td>
                    <?= $form->field($billProduct, "[{$index}]measure_id", [    
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                        'options' => [
                            'id' => "product-{$index}-measure_id",
                            'class' => 'form-group',
                            'style' => 'text-align: center;',
                        ],
                    ])->widget(Select2::class, [
                        'data' => $measureList, 
                        'options' => [
                            'placeholder' => '...',
                            'class' => 'measure-item-select',
                            'value' => (!empty($billProduct->measure_id) ? $billProduct->measure_id : (isset($billProduct->product) ? $billProduct->product->measure_id : null)),
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
                        ],
                        'addon' => [
                            'prepend' => $billProduct->getModalButtonContent([
                                'formId' => $form->id,
                                'controller' => 'measure',
                            ]),
                        ],                          
                    ])->label(false); ?>
                </td>
                <td>
                    <?= $form->field($billProduct, "[{$index}]amount", [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                    ])->widget(MaskedInput::class, [
                        //'mask' => '9{1,10}[.9{1,3}]',
                        'options' => [
                            'id' => "product-{$index}-amount",
                            'class' => 'form-control number-field bill-product-amount',
                            'style' => 'text-align: right;',
                        ],
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'rightAlign' => false,
                            'digits' => 3,
                            'allowMinus' => false,
                        ],                                    
                    ])->label(false); ?>
                </td>
                <td>
                    <?= $form->field($billProduct, "[{$index}]price", [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                    ])->widget(MaskedInput::class, [
                        //'mask' => '[-]9{1,10}[.9{1,3}]',
                        'options' => [
                            'id' => "product-{$index}-price",
                            'class' => 'form-control number-field bill-product-price',
                            'style' => 'text-align: right;',
                        ],
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'rightAlign' => false,
                            'digits' => 3,
                            'allowMinus' => true,
                        ],                                    
                    ])->label(false); ?>
                </td>
                <td>
                    <?= $form->field($billProduct, "[{$index}]vat", [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                    ])->widget(MaskedInput::class, [
                        //'mask' => '[-]9{1,10}[.9{1,2}]',
                        'options' => [
                            'id' => "product-{$index}-vat",
                            'class' => 'form-control number-field bill-product-vat',
                            'style' => 'text-align: right;',
                        ],
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'rightAlign' => false,
                            'digits' => 3,
                            'allowMinus' => true,
                        ],                                    
                    ])->label(false); ?>
                </td>
                <td style='text-align: center;'>
                    <?= $form->field($billProduct, "[{$index}]revers", [
                        'template' => '{input}{label}{error}{hint}',
                        'options' => [
                            'id' => "product-{$index}-revers",
                            'class' => 'vat-revers',
                        ],
                        //'labelOptions' => ['class' => 'cbx-label']
                    ])->widget(CheckboxX::class, [
                        //'autoLabel'=>false,
                        'readonly' => true,
                        'pluginOptions' => ['threeState' => false],
                    ])->label(false); ?>
                </td>
                <td>
                    <?= $form->field($billProduct, "[{$index}]summa", [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                    ])->widget(MaskedInput::class, [
                        'options' => [
                            'id' => "product-{$index}-summa",
                            'class' => 'form-control number-field bill-product-summa',
                            'style' => 'text-align: right;',
                            'value' => (!empty($billProduct->product_id) || !empty($billProduct->product_name) ? $billProduct->summa : ''),
                        ],
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'rightAlign' => false,
                            'digits' => 2,
                            'allowMinus' => true,
                        ],                                    
                    ])/*->textInput([
                        'id' => "product-{$index}-summa",
                        'class' => 'bill-product-summa', 
                        //'readonly' => true, 
                        'style' => 'text-align: right;',
                        'value' => (!empty($billProduct->product_id) || !empty($billProduct->product_name) ? $billProduct->summa : ''),
                    ])
                     * 
                     */->label(false); ?>
                </td>
                <td>
                    <?= $form->field($billProduct, "[{$index}]summa_vat", [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                    ])->widget(MaskedInput::class, [
                        'options' => [
                            'id' => "product-{$index}-summa_vat",
                            'class' => 'form-control number-field bill-product-summa_vat',
                            'style' => 'text-align: right;',
                            'value' => (!empty($billProduct->product_id) || !empty($billProduct->product_name) ? $billProduct->summa_vat : ''),
                        ],
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'rightAlign' => false,
                            'digits' => 2,
                            'allowMinus' => true,
                        ],                                    
                    ])/*->textInput([
                        'id' => "product-{$index}-summa_vat",
                        'class' => 'bill-product-summa_vat', 
                        //'readonly' => true, 
                        'style' => 'text-align: right;',
                        'value' => (!empty($billProduct->product_id) || !empty($billProduct->product_name) ? $billProduct->summa_vat : ''),
                    ])
                     * 
                     */->label(false); ?>
                </td>
                <td>
                    <?= $form->field($billProduct, "[{$index}]total", [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                    ])->textInput([
                        'id' => "product-{$index}-total",
                        'class' => 'bill-product-total', 
                        'readonly' => true, 
                        'style' => 'text-align: right;',
                        'value' => (!empty($billProduct->product_id) || !empty($billProduct->product_name) ? $billProduct->total : ''),
                    ])/*->widget(MaskedInput::class, [
                        //'mask' => '[-]9{1,10}[.9{1,2}]',
                        'options' => [
                            'id' => "product-{$index}-total",
                            'class' => 'form-control number-field bill-product-total',
                            'style' => 'text-align: right;',
                        ],
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'rightAlign' => false,
                            'digits' => 3,
                            'allowMinus' => true,
                        ],                                    
                    ])
                     * 
                     */->label(false); ?>
                </td>
                <td class="text-center vcenter">
                    <button type="button" class="delete-item btn btn-danger btn-xs"><?= Html::icon('minus');?></button>
                </td>
            </tr>

        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="10"><button type="button" class="add-item btn btn-success btn-sm"><?= Html::icon('plus');?> <?= Yii::t('bill', 'Add product'); ?></button></td>
        </tr>
    </tfoot>
</table>

<?php DynamicFormWidget::end(); ?>

<?php
    $body = ob_get_contents();
    ob_get_clean(); 

    $panelContent = [
        'heading' => BillProduct::modelTitle(2),
        'preBody' => '<div class="panel-body">',
        'body' => $body,
        'postBody' => '</div>',
    ];
    echo FSMHtml::panel(
        $panelContent,
        'default', 
        [
            'id' => "panel-product-data",
        ]
    );
?>