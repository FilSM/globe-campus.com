<?php
//namespace common\models;

use yii\helpers\Url;

use kartik\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

use common\models\bill\Bill;
use common\components\FSMAccessHelper;
use common\components\FSMExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel common\models\bill\search\BillSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$exportMode = !empty($_POST['export_type']);
?>
<div id="bill-index">

    <?= Html::tag('H1', Html::tag('small', $searchModel->modelTitle(2)));?>
    
    <?php
        $columns = [
            //['class' => '\kartik\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'width' => '75px',
                'hAlign' => 'center',
                'pageSummary' => $searchModel->getAttributeLabel('total'),
            ],
            [
                'attribute'=>'doc_type',
                'headerOptions' => ['class'=>'td-mw-100'],
                'value' => function ($model) {
                    return isset($model->doc_type) ? $model->billDocTypeList[$model->doc_type] : null;
                },                         
                'format'=>'raw',
                'visible' => $exportMode,                        
            ],                      
            [
                'attribute'=>'doc_number',
                'headerOptions' => ['class'=>'td-mw-100'],
                'value' => function ($model) {
                    return !empty($model->doc_number) ? $model->doc_number : null;
                },                         
                'format'=>'raw',
                'visible' => $exportMode,                        
            ],                      
            [
                'attribute'=>'doc_number',
                'headerOptions' => ['class'=>'td-mw-100'],
                'value' => function ($model) {
                    return !empty($model->doc_number) ? 
                        (isset($model->doc_type) ? 
                            $model->billDocTypeList[$model->doc_type].'<br/>' : null).
                            (FSMAccessHelper::can('viewBill', $model) ?
                                Html::a($model->doc_number, ['/bill/view', 'id' => $model->id], ['target' => '_blank', 'data-pjax' => 0]) :
                                $model->doc_number
                            )
                        : null;
                },                         
                'format'=>'raw',
                'hiddenFromExport' => true,                        
            ],                      
            [
                'attribute' => 'status',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) {
                    return $model->statusHTML;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $searchModel->billStatusList,
                'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                'filterInputOptions' => [
                    'placeholder' => '...',
                    //'multiple' => true,
                ],
                'format'=>'raw',
            ],                         
            [
                'header' => Yii::t('common', 'Progress'),
                'headerOptions' => ['class'=>'td-mw-125'],
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'mergeHeader' => true,
                'value' => function ($model) {
                    return in_array($model->doc_type, [
                        Bill::BILL_DOC_TYPE_AVANS,
                        Bill::BILL_DOC_TYPE_BILL,
                        Bill::BILL_DOC_TYPE_INVOICE,
                        Bill::BILL_DOC_TYPE_CESSION,
                        ]) ? $model->getProgressButtons() : '&nbsp;';
                },
                'format' => 'raw',
                'visible' => FSMAccessHelper::can('changeBillStatus'),
                'hiddenFromExport' => true,
            ],                        
            [
                'attribute'=>'first_client_role_name',
                'label' => Yii::t('agreement', 'First party role'),
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) {
                    return !empty($model->first_client_role_name) ? $model->first_client_role_name : '';
                },
                'visible' => $exportMode,
            ],                          
            [
                'attribute'=>'first_client_name',
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) {
                    return !empty($model->first_client_name) ? $model->first_client_name : null;
                },                         
                'format'=>'raw',
                'visible' => $exportMode,
            ],                          
            [
                'attribute'=>'first_client_name',
                'contentOptions' => [
                    'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                ],
                'value' => function ($model) {
                    return    
                    '<div style="overflow-x: auto;">'.
                        (!empty($model->first_client_name) ? 
                        (!empty($model->first_client_role_name) ? $model->first_client_role_name.'<br/>' : '') . 
                        Html::a($model->first_client_name, ['/client/view', 'id' => $model->first_client_id], ['target' => '_blank', 'data-pjax' => 0]) : null).
                    '</div>';
                },                         
                'format'=>'raw',
                'hiddenFromExport' => true,
            ],                          
            [
                'attribute'=>'second_client_role_name',
                'label' => Yii::t('agreement', 'Second party role'),
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) {
                    return !empty($model->second_client_role_name) ? $model->second_client_role_name : '';
                },                         
                'visible' => $exportMode,
            ],       
            [
                'attribute'=>'second_client_name',
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) {
                    return !empty($model->second_client_name) ? $model->second_client_name : null;
                },                         
                'visible' => $exportMode,
            ],       
            [
                'attribute'=>'second_client_name',
                'contentOptions' => [
                    'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                ],
                'value' => function ($model) {
                    return   
                    '<div style="overflow-x: auto;">'.
                        (!empty($model->second_client_name) ?  
                        (!empty($model->second_client_role_name) ? $model->second_client_role_name.'<br/>' : '') . 
                        Html::a($model->second_client_name, ['/client/view', 'id' => $model->second_client_id], ['target' => '_blank', 'data-pjax' => 0]) : null).
                    '</div>';
                },                         
                'format'=>'raw',
                'hiddenFromExport' => true,
            ],       
            [
                'attribute' => 'total_eur',
                'hAlign' => 'right',
                'headerOptions' => ['style'=>'text-align: center;'],
                'mergeHeader' => true,
                'value' => function ($model) use ($tillDate) {
                    return isset($model->total_eur) ? ($model->total_eur - $model->getBillPaidSummaToDate(['from' => '2010-01-01', 'till' => $tillDate])) : null;
                },
                'xlFormat' => '###0.00',
                'exportMenuStyle' => [
                    'numberFormat' => ['formatCode' => '###0.00'],
                ],                            
                'format' => ['decimal', 2],
                'pageSummary' => true,                 
            ],
            [
                'attribute' => 'doc_date',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class'=>'td-mw-100'],
                'value' => function ($model) {
                    return isset($model->doc_date) && ($model->doc_date == date('Y-m-d')) ? 
                        Html::a(Html::badge(Yii::t('common', 'Today'), ['class' => 'badge-success']), 
                            Url::to(['/bill/index']).'?BillSearch[doc_date]='.$model->doc_date, 
                            ['target' => '_blank', 'data-pjax' => 0]) 
                        : date('d-M-Y', strtotime($model->doc_date));
                },
                'format'=>'raw',        
            ],                         
            [
                'attribute' => 'pay_date',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class'=>'td-mw-100'],
                'value' => function ($model) {
                    $class = 'badge-success';
                    if(in_array($model->status, [
                        Bill::BILL_STATUS_CANCELED,
                        Bill::BILL_STATUS_COMPLETE,
                        ]))
                    {
                        $badge = '';
                    }else{
                        $firstDate = new DateTime(date('Y-m-d'));
                        $secondDate = new DateTime($model->pay_date);
                        $dateDiff = date_diff($firstDate, $secondDate, true)->days;

                        $firstDate = $firstDate->format('Y-m-d');
                        $secondDate = $secondDate->format('Y-m-d');
                        if($firstDate > $secondDate){
                            $dateDiff = -1 * $dateDiff;
                            $class = 'badge-danger';
                        }elseif($dateDiff >= 0){
                            $class = 'badge-success';
                        }

                        $badge = isset($model->pay_date) && ($model->pay_date == date('Y-m-d')) ? 
                            '' : Html::badge($dateDiff, ['class' => $class]);
                    }
                    return isset($model->pay_date) && ($model->pay_date == date('Y-m-d')) ? 
                        Html::a(Html::badge(Yii::t('common', 'Today'), ['class' => $class]) , 
                            Url::to(['/bill/index']).'?BillSearch[pay_date]='.$model->pay_date, 
                            ['target' => '_blank', 'data-pjax' => 0]) 
                        : date('d-M-Y', strtotime($model->pay_date)).(!empty($badge) ? '<br/>&nbsp;' : '').$badge;
                },
                'format'=>'raw',
            ],              
            [
                'attribute' => 'paid_date',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class'=>'td-mw-100'],
                'value' => function ($model) {
                    return isset($model->paid_date) ? date('d-M-Y', strtotime($model->paid_date)) : null;
                },
            ],                                          
            [
                'attribute' => 'pay_status',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) {
                    return $model->payStatusHTML;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $searchModel->billPayStatusList,
                'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                'filterInputOptions' => [
                    'placeholder' => '...',
                    //'multiple' => true,
                ],
                'format'=>'raw',
            ],     
            [
                'class' => '\common\components\FSMActionColumn',
                'header' => Yii::t('common', 'Options'),
                'headerOptions' => ['class'=>'td-mw-125'],
                'dropdown' => true,
                'isDropdownActionColumn' => true,
                'dropdownDefaultBtn' => 'pay',
                'template' => '{pay} {write-on-basis} {credit-invoice} {mutual-settlement} {cession} {debt-relief} {cancel}',
                'buttons' => $searchModel->getOptionsActionButtons('xs'),
            ],      
            [
                'attribute' => "deleted",   
                'vAlign' => 'middle',
                'class' => '\kartik\grid\BooleanColumn',
                'trueLabel' => 'Yes', 
                'falseLabel' => 'No',
                'width' => '100px',
                'visible' => $isAdmin,
                'hiddenFromExport' => true,
            ],
            [
                'class' => '\common\components\FSMActionColumn',
                'headerOptions' => ['class'=>'td-mw-125'],
                'dropdown' => true,
                'dropdownDefaultBtn' => 'view-attachment',
                'checkPermission' => true,
                'template' => !empty(Yii::$app->params['ENABLE_BILL_TRANSFER']) ?
                    '{view-pdf} {view-attachment} {ajax-add-attachment} {view} {send-other-abonent} {receive-other-abonent} {send-mail-client} {send-mail-agent} {copy} {update} {delete}' :
                    '{view-pdf} {view-attachment} {ajax-add-attachment} {view} {send-mail-client} {send-mail-agent} {copy} {update} {delete}',
                'buttons' => [
                    'ajax-add-attachment' => function (array $params) { 
                        return Bill::getButtonAddAttachment($params);
                    },
                    'view-attachment' => function (array $params) { 
                        return Bill::getButtonViewAttachment($params);
                    },
                    'view-pdf' => function (array $params) { 
                        return Bill::getButtonPrint($params);
                    },
                    'send-other-abonent' => function (array $params) { 
                        return Bill::getButtonSendOtherAbonent($params);
                    },
                    'receive-other-abonent' => function (array $params) { 
                        return Bill::getButtonReceiveOtherAbonent($params);
                    },
                    'send-mail-client' => function (array $params) { 
                        return Bill::getButtonSendMailClient($params);
                    },
                    'send-mail-agent' => function (array $params) { 
                        return Bill::getButtonSendMailAgent($params);
                    },
                    'copy' => function (array $params) { 
                        return Bill::getButtonCopy($params);
                    },
                ],
                'controller' => 'bill',
            ],
        ];
    ?>

    <?= FSMExportMenu::widget([
        'options' => ['id' => 'bill-index-menu'],
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'showColumnSelector' => false,
        'showConfirmAlert' => false,
        'dropdownOptions' => [
            'label' => Yii::t('kvgrid', 'Export'),
            'class' => 'btn btn-default'
        ],
        'target' => ExportMenu::TARGET_SELF,
        'exportConfig' => [
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_HTML => false,
            ExportMenu::FORMAT_CSV => false,
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_PDF => false,
            ExportMenu::FORMAT_EXCEL => false,
            ExportMenu::FORMAT_EXCEL_X => ['label' => Yii::t('kvgrid', 'Excel'), 'alertMsg' => ''],
        ],
        'pjaxContainerId' => 'bill-index',
        'exportFormView' => '@vendor/kartik-v/yii2-export/views/_form',
    ]);?> 

    <p/>

    <?= GridView::widget([
        'id' => 'grid-view',
        'tableOptions' => [
            'id' => 'bill-list',
        ],
        'responsive' => false,
        'striped' => true,
        'hover' => true,
        'bordered' => true,
        'condensed' => true,
        'persistResize' => false,
        'floatHeader' => true,
        'autoXlFormat' => true,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showPageSummary' => true,
        'pjax' => true,
        'columns' => $columns,
        'pjaxSettings' => ['options' => ['id' => 'bill-index']],
    ]);
    ?>
  
</div>