<?php

use yii\helpers\Url;

use kartik\helpers\Html;
use kartik\detail\DetailView;
use kartik\tabs\TabsX;

use common\components\FSMAccessHelper;
use common\components\FSMHelper;
use common\models\bill\Bill;

/* @var $this yii\web\View */
/* @var $model common\models\bill\Bill */

$this->title = $model->billDocTypeList[$model->doc_type] .' #'. $model->doc_number;
if(FSMAccessHelper::checkRoute('/bill/index')){
    $this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
}
$this->params['breadcrumbs'][] = $this->title;

$class = $model->delayed ? 'badge-danger' : $model->payStatusBackgroundColor;
$payClass = $model->delayed ? 'badge-danger' : $model->payStatusBackgroundColor;
$payStatus = $model->payStatusHTML;
$payStatus = !empty($payStatus) ? $payStatus : 
    (isset($model->pay_status) ? Html::badge($model->billPayStatusList[$model->pay_status], ['class' => $payClass.' pay-status-badge']) : null);
?>
<div class="bill-view">
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= Html::pageHeader(Html::encode($this->title)); ?>

    <div class='col-md-12' style="margin-bottom: 5px;">                     
        <div class='col-xs-3' style="padding: 0;">
            <?php if(FSMAccessHelper::can('updateBill', $model) && $model->canUpdate()): ?>
            <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?php endif; ?>
            <?php if(FSMAccessHelper::can('deleteBill', $model) && $model->canDelete()): ?>
            <?= \common\components\FSMBtnDialog::button(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
                'id' => 'btn-dialog-selected',
                'class' => 'btn btn-danger',
            ]); ?> 
            <?php endif; ?>
        </div>

        <div class='col-xs-offset-3' style="padding: 0; text-align: right;">
            <?= $model->getProgressButtons('', true);?>
            <?= $model->getOptionsButtons('', true);?>
            <?php if(FSMAccessHelper::can('viewBillPDF', $model) &&
                in_array($model->doc_type, [
                    Bill::BILL_DOC_TYPE_BILL,
                    Bill::BILL_DOC_TYPE_INVOICE,
                    Bill::BILL_DOC_TYPE_CRBILL,
                    Bill::BILL_DOC_TYPE_CESSION,
                ])
            ) : 
                    $class = !$model->e_signing ? 'success' : 'warning';
                    echo FSMHelper::aButton($model->id, [
                    'label' => Yii::t('common', 'Print'),
                    'icon' => 'print',
                    'action' => 'view-pdf',
                    'class' => $class,
                    'options' => [
                        'target' => '_blank',
                    ],
                ]);
            endif;?>
            <?php if(FSMAccessHelper::can('viewBillActPDF', $model) &&
                in_array($model->doc_type, [
                    Bill::BILL_DOC_TYPE_BILL,
                    Bill::BILL_DOC_TYPE_INVOICE,
                    Bill::BILL_DOC_TYPE_CRBILL,
                    Bill::BILL_DOC_TYPE_CESSION,
                ]) &&
                !empty($model->has_act)   
            ) : 
                    echo FSMHelper::aButton($model->id, [
                    'label' => Yii::t('common', 'Print act'),
                    'icon' => 'print',
                    'action' => 'view-act-pdf',
                    'class' => 'warning',
                    'options' => [
                        'target' => '_blank',
                    ],
                ]);
            endif;?>
            <?php
                $params = [
                    'url' => Url::to(['ajax-add-attachment', 'id' => $model->id]), 
                    'model' => $model, 
                    'isBtn' => true,
                ];
                echo Bill::getButtonAddAttachment($params); 
            ?>            
            <?php
                $params = [
                    'url' => Url::to(['send-other-abonent', 'id' => $model->id]), 
                    'model' => $model, 
                    'isBtn' => true,
                ];
                echo Bill::getButtonSendOtherAbonent($params); 
            ?>            
            <?php
                $params = [
                    'url' => Url::to(['receive-other-abonent', 'id' => $model->id]), 
                    'model' => $model, 
                    'isBtn' => true,
                ];
                echo Bill::getButtonReceiveOtherAbonent($params); 
            ?>            
            <?php
                $params = [
                    'url' => Url::to(['send-mail-client', 'id' => $model->id]), 
                    'model' => $model, 
                    'isBtn' => true,
                ];
                echo Bill::getButtonSendMailClient($params); 
            ?>
            <?php
                $params = [
                    'url' => Url::to(['send-mail-agent', 'id' => $model->id]), 
                    'model' => $model, 
                    'isBtn' => true,
                ];
                echo Bill::getButtonSendMailAgent($params); 
            ?>
            <?php
                $params = [
                    'url' => Url::to(['block', 'id' => $model->id]), 
                    'model' => $model, 
                    'isBtn' => true,
                ];
                echo Bill::getButtonBlock($params); 
            ?>
        </div>
    </div>

    <div class='col-md-12'>
        <?php 
            $attributes = [
                [
                    'attribute' => 'id',
                    'visible' => $isAdmin,
                ], 
                [
                    'attribute' => 'doc_type',
                    'value' => isset($model->doc_type) ? $model->billDocTypeList[$model->doc_type] : null,
                ], 
                [
                    'attribute' => 'doc_number',
                    'valueColOptions' => ['style' => 'font-weight: bold; font-size: 150%;'],
                    'format'=>'raw',
                ],                         
                [
                    'attribute' => 'doc_date',
                    'value' => isset($model->doc_date) && ($model->doc_date == date('Y-m-d')) ? Yii::t('common', 'Today') : date('d-M-Y', strtotime($model->doc_date)),
                    'valueColOptions' => ['style' => 'font-weight: bold; font-size: 150%;'],
                    'format'=>'raw',
                ],                         
                [
                    'attribute' => 'status',
                    'value' => isset($model->status) ? $model->statusHTML : null,
                    'format'=>'raw',
                ],                 
                [
                    'attribute' => 'mail_status',
                    'value' => isset($model->mail_status) ? Html::badge($model->billMailStatusList[$model->mail_status], [
                        'class' => 'status-badge',
                        'style' => ['background-color' => (!empty($model->mail_status) ? ($model->mail_status == BILL::BILL_MAIL_STATUS_SEND ? '#5cb85c' : '#d9534f') : '#777')]
                    ]) : null,
                    'format'=>'raw',
                    'visible' => !empty($model->mail_status),
                ],                 
                [
                    'attribute' => 'transfer_status',
                    'value' => isset($model->transfer_status) ? Html::badge($model->billTransferStatusList[$model->transfer_status], [
                        'class' => 'status-badge',
                        'style' => ['background-color' => (!empty($model->transfer_status) ? ($model->transfer_status == BILL::BILL_TRANSFER_STATUS_SENT ? '#5cb85c' : '#d9534f') : '#777')]
                    ]) : null,
                    'format'=>'raw',
                    'visible' => !empty($model->transfer_status),
                ],                 
                [
                    'attribute' => 'loading_address',
                    'visible' => !empty($model->loading_address),
                ], 
                [
                    'attribute' => 'unloading_address',
                    'visible' => !empty($model->unloading_address),
                ], 
                [
                    'attribute' => 'carrier',
                    'visible' => !empty($model->carrier),
                ], 
                [
                    'attribute' => 'transport',
                    'visible' => !empty($model->transport),
                ], 
            ]
        ?>
        <?php
            $panelContent = [
                'heading' => Yii::t('common', 'Common data'),
                'preBody' => '<div class="list-group-item">',
                'body' => DetailView::widget([
                    'model' => $model,
                    'attributes' => $attributes,
                ]),
                'postBody' => '</div>',
            ];                        
            echo Html::panel(
                $panelContent, 
                'primary', 
                [
                    'id' => "common-data",
                ]
            );                        
        ?>   
    </div>
        
    <div class='col-md-12'>
        <div class='col-md-6' style="padding-left: 0;">
            <?php 
                $attributes = [
                    [
                        'label' => Yii::t('client', 'Full name'),
                        'value' => !empty($firstClientModel->id) ? 
                            (!empty($agreementModel->first_client_role_id) ? $model->firstClientRole->name.': ' : '') . 
                            (FSMAccessHelper::can('viewClient', $firstClientModel) ?
                                Html::a($firstClientModel->name, ['/client/view', 'id' => $firstClientModel->id], ['target' => '_blank']) :
                                $firstClientModel->name
                            )
                             : null,
                        'format'=>'raw',
                        'valueColOptions' => ['style' => 'font-weight: bold; font-size: 150%;'],
                    ],
                    [
                        'label' => Yii::t('client', 'Reg.number'),
                        'value' => !empty($firstClientModel->id) ? $firstClientModel->reg_number : null,
                    ],
                    [
                        'label' => Yii::t('client', 'VAT number'),
                        'value' => !empty($firstClientModel->id) ? $firstClientModel->vat_number : null,
                    ],
                    [
                        'label' => Yii::t('client', 'Legal address'),
                        'value' => !empty($firstClientModel->id) ? $firstClientModel->fullLegalAddress : null,
                    ],
                    [
                        'label' => Yii::t('bill', 'Bank account'),
                        'attribute' => 'first_client_bank_id',
                        'value' => !empty($model->first_client_bank_id) ? 
                            $model->firstClientBank->bank->name . ' | ' . $model->firstClientBank->account . (!empty($model->firstClientBank->name) ? ' ( '.$model->firstClientBank->name . ' )' : '') :
                            null,
                    ],
                ]
            ?>
            <?php
                $panelContent = [
                    'heading' => Yii::t('bill', 'First party data'),
                    'preBody' => '<div class="list-group-item">',
                    'body' => DetailView::widget([
                        'model' => $model,
                        'attributes' => $attributes,
                    ]),
                    'postBody' => '</div>',
                ];                        
                echo Html::panel(
                    $panelContent, 
                    'success', 
                    [
                        'id' => "first-party-data",
                    ]
                );                        
            ?>   
        </div>
        
        <div class='col-md-6' style="padding-right: 0;">
            <?php 
                $attributes = [
                    [
                        'label' => Yii::t('client', 'Full name'),
                        'value' => !empty($secondClientModel->id) ? 
                            (!empty($agreementModel->second_client_role_id) ? $model->secondClientRole->name.': ' : '') . 
                            (FSMAccessHelper::can('viewClient', $secondClientModel) ?
                                Html::a($secondClientModel->name, ['/client/view', 'id' => $secondClientModel->id], ['target' => '_blank']) :
                                $secondClientModel->name
                            )
                            : null,
                        'format'=>'raw',
                        'valueColOptions' => ['style' => 'font-weight: bold; font-size: 150%;'],
                    ],
                    [
                        'label' => Yii::t('client', 'Reg.number'),
                        'value' => !empty($secondClientModel->id) ? $secondClientModel->reg_number : null,
                    ],
                    [
                        'label' => Yii::t('client', 'VAT number'),
                        'value' => !empty($secondClientModel->id) ? $secondClientModel->vat_number : null,
                    ],
                    [
                        'label' => Yii::t('client', 'Legal address'),
                        'value' => !empty($secondClientModel->id) ? $secondClientModel->fullLegalAddress : null,
                    ],
                    [
                        'attribute' => 'second_client_bank_id',
                        'label' => Yii::t('bill', 'Bank account'),
                        'value' => !empty($model->second_client_bank_id) ? 
                            $model->secondClientBank->bank->name . ' | ' . $model->secondClientBank->account . (!empty($model->secondClientBank->name) ? ' ( '.$model->secondClientBank->name . ' )' : '') :
                            null,
                    ],
                ]
            ?>
            <?php
                $panelContent = [
                    'heading' => Yii::t('bill', 'Second party data'),
                    'preBody' => '<div class="list-group-item">',
                    'body' => DetailView::widget([
                        'model' => $model,
                        'attributes' => $attributes,
                    ]),
                    'postBody' => '</div>',
                ];                        
                echo Html::panel(
                    $panelContent, 
                    'success', 
                    [
                        'id' => "second-party-data",
                    ]
                );                        
            ?>   
        </div>
    </div>
    
    <div class='col-md-12'>
        <div class='col-md-6' style="padding-left: 0;">
            <?php 
                $firstDate = new DateTime(date('Y-m-d'));
                $secondDate = new DateTime($model->pay_date);
                $dateDiff = date_diff($firstDate, $secondDate, true)->days;

                $firstDate = $firstDate->format('Y-m-d');
                $secondDate = $secondDate->format('Y-m-d');
                if($firstDate > $secondDate){
                    //$dateDiff = -1 * $dateDiff;
                    $class = 'badge-danger';
                    $dateDiffTxt = Yii::t('bill', 'delayed {count, plural, =1{one day} other{# days}}', ['count' => $dateDiff]);
                }elseif($dateDiff >= 0){
                    $class = 'badge-success';
                    $dateDiffTxt = Yii::t('bill', 'after {count, plural, =1{one day} other{# days}}', ['count' => $dateDiff]);
                }

                $attributes = [
                    [
                        'attribute' => 'pay_date',
                        'value' => isset($model->pay_date) && ($model->pay_date == date('Y-m-d')) ? 
                            Html::badge(Yii::t('common', 'Today'), ['class' => $class]) : 
                            date('d-M-Y', strtotime($model->pay_date)).(!in_array($model->pay_status, [Bill::BILL_PAY_STATUS_FULL, Bill::BILL_PAY_STATUS_OVER]) ? '&nbsp;&nbsp;&nbsp;' . Html::badge($dateDiffTxt, ['class' => $class]) : ''),
                        'format' => 'raw',
                    ],     
                    [
                        'attribute' => 'paid_date',
                        'value' => isset($model->paid_date) ? date('d-M-Y', strtotime($model->paid_date)) : null,
                    ],     
                    [
                        'attribute' => 'services_period',
                        'visible' => !empty($model->services_period),
                    ],                         
                    [
                        'attribute' => 'pay_status',
                        'value' => $payStatus,
                        'format' => 'raw',
                    ],     
                    //'delayed:boolean',
                    [
                        'attribute' => 'summa',
                        'value' => number_format($model->summa, 2, '.', ' '),
                    ],                         
                    [
                        'attribute' => 'vat',
                        'value' => number_format($model->vat, 2, '.', ' '),
                    ],     
                    [
                        'attribute' => 'total',
                        'value' => isset($model->valuta_id) ? number_format($model->total, 2, '.', ' ') . ' ' . $model->valuta->name : number_format($model->total, 2, '.', ' '),
                    ],                         
                    [
                        'attribute' => 'total_eur',
                        'value' => number_format($model->total_eur, 2, '.', ' '),
                        'visible' => !isset($model->valuta_id) || (isset($model->valuta_id) && ($model->valuta_id != common\models\Valuta::VALUTA_DEFAULT)),
                    ],                         
                ]
            ?>
            <?php 
                $paymentTableView = '';
                if($paymentDataProvider->count > 0) :
                    $paymentTableView = $this->render('_payment_table_view', [
                        'searchModel' => $paymentModel,
                        'dataProvider' => $paymentDataProvider,
                        'billdModel' => $model,
                    ]);
                endif;
                
                $panelContent = [
                    'heading' => Yii::t('bill', 'Payment data'),
                    'preBody' => '<div class="list-group-item">',
                    'body' => DetailView::widget([
                        'model' => $model,
                        'attributes' => $attributes,
                    ]).
                    $paymentTableView
                    ,
                    'postBody' => '</div>',
                ];                        
                echo Html::panel(
                    $panelContent, 
                    'success', 
                    [
                        'id' => "payment-data",
                    ]
                );                        
            ?>   
        </div>
        <div class='col-md-6' style="padding-right: 0;">
            <?php 
                $attributes = [
                    [
                        'label' => $model->getAttributeLabel('project_id'),
                        //'attribute' => 'project_id',
                        //'value' => !empty($model->project_id) ? Html::a($model->project->name, ['/project/view', 'id' => $model->project_id], ['target' => '_blank']) : null,
                        'value' => (!empty($model->project) ?
                            (FSMAccessHelper::can('viewProject', $model->project) ?
                            Html::a($model->project->name, ['/project/view', 'id' => $model->project->id], ['target' => '_blank']) :
                            $model->project->name) :
                            null
                        ),
                        'format' => 'raw',
                    ], 
                    [
                        'attribute' => 'agreement_id',
                        'value' => !$model->agreement_id ? null : (FSMAccessHelper::can('viewAgreement', $model->agreement) ?
                            Html::a($model->agreement->number, ['agreement/view', 'id' => $model->agreement_id], ['target' => '_blank']) :
                            $model->agreement->number
                        ),
                        'format'=>'raw',
                    ], 
                    [
                        'attribute' => 'parent_id',
                        'value' => !empty($model->parent_id) ? Html::a($model->parent->doc_number, ['/bill/view', 'id' => $model->parent_id], ['target' => '_blank']) : null,
                        'format' => 'raw',
                        'visible' => !empty($model->parent_id),
                    ],            
                    [
                        'attribute' => 'child_id',
                        'value' => !empty($model->child_id) ? Html::a($model->child->doc_number, ['/bill/view', 'id' => $model->child_id], ['target' => '_blank']) : null,
                        'format' => 'raw',
                        'visible' => !empty($model->child_id),
                    ],  
                    /*
                    [
                        'attribute' => 'first_account_id',
                        'value' => !empty($model->first_account_id) ? Html::a($model->firstAccount->name, ['/account-map/view', 'id' => $model->first_account_id], ['target' => '_blank']) : null,
                        'format' => 'raw',
                        'visible' => !empty($model->first_account_id),
                    ],        
                    [
                        'attribute' => 'second_account_id',
                        'value' => !empty($model->second_account_id) ? Html::a($model->secondAccount->name, ['/account-map/view', 'id' => $model->second_account_id], ['target' => '_blank']) : null,
                        'format' => 'raw',
                        'visible' => !empty($model->second_account_id),
                    ],     
                     * 
                     */   
                    /*
                    [
                        'attribute' => 'valuta_id',
                        'value' => isset($model->valuta_id) ? $model->valuta->name : null,
                    ],    
                     * 
                     */                     
                    [
                        'attribute' => 'manager_id',
                        'value' => isset($model->manager_id, $model->author) ? Html::a($model->author->name, ['/user/profile/show', 'id' => (isset($model->author->user) ? $model->author->user->id : null)], ['target' => '_blank']) : null,
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'language_id',
                        'value' => !empty($model->language_id) ? $model->language->name : null,
                    ],
                    'comment:ntext',
                    [
                        'attribute' => 'not_expenses',
                        'format' => 'boolean',
                    ],
                    [
                        'attribute' => 'deleted',
                        'format' => 'boolean',
                        'visible' => $isAdmin,
                    ],
                ]
            ?>
            <?php
                $panelContent = [
                    'heading' => Yii::t('common', 'Other data'),
                    'preBody' => '<div class="list-group-item">',
                    'body' => DetailView::widget([
                        'model' => $model,
                        'attributes' => $attributes,
                    ]),
                    'postBody' => '</div>',
                ];                        
                echo Html::panel(
                    $panelContent, 
                    'success', 
                    [
                        'id' => "other-data",
                    ]
                );                        
            ?>   
        </div>
    </div>
    
    <div class='col-md-12'>
        <?php
            $tabItems = [];

            $tabItems[] = [
                'label' => \common\models\bill\BillProduct::modelTitle(2),
                'content' => $this->render('_product_table_view', [
                    'model' => $billProductModel,
                    'bill' => $model,
                ]),
                'active' => 1,
            ];
            if(count($billProductProjectModel) > 0) :
                $tabItems[] = [
                    'label' => common\models\bill\BillProductProject::modelTitle(2),
                    'content' => $this->render('_project_table_view', [
                        'billProductModel' => $billProductModel,
                        'billProductProjectModel' => $billProductProjectModel,
                        'bill' => $model,
                    ]),
                ];
            endif;

            $tabItems[] = [
                'label' => \common\models\bill\BillHistory::modelTitle(),
                'content' => $this->render('_history_table_view', [
                    'searchModel' => $historyModel,
                    'dataProvider' => $historyDataProvider,
                ]),
            ];

            if(!empty($filesModel[0]->id)) :
                $tabItems[] = [
                    'label' => common\models\bill\BillAttachment::modelTitle(2),
                    'content' => $this->render('_attachment_table', [
                        'model' => $filesModel,
                    ]),
                ];
            endif;

            echo TabsX::widget([
                'items' => $tabItems,
                'position' => TabsX::POS_BELOW,
                'align' => TabsX::ALIGN_LEFT,
                'bordered' => true,
                'encodeLabels' => false,
                'containerOptions' => [
                    'style' => 'padding-bottom: 10px;',
                ],
            ]);
        ?>
    </div>

</div>
