<?php

use yii\helpers\Url;
use kartik\helpers\Html;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;

use common\components\FSMHtml;
?>

<?php

ob_start();
ob_implicit_flush(false);
?>

<table class="table table-bordered table-striped margin-b-none">
    <thead>
        <tr>
            <th class="required"><?= Yii::t('client', 'Signing person'); ?></th>
        </tr>
    </thead>
    <tbody class="form-persons-body">
        <tr class="form-persons-item">
            <td>
                <?php 
                    $billPerson = $model[0]; 
                    $className = substr(strrchr(get_class($billPerson), "\\"), 1);
                ?>
                <?= Html::activeHiddenInput($billPerson, 'id', [
                    'id' => strtolower($className).'-0', 
                    'name' => $className.'[0][id]', 
                    'value' => $billPerson->id,
                ]); ?>
                <?= $form->field($billPerson, "[0]client_person_id", [
                    'horizontalCssClasses' => [
                        'wrapper' => 'col-md-12',
                    ],
                ])->widget(DepDrop::class, [
                    'type' => DepDrop::TYPE_SELECT2,
                    'data' => empty($billPerson->client_person_id) || empty($billPerson->clientPerson) ? null :
                            [$billPerson->client_person_id => $billPerson->clientPerson->first_name . ' ' . $billPerson->clientPerson->last_name .
                        (!empty($billPerson->clientPerson->position_id) ? ' ( ' . $billPerson->clientPerson->position->name . ' )' : '')],
                    'options' => [
                        'class' => 'person-item-select',
                    ],                        
                    'select2Options' => [
                        'pluginOptions' => [
                            'allowClear' => !$billPerson->isAttributeRequired('client_person_id'),
                        ],
                    ],
                    'pluginOptions' => [
                        'depends' => [$depends],
                        //'initDepends' => [$depends],
                        //'initialize' => !empty($billPerson->client_person_id) && empty($model[1]->id) && ($depends == 'second-client-id'),
                        'url' => Url::to(['/client/ajax-get-client-person-list', 'can_sign' => true, 'doc_date' => ($docDate ?? null)]),
                        'placeholder' => '...',
                    ]
                ])->label(false);
                ?>
            </td>
        </tr>
        
        <tr class="form-persons-item">
            <td>
                <?php 
                    $billPerson = $model[1]; 
                    $className = substr(strrchr(get_class($billPerson), "\\"), 1);
                ?>
                <?= Html::activeHiddenInput($billPerson, 'id', [
                    'id' => strtolower($className).'-1', 
                    'name' => $className.'[1][id]', 
                    'value' => $billPerson->id,
                ]); ?>
                <?= $form->field($billPerson, "[1]client_person_id", [
                    'horizontalCssClasses' => [
                        'wrapper' => 'col-md-12',
                    ],
                ])->widget(DepDrop::class, [
                    'type' => DepDrop::TYPE_SELECT2,
                    'data' => empty($billPerson->client_person_id) || empty($billPerson->clientPerson) ? null :
                            [$billPerson->client_person_id => $billPerson->clientPerson->first_name . ' ' . $billPerson->clientPerson->last_name .
                        (!empty($billPerson->clientPerson->position_id) ? ' ( ' . $billPerson->clientPerson->position->name . ' )' : '')],
                    'options' => [
                        'class' => 'person-item-select',
                    ],                        
                    'select2Options' => [
                        'pluginOptions' => [
                            'allowClear' => !$billPerson->isAttributeRequired('client_person_id'),
                        ],
                    ],
                    'pluginOptions' => [
                        'depends' => [$depends],
                        'initDepends' => [$depends],
                        //'initialize' => !empty($billPerson->client_person_id) && ($depends == 'second-client-id'),
                        'initialize' => true,
                        'url' => Url::to(['/client/ajax-get-client-person-list', 'can_sign' => true, 'doc_date' => ($docDate ?? null)]),
                        'placeholder' => '...',
                    ]
                ])->label(false);
                ?>
            </td>
        </tr>
    </tbody>
</table>

<?php
    $body = ob_get_contents();
    ob_get_clean(); 
    
    //echo $body;

    $panelContent = [
        //'heading' => BillPerson::modelTitle(2),
        'preBody' => '<div class="panel-body">',
        'body' => $body,
        'postBody' => '</div>',
    ];
    echo FSMHtml::panel(
        $panelContent, 
        'default', 
        [
            'id' => $panel_person_id,
            'class' => 'panel-person-data',
        ]
    );
?>