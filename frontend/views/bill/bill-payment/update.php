<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\bill\BillPayment */

$this->title = Yii::t($model->tableName(), 'Update a '.$model->modelTitle(1, false)) . ': #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Edit');

$isModal = !empty($isModal);
?>
<div class="bill-payment-update">

    <?php if(!$isModal): ?>
    <?= Html::pageHeader(Html::encode($this->title));?>
    <?php endif; ?>
    
    <?= $this->render('_form', [
        'model' => $model,
        'historyModel' => $historyModel,
        'paymentOrderList' => $paymentOrderList,
        'bankFromList' => $bankFromList,
        'bankToList' => $bankToList,
        'valutaList' => $valutaList,
        'isAdmin' => !empty($isAdmin),
        'isModal' => $isModal,
    ]) ?>

</div>