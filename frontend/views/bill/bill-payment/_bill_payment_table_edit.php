<?php
use yii\helpers\Url;
//use yii\widgets\MaskedInput;

use kartik\helpers\Html;
use kartik\widgets\Select2;
use kartik\checkbox\CheckboxX;

use common\components\FSMHtml;
use common\widgets\dynamicform\DynamicFormWidget;
use common\widgets\MaskedInput;
?>

<?php 
    ob_start();
    ob_implicit_flush(false);
?>

<?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_wrapper',
    'widgetBody' => '.form-bill-payment-body',
    'widgetItem' => '.form-bill-payment-item',
    'min' => 1,
    'insertButton' => '.add-item',
    'deleteButton' => '.delete-item',
    'model' => $model[0],
    'formId' => $form->id,
    'formFields' => [
        'combine',
        'bill_number',
        'summa',
        'rate',
        'valuta_id',
        'need_agreement',
    ],
]); ?>

<table class="table table-bordered table-striped margin-b-none">
    <thead>
        <tr>
            <th class="kv-all-select kv-align-center kv-align-middle kv-merged-header" style="width: 3%;">
                <?= $model[0]->getAttributeLabel('combine'); ?>
            </th>
            <th style="width: 15%;"><?= $model[0]->getAttributeLabel('bill_number'); ?></th>
            <th style="width: 40%;"><?= $model[0]->getAttributeLabel('first_client_name'); ?></th>
            <th class="required" style="width: 15%; text-align: right;"><?= $model[0]->getAttributeLabel('summa'); ?></th>
            <th class="required" style="width: 15%; text-align: right;"><?= $model[0]->getAttributeLabel('rate'); ?></th>
            <th class="required" style="width: 15%; text-align: right;"><?= $model[0]->getAttributeLabel('valuta_id'); ?></th>
            <th class="kv-all-select kv-align-center kv-align-middle kv-merged-header" style="width: 3%;">
                <?= $model[0]->getAttributeLabel('need_agreement'); ?>
            </th>
            <th style="width: 90px; text-align: center"><?= Yii::t('kvgrid', 'Actions'); ?></th>
        </tr>
    </thead>
    <tbody class="form-bill-payment-body">
        <?php foreach ($model as $index => $billPayment): ?>
            <tr class="form-bill-payment-item">
                <td class="kv-align-center kv-align-middle kv-row-select">
                    <?= $form->field($billPayment, "[{$index}]combine", [
                        'template' => '{input}{label}{error}{hint}',
                        'options' => [
                            'id' => "payment-{$index}-combine",
                            'class' => 'payment-combine',
                        ],
                        //'labelOptions' => ['class' => 'cbx-label']
                    ])->widget(CheckboxX::class, [
                        'pluginOptions' => ['threeState' => false],
                    ])->label(false); ?>
                </td>                
                <td>
                    <?= Html::activeHiddenInput($billPayment, "[{$index}]bill_id", ['value' => $billPayment->bill_id]); ?>
                    <?= $form->field($billPayment, "[{$index}]bill_number", [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                        'staticValue' => (isset($billPayment->bill_id) ? $billPayment->bill_number : ''),
                    ])->staticInput()->label(false); ?>
                </td>
                <td>
                    <?= Html::activeHiddenInput($billPayment, "[{$index}]first_client_id", ['value' => $billPayment->first_client_id]); ?>
                    <?= $form->field($billPayment, "[{$index}]first_client_name", [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                        'staticValue' => (isset($billPayment->first_client_id) ? $billPayment->first_client_name : ''),
                    ])->staticInput()->label(false); ?>
                </td>
                <td>
                    <?= $form->field($billPayment, "[{$index}]summa", [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                        //'template' => '<div class="col-md-12">{input}</div>',
                    ])->widget(MaskedInput::class, [
                        //'mask' => '9{1,10}[.9{1,3}]',
                        'options' => [
                            'id' => "billpayment-{$index}-summa",
                            'class' => 'form-control number-field billpayment-summa',
                            'style' => 'text-align: right;',
                        ],
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'rightAlign' => false,
                            'digits' => 3,
                            'allowMinus' => false,
                        ],                                    
                    ])->label(false); ?>                    
                </td>               
                <td>
                    <?= $form->field($billPayment, "[{$index}]rate", [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                    ])->widget(MaskedInput::class, [
                        //'mask' => '9{1,10}[.9{1,3}]',
                        'options' => [
                            'id' => "billpayment-{$index}-rate",
                            'class' => 'form-control number-field billpayment-rate',
                            'style' => 'text-align: right;',
                            //'value' => (!empty($billPayment->bill_id) ? $billPayment->rate : ''),
                        ],
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'rightAlign' => false,
                            'digits' => 3,
                            'allowMinus' => false,
                        ],                                    
                    ])->label(false); ?>                    
                </td>
                <td>
                    <?= $form->field($billPayment, "[{$index}]valuta_id", [    
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ],
                        'options' => [
                            'class' => 'form-group',
                        ],
                    ])->widget(Select2::class, [
                        'data' => $valutaList, 
                        'options' => [
                            'placeholder' => '...',
                            'id' => "billpayment-{$index}-valuta_id",
                            'class' => 'billpayment-valuta',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false); ?>                    
                </td>                
                <td class="kv-align-center kv-align-middle kv-row-select">
                    <?= $form->field($billPayment, "[{$index}]need_agreement", [
                        'template' => '{input}{label}{error}{hint}',
                        'options' => [
                            'id' => "payment-{$index}-need-agreement",
                            'class' => 'payment-need-agreement',
                        ],
                        //'labelOptions' => ['class' => 'cbx-label']
                    ])->widget(CheckboxX::class, [
                        'pluginOptions' => ['threeState' => false],
                    ])->label(false); ?>
                </td>                
                <td class="text-center vcenter">
                    <button type="button" class="delete-item btn btn-danger btn-xs"><?= Html::icon('minus');?></button>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php DynamicFormWidget::end(); ?>

<?php
    $body = ob_get_contents();
    ob_get_clean(); 

    $panelContent = [
        //'heading' => Yii::t('bill', 'Invoice list'),
        //'preBody' => '<div class="panel-body">',
        'body' => $body,
        //'postBody' => '</div>',
    ];
    echo FSMHtml::panel(
        $panelContent, 
        'default', 
        [
            'id' => "panel-bill-payment-data",
        ]
    );
?>