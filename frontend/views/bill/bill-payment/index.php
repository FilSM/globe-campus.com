<?php
namespace common\models;

use Yii;
//use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\grid\GridView;

use kartik\helpers\Html;
use kartik\grid\GridView;

use common\components\FSMAccessHelper;
use common\models\bill\BillPayment;

/* @var $this yii\web\View */
/* @var $searchModel common\models\bill\search\BillPaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $searchModel->modelTitle(2);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bill-payment-index">

    <?= Html::pageHeader(Html::encode($this->title)); ?>
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <?php 
    $columns = [
        //['class' => '\kartik\grid\SerialColumn'],
        [
            'attribute' => 'id',
            'width' => '75px',
            'hAlign' => 'center',
            //'pageSummary' => $searchModel->getAttributeLabel('total'),
        ],
        [
            'attribute' => 'bill_number',
            'contentOptions' => [
                'style'=>'max-width:100px; min-height:75px; word-wrap: break-word;'
            ],
            'value' => function ($model) {
                return 
                    '<div style="overflow-x: auto;">'.
                    (!empty($model->bill_number) ? (FSMAccessHelper::can('viewBill', $model) ?
                        Html::a($model->bill_number, ['/bill/view', 'id' => $model->bill_id], ['target' => '_blank', 'data-pjax' => 0]) :
                        $model->bill_number
                    )
                    : null).
                    '</div>';
            },                        
            'format' => 'raw',
        ],
        [
            'attribute' => 'payment_order_number',
            'contentOptions' => [
                'style'=>'max-width:100px; min-height:75px; word-wrap: break-word;'
            ],
            'value' => function ($model) {
                return 
                    '<div style="overflow-x: auto;">'.
                    (!empty($model->payment_order_number) ? (FSMAccessHelper::can('viewBill', $model) ?
                        Html::a($model->payment_order_number, ['/payment-order/view', 'id' => $model->payment_order_id], ['target' => '_blank', 'data-pjax' => 0]) :
                        $model->payment_order_number
                    )
                    : null).
                    '</div>';
            },                        
            'format' => 'raw',
        ],
        [
            'attribute'=>'from_bank_id',
            'contentOptions' => [
                'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
            ],
            'value' => function ($model) {
                return 
                    '<div style="overflow-x: auto;">'.
                    (!empty($model->from_bank_name) ? 
                        Html::a($model->from_bank_name, ['/bank/view', 'id' => $model->from_bank_id], ['target' => '_blank', 'data-pjax' => 0])
                        : null
                    ).
                    '</div>';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $bankList,
            'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
            'filterInputOptions' => [
                'id' => 'agrpayment-from_bank_name',
                'placeholder' => '...',
            ],                            
            'format'=>'raw',
        ],                    
        [
            'attribute'=>'to_bank_id',
            'contentOptions' => [
                'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
            ],
            'value' => function ($model) {
                return 
                    '<div style="overflow-x: auto;">'.
                    (!empty($model->to_bank_name) ? 
                        Html::a($model->to_bank_name, ['/bank/view', 'id' => $model->to_bank_id], ['target' => '_blank', 'data-pjax' => 0])
                        : null
                    ).
                    '</div>';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $bankList,
            'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
            'filterInputOptions' => [
                'id' => 'agrpayment-to_bank_name',
                'placeholder' => '...',
            ],                            
            'format'=>'raw',
        ], 
        [
            'attribute' => 'paid_date',
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'width' => '150px',
            'headerOptions' => ['class'=>'td-mw-75'],
            'value' => function ($model) {
                return isset($model->paid_date) ? date('d-M-Y', strtotime($model->paid_date)) : null;
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'pluginOptions' => \yii\helpers\ArrayHelper::merge(
                    Yii::$app->params['DatePickerPluginOptions'], 
                    [
                        'locale' => [
                            'firstDay' => 1,
                            'format' => 'd-m-Y',
                            'cancelLabel' => Yii::t('common', 'Clear'),
                        ],
                        //'opens'=>'left',
                        //'startDate' => date('d-m-Y'),
                    ]),
                'convertFormat' => true,
                //'presetDropdown' => true,
                'hideInput' => true,
                'pluginEvents' => [
                    'cancel.daterangepicker' => 'function() {
                        //console.log("cancel.daterangepicker"); 
                        var gridFilters = $("table tr.filters");
                        var daterangeinput = gridFilters.find("#billpaymentsearch-paid_date");
                        daterangeinput.val("").change();
                    }',
                ],
            ],
            'format'=>'raw',        
        ],                     
        [
            'attribute' => 'confirmed',
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'width' => '150px',
            'headerOptions' => ['class'=>'td-mw-75'],
            'value' => function ($model) {
                return isset($model->confirmed) ? date('d-M-Y', strtotime($model->confirmed)) : null;
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'pluginOptions' => \yii\helpers\ArrayHelper::merge(
                    Yii::$app->params['DatePickerPluginOptions'], 
                    [
                        'locale' => [
                            'firstDay' => 1,
                            'format' => 'd-m-Y',
                            'cancelLabel' => Yii::t('common', 'Clear'),
                        ],
                        //'opens'=>'left',
                        //'startDate' => date('d-m-Y'),
                    ]),
                'convertFormat' => true,
                //'presetDropdown' => true,
                'hideInput' => true,
                'pluginEvents' => [
                    'cancel.daterangepicker' => 'function() {
                        //console.log("cancel.daterangepicker"); 
                        var gridFilters = $("table tr.filters");
                        var daterangeinput = gridFilters.find("#billpaymentsearch-confirmed");
                        daterangeinput.val("").change();
                    }',
                ],
            ],
            'format'=>'raw',        
        ],                     
        [
            'attribute' => 'summa_origin',
            'hAlign' => 'right',
            'headerOptions' => ['style'=>'text-align: center;'],
            'mergeHeader' => true,
            'value' => function ($model) {
                return isset($model->summa_origin) ? $model->summa_origin : null;
            },
            'format' => ['decimal', 2],
            //'pageSummary' => true,                 
        ],                     
        [
            'attribute' => 'summa_eur',
            'hAlign' => 'right',
            'headerOptions' => ['style'=>'text-align: center;'],
            'mergeHeader' => true,
            'value' => function ($model) {
                return isset($model->summa_eur) ? $model->summa_eur : null;
            },
            'format' => ['decimal', 2],
            //'pageSummary' => true,                 
        ],
        [
            'class' => '\common\components\FSMActionColumn',
            'headerOptions' => ['class'=>'td-mw-125'],
            'checkCanDo' => true,
            'dropdown' => true,
            'template' => '{confirm} {view} {update} {delete}',
            'checkCanDo' => true,
            'buttons' => [
                'confirm' => function ($params) { 
                    return BillPayment::getButtonConfirm($params);
                },
                'update' => function ($params) { 
                    return BillPayment::getButtonUpdate($params);
                },
            ], 
        ],
    ];
    ?>
    
    <?= GridView::widget([
        'id' => 'grid-view',
        /*
        'tableOptions' => [
            'id' => 'bill-payment-list',
        ],
         * 
         */
        'responsive' => false,
        'striped' => true,
        'hover' => true,
        'bordered' => true,
        'condensed' => true,
        'persistResize' => false,
        'floatHeader' => true,
        'autoXlFormat' => true,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        //'showPageSummary' => true,
        'pjax' => true,
        'columns' => $columns,
    ]); ?>     
</div>