<?php

use kartik\helpers\Html;
use kartik\detail\DetailView;

use common\components\FSMAccessHelper;

/* @var $this yii\web\View */
/* @var $model common\models\bill\BillPayment */

$this->title = $model->modelTitle() .' #'. $model->id;
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bill-payment-view">

    <?= Html::pageHeader(Html::encode($this->title)); ?>

    <p>
        <?php if($model->canUpdate()){
            echo Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
        } 
        ?>
        <?php if($model->canDelete()){
            echo\common\components\FSMBtnDialog::button(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
                'id' => 'btn-dialog-selected',
                'class' => 'btn btn-danger',
            ]); 
        } 
        ?>   
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'bill_id',
                'value' => !empty($model->bill_id) ? 
                    (FSMAccessHelper::can('viewBill', $model) ?
                        Html::a($model->bill->doc_number, ['/bill/view', 'id' => $model->bill_id], ['target' => '_blank']) : 
                        $model->bill->doc_number
                    )
                    : null,
                'format' => 'raw',
            ],
            [
                'attribute' => 'payment_order_id',
                'value' => !empty($model->payment_order_id) ? 
                    (FSMAccessHelper::can('viewPaymentOrder', $model) ?
                        Html::a($model->paymentOrder->number, ['/payment-order/view', 'id' => $model->payment_order_id], ['target' => '_blank']) : 
                        $model->paymentOrder->number
                    )
                    : null,
                'format' => 'raw',
            ],              
            [
                'attribute' => 'from_bank_id',
                'value' => !empty($model->from_bank_id) ? 
                    Html::a($model->bankFrom->name, ['/bank/view', 'id' => $model->from_bank_id], ['target' => '_blank'])
                    : null,
                'format' => 'raw',
            ],              
            [
                'attribute' => 'to_bank_id',
                'value' => !empty($model->to_bank_id) ? 
                    Html::a($model->bankTo->name, ['/bank/view', 'id' => $model->to_bank_id], ['target' => '_blank'])
                    : null,
                'format' => 'raw',
            ],
            [
                'attribute' => 'paid_date',
                'value' => isset($model->paid_date) ? date('d-M-Y', strtotime($model->paid_date)) : null,
            ],     
            [
                'attribute' => 'summa_eur',
                'value' => isset($model->summa_eur) ? $model->summa_eur : null,
                'format' => ['decimal', 2],
            ],         
            [
                'label' => Yii::t('common', 'Comment'),
                'value' => isset($model->billHistory) ? $model->billHistory->comment : '',
            ],   
        ],
    ]) ?>

</div>