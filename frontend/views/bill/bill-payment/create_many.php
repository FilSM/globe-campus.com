<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\bill\BillPayment */

$this->title = Yii::t($model[0]->tableName(), 'Create a new '.$model[0]->modelTitle(1, false));
$this->params['breadcrumbs'][] = ['label' => $model[0]->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bill-payment-create">

    <?php if(!$isModal): ?>
    <?= Html::pageHeader(Html::encode($this->title));?>
    <?php endif; ?>
    
    <?= $this->render('_form_many', [
        'model' => $model,
        'historyModel' => $historyModel,
        'paymentOrderModel' => $paymentOrderModel,
        'paymentOrderList' => $paymentOrderList,
        'valutaList' => $valutaList,
        'bankList' => $bankList,
        'isAdmin' => $isAdmin,
        'isModal' => $isModal,
    ]) ?>

</div>