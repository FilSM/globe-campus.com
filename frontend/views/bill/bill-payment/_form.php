<?php
namespace common\models;

use Yii;
use yii\widgets\Pjax;
use yii\widgets\MaskedInput;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\bill\BillPayment */
/* @var $form yii\widgets\ActiveForm */
$isModal = !empty($isModal);
?>

<div class="bill-payment-form">
    <?php if($isModal) : Pjax::begin(Yii::$app->params['PjaxModalOptions']); endif; ?>

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => $model->tableName().'-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'options' => [
            'data-pjax' => $isModal,    
        ],           
    ]); ?>

    <?= $form->field($historyModel, 'bill_id', [
        'options' => [
            'id' => 'bill-static-text',
            'class' => 'form-group static-input',
        ],
        'staticValue' => (isset($historyModel->bill_id) ? $historyModel->bill->doc_number : ''),
    ])->staticInput(['class' => 'form-control', 'disabled' => true]); ?>

    <?= $form->field($historyModel, 'create_time', [
        'options' => [
            'id' => 'create-time-static-text',
            'class' => 'form-group static-input',
        ],
        'staticValue' => (isset($historyModel->create_time) ? date('d-M-Y H:i:s', strtotime($historyModel->create_time)) : date('d-M-Y H:i:s')),
    ])->staticInput(['class' => 'form-control', 'disabled' => true]); ?>

    <?= $form->field($historyModel, 'create_user_id', [
        'options' => [
            'id' => 'create-user-static-text',
            'class' => 'form-group static-input',
        ],
        'staticValue' => (isset($historyModel->create_user_id) ? $historyModel->createUserProfile->name : Yii::$app->user->identity->profile->name),
    ])->staticInput(['class' => 'form-control', 'disabled' => true]); ?>

    <?= $form->field($model, 'payment_order_id')->widget(Select2::class, [
            'data' => $paymentOrderList,
            'options' => [
                'id' => 'payment-order-id',
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => !$model->isAttributeRequired('payment_order_id'),
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],            
            'addon' => [
                'prepend' => $model::getModalButtonContent([
                    'formId' => $form->id,
                    'controller' => 'payment-order',
                ]),
            ],
            'pluginEvents' => [
                "change" => "function() {
                    var form = $('#{$model->tableName()}' + '-form');
                    var data = form.data('yiiActiveForm');
                    $.each(data.attributes, function() {
                        this.status = 3;
                    });
                    form.yiiActiveForm('validate');

                    var id = $(this).val();
                    var emptyId = empty(id);
                    
                    if (emptyId){
                        form.find('#paid-date-container').show('slow');
                    }else{
                        form.find('#paid-date-container').hide('slow');
                        form.find('.field-billpayment-paid_date').find('input').val('');
                        form.find('#from-bank-id').val('').change();
                        form.find('#to-bank-id').val('').change();
                    }
                }",
            ],          
        ]); 
    ?>
    
    <div id="paid-date-container">
    <?php if(!is_array($model)) : ?>
    <?= $form->field($model, 'paid_date')->widget(DatePicker::class, [
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
            'options' => [
                'class' => 'rate-changer-date',
                'data-currency_input_id' => 'valuta-id',
                'data-rate_input_id' => 'rate',
            ],
            'pluginEvents' => [
                "change" => "function() {
                    var form = $('#{$model->tableName()}' + '-form');
                    var data = form.data('yiiActiveForm');
                    $.each(data.attributes, function() {
                        this.status = 3;
                    });
                    form.yiiActiveForm('validate');
                }",
            ],          
        ]); 
    ?>
    
    <?= $form->field($model, 'confirmed')->widget(DatePicker::class, [
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
        ]); 
    ?>
    
    <?= $form->field($model, 'from_bank_id')->widget(Select2::class, [
            'data' => $bankFromList,
            'options' => [
                'id' => 'from-bank-id',
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => !$model->isAttributeRequired('from_bank_id'),
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],            
        ]);
    ?>
    
    <?= $form->field($model, 'to_bank_id')->widget(Select2::class, [
            'data' => $bankToList,
            'options' => [
                'id' => 'to-bank-id',
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => !$model->isAttributeRequired('to_bank_id'),
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],            
        ]);
    ?>
    <?php endif; ?>
    </div>
    
    <?= $form->field($model, 'summa', [
        'addon' => [
            'append' => [
                ['content' => 
                    MaskedInput::widget([
                        'model' => $model,
                        'attribute' => 'rate',
                        //'mask' => '9{1,10}[.9{1,4}]',
                        'options' => [
                            'id' => 'rate', 
                            'placeholder' => 'Rate',
                            'class' => 'form-control number-field',
                            'style' => 'text-align: right; min-width: 90px;'
                        ],
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'rightAlign' => false,
                            'digits' => 4,
                            'allowMinus' => false,
                        ],                                    
                    ]),
                    'asButton' => true,
                ],
                ['content' => 
                    Select2::widget([
                        'model' => $model,
                        'attribute' => 'valuta_id',
                        'data' => $valutaList, 
                        'options' => [
                            'id' => 'valuta-id', 
                            'class' => 'rate-changer-currency',
                            'placeholder' => '...',
                            'data-date_input_id' => 'billpayment-paid_date',
                            'data-rate_input_id' => 'rate',
                        ], 
                        'pluginOptions' => [
                            'allowClear' => !$model->isAttributeRequired('valuta_id'),
                            'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
                        ],            
                        'size' => 'control-width-90',
                    ]),
                    'asButton' => true,
                ],
            ],
        ],
        //'labelOptions' => ['class' => 'col-md-3'],
    ])->widget(MaskedInput::class, [
        //'mask' => '9{1,10}[.9{1,2}]',
        'options' => [
            'class' => 'form-control number-field',
            //'readonly' => true,
        ],
        'clientOptions' => [
            'alias' => 'decimal',
            'rightAlign' => false,
            'digits' => 2,
            'allowMinus' => false,
        ],                                    
    ])->label($model->getAttributeLabel('summa').' / '.$model->getAttributeLabel('rate').' / '.$model->getAttributeLabel('valuta_id'));
    ?>
    
    <?= $form->field($historyModel, 'comment')->textarea(['rows' => 3]) ?>

    <?php if(!empty($model->id) && !empty($isAdmin)){
        echo $form->field($model, 'confirmed')->widget(DatePicker::class, [
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
        ]); 
    } ?>    

    <?= $form->field($model, 'combine')->widget(SwitchInput::class, [
        'pluginOptions' => [
            'onText' => Yii::t('common', 'Yes'),
            'offText' => Yii::t('common', 'No'),
            'handleWidth' => (!$isModal ? 'auto' : '45')
        ],
    ]); ?>
    
    <div class="form-group <?php if($isModal) : echo 'modal-button-group'; endif; ?>">
        <div class="col-md-offset-2 col-md-10" style="text-align: right;">
            <?= $model->saveButton; ?>
            <?= $model->cancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <?php if($isModal) : Pjax::end(); endif; ?>
</div>

<?php
$this->registerJs(
    "var form = $('#bill_payment-form');
    form.find('#rate').change(function(){
        form.yiiActiveForm('validateAttribute', 'billpayment-summa');                    
    });",
    \yii\web\View::POS_END
);
?>