<?php
namespace common\models;

use Yii;
use yii\widgets\Pjax;
use yii\widgets\MaskedInput;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;
use kartik\checkbox\CheckboxX;

use common\components\FSMHtml;
/* @var $this yii\web\View */
/* @var $model common\models\bill\BillPayment */
/* @var $form yii\widgets\ActiveForm */
$isModal = !empty($isModal);
?>

<div class="bill-payment-form">
    <?php if($isModal) : Pjax::begin(Yii::$app->params['PjaxModalOptions']); endif; ?>

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => $model[0]->tableName().'-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'options' => [
            'data-pjax' => $isModal,
        ],           
    ]); ?>

    <?= $form->field($historyModel, 'create_time', [
        'options' => [
            'id' => 'create-time-static-text',
            'class' => 'form-group static-input',
        ],
        'staticValue' => (isset($historyModel->create_time) ? date('d-M-Y H:i:s', strtotime($historyModel->create_time)) : date('d-M-Y H:i:s')),
    ])->staticInput(['class' => 'form-control', 'disabled' => true]); ?>

    <?= $form->field($historyModel, 'create_user_id', [
        'options' => [
            'id' => 'create-user-static-text',
            'class' => 'form-group static-input',
        ],
        'staticValue' => (isset($historyModel->create_user_id) ? $historyModel->createUserProfile->name : Yii::$app->user->identity->profile->name),
    ])->staticInput(['class' => 'form-control', 'disabled' => true]); ?>

    <?= $form->field($paymentOrderModel, 'id', [
        'options' => [
            'class' => 'form-group required',
        ],
    ])->widget(Select2::class, [
            'data' => $paymentOrderList,
            'options' => [
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => !$model[0]->isAttributeRequired('payment_order_id'),
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],           
            /*
            'addon' => [
                'prepend' => $model[0]::getModalButtonContent([
                    'formId' => $form->id,
                    'controller' => 'payment-order',
                ]),
            ],        
             * 
             */
        ])->label($model[0]->getAttributeLabel('payment_order_id')); 
    ?>
    
    <?php
        echo Html::beginTag('div', [
            'class' => 'form-group',
        ]);
        echo Html::beginTag('div', [
            'class' => 'col-md-2',
        ]);
        echo Html::endTag('div');
        echo Html::beginTag('div', [
            'id' => 'new-payment-order-container',
            'class' => 'col-md-10',
        ]);
        echo CheckboxX::widget([
            'name' => 'new_payment_order',
            'options' => ['id' => 'cbx-new-payment-order'],
            'pluginOptions' => ['threeState' => false],
        ]);
        $labelLegalTxt = Yii::t('bill', 'Create new payment order');
        echo Html::label($labelLegalTxt, 'cbx-new-payment-order',
            [
                'class' => 'cbx-label fsm-label',
            ]
        );
        echo '<div class="help-block"></div>';
        echo Html::endTag('div');
        echo Html::endTag('div');    
    ?>

    <?php 
        $body = $this->render('@frontend/views/bill/payment-order/_form', [
            'form' => $form,
            'model' => $paymentOrderModel,
            'bankList' => $bankList,
            'isAdmin' => $isAdmin,
            'isModal' => true,
            'fromPayment' => true,
        ]); 

        $panelContent = [
            'heading' => $model[0]->getAttributeLabel('payment_order_id'),
            'preBody' => '<div class="panel-body">',
            'body' => $body,
            'postBody' => '</div>',
        ];
        
        echo Html::beginTag('div', [
            'id' => 'payment-order-data-container',
            'class' => 'form-group',
            'style' => 'display: none;'
        ]);
        echo Html::beginTag('div', [
            'class' => 'col-md-2',
        ]);
        echo Html::endTag('div');
        echo Html::beginTag('div', [
            'class' => 'col-md-10',
        ]);
        
        echo FSMHtml::panel(
            $panelContent, 
            'default', 
            [
                'id' => "payment-order-data",
                //'style' => "background: antiquewhite;",
            ]
        );
        echo Html::endTag('div');
        echo Html::endTag('div');           
    ?>  
    
    <?php
        echo Html::beginTag('div', [
            'id' => 'product-data-container',
            'class' => 'form-group',
        ]);
        echo Html::beginTag('div', [
            'class' => 'col-md-2',
        ]);
        echo Html::endTag('div');
        echo Html::beginTag('div', [
            'class' => 'col-md-10',
        ]);
    ?>

    <?= $this->render('_bill_payment_table_edit', [
        'form' => $form,
        'model' => $model,
        'valutaList' => $valutaList,
        'isModal' => $isModal,
    ]) ?>

    <?php
        echo Html::endTag('div');
        echo Html::endTag('div');           
    ?> 
    
    <?= $form->field($historyModel, 'comment')->textarea(['rows' => 3]) ?>

    <div class="form-group <?php if($isModal) : echo 'modal-button-group'; endif; ?>">
        <div class="col-md-offset-2 col-md-10" style="text-align: right;">
            <?= $model[0]->SaveButton; ?>
            <?= Html::submitButton(Html::icon('download-alt', ['class' => 'bootstrap-dialog-button-icon']) . ' ' . Yii::t('comment', 'Save & Download XML'), 
                    [
                        'id' => 'button-download-xml',
                        'class' => 'btn btn-lg btn-primary', 
                        'name' => 'download_xml', 
                    ]
            ); ?>
            <?= Html::submitButton(Html::icon('remove', ['class' => 'bootstrap-dialog-button-icon']) . ' ' . Yii::t('comment', 'Close'), 
                    [
                        'id' => 'button-close',
                        'class' => 'btn btn-lg btn-success', 
                        'name' => 'close', 
                        'style' => 'display: none;'
                    ]
            ); ?>
            <?= $model[0]->CancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <?php if($isModal) : Pjax::end(); endif; ?>
</div>