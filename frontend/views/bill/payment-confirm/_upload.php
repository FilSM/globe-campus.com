<?php
namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;

use common\widgets\EnumInput;

/* @var $this yii\web\View */
/* @var $model common\models\client\Agreement */
/* @var $form yii\widgets\ActiveForm */

$isModal = !empty($isModal);
$isAdmin = !empty($isAdmin);
?>

<div class="payment-confirm-upload-form">
    <?php if($isModal) : Pjax::begin(Yii::$app->params['PjaxModalOptions']); endif; ?>
    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => $model->tableName().'-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'options' => [
            'data-pjax' => $isModal,
        ],         
    ]); ?>
    
    <?= Html::activeHiddenInput($model, 'abonent_id'); ?>
    
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'client_id')->widget(Select2::class, [
            'data' => $clientList,
            'options' => [
                //'id' => 'first-client-id', 
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => true,
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],            
            'addon' => [
                'prepend' => $model::getModalButtonContent([
                    'formId' => $form->id,
                    'controller' => 'client',
                ]),
            ],        
        ]); 
    ?>

    <?= $form->field($model, 'bank_id')->widget(DepDrop::class, [
        'type' => DepDrop::TYPE_SELECT2,
        'data' => empty($model->bank_id) ? null : [$model->bank_id => $model->bank->name],
        'select2Options' => [
            'pluginOptions' => [
                'allowClear' => !$model->isAttributeRequired('bank_id'),
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],
        ],
        'pluginOptions' => [
            'depends' => ['paymentconfirm-client_id'],
            //'initDepends' => ['paymentconfirm-client_id'],
            'initialize' => true,            
            'url' => Url::to(['/client/ajax-get-client-bank-list']),
            'placeholder' => '...',
        ],
        /*
        'pluginEvents' => [
            "depdrop:afterChange" => "function() {
                var form = $('#{$model->tableName()}' + '-form');
                checkRequiredInputs(form);
            }",
        ],
         * 
         */
     ]); ?>        
        
    <?php if(empty($model->id)) :
        echo $form->field($filesXMLModel, 'file', [
            'options' => [
                'class' => 'form-group',
            ],
        ])->widget(FileInput::class, [
            'language' =>  strtolower(substr(Yii::$app->language, 0, 2)),
            'sortThumbs' => false,
            'options' => [
                'id' => 'xml-file',
            ],
            'pluginOptions' => [
                'allowedFileExtensions' => ['xml'],
                'maxFileSize' => 20000,
                'showRemove' => false,
                'showUpload' => false,
            ],
            'pluginEvents' => [
                "change" => "function() {
                    var form = $('#{$model->tableName()}' + '-form');
                    var \$this = $(this);
                    var value = \$this.val();
                    //var display = (!empty(value) ? 'none' : 'block');
                    //form.find('#bank-data-group').css({'display': display});
                    if(!empty(value)){
                        form.find('#bank-data-group').hide('slow');
                    }else{
                        form.find('#bank-data-group').show('slow');
                    }
                }",
            ],            
        ])->label(Yii::t('bill', 'XML filename to import')); 
   
        echo $form->field($filesPDFModel, 'file', [
            'options' => [
                'class' => 'form-group',
            ],
        ])->widget(FileInput::class, [
            'language' =>  strtolower(substr(Yii::$app->language, 0, 2)),
            'sortThumbs' => false,
            'options' => [
                'id' => 'pdf-file',
            ],
            'pluginOptions' => [
                'allowedFileExtensions' => ['pdf'],
                'maxFileSize' => 20000,
                'showRemove' => false,
                'showUpload' => false,
            ],
        ])->label(Yii::t('bill', 'PDF filename to import')); 

    endif; ?>
    
    <div id="bank-data-group">
        <?= $form->field($model, 'start_date', [])->widget(DatePicker::class, [
                'type' => DatePicker::TYPE_RANGE,
                'attribute' => 'start_date',
                'attribute2' => 'end_date',
                'options' => [
                    'placeholder' => Yii::t('common', 'Start date'),
                ],
                'options2' => [
                    'placeholder' => Yii::t('common', 'End date'),
                ],
                'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
            ])->label(Yii::t('client', 'Period')); 
         ?>

        <?= $form->field($model, 'pay_date')->widget(DatePicker::class, [
                'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
            ]); 
        ?>
    </div>    
    
    <?php if(!empty($model->id) && !empty($isAdmin)){
        echo $form->field($model, 'status')->widget(EnumInput::class, [
            'type' => EnumInput::TYPE_RADIOBUTTON,
            'options' => [
                'translate' => $model->importStateList,
            ],
        ]); 
    } ?>
    
    <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>

    <div class="form-group <?php if($isModal) : echo 'modal-button-group'; endif; ?>">
        <div class="col-md-offset-2 col-md-10" style="text-align: right;">
            <?= $model->SaveButton; ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <?php if($isModal) : Pjax::end(); endif; ?>
</div>