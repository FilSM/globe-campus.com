<?php

use yii\widgets\Pjax;

use kartik\helpers\Html;
use kartik\grid\GridView;

use common\components\FSMAccessHelper;
use common\components\FSMHelper;
use common\models\bill\PaymentConfirm;
use common\models\bill\BillConfirm;
use common\models\bill\BillConfirmLink;
?>

<?php 
    ob_start();
    ob_implicit_flush(false);
?>

<div class="bill-confirm-index">
    <?php if(empty($linkedModel->uploaded_file_id) && !empty($linkedModel->uploaded_pdf_file_id)): ?>
    <p>
        <?= FSMHelper::aButton($linkedModel->id, [
            'label' => Yii::t('bill', 'Autocomplete'),
            'title' => Yii::t('bill', 'Autocomplete confirmed payments'),
            'controller' => 'payment-confirm',
            'action' => 'generate',
            'class' => 'success',
            'icon' => 'plus',
            //'modal' => true,
        ]); ?>        
        
        <?= FSMHelper::aButton($linkedModel->id, [
            'label' => $searchModel->modelTitle(),
            'title' => $searchModel->modelTitle(),
            'controller' => 'bill-confirm',
            'action' => 'create',
            'class' => 'success',
            'icon' => 'plus',
            //'modal' => true,
        ]); ?>
    </p>
    <?php endif; ?>
    
    <?= GridView::widget([
        'responsive' => false,
        //'striped' => false,
        'hover' => true,
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'pjax' => true, 
        'pjaxSettings' => [
            'enablePushState' => false,
            'enableReplaceState' => false,
        ],
        'columns' => [
            ['class' => '\kartik\grid\SerialColumn'],
            [
                'attribute'=>'linked_ids',
                'headerOptions' => ['class'=>'td-mw-75'],
                'value' => function ($model) use ($linkedModel) {
                    $linkedObjList = $model->getLinkedObjects($linkedModel->status);
                    if(empty($linkedObjList)){
                        return null;
                    }
                    
                    $output = [];
                    foreach ($linkedObjList as $key => $linkedObjArr) {
                        foreach ($linkedObjArr as $linkedObj) {
                            $docType = BillConfirmLink::getDocTypeList()[$key];
                            switch ($key) {
                                case BillConfirmLink::DOC_TYPE_BILL:
                                    if(FSMAccessHelper::can('viewBill')){
                                        $output[] = Html::a($docType.': '.$linkedObj->doc_number, [
                                                '/bill/view', 
                                                'id' => $linkedObj->id
                                            ], [
                                                'target' => '_blank', 
                                                'data-pjax' => 0,
                                            ]
                                        );
                                    }else{
                                        $output[] = $docType.': '.$linkedObj->doc_number;
                                    }
                                    break;
                                case BillConfirmLink::DOC_TYPE_AGREEMENT:
                                    if(FSMAccessHelper::can('viewAgreement')){
                                        $output[] = Html::a($docType.': '.$linkedObj->number, [
                                                '/agreement/view', 
                                                'id' => $linkedObj->id
                                            ], [
                                                'target' => '_blank', 
                                                'data-pjax' => 0,
                                            ]
                                        );
                                    }else{
                                        $output[] = $docType.': '.$linkedObj->number;
                                    }
                                    break;
                                case BillConfirmLink::DOC_TYPE_EXPENSE:
                                    if(FSMAccessHelper::can('viewExpense')){
                                        $output[] = Html::a($docType.': '.$linkedObj->doc_number, [
                                                '/expense/view', 
                                                'id' => $linkedObj->id
                                            ], [
                                                'target' => '_blank', 
                                                'data-pjax' => 0,
                                            ]
                                        );
                                    }else{
                                        $output[] = $docType.': '.$linkedObj->doc_number;
                                    }
                                    break;
                                case BillConfirmLink::DOC_TYPE_DIRECT:
                                    if(!empty($linkedObj->doc_number) && ($linkedModel->status == PaymentConfirm::IMPORT_STATE_COMPLETE)){
                                        if(FSMAccessHelper::can('viewExpense')){
                                            $output[] = Html::a($docType.': '.$linkedObj->doc_number, [
                                                    '/expense/view', 
                                                    'id' => $linkedObj->id
                                                ], [
                                                    'target' => '_blank', 
                                                    'data-pjax' => 0,
                                                ]
                                            );
                                        }else{
                                            $output[] = $docType.': '.$linkedObj->doc_number;
                                        }
                                    }else{
                                        $output[] = $docType.': '.$linkedObj->summa.(isset($model->valuta_id) ? ' '.$model->valuta->name : '');
                                    }
                                    break;
                                case BillConfirmLink::DOC_TYPE_TAX:
                                    if(!empty($linkedObj->doc_number) && ($linkedModel->status == PaymentConfirm::IMPORT_STATE_COMPLETE)){
                                        if(FSMAccessHelper::can('viewTaxPayment')){
                                            $output[] = Html::a($docType.': '.$linkedObj->doc_number, [
                                                    '/tax-payment/view', 
                                                    'id' => $linkedObj->id
                                                ], [
                                                    'target' => '_blank', 
                                                    'data-pjax' => 0,
                                                ]
                                            );
                                        }else{
                                            $output[] = $docType.': '.$linkedObj->doc_number;
                                        }
                                    }else{
                                        $output[] = $docType.': '.$linkedObj->summa.(isset($model->valuta_id) ? ' '.$model->valuta->name : '');
                                    }
                                    break;

                                default:
                                    break;
                            }
                        }
                    }
                    $output = implode('<br/>', $output);
                    return !empty($output) ? $output : null;
                },                         
                'format'=>'raw',
            ],  
            'bank_ref',
            [
                'attribute' => 'first_client_account',
                'headerOptions' => ['class'=>'td-mw-100'],
            ],                           
            [
                'attribute' => 'second_client_name',
                'contentOptions' => [
                    'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                ],                
                'value' => function ($model) {
                    return  
                        '<div style="overflow-x: auto;">'.
                        (isset($model->second_client_id) ? 
                            Html::a($model->second_client_name, ['/client/view', 'id' => $model->second_client_id], ['target' => '_blank', 'data-pjax' => 0,]) : 
                            $model->second_client_name).
                        '</div>';
                },                         
                'format' => 'raw',
            ],                          
            [
                'attribute' => 'second_client_reg_number',
                'headerOptions' => ['class'=>'td-mw-75'],
            ],                           
            [
                'attribute' => 'second_client_account',
                'headerOptions' => ['class'=>'td-mw-75'],
            ],                           
            // 'second_client_id',
            [
                'attribute' => 'summa',
                'hAlign' => 'right',
                'headerOptions' => ['class'=>'td-mw-100'],
                //'width' => '150px',
                //'label' => Yii::t('bill', 'Sum'),
                'value' => function ($model) {
                    return isset($model->summa) ? number_format($model->summa, 2, '.', ' ').(!empty($model->currency) ? ' '.$model->currency : '') : null;
                },
            ],                    
            [
                'attribute' => 'doc_date',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class'=>'td-mw-75'],
                'value' => function ($model) {
                    return isset($model->doc_date) ? date('d-M-Y', strtotime($model->doc_date)) : null;
                },
            ],                         
            'doc_number',
            [
                'attribute' => 'direction',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class'=>'td-mw-50'],
                'value' => function ($model) {
                    return !empty($model->direction) ? $model->directionList[$model->direction] : null;
                },
            ],
            [
                'attribute'=>'comment',
                'contentOptions' => [
                    'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                ],
                'value' => function ($model) {
                    return 
                        '<div style="overflow-x: auto;">'.
                        (!empty($model->comment) ? $model->comment : '').
                        '</div>';
                },                
                'format'=>'raw',
            ],                        
            [
                'class' => '\common\components\FSMActionColumn',
                'headerOptions' => ['class'=>'td-mw-125'],
                'checkCanDo' => true,
                //'dropdown' => true,
                'template' => '{update} {delete}',
                /*
                'buttons' => [
                    'update' => function ($params) { 
                        return BillConfirm::getButtonUpdate($params);
                    },
                ],
                 * 
                 */ 
                'controller' => 'bill-confirm',
                'linkedObj' => [
                    ['fieldName' => 'payment_confirm_id', 'id' => (!empty($linkedModel->id) ? $linkedModel->id : null)],
                ],                
            ],                        
        ],
        'rowOptions' => function ($model, $key, $index, $grid) {
            if($model['find_part'] == 1){
                return ['class' => 'client-status-deleted'];
            }else{
                return [];
            }
        }, 
    ]); ?>
</div>

<?php
    $body = ob_get_contents();
    ob_get_clean(); 

    $panelContent = [
        'heading' => $searchModel->modelTitle(2),
        'preBody' => '<div class="panel-body">',
        'body' => $body,
        'postBody' => '</div>',
    ];
    echo Html::panel(
        $panelContent, 
        'success', 
        [
            'id' => "panel-bill-payment-data",
        ]
    );
?>