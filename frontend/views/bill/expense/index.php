<?php
namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use kartik\helpers\Html;
use kartik\grid\GridView;

use common\components\FSMAccessHelper;
use common\models\bill\Expense;

/* @var $this yii\web\View */
/* @var $searchModel common\models\bill\search\ExpenseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $searchModel->modelTitle(2);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="expense-index">

    <?= Html::pageHeader(Html::encode($this->title)); ?>
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if(FSMAccessHelper::can('createExpense')): ?>
    <p>
        <?= Html::a(Html::icon('plus').'&nbsp;'.$searchModel->modelTitle(), ['create'], ['class' => 'btn btn-success']); ?>
    </p>
    <?php endif; ?>
    
    <?= GridView::widget([
        'responsive' => false,
        //'striped' => false,
        'hover' => true,
        'floatHeader' => true,
        'pjax' => true,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => '\kartik\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'width' => '75px',
                'hAlign' => 'center',
            ],
            [
                'attribute'=>'doc_number',
                'headerOptions' => ['class'=>'td-mw-100'],
                'value' => function ($model) {
                    return !empty($model->doc_number) ? 
                        (FSMAccessHelper::can('viewExpense', $model) ?
                            Html::a($model->doc_number, ['/expense/view', 'id' => $model->id], ['target' => '_blank', 'data-pjax' => 0]) :
                            $model->doc_number
                        )
                        : null;
                },                         
                'format'=>'raw',                        
            ],
            [
                'attribute' => 'expense_type_id',
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) {
                    return isset($model->expense_type_id) ? $model->expense_type_name : null;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $expenseTypeList,
                'filterWidgetOptions'=>['pluginOptions' => ['allowClear' => true],],
                'filterInputOptions' => ['placeholder' => '...'],
            ],            
            [
                'attribute'=>'project_id',
                'headerOptions' => ['class'=>'td-mw-100'],
                'value' => function ($model) {
                    return !empty($model->project_id) ? Html::a($model->project_name, ['/project/view', 'id' => $model->project_id], ['target' => '_blank', 'data-pjax' => 0]) : null;
                },                         
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $projectList,
                'filterWidgetOptions'=>['pluginOptions' => ['allowClear' => true],],
                'filterInputOptions' => ['placeholder' => '...'],
                'format'=>'raw',
                'visible' => FSMAccessHelper::can('createExpense'),
            ],                           
            [
                'attribute'=>'first_client_id',
                'contentOptions' => [
                    'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                ],
                'value' => function ($model) {
                    return  
                        '<div style="overflow-x: auto;">'.
                        (!empty($model->first_client_id) ?  
                        Html::a($model->first_client_name, ['/client/view', 'id' => $model->first_client_id], ['target' => '_blank', 'data-pjax' => 0]) : null).
                        '</div>';
                },   
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $clientList,
                'filterWidgetOptions'=>['pluginOptions' => ['allowClear' => true],],
                'filterInputOptions' => ['placeholder' => '...'],
                'format'=>'raw',
            ],                          
            [
                'attribute'=>'second_client_id',
                'contentOptions' => [
                    'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                ],
                'value' => function ($model) {
                    return  
                        '<div style="overflow-x: auto;">'.
                        (!empty($model->second_client_id) ?  
                        Html::a($model->second_client_name, ['/client/view', 'id' => $model->second_client_id], ['target' => '_blank', 'data-pjax' => 0]) : null).
                        '</div>';
                },   
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $clientList,
                'filterWidgetOptions'=>['pluginOptions' => ['allowClear' => true],],
                'filterInputOptions' => ['placeholder' => '...'],
                'format'=>'raw',
            ],
            [
                'attribute' => 'doc_date',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'width' => '150px',
                'headerOptions' => ['class'=>'td-mw-75'],
                'value' => function ($model) {
                    return isset($model->doc_date) && ($model->doc_date == date('Y-m-d')) ? 
                        Html::a(Html::badge(Yii::t('common', 'Today'), ['class' => 'badge-success']), 
                            Url::to(['/bill/index']).'?BillSearch[doc_date]='.$model->doc_date, 
                            ['target' => '_blank', 'data-pjax' => 0]) 
                        : date('d-M-Y', strtotime($model->doc_date));
                },
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [
                    'pluginOptions' => ArrayHelper::merge(
                        Yii::$app->params['DatePickerPluginOptions'], 
                        [
                            'locale' => [
                                'firstDay' => 1,
                                'format' => 'd-m-Y',
                                'cancelLabel' => Yii::t('common', 'Clear'),
                            ],
                            //'opens'=>'left',
                            //'startDate' => date('d-m-Y'),
                        ]),
                    'convertFormat' => true,
                    //'presetDropdown' => true,
                    'hideInput' => true,
                    'pluginEvents' => [
                        'cancel.daterangepicker' => 'function() {
                            //console.log("cancel.daterangepicker"); 
                            var gridFilters = $("table tr.filters");
                            var daterangeinput = gridFilters.find("#expensesearch-doc_date");
                            daterangeinput.val("").change();
                        }',
                    ],
                ],
                'format'=>'raw',  
            ],
            [
                'attribute' => 'summa',
                'hAlign' => 'right',
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) {
                    return isset($model->summa) ? number_format($model->summa, 2, '.', ' ') : null;
                },
            ],                          
            [
                'attribute' => 'vat',
                'hAlign' => 'right',
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) {
                    return isset($model->vat) ? number_format($model->vat, 2, '.', ' ') : null;
                },
            ],                          
            [
                'attribute' => 'total',
                'hAlign' => 'right',
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) {
                    return isset($model->valuta_id) ? number_format($model->total, 2, '.', ' ') . ' ' . $model->valuta->name : number_format($model->total, 2, '.', ' ');
                },
            ],   
            [
                'attribute' => 'confirmed',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'width' => '150px',
                'headerOptions' => ['class'=>'td-mw-75'],
                'value' => function ($model) {
                    return isset($model->confirmed) ? date('d-M-Y', strtotime($model->confirmed)) : null;
                },
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [
                    'pluginOptions' => \yii\helpers\ArrayHelper::merge(
                        Yii::$app->params['DatePickerPluginOptions'], 
                        [
                            'locale' => [
                                'firstDay' => 1,
                                'format' => 'd-m-Y',
                                'cancelLabel' => Yii::t('common', 'Clear'),
                            ],
                            //'opens'=>'left',
                            //'startDate' => date('d-m-Y'),
                        ]),
                    'convertFormat' => true,
                    //'presetDropdown' => true,
                    'hideInput' => true,
                    'pluginEvents' => [
                        'cancel.daterangepicker' => 'function() {
                            //console.log("cancel.daterangepicker"); 
                            var gridFilters = $("table tr.filters");
                            var daterangeinput = gridFilters.find("#billpaymentsearch-confirmed");
                            daterangeinput.val("").change();
                        }',
                    ],
                ],
                'format'=>'raw',        
            ],                          
            [
                'attribute' => "report_plus",   
                'vAlign' => 'middle',
                'class' => '\kartik\grid\BooleanColumn',
                'trueLabel' => 'Yes', 
                'falseLabel' => 'No',
                'width' => '100px',
                'visible' => FSMAccessHelper::can('changeExpenseEbitda'),
            ],                        
            // 'comment:ntext',
            // 'create_time',
            // 'create_user_id',
            // 'update_time',
            // 'update_user_id',

            [
                'class' => '\common\components\FSMActionColumn',
                'headerOptions' => ['class'=>'td-mw-125'],
                'dropdown' => true,
                'checkPermission' => true,
                'template' => '{attachment} {view} {copy} {update} {delete}',
                'buttons' => [
                    'attachment' => function (array $params) { 
                        return Expense::getButtonAttachment($params);
                    },                            
                    'copy' => function (array $params) { 
                        return Expense::getButtonCopy($params);
                    },
                ],                
                'controller' => 'expense',
            ],
        ],
    ]); ?>
</div>