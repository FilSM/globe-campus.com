<?php

use yii\helpers\Url;

use kartik\helpers\Html;
use kartik\detail\DetailView;

use common\components\FSMAccessHelper;
use common\models\bill\Expense;

/* @var $this yii\web\View */
/* @var $model common\models\bill\Expense */

$this->title = $model->modelTitle() .' #'. $model->doc_number;
if(FSMAccessHelper::checkRoute('/expense/index')){
    $this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
} 
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="expense-view">

    <?= Html::pageHeader(Html::encode($this->title)); ?>
    <div class='col-md-12' style="margin-bottom: 5px;">
        <?php if(FSMAccessHelper::can('createExpense', $model)):
            $params = [
                'url' => Url::to(['copy', 'id' => $model->id]), 
                'model' => $model, 
                'isBtn' => true,
            ];            
            echo Expense::getButtonCopy($params);
        endif; ?>
        <?php if(FSMAccessHelper::can('updateExpense', $model)): ?>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>
        <?php if(FSMAccessHelper::can('deleteExpense', $model)): ?>
        <?= \common\components\FSMBtnDialog::button(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'id' => 'btn-dialog-selected',
            'class' => 'btn btn-danger',
        ]); ?>
        <?php endif; ?>
    </div> 

    <div class='col-md-12'>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                [
                    'attribute' => 'expense_type_id',
                    'value' => isset($model->expense_type_id) ? $model->expenseType->name : null,
                ],    
                [
                    'attribute' => 'project_id',
                    'value' => !empty($model->project_id) ? Html::a($model->project->name, ['/project/view', 'id' => $model->project_id], ['target' => '_blank']) : null,
                    'format' => 'raw',
                ],
                'doc_number',
                [
                    'attribute' => 'doc_date',
                    'value' => isset($model->doc_date) ? date('d-M-Y', strtotime($model->doc_date)) : null,
                ],            
                [
                    'attribute' => 'first_client_id',
                    'value' => !empty($model->first_client_id) ? 
                        Html::a($model->firstClient->name, ['/client/view', 'id' => $model->first_client_id], ['target' => '_blank']) : null,
                    'format' => 'raw',
                ],            
                [
                    'attribute' => 'second_client_id',
                    'value' => !empty($model->second_client_id) ?   
                        Html::a($model->secondClient->name, ['/client/view', 'id' => $model->second_client_id], ['target' => '_blank']) : null,
                    'format' => 'raw',
                ],              
                'summa',          
                'vat',
                [
                    'attribute' => 'total',
                    'value' => isset($model->valuta_id) ? number_format($model->total, 2, '.', ' ') . ' ' . $model->valuta->name : number_format($model->total, 2, '.', ' '),
                ],
                [
                    'attribute' => 'confirmed',
                    'value' => isset($model->confirmed) ? date('d-M-Y', strtotime($model->confirmed)) : null,
                ],             
                [
                    'attribute' => 'pay_type',
                    'value' => isset($model->pay_type) ? $model->payTypeList[$model->pay_type] : null,
                    //'visible' => !empty($model->confirmed),
                ],                
                [
                    'attribute' => "report_plus",   
                    'format' => 'boolean',
                    'visible' => FSMAccessHelper::can('changeExpenseEbitda'),
                ],                 
                'comment:ntext',
            ],
        ]) ?>
    </div>

    <div class='col-md-12'>
        <?= $this->render('_attachment_table', [
            'model' => $filesModel,
        ]) ?>         
    </div>
</div>