<?php
namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;
use kartik\icons\Icon;

use common\components\FSMAccessHelper;
use common\models\user\FSMUser;
use common\widgets\EnumInput;

/* @var $this yii\web\View */
/* @var $model common\models\bill\Expense */
/* @var $form yii\widgets\ActiveForm */

Icon::map($this);
$isModal = !empty($isModal);
$isAdmin = !empty($isAdmin);
?>

<div class="expense-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => $model->tableName().'-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>
    
    <?= Html::activeHiddenInput($model, 'abonent_id'); ?>
    
    <?= $form->field($model, 'expense_type_id')->widget(Select2::class, [
            'data' => $expenseTypeList,
            'options' => [
                'id' => 'expense-type-id',
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => !$model->isAttributeRequired('expense_type_id'),
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],            
            'addon' => [
                'prepend' => $model::getModalButtonContent([
                    'formId' => $form->id,
                    'controller' => 'expense-type',
                ]),
            ],        
        ]); 
    ?>

    <?= $form->field($model, 'project_id')->widget(Select2::class, [
            'data' => $projectList,
            'options' => [
                'id' => 'project-id',
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => !$model->isAttributeRequired('project_id'),
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],            
            'addon' => [
                'prepend' => $model::getModalButtonContent([
                    'formId' => $form->id,
                    'controller' => 'project',
                ]),
            ],        
        ]); 
    ?>

    <?= $form->field($model, 'doc_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'doc_date')->widget(DatePicker::class, [
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
            'options' => [
                'class' => 'rate-changer-date',
                'data-currency_input_id' => 'valuta-id',
                'data-rate_input_id' => 'rate',
            ],        
        ]); 
    ?>

    <?= $form->field($model, 'first_client_id')->widget(Select2::class, [
            'data' => $clientList,
            'options' => [
                //'id' => 'first-client-id', 
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => true,
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],            
            'addon' => [
                'prepend' => $model::getModalButtonContent([
                    'formId' => $form->id,
                    'prefix' => 'first-',
                    'controller' => 'client',
                ]),
            ],        
        ]); 
    ?>
    
    <?= $form->field($model, 'second_client_id')->widget(Select2::class, [
            'data' => $clientList,
            'options' => [
                //'id' => 'second-client-id', 
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => true,
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],            
            'addon' => [
                'prepend' => $model::getModalButtonContent([
                    'formId' => $form->id,
                    'prefix' => 'second-',
                    'controller' => 'client',
                ]), 
            ],        
        ]); 
    ?>

    <?= $form->field($model, 'summa', [
        'addon' => [
            'append' => [
                ['content' => 
                    MaskedInput::widget([
                        'model' => $model,
                        'attribute' => 'rate',
                        //'mask' => '9{1,10}[.9{1,3}]',
                        'options' => [
                            'id' => 'rate', 
                            'placeholder' => 'Rate',
                            'class' => 'form-control number-field',
                            'style' => 'text-align: right; min-width: 90px;'
                        ],
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'rightAlign' => false,
                            'digits' => 4,
                            'allowMinus' => false,
                        ],                                    
                    ]),
                    'asButton' => true,
                ],
                ['content' => 
                    Select2::widget([
                        'model' => $model,
                        'attribute' => 'valuta_id',
                        'data' => $valutaList, 
                        'options' => [
                            'id' => 'valuta-id', 
                            'class' => 'rate-changer-currency',
                            'placeholder' => '...',
                            'data-date_input_id' => 'expense-doc_date',
                            'data-rate_input_id' => 'rate',
                        ],
                        'pluginOptions' => [
                            'allowClear' => !$model->isAttributeRequired('valuta_id'),
                            'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
                        ],            
                        'size' => 'control-width-90',
                    ]),
                    'asButton' => true,
                ],
            ],
        ],
    ])->widget(MaskedInput::class, [
        //'mask' => '9{1,10}[.9{1,2}]',
        'options' => [
            'class' => 'form-control number-field',
        ],
        'clientOptions' => [
            'alias' => 'decimal',
            'rightAlign' => false,
            'digits' => 2,
            'allowMinus' => false,
        ],                                    
    ])->label($model->getAttributeLabel('summa').' / '.$model->getAttributeLabel('rate').' / '.$model->getAttributeLabel('valuta_id'));
    ?>

    <?= $form->field($model, 'vat')->widget(MaskedInput::class, [
        //'mask' => '9{1,10}[.9{1,2}]',
        'options' => [
            'class' => 'form-control number-field',
        ],
        'clientOptions' => [
            'alias' => 'decimal',
            'rightAlign' => false,
            'digits' => 2,
            'allowMinus' => false,
        ],                                    
    ]); ?>
    
    <?= $form->field($model, 'total')->textInput([
        'readonly' => true,
    ]); ?>
        
    <?php
    if(!empty($filesModel)){
        $preview = $previewConfig = [];
        foreach ($filesModel as $key => $file) {
            if(empty($file->id)){
                continue;
            }
            $preview[] = $file->uploadedFileUrl;
            $ext = pathinfo($file->filename, PATHINFO_EXTENSION);
            switch ($ext) {
                default:
                case 'pdf':
                    $type = 'pdf';
                    break;
                case 'doc':
                case 'docx':
                    case 'edoc':
                    case 'bdoc':
                case 'xls':
                case 'xlsx':
                    $type = 'gdocs';
                    break;
                case 'zip':
                    $type = 'object';
                    break;
            }
            $previewConfig[] = [
                'type' => $type,
                'size' => $file->filesize, 
                'caption' => $file->filename, 
                'url' => Url::to(['/expense/attachment-delete', 'deleted_id' => $file->id]), 
                'key' => 101 + $key,
                'downloadUrl' => $file->uploadedFileUrl,
                'width' => 'auto',
                'height' => 'auto',
                'max-width' => '100%',
                'max-height' => '100%',
                'image-orientation' => 'from-image',
            ];
        }
    
        echo $form->field($filesModel[0], 'file[]')->widget(FileInput::class, [
            'language' =>  strtolower(substr(Yii::$app->language, 0, 2)),
            'sortThumbs' => true,
            'options' => [
                'id' => 'expense-files',
                'multiple' => true,
            ],
            'pluginOptions' => [
                    'allowedFileExtensions' => ['png', 'jpg', 'jpeg', 'pdf', 'doc', 'docx', 'edoc', 'bdoc', 'xls', 'xlsx', 'zip'],
                'uploadAsync' => false,
                'maxFileSize' => 20000,
                'showRemove' => false,
                'showUpload' => false,
                'overwriteInitial' => false,
                'initialPreview' => $preview,
                'initialPreviewAsData' => true,
                'initialPreviewFileType' => 'pdf',
                'initialPreviewConfig' => $previewConfig,
                'initialPreviewShowDelete' => true,            
                
                'preferIconicPreview' => true, // this will force thumbnails to display icons for following file extensions
                'previewFileIcon' => Icon::show('file'),
                'allowedPreviewTypes' => null, // set to empty, null or false to disable preview for all types                
                'previewFileIconSettings' => [ // configure your icon file extensions
                        'png' => Icon::show('file-image', ['class' => 'text-info']),
                        'jpg' => Icon::show('file-image', ['class' => 'text-info']),
                        'jpeg' => Icon::show('file-image', ['class' => 'text-info']),
                        'pdf' => Icon::show('file-pdf', ['class' => 'text-danger']),
                        'doc' => Icon::show('file-word', ['class' => 'text-primary']),
                        'docx' => Icon::show('file-word', ['class' => 'text-primary']),
                        'edoc' => Icon::show('file-word', ['class' => 'text-primary']),
                        'bdoc' => Icon::show('file-word', ['class' => 'text-primary']),
                        'xls' => Icon::show('file-excel', ['class' => 'text-success']),
                        'xlsx' => Icon::show('file-excel', ['class' => 'text-success']),
                        'zip' => Icon::show('file-archive', ['class' => 'text-muted']),
                    ],                
                ]
            ])->label(Yii::t('files', 'Attachment'))->hint('Allowed file extensions: .png .jpg .jpeg .pdf .doc .docx .edoc .bdoc .xls .xlsx .zip');  
    }
    ?>
    
    <?php if(FSMAccessHelper::can('changeExpenseEbitda')){
        echo $form->field($model, 'report_plus')->widget(SwitchInput::class, [
            'pluginOptions' => [
                'onText' => Yii::t('common', 'Yes'),
                'offText' => Yii::t('common', 'No'),
                'handleWidth' => (!$isModal ? 'auto' : '45')
            ],
        ]);
    } ?>
    
    <?= $form->field($model, 'confirmed')->widget(DatePicker::class, [
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
        ]); 
    ?>
    
    <?= $form->field($model, 'pay_type')->widget(EnumInput::class, [
            'type' => EnumInput::TYPE_RADIOBUTTON,
            'options' => [
                'translate' => $model->payTypeList,
            ],
        ]); 
    ?>     
    
    <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-10" style="text-align: right;">
            <?= $model->SubmitButton; ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$this->registerJs(
    "var form = $('#expense-form');
    form.find('#rate').change(function(){
        form.yiiActiveForm('validateAttribute', 'expense-summa');                    
    });
    ",
    \yii\web\View::POS_END
);
?>