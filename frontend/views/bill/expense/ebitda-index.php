<?php
namespace common\models;

use Yii;
//use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\grid\GridView;

use kartik\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

use common\components\FSMExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel common\models\bill\search\ExpenseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$client_id = !empty($client_id) ? $client_id : null;
?>
<div id="expense-index">

    <?= Html::tag('H1', Html::tag('small', $searchModel->modelTitle(2))); ?>
    
    <?php 
    $columns = [
        //['class' => '\kartik\grid\SerialColumn'],
        [
            'attribute' => 'id',
            'width' => '75px',
            'hAlign' => 'center',
            'pageSummary' => $searchModel->getAttributeLabel('total'),
        ],
        [
            'attribute' => 'expense_type_id',
            'headerOptions' => ['class'=>'td-mw-150'],
            'value' => function ($model) {
                return isset($model->expense_type_id) ? $model->expense_type_name : null;
            },
        ],                         
        'doc_number',
        [
            'attribute' => 'doc_date',
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'headerOptions' => ['class'=>'td-mw-100'],
            'value' => function ($model) {
                return isset($model->doc_date) ? date('d-M-Y', strtotime($model->doc_date)) : null;
            },
        ],                          
        [
            'attribute'=>'first_client_id',
            'contentOptions' => [
                'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
            ],
            'value' => function ($model) {
                return 
                '<div style="overflow-x: auto;">'.
                    (!empty($model->first_client_id) ? 
                    Html::a($model->first_client_name, ['/client/view', 'id' => $model->first_client_id], ['target' => '_blank', 'data-pjax' => 0]) : null).
                '</div>';
            },                
            'format' => 'raw',
        ],                          
        [
            'attribute'=>'second_client_id',
            'contentOptions' => [
                'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
            ],
            'value' => function ($model) {
                return  
                '<div style="overflow-x: auto;">'.
                    (!empty($model->second_client_id) ?  
                    Html::a($model->second_client_name, ['/client/view', 'id' => $model->second_client_id], ['target' => '_blank', 'data-pjax' => 0]) : null).
                '</div>';
            },                
            'format' => 'raw',
        ], 
        [
            'attribute' => 'total_eur',
            'hAlign' => 'right',
            'headerOptions' => ['style'=>'text-align: center;'],
            'mergeHeader' => true,
            'value' => function ($model) {
                return isset($model->total_eur) ? $model->total_eur : null;
            },
            'xlFormat' => '###0.00',
            'exportMenuStyle' => [
                'numberFormat' => ['formatCode' => '###0.00'],
            ],                                               
            'format' => ['decimal', 2],
            'pageSummary' => true,                
        ],                      
        [
            'attribute' => 'vat_eur',
            'hAlign' => 'right',
            'headerOptions' => ['style'=>'text-align: center;'],
            'mergeHeader' => true,
            'value' => function ($model) use ($client_id) {
                if($model->first_client_id == $client_id){
                    return ($model->first_client_pvn_payer ? $model->vat_eur : 0);
                }elseif($model->second_client_id == $client_id){
                    return ($model->second_client_pvn_payer ? $model->vat_eur : 0);
                }else{
                    return $model->summa_eur;
                }
            }, 
            'xlFormat' => '###0.00',
            'exportMenuStyle' => [
                'numberFormat' => ['formatCode' => '###0.00'],
            ],                                               
            'format' => ['decimal', 2],
            'pageSummary' => true,                
        ],                       
        [
            'class' => '\common\components\FSMActionColumn',
            'headerOptions' => ['class'=>'td-mw-125'],
            'dropdown' => true,
            'template' => '{view} {update} {delete}',
            'controller' => 'expense',
        ],
    ];
    ?>

    <?= FSMExportMenu::widget([
        'options' => ['id' => 'expense-index-menu'],
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'showColumnSelector' => false,
        'showConfirmAlert' => false,
        'dropdownOptions' => [
            'label' => Yii::t('kvgrid', 'Export'),
            'class' => 'btn btn-default'
        ],
        'target' => ExportMenu::TARGET_SELF,
        'exportConfig' => [
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_HTML => false,
            ExportMenu::FORMAT_CSV => false,
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_PDF => false,
            ExportMenu::FORMAT_EXCEL => false,
            ExportMenu::FORMAT_EXCEL_X => ['label' => Yii::t('kvgrid', 'Excel'), 'alertMsg' => ''],
        ],
        'pjaxContainerId' => 'expense-index',
        'exportFormView' => '@vendor/kartik-v/yii2-export/views/_form',
    ]);?> 

    <p/>

    <?= GridView::widget([
        'id' => 'grid-view',
        'tableOptions' => [
            'id' => 'expense-list',
        ],
        'responsive' => false,
        'striped' => true,
        'hover' => true,
        'bordered' => true,
        'condensed' => true,
        'persistResize' => false,
        //'floatHeader' => true,
        'autoXlFormat' => true,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showPageSummary' => true,
        'pjax' => true,
        'columns' => $columns,
        'pjaxSettings' => ['options' => ['id' => 'expense-index']],
    ]); ?>
</div>