<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\bill\Expense */

$this->title = Yii::t($model->tableName(), 'Update a '.$model->modelTitle(1, false)) . ': ' . $model->doc_number;
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->doc_number, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Edit');
?>
<div class="expense-update">

    <?= Html::pageHeader(Html::encode($this->title)); ?>

    <?= $this->render('_form', [
        'model' => $model,
        'filesModel' => $filesModel,
        'projectList' => $projectList,
        'clientList' => $clientList,
        'expenseTypeList' => $expenseTypeList,
        'valutaList' => $valutaList,
        'isAdmin' => $isAdmin,
        'isModal' => false,
    ]) ?>

</div>