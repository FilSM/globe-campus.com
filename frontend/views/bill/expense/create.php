<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\bill\Expense */

$this->title = Yii::t($model->tableName(), 'Create a new '.$model->modelTitle(1, false));
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="expense-create">

    <?php if(!$isModal): ?>
    <?= Html::pageHeader(Html::encode($this->title));?>
    <?php endif; ?>  

    <?= $this->render('_form', [
        'model' => $model,
        'filesModel' => $filesModel,
        'projectList' => $projectList,
        'clientList' => $clientList,
        'expenseTypeList' => $expenseTypeList,
        'valutaList' => $valutaList,
        'isAdmin' => $isAdmin,
        'isModal' => $isModal,
    ]) ?>

</div>