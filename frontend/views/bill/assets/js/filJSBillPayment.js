(function ($) {

    //$(document).ready(function () {
        console.log('filJSBillPayment is loaded');
        
        var form = $("#bill_payment-form");
        
        form.on("change", ".billpayment-rate, .billpayment-valuta", function(){
            var idArr = $(this).attr('id').split('-');
            var index = idArr[1];            
            form.yiiActiveForm('validateAttribute', 'billpayment-'+index+'-rate');                    
        });        
        
        form.find('#cbx-new-payment-order').on('change', function() {
            var checked = ($(this).val() == 1);
            if(!checked){
                form.find('div.field-payment-order-id').show('slow');
                
                form.find('#payment-order-data-container').hide('slow');
                form.find('#payment-order-data-container input[type="text"]').val('');
                form.find('#payment-order-data-container select').val('').change();
            }else{
                form.find('div.field-payment-order-id').hide('slow');
                form.find('#payment-order-data-container').show('slow');
            }
            //console.log('checkbox changed');
        });
        
        form.find('#button-download-xml').on('click', function() {
            form.find('button[type="submit"], a.btn-cancel').css('display', 'none');
            form.find('#button-close').css('display', 'inline-block');
        });
    //});
    
})(window.jQuery);     