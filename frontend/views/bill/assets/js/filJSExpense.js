(function ($) {

    //$(document).ready(function () {
        
        var form = $("#expense-form");
        
        form.on("change", "#expense-summa, #expense-vat", 
            function(e) {
                var summa = parseFloat(form.find('#expense-summa').val());
                var summaVat = parseFloat(form.find('#expense-vat').val());
                summa = !isNaN(summa) ? filRound(summa, 2) : '0.00';
                summaVat = !isNaN(summaVat) ? filRound(summaVat, 2) : '0.00';
                
                var total = filRound(parseFloat(summa) + parseFloat(summaVat), 2);
                form.find('#expense-total').prop('value', total);
            }
        );
/*
        form.find('#expense-confirmed').on('change', function(){
            var val = $(this).val();
            if(empty(val)){
                form.find('.field-expense-pay_type').hide('slow');
                form.find('#expense-pay_type').val('');
                form.find('#expense-pay_type button').removeClass('active');
            } else {
                form.find('.field-expense-pay_type').show('slow');
            }
        }).change();
     * 
 */
    //});
    
})(window.jQuery);     