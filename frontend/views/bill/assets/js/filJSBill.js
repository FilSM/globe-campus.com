(function ($) {

    //$(document).ready(function () {
        console.log('filJSBill is loaded');
        
        var form = $("#bill-form");
        var clientTax = 0;
        var vatTaxable = 1;
        var billId = $_GET.id;
        //var billId = form.find('#bill-id').val();
        
        $('body').off('click.search').on('click.search', '#bill-index .search-toggle-button', showSearchButtonClick);
        function showSearchButtonClick(e, options){
            var billSearchBlock = $('#bill-index .bill-search');
            var sidebarIsVisible = $('#bill-index .bill-search').is(":visible");
            if(sidebarIsVisible){
                billSearchBlock.hide();
            } else {
                billSearchBlock.show();
            }
            $('table#bill-list').floatThead('reflow');
        }
        
        form.find('#e-signing').on('init.bootstrapSwitch switchChange.bootstrapSwitch', function(){
            var state = (arguments.length == 1 ? arguments[0] : (arguments[1] != undefined ? arguments[1] : null));
            console.log('Change state to '+state);
            switch (state) {
                case false:
                default:
                    form.find('.panel-person-data').show();
                    break;
                case true:
                    form.find('.panel-person-data').hide();
                    var selectList = form.find('.panel-person-data').find('.person-item-select');
                    selectList.val('').change();
                    break;
            }            
        });
        
        form.on("change", ".vat-revers input", function(){
            var $this = $(this);
            
            if(!vatTaxable){
                $this.val(0);
                return;
            }
            
            var checked = ($this.val() == 1);
            var idArr = $(this).attr('id').split('-')
            var index = idArr[1];
            var vatControl = form.find('#product-'+index+'-vat');
            vatControl.prop('readonly', checked);
            vatControl.val(checked ? '' : 0).change();
        });
        form.find('.vat-revers input[value="1"]').change();
        
        var currentValue = form.find('[name="Bill[doc_type]"]').val();
        switchByBillType(currentValue);
        
        function switchByBillType(billType){
            switch (billType) {
                case 'avans':
                case 'bill':
                default:
                    form.find('.field-e-signing').show("slow");
                    form.find('#waybill-data').hide("slow");
                    form.find('#waybill-data input[type="text"]').val('');
                    break;
                case 'invoice':
                    form.find('.field-e-signing').hide("slow");
                    form.find('[name="Bill[e_signing]"]').val(0);
                    form.find('#waybill-data').show("slow");
                    break;
            }
        }
        
        var accordingContractState = 0;
        
        form.find('#according-contract').on('init.bootstrapSwitch switchChange.bootstrapSwitch', checkAccordingContractState);
        
        function checkAccordingContractState(){
            accordingContractState = (arguments.length == 1 ? arguments[0] : (arguments[1] != undefined ? arguments[1] : null));
            var inputList = form.find('#product-data-container').find('.product-item-input');
            var selectList = form.find('#product-data-container').find('.product-item-select');
            //var measureSelect = form.find('#product-data-container').find('.measure-item-select');
            switch (accordingContractState) {
                case false:
                default:
                    inputList.closest('.form-group').hide();
                    selectList.closest('.form-group').show();
                    
                    inputList.val('');
                    break;
                case true:
                    selectList.closest('.form-group').hide();
                    inputList.closest('.form-group').show();
                    
                    selectList.val('').change();
                    break;
            }            
        }

        form.find('#first-client-id').on('depdrop:change', function(e) {
            var clientId = $(this).val();
            var url = appUrl + '/client/ajax-get-tax';
            if(empty(clientId)){
                return;
            }
            $.get(
                url,
                {id: clientId}, 
                function (data) {
                    data = JSON.parse(data);
                    clientTax = data[0].tax;
                    vatTaxable = data[0].vat_payer;
                    if(empty(billId)){
                        if(!vatTaxable){
                            form.find('.vat-revers input').val(0);
                        }
                        var templateId = form.find('#bill-bill_template_id').val();
                        if(empty(templateId)){
                            form.find('.bill-product-vat').val(clientTax).change();
                        }
                    }                     
                    form.find('.bill-product-vat').prop('readonly', !vatTaxable);
                    form.find('.vat-revers input').prop('readonly', !vatTaxable);                    
                    form.find('.vat-revers input').checkboxX('refresh');
                }
            );
        });

        form.find('#bill-doc_type button').click(function () {
            var billType = $(this).val();
            switchByBillType(billType);
        });
        
        form.on("change", ".product-item-select", 
            function(e) {
                var id = $(this).val();
                var idArr = $(this).attr('id').split('-')
                var index = idArr[1];
                if(!accordingContractState){
                    form.find('#product-'+index+'-measure_id').val('').change();
                }
                
                var url = appUrl + "/product/ajax-get-measure";
                $.get(
                    url,
                    {id: id}, 
                    function (data) {
                        //data = JSON.parse(data);
                        var measure = data[0].measure;
                        if(!accordingContractState){
                            form.find('#product-'+index+'-measure_id').val(measure.id).change();
                        }
                        
                        if(!empty(clientTax)){
                            form.find('#product-'+index+'-vat').prop('value', clientTax);
                        }else{
                            form.find('#product-'+index+'-vat').val('');
                        }
                    },
                    'json'
                );
                //alert('selected id = ' + id); 
            }
        );
        
        form.on("change", ".bill-product-amount, .bill-product-price, .bill-product-vat", 
            function(e) {
                var idArr = $(this).attr('id').split('-');
                var index = idArr[1];
                
                var amount = parseFloat(form.find('#product-'+index+'-amount').val());
                var price = parseFloat(form.find('#product-'+index+'-price').val());
                var vat = parseFloat(form.find('#product-'+index+'-vat').val());
                
                var summa = (!isNaN(amount) && !isNaN(price)) ? filRound(amount * price, 2) : '0.00';
                var summaVat = !isNaN(vat) ? filRound((summa * vat) / 100, 2) : '0.00';
                var total = filRound(parseFloat(summa) + parseFloat(summaVat), 2);
                
                form.find('#product-'+index+'-summa').prop('value', summa);
                form.find('#product-'+index+'-summa_vat').prop('value', summaVat);
                form.find('#product-'+index+'-total').prop('value', total);
                recalcBillSumma();
            }
        );
        //form.find('.bill-product-amount').trigger('change');
        
        form.on("change", ".bill-product-summa, .bill-product-summa_vat", 
            function(e) {
                var idArr = $(this).attr('id').split('-');
                var index = idArr[1];
                
                var summa = form.find('#product-'+index+'-summa').prop('value');
                var summaVat = form.find('#product-'+index+'-summa_vat').prop('value');
                
                summa = !isNaN(summa) ? filRound(summa, 2) : '0.00';
                summaVat = !isNaN(summaVat) ? filRound(summaVat, 2) : '0.00';
                total = filRound(parseFloat(summa) + parseFloat(summaVat), 2);                
                
                form.find('#product-'+index+'-total').prop('value', total);
                recalcBillSumma();
            }
        );
        
        form.find('.dynamicform_wrapper').bind("afterInsert", 
            function(e) {
                checkAccordingContractState(accordingContractState);
                form.find('.bill-product-vat').each(function(index, item){
                    var $this = $(item);
                    if((form.find('#product-'+index+'-product_id').val() === '') && !empty(clientTax)){
                        $this.val(clientTax);
                    }
                    $this.prop('readonly', !vatTaxable);
                });
                form.find('.vat-revers input').each(function(index, item){
                    var $this = $(item);
                    $this.prop('readonly', !vatTaxable)
                    $this.checkboxX('refresh');
                });                
            }
        );
        
        form.find('.dynamicform_wrapper').bind("afterDelete", 
            function(e) {
                recalcBillSumma();
            }
        );

        function recalcBillSumma(){
            var billSumma = 0;
            form.find('.bill-product-summa').each(function(index, item){
                var val = $(item).val();
                if(val){
                    billSumma += parseFloat($(item).val());
                }
            });
            billSumma = filRound(billSumma, 2);
            form.find('#bill-summa').prop('value', billSumma);
            
            var billSumma = 0;
            form.find('.bill-product-summa_vat').each(function(index, item){
                var val = $(item).val();
                if(val){
                    billSumma += parseFloat($(item).val());
                }
            });
            billSumma = filRound(billSumma, 2);
            form.find('#bill-vat').prop('value', billSumma);
            
            var billSumma = 0;
            form.find('.bill-product-total').each(function(index, item){
                var val = $(item).val();
                if(val){
                    billSumma += parseFloat($(item).val());
                }
            });
            billSumma = filRound(billSumma, 2);
            form.find('#bill-total').prop('value', billSumma);
        }
            
        $('#bill-search').find('#billsearch-project_id, #billsearch-agreement_id, #billsearch-first_client_id').on('select2:select', function (evt) {
            $('table#bill-list').floatThead('reflow');
            var id 
        });
            
        $('#bill-search').find('#billsearch-project_id, #billsearch-agreement_id, #billsearch-first_client_id').on('select2:unselect', function (evt) {
            $('table#bill-list').floatThead('reflow');
        });
        
        $('#bill-search #billsearch-project_id').on('change', function () {
            var ids = $(this).val(); 
            var url = appUrl + (!empty(ids) ? "/project/ajax-get-agreement-list" : "/agreement/ajax-get-full-agreement-list");
            $.post(
                url,
                {
                    depdrop_parents: {ids: ids},
                }, 
                function (data) {
                    //data = JSON.parse(data);
                    var newData = [];
                    if(!empty(data) && !empty(data.output)){
                        $.each(data.output, function(key, item){
                            newData.push({id: item.id, text: item.name})
                        });
                    }
                    var id = 'bill-search #billsearch-agreement_id';
                    var updatedSelect = $('#'+id);
                    var $s2Options = $(updatedSelect).attr('data-s2-options');
                    var configSelect2 = eval($(updatedSelect).attr('data-krajee-select2'));
                    configSelect2.data = newData;

                    $('#'+id).find('option').remove();
                    //updatedSelect.select2(configSelect2);
                    $.when(updatedSelect.select2(configSelect2)).done(initS2Loading(id, $s2Options));                    
                },
                'json'
            );            
        });
        
        $('body').off('click.selectall').on('click.selectall', '.select-on-check-all', function(){
            var btnAvans = $('#bill-write-on-basis-many', '#bill-index');
            var btnPayment = $('#bill-payments-create-many', '#bill-index');
            var btnConvention = $('#bill-convention-create', '#bill-index');
            var checked = $(this).prop('checked');
            recalcTotalSumma(true, checked);
            if(!checked){
                if(btnAvans.length > 0){
                    btnAvans.attr('disabled', true);
                }
                if(btnPayment.length > 0){
                    btnPayment.attr('disabled', true);
                }
                if(btnConvention.length > 0){
                    btnConvention.attr('disabled', true);
                }
                return;
            }
            setHrefMultiBtn(true);
        });
        
        $('body').off('click.selectone').on('click.selectone', '[name="selection[]"]', function(){
            var btnAvans = $('#bill-write-on-basis-many', '#bill-index');
            var btnPayment = $('#bill-payments-create-many', '#bill-index');
            var btnConvention = $('#bill-convention-create', '#bill-index');
            var checkedCB = $('[name="selection[]"]:checked');
            var checked = checkedCB.length > 0;
            recalcTotalSumma(false, checked);
            if(!checked){
                if(btnAvans.length > 0){
                    btnAvans.attr('disabled', true);
                }
                if(btnPayment.length > 0){
                    btnPayment.attr('disabled', true);
                }
                if(btnConvention.length > 0){
                    btnConvention.attr('disabled', true);
                }
                return;
            }
            setHrefMultiBtn(false);
        });
        
        function recalcTotalSumma(all, checked){
            var keys = [];
            if(all){
                if(checked){
                    var cbList = $('[name="selection[]"]');
                    cbList.each(function(index, item){
                        keys.push($(item).val());
                    });
                }
            }else{
                keys = $('#grid-view').yiiGridView('getSelectedRows');
            }
            
            var sumSelected = 0;
            var currency = 0;
            var sumCell;
            var currencyCell;
            var currencyError = 0;
            keys.forEach(function(item){
                sumCell = $('#bill-list tr[data-key="'+item+'"] .cell-total');
                if(sumCell.length > 0){
                    currencyCell = sumCell.data('valuta');
                    if(currency == 0){
                        currency = currencyCell;
                    }else if(currency != currencyCell){
                        sumSelected = 0;
                        currencyError = 1;
                        return;
                    }
                    sumSelected += parseFloat(sumCell.data('sum'));
                }
            });
            
            if(!currencyError){
                $('#bill-index #total-selected-sum').html(filRound(sumSelected, 2)).css({color: 'black'});
            }else{
                $('#bill-index #total-selected-sum').html('0.00 | Currency error').css({color: 'red'});
            }
        }
        
        function setHrefMultiBtn(all){
            var btnAvans = $('#bill-write-on-basis-many', '#bill-index');
            var btnPayment = $('#bill-payments-create-many', '#bill-index');
            var btnConvention = $('#bill-convention-create', '#bill-index');
            var keys = [];
            if(all){
                var cbList = $('[name="selection[]"]');
                cbList.each(function(index, item){
                    keys.push($(item).val());
                });
            }else{
                keys = $('#grid-view').yiiGridView('getSelectedRows');
            }
            
            if(keys.length > 0){
                if(btnAvans.length > 0){
                    var attribute = 'href';
                    var href = btnAvans.attr('href');
                    var oldHref = btnAvans.prop('oldHref');
                    if(oldHref == undefined){
                        btnAvans.prop('oldHref', href);
                    }else{
                        href = oldHref;
                    }
                    var ids = keys.join();
                    href = href + '?ids=' + ids;
                    btnAvans.attr(attribute, href);
                    btnAvans.attr('disabled', false);
                }
                if(btnPayment.length > 0){
                    var attribute = 'value';
                    var href = btnPayment.attr(attribute);
                    var oldHref = btnPayment.prop('oldHref');
                    if(oldHref == undefined){
                        btnPayment.prop('oldHref', href);
                    }else{
                        href = oldHref;
                    }
                    var ids = keys.join();
                    href = href + '?ids=' + ids;
                    btnPayment.attr(attribute, href);
                    btnPayment.attr('disabled', false);
                }
                if(btnConvention.length > 0){
                    var attribute = 'value';
                    var href = btnConvention.attr(attribute);
                    var oldHref = btnConvention.prop('oldHref');
                    if(oldHref == undefined){
                        btnConvention.prop('oldHref', href);
                    }else{
                        href = oldHref;
                    }
                    var ids = keys.join();
                    href = href + '?ids=' + ids;
                    btnConvention.attr(attribute, href);
                    btnConvention.attr('disabled', false);
                }                
            }
        }
            
        form.find('#project-id').change(function(){
            form.find('#project-static-text').hide('slow').find('.form-control-static').html('');

            var $this = $(this);
            var id = $this.val();
            var url = appUrl + "/project/ajax-get-model";

            $.get(
                url,
                {id: id}, 
                function (data) {
                    if(empty(data)){
                        return false;
                    }
                    vatTaxable = data.vat_taxable;
                    if(!vatTaxable){
                        var message = lajax.t('This project is non taxable and you will not have the opportunity to charge VAT');
                        form.find('#project-static-text').show('slow').find('.form-control-static').html(message);
                        form.find('.bill-product-vat').val('').change();
                        form.find('.vat-revers input').val(0);
                    }
                    form.find('.bill-product-vat').prop('readonly', !vatTaxable);
                    form.find('.vat-revers input').prop('readonly', !vatTaxable);
                }
            );                    
        });
        
        form.find('a.btn-cancel').click(function(){
            var reservedId = form.find('#bill-reserved-id').val();
            if(!empty(reservedId)){
                var url = appUrl + "/bill/ajax-clear-reserved-id";
                $.get(
                    url,
                    {reservedId: reservedId}, 
                    function (data) {
                        var result = data.result;
                    }
                );             
            }
        });
        
        var initFormMode = 1;
        form.find('#agreement-id').on('change', function(event) {
            var id = $(this).val();
            var emptyId = empty(id);

            var btnRefreshAgreement = form.find('.group-agreement-id .refresh-list-button');
            if(btnRefreshAgreement.length > 0){
                btnRefreshAgreement.attr('disabled', (emptyId ? 'disabled' : false));
            }

            var btnViewAgreement = form.find('#btn-view-agreement');
            if(btnViewAgreement.length > 0){
                var href = btnViewAgreement.attr('href').split('?');
                var display = (emptyId ? 'none' : 'inline-block');
                btnViewAgreement.attr('href', href[0] + '?id=' + id);
                btnViewAgreement.css({'display': display});
            }

            var btnViewClient = form.find('#btn-view-first-client');
            if(btnViewClient.length > 0){
                href = btnViewClient.attr('href').split('?');
                btnViewClient.attr('href', href[0]);
                btnViewClient.prop({disabled: true});

                btnViewClient = form.find('#btn-view-second-client');
                btnViewClient.attr('href', href[0]);
                btnViewClient.prop({disabled: true});
            }

            if (emptyId){
                form.find('#first-client-role').val('');
                form.find('#first-client-id').val('');
                form.find('#first-client-name').val('');
                form.find('#first-client-reg').val('');
                form.find('#first-client-vat').val('');
                form.find('#first-client-address').val('');

                form.find('#second-client-role').val('');
                form.find('#second-client-id').val('');
                form.find('#second-client-name').val('');
                form.find('#second-client-reg').val('');
                form.find('#second-client-vat').val('');
                form.find('#second-client-address').val('');

                form.find('#first-client-id').trigger('depdrop:change');
                form.find('#second-client-id').trigger('depdrop:change');

                return false;
            }

            var reservedId = form.find('#bill-reserved-id').val();
            var url = appUrl + '/agreement/ajax-get-model';
            $.get(
                url,
                {
                    id: id, 
                    reservedId: reservedId
                }, 
                function (data) {
                    var agreement = data.agreement;
                    var first_client = data.first_client;
                    var second_client = data.second_client;

                    form.find('#first-client-role').val(data.first_client_role);
                    form.find('#first-client-id').val(first_client.id);
                    form.find('#first-client-name').val(first_client.name);
                    form.find('#first-client-reg').val(first_client.reg_number);
                    form.find('#first-client-vat').val(first_client.vat_number);
                    form.find('#first-client-address').val(data.first_client_address);

                    form.find('#second-client-role').val(data.second_client_role);
                    form.find('#second-client-id').val(second_client.id);
                    form.find('#second-client-name').val(second_client.name);
                    form.find('#second-client-reg').val(second_client.reg_number);
                    form.find('#second-client-vat').val(second_client.vat_number);
                    form.find('#second-client-address').val(data.second_client_address);

                    form.find('#first-client-id').trigger('depdrop:change');
                    form.find('#second-client-id').trigger('depdrop:change');

                    var btnViewClient = form.find('#btn-view-first-client');
                    if(btnViewClient.length > 0){
                        var href = btnViewClient.attr('href').split('?');
                        btnViewClient.attr('href', href[0]+'?id='+first_client.id);
                        btnViewClient.attr('disabled', empty(first_client.id));

                        btnViewClient = form.find('#btn-view-second-client');
                        btnViewClient.attr('href', href[0]+'?id='+second_client.id);
                        btnViewClient.attr('disabled', empty(second_client.id));
                    }

                    var billId = form.find('#bill-id').val();
                    if(empty(billId) || !initFormMode){
                        var docDate = form.find('#bill-doc_date').val();
                        var docDateInt = strtotime(docDate);
                        var dueDateInt = strtotime('+' + agreement.deferment_payment + ' days', docDateInt); 
                        var dueDate = date('d-m-Y', dueDateInt);
                        form.find('#bill-doc_date').data('deferment_payment', agreement.deferment_payment);
                        form.find('#bill-pay_date').val(dueDate);
                    }
                    if(initFormMode && (event.type == 'depdrop:change')){
                        initFormMode = 0;
                    }

                    if(!empty(first_client.lastNumber)){
                        var billId = form.find('#bill-id').val();
                        if(empty(billId)){
                            form.find('#bill-doc_number').val(first_client.lastNumber);
                        }
                    }
                }
            );
            //alert('selected id = ' + clientId); 
        }).change();
        
        form.find('.group-agreement-id .refresh-list-button').on('click', function(){
            var btnViewAgreement = form.find('#btn-view-agreement');
            if(btnViewAgreement.length > 0){
                var href = btnViewAgreement.attr('href').split('?');
                btnViewAgreement.attr('href', href[0]);
                btnViewAgreement.css({'display': 'none'});
            }
        });
        
        form.find('#rate, #valuta-id').change(function(){
            form.yiiActiveForm('validateAttribute', 'bill-total');                    
        });
    
        form.find('#bill-doc_number').change(function(e) {
            var $this = $(this);
            var value = $this.val();
            var url = appUrl + '/bill/ajax-validate-unique-number';
            
            $.get(
                url,
                {
                    doc_number: value,
                    id: billId 
                },
                function (data) {
                    if(!empty(data.exists)){
                        if(!empty(data.message)){
                            krajeeDialogWarning.alert(data.message);
                        }
                        //alert('No data!');
                        return false;
                    }
                });
            //alert('selected id = ' + id); 
        });      

    //});
    
})(window.jQuery);     