(function ($) {

    //$(document).ready(function () {
        
        var form = $("#bill_confirm-form");
        var manualInput = form.find('#billconfirm-manual_input').val();
        var confirmVat = form.find('#doc-0-vat').val();
        var confirmSumma = form.find('#doc-0-summa').val();
        
        form.on("change", ".doc_type-item-select", function(e) {
            var docType = $(this).val();
            console.log('Change doc type to '+docType);
            var idArr = $(this).attr('id').split('-')
            var index = idArr[1];

            var btnAdd = form.find('#doc-'+index+'-doc_id .show-modal-button');
            var btnRefresh = form.find('#doc-'+index+'-doc_id .refresh-list-button');
            var btnView = form.find('#doc-'+index+'-doc_id .btn-view-doc');
            
            var pathArr = btnAdd.val().split('/');
            var partCount = pathArr.length;
            var typeIndex = partCount - 2;
            
            pathArr[typeIndex] = docType;
            var newPath = implode('/', pathArr);
            btnAdd.val(newPath);
            var display = (empty(docType) || (docType == 'tax') ? 'none' : 'inline-block');
            var disabled = (display == 'none');
            btnAdd.attr('disabled', disabled || (docType == 'tax'));
            form.find('#doc-'+index+'-vat').attr('readonly', (docType != 'direct'));
            form.find('#doc-'+index+'-summa').attr('readonly', disabled);

            if(btnRefresh.length > 0){
                pathArr = btnRefresh.val().split('/');
                pathArr[typeIndex] = docType;
                newPath = implode('/', pathArr);
                btnRefresh.val(newPath);
                btnRefresh.css({'display': display});
            }

            if(btnView.length > 0){
                pathArr = btnView.attr('href').split('/');
                pathArr[typeIndex] = docType;
                newPath = implode('/', pathArr);
                btnView.attr('href', newPath);
                btnView.css({'display': display});
            }
            
            if(!in_array(docType, ['direct', 'tax']) || (in_array(docType, ['direct', 'tax']) && (form.find('.form-docs-item').length > 1))){
                form.find('#doc-'+index+'-vat').prop('value', 0);
                form.find('#doc-'+index+'-summa').prop('value', 0);
            }else if(in_array(docType, ['direct', 'tax']) && (form.find('.form-docs-item').length = 1)){
                form.find('#doc-'+index+'-vat').prop('value', confirmVat);
                form.find('#doc-'+index+'-summa').prop('value', confirmSumma);
            }
            recalcConfirmSumma();
        });

        form.on("depdrop:change change", ".doc-doc_id", function(e) {
            var $this = $(this);
            var id = $this.val();
            console.log('Change doc ID to '+id);
            var emptyId = empty(id);

            var idArr = $this.attr('id').split('-')
            var index = idArr[1];

            var mainClientId = form.find('#billconfirm-main_client_id').val();
            var docType = form.find('#doc-'+index+'-doc_type').val();

            var btnViewDoc = form.find('#btn-'+index+'-view-doc');
            var href = btnViewDoc.attr('href').split('?');
            var display = (emptyId || in_array(docType, ['direct', 'tax']) ? 'none' : 'inline-block');
            btnViewDoc.prop('href', href[0] + '?id=' + id);
            btnViewDoc.css({'display': display});

            if(!emptyId && $.isNumeric(id) && (docType != 'direct')){
                var url = appUrl + '/'+docType+'/ajax-get-doc-confirm';
                $.get(
                    url,
                    {
                        id: id, 
                        for_client_id: mainClientId
                    }, 
                    function (data) {
                        //data = JSON.parse(data);
                        if(!empty(data)){
                            if(!empty(manualInput)){
                                if(!empty(data.firstClientBankAccount)){
                                    form.find('#first-client-account-static-text input').val(data.firstClientBankAccount).attr('readonly', true);
                                }else{
                                    form.find('#first-client-account-static-text input').val('').attr('readonly', false);
                                }
                                if(!empty(data.secondClientName)){
                                    form.find('#second-client-name-static-text input').val(data.secondClientName).attr('readonly', true);
                                }else{
                                    form.find('#second-client-name-static-text input').val('').attr('readonly', false);
                                }
                                if(!empty(data.secondClientReg)){
                                    form.find('#second-client-reg-number-static-text input').val(data.secondClientReg).attr('readonly', true);
                                }else{
                                    form.find('#second-client-reg-number-static-text input').val('').attr('readonly', false);
                                }
                                if(!empty(data.secondClientBankAccount)){
                                    form.find('#second-client-account-static-text input').val(data.secondClientBankAccount).attr('readonly', true);
                                }else{
                                    form.find('#second-client-account-static-text input').val('').attr('readonly', false);
                                }

                                $.when(form.find('#doc-'+index+'-summa').prop('value', filRound(data.notPaidSumma, 2))).done(recalcConfirmSumma());
                                //form.find('#doc-'+index+'-summa').prop('value', filRound(data.notPaidSumma, 2)).change();
                                form.find('#rate').val(data.rate);
                                form.find('#valuta-id').val(data.currency).change();                        
                            
                                form.find('#billconfirm-second_client_id').val(data.secondClientId);
                            }else{
                                form.find('#doc-'+index+'-summa').prop('value', filRound(data.notPaidSumma, 2));
                                form.find('#billconfirm-rate').val(data.rate);
                                form.find('#billconfirm-valuta_id').val(data.currency);
                                var secondClientId = form.find('#billconfirm-second_client_id').val();
                                if(empty(secondClientId)){
                                    form.find('#billconfirm-second_client_id').val(data.secondClientId);
                                }
                                recalcConfirmSumma();
                            }
                        }
                    },
                    'json'
                );
            }else{
                if(!empty(manualInput) && !in_array(docType, ['direct', 'tax'])){
                    form.find('#first-client-account-static-text input').val('').attr('readonly', true);
                    form.find('#second-client-name-static-text input').val('').attr('readonly', true);
                    form.find('#second-client-reg-number-static-text input').val('').attr('readonly', true);
                    form.find('#second-client-account-static-text input').val('').attr('readonly', true);

                    form.find('#rate').val('1.0000');
                    form.find('#valuta-id').val('').change();
                    recalcConfirmSumma();
                }else{
                    if(!in_array(docType, ['direct', 'tax']) || (in_array(docType, ['direct', 'tax']) && (form.find('.form-docs-item').length > 1))){
                        form.find('#doc-'+index+'-summa').prop('value', filRound(0, 2));
                    }                    
                    form.find('#billconfirm-rate').val('1.0000');
                    form.find('#billconfirm-valuta_id').val('');
                }
            }
            form.find('#billconfirmlink-'+index+'-doc_id').attr('disabled', in_array(docType, ['tax']));
            checkRequiredInputs(form);
        });
        
        form.on("change", ".doc-summa", function(e) {
            recalcConfirmSumma();
        });
        
        form.find('.refresh-list-button').on('click', function(){
            var btnViewAgreement = $(this).closest('.input-group').find('.btn-view-doc');
            if(btnViewAgreement.length > 0){
                var href = btnViewAgreement.attr('href').split('?');
                btnViewAgreement.attr('href', href[0]);
                btnViewAgreement.css({'display': 'none'});
            }
        });
        
        form.find('.dynamicform_wrapper').bind("afterInsert", 
            function(e, addedRow) {
                console.log('Added new doc');                
                $(addedRow).find('.doc-doc_id').find('option').remove();
                $(addedRow).find('.doc_type-item-select').change();
            }
        ); 

        form.find('.dynamicform_wrapper').bind("afterDelete", 
            function(e) {
                console.log('Deleted doc');                
                recalcConfirmSumma();
            }
        );        

        function recalcConfirmSumma(){
            var confirmSumma = 0;
            form.find('.doc-summa').each(function(index, item){
                var val = $(item).val();
                if(val){
                    confirmSumma += parseFloat($(item).val());
                }
            });
            confirmSumma = number_format(confirmSumma, 2, '.', '');
            form.find('#total-row #total-input').prop('value', confirmSumma);
            
            if(empty(manualInput)){
                return;
            }
            form.find('#billconfirm-summa').prop('value', confirmSumma);
        }
                
        form.find('#rate, #valuta-id').change(function(){
            form.yiiActiveForm('validateAttribute', 'billconfirm-summa');                    
        }); 
    //});
    
})(window.jQuery);     