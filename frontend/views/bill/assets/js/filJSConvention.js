(function ($) {

    //$(document).ready(function () {
        
        var form = $("#convention-form");

        form.on("change", ".convention-bill-bill", 
            function(e) {
                var id = $(this).val();
                var idArr = $(this).attr('id').split('-')
                var index = idArr[1];
                if(empty(id)){
                    return false;
                }

                var url = appUrl + "/bill/ajax-get-model";
                $.get(
                    url,
                    {id: id}, 
                    function (data) {
                        //data = JSON.parse(data);
                        if(!empty(data)){
                            form.find('#conventionbill-'+index+'-summa').val(data.notPaidSumma);
                            form.find('#conventionbill-'+index+'-rate').val(data.rate).change();
                            form.find('#conventionbill-'+index+'-valuta_id').val(data.valuta_id).change();
                            checkRequiredInputs(form);
                        }
                    },
                    'json'
                );
                //alert('selected id = ' + id); 
            }
        );

        $('body').off('change.conventionsumma').on('change.conventionsumma', '.convention-bill-summa, .convention-bill-rate', changeConventionSumma);
        function changeConventionSumma(){
            var idArr = $(this).attr('id').split('-');
            var index = idArr[1];

            var summa = parseFloat(form.find('#conventionbill-'+index+'-summa').val());
            var summa = !isNaN(summa) ? filRound(summa, 2) : '0.00';
            var rate = parseFloat(form.find('#conventionbill-'+index+'-rate').val());
            var rate = !isNaN(rate) ? filRound(rate, 2) : '0.00';

            form.find('#conventionbill-'+index+'-summa').prop('value', summa);
            form.find('#conventionbill-'+index+'-rate').prop('value', rate);
            recalcConventionSumma();
        }

        form.find('.convention-bill-summa').trigger('change');
        
        form.find('.dynamicform_wrapper .delete-item').on("click", 
            function(e) {
                var countInput = 0;
                form.find('.convention-bill-bill_number').each(function(index, item){
                    countInput++;
                });
                
                if(countInput == 1){
                    form.find('.convention-bill-bill_number').html('');
                }
            }
        );
/*
        form.find('.dynamicform_wrapper').bind("afterInsert", 
            function(e) {
                return false;
            }
        );
*/
        form.find('.dynamicform_wrapper').bind("afterDelete", 
            function(e) {
                recalcConventionSumma();
            }
        );

        function recalcConventionSumma(){
            var conventionSumma = 0;
            form.find('.convention-bill-summa').each(function(index, item){
                var summa = parseFloat(form.find('#conventionbill-'+index+'-summa').val());
                var summa = !isNaN(summa) ? filRound(summa, 2) : '0.00';
                var rate = parseFloat(form.find('#conventionbill-'+index+'-rate').val());
                var rate = !isNaN(rate) ? filRound(rate, 2) : '0.00';
                
                var summaEur = rate > 0 ? filRound((summa / rate), 2) : '0.00';
                conventionSumma += parseFloat(summaEur);
            });
            conventionSumma = filRound(conventionSumma, 2);
            form.find('#convention-summa_eur').prop('value', conventionSumma);
        }
            
        //form.find('#agreement-id').on('change depdrop:change', function(event) {
        form.find('#agreement-id').on('change', function(event) {
            var id = $(this).val();
            var emptyId = empty(id);

            var btnViewAgreement = form.find('#btn-view-agreement');
            if(btnViewAgreement.length > 0){
                var href = btnViewAgreement.attr('href').split('?');
                btnViewAgreement.attr('href', href[0] + '?id=' + id);
                btnViewAgreement.attr('disabled', (emptyId ? 'disabled' : false));
            }

            var btnViewClient = form.find('#btn-view-first-client');
            if(btnViewClient.length > 0){
                href = btnViewClient.attr('href').split('?');
                btnViewClient.attr('href', href[0]);
                btnViewClient.prop({disabled: true});

                btnViewClient = form.find('#btn-view-second-client');
                btnViewClient.attr('href', href[0]);
                btnViewClient.prop({disabled: true});
            }

            if (emptyId){
                form.find('#first-client-role').val('');
                form.find('#first-client-id').val('');
                form.find('#first-client-name').val('');
                form.find('#first-client-reg').val('');
                form.find('#first-client-vat').val('');
                form.find('#first-client-address').val('');

                form.find('#second-client-role').val('');
                form.find('#second-client-id').val('');
                form.find('#second-client-name').val('');
                form.find('#second-client-reg').val('');
                form.find('#second-client-vat').val('');
                form.find('#second-client-address').val('');

                form.find('#first-client-id').trigger('depdrop:change');
                form.find('#second-client-id').trigger('depdrop:change');

                return false;
            }

            var url = appUrl + '/agreement/ajax-get-model';
            $.get(
                url,
                {
                    id: id, 
                }, 
                function (data) {
                    var agreement = data.agreement;
                    var first_client = data.first_client;
                    var second_client = data.second_client;

                    form.find('#first-client-role').val(data.first_client_role);
                    form.find('#first-client-id').val(first_client.id);
                    form.find('#first-client-name').val(first_client.name);
                    form.find('#first-client-reg').val(first_client.reg_number);
                    form.find('#first-client-vat').val(first_client.vat_number);
                    form.find('#first-client-address').val(data.first_client_address);

                    form.find('#second-client-role').val(data.second_client_role);
                    form.find('#second-client-id').val(second_client.id);
                    form.find('#second-client-name').val(second_client.name);
                    form.find('#second-client-reg').val(second_client.reg_number);
                    form.find('#second-client-vat').val(second_client.vat_number);
                    form.find('#second-client-address').val(data.second_client_address);

                    form.find('#first-client-id').trigger('depdrop:change');
                    form.find('#second-client-id').trigger('depdrop:change');

                    var btnViewClient = form.find('#btn-view-first-client');
                    if(btnViewClient.length > 0){
                        var href = btnViewClient.attr('href').split('?');
                        btnViewClient.attr('href', href[0]+'?id='+first_client.id);
                        btnViewClient.attr('disabled', empty(first_client.id));

                        btnViewClient = form.find('#btn-view-second-client');
                        btnViewClient.attr('href', href[0]+'?id='+second_client.id);
                        btnViewClient.attr('disabled', empty(second_client.id));
                    }
                    
                    var deleteBtnList = form.find('.delete-item');
                    if(deleteBtnList.length > 0){
                        deleteBtnList.each(function(index, item){
                            if(index != 0){
                                $(this).trigger('click');
                            }else{
                                form.find('#conventionbill-0-bill_id').val('');
                                form.find('#conventionbill-0-summa').val('');
                                form.find('#conventionbill-0-rate').val('');
                                form.find('#conventionbill-0-valuta_id').val('');
                            }
                        });                
                        recalcConventionSumma();
                    }
                    getActiveBillList(agreement.id);
                }
            );
            //alert('selected id = ' + clientId); 
        });
        
        function getActiveBillList(agreement_id){
            var url = appUrl + '/agreement/ajax-get-active-bill-list';
            $.get(
                url,
                {
                    id: agreement_id, 
                }, 
                function (data) {
                    data = JSON.parse(data);
                    
                    var newData = [];
                    if(!empty(data) && !empty(data.output)){
                        $.each(data.output, function(key, item){
                            newData.push({id: item.id, text: item.doc_number})
                        });
                    }
                    var id = 'conventionbill-0-bill_id';
                    var updatedSelect = $('#'+id);
                    var $s2Options = $(updatedSelect).attr('data-s2-options');
                    var configSelect2 = eval($(updatedSelect).attr('data-krajee-select2'));
                    configSelect2.data = newData;

                    updatedSelect.find('option').remove();
                    //updatedSelect.select2(configSelect2);
                    $.when(updatedSelect.select2(configSelect2)).done(initS2Loading(id, $s2Options));
                    var value = updatedSelect.val();
                    if(!empty(value)){
                        updatedSelect.change();
                    }
                }
            );
        }
        
        $('body').off('change.conventionbillrate').on('change.conventionbillrate', '.convention-bill-rate, .convention-bill-valuta', 
            function(){
                var idArr = $(this).attr('id').split('-');
                var index = idArr[1];            
                form.yiiActiveForm('validateAttribute', 'conventionbill-'+index+'-rate');                    
            }
        );        

    //});
    
})(window.jQuery);     