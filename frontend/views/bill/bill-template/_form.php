<?php
namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use kartik\widgets\DatePicker;

use common\components\FSMAccessHelper;
use common\widgets\EnumInput;

/* @var $this yii\web\View */
/* @var $model common\models\bill\Bill */
/* @var $form yii\widgets\ActiveForm */

$isModal = !empty($isModal);
$isAdmin = !empty($isAdmin);
?>

<div class="bill-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => 'bill-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>

    <?= Html::activeHiddenInput($model, 'id', ['id' => 'bill-id', 'value' => $model->id]); ?>
    
    <?= $form->field($abonentModel, 'project_id')->widget(Select2::class, [
        'data' => $projectList,
        'options' => [
            'id' => 'project-id',
            'placeholder' => '...',
        ],
        'pluginOptions' => [
            'allowClear' => !$abonentModel->isAttributeRequired('project_id'),
            'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
        ],
        'addon' => !FSMAccessHelper::can('createProject') ? 
            null : 
            [
            'prepend' => $model::getModalButtonContent([
                'formId' => $form->id,
                'controller' => 'project',
            ]),
        ],     
    ]);?>

    <?= $form->field($model, 'agreement_id')->widget(Select2::class, [
        'data' => $agreementList,
        'options' => [
            'id' => 'agreement-id',
            'placeholder' => '...',
        ],
        'pluginOptions' => [
            'allowClear' => !$model->isAttributeRequired('agreement_id'),
            'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
        ],
        'addon' => !FSMAccessHelper::can('createAgreement') ? 
            null : 
            [
            'prepend' => [
                'content' => 
                $model->getModalButton([
                    'formId' => $form->id,
                    'controller' => 'agreement',
                    'addNewBtnTitle' => Yii::t('bill', 'Add new agreement'),
                    'parent' => (!empty($abonentModel->project_id) ? 
                        [
                            'field_name' => 'project_id',
                            'id' => $abonentModel->project_id
                        ] :
                        null),
                    'options' => [
                        'disabled' => empty($abonentModel->project_id),
                        //'style' => 'display: none;',
                    ],
                ]).
                Html::a(Html::icon('eye-open'), 
                    Url::to(['/agreement/view', 'id' => $model->agreement_id]),
                    [
                        'id' => 'btn-view-agreement', 
                        'class'=>'btn btn-info',
                        'target' => '_blank',
                        'style' => empty($model->agreement_id) ? 'display: none;' : '',
                        'title' => Yii::t('client', 'View agreement data'),
                    ]
                ),
                'asButton' => true, 
            ],
        ],
    ]);?>

    <?= $form->field($model, 'e_signing')->widget(SwitchInput::class, [
        'options' => [
            'id' => 'e-signing',
        ],        
        'pluginOptions' => [
            'onText' => Yii::t('common', 'Yes'),
            'offText' => Yii::t('common', 'No'),
        ],
        /*
        'pluginEvents' => [
            "switchChange.bootstrapSwitch" => "function() { console.log('switchChange'); }",
        ],
         * 
         */
    ]); ?>
        
    <fieldset id="client-data" style="margin-bottom: 10px;">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-10">
                <div class="col-md-6 double-line-top double-line-bottom first-client-data" style="background: aliceblue;">
                    <div class="col-md-12" style="padding: 0;">
                        <div class="field-group-title"><h3 id="first-party-title"><?= Yii::t('bill', 'First party data'); ?></h3></div>
                    </div>

                    <div class="col-md-12" style="padding: 0;">
                        <?= Html::activeHiddenInput($firstClientModel, 'id', ['id' => 'first-client-id']); ?>

                        <div class="form-group static-input">
                            <?= Html::label(Yii::t('bill', 'Party role'), 'first-client-address', ['class' => 'control-label col-md-3']); ?>
                            <div class="col-md-9">
                                <?= Html::textInput('first_client_role', 
                                    (!empty($agreementModel->first_client_role_id) ? $agreementModel->firstClientRole->name : null), [
                                    'id' => 'first-client-role', 
                                    'class' => 'form-control', 
                                    'disabled' => true,
                                ]); ?>
                            </div>
                        </div>                        
                        
                        <?= $form->field($firstClientModel, 'name',[
                            'options' => [
                                'class' => 'form-group static-input',
                            ],
                            'horizontalCssClasses' => [
                                'label' => 'col-md-3',
                                'wrapper' => 'col-md-9',
                            ], 
                            'template' => '{label} <div class="col-md-9">{input}</div>',
                            'addon' => !FSMAccessHelper::can('viewClient') ? 
                                null : [
                                'prepend' => [
                                    'content' => 
                                        Html::a(Html::icon('eye-open'), 
                                            Url::to(['/client/view', 'id' => $firstClientModel->id]),
                                            [
                                                'id' => 'btn-view-first-client', 
                                                'class'=>'btn btn-info',
                                                'target' => '_blank',
                                                'title' => Yii::t('client', 'View client data'),
                                                'disabled' => empty($firstClientModel->id),
                                            ]
                                        ),
                                    'asButton' => true, 
                                ],
                            ],
                        ])->textInput([
                            'id' => 'first-client-name', 
                            'disabled' => true,
                            'style' => 'font-weight: bold; font-size: x-large;',
                        ])->label(Yii::t('client', 'Full name')); 
                        ?>    
                        
                        <?= $form->field($firstClientModel, 'reg_number',[
                            'options' => [
                                'class' => 'form-group static-input',
                            ],
                            'horizontalCssClasses' => [
                                'label' => 'col-md-3',
                                'wrapper' => 'col-md-9',
                            ], 
                            'template' => '{label} <div class="col-md-9">{input}</div>',
                        ])->textInput([
                            'id' => 'first-client-reg',
                            'disabled' => true,
                        ])->label(Yii::t('client', 'Reg.number')); 
                        ?>
                        
                        <?= $form->field($firstClientModel, 'vat_number',[
                            'options' => [
                                'class' => 'form-group static-input',
                            ],
                            'horizontalCssClasses' => [
                                'label' => 'col-md-3',
                                'wrapper' => 'col-md-9',
                            ], 
                            'template' => '{label} <div class="col-md-9">{input}</div>',
                        ])->textInput([
                            'id' => 'first-client-vat',
                            'disabled' => true,
                        ])->label(Yii::t('client', 'VAT number')); 
                        ?>

                        <?= $form->field($firstClientModel, 'legal_address',[
                            'options' => [
                                'class' => 'form-group static-input',
                            ],
                            'horizontalCssClasses' => [
                                'label' => 'col-md-3',
                                'wrapper' => 'col-md-9',
                            ], 
                            'template' => '{label} <div class="col-md-9">{input}</div>',
                        ])->textInput([
                            'id' => 'first-client-address',
                            'value' => $firstClientModel->fullLegalAddress,
                            'disabled' => true,
                        ])->label(Yii::t('client', 'Legal address')); 
                        ?>

                        <?= $form->field($model, 'first_client_bank_id',[
                            'horizontalCssClasses' => [
                                'label' => 'col-md-3',
                                //'offset' => 'col-sm-offset-4',
                                'wrapper' => 'col-md-9',
                                //'error' => '',
                                //'hint' => '',
                            ],                            
                        ])->widget(DepDrop::class, [
                            'type' => DepDrop::TYPE_SELECT2,
                            'data' => empty($model->first_client_bank_id) ? null : [$model->first_client_bank_id => $model->firstClientBank->bank->name . ' | ' . $model->firstClientBank->account . (!empty($model->firstClientBank->name) ? ' ( '.$model->firstClientBank->name . ' )' : '')],
                            'select2Options' => [
                                'pluginOptions' => [
                                    'allowClear' => !$model->isAttributeRequired('first_client_bank_id'),
                                    'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
                                ],
                            ],
                            'pluginOptions' => [
                                'depends' => ['first-client-id'],
                                'initDepends' => ['first-client-id'],
                                'initialize' => !empty($model->id),
                                'url' => Url::to(['/client/ajax-get-client-account-list', 'selected' => $model->first_client_bank_id]),
                                'placeholder' => '...',
                            ],
                            'pluginEvents' => [
                                "depdrop:afterChange" => "function() {
                                    var form = $('#bill-form');
                                    checkRequiredInputs(form);
                                }",
                            ],
                         ])->label(Yii::t('bill', 'Bank account')); ?>
                        
                        <?= $this->render('../bill/_person_table_edit', [
                            'form' => $form,
                            'model' => $billFirstPersonModel,
                            'invoice' => $model,
                            'isModal' => $isModal,
                            'depends' => 'first-client-id',
                            'panel_person_id' => 'panel-first-person-data',
                            'docDate' => $model->periodical_next_date ?? null,
                        ]) ?>

                    </div>    
                </div>

                <div class="col-md-6 double-line-top double-line-bottom second-client-data" style="background: antiquewhite;">
                    <div class="col-md-12" style="padding: 0;">
                        <div class="field-group-title"><h3 id="second-party-title"><?= Yii::t('bill', 'Second party data'); ?></h3></div>
                    </div>

                    <div class="col-md-12" style="padding: 0;">
                        <?= Html::activeHiddenInput($secondClientModel, 'id', ['id' => 'second-client-id']); ?>
                        
                        <div class="form-group static-input">
                            <?= Html::label(Yii::t('bill', 'Party role'), 'second-client-role', ['class' => 'control-label col-md-3']); ?>
                            <div class="col-md-9">
                                <?= Html::textInput('second_client_role', 
                                    (!empty($agreementModel->second_client_role_id) ? $agreementModel->secondClientRole->name : null), [
                                    'id' => 'second-client-role', 
                                    'class' => 'form-control', 
                                    'disabled' => true,
                                ]); ?>
                            </div>
                        </div>                        

                        <?= $form->field($secondClientModel, 'name',[
                            'options' => [
                                'class' => 'form-group static-input',
                            ],
                            'horizontalCssClasses' => [
                                'label' => 'col-md-3',
                                'wrapper' => 'col-md-9',
                            ],  
                            'template' => '{label} <div class="col-md-9">{input}</div>',
                            'addon' => !FSMAccessHelper::can('viewClient') ? 
                                null : [
                                'prepend' => [
                                    'content' => 
                                    Html::a(Html::icon('eye-open'), 
                                        Url::to(['/client/view', 'id' => $secondClientModel->id]),
                                        [
                                            'id' => 'btn-view-second-client', 
                                            'class'=>'btn btn-info',
                                            'target' => '_blank',
                                            'title' => Yii::t('client', 'View client data'),
                                            'disabled' => empty($secondClientModel->id),
                                        ]
                                    ),
                                    'asButton' => true, 
                                ],
                            ],
                        ])->textInput([
                            'id' => 'second-client-name', 
                            //'value' => (!empty($secondClientModel->id) ? $secondClientModel->name : null),
                            'disabled' => true,
                            'style' => 'font-weight: bold; font-size: x-large;',
                        ])->label(Yii::t('client', 'Full name')); 
                        ?>
                        
                        <?= $form->field($secondClientModel, 'reg_number',[
                            'options' => [
                                'class' => 'form-group static-input',
                            ],
                            'horizontalCssClasses' => [
                                'label' => 'col-md-3',
                                'wrapper' => 'col-md-9',
                            ], 
                            'template' => '{label} <div class="col-md-9">{input}</div>',
                        ])->textInput([
                            'id' => 'second-client-reg',
                            'disabled' => true,
                        ])->label(Yii::t('client', 'Reg.number')); 
                        ?>
                        
                        <?= $form->field($secondClientModel, 'vat_number',[
                            'options' => [
                                'class' => 'form-group static-input',
                            ],
                            'horizontalCssClasses' => [
                                'label' => 'col-md-3',
                                'wrapper' => 'col-md-9',
                            ], 
                            'template' => '{label} <div class="col-md-9">{input}</div>',
                        ])->textInput([
                            'id' => 'second-client-vat',
                            'disabled' => true,
                        ])->label(Yii::t('client', 'VAT number')); 
                        ?>

                        <?= $form->field($secondClientModel, 'legal_address',[
                            'options' => [
                                'class' => 'form-group static-input',
                            ],
                            'horizontalCssClasses' => [
                                'label' => 'col-md-3',
                                'wrapper' => 'col-md-9',
                            ], 
                            'template' => '{label} <div class="col-md-9">{input}</div>',
                        ])->textInput([
                            'id' => 'second-client-address',
                            'value' => $secondClientModel->fullLegalAddress,
                            'disabled' => true,
                        ])->label(Yii::t('client', 'Legal address')); 
                        ?>

                        <?= $form->field($model, 'second_client_bank_id',[
                            'horizontalCssClasses' => [
                                'label' => 'col-md-3',
                                //'offset' => 'col-sm-offset-4',
                                'wrapper' => 'col-md-9',
                                //'error' => '',
                                //'hint' => '',
                            ],                            
                        ])->widget(DepDrop::class, [
                            'type' => DepDrop::TYPE_SELECT2,
                            'data' => empty($model->second_client_bank_id) ? null : [$model->second_client_bank_id => $model->secondClientBank->bank->name . ' | ' . $model->secondClientBank->account . (!empty($model->secondClientBank->name) ? ' ( '.$model->secondClientBank->name . ' )' : '')],
                            'select2Options' => [
                                'pluginOptions' => [
                                    'allowClear' => !$model->isAttributeRequired('second_client_bank_id'),
                                    'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
                                ],
                            ],
                            'pluginOptions' => [
                                'depends' => ['second-client-id'],
                                'initDepends' => ['second-client-id'],
                                'initialize' => !empty($model->id),            
                                'url' => Url::to(['/client/ajax-get-client-account-list', 'selected' => $model->second_client_bank_id]),
                                'placeholder' => '...',
                            ],
                            'pluginEvents' => [
                                "depdrop:afterChange" => "function() {
                                    var form = $('#bill-form');
                                    checkRequiredInputs(form);
                                }",
                            ],
                         ])->label(Yii::t('bill', 'Bank account')); ?>
                        
                        <?= $this->render('../bill/_person_table_edit', [
                            'form' => $form,
                            'model' => $billSecondPersonModel,
                            'invoice' => $model,
                            'isModal' => $isModal,
                            'depends' => 'second-client-id',
                            'panel_person_id' => 'panel-second-person-data',
                            'docDate' => $model->periodical_next_date ?? null,
                        ]) ?>

                    </div>    
                </div>            
            </div>
        </div>
    </fieldset>
    
    <?= $form->field($model, 'according_contract')->widget(SwitchInput::class, [
        'options' => [
            'id' => 'according-contract',
        ],        
        'pluginOptions' => [
            'onText' => Yii::t('common', 'Yes'),
            'offText' => Yii::t('common', 'No'),
        ],
        'pluginEvents' => [
            "switchChange.bootstrapSwitch" => "function() { console.log('switchChange'); }",
        ],
    ]); ?>
    
    <?php
        echo Html::beginTag('div', [
            'id' => 'product-data-container',
            'class' => 'form-group',
        ]);
        echo Html::beginTag('div', [
            'class' => 'col-md-2',
        ]);
        echo Html::endTag('div');
        echo Html::beginTag('div', [
            'class' => 'col-md-10',
        ]);
    ?>

    <?= $this->render('../bill/_product_table_edit', [
        'form' => $form,
        'model' => $billProductModel,
        'billProductProjectModel' => [],
        'invoiceModel' => $model,
        'productList' => $productList,
        'measureList' => $measureList,
        'useProjects' => 0,
    ]) ?>

    <?php
        echo Html::endTag('div');
        echo Html::endTag('div');           
    ?> 

    <?php
        echo Html::beginTag('div', [
            'id' => 'summa-data-container',
        ]);
        
        echo $form->field($model, 'summa')/*->textInput([
            'readonly' => true,
        ])*/->widget(MaskedInput::class, [
            //'mask' => '9{1,10}[.9{1,2}]',
            'options' => [
                'id' => 'bill-summa',
                'class' => 'form-control number-field',
                'readonly' => true,
            ],
            'clientOptions' => [
                'alias' => 'decimal',
                'rightAlign' => false,
                'digits' => 2,
                'allowMinus' => false,
            ],                                    
        ]);
        
        echo $form->field($model, 'vat')/*->textInput([
            'readonly' => true,
        ])*/->widget(MaskedInput::class, [
            //'mask' => '9{1,10}[.9{1,2}]',
            'options' => [
                'id' => 'bill-vat',
                'class' => 'form-control number-field',
                'readonly' => true,
            ],
            'clientOptions' => [
                'alias' => 'decimal',
                'rightAlign' => false,
                'digits' => 2,
                'allowMinus' => false,
            ],                                    
        ]);
        
        
        if(!$model->according_contract) :
            //echo Html::activeHiddenInput($model, 'summa');
            //echo Html::activeHiddenInput($model, 'vat');
        else :
            
        endif;
        echo Html::endTag('div');           
    ?>     
    
    <?= $form->field($model, 'total', [
        'addon' => [
            'append' => [
                ['content' => 
                    MaskedInput::widget([
                        'model' => $model,
                        'attribute' => 'rate',
                        //'mask' => '9{1,10}[.9{1,4}]',
                        'options' => [
                            'id' => 'rate', 
                            'placeholder' => 'Rate',
                            'class' => 'form-control number-field',
                            'style' => 'text-align: right; min-width: 90px;'
                        ],
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'rightAlign' => false,
                            'digits' => 4,
                            'allowMinus' => false,
                        ],                                    
                    ]),
                    'asButton' => true,
                ],
                ['content' => 
                    Select2::widget([
                        'model' => $model,
                        'attribute' => 'valuta_id',
                        'data' => $valutaList, 
                        'options' => [
                            'id' => 'valuta-id', 
                            'placeholder' => '...',
                        ],
                        'pluginOptions' => [
                            'allowClear' => !$model->isAttributeRequired('valuta_id'),
                            'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
                        ],            
                        'size' => 'control-width-90',
                    ]),
                    'asButton' => true,
                ],
            ],
        ],
        //'labelOptions' => ['class' => 'col-md-3'],
    ])->textInput([
        'readonly' => true,
        'id' => 'bill-total',
    ])->label($model->getAttributeLabel('total').' / '.$model->getAttributeLabel('rate').' / '.$model->getAttributeLabel('valuta_id'));
    ?>
    
    <?= $form->field($model, 'language_id')->widget(Select2::class, [
        'data' => $languageList,
        'options' => [
            'placeholder' => '...',
        ],
        'pluginOptions' => [
            'allowClear' => boolval(!$model->isAttributeRequired('language_id')),
            'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
        ],            
    ]); ?>
    
    <?php 
        $periodicalDayList = [];
        foreach (range(1, 28) as $key => $value) {
            $periodicalDayList[$key+1] = $value;
        }
        echo $form->field($model, 'periodical_day')->widget(EnumInput::class, [
        'type' => EnumInput::TYPE_SELECT2,
        'data' => $periodicalDayList,
    ]);?>

    <?= $form->field($model, 'periodicity')->widget(EnumInput::class, [
        'type' => EnumInput::TYPE_RADIOBUTTON,
        'options' => [
            'translate' => $model->billPeriodicityList,
        ],
    ]);?>

    <?= $form->field($model, 'periodical_last_date')->widget(DatePicker::class, [
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
        ]); 
    ?>
    
    <?= $form->field($model, 'periodical_finish_date')->widget(DatePicker::class, [
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
        ]); 
    ?>
        
    <?= $form->field($model, 'justification')->textInput(['maxlength' => true]) ?>        
    
    <?= $form->field($model, 'place_service')->textInput(['maxlength' => true]) ?>        
    
    <?= $form->field($model, 'comment_special')->textarea(['rows' => 3]) ?>
    
    <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>

    <div class="form-group">
        <div class="col-md-offset-9 col-md-3" style="text-align: right;">
            <?= $model->SubmitButton; ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>