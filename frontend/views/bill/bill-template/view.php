<?php

use yii\widgets\Pjax;
use yii\helpers\Url;

use kartik\helpers\Html;
use kartik\detail\DetailView;

use common\components\FSMAccessHelper;
use common\components\FSMHelper;
use common\models\bill\BillTemplate;

/* @var $this yii\web\View */
/* @var $model common\models\bill\Bill */

$this->title = '#'. $model->id;
if(FSMAccessHelper::checkRoute('/bill-template/index')){
    $this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bill-template-view">
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= Html::pageHeader(Html::encode($this->title)); ?>

    <div class='col-md-12' style="margin-bottom: 5px;">                    
        <div class='col-xs-6' style="padding: 0;">
            <?php if(FSMAccessHelper::can('updateBillTemplate', $model)): ?>
            <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?php endif; ?>
            <?php if(FSMAccessHelper::can('updateBillTemplate', $model)): ?>
            <?= \common\components\FSMBtnDialog::button(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
                'id' => 'btn-dialog-selected',
                'class' => 'btn btn-danger',
            ]); ?> 
            <?php endif; ?>
        </div>

        <div class='col-xs-offset-6' style="padding: 0; text-align: right;">
            <?php
                $params = [
                    //'url' => Url::to(['create-bill', 'id' => $model->id]), 
                    'model' => $model, 
                    'isBtn' => true,
                ];
                echo BillTemplate::getButtonCreateBill($params, '', true); 
            ?>
        </div>
    </div>
        
    <div class='col-md-12'>
        <div class='col-md-6' style="padding-left: 0;">
            <?php 
                $attributes = [
                    [
                        'label' => Yii::t('client', 'Full name'),
                        'value' => !empty($firstClientModel->id) ? 
                            (!empty($agreementModel->first_client_role_id) ? $model->firstClientRole->name.': ' : '') . 
                            (FSMAccessHelper::can('viewClient', $firstClientModel) ?
                                Html::a($firstClientModel->name, ['/client/view', 'id' => $firstClientModel->id], ['target' => '_blank']) :
                                $firstClientModel->name
                            )
                             : null,
                        'format'=>'raw',
                        'valueColOptions' => ['style' => 'font-weight: bold; font-size: 150%;'],
                    ],
                    [
                        'label' => Yii::t('client', 'Reg.number'),
                        'value' => !empty($firstClientModel->id) ? $firstClientModel->reg_number : null,
                    ],
                    [
                        'label' => Yii::t('client', 'VAT number'),
                        'value' => !empty($firstClientModel->id) ? $firstClientModel->vat_number : null,
                    ],
                    [
                        'label' => Yii::t('client', 'Legal address'),
                        'value' => !empty($firstClientModel->id) ? $firstClientModel->fullLegalAddress : null,
                    ],
                    [
                        'label' => Yii::t('bill', 'Bank account'),
                        'attribute' => 'first_client_bank_id',
                        'value' => !empty($model->first_client_bank_id) ? 
                            $model->firstClientBank->bank->name . ' | ' . $model->firstClientBank->account . (!empty($model->firstClientBank->name) ? ' ( '.$model->firstClientBank->name . ' )' : '') :
                            null,
                    ],
                ]
            ?>
            <?php
                $panelContent = [
                    'heading' => Yii::t('bill', 'First party data'),
                    'preBody' => '<div class="list-group-item">',
                    'body' => DetailView::widget([
                        'model' => $model,
                        'attributes' => $attributes,
                    ]),
                    'postBody' => '</div>',
                ];                        
                echo Html::panel(
                    $panelContent, 
                    'success', 
                    [
                        'id' => "first-party-data",
                    ]
                );                        
            ?>   
        </div>
        
        <div class='col-md-6' style="padding-right: 0;">
            <?php 
                $attributes = [
                    [
                        'label' => Yii::t('client', 'Full name'),
                        'value' => !empty($secondClientModel->id) ? 
                            (!empty($agreementModel->second_client_role_id) ? $model->secondClientRole->name.': ' : '') . 
                            (FSMAccessHelper::can('viewClient', $secondClientModel) ?
                                Html::a($secondClientModel->name, ['/client/view', 'id' => $secondClientModel->id], ['target' => '_blank']) :
                                $secondClientModel->name
                            )
                            : null,
                        'format'=>'raw',
                        'valueColOptions' => ['style' => 'font-weight: bold; font-size: 150%;'],
                    ],
                    [
                        'label' => Yii::t('client', 'Reg.number'),
                        'value' => !empty($secondClientModel->id) ? $secondClientModel->reg_number : null,
                    ],
                    [
                        'label' => Yii::t('client', 'VAT number'),
                        'value' => !empty($secondClientModel->id) ? $secondClientModel->vat_number : null,
                    ],
                    [
                        'label' => Yii::t('client', 'Legal address'),
                        'value' => !empty($secondClientModel->id) ? $secondClientModel->fullLegalAddress : null,
                    ],
                                    [
                        'attribute' => 'second_client_bank_id',
                        'label' => Yii::t('bill', 'Bank account'),
                        'value' => !empty($model->second_client_bank_id) ? 
                            $model->secondClientBank->bank->name . ' | ' . $model->secondClientBank->account . (!empty($model->secondClientBank->name) ? ' ( '.$model->secondClientBank->name . ' )' : '') :
                            null,
                    ],
                ]
            ?>
            <?php
                $panelContent = [
                    'heading' => Yii::t('bill', 'Second party data'),
                    'preBody' => '<div class="list-group-item">',
                    'body' => DetailView::widget([
                        'model' => $model,
                        'attributes' => $attributes,
                    ]),
                    'postBody' => '</div>',
                ];                        
                echo Html::panel(
                    $panelContent, 
                    'success', 
                    [
                        'id' => "second-party-data",
                    ]
                );                        
            ?>   
        </div>
    </div>
    
    <div class='col-md-12'>
        <div class='col-md-6' style="padding-left: 0;">
            <?php 
                $attributes = [
                    [
                        'attribute' => 'summa',
                        'value' => number_format($model->summa, 2, '.', ' '),
                    ],                         
                    [
                        'attribute' => 'vat',
                        'value' => number_format($model->vat, 2, '.', ' '),
                    ],     
                    [
                        'attribute' => 'total',
                        'value' => isset($model->valuta_id) ? number_format($model->total, 2, '.', ' ') . ' ' . $model->valuta->name : number_format($model->total, 2, '.', ' '),
                    ],                         
                ]
            ?>
            <?php
                $panelContent = [
                    'heading' => Yii::t('bill', 'Payment data'),
                    'preBody' => '<div class="list-group-item">',
                    'body' => DetailView::widget([
                        'model' => $model,
                        'attributes' => $attributes,
                    ]),
                    'postBody' => '</div>',
                ];                        
                echo Html::panel(
                    $panelContent, 
                    'success', 
                    [
                        'id' => "payment-data",
                    ]
                );                        
            ?>   
        </div>
        <div class='col-md-6' style="padding-right: 0;">
            <?php 
                $attributes = [
                    [
                        'label' => $model->getAttributeLabel('project_id'),
                        //'attribute' => 'project_id',
                        //'value' => !empty($model->project_id) ? Html::a($model->project->name, ['/project/view', 'id' => $model->project_id], ['target' => '_blank']) : null,
                        'value' => (FSMAccessHelper::can('viewProject', $model->project) ?
                            Html::a($model->project->name, ['/project/view', 'id' => $model->project->id], ['target' => '_blank']) :
                            $model->project->name
                        ),
                        'format' => 'raw',
                    ], 
                    [
                        'attribute' => 'agreement_id',
                        'value' => !$model->agreement_id ? null : (FSMAccessHelper::can('viewAgreement', $model->agreement) ?
                            Html::a($model->agreement->number, ['agreement/view', 'id' => $model->agreement_id], ['target' => '_blank']) :
                            $model->agreement->number
                        ),
                        'format'=>'raw',
                    ], 
                    'comment:ntext',
                ]
            ?>
            <?php
                $panelContent = [
                    'heading' => Yii::t('common', 'Other data'),
                    'preBody' => '<div class="list-group-item">',
                    'body' => DetailView::widget([
                        'model' => $model,
                        'attributes' => $attributes,
                    ]),
                    'postBody' => '</div>',
                ];                        
                echo Html::panel(
                    $panelContent, 
                    'success', 
                    [
                        'id' => "other-data",
                    ]
                );                        
            ?>   
        </div>
    </div>
    
    <div class='col-md-12'>
        <?= $this->render('../bill/_product_table_view', [
                'model' => $billProductModel,
                'bill' => $model,
            ]); 
        ?>
    </div>

</div>
