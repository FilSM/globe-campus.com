<?php
namespace common\models;

use Yii;
//use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\grid\GridView;
use yii\helpers\Url;

use kartik\helpers\Html;
use kartik\grid\GridView;

use common\models\bill\BillTemplate;
use common\components\FSMHelper;
use common\components\FSMAccessHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\bill\search\BillTemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $searchModel->modelTitle(2);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bill-template-index">

    <div id="page-content">
        <?= Html::pageHeader(Html::encode($this->title)); ?>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?php if(FSMAccessHelper::can('createBill')): ?>
        <p>
            <?= Html::a(Html::icon('plus').'&nbsp;'.$searchModel->modelTitle(), ['create'], ['class' => 'btn btn-success']); ?>
        </p>
        <?php endif; ?>
        
        <?php
            $columns = [
                //['class' => '\kartik\grid\SerialColumn'],

                [
                    'attribute' => 'id',
                    'width' => '75px',
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                ],
                [
                    'attribute'=>'project_name',
                    'contentOptions' => [
                        'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                    ],

                    'value' => function ($model) {
                        return 
                            '<div style="overflow-x: auto;">'.
                            (!empty($model->project_id) ? (FSMAccessHelper::can('viewProject', $model->project) ?
                                Html::a($model->project_name, ['/project/view', 'id' => $model->project_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                $model->project_name 
                            )
                            : null).
                            '</div>';                    
                    },                         
                    'format'=>'raw',
                ],                 
                [
                    'attribute'=>'agreement_number',
                    'value' => function ($model) {
                        return (!empty($model->agreement_id) ? (FSMAccessHelper::can('viewAgreement', $model->agreement) ?
                                Html::a($model->agreement_number, ['/agreement/view', 'id' => $model->agreement_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                $model->agreement_number 
                            )
                            : null);                    
                    },                         
                    'format'=>'raw',
                ],                 
                [
                    'attribute'=>'first_client_id',
                    'headerOptions' => ['class'=>'td-mw-150'],
                    'contentOptions' => [
                        'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                    ],
                    'value' => function ($model) {
                        return 
                            '<div style="overflow-x: auto;">'.
                            (!empty($model->first_client_name) ? 
                            (!empty($model->first_client_role_name) ? $model->first_client_role_name.'<br/>' : '') . 
                                (FSMAccessHelper::can('viewClient', $model->firstClient) ?
                                    Html::a($model->first_client_name, ['/client/view', 'id' => $model->first_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                    $model->first_client_name
                                )
                            : null).
                            '</div>';
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => $clientList,
                    'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                    'filterInputOptions' => [
                        'id' => 'billsearch-first_client_name',
                        'placeholder' => '...',
                    ],                            
                    'format'=>'raw',
                ],                          
                [
                    'attribute'=>'second_client_id',
                    'headerOptions' => ['class'=>'td-mw-150'],
                    'contentOptions' => [
                        'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                    ],
                    'value' => function ($model) {
                        return  
                            '<div style="overflow-x: auto;">'.
                            (!empty($model->second_client_name) ?  
                            (!empty($model->second_client_role_name) ? $model->second_client_role_name.'<br/>' : '') . 
                                (FSMAccessHelper::can('viewClient', $model->secondClient) ?
                                    Html::a($model->second_client_name, ['/client/view', 'id' => $model->second_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                    $model->second_client_name
                                )
                            : null).
                            '</div>';
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => $clientList,
                    'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                    'filterInputOptions' => [
                        'placeholder' => '...',
                    ],
                    'format'=>'raw',
                ],
                            /*
                [
                    'attribute'=>'third_client_id',
                    'headerOptions' => ['class'=>'td-mw-150'],
                    'contentOptions' => [
                        'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                    ],
                    'value' => function ($model) {
                        return  
                            '<div style="overflow-x: auto;">'.
                            (!empty($model->third_client_name) ?  
                            (!empty($model->third_client_role_name) ? $model->third_client_role_name.'<br/>' : '') . 
                                (FSMAccessHelper::can('viewClient', $model->thirdClient) ?
                                    Html::a($model->third_client_name, ['/client/view', 'id' => $model->third_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                    $model->third_client_name
                                )
                            : null).
                            '</div>';
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => $clientList,
                    'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                    'filterInputOptions' => [
                        'placeholder' => '...',
                    ],
                    'format'=>'raw',
                ],       
                             * 
                             */
                [
                    'attribute' => 'summa',
                    'hAlign' => 'right',
                    'vAlign' => 'middle',
                    'value' => function ($model) {
                        return number_format($model->summa, 2, '.', ' ');
                    },
                ],                         
                [
                    'attribute' => 'vat',
                    'hAlign' => 'right',
                    'vAlign' => 'middle',
                    'value' => function ($model) {
                        return number_format($model->vat, 2, '.', ' ');
                    },
                ],     
                [
                    'attribute' => 'total',
                    'hAlign' => 'right',
                    'vAlign' => 'middle',
                    'mergeHeader' => true,
                    'headerOptions' => ['style'=>'text-align: center;'],
                    'value' => function ($model) {
                        return isset($model->valuta_id) ? number_format($model->total, 2, '.', ' ') . ' ' . $model->valuta->name : $model->total;
                    },
                ],                         
                [
                    'attribute' => 'periodical_day',
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'hAlign' => 'right',
                    'vAlign' => 'middle',
                    'headerOptions' => ['style'=>'text-align: center;'],
                    'value' => function ($model) {
                        return isset($model->periodical_day) ? $model->periodical_day : null;
                    },
                ],                         
                [
                    'attribute' => 'periodicity',
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'hAlign' => 'right',
                    'vAlign' => 'middle',
                    'value' => function ($model) {
                        return isset($model->periodicity) ? $model->billPeriodicityList[$model->periodicity] : null;
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => $searchModel->billPeriodicityList,
                    'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                    'filterInputOptions' => [
                        'placeholder' => '...',
                    ],                            
                ],       
                [
                    'attribute'=>'periodical_next_date',
                    'headerOptions' => ['class'=>'td-mw-75'],
                    'hAlign' => 'right',
                    'vAlign' => 'middle',
                    'value' => function ($model) {
                        return !empty($model->periodical_next_date) ? date('d-M-Y', strtotime($model->periodical_next_date)) : null;
                    },
                    'filterType' => GridView::FILTER_DATE,
                    'filterWidgetOptions' => [
                        'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
                        'pickerButton' => false,
                        'layout' => '{input}{remove}',
                        'buttonOptions' => [],
                    ],
                ],                            
                [
                    'attribute'=>'periodical_finish_date',
                    'headerOptions' => ['class'=>'td-mw-75'],
                    'hAlign' => 'right',
                    'vAlign' => 'middle',
                    'value' => function ($model) {
                        return !empty($model->periodical_finish_date) ? date('d-M-Y', strtotime($model->periodical_finish_date)) : null;
                    },
                    'filterType' => GridView::FILTER_DATE,
                    'filterWidgetOptions' => [
                        'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
                        'pickerButton' => false,
                        'layout' => '{input}{remove}',
                        'buttonOptions' => [],
                    ],
                ],                            
                [
                    'class' => '\common\components\FSMActionColumn',
                    'headerOptions' => ['class'=>'td-mw-125'],
                    'dropdown' => true,
                    'dropdownDefaultBtn' => 'create-bill',
                    //'checkPermission' => true,
                    'template' => '{create-bill} {view} {update} {delete}',
                    'buttons' => [
                        'create-bill' => function (array $params) { 
                            return BillTemplate::getButtonCreateBill($params);
                        },
                    ],
                    'controller' => 'bill-template',
                ],
            ];
        ?>

        <?= GridView::widget([
            'id' => 'grid-view',
            'tableOptions' => [
                'id' => 'bill-list',
            ],
            'responsive' => false,
            'condensed' => true,
            'hover' => true,
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $columns,
            'pjax' => true,
            'floatHeader' => true,
            'export' => false,
        ]);
        ?>
    </div>
</div>