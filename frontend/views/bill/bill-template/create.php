<?php

use kartik\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\bill\BillTemplate */

$this->title = Yii::t($model->tableName(), 'Create a new '.$model->modelTitle(1, false));
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bill-template-create">

    <?= Html::pageHeader(Html::encode($this->title));?>

    <?= $this->render('_form', [
        'model' => $model,
        'abonentModel' => $abonentModel,
        'agreementModel' => $agreementModel,
        'firstClientModel' => $firstClientModel,
        'secondClientModel' => $secondClientModel,
        'projectList' => $projectList,
        'agreementList' => $agreementList,
        'clientRoleList' => $clientRoleList,
        'valutaList' => $valutaList,
        'billProductModel' => $billProductModel,
        'billFirstPersonModel' => $billFirstPersonModel,
        'billSecondPersonModel' => $billSecondPersonModel,
        'productList' => $productList,
        'measureList' => $measureList,
        'languageList' => $languageList,
        'isAdmin' => $isAdmin,
    ]) ?>

</div>