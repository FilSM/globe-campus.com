<?php

use kartik\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\client\AgreementPayment */

$this->title = Yii::t($model->tableName(), 'Create a new '.$model->modelTitle(1, false));
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-payment-create">

    <?php if(!$isModal): ?>
    <?= Html::pageHeader(Html::encode($this->title));?>
    <?php endif; ?>

    <?= $this->render('_form', [
        'model' => $model,
        'historyModel' => $historyModel,
        'bankFromList' => $bankFromList,
        'bankToList' => $bankToList,
        'valutaList' => $valutaList,
        'isAdmin' => !empty($isAdmin),
        'showDirection' => $showDirection,
        'isModal' => $isModal,
    ]) ?>

</div>