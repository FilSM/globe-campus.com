<?php
namespace common\models;

use Yii;
//use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\grid\GridView;

use kartik\helpers\Html;
use kartik\grid\GridView;

use common\components\FSMAccessHelper;
use common\models\client\AgreementHistory;
use common\models\client\AgreementPayment;

/* @var $this yii\web\View */
/* @var $searchModel common\models\client\search\AgreementPaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $searchModel->modelTitle(2);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-payment-index">

    <?= Html::pageHeader(Html::encode($this->title)); ?>
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php 
    $columns = [
        //['class' => '\kartik\grid\SerialColumn'],
        [
            'attribute' => 'id',
            'width' => '75px',
            'hAlign' => 'center',
            //'pageSummary' => $searchModel->getAttributeLabel('total'),
        ],
        [
            'attribute' => 'agreement_number',
            'contentOptions' => [
                'style'=>'max-width:100px; min-height:75px; word-wrap: break-word;'
            ],
            'value' => function ($model) {
                return 
                    '<div style="overflow-x: auto;">'.
                    (!empty($model->agreement_number) ? (FSMAccessHelper::can('viewAgreement', $model) ?
                        Html::a($model->agreement_number, ['/agreement/view', 'id' => $model->agreement_id], ['target' => '_blank', 'data-pjax' => 0]) :
                        $model->agreement_number
                    )
                    : null).
                    '</div>';
            },                        
            'format' => 'raw',
        ],
        [
            'attribute'=>'from_bank_id',
            'contentOptions' => [
                'style'=>'max-width:200px; min-height:100px; word-wrap: break-word;'
            ],
            'value' => function ($model) {
                return 
                    '<div style="overflow-x: auto;">'.
                    (!empty($model->from_bank_name) ? 
                        Html::a($model->from_bank_name, ['/bank/view', 'id' => $model->from_bank_id], ['target' => '_blank', 'data-pjax' => 0])
                        : null
                    ).
                    '</div>';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $bankList,
            'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
            'filterInputOptions' => [
                'id' => 'agrpayment-from_bank_name',
                'placeholder' => '...',
            ],                            
            'format'=>'raw',
        ],                    
        [
            'attribute'=>'to_bank_id',
            'contentOptions' => [
                'style'=>'max-width:200px; min-height:100px; word-wrap: break-word;'
            ],
            'value' => function ($model) {
                return 
                    '<div style="overflow-x: auto;">'.
                    (!empty($model->to_bank_name) ? 
                        Html::a($model->to_bank_name, ['/bank/view', 'id' => $model->to_bank_id], ['target' => '_blank', 'data-pjax' => 0])
                        : null
                    ).
                    '</div>';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $bankList,
            'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
            'filterInputOptions' => [
                'id' => 'agrpayment-to_bank_name',
                'placeholder' => '...',
            ],                            
            'format'=>'raw',
        ],
        [
            'attribute' => 'paid_date',
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'width' => '150px',
            'headerOptions' => ['class'=>'td-mw-75'],
            'value' => function ($model) {
                return isset($model->paid_date) ? date('d-M-Y', strtotime($model->paid_date)) : null;
            },
            'filterType' => GridView::FILTER_DATE_RANGE,
            'filterWidgetOptions' => [
                'pluginOptions' => \yii\helpers\ArrayHelper::merge(
                    Yii::$app->params['DatePickerPluginOptions'], 
                    [
                        'locale' => [
                            'firstDay' => 1,
                            'format' => 'd-m-Y',
                            'cancelLabel' => Yii::t('common', 'Clear'),
                        ],
                        //'opens'=>'left',
                        //'startDate' => date('d-m-Y'),
                    ]),
                'convertFormat' => true,
                //'presetDropdown' => true,
                'hideInput' => true,
                'pluginEvents' => [
                    'cancel.daterangepicker' => 'function() {
                        //console.log("cancel.daterangepicker"); 
                        var gridFilters = $("table tr.filters");
                        var daterangeinput = gridFilters.find("#agreementpaymentsearch-paid_date");
                        daterangeinput.val("").change();
                    }',
                ],
            ],
            'format'=>'raw',        
        ],           
        [
            'attribute' => 'direction',
            'value' => function ($model) {
                return $model->getPaymentDirectionList()[$model->direction];
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $searchModel->getPaymentDirectionList(),
            'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
            'filterInputOptions' => [
                'id' => 'agrpayment-direction',
                'placeholder' => '...',
            ],                      
        ],                     
        [
            'attribute' => 'summa_eur',
            'hAlign' => 'right',
            'headerOptions' => ['style'=>'text-align: center;'],
            'mergeHeader' => true,
            'value' => function ($model) {
                return isset($model->summa_eur) ? $model->summa_eur : null;
            },
            'xlFormat' => '###0.00',
            'exportMenuStyle' => [
                'numberFormat' => ['formatCode' => '###0.00'],
            ],                            
            'format' => ['decimal', 2],
            //'pageSummary' => true,                 
        ],        
        [
            'class' => '\common\components\FSMActionColumn',
            'headerOptions' => ['class'=>'td-mw-125'],
            'checkPermission' => true,
            'dropdown' => true,
            'template' => '{confirm} {view} {update} {delete}',
            'checkCanDo' => true,
            'buttons' => [
                'confirm' => function ($params) { 
                    return AgreementPayment::getButtonConfirm($params);
                },
                'update' => function ($params) { 
                    return AgreementPayment::getButtonUpdate($params);
                },
            ], 
        ],
    ];
    ?>
    
    <?= GridView::widget([
        'id' => 'grid-view',
        /*
        'tableOptions' => [
            'id' => 'agr-payment-list',
        ],
         * 
         */
        'responsive' => false,
        'striped' => true,
        'hover' => true,
        'bordered' => true,
        'condensed' => true,
        'persistResize' => false,
        'floatHeader' => true,
        'autoXlFormat' => true,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        //'showPageSummary' => true,
        'pjax' => true,
        'columns' => $columns,
    ]); ?>  
</div>