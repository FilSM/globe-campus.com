<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\client\AgreementPayment */

$this->title = Yii::t($model->tableName(), 'Update a '.$model->modelTitle(1, false)) . ': #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Edit');
$isModal = !empty($isModal);
?>
<div class="agreement-payment-update">

    <?php if(!$isModal): ?>
    <?= Html::pageHeader(Html::encode($this->title));?>
    <?php endif; ?>

    <?= $this->render('_form', [
        'model' => $model,
        'historyModel' => $historyModel,
        'bankFromList' => $bankFromList,
        'bankToList' => $bankToList,
        'valutaList' => $valutaList,
        'isAdmin' => !empty($isAdmin),
        'showDirection' => $showDirection,
        'isModal' => $isModal,
    ]) ?>

</div>