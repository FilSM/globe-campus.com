<?php

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\client\search\AgreementPaymentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agreement-payment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'agreement_history_id') ?>

    <?= $form->field($model, 'from_bank_id') ?>

    <?= $form->field($model, 'to_bank_id') ?>

    <?= $form->field($model, 'agreement_id') ?>

    <?php //echo $form->field($model, 'summa') ?>

    <?php //echo $form->field($model, 'valuta_id') ?>

    <?php //echo $form->field($model, 'rate') ?>

    <?php //echo $form->field($model, 'summa_eur') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('common', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('common', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>