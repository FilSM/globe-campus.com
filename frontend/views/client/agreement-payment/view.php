<?php

use kartik\helpers\Html;
use kartik\detail\DetailView;

use common\components\FSMAccessHelper;
use common\models\client\AgreementHistory;

/* @var $this yii\web\View */
/* @var $model common\models\client\AgreementPayment */

$this->title = $model->modelTitle() .' #'. $model->id;
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$agreementHistory = $model->agreementHistory;
$historyAction = AgreementHistory::getAgreementActionList()[$agreementHistory->action_id]
?>
<div class="agreement-payment-view">

    <?= Html::pageHeader(Html::encode($this->title)); ?>

    <p>
        <?php if(FSMAccessHelper::can('updateAgreementPayment', $model) && $model->canUpdate()): ?>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>
        <?php if(FSMAccessHelper::can('deleteAgreementPayment', $model) && $model->canDelete()): ?>
        <?= \common\components\FSMBtnDialog::button(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'id' => 'btn-dialog-selected',
            'class' => 'btn btn-danger',
        ]); ?> 
        <?php endif; ?>        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'agreement_id',
                'value' => !empty($model->agreement_id) ? 
                    (FSMAccessHelper::can('viewAgreement', $model) ?
                        Html::a($model->agreement->number, ['/agreement/view', 'id' => $model->agreement_id], ['target' => '_blank']) : 
                        $model->agreement->number
                    )
                    : null,
                'format' => 'raw',
            ],            
            [
                'attribute' => 'agreement_history_id',
                'value' => !empty($model->agreement_history_id) ? $historyAction : null,
                'format' => 'raw',
            ], 
            [
                'attribute' => 'from_bank_id',
                'value' => !empty($model->from_bank_id) ? 
                    Html::a($model->fromBank->name, ['/bank/view', 'id' => $model->from_bank_id], ['target' => '_blank']) : null,
                'format' => 'raw',
            ], 
            [
                'attribute' => 'to_bank_id',
                'value' => !empty($model->to_bank_id) ? 
                    Html::a($model->toBank->name, ['/bank/view', 'id' => $model->to_bank_id], ['target' => '_blank']) : null,
                'format' => 'raw',
            ], 
            [
                'attribute' => 'paid_date',
                'value' => isset($model->paid_date) ? date('d-M-Y', strtotime($model->paid_date)) : null,
            ],              
            //'summa',
            //'valuta_id',
            //'rate',
            //'summa_eur',
            [
                'attribute' => 'summa',
                'value' => isset($model->valuta_id) ? number_format($model->summa, 2, '.', ' ') . ' ' . $model->valuta->name : number_format($model->summa, 2, '.', ' '),
            ],       
            [
                'label' => Yii::t('common', 'Comment'),
                'value' => isset($model->agreementHistory) ? $model->agreementHistory->comment : '',
            ],      
            [
                'attribute' => 'confirmed',
                'value' => isset($model->confirmed) ? date('d-M-Y', strtotime($model->confirmed)) : null,
            ],             
        ],
    ]) ?>

</div>