<?php
namespace common\models\client;

use Yii;
use yii\helpers\ArrayHelper;
//use yii\grid\GridView;

use kartik\helpers\Html;
use kartik\grid\GridView;

use common\components\FSMAccessHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\client\search\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $searchModel->modelTitle(2);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-index">
    
    <?= Html::pageHeader(Html::encode($this->title));?>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if(FSMAccessHelper::can('createClient')): ?>
    <p>
        <?= Html::a(Html::icon('plus').'&nbsp;'.$searchModel->modelTitle(), ['create'], ['class' => 'btn btn-success']); ?>
    </p>
    <?php endif; ?>
    
    <?php 
        $columns = [
            [
                'class' => '\kartik\grid\SerialColumn',
                'visible' => false,//!$isAdmin,
            ],
            [
                'attribute' => 'id',
                'width' => '75px',
                'hAlign' => 'center',
                //'visible' => $isAdmin,
            ],
            [
                'attribute'=>'name',
                //'headerOptions' => ['class'=>'td-mw-100'],
                'contentOptions' => [
                    'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                ],
                'value' => function ($model) {
                    return  
                        '<div style="overflow-x: auto;">'.
                        (Html::a($model->name, ['client/view', 'id' => $model->id], ['target' => '_blank', 'data-pjax' => 0])).
                        '</div>';
                },                         
                'format'=>'raw',
            ],                              
            [
                'attribute'=>'legal_country_id',
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) {
                    return !empty($model->legal_country_id) ? $model->country_name : null;
                },                         
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $countryList,
                'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                'filterInputOptions' => ['placeholder' => '...'],
            ],                             
            'reg_number',
                        /*
            'vat_number',
                         * 
                         */
            [
                'attribute'=>'manager_id',
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) {
                    return !empty($model->manager_id) ? Html::a($model->manager_name, ['/user/'.$model->manager_user_id], ['target' => '_blank', 'data-pjax' => 0]) : null;
                },                         
                'format'=>'raw',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $managerList,
                'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                'filterInputOptions' => ['placeholder' => '...'],
            ],                             
            [
                'attribute'=>'agent_id',
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) {
                    return !empty($model->agent_id) ? Html::a($model->agent_name, ['/user/'.$model->agent_user_id], ['target' => '_blank', 'data-pjax' => 0]) : null;
                },                         
                'format'=>'raw',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $agentList,
                'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                'filterInputOptions' => ['placeholder' => '...'],
            ],                             
            [
                'attribute' => 'status',
                'headerOptions' => ['class'=>'td-mw-150'],                
                'value' => function ($model) {
                    return isset($model->status) ? $model->clientStatusList[$model->status] : null;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $searchModel->clientStatusList,
                'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                'filterInputOptions' => ['placeholder' => '...'],
                'visible' => $isAdmin,
            ],
            /* [
                'attribute' => 'language_id',
                'value' => function ($model) {
                    return $model->language->name;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $dataFilterLanguage,
                'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                'filterInputOptions' => ['placeholder' => '...'],
            ], */
            [
                'attribute' => 'it_is',
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) {
                    return isset($model->it_is) ? $model->clientItIsList[$model->it_is] : null;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $itIsList,
                'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                'filterInputOptions' => ['placeholder' => '...'],
                'visible' => $isAdmin,
            ],
            [
                'attribute' => 'client_type',
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) {
                    return isset($model->client_type) ? $model->clientTypeList[$model->client_type] : null;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $searchModel->clientTypeList,
                'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                'filterInputOptions' => ['placeholder' => '...'],
                'visible' => $isAdmin,
            ],
            [
                'attribute' => "vat_payer",                
                'class' => '\kartik\grid\BooleanColumn',
                'trueLabel' => 'Yes', 
                'falseLabel' => 'No',
                'width' => '100px',
            ],                        
            [
                'attribute' => 'uploaded_file_id',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'mergeHeader' => true,
                'value' => function ($model) {
                    $logoModel = $model->logo;
                    if(empty($logoModel)){
                        return null;
                    }
                    $logoPath = $logoModel->uploadedFileUrl;
                    return Html::img($logoPath, ['width' => '70px']);
                },
                'format' => 'html',    
            ],                            
            [
                'attribute' => "deleted",                
                'class' => '\kartik\grid\BooleanColumn',
                'trueLabel' => 'Yes', 
                'falseLabel' => 'No',
                'width' => '100px',
                'visible' => $isAdmin,
            ],       
            [
                'class' => '\common\components\FSMActionColumn',
                'headerOptions' => ['class' => 'td-mw-150'],
                'dropdown' => true,
                'checkPermission' => true,
            ],                        
        ];
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsive' => false,
        //'striped' => false,
        'hover' => true,      
        'floatHeader' => true,
        'pjax' => true,
        'columns' => $columns,
        'rowOptions' => function ($model, $key, $index, $grid) {
            if($model->deleted){
                return ['class' => 'client-status-deleted'];
            }
            switch ($model->status) {
                case Client::CLIENT_STATUS_ACTIVE:
                    return ['class' => 'client-status-active'];
                case Client::CLIENT_STATUS_POTENTIAL:
                    return ['class' => 'client-status-potential'];
                case Client::CLIENT_STATUS_ARCHIVED:
                default: 
                    return ['class' => 'client-status-archived'];
            }        
        },        
    ]); ?>

</div>

<?php
    $panelAfter = '<fieldset id="background-legend-fieldset">';
    $panelAfter .= '<table class="kv-grid-table" style="width: 50%;"><tr>';
    $panelAfter .= 
        '<td style="width: 10%;">'.Yii::t('bill', 'Background legend').': </td>'.
        '<td class="client-status-'.Client::CLIENT_STATUS_ACTIVE.'" style="width: 10%; text-align: center;">'. $searchModel->clientStatusList[Client::CLIENT_STATUS_ACTIVE].'</td>'.
        '<td class="client-status-'.Client::CLIENT_STATUS_POTENTIAL.'" style="width: 10%; text-align: center;">'. $searchModel->clientStatusList[Client::CLIENT_STATUS_POTENTIAL].'</td>'.
        '<td class="client-status-'.Client::CLIENT_STATUS_ARCHIVED.'" style="width: 10%; text-align: center;">'. $searchModel->clientStatusList[Client::CLIENT_STATUS_ARCHIVED].'</td>'.
        '<td class="client-status-'.Client::CLIENT_STATUS_DELETED.'" style="width: 10%; text-align: center;">'. Yii::t('common', 'Deleted').'</td>';
    $panelAfter .= '</tr></table>';
    $panelAfter .= '</fieldset>';
    
    echo $panelAfter;
?>
