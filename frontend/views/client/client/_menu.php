<?php

namespace common\models\client;

use Yii;
use kartik\widgets\SideNav;

use common\models\client\Client;
use common\models\client\ClientContact;
use common\models\event\Event;
use common\models\client\Agreement;
use common\components\FSMAccessHelper;

$clientMailTemplate = $client->clientMailTemplate;
$clientMailTemplate = !empty($clientMailTemplate) ? $clientMailTemplate : new ClientMailTemplate();

$clientInvoicePdf = $client->clientInvoicePdf;
$clientInvoicePdf = !empty($clientInvoicePdf) ? $clientInvoicePdf : new clientInvoicePdf();

$items = [
    ['label' => Yii::t('common', 'Summary'), 
        'url' => ['/client/view', 'id' => $client->id],
        'icon' => 'list-alt',
        'active' => (isset($activeItem) && ($activeItem == 'general')),
    ],
    ['label' => ClientContact::modelTitle(2), 
        'url' => ['/client-contact/index', 'client_id' => $client->id],
        'icon' => 'envelope',
        'active' => (isset($activeItem) && ($activeItem == ClientContact::tableName())),
    ],
    ['label' => ClientRegDoc::modelTitle(2), 
        'url' => ['/client-reg-doc/index', 'client_id' => $client->id],
        'icon' => 'file',
        'active' => (isset($activeItem) && ($activeItem == ClientRegDoc::tableName())),
    ],
    /*
    ['label' => Share::modelTitle(2), 
        'items' => [
            ['label' => Yii::t('client', 'Our shares'), 
                'url' => ['/share/index', 'client_id' => $client->id],
                //'icon' => 'paste',
                'active' => (isset($activeItem) && ($activeItem == 'our-shares')),    
            ],
            ['label' => Yii::t('client', 'Our shareholders'), 
                'url' => ['/shareholder/index', 'client_id' => $client->id],
                //'icon' => 'paste',
                'active' => (isset($activeItem) && ($activeItem == 'our-shareholders')),    
            ],
        ],        
        'icon' => 'euro',
    ],
     * 
     */
    ['label' => ClientMailTemplate::modelTitle(), 
        'url' => ['/mail-template/view', 'id' => !empty($clientMailTemplate->id) ? $clientMailTemplate->id : 0, 'client_id' => $client->id],
        'icon' => 'th',
        'active' => (isset($activeItem) && ($activeItem == ClientMailTemplate::tableName())),
        'visible' => FSMAccessHelper::can('updateClient'),
    ],
    ['label' => ClientHistory::modelTitle(2), 
        'url' => ['/client-history/index', 'client_id' => $client->id],
        'icon' => 'time',
        'active' => (isset($activeItem) && ($activeItem == ClientHistory::tableName())),
    ],
    ['label' => ClientInvoicePdf::modelTitle(), 
        'url' => ['/client-invoice-pdf/view', 'id' => !empty($clientInvoicePdf->id) ? $clientInvoicePdf->id : 0, 'client_id' => $client->id],
        'icon' => 'paperclip',
        'active' => (isset($activeItem) && ($activeItem == ClientInvoicePdf::tableName())),
        'visible' => FSMAccessHelper::can('updateClient'),
    ],
    ['label' => Yii::t('client', 'Staff'), 
        'url' => ['/client/staff', 'id' => $client->id],
        'icon' => 'user',
        'active' => (isset($activeItem) && ($activeItem == 'staff')),
        'visible' => ($client->it_is == Client::CLIENT_IT_IS_OWNER) && FSMAccessHelper::can('viewStaff', $client),
    ],
    ['label' => Agreement::modelTitle(2), 
        'url' => ['/agreement/index', 'client_id' => $client->id],
        'icon' => 'briefcase',
        'active' => (isset($activeItem) && ($activeItem == Agreement::tableName())),
    ],
    ['label' => Event::modelTitle(2), 
        'url' => ['/event/index', 'client_id' => $client->id],
        'icon' => 'phone-alt',
        'active' => (isset($activeItem) && ($activeItem == Event::tableName())),
        'visible' => FSMAccessHelper::checkRoute('/event/*'),
    ],
];

switch ($client->status) {
    case Client::CLIENT_STATUS_ACTIVE:
        $headerClass = 'client-status-active';
        break;
    case Client::CLIENT_STATUS_POTENTIAL:
        $headerClass = 'client-status-potential';
        break;
    case Client::CLIENT_STATUS_ARCHIVED:
    default: 
        $headerClass = 'client-status-archived';
        break;
}
if($client->deleted){
    $headerClass = 'client-status-deleted';
}
?>

<?= SideNav::widget([
    'items' => $items,
    'heading' => $client->name,
    'headingOptions' => [
        'class' => $headerClass,
    ],
    'containerOptions' => [
        'id' => 'client-navbar',
    ],
]);
?>
