<?php

use yii\helpers\Url;

use kartik\helpers\Html;
use kartik\detail\DetailView;

use common\components\FSMAccessHelper;
use common\models\user\FSMUser;
use common\models\client\Client;

/* @var $this yii\web\View */
/* @var $model common\models\Client */

$this->title = $model->modelTitle().': '.$model->name;
if(FSMAccessHelper::checkRoute('/client/*')){
    $this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row client-view">

    <div class="col-md-2">
        <?=
        $this->render('_menu', [
            'client' => $model,
            'activeItem' => 'general',
        ])
        ?>
    </div>

    <div class="col-md-10">
        <?php 
            ob_start();
            ob_implicit_flush(false);
        ?>
        <div class='col-md-12'>
            <?php if(FSMAccessHelper::can('updateClient', $model)): ?>
            <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?php endif; ?>
            <?php if(FSMAccessHelper::can('deleteClient', $model)): ?>
            <?= \common\components\FSMBtnDialog::button(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
                'id' => 'btn-dialog-selected',
                'class' => 'btn btn-danger',
            ]); ?> 
            <?php endif; ?>
        </div>

        <div class='col-md-12'>
            <p></p>
        </div>  

        <div class='col-md-12'>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    [
                        'label' => $model->getAttributeLabel('client_group_id'),
                        'value' => !empty($model->clientGroup->id) ? $model->clientGroup->name : null,
                    ],
                    [
                        'attribute' => 'name',
                        'label' => isset($model->client_type) && ($model->client_type == Client::CLIENT_TYPE_LEGAL) ? $model->getAttributeLabel('name') : Yii::t('client', 'Firstname, Lastname'),
                    ],
                    [
                        'attribute' => 'reg_number',
                        'label' => isset($model->client_type) && ($model->client_type == Client::CLIENT_TYPE_LEGAL) ? $model->getAttributeLabel('reg_number') : Yii::t('client', 'Personal code'),
                    ],
                    [
                        'attribute' => 'vat_number',
                        'visible' => !empty($model->vat_payer),
                    ],
                    [
                        'attribute' => 'tax',
                        'visible' => !empty($model->vat_payer),
                    ],
                    [
                        'attribute' => 'legal_address',
                        'label' => isset($model->client_type) && ($model->client_type == Client::CLIENT_TYPE_LEGAL) ? $model->getAttributeLabel('legal_address') : Yii::t('client', 'Registration address'),
                        'value' => $model->fullLegalAddress,
                    ],                            
                    [
                        'attribute' => 'office_address',
                        'label' => isset($model->client_type) && ($model->client_type == Client::CLIENT_TYPE_LEGAL) ? $model->getAttributeLabel('office_address') : Yii::t('client', 'Home address'),
                        'value' => $model->fullOfficeAddress,
                        'visible' => !empty($model->office_address) && ($model->legal_address != $model->office_address),
                    ], 
                    [
                        'attribute' => 'language_id',
                        'value' => !empty($abonentModel->language_id) ? $abonentModel->language->name : null,
                    ],
                    [
                        'attribute' => 'manager_id',
                        'value' => isset($abonentModel->manager_id, $abonentModel->manager) ? Html::a($abonentModel->manager->name, ['/user/profile/show', 'id' => (isset($abonentModel->manager->user) ? $abonentModel->manager->user->id : null)], ['target' => '_blank']) : null,
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'agent_id',
                        'value' => isset($abonentModel->agent_id, $abonentModel->agent) ? Html::a($abonentModel->agent->name, ['/user/profile/show', 'id' => (isset($abonentModel->agent->user) ? $abonentModel->agent->user->id : null)], ['target' => '_blank']) : null,
                        'format' => 'raw',
                        'visible' => !empty($abonentModel->agent_id),
                    ],                    
                    [
                        'attribute' => 'parent_id',
                        'value' => !empty($abonentModel->parent_id) ? Html::a($abonentModel->parent->name, ['/client/view', 'id' => $abonentModel->parent_id], ['target' => '_blank']) : null,
                        'format' => 'raw',
                        'visible' => !empty($abonentModel->parent_id),
                    ],
                    [
                        'attribute' => 'client_type',
                        'value' => isset($model->client_type) ? $model->clientTypeList[$model->client_type] : null,
                    ],
                    [
                        'attribute' => 'it_is',
                        'value' => isset($abonentModel->it_is) ? $model->clientItIsList[$abonentModel->it_is] : null,
                        'visible' => $isAdmin,
                    ],
                    [
                        'attribute' => 'status',
                        'value' => isset($abonentModel->status) ? $model->clientStatusList[$abonentModel->status] : null,
                    ],
                    [
                        'attribute' => 'gen_number',
                        'value' => $abonentModel->gen_number,
                        'visible' => !empty($abonentModel->gen_number),
                    ],
                    'email_address:email',
                    'phone_number',
                    'nace_code:ntext',
                    'network:ntext',
                    [
                        'attribute' => 'comment',
                        'value' => $abonentModel->comment,
                        'format' => 'ntext', 
                    ],                    
                    [
                        'attribute' => 'uploaded_file_id',
                        'value' => !empty($logoPath) ? Html::img($logoPath, ['width' => '100px']) : null,
                        'format' => 'html',    
                        'visible' => !empty($logoPath),
                    ],                            
                    [
                        'attribute' => 'deleted',
                        'format' => 'boolean',
                        'visible' => $isAdmin,
                    ],
                    //'create_time',
                    //'create_user_id',
                    //'update_time',
                    //'update_user_id',
                ],
            ]) ?>

            <?= $this->render('_bank_table_view', [
                'dataProvider' => $clientBankdataProvider,
                'searchModel' => $clientBankSearchModel,
                'clientModel' => $model,
                'isAdmin' => $isAdmin,
            ]) ?>                    
        </div>
        
        <?php
            $body = ob_get_contents();
            ob_get_clean(); 

            $panelContent = [
                'heading' => Yii::t('client', 'About client'),
                'preBody' => '<div class="panel-body">',
                'body' => $body,
                'postBody' => '</div>',
            ];
            echo Html::panel(
                $panelContent, 
                'primary', 
                [
                    'id' => "panel-client-data",
                ]
            );
        ?>
        
    </div>
</div>


