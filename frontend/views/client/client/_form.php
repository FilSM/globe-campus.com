<?php
namespace common\models\client;

use Yii;
use yii\widgets\MaskedInput;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;
use kartik\checkbox\CheckboxX;
use kartik\widgets\FileInput;

use borales\extensions\phoneInput\PhoneInput;

use common\components\FSMAccessHelper;
use common\components\FSMHtml;
use common\widgets\EnumInput;

/* @var $this yii\web\View */
/* @var $model common\models\Client */
/* @var $form yii\widgets\ActiveForm */
$isModal = !empty($isModal);
$isAdmin = !empty($isAdmin);
?>

<div class="<?php empty($registerAction) ? 'client-form' : 'client-register-form'?>">
    <?php if($isModal) : Pjax::begin(Yii::$app->params['PjaxModalOptions']); endif; ?>
    <?php if(!isset($fromAbonent)) : ?>
    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'id' => $model->tableName().'-form',
        'formConfig' => [
            'labelSpan' => 3,
        ],
        'fieldConfig' => [
            //'template' => "{label}\n<div class=\"col-md-9\">{input}</div>\n<div class=\"col-sm-offset-3 col-md-9\">\n{hint}\n{error}</div>",
            //'labelOptions' => ['class' => 'col-md-3 control-label'],
            'showHints' => true,
        ],
        'options' => [
            'enctype' => 'multipart/form-data',
            'data-pjax' => $isModal,
        ],
    ]);
    ?>        
    <?php endif; ?>

    <?php if(!empty($isAdmin)) : ?>
    <?= $form->field($abonentModel, 'it_is')->widget(EnumInput::class, [
            'type' => EnumInput::TYPE_RADIOBUTTON,
            'options' => [
                'translate' => $model->clientItIsList,
            ],
        ]); 
    ?>  
    <?php endif; ?>
    
    <?= $form->field($abonentModel, 'status')->widget(EnumInput::class, [
            'type' => EnumInput::TYPE_RADIOBUTTON,
            'options' => [
                'translate' => $model->clientStatusList,
            ],
        ]); 
    ?>
    
    <?= $form->field($model, 'client_type')->widget(EnumInput::class, [
            'type' => EnumInput::TYPE_RADIOBUTTON,
            'options' => [
                'translate' => $model->clientTypeList,
            ],
        ])->label((empty($registerAction) ? $model->getAttributeLabel('client_type') : Yii::t('user', 'User type'))); 
    ?>  
    <?php if(FSMAccessHelper::can('createClientGroup')): ?>
    <?= $form->field($abonentModel, 'client_group_id')->widget(Select2::class, [
            'data' => $clientGroupList,
            'options' => [
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => !$abonentModel->isAttributeRequired('client_group_id'),
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],
            'addon' => 
                [
                'prepend' => $model::getModalButtonContent([
                    'formId' => $form->id,
                    'controller' => 'client-group',
                    'options' => [
                        //'title' => 'Add new client group',
                    ]
                ]),
            ],        
        ]); 
    ?>
    <?php endif; ?>
    
    <?php if(!isset($fromAbonent)) : ?>
    <?= $form->field($abonentModel, 'parent_id', [
        ])->widget(Select2::class, [
        'initValueText' => empty($abonentModel->parent_id) ? '' : $abonentModel->parent->name,
        'options' => [
            'placeholder' => '...',
        ],
        'pluginOptions' => [
            'allowClear' => !$abonentModel->isAttributeRequired('parent_id'),
            'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            'minimumInputLength' => 3,
            'ajax' => [
                'url' => Url::to(['/client/ajax-name-list?it_is=abonent']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
            'templateResult' => new JsExpression('function(data) { return data.text; }'),
            'templateSelection' => new JsExpression('function(data) { return data.text; }'),       
        ],           
    ]); ?>
    <?php endif; ?>
    
    <?= $form->field($model, 'name')->textInput([
        'maxlength' => 64,
        'data-labels' => [
            'physical' => Yii::t('client', 'Firstname, Lastname'),
            'legal' => $model->getAttributeLabel('name'),
        ],
    ]); ?>

    <?= $form->field($model, 'reg_number', [
        'addon' => [
            'prepend' => [
                'content' => 
                    Html::button(Html::icon('search'), [
                        'id' => 'btn-lursoft-search', 
                        'class'=>'btn btn-default',
                        'value' => Url::to(["/client/ajax-get-lursoft-data"]),
                        'title' => Yii::t('client', 'Get data from Lursoft service'),
                    ]),
                'asButton' => true, 
            ],
        ],
    ])->textInput([
            'maxlength' => 30,
            'data-labels' => [
                'physical' => Yii::t('client', 'Personal code'),
                'legal' => $model->getAttributeLabel('reg_number'),
            ],
        ]); 
    ?>

    <?= $form->field($model, 'vat_payer')->widget(EnumInput::class, [
            'data' => $model->getVATPayerTypeList(),
            'type' => EnumInput::TYPE_RADIOBUTTON,
        ]); 
    ?>   

    <?= $form->field($model, 'vat_number', [
        'addon' => [
            'prepend' => [
                'content' => 
                    Html::button(Html::icon('search'), [
                        'id' => 'btn-vies-search', 
                        'class'=>'btn btn-default',
                        'value' => Url::to(["/client/ajax-get-vies-data"]),
                        'title' => Yii::t('client', 'Get data from VIES service'),
                    ]),
                'asButton' => true, 
            ],
        ],
    ])->textInput(['maxlength' => 30]) ?>
    
    <?= $form->field($model, 'tax')->widget(MaskedInput::class, [
        'mask' => '9{1,2}',
    ]); ?>

    <?php 
        $body = $form->field($model, 'legal_address')->textInput()->label(Yii::t('address', 'Address'));

        $body .= $form->field($model, 'legal_country_id')->widget(Select2::class, [
            'data' => $countryList,
            'options' => [
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => !$model->isAttributeRequired('legal_country_id'),
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],
            'addon' => [
                'prepend' => $model::getModalButtonContent([
                    'formId' => $form->id,
                    'controller' => 'country',
                ]),
            ],        
        ]); 
        
        $panelContent = [
            'heading' => $model->getAttributeLabel('legal_address'),
            'preBody' => '<div class="panel-body">',
            'body' => $body,
            'postBody' => '</div>',
        ];
        
        echo Html::beginTag('div', [
            'id' => 'legal-address-data-container',
            'class' => 'form-group',
        ]);
        echo Html::beginTag('div', [
            'class' => 'col-md-3',
        ]);
        echo Html::endTag('div');
        echo Html::beginTag('div', [
            'class' => 'col-md-9',
        ]);
        
        echo FSMHtml::panel(
            $panelContent, 
            'default', 
            [
                'id' => "legal-address-data",
                'data-labels' => [
                    'physical' => Yii::t('client', 'Registration address'),
                    'legal' => $model->getAttributeLabel('legal_address'),
                ],
                //'style' => "background: antiquewhite;",
            ]
        );
        echo Html::endTag('div');
        echo Html::endTag('div');           
    ?>    
    
    <?php
        echo Html::beginTag('div', [
            'class' => 'form-group',
        ]);
        echo Html::beginTag('div', [
            'class' => 'col-md-3',
        ]);
        echo Html::endTag('div');
        echo Html::beginTag('div', [
            'id' => 'cbx-use-address-container',
            'class' => 'col-md-9',
        ]);
        echo CheckboxX::widget([
            'name' => 'use_legal_address',
            'value' => (empty($model->legal_address) && empty($model->office_address) || ($model->legal_address == $model->office_address)),
            'options' => ['id' => 'cbx-use-legal-address'],
            'pluginOptions' => ['threeState' => false],
        ]);
        $labelLegalTxt = Yii::t('client', 'For office address to use the legal address');
        echo Html::label($labelLegalTxt, 'cbx-use-legal-address',
            [
                'class' => 'cbx-label fsm-label',
                'data-labels' => [
                    'physical' => Yii::t('client', 'For home address to use the registration address'),
                    'legal' => $labelLegalTxt,
                ],
            ]
        );
        echo '<div class="help-block"></div>';
        echo Html::endTag('div');
        echo Html::endTag('div');    
    ?>
    
    <?php 
        $body = $form->field($model, 'office_address')->textInput()->label(Yii::t('address', 'Address'));

        $body .= $form->field($model, 'office_country_id')->widget(Select2::class, [
            'data' => $countryList,
            'options' => [
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => !$model->isAttributeRequired('office_country_id'),
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],
            'addon' => [
                'prepend' => $model::getModalButtonContent([
                    'formId' => $form->id,
                    'controller' => 'country',
                ]),
            ],        
        ]); 
        
        $panelContent = [
            'heading' => $model->getAttributeLabel('office_address'),
            'preBody' => '<div class="panel-body">',
            'body' => $body,
            'postBody' => '</div>',
        ];
        
        echo Html::beginTag('div', [
            'id' => 'office-address-data-container',
            'class' => 'form-group',
        ]);
        echo Html::beginTag('div', [
            'class' => 'col-md-3',
        ]);
        echo Html::endTag('div');
        echo Html::beginTag('div', [
            'class' => 'col-md-9',
        ]);
        
        echo FSMHtml::panel(
            $panelContent, 
            'default', 
            [
                'id' => "office-address-data",
                'data-labels' => [
                    'physical' => Yii::t('client', 'Home address'),
                    'legal' => $model->getAttributeLabel('office_address'),
                ],
                //'style' => "background: antiquewhite;",
            ]
        );
        echo Html::endTag('div');
        echo Html::endTag('div');           
    ?>    
    
    <?php 
        $body = $form->field($mainContactModel, 'first_name')->textInput(['maxlength' => true]);

        $body .= $form->field($mainContactModel, 'last_name')->textInput(['maxlength' => true]);

        $body .= $form->field($mainContactModel, 'phone', [
            'options' => [
                'class' => 'form-group '.((Yii::$app->params['appSector'] == 'estate') ? 'required' : '')
            ],
            'inputOptions' => [
                'value' => !empty($mainContactModel->phone) ? $mainContactModel->phone : 
                    (!empty(Yii::$app->params['defaultPhoneCode']) ? Yii::$app->params['defaultPhoneCode'] : ''),
            ]
        ])->widget(MaskedInput::class, [
            'clientOptions' => [
                'greedy' => false,
            ],
            'mask' => '(+9{1,4}) 9{8,15}',
            'options' => [
                'class' => 'form-control',
                'placeholder' => Yii::t('common', 'Enter as') . ' (+999) 9999999999...',
            ],
        ]);
    
        $body .= $form->field($mainContactModel, 'email')
            ->widget(MaskedInput::class, [
                'clientOptions' => [
                    'alias' => 'email',
                ],
            ]);
        
        $panelContent = [
            'heading' => Yii::t('client', 'Main contact person'),
            'preBody' => '<div class="panel-body">',
            'body' => $body,
            'postBody' => '</div>',
        ];
        
        echo Html::beginTag('div', [
            'id' => 'client-contact-data-container',
            'class' => 'form-group',
        ]);
        echo Html::beginTag('div', [
            'class' => 'col-md-3',
        ]);
        echo Html::endTag('div');
        echo Html::beginTag('div', [
            'class' => 'col-md-9',
        ]);
        
        echo FSMHtml::panel(
            $panelContent, 
            'default', 
            [
                'id' => "client-contact-data",
                'data-labels' => [
                    'physical' => Yii::t('client', 'Main contact data'),
                    'legal' => Yii::t('client', 'Main contact person'),
                ],                
            ]
        );
        echo Html::endTag('div');
        echo Html::endTag('div');           
    ?>  
    
    <?= $form->field($abonentModel, 'language_id')->widget(Select2::class, [
        'data' => $languageList,
        'options' => [
            'placeholder' => '...',
        ],
        'pluginOptions' => [
            'allowClear' => !$abonentModel->isAttributeRequired('language_id'),
            'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
        ],            
    ]); ?>

    <?php 
    if(!empty($registerAction) || empty($isAdmin)): 
        echo Html::activeHiddenInput($abonentModel, 'manager_id');    
    else : 
        echo $form->field($abonentModel, 'manager_id')->widget(Select2::class, [
            'data' => $managerList,
            'options' => [
                'placeholder' => '...',
                'id' => 'client-manager-id',
            ],   
            'pluginOptions' => [
                'allowClear' => !$abonentModel->isAttributeRequired('manager_id'),
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],            
        ]); 
    endif; 
    
    if(!empty($model->id) && !empty($agentList)){
        echo $form->field($abonentModel, 'agent_id')->widget(Select2::class, [
            'data' => $agentList,
            'options' => [
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => !$abonentModel->isAttributeRequired('agent_id'),
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],            
        ]); 
    }
    ?>
    
    <?php /*
    <div class="padding-v-md">
        <div class="line line-dashed"></div>
    </div> 
     * 
     */?>
    
    <?php
        echo Html::beginTag('div', [
            'id' => 'bank-data-container',
            'class' => 'form-group',
        ]);
        echo Html::beginTag('div', [
            'class' => 'col-md-12',
        ]);
    ?>
    
    <?= $this->render('_bank_table_edit', [
        'form' => $form,
        'model' => $clientBankModel,
        'bankList' => $bankList,
        'isModal' => $isModal,
    ]) ?>

    <?php
        echo Html::endTag('div');
        echo Html::endTag('div');           
    ?>     
    
    <?= $form->field($abonentModel, 'debit', [
        'addon' => [
            'append' => [
                'content' => Select2::widget([
                    'model' => $abonentModel,
                    'attribute' => 'debit_valuta_id',
                    'data' => $valutaList, 
                    'options' => [
                        'id' => 'debit-valuta-id', 
                        'placeholder' => '...',
                        //'style' => 'min-width: 90px;'
                    ],
                    'pluginOptions' => [
                        'allowClear' => !$abonentModel->isAttributeRequired('debit_valuta_id'),
                        'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
                    ],            
                    'size' => 'control-width-90',
                ]),
                'asButton' => true,
            ],
        ],
        //'labelOptions' => ['class' => 'col-md-3'],
    ])->widget(MaskedInput::class, [
        //'mask' => '[-]9{1,10}[.9{1,2}]',
        'options' => [
            'class' => 'form-control number-field',
        ],
        'clientOptions' => [
            'alias' => 'decimal',
            'rightAlign' => false,
            'digits' => 2,
            'allowMinus' => true,
        ],                                    
    ])->label($abonentModel->getAttributeLabel('debit').' / '.$abonentModel->getAttributeLabel('debit_valuta_id'));
    ?>
    
    <?php $preview = !empty($filesModel->uploadedFileUrl) ? [$filesModel->uploadedFileUrl] : [];
        echo $form->field($filesModel, 'file')->widget(FileInput::class, [
            'language' =>  strtolower(substr(Yii::$app->language, 0, 2)),
            'sortThumbs' => false,
            'options' => [
                'id' => 'client-files',
                //'multiple' => true,
            ],
            'pluginOptions' => [
                'allowedFileExtensions' => ['png', 'jpg', 'jpeg'],
                'maxFileSize' => 5000,
                'maxFileCount' => 1,
                'showRemove' => false,
                'showUpload' => false,
                //'showCaption' => false,
                //'browseClass' => 'btn btn-primary btn-block',
                'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                'initialPreview' => $preview,
                'initialPreviewShowDelete' => true,                
                'initialPreviewAsData' => true,
                'initialPreviewConfig' => [
                    [
                        'type' => "image",
                        'size' => $filesModel->filesize, 
                        'caption' => $filesModel->filename, 
                        'url' => Url::to(['/client/logo-delete', 'deleted_id' => $filesModel->id]), 
                        //'key' => 101,
                    ],
                ],
                'overwriteInitial' => false,
            ],
        ])->label(Yii::t('files', 'Logo')); 
    ?>
    
    <?= $form->field($abonentModel, 'gen_number')->textInput(['maxlength' => true]); ?>

    <?= $form->field($model, 'email_address')->widget(MaskedInput::class, [
        'clientOptions' => [
            'alias' => 'email',
        ],
    ]);
    ?>

    <?= $form->field($model, 'phone_number')->widget(PhoneInput::className(), [
        'jsOptions' => [
            'preferredCountries' => ArrayHelper::getValue(Yii::$app->params, 'defaultPhoneCode', null),
        ]
    ]); ?>
        
    <?= $form->field($model, 'nace_code')->textarea(['rows' => 3]) ?>
        
    <?= $form->field($model, 'network')->textarea(['rows' => 3]) ?>
        
    <?= $form->field($abonentModel, 'comment')->textarea(['rows' => 3]) ?>
    
    <?php if(!empty($model->id) && !empty($isAdmin)){
        echo $form->field($model, 'deleted')->widget(SwitchInput::class, [
            'pluginOptions' => [
                'onText' => Yii::t('common', 'Yes'),
                'offText' => Yii::t('common', 'No'),
            ],
        ]);
    } ?>
    
    <?php if(!isset($fromAbonent)) : ?>         
    <div class="form-group clearfix double-line-top">
        <div class="col-md-offset-8 col-md-4" style="text-align: right;">
            <?php if(empty($registerAction)) :
                echo $model->SubmitButton; 
            else :
                echo Html::submitButton(Yii::t('user', 'Sign up'), ['class' => 'btn btn-lg btn-success']); 
            endif; ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>
    <?php endif; ?>

<?php if(!isset($fromAbonent)) : ?>    
<?php ActiveForm::end(); ?>
<?php endif; ?>
<?php if($isModal) : Pjax::end(); endif; ?>
</div>