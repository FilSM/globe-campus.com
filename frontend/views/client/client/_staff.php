<?php

use yii\helpers\Url;

use kartik\helpers\Html;

use common\components\FSMAccessHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Client */

$this->title = $model->name.': '.Yii::t('client', 'Staff');
if(FSMAccessHelper::checkRoute('/client/*')){
    $this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
}
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['client/view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row client-view">

    <div class="col-md-2">
        <?=
        $this->render('_menu', [
            'client' => $model,
            'activeItem' => 'staff',
        ])
        ?>
    </div>

    <div class="col-md-10">
        <?php 
            ob_start();
            ob_implicit_flush(false);
        ?>
        <div class='col-md-12'>
            <?php if(FSMAccessHelper::can('administrateUser')): ?>
            <?= \common\components\FSMHelper::vButton(null, [
                'label' => Yii::t('fsmuser', 'Add user'),
                'title' => Yii::t('fsmuser', 'Add user'),
                'url' => Url::to('@web/user/admin/create'),
                'icon' => 'plus',
                'modal' => true,
                'options' => [
                    'id' => 'modal-add-user-to-company',
                ],                
            ]); ?>
            <?php endif; ?>
        </div>

        <div class='col-md-12'>
            <p></p>
        </div>  

        <div class='col-md-12'>
            <?= Html::panel(
                [
                    //'heading' => Yii::t('client', 'Client staff'),
                    'body' => Html::listGroup($userList),
                ], 
                'primary'
            );?>
        </div>
        
        <?php
            $body = ob_get_contents();
            ob_get_clean(); 

            $panelContent = [
                'heading' => Yii::t('client', 'Client staff'),
                'preBody' => '<div class="panel-body"><div class="cargo-staff-index">',
                'body' => $body,
                'postBody' => '</div></div>',
            ];
            echo Html::panel(
                $panelContent, 
                'primary', 
                [
                    'id' => "panel-staff-data",
                ]
            );
        ?>        
    </div>
</div>


