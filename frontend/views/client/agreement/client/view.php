<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\client\Agreement */

$this->title = $model->modelTitle() .' #'. $model->number;
$this->params['breadcrumbs'][] = ['label' => $fromClientModel->modelTitle(2), 'url' => ['client/index']];
$this->params['breadcrumbs'][] = ['label' => $fromClientModel->name, 'url' => ['client/view', 'id' => $fromClientModel->id]];
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index', 'client_id' => $fromClientModel->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row client-agreement-view">
    
    <div class="col-md-2">
        <?= $this->render('@frontend/views/client/client/_menu', [
            'client' => $fromClientModel,
            'activeItem' => $model->tableName(),
        ])
        ?>
    </div>
    
    <div class="col-md-10">

        <?php /*= Html::pageHeader(Html::encode($this->title)); */?>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?php 
            $body = $this->render('../_view', [
                'model' => $model,
                'paymentModel' => $paymentModel,
                'historyModel' => $historyModel,
                'paymentDataProvider' => $paymentDataProvider,
                'historyDataProvider' => $historyDataProvider,
                'filesModel' => $filesModel,
                'fromClientModel' => $fromClientModel,
                'isAdmin' => $isAdmin,
            ]); 

            $panelContent = [
                'heading' => Html::encode($this->title),
                'preBody' => '<div class="panel-body">',
                'body' => $body,
                'postBody' => '</div>',
            ];
            echo Html::panel(
                $panelContent, 
                'primary', 
                [
                    'id' => "panel-client-agreement-data",
                ]
            );        
        ?>
    </div>

</div>