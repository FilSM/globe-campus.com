<?php

use kartik\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\client\ClientContact */

$this->title = Yii::t($model->tableName(), 'Create a new '.$model->modelTitle(1, false));
$this->params['breadcrumbs'][] = ['label' => $fromClientModel->modelTitle(2), 'url' => ['client/index']];
$this->params['breadcrumbs'][] = ['label' => $fromClientModel->name, 'url' => ['client/view', 'id' => $fromClientModel->id]];
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index', 'client_id' => $fromClientModel->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row client-agreement-create">
    
    <div class="col-md-2">
        <?=
        $this->render('@frontend/views/client/client/_menu', [
            'client' => $fromClientModel,
            'activeItem' => $model->tableName(),
        ])
        ?>
    </div>
    
    <div class="col-md-10">
        <?php 
            $body = $this->render('../_form', [
                'model' => $model,
                'abonentModel' => $abonentModel,
                'filesModel' => $filesModel,
                'fromClientModel' => $fromClientModel,
                'firstPersonModel' => $firstPersonModel,
                'secondPersonModel' => $secondPersonModel,
                'projectList' => $projectList,
                'valutaList' => $valutaList,
                'clientList' => $clientList,
                'clientRoleList' => $clientRoleList,
                'isAdmin' => $isAdmin,
            ]);
            
            $panelContent = [
                'heading' => Html::encode($this->title),
                'preBody' => '<div class="panel-body">',
                'body' => $body,
                'postBody' => '</div>',
            ];
            echo Html::panel(
                $panelContent, 
                'primary', 
                [
                    'id' => "panel-client-agreement-data",
                ]
            );             
        ?>
    </div>

</div>