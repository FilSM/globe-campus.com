<?php
namespace common\models\client;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use kartik\helpers\Html;
use kartik\grid\GridView;

use common\components\FSMAccessHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\client\search\AgreementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $searchModel->modelTitle(2);
$this->params['breadcrumbs'][] = ['label' => $fromClientModel->modelTitle(2), 'url' => ['client/index']];
$this->params['breadcrumbs'][] = ['label' => $fromClientModel->name, 'url' => ['client/view', 'id' => $fromClientModel->id]];
$this->params['breadcrumbs'][] = $this->title;
$forDetail = !empty($forDetail);
?>
<div class="row client-agreement-index">

    <div class="col-md-2">
        <?= $this->render('@frontend/views/client/client/_menu', [
            'client' => $fromClientModel,
            'activeItem' => $searchModel->tableName(),
        ])
        ?>
    </div>
    
    <div class="col-md-10">

        <?php /*= Html::pageHeader(Html::encode($this->title)); */?>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?php 
            $body = FSMAccessHelper::can('createAgreement') ?
                '<p>'.
                Html::a(Html::icon('plus').'&nbsp;'.$searchModel->modelTitle(), ['create', 'client_id' => $fromClientModel->id], ['class' => 'btn btn-success']).
                '</p>' :
                '';
            $body .= GridView::widget([
                'responsive' => false,
                //'striped' => false,
                'hover' => true,
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'floatHeader' => true,
                'columns' => [
                    //['class' => '\kartik\grid\SerialColumn'],

                    [
                        'attribute' => 'id',
                        'width' => '75px',
                        'hAlign' => 'center',
                    ],
                    [
                        'attribute' => 'number',
                        'contentOptions' => [
                            'style'=>'max-width:100px; min-height:75px; word-wrap: break-word;'
                        ],
                        'value' => function ($model) use ($fromClientModel) {
                            return 
                                '<div style="overflow-x: auto;">'.
                                (!empty($model->number) ? (FSMAccessHelper::can('viewAgreement', $model) ?
                                    Html::a($model->number, ['/agreement/view', 'id' => $model->id, 'client_id' => (isset($fromClientModel) ? $fromClientModel->id : null)], ['data-pjax' => 0]) :
                                    $model->number
                                )
                                : null).
                                '</div>';
                        },                        
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'agreement_type',
                        //'headerOptions' => ['class'=>'td-mw-100'],
                        'value' => function ($model) {
                            return isset($model->agreement_type) ? $model->agreementTypeList[$model->agreement_type] : null;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => $searchModel->agreementTypeList,
                        'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                        'filterInputOptions' => ['placeholder' => '...'],
                        'visible' => $isAdmin,
                        'format'=>'raw',
                    ],
                    [
                        'attribute'=>'project_name',
                        'contentOptions' => [
                            'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                        ],

                        'value' => function ($model) {
                            $currentAbonentAgreement = $model->currentAbonentAgreement;
                            $result = [];
                            foreach ($currentAbonentAgreement->abonentAgreementProjects as $abonentAgreementProject) {
                                $result[] = (FSMAccessHelper::can('viewProject', $abonentAgreementProject->project) ?
                                    Html::a($abonentAgreementProject->projectName, ['/project/view', 'id' => $abonentAgreementProject->project_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                    $abonentAgreementProject->projectName 
                                );
                            }
                            $content = implode('<br/>', $result);
                            return 
                                '<div style="overflow-x: auto;">'.$content.'</div>';                    
                        },                         
                        'format'=>'raw',
                        'visible' => FSMAccessHelper::can('viewProject') && !$forDetail,
                    ],            
                    [
                        'attribute' => 'status',
                        'hAlign' => 'center',
                        'vAlign' => 'middle',
                        'headerOptions' => ['class'=>'td-mw-100'],
                        'value' => function ($model) {
                            return isset($model->status) ? Html::badge($model->agreementStatusList[$model->status], ['class' => $model->statusBackgroundColor.' status-badge']) : null;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => $searchModel->agreementStatusList,
                        'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                        'filterInputOptions' => ['placeholder' => '...'],
                        'visible' => $isAdmin,
                        'format'=>'raw',
                    ],
                    [
                        'attribute' => 'grid_progress',
                        'header' => Yii::t('common', 'Progress'),
                        'headerOptions' => ['class'=>'td-mw-100'],
                        'contentOptions' => [
                            'class' => 'td-slider',
                        ],
                        'hAlign' => 'center',
                        'vAlign' => 'middle',
                        'mergeHeader' => true,
                        'value' => function ($model) {
                            return $model->getProgressButtons();
                        },
                        'format' => 'raw',
                        'visible' => FSMAccessHelper::can('changeAgreementStatus'),
                        'hiddenFromExport' => true,
                    ],
                    [
                        'attribute'=>'first_client_name',
                        //'headerOptions' => ['class'=>'td-mw-100'],
                        'contentOptions' => [
                            'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                        ],
                        'value' => function ($model) {
                            return 
                                '<div style="overflow-x: auto;">'.
                                (!empty($model->first_client_id) ? 
                                (!empty($model->first_client_role_id) ? $model->firstClientRole->name.':<br/>' : '') . 
                                    (FSMAccessHelper::can('viewClient', $model->firstClient) ?
                                        Html::a($model->first_client_name, ['/client/view', 'id' => $model->first_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                        $model->first_client_name
                                    )
                                : null).
                                '</div>';
                        },                         
                        'format'=>'raw',
                        //'hiddenFromExport' => true,
                    ],            
                    [
                        'attribute'=>'second_client_name',
                        //'headerOptions' => ['class'=>'td-mw-100'],
                        'contentOptions' => [
                            'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                        ],
                        'value' => function ($model) {
                            return 
                                '<div style="overflow-x: auto;">'.
                                (!empty($model->second_client_id) ?  
                                (!empty($model->second_client_role_id) ? $model->secondClientRole->name.':<br/>' : '') . 
                                    (FSMAccessHelper::can('viewClient', $model->secondClient) ?
                                        Html::a($model->second_client_name, ['/client/view', 'id' => $model->second_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                        $model->second_client_name
                                    )
                                : null).
                                '</div>';
                        },                         
                        'format'=>'raw',
                        //'hiddenFromExport' => true,
                    ],            
                    [
                        'attribute'=>'third_client_name',
                        //'headerOptions' => ['class'=>'td-mw-100'],
                        'contentOptions' => [
                            'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                        ],
                        'value' => function ($model) {
                            return 
                                '<div style="overflow-x: auto;">'.
                                (!empty($model->third_client_id) ?   
                                (!empty($model->third_client_role_id) ? $model->thirdClientRole->name.':<br/>' : '') . 
                                    (FSMAccessHelper::can('viewClient', $model->thirdClient) ?
                                        Html::a($model->third_client_name, ['/client/view', 'id' => $model->third_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                        $model->third_client_name
                                    )
                                : null).
                                '</div>';
                        },                         
                        'format'=>'raw',
                        //'hiddenFromExport' => true,
                    ],            
                    [
                        'attribute' => 'signing_date',
                        'headerOptions' => [
                            'class' => 'td-mw-75',
                        ],
                        'value' => function ($model) {
                            return !empty($model->signing_date) ? date('d-M-Y', strtotime($model->signing_date)) : null;
                        },                 
                    ],                         
                    [
                        'attribute' => 'due_date',
                        'headerOptions' => [
                            'class' => 'td-mw-75',
                        ],
                        'value' => function ($model) {
                            return !empty($model->due_date) ? date('d-M-Y', strtotime($model->due_date)) : null;
                        },                 
                    ],                                                 
                    [
                        'attribute' => 'summa',
                        'hAlign' => 'right',
                        'headerOptions' => [
                            'class' => 'td-mw-75',
                            //'style' => 'text-align: left;',
                        ],
                    ],                 
                                /*
                    [
                        'attribute' => 'rate',
                        'hAlign' => 'right',
                        'headerOptions' => [
                            'class' => 'td-mw-75',
                            //'style' => 'text-align: left;',
                        ],
                    ],      
                                 * 
                                 */
                    [
                        'attribute' => 'conclusion',
                        'headerOptions' => ['class'=>'td-mw-100'],                
                        'value' => function ($model) {
                            return isset($model->status) ? $model->agreementConclusionList[$model->conclusion] : null;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => $searchModel->agreementConclusionList,
                        'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                        'filterInputOptions' => ['placeholder' => '...'],
                        'visible' => $isAdmin,
                    ],
                    [
                        'attribute' => 'transfer_status',
                        'hAlign' => 'center',
                        'vAlign' => 'middle',
                        'headerOptions' => ['class'=>'td-mw-100'],
                        'value' => function ($model) {
                            return $model->transferStatusHTML;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => $searchModel->agreementTransferStatusList,
                        'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                        'filterInputOptions' => [
                            'placeholder' => '...',
                            //'multiple' => true,
                        ],
                        'format'=>'raw',
                        'visible' => !empty(Yii::$app->params['ENABLE_BILL_TRANSFER']),
                    ],                          
                    [
                        'class' => '\common\components\FSMActionColumn',
                        'headerOptions' => ['class'=>'td-mw-125'],
                        'dropdown' => true,
                        'dropdownDefaultBtn' => 'attachment',
                        'checkPermission' => true,
                            'template' => !empty(Yii::$app->params['ENABLE_BILL_TRANSFER']) ?
                                '{attachment} {view} {payment} {send-other-abonent} {receive-other-abonent} {return} {update} {delete}' :
                                '{attachment} {view} {payment} {update} {delete}',
                            'buttons' => [
                                'attachment' => function (array $params) { 
                                    return Agreement::getButtonAttachment($params);
                                },
                                'payment' => function (array $params) { 
                                    return Agreement::getButtonPayment($params);
                                },
                                'send-other-abonent' => function (array $params) { 
                                    return Agreement::getButtonSendOtherAbonent($params);
                                },
                                'receive-other-abonent' => function (array $params) { 
                                    return Agreement::getButtonReceiveOtherAbonent($params);
                                },
                                'return' => function (array $params) { 
                                    return Agreement::getButtonReturn($params);
                                },
                            ],
                        'linkedObj' => [
                            ['fieldName' => 'client_id', 'id' => $fromClientModel->id],
                        ],                                    
                        'visible' => !$forDetail,
                    ],                        
                ]
            ]); 

            $panelContent = [
                'heading' => Html::encode($this->title),
                'preBody' => '<div class="panel-body">',
                'body' => $body,
                'postBody' => '</div>',
            ];
            echo Html::panel(
                $panelContent, 
                'primary', 
                [
                    'id' => "panel-client-agreement-data",
                ]
            );        
        ?>
    </div>
</div>