<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\client\Agreement */

$this->title = Yii::t($model->tableName(), 'Update a '.$model->modelTitle(1, false)) . ': #'. $model->number;
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->number, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Edit');
?>
<div class="agreement-update">

    <?= Html::pageHeader(Html::encode($this->title)); ?>

    <?= $this->render('_form', [
        'model' => $model,
        'filesModel' => $filesModel,
        'firstPersonModel' => $firstPersonModel,
        'secondPersonModel' => $secondPersonModel,
        'projectList' => $projectList,
        'valutaList' => $valutaList,
        'clientList' => $clientList,
        'clientRoleList' => $clientRoleList,
        'isAdmin' => $isAdmin,
    ]) ?>

</div>