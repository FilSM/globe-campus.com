<?php
namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;
use kartik\icons\Icon;
use kartik\dialog\Dialog;

use unclead\multipleinput\MultipleInput;

use common\widgets\EnumInput;

/* @var $this yii\web\View */
/* @var $model common\models\client\Agreement */
/* @var $form yii\widgets\ActiveForm */

Icon::map($this);
$isModal = !empty($isModal);
?>
  
<?= Dialog::widget([
    'libName' => 'krajeeDialogWarning',
    'options' => [
        'draggable' => true, 
        'type' => Dialog::TYPE_WARNING,
        'title' => Yii::t('common', 'Confirmation'),
    ],
]);?> 

<div class="agreement-form">
    <?php if($isModal) : Pjax::begin(Yii::$app->params['PjaxModalOptions']); endif; ?>
    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => $model->tableName().'-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'options' => [
            'data-pjax' => $isModal,
        ],         
    ]); ?>
    
    <?= $form->field($model, 'agreement_type')->widget(EnumInput::class, [
            'type' => EnumInput::TYPE_RADIOBUTTON,
            'options' => [
                'translate' => $model->agreementTypeList,
            ],
        ]); 
    ?> 
    
    <?php if(empty($model->id) && !empty($model->project_id)) : ?>
        <?= Html::activeHiddenInput($model, 'project_id'); ?>
    <?php else: ?>    
        <?= $form->field($model, 'project_id')->widget(Select2::class, [
                'data' => $projectList,
                'options' => [
                    'placeholder' => '...',
                    'multiple' => true,
                ],   
                'pluginOptions' => [
                    'allowClear' => true,
                    'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
                ],
                'addon' => [
                    'prepend' => $model::getModalButtonContent([
                        'formId' => $form->id,
                        'controller' => 'project',
                        'addNewBtnTitle' => Yii::t('bill', 'Add new project'),
                        'options' => [
                            'style' => 'height: 39px;',
                        ],   
                    ]),
                ],        
            ]); 
        ?>
    <?php endif; ?>
    
    <?= $form->field($model, 'parent_id')->widget(DepDrop::class, [
        'type' => DepDrop::TYPE_SELECT2,
        'data' => empty($model->parent_id) ? null : [$model->parent_id => $model->parent->number],
        //'options' => ['placeholder' => '...'],
        'select2Options' => [
            'pluginOptions' => [
                'allowClear' => !$model->isAttributeRequired('parent_id'),
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],
        ],
        'pluginOptions' => [
            'depends' => ['agreement-project_id'],
            'initialize' => true,            
            //'initDepends' => ['agreement-project_id'],
            'url' => Url::to(['/project/ajax-get-agreement-list', 'without-id' => $model->id, 'without-selected' => true]),
            'placeholder' => '...',
        ],
    ]); ?>

    <?= $form->field($model, 'status')->widget(EnumInput::class, [
            'type' => EnumInput::TYPE_RADIOBUTTON,
            'options' => [
                'translate' => $model->agreementStatusList,
            ],
        ]); 
    ?> 

    <?php if(!empty($model->id) && !empty($isAdmin)) : 
        echo $form->field($model, 'transfer_status')->widget(EnumInput::class, [
                'type' => EnumInput::TYPE_RADIOBUTTON,
                'options' => [
                    'translate' => $model->agreementTransferStatusList,
                ],
            ]);
    else: 
        echo Html::activeHiddenInput($model, 'transfer_status');
    endif; ?>
            
    <?= $form->field($model, 'conclusion')->widget(EnumInput::class, [
            'type' => EnumInput::TYPE_RADIOBUTTON,
            'options' => [
                'translate' => $model->agreementConclusionList,
            ],
        ]); 
    ?> 
    <fieldset id="client-data" style="margin-bottom: 10px;">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <div class="col-md-6 double-line-top double-line-bottom first-client-data" style="background: aliceblue;">
                    <div class="col-md-12" style="padding: 0;">
                        <div class="field-group-title"><h3 id="first-party-title"><?= Yii::t('bill', 'First party data'); ?></h3></div>
                    </div>

                    <div class="col-md-12" style="padding: 0;">
                        <?= $form->field($model, 'first_client_role_id')->widget(Select2::class, [
                                'data' => $clientRoleList,
                                'options' => [
                                    'placeholder' => '...',
                                ],   
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
                                ],            
                                'addon' => [
                                    'prepend' => $model::getModalButtonContent([
                                        'formId' => $form->id,
                                        'prefix' => 'first-',
                                        'controller' => 'client-role',
                                    ]),
                                ],        
                            ]); 
                        ?>

                        <?= $form->field($model, 'first_client_id')->widget(Select2::class, [
                                'data' => $clientList,
                                'options' => [
                                    'id' => 'agreement-first-client-id', 
                                    'placeholder' => '...',
                                ],   
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
                                ],            
                                'addon' => [
                                    'prepend' => $model::getModalButtonContent([
                                        'formId' => $form->id,
                                        'prefix' => 'first-',
                                        'controller' => 'client',
                                    ]),
                                ],        
                            ]); 
                        ?>
                        
                        <?= $this->render('@frontend/views/client/agreement/_person_table_edit', [
                            'form' => $form,
                            'model' => $firstPersonModel,
                            'agreement' => $model,
                            'isModal' => $isModal,
                            'depends' => 'agreement-first-client-id',
                            'panel_person_id' => 'panel-first-person-data',
                            'docDate' => $model->signing_date ?? null,
                        ]) ?>
                    </div>    
                </div>
                
                <div class="col-md-6 double-line-top double-line-bottom second-client-data" style="background: antiquewhite;">
                    <div class="col-md-12" style="padding: 0;">
                        <div class="field-group-title"><h3 id="second-party-title"><?= Yii::t('bill', 'Second party data'); ?></h3></div>
                    </div>

                    <div class="col-md-12" style="padding: 0;">
                        <?= $form->field($model, 'second_client_role_id')->widget(Select2::class, [
                                'data' => $clientRoleList,
                                'options' => [
                                    'placeholder' => '...',
                                ],   
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
                                ],            
                                'addon' => [
                                    'prepend' => $model::getModalButtonContent([
                                        'formId' => $form->id,
                                        'prefix' => 'second-',
                                        'controller' => 'client-role',
                                    ]), 
                                ],        
                            ]); 
                        ?>

                        <?= $form->field($model, 'second_client_id')->widget(Select2::class, [
                                'data' => $clientList,
                                'options' => [
                                    'id' => 'agreement-second-client-id', 
                                    'placeholder' => '...',
                                ],   
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
                                ],            
                                'addon' => [
                                    'prepend' => $model::getModalButtonContent([
                                        'formId' => $form->id,
                                        'prefix' => 'second-',
                                        'controller' => 'client',
                                    ]), 
                                ],        
                            ]); 
                        ?>
                        
                        <?= $this->render('@frontend/views/client/agreement/_person_table_edit', [
                            'form' => $form,
                            'model' => $secondPersonModel,
                            'agreement' => $model,
                            'isModal' => $isModal,
                            'depends' => 'agreement-second-client-id',
                            'panel_person_id' => 'panel-second-person-data',
                            'docDate' => $model->signing_date ?? null,
                        ]) ?>
                    </div>    
                </div>
            </div>
        </div>
    </fieldset>
    
    <?= $form->field($model, 'third_client_role_id')->widget(Select2::class, [
            'data' => $clientRoleList,
            'options' => [
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => true,
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],            
            'addon' => [
                'prepend' => $model::getModalButtonContent([
                    'formId' => $form->id,
                    'prefix' => 'third-',
                    'controller' => 'client-role',
                ]), 
            ],        
        ]); 
    ?>
    
    <?= $form->field($model, 'third_client_id')->widget(Select2::class, [
            'data' => $clientList,
            'options' => [
                //'id' => 'third-client-id', 
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => true,
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],            
            'addon' => [
                'prepend' => $model::getModalButtonContent([
                    'formId' => $form->id,
                    'prefix' => 'third-',
                    'controller' => 'client',
                ]), 
            ],        
        ]); 
    ?>
    
    <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'signing_date')->widget(DatePicker::class, [
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
        ]); 
    ?>

    <?= $form->field($model, 'due_date')->widget(DatePicker::class, [
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
        ]); 
    ?>

    <?= $form->field($model, 'summa', [
        'addon' => [
            'append' => [
                ['content' => 
                    MaskedInput::widget([
                        'model' => $model,
                        'attribute' => 'rate',
                        //'mask' => '9{1,10}[.9{1,4}]',
                        'options' => [
                            'id' => 'rate', 
                            'placeholder' => 'Rate',
                            'class' => 'form-control number-field',
                            'style' => 'text-align: right; min-width: 90px;'
                        ],
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'rightAlign' => false,
                            'digits' => 4,
                            'allowMinus' => false,
                        ],                                    
                    ]),
                    'asButton' => true,
                ],
                ['content' => 
                    Select2::widget([
                        'model' => $model,
                        'attribute' => 'valuta_id',
                        'data' => $valutaList, 
                        'options' => [
                            'id' => 'agreement-valuta-id', 
                            'class' => 'rate-changer-currency',
                            'placeholder' => '...',
                            'data-rate_input_id' => 'rate',
                        ],                            
                        'pluginOptions' => [
                            'allowClear' => !$model->isAttributeRequired('valuta_id'),
                        ],            
                        'size' => 'control-width-90',
                    ]),
                    'asButton' => true,
                ],
            ],
        ],
        //'labelOptions' => ['class' => 'col-md-3'],
    ])->widget(MaskedInput::class, [
        //'mask' => '9{1,10}[.9{1,2}]',
        'options' => [
            'class' => 'form-control number-field',
        ],
        'clientOptions' => [
            'alias' => 'decimal',
            'rightAlign' => false,
            'digits' => 2,
            'allowMinus' => false,
        ],                                    
    ])->label($model->getAttributeLabel('summa').' / '.$model->getAttributeLabel('rate').' / '.$model->getAttributeLabel('valuta_id'));
    ?>
    
    <?= $form->field($model, 'deferment_payment')->widget(MaskedInput::class, [
        'mask' => '9{1,3}',
        'options' => [
            'class' => 'form-control number-field',
        ],
    ]);
    ?>
    
    <?= Html::beginTag('div', [
        'id' => 'rate-data-container',
    ]);?>        
    
    <?= $form->field($model, 'rate_rate')->widget(MaskedInput::class, [
        //'mask' => '9{1,10}[.9{1,2}]',
        'options' => [
            'class' => 'form-control number-field',
        ],
        'clientOptions' => [
            'alias' => 'decimal',
            'rightAlign' => false,
            'digits' => 2,
            'allowMinus' => false,
        ],                                    
    ]);
    ?>
    
    <?= $form->field($model, 'rate_summa')->widget(MaskedInput::class, [
        //'mask' => '9{1,10}[.9{1,2}]',
        'options' => [
            'class' => 'form-control number-field',
        ],
        'clientOptions' => [
            'alias' => 'decimal',
            'rightAlign' => false,
            'digits' => 2,
            'allowMinus' => false,
        ],                                    
    ]);
    ?>

    <?= $form->field($model, 'rate_from_date', [
                //'template' => '{label} <div class="input-group col-md-9">{input}{error}{hint}</div>',
            ])->widget(DatePicker::class, [
            'type' => DatePicker::TYPE_RANGE,
            'attribute' => 'rate_from_date',
            'attribute2' => 'rate_till_date',
            'options' => [
                'placeholder' => Yii::t('common', 'Start date'),
            ],
            'options2' => [
                'placeholder' => Yii::t('common', 'End date'),
            ],
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
        ])->label(Yii::t('client', 'Interest period')); 
     ?>
    
    <?= Html::endTag('div');?>        
        
    <?php
    if(!empty($filesModel)){
        $preview = $previewConfig = [];
        foreach ($filesModel as $key => $file) {
            if(empty($file->id)){
                continue;
            }
            $preview[] = $file->uploadedFileUrl;
            $ext = pathinfo($file->filename, PATHINFO_EXTENSION);
            switch ($ext) {
                default:
                case 'pdf':
                    $type = 'pdf';
                    break;
                case 'doc':
                case 'docx':
                case 'edoc':
                case 'bdoc':
                case 'xls':
                case 'xlsx':
                case 'asice':
                case 'sce':
                    $type = 'gdocs';
                    break;
                case 'zip':
                    $type = 'object';
                    break;
            }
            $previewConfig[] = [
                'type' => $type,
                'size' => $file->filesize, 
                'caption' => $file->filename, 
                'url' => Url::to(['/agreement/attachment-delete', 'deleted_id' => $file->id]), 
                'key' => 101 + $key,
                'downloadUrl' => $file->uploadedFileUrl,
            ];
        }
    
        echo $form->field($filesModel[0], 'file[]')->widget(FileInput::class, [
            'language' =>  strtolower(substr(Yii::$app->language, 0, 2)),
            'sortThumbs' => true,
            'options' => [
                'id' => 'agreement-files',
                'multiple' => true,
            ],
            'pluginOptions' => [
                'allowedFileExtensions' => ['png', 'jpg', 'jpeg', 'pdf', 'doc', 'docx', 'edoc', 'bdoc', 'asice', 'xls', 'xlsx', 'sce', 'zip'],
                'uploadAsync' => false,
                'maxFileSize' => 20000,
                'showRemove' => false,
                'showUpload' => false,
                'overwriteInitial' => false,
                'initialPreview' => $preview,
                'initialPreviewAsData' => true,
                'initialPreviewFileType' => 'pdf',
                'initialPreviewConfig' => $previewConfig,
                'initialPreviewShowDelete' => true,            
                
                'preferIconicPreview' => true, // this will force thumbnails to display icons for following file extensions
                'previewFileIcon' => Icon::show('file'),
                'allowedPreviewTypes' => null, // set to empty, null or false to disable preview for all types                
                'previewFileIconSettings' => [ // configure your icon file extensions
                    'pdf' => Icon::show('file-pdf', ['class' => 'text-danger']),
                    'doc' => Icon::show('file-word', ['class' => 'text-primary']),
                    'docx' => Icon::show('file-word', ['class' => 'text-primary']),
                    'edoc' => Icon::show('file-word', ['class' => 'text-primary']),
                    'bdoc' => Icon::show('file-word', ['class' => 'text-primary']),
                    'xls' => Icon::show('file-excel', ['class' => 'text-success']),
                    'xlsx' => Icon::show('file-excel', ['class' => 'text-success']),
                    'zip' => Icon::show('file-archive', ['class' => 'text-muted']),
                ],
            ]
        ])->label(Yii::t('files', 'Attachment'))->hint('Allowed file extensions: .png .jpg .jpeg .pdf .doc .docx .edoc .bdoc .xls .xlsx .asice .sce .zip'); 
    }
    ?>
    
    <?= $form->field($model, 'write_activity')->textInput() ?>
    
    <?= $form->field($model, 'subject_contract')->textInput() ?>
    
    <?= $form->field($model, 'prepayment_proc')->widget(MaskedInput::class, [
        'options' => [
            'class' => 'form-control number-field',
        ],
        'clientOptions' => [
            'alias' => 'decimal',
            'rightAlign' => false,
            'digits' => 2,
            'allowMinus' => false,
        ],                                    
    ]);
    ?>

    <?= $form->field($model, 'prepayment_sum', [
        'addon' => [
            'append' => [
                ['content' => 
                    Select2::widget([
                        'model' => $model,
                        'attribute' => 'prepayment_valuta_id',
                        'data' => $valutaList, 
                        'options' => [
                            'id' => 'prepayment-valuta-id', 
                            'class' => 'rate-changer-currency',
                            'placeholder' => '...',
                        ],                            
                        'pluginOptions' => [
                            'allowClear' => !$model->isAttributeRequired('prepayment_valuta_id'),
                        ],            
                        'size' => 'control-width-90',
                    ]),
                    'asButton' => true,
                ],
            ],
        ],
    ])->widget(MaskedInput::class, [
        'options' => [
            'class' => 'form-control number-field',
        ],
        'clientOptions' => [
            'alias' => 'decimal',
            'rightAlign' => false,
            'digits' => 2,
            'allowMinus' => false,
        ],                                    
    ])->label($model->getAttributeLabel('prepayment_sum').' / '.$model->getAttributeLabel('prepayment_valuta_id'));
    ?>
    
    <?= $form->field($model, 'prepayment_term')->widget(MaskedInput::class, [
        'mask' => '9{1,3}',
        'options' => [
            'class' => 'form-control number-field',
        ],
    ]);
    ?>
    
    <?php
        $jsonParse = [];
        if (!empty($model->id)) {
            $jsonParse = isset($model->consideration) ? json_decode($model->consideration, true) : [];
        }
        
        echo $form->field($model, "consideration")->widget(MultipleInput::className(), [
            'data'              => $jsonParse,
            'max'               => 10,
            'min'               => 1, // should be at least 2 rows
            'allowEmptyList'    => false,
            'enableGuessTitle'  => false,
            'addButtonPosition' => MultipleInput::POS_ROW, // show add button in the header
        ]);
    ?>
    
    <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>

    <?php if(!empty($model->id) && !empty($isAdmin)){
        echo $form->field($model, 'deleted')->widget(SwitchInput::class, [
            'pluginOptions' => [
                'onText' => Yii::t('common', 'Yes'),
                'offText' => Yii::t('common', 'No'),
                'handleWidth' => (!$isModal ? 'auto' : '45')
            ],
        ]);
    } ?>

    <div class="form-group <?php if($isModal) : echo 'modal-button-group'; endif; ?>">
        <div class="col-md-offset-2 col-md-10" style="text-align: right;">
            <?= $model->SubmitButton; ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <?php if($isModal) : Pjax::end(); endif; ?>
</div>

<?php
$this->registerJs(
    "var form = $('#agreement-form');
    form.find('#rate').change(function(){
        form.yiiActiveForm('validateAttribute', 'agreement-summa');                    
    });",
    \yii\web\View::POS_END
);
?>