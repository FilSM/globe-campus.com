<?php
use kartik\helpers\Html;

use common\components\FSMHelper;
use common\models\client\Agreement;
use common\models\client\ClientContact;
?>

<body>
    <table border=0 cellspacing=0 cellpadding=0 width=720>
        <tbody>
            <tr>
                <td colspan=2 valign=top>
                    <p>
                        <b>
                            <?= strtoupper(Agreement::getAgreementTypeList()[$model->agreement_type]).' CONTRACT '.strtoupper($model->number);?> 
                        </b>
                    </p>
                    
                    <p>
                        <?= date('F jS, Y', strtotime($model->signing_date));?>
                    </p>
                </td>
                
                <td valign=top align=right>
                    <?= (!empty($logoPath) ? Html::img($logoPath, ['style' => 'max-width: 200px; max-height: 100px;']) :'');?>
                </td>
            </tr>

            <tr>
                <td colspan=3></td>
            </tr>

            <tr>
                <td width=160 valign=top>
                    <p>
                        <b>
                            <?= $firstClientRoleModel->name ?? ''; ?>:
                        </b>
                    </p>
                </td>
                <td width=559 colspan=2 valign=top>
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=160 valign=top>
                    <p>
                        Company name:
                    </p>
                </td>
                <td width=559 colspan=2 valign=top>
                    <p>
                        <?= $firstClientModel->name; ?>
                    </p>
                </td>
            </tr>

            <tr>
                <td width=160 valign=top>
                    <p>
                        Registration number:
                    </p>
                </td>
                <td width=559 colspan=2 valign=top>
                    <p>
                        <?= $firstClientModel->reg_number ?? ''; ?>
                    </p>
                </td>
            </tr>

            <tr>
                <td width=160 valign=top>
                    <p>
                        Registered address:
                    </p>
                </td>
                <td width=559 colspan=2 valign=top>
                    <p>
                        <?= $firstClientModel->fullLegalAddress; ?>
                    </p>
                </td>
            </tr>

            <?php foreach ($firstClientPersonModel as $key => $person): ?>
            <tr>
                <td width=160 valign=top>
                    <p>
                        <?= ($key == 0 ? 'Represented by:' : '')?>
                    </p>
                </td>
                <td width=275 valign=top>
                    <p>
                        <?= $person->clientContact->position->name ?? ''; ?>
                    </p>
                </td>
                <td width=285 valign=top>
                    <p>
                        <?= $person->clientContact->first_name.' '.$person->clientContact->last_name; ?>
                    </p>
                </td>
            </tr>

            <tr>
                <td width=160 valign=top>
                    <p>
                        &nbsp;
                    </p>
                </td>
                <td width=275 valign=top>
                    <p>
                        Acting on the basis of:
                    </p>
                </td>
                <td width=285 valign=top>
                    <p>
                        <?= (isset($person->clientContact->acting_on_basis) ? $person->clientContact->actingTypeList[$person->clientContact->acting_on_basis] : null)?>
                        <?php if($person->clientContact->acting_on_basis == ClientContact::ACTING_TYPE_ATTORNEY): ?>
                            dated 
                            <?= !empty($person->clientContact->term_from) ? date('d.m.Y', strtotime($person->clientContact->term_from)) : ''; ?>
                        <?php endif;?>
                    </p>
                </td>
            </tr>
            <?php endforeach;?>

            <tr>
                <td width=160 valign=top>
                    <p>
                        from one side, and
                    </p>
                </td>
                <td width=274 valign=top>
                    <p>
                        &nbsp;
                    </p>
                </td>
                <td width=285 valign=top>
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

        </tbody>
    </table>

    <table  border=0 cellspacing=0 cellpadding=0>
        <tbody>
            <tr>
                <td width=160 valign=top>
                    <p>
                        <b>
                            <?= $secondClientRoleModel->name ?? ''; ?>:
                        </b>
                    </p>
                </td>
                <td width=559 colspan=2 valign=top>
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=160 valign=top>
                    <p>
                        Company name:
                    </p>
                </td>
                <td width=559 colspan=2 valign=top>
                    <p>
                        <?= $secondClientModel->name; ?>
                    </p>
                </td>
            </tr>

            <tr>
                <td width=160 valign=top>
                    <p>
                        Registration number:
                    </p>
                </td>
                <td width=559 colspan=2 valign=top>
                    <p>
                        <?= $secondClientModel->reg_number ?? ''; ?>
                    </p>
                </td>
            </tr>

            <tr>
                <td width=160 valign=top>
                    <p>
                        Registered address:
                    </p>
                </td>
                <td width=559 colspan=2 valign=top>
                    <p>
                        <?= $secondClientModel->fullLegalAddress; ?>
                    </p>
                </td>
            </tr>

            <?php foreach ($secondClientPersonModel as $key => $person): ?>
            <tr>
                <td width=160 valign=top>
                    <p>
                        <?= ($key == 0 ? 'Represented by:' : '')?>
                    </p>
                </td>
                <td width=274 valign=top>
                    <p>
                        <?= $person->clientContact->position->name ?? ''; ?>
                    </p>
                </td>
                <td width=285 valign=top>
                    <p>
                        <?= $person->clientContact->first_name.' '.$person->clientContact->last_name; ?>
                    </p>
                </td>
            </tr>

            <tr>
                <td width=160 valign=top>
                    <p>
                        &nbsp;
                    </p>
                </td>
                <td width=274 valign=top>
                    <p>
                        Acting on the basis of:
                    </p>
                </td>
                <td width=285 valign=top>
                    <p>
                        <?= (isset($person->clientContact->acting_on_basis) ? $person->clientContact->actingTypeList[$person->clientContact->acting_on_basis] : null)?>
                        <?php if($person->clientContact->acting_on_basis == ClientContact::ACTING_TYPE_ATTORNEY): ?>
                            dated 
                            <?= !empty($person->clientContact->term_from) ? date('d.m.Y', strtotime($person->clientContact->term_from)) : ''; ?>
                        <?php endif;?>
                    </p>
                </td>
            </tr>
            <?php endforeach;?>
            
            <tr>
                <td width=434 colspan=2 valign=top>
                    <p>
                        from the other side, 
                    </p>
                </td>
                <td width=285 valign=top>
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

        </tbody>
    </table>

    <table border=0 cellspacing=0 cellpadding=0>
        <tbody>

            <tr>
                <td width=720 colspan=4 valign=top>
                    <p>
                        <?= $firstClientRoleModel->name ?? ''; ?> and Client hereinafter also separately referred to as the Party, and jointly as the Parties,
                    </p>
                    <br/>
                    <p>
                        Considering that:
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 colspan=4 valign=top>
                    <p>
                        -  the Client <?= $model->write_activity ?? ''; ?> and is willing to receive <?= $model->subject_contract ?? ''; ?> from the <?= $firstClientRoleModel->name ?? ''; ?>; 
                    </p>
                </td>
            </tr>

            <?php if(!empty($model->consideration) && ($model->consideration != '[""]')) :?>
            <tr>
                <td width=720 colspan=4 valign=top>
                    <?php 
                        $jsonParseArr = json_decode($model->consideration, true);
                        foreach ($jsonParseArr as $jsonParse):
                            echo '<p>- '.$jsonParse.'</p>';
                        endforeach;
                    ?>
                </td>
            </tr>
            <?php endif;?>

            <tr>
                <td width=720 colspan=4 valign=top>
                    <p>
                        -  the <?= $firstClientRoleModel->name ?? ''; ?> has the necessary knowledge, resources, contacts and know-how to perform the assignment of the Client,
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 colspan=4 valign=top>
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 colspan=4 valign=top>
                    <p>
                        the Parties, based on mutual interest and benefit, agree to enter into the following contract (hereinafter - <b>Contract):</b>
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 colspan=4 valign=top align=center>
                    <p>
                        <b>
                            SPECIAL CONDITIONS
                        </b>
                    </p>
                </td>
            </tr>

            <tr>
                <td width=37 valign=top>
                    <p>
                        1.
                    </p>
                </td>
                <td width=189 valign=top>
                    <p>
                        Subject of the Contract:
                    </p>
                </td>
                <td width=493 colspan=2 valign=top>
                    <p>
                        <?= ucfirst($model->subject_contract ?? ''); ?>
                    </p>
                </td>
            </tr>

            <tr>
                <td width=37 valign=top>
                    <p>
                        2.
                    </p>
                </td>
                <td width=189 valign=top>
                    <p>
                        Term of the Contract:
                    </p>
                </td>
                <td width=493 colspan=2 valign=top>
                    <p>
                        <?= !empty($model->due_date) ? date('d.m.Y', strtotime($model->due_date)) : 'Until the obligations set out have been fully fulfilled'?>
                    </p>
                </td>
            </tr>

            <tr>
                <td width=37 valign=top>
                    <p>
                        3.
                    </p>
                </td>
                <td width=189 valign=top>
                    <p>
                        Contract sum:
                    </p>
                </td>
                <td width=493 colspan=2 valign=top>
                    <p>
                        <?= $model->summa.' '.$model->valuta->name.' ('.FSMHelper::getSummaToWords($model->summa, '', $model->valuta->name).')';?>
                    </p>
                </td>
            </tr>

            <tr>
                <td width=37 valign=top>
                    <p>
                        4.
                    </p>
                </td>
                <td width=189 valign=top>
                    <p>
                        Payment terms:
                    </p>
                </td>
                <?php if(!empty($model->prepayment_proc) || !empty($model->prepayment_sum)){
                    
                }?>
                <td width=170 valign=top>
                    <p>
                        <?php if(!empty($model->prepayment_proc) || !empty($model->prepayment_sum)){
                            echo 'Prepayment';
                        } else {
                            echo 'Payment term';
                        }?>
                    </p>
                </td>
                <td width=323 valign=top>
                    <p>
                        <?php if(!empty($model->prepayment_proc) || !empty($model->prepayment_sum)){
                            if(!empty($model->prepayment_proc)){
                                echo $model->prepayment_proc.' %';
                            }else{
                                echo $model->prepayment_sum.' '.$model->prepaymentValuta->name.' ('.FSMHelper::getSummaToWords($model->prepayment_sum, '', $model->prepaymentValuta->name).')';
                            }
                        }else{
                            echo !empty($model->deferment_payment) ?  $model->deferment_payment.' calendar day(s)': '';
                        }
                        ?>
                    </p>
                </td>
            </tr>

            <?php if(!empty($model->prepayment_term)):?>
            <tr>
                <td width=37 valign=top>
                    <p>
                        &nbsp;
                    </p>
                </td>
                <td width=189 valign=top>
                    <p>
                        &nbsp;
                    </p>
                </td>
                <td width=170 valign=top>
                    <p>
                        Prepayment term
                    </p>
                </td>
                <td width=323 valign=top>
                    <p>
                        <?= !empty($model->prepayment_term) ?  $model->prepayment_term.' calendar day(s)': ''; ?>
                    </p>
                </td>
            </tr>
            <?php endif;?>

            <?php if(!empty($model->prepayment_proc) || !empty($model->prepayment_sum)):?>
            <tr>
                <td width=37 valign=top>
                    <p>
                        &nbsp;
                    </p>
                </td>
                <td width=189 valign=top>
                    <p>
                        &nbsp;
                    </p>
                </td>
                <td width=170 valign=top>
                    <p>
                        Payment term
                    </p>
                </td>
                <td width=323 valign=top>
                    <p>
                        <?= !empty($model->deferment_payment) ?  $model->deferment_payment.' calendar day(s)': ''; ?>
                    </p>
                </td>
            </tr>
            <?php endif;?>

        </tbody>
    </table>

    <p>
        &nbsp;
    </p>

    <table border=0 cellspacing=0 cellpadding=0>
        <tbody>
            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        5. This Contract consists of Special Conditions and General Conditions which both are integral parts of this Contract. By signing this Contract the Parties acknowledge that they have read and understood the Special Conditions and the General Conditions. Parties agree with these Conditions and shall observe them.
                    </p>
                </td>
            </tr>

        </tbody>        
    </table>

    <p>
        &nbsp;
    </p>

    <table  border=0 cellspacing=0 cellpadding=0>
        <tbody>
            <tr>
                <td width=720 valign=top align="center">
                    <p>
                        <b>
                            GENERAL CONDITIONS
                        </b>
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="center">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        <b>
                            1. RIGHTS AND OBLIGATIONS OF THE PARTIES
                        </b>
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        1.1. The <?= $firstClientRoleModel->name ?? ''; ?> has the following obligations:
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify" style="padding-left: 25px;">
                    <p>
                        1.1.1. to provide professional services, in high quality and in accordance to the generally accepted professional standards;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify" style="padding-left: 25px;">
                    <p>
                        1.1.2. to cooperate with persons appointed by the Client in order to ensure performance of the Contract;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify" style="padding-left: 25px;">
                    <p>
                        1.1.3. to inform the Client immediately about any essential information relevant for performance of the Contract.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        1.2. The <?= $firstClientRoleModel->name ?? ''; ?> is entitled to:
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify" style="padding-left: 25px;">
                    <p>
                        1.2.1. request and receive from the Client any information necessary for performance of the obligations prescribed in the Contract (<?= $firstClientRoleModel->name ?? ''; ?> has the right to make requests orally, in writing, or using e-mail - in any of the forms mentioned appropriate for the particular situation);
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify" style="padding-left: 25px;">
                    <p>
                        1.2.2. contract third persons for performance of the Contract, by ensuring that those persons shall comply with confidentiality provisions of the Contract;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify" style="padding-left: 25px;">
                    <p>
                        1.2.3. receive the remuneration pursuant to the provisions of the Contract.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        1.3. The Client has the following obligations:
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify" style="padding-left: 25px;">
                    <p>
                        1.3.1. to cooperate with the <?= $firstClientRoleModel->name ?? ''; ?> and appoint the responsible person, who shall provide to the <?= $firstClientRoleModel->name ?? ''; ?> the requested information, that is necessary for successful performance of the Contract;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify" style="padding-left: 25px;">
                    <p>
                        1.3.2. to inform the <?= $firstClientRoleModel->name ?? ''; ?> about all conditions and circumstances, which might be relevant for performance of the Contract;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify" style="padding-left: 25px;">
                    <p>
                        1.3.3. The information provided by the Client should be true and accurate to the best knowledge of the Client. The Client undertakes to observe the Contract in good faith;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify" style="padding-left: 25px;">
                    <p>
                        1.3.4. to pay to the <?= $firstClientRoleModel->name ?? ''; ?> a remuneration for the provided services as prescribed in Contract.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        1.4. The Client is entitled to:
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify" style="padding-left: 25px;">
                    <p>
                        1.4.1. request from the <?= $firstClientRoleModel->name ?? ''; ?> information about the provisions of the services and its results;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify" style="padding-left: 25px;">
                    <p>
                        1.4.2. order from the <?= $firstClientRoleModel->name ?? ''; ?> provision of new services, the amount, terms and other conditions of which shall be agreed between the Parties separately.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        <b>
                            2. VALIDITY OF THE CONTRACT
                        </b>
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        2.1. The <?= $firstClientRoleModel->name ?? ''; ?> and the Client have the right to unilaterally terminate this Contract in case the other Party materially breaches this Contract and does not remedy the breach within 10 (ten) days after the delivery of the respective notification.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        2.2. In case of the termination of the Contract under clause 2.1 above, the <?= $firstClientRoleModel->name ?? ''; ?> is entitled to receive the remuneration for the work already done and none of the already paid sums are subject to reimbursement or recovery by the Client.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        <b>
                            3. INVOICING AND PAYMENTS
                        </b>
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        3.1. The payments are deemed to be paid once received in the bank account of the <?= $firstClientRoleModel->name ?? ''; ?>.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        3.2. All the prices in the Contract are indicated without value added tax and shall be subject to applicable taxes upon issuing of the invoice. The <?= $firstClientRoleModel->name ?? ''; ?> shall receive in its bank account the whole payable sum without application of any withholding taxes, deductions and bank commissions.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        3.3. All the payments by the Client shall be transferred to the <?= $firstClientRoleModel->name ?? ''; ?> bank account in accordance with the invoices issued under the Contract and are non-refundable.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        3.4. In case of late payments the <?= $firstClientRoleModel->name ?? ''; ?> is entitled to receive and Client are obliged to pay a penalty in amount of 0,05% per day from the amount of late payments. With this requested penalty payment, the Client is not discharged from the obligation to pay residues under the Contract and to cover all direct losses of the <?= $firstClientRoleModel->name ?? ''; ?>.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        3.5. Every payment payable by the Client under this Contract including any payment of consideration shall be made in full without any set-off or counterclaim howsoever arising.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        <b>
                            4. CONFIDENTIALITY
                        </b>
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        4.1. Subject to clause 4.2 of the Contract, for the duration of the Contract and for 5 (five) years after its expiry, the Parties of the Contract:
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify" style="padding-left: 25px;">
                    <p>
                        4.1.1. shall not make use of or disclose or divulge to any third party the Confidential Information;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify" style="padding-left: 25px;">
                    <p>
                        4.1.2. shall procure that their employees or agents do not make use of or disclose or divulge to any third party the Confidential Information.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        4.2. Clause 4.1 of the Contract does not apply to a Party who discloses or divulges any information:
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify" style="padding-left: 25px;">
                    <p>
                        4.2.1. which has become accessible to the public without the Party breaching this Contract;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify" style="padding-left: 25px;">
                    <p>
                        4.2.2. required to do so by law or governmental or other relevant authority;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify" style="padding-left: 25px;">
                    <p>
                        4.2.3. which has become known to the Party from a source other than the disclosing Party, without breaching the Contract and provided that such information was not disclosed to the Party as a result of any breach of Contracts or other covenants by third party.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        4.3. If a Party becomes required, in circumstances contemplated by clause 4.2.2, to disclose any information, such Party shall (save to the extent prohibited by law) give to the other Party such notice as is practical in the circumstances of such disclosure and shall cooperate with the other Party, having due regard to the other Party's views, and take such steps as the other Party may reasonably require in order to enable it to mitigate the effects of, or avoid the requirements for, any such disclosure.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        4.4. Confidential information means any information provided by the Parties to each other which is not in the public domain and which, by its nature and content, should be treated as confidential under a reasonable business judgment. Confidential information, without any exceptions, includes information about the Contract and its terms.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        <b>
                            5. RESPONSIBILITY
                        </b>
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        5.1. The Parties shall be liable only for direct loses and harm caused to other as a result of guilty action.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        5.2. The <?= $firstClientRoleModel->name ?? ''; ?> shall strive to provide to the Client the services with maximum efforts to ensure beneficial result. However, Contract provisions or written or oral announcement of the <?= $firstClientRoleModel->name ?? ''; ?> shall not be interpreted as a promise or warranty to achieve certain result for the Client but shall be regarded as a professional insight of the <?= $firstClientRoleModel->name ?? ''; ?> only, possible to be reached under particular circumstances.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        5.3. The Parties shall be released from the liability for partial or full non-performance of the Contract, if it is caused as a result of <i>force majeure</i>. The <i>force majeure</i> shall be every event, which does not depend on the Parties, which the Parties were not able to prevent with reasonable efforts, including, but not limited, war, mobilization, embargo, civil riots, fire, floods, earthquake, as well as unexpected and unplanned amendments in state or municipality legal acts. 
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        5.4. The Party, which refer to the <i>force majeure</i> circumstances in case of non-performance of its obligations shall immediately, when it is possible, but not later than within 5 (five) days, inform the other Party and justify its nonperformance of the obligations. All the Contract terms are suspended and shall continue their running only after termination of the <i>force majeure</i> circumstances. The Party who has not informed the other Party about the <i>force majeure</i> circumstances in compliance with this clause shall not be entitled to refer to the <i>force majeure</i> circumstances as a cause for nonperformance of its obligations. In case of <i>force majeure</i> circumstances, the Parties may agree to prolong the Contract terms or terminate the Contract. 
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        <b>
                            6. APPLICABLE LAW AND DISPUTE RESOLUTION
                        </b>
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        6.1. All disputes and divergences which arise during the Contract between the Parties shall be resolved through mutual negotiations, but, if the settlement is not reached within 1 (one) month, the dispute shall be finally resolved before the Arbitration Court of the Latvian Chamber of Commerce and Industry in Riga, Latvia by one arbitrator in accordance with its procedural rules in English language.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        6.2. This Contract shall be construed, interpreted and applied in compliance with the laws of the Republic of Latvia.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        <b>
                            7. MISCELLANEOUS PROVISIONS
                        </b>
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        7.1. All amendments, additions, corrections to the Contract shall be prepared in writing upon mutual agreement between the Parties. They shall be added to the Contract and shall become integral part of Contract.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        7.2. Every payment payable by the Client under this Contract including any payment of consideration shall be made in full without any set-off or counterclaim howsoever arising and shall be free and clear of, and without deduction of, or withholding for or on account of, any amount which is due or payable by the <?= $firstClientRoleModel->name ?? ''; ?>.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        7.3. The Parties shall after the countersigning of this Contract execute all such deeds and documents and do all such things as the other Party may reasonably require for perfecting the transactions intended to be effected under or pursuant to this Contract and for giving the other Party the full benefit of the provisions of this Contract.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        7.4. Unless agreed otherwise between the Parties, this Contract shall be binding to successors in title.
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        &nbsp;
                    </p>
                </td>
            </tr>

            <tr>
                <td width=720 valign=top align="justify">
                    <p>
                        7.5. The Contract is prepared in the English language on <?= $pageTotal ?? 'x'?> pages. Every countersigned copy of this Contract shall deemed valid and executable.
                    </p>
                </td>
            </tr>

        </tbody>
    </table>
    
    <p>
        &nbsp;
    </p>

    <table border=0 cellspacing=0 cellpadding=0>
        <tbody>
            <tr>
                <td width=350 align="center">
                    <?php foreach ($firstClientPersonModel as $key => $person): ?>
                        <hr>
                        <p>
                            <b>
                                <?= $person->clientContact->first_name.' '.$person->clientContact->last_name; ?>
                            </b>
                        </p>
                        <p>
                            <?= $person->clientContact->position->name ?? ''; ?>
                        </p>
                        <?php if(($key == 0) && (count($firstClientPersonModel) > 1)) :?>
                            <br/>
                        <?php endif;?>
                    <?php endforeach;?>
                </td>
                <td width=20>
                    <p>
                        &nbsp;
                    </p>
                </td>
                <td width=350 align="center">
                    <?php foreach ($secondClientPersonModel as $key => $person): ?>
                        <hr>
                        <p>
                            <b>
                                <?= $person->clientContact->first_name.' '.$person->clientContact->last_name; ?>
                            </b>
                        </p>
                        <p>
                            <?= $person->clientContact->position->name ?? ''; ?>
                        </p>
                        <?php if(($key == 0) && (count($secondClientPersonModel) > 1)) :?>
                            <br/>
                        <?php endif;?>
                    <?php endforeach;?>
                </td>
            </tr>
        </tbody>
    </table>
</body>