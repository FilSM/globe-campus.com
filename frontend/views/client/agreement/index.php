<?php
namespace common\models;

use Yii;
//use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
//use yii\grid\GridView;

use kartik\helpers\Html;
use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;
use kartik\export\ExportMenu;
use kartik\icons\Icon;

use common\components\FSMAccessHelper;
use common\models\client\Agreement;
use common\components\FSMExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel common\models\client\search\AgreementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

Icon::map($this);
$this->title = $searchModel->modelTitle(2);
$this->params['breadcrumbs'][] = $this->title;
$forDetail = !empty($forDetail);
?>
<div class="agreement-index">

    <?php
        $columns = [
            //['class' => '\kartik\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'width' => '75px',
                'hAlign' => 'center',
            ],
            [
                'attribute' => 'number',
                'contentOptions' => [
                    'style'=>'max-width:100px; min-height:75px; word-wrap: break-word;'
                ],
                'value' => function ($model) {
                    return 
                        '<div style="overflow-x: auto;">'.
                        (!empty($model->number) ? (FSMAccessHelper::can('viewAgreement', $model) ?
                            Html::a($model->number, ['/agreement/view', 'id' => $model->id], ['target' => '_blank', 'data-pjax' => 0]) :
                            $model->number
                        )
                        : null).
                        '</div>';
                },                        
                'format' => 'raw',
            ],
            [
                'class' => '\kartik\grid\ExpandRowColumn',
                'expandIcon' => Icon::show('chevron-circle-right'),
                'collapseIcon' => Icon::show('chevron-circle-down'),
                'value' => function ($model, $key, $index, $column) {
                    return GridView::ROW_COLLAPSED;
                },
                'detailUrl' => Url::to(['/bill/bill-detail']),
                'visible' => FSMAccessHelper::checkRoute('/bill/*') && !$forDetail,
            ],
            [
                'attribute' => 'agreement_type',
                'value' => function ($model) {
                    return isset($model->agreement_type) ? $model->agreementTypeList[$model->agreement_type] : null;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $searchModel->agreementTypeList,
                'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                'filterInputOptions' => ['placeholder' => '...'],
                'format'=>'raw',
            ],
            [
                'attribute'=>'project_name',
                'contentOptions' => [
                    'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                ],
                
                'value' => function ($model) {
                    $currentAbonentAgreement = $model->currentAbonentAgreement;
                    $result = [];
                    foreach ($currentAbonentAgreement->abonentAgreementProjects as $abonentAgreementProject) {
                        $result[] = (FSMAccessHelper::can('viewProject', $abonentAgreementProject->project) ?
                            Html::a($abonentAgreementProject->projectName, ['/project/view', 'id' => $abonentAgreementProject->project_id], ['target' => '_blank', 'data-pjax' => 0]) :
                            $abonentAgreementProject->projectName 
                        );
                    }
                    $content = implode('<br/>', $result);
                    return 
                        '<div style="overflow-x: auto;">'.$content.'</div>';                    
                },                         
                'format'=>'raw',
                'visible' => FSMAccessHelper::can('viewProject') && !$forDetail,
            ],            
            [
                'attribute' => 'status',
                'hAlign' => 'center',
                'headerOptions' => ['class'=>'td-mw-100'],
                'value' => function ($model) {
                    return isset($model->status) ? Html::badge($model->agreementStatusList[$model->status], ['class' => $model->statusBackgroundColor.' status-badge']) : null;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $searchModel->agreementStatusList,
                'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                'filterInputOptions' => ['placeholder' => '...'],
                'format'=>'raw',
            ],
            [
                'attribute' => 'grid_progress',
                'header' => Yii::t('common', 'Progress'),
                'headerOptions' => ['class'=>'td-mw-100'],
                'contentOptions' => [
                    'class' => 'td-slider',
                ],
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'mergeHeader' => true,
                'value' => function ($model) {
                    return $model->getProgressButtons();
                },
                'format' => 'raw',
                'visible' => FSMAccessHelper::can('changeAgreementStatus'),
                'hiddenFromExport' => true,
            ],
            [
                'attribute'=>'first_client_id',
                'headerOptions' => ['class'=>'td-mw-200'],
                'contentOptions' => [
                    'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                ],
                'value' => function ($model) {
                    return 
                        '<div style="overflow-x: auto;">'.
                        (!empty($model->first_client_id) ? 
                        (!empty($model->first_client_role_id) ? $model->firstClientRole->name.':<br/>' : '') . 
                            (FSMAccessHelper::can('viewClient', $model->firstClient) ?
                                Html::a($model->first_client_name, ['/client/view', 'id' => $model->first_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                $model->first_client_name
                            )
                        : null).
                        '</div>';
                },             
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $clientList,
                'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                'filterInputOptions' => [
                    'id' => 'agreementsearch-first_client_name',
                    'placeholder' => '...',
                ],                        
                'format'=>'raw',
                //'hiddenFromExport' => true,
            ],            
            [
                'attribute'=>'second_client_id',
                'headerOptions' => ['class'=>'td-mw-200'],
                'contentOptions' => [
                    'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                ],
                'value' => function ($model) {
                    return 
                        '<div style="overflow-x: auto;">'.
                        (!empty($model->second_client_id) ?  
                        (!empty($model->second_client_role_id) ? $model->secondClientRole->name.':<br/>' : '') . 
                            (FSMAccessHelper::can('viewClient', $model->secondClient) ?
                                Html::a($model->second_client_name, ['/client/view', 'id' => $model->second_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                $model->second_client_name
                            )
                        : null).
                        '</div>';
                },                         
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $clientList,
                'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                'filterInputOptions' => [
                    'id' => 'agreementsearch-second_client_name',
                    'placeholder' => '...',
                ],                        
                'format'=>'raw',
                //'hiddenFromExport' => true,
            ],            
            [
                'attribute'=>'third_client_name',
                'contentOptions' => [
                    'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                ],
                'value' => function ($model) {
                    return 
                        '<div style="overflow-x: auto;">'.
                        (!empty($model->third_client_id) ?   
                        (!empty($model->third_client_role_id) ? $model->thirdClientRole->name.':<br/>' : '') . 
                            (FSMAccessHelper::can('viewClient', $model->thirdClient) ?
                                Html::a($model->third_client_name, ['/client/view', 'id' => $model->third_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                $model->third_client_name
                            )
                        : null).
                        '</div>';
                },                         
                'format'=>'raw',
                //'hiddenFromExport' => true,
            ],            
            [
                'attribute' => 'signing_date',
                'headerOptions' => [
                    'class' => 'td-mw-75',
                ],
                'value' => function ($model) {
                    return !empty($model->signing_date) ? date('d-M-Y', strtotime($model->signing_date)) : null;
                },                 
            ],                         
            [
                'attribute' => 'due_date',
                'headerOptions' => [
                    'class' => 'td-mw-75',
                ],
                'value' => function ($model) {
                    return !empty($model->due_date) ? date('d-M-Y', strtotime($model->due_date)) : null;
                },                 
            ],                                                 
            [
                'attribute' => 'summa',
                'hAlign' => 'right',
                'headerOptions' => [
                    'class' => 'td-mw-75',
                    //'style' => 'text-align: left;',
                ],
            ],                 
                        /*
            [
                'attribute' => 'rate',
                'hAlign' => 'right',
                'headerOptions' => [
                    'class' => 'td-mw-75',
                    //'style' => 'text-align: left;',
                ],
            ],      
                         * 
                         */
            [
                'attribute' => 'conclusion',
                'headerOptions' => ['class'=>'td-mw-100'],                
                'value' => function ($model) {
                    return isset($model->status) ? $model->agreementConclusionList[$model->conclusion] : null;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $searchModel->agreementConclusionList,
                'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                'filterInputOptions' => ['placeholder' => '...'],
                'visible' => $isAdmin,
            ],
            [
                'attribute' => 'transfer_status',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class'=>'td-mw-100'],
                'value' => function ($model) {
                    return $model->transferStatusHTML;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $searchModel->agreementTransferStatusList,
                'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                'filterInputOptions' => [
                    'placeholder' => '...',
                    //'multiple' => true,
                ],
                'format'=>'raw',
                'visible' => !empty(Yii::$app->params['ENABLE_BILL_TRANSFER']),
            ],                          
            [
                'attribute' => "deleted",                
                'class' => '\kartik\grid\BooleanColumn',
                'trueLabel' => 'Yes', 
                'falseLabel' => 'No',
                'headerOptions' => [
                    'class' => 'td-mw-100',
                ],
                'visible' => $isAdmin && !$forDetail,
                'hiddenFromExport' => true,
            ],

            [
                'class' => '\common\components\FSMActionColumn',
                'headerOptions' => ['class'=>'td-mw-125'],
                'dropdown' => true,
                'dropdownDefaultBtn' => 'attachment',
                'checkPermission' => true,
                'template' => !empty(Yii::$app->params['ENABLE_BILL_TRANSFER']) ?
                    '{attachment} {print} {payment} {send-other-abonent} {receive-other-abonent} {return} {view} {update} {delete}' :
                    '{attachment} {print} {payment} {view} {update} {delete}',
                'buttons' => [
                    'attachment' => function (array $params) { 
                        return Agreement::getButtonAttachment($params);
                    },
                    'payment' => function (array $params) { 
                        return Agreement::getButtonPayment($params);
                    },
                    'send-other-abonent' => function (array $params) { 
                        return Agreement::getButtonSendOtherAbonent($params);
                    },
                    'receive-other-abonent' => function (array $params) { 
                        return Agreement::getButtonReceiveOtherAbonent($params);
                    },
                    'return' => function (array $params) { 
                        return Agreement::getButtonReturn($params);
                    },
                    'print' => function (array $params) { 
                        return Agreement::getButtonPdf($params);
                    },
                ],
                'visible' => !$forDetail,
            ],                        
        ];
    ?>
        
    <?php
        $panelBefore = '';
        if(!$forDetail): 
            $panelBefore = '<div class="btn-group">';
            if (FSMAccessHelper::can('createAgreement')):
                $panelBefore .= Html::a(Html::icon('plus') . '&nbsp;' . $searchModel->modelTitle(), ['create'], ['class' => 'btn btn-success', 'data-pjax' => 0]);
            endif;
            $panelBefore .= '</div>';
        endif;
    ?>
    
    <?= DynaGrid::widget([
        'columns' => $columns,
        'storage' => DynaGrid::TYPE_DB,
        //'theme' => 'simple-striped',
        'allowThemeSetting' => false,
        'allowFilterSetting' => false,
        'allowSortSetting' => false,
        'gridOptions' => [
            //'panel' => false,
            'panel' => [
                'heading' => '<h3 class="panel-title">' . Html::encode($this->title) . '</h3>',
                'type' => 'default',
                'before' => !$forDetail ? $panelBefore : null,
                'after' => false
            ],
/*
            'panelTemplate' => 
                '<div class="panel {type}">
                    {panelHeading}
                    {panelBefore}
                    {items}
                    {panelAfter}
                    {panelFooter}
                </div>',
* 
*/
            'id' => 'grid-view',
            'tableOptions' => [
                'id' => 'agreement-list',
            ],
            'responsive' => false,
            'condensed' => true,
            //'striped' => false,
            'hover' => true,
            'dataProvider' => $dataProvider,
            'filterModel' => !$forDetail ? $searchModel : null,
            'pjax' => true,
            //'export' => false,
            'floatHeader' => true,
            'toolbar' =>  [
                //['content' =>
                //    FSMAccessHelper::can('createAgreement') ?
                //        Html::a(Html::icon('plus').'&nbsp;'.$searchModel->modelTitle(), ['create'], ['class' => 'btn btn-success']) : null
                //],
                //['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                ['content' => 
                    FSMExportMenu::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => $columns,
                        'showColumnSelector' => false,
                        'showConfirmAlert' => false,
                        'dropdownOptions' => [
                            'label' => Yii::t('kvgrid', 'Export'),
                            'menuOptions' => [
                                'class' => 'pull-right',
                            ],
                        ],
                        'target' => ExportMenu::TARGET_SELF,
                        'exportConfig' => [
                            ExportMenu::FORMAT_TEXT => false,
                            ExportMenu::FORMAT_HTML => false,
                            ExportMenu::FORMAT_CSV => false,
                            ExportMenu::FORMAT_TEXT => false,
                            ExportMenu::FORMAT_PDF => false,
                            ExportMenu::FORMAT_EXCEL => false,
                            ExportMenu::FORMAT_EXCEL_X => ['label' => Yii::t('kvgrid', 'Excel'), 'alertMsg' => ''],
                        ],
                        'pjaxContainerId' => 'agreement-index',
                        'exportFormView' => '@vendor/kartik-v/yii2-export/views/_form',
                    ]).'{dynagrid}'
                ],
                //'{export}',
            ],
        ],
        'options' => ['id' => 'dynagrid-agreement'], // a unique identifier is important
    ]);
    ?>
</div>