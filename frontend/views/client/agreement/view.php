<?php

use kartik\helpers\Html;

use common\components\FSMAccessHelper;

/* @var $this yii\web\View */
/* @var $model common\models\client\Agreement */

$this->title = $model->modelTitle() .' #'. $model->number;
if(FSMAccessHelper::checkRoute('/agreement/index')){
    $this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-view">

    <?= Html::pageHeader(Html::encode($this->title)); ?>

    <?= $this->render('_view', [
        'model' => $model,
        'paymentModel' => $paymentModel,
        'historyModel' => $historyModel,
        'paymentDataProvider' => $paymentDataProvider,
        'historyDataProvider' => $historyDataProvider,
        'filesModel' => $filesModel,
        'isAdmin' => $isAdmin,
    ]) ?> 

</div>