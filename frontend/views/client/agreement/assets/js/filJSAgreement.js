(function ($) {

    //$(document).ready(function () {

        var form = $('#agreement-form');
        var agreementId = $_GET.id;
        
        var currentType = form.find('[name="Agreement[agreement_type]"]').val();
        switch (currentType) {
            default:
            case 'cooperation':
            case 'cession':
            case 'agency':
            case 'employment':
                form.find('#rate-data-container').hide();
                break;
            case 'loan':
                form.find('#rate-data-container').show();
                break;
        }        
        
        form.find('#agreement-agreement_type button').click(function () {
            var $this = $(this);
            if($this.hasClass( "active" )){
                return false;
            }
            var currentType = $(this).val();
            switch (currentType) {
                case 'cooperation':
                case 'cession':
                case 'agency':
                case 'employment':
                    form.find('#rate-data-container').hide('slow');
                    form.find('#rate-data-container input[type="text"]').val('');
                    break;
                    
                case 'loan':
                    form.find('#rate-data-container').show('slow');
                    break;
            }
        });        
        
        form.find('#rate, #valuta-id').change(function(){
            form.yiiActiveForm('validateAttribute', 'agreement-summa');                    
        });
    
        form.find('#agreement-number').change(function(e) {
            var $this = $(this);
            var value = $this.val();
            var url = appUrl + '/agreement/ajax-validate-unique-number';
            
            $.get(
                url,
                {
                    doc_number: value,
                    id: agreementId 
                },
                function (data) {
                    if(!empty(data.exists)){
                        if(!empty(data.message)){
                            krajeeDialogWarning.alert(data.message);
                        }
                        //alert('No data!');
                        return false;
                    }
                });
            //alert('selected id = ' + id); 
        });         
    //});
    
})(window.jQuery);     