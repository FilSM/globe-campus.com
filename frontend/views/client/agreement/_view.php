<?php
namespace common\models\client;

use Yii;
use yii\helpers\Url;

use kartik\helpers\Html;
use kartik\detail\DetailView;

use common\components\FSMAccessHelper;
use common\components\FSMHelper;
use common\models\client\Agreement;

/* @var $this yii\web\View */
/* @var $model common\models\client\Agreement */

$jsonParseStr = '';
if(!empty($model->consideration)):
    $jsonParseArr = json_decode($model->consideration, true);
    foreach ($jsonParseArr as $jsonParse):
        $jsonParseStr .= $jsonParse.'<br/>';
    endforeach;
endif;

$currentAbonentAgreement = $model->currentAbonentAgreement;
$result = [];
foreach ($currentAbonentAgreement->abonentAgreementProjects as $abonentAgreementProject) {
    $result[] = (FSMAccessHelper::can('viewProject', $abonentAgreementProject->project) ?
        Html::a($abonentAgreementProject->projectName, ['/project/view', 'id' => $abonentAgreementProject->project_id], ['target' => '_blank', 'data-pjax' => 0]) :
        $abonentAgreementProject->projectName 
    );
}
$contentProject = implode('<br/>', $result);
?>

<div class='col-md-12' style="margin-bottom: 5px;">                  
    <div class='col-md-3' style="padding: 0;">
        <?php if(isset($fromClientModel)): ?> 
        <?= $model->getBackButton().'&nbsp;'; ?>
        <?php endif; ?>
        <?php if(FSMAccessHelper::can('updateAgreement', $model)): ?>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id, 'client_id' => (isset($fromClientModel) ? $fromClientModel->id : null)], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>
        <?php if(FSMAccessHelper::can('deleteAgreement', $model)): ?>
        <?= \common\components\FSMBtnDialog::button(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id, 'client_id' => (isset($fromClientModel) ? $fromClientModel->id : null)], [
            'id' => 'btn-dialog-selected',
            'class' => 'btn btn-danger',
        ]); ?> 
        <?php endif; ?>
    </div>

    <div class='col-md-3' style="padding: 0; text-align: right;">
        <?= $model->getProgressButtons('', true);?>
    </div>

    <div class='col-md-6 pr-0' style="padding: 0; text-align: right;">
        <?php 
            $disabled = $oral = false;
            $printModel = $filesModel;
            $printModel = count($printModel) > 0 ? $printModel[0] : null;
            switch ($model->conclusion) {
                case Agreement::AGREEMENT_CONCLUSION_ORAL:
                    $oral = true;
                    break;
                case Agreement::AGREEMENT_CONCLUSION_WRITTEN:
                    $disabled = empty($printModel);
                    break;
            }
            if(!$disabled && !$oral && !empty($filesModel[0]->id)){
                echo FSMHelper::aButton($model->id, [
                    'label' => Yii::t('common', 'Print'),
                    'action' => 'print',
                    'url' => isset($printModel) ? $printModel->fileurl : null,
                    'icon' => 'print',
                    'options' => [
                        'target' => '_blank',
                    ],
                ]);
            }
        ?> 
        <?php 
            $params = [
                'model' => $model, 
                'isBtn' => true,
            ];
            echo $model->getButtonPdf($params);
        ?>  
        <?php
            $params = [
                'url' => Url::to(['send-other-abonent', 'id' => $model->id]), 
                'model' => $model, 
                'isBtn' => true,
            ];
            echo Agreement::getButtonSendOtherAbonent($params); 
        ?>            
        <?php
            $params = [
                'url' => Url::to(['receive-other-abonent', 'id' => $model->id]), 
                'model' => $model, 
                'isBtn' => true,
            ];
            echo Agreement::getButtonReceiveOtherAbonent($params); 
        ?>            
        <?php
            $params = [
                'model' => $model, 
                'isBtn' => true,
            ];
            echo Agreement::getButtonPayment($params); 
        ?>
    </div>    
</div>

<div class='col-md-12'>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',                         
            [
                'attribute' => 'status',
                'value' => isset($model->status) ? $model->statusHTML : null,
                'format'=>'raw',
            ],
            [
                'attribute' => 'agreement_type',
                'value' => isset($model->agreement_type) ? $model->agreementTypeList[$model->agreement_type] : null,
            ],
            [
                'attribute' => 'project_name',
                'value' => '<div style="overflow-x: auto;">' . $contentProject . '</div>',
                'format' => 'raw',
                'visible' => FSMAccessHelper::can('viewProject'),
            ],
            [
                'attribute' => 'parent_id',
                'value' => !empty($model->parent_id) ? Html::a($model->parent->number, ['/agreement/view', 'id' => $model->parent_id], ['target' => '_blank']) : null,
                'format' => 'raw',
                'visible' => !empty($model->parent_id),
            ],
            [
                'attribute' => 'first_client_id',
                'value' => !empty($model->first_client_id) ? 
                    (!empty($model->first_client_role_id) ? $model->firstClientRole->name.': ' : '') .
                    (FSMAccessHelper::can('viewClient', $model->firstClient) ?
                        Html::a($model->firstClient->name, ['/client/view', 'id' => $model->first_client_id], ['target' => '_blank']) :
                        $model->firstClient->name
                    )
                    : null,
                'format' => 'raw',
            ],            
            [
                'attribute' => 'second_client_id',
                'value' => !empty($model->second_client_id) ?   
                    (!empty($model->second_client_role_id) ? $model->secondClientRole->name.': ' : '') . 
                    (FSMAccessHelper::can('viewClient', $model->secondClient) ?
                        Html::a($model->secondClient->name, ['/client/view', 'id' => $model->second_client_id], ['target' => '_blank']) :
                        $model->secondClient->name 
                    )
                    : null,
                'format' => 'raw',
            ],            
            [
                'attribute' => 'third_client_id',
                'value' => !empty($model->third_client_id) ?   
                    (!empty($model->third_client_role_id) ? $model->thirdClientRole->name.': ' : '') . 
                    (FSMAccessHelper::can('viewClient', $model->thirdClient) ?
                        Html::a($model->thirdClient->name, ['/client/view', 'id' => $model->third_client_id], ['target' => '_blank']) :
                        $model->thirdClient->name 
                    )
                    : null,
                'format' => 'raw',
                'visible' => !empty($model->third_client_id),
            ],            
            'number',
            'signing_date',
            'due_date',
            'summa',
            'deferment_payment',
            [
                'attribute' => 'rate',
                'visible' => !empty($model->rate),
            ],
            [
                'attribute' => 'rate_summa',
                'visible' => !empty($model->rate_summa),
            ],
            [
                'attribute' => 'rate_from_date',
                'label' => Yii::t('client', 'Interest period'),
                'value' => isset($model->rate_from_date) && isset($model->rate_till_date) ? 
                    date('d-M-Y', strtotime($model->rate_from_date)) . ' | ' . date('d-M-Y', strtotime($model->rate_till_date)) : null,
                'visible' => !empty($model->rate_from_date) && !empty($model->rate_till_date),
            ],
            [
                'attribute' => 'conclusion',
                'value' => isset($model->conclusion) ? $model->agreementConclusionList[$model->conclusion] : null,
            ],
            'write_activity',
            'subject_contract',    
            [
                'attribute' => 'consideration',
                'value' => $jsonParseStr,
                'visible' => !empty($jsonParseStr),
                'format' => 'raw',
            ],
            [
                'attribute' => 'prepayment_proc',
                'visible' => !empty($model->prepayment_proc),
            ],     
            [
                'attribute' => 'prepayment_sum',
                'value' => isset($model->prepayment_valuta_id) ? number_format($model->prepayment_sum, 2, '.', ' ') . ' ' . $model->prepaymentValuta->name : number_format($model->prepayment_sum, 2, '.', ' '),
            ],
            'prepayment_term',
            'comment:ntext',
            [
                'attribute' => 'deleted',
                'format' => 'boolean',
                'visible' => $isAdmin,
            ],
        ],
    ]) ?>
</div>

<?php 
    $paymentTableView = '';
    if($paymentDataProvider->count > 0) :
        echo '<div class="col-md-12">';
        echo $this->render('_payment_table_view', [
                'searchModel' => $paymentModel,
                'dataProvider' => $paymentDataProvider,
                'linkedModel' => $model,
            ]);                        
        echo '</div>';
    endif;
?> 

<div class='col-md-12'>
    <?= $this->render('_history_table_view', [
            'searchModel' => $historyModel,
            'dataProvider' => $historyDataProvider,
        ]); 
    ?>
</div>    

<div class='col-md-12'>
    <?= $this->render('_attachment_table', [
        'model' => $filesModel,
    ]) ?>         
</div>
