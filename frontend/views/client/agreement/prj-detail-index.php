<?php
//namespace common\models;

//use Yii;
//use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\grid\GridView;
use yii\helpers\Url;

use kartik\helpers\Html;
use kartik\grid\GridView;

use common\models\client\Agreement;
use common\components\FSMHelper;
use common\components\FSMAccessHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\client\search\AgreementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="agreement-index">

    <div id="page-content">
        
        <?php
            $columns = [
                //['class' => '\kartik\grid\SerialColumn'],

                [
                    'attribute' => 'id',
                    'width' => '75px',
                    'hAlign' => 'center',
                ],
                [
                    'attribute' => 'number',
                    'contentOptions' => [
                        'style'=>'max-width:100px; min-height:75px; word-wrap: break-word;'
                    ],
                    'value' => function ($model) {
                        return 
                            '<div style="overflow-x: auto;">'.
                            (!empty($model->number) ? (FSMAccessHelper::can('viewAgreement', $model) ?
                                Html::a($model->number, ['/agreement/view', 'id' => $model->id], ['target' => '_blank', 'data-pjax' => 0]) :
                                $model->number
                            )
                            : null).
                            '</div>';
                    },                        
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'agreement_type',
                    //'headerOptions' => ['class'=>'td-mw-100'],
                    'value' => function ($model) {
                        return isset($model->agreement_type) ? $model->agreementTypeList[$model->agreement_type] : null;
                    },
                    'visible' => $isAdmin,
                    'format'=>'raw',
                ],
                [
                    'attribute' => 'status',
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'value' => function ($model) {
                        return isset($model->status) ? Html::badge($model->agreementStatusList[$model->status], ['class' => $model->statusBackgroundColor.' status-badge']) : null;
                    },
                    'visible' => $isAdmin,
                    'format'=>'raw',
                ],
                [
                    'attribute' => 'grid_progress',
                    'header' => Yii::t('common', 'Progress'),
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'contentOptions' => [
                        'class' => 'td-slider',
                    ],
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'mergeHeader' => true,
                    'value' => function ($model) {
                        return $model->getProgressButtons();
                    },
                    'format' => 'raw',
                    'visible' => FSMAccessHelper::can('changeAgreementStatus'),
                    'hiddenFromExport' => true,
                ],
                [
                    'attribute'=>'first_client_name',
                    //'headerOptions' => ['class'=>'td-mw-100'],
                    'contentOptions' => [
                        'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                    ],
                    'value' => function ($model) {
                        return 
                            '<div style="overflow-x: auto;">'.
                            (!empty($model->first_client_id) ? 
                            (!empty($model->first_client_role_id) ? $model->firstClientRole->name.':<br/>' : '') . 
                                (FSMAccessHelper::can('viewClient', $model->firstClient) ?
                                    Html::a($model->first_client_name, ['/client/view', 'id' => $model->first_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                    $model->first_client_name
                                )
                            : null).
                            '</div>';
                    },                         
                    'format'=>'raw',
                    //'hiddenFromExport' => true,
                ],            
                [
                    'attribute'=>'second_client_name',
                    //'headerOptions' => ['class'=>'td-mw-100'],
                    'contentOptions' => [
                        'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                    ],
                    'value' => function ($model) {
                        return 
                            '<div style="overflow-x: auto;">'.
                            (!empty($model->second_client_id) ?  
                            (!empty($model->second_client_role_id) ? $model->secondClientRole->name.':<br/>' : '') . 
                                (FSMAccessHelper::can('viewClient', $model->secondClient) ?
                                    Html::a($model->second_client_name, ['/client/view', 'id' => $model->second_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                    $model->second_client_name
                                )
                            : null).
                            '</div>';
                    },                         
                    'format'=>'raw',
                    //'hiddenFromExport' => true,
                ],            
                [
                    'attribute'=>'third_client_name',
                    //'headerOptions' => ['class'=>'td-mw-100'],
                    'contentOptions' => [
                        'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                    ],
                    'value' => function ($model) {
                        return 
                            '<div style="overflow-x: auto;">'.
                            (!empty($model->third_client_id) ?   
                            (!empty($model->third_client_role_id) ? $model->thirdClientRole->name.':<br/>' : '') . 
                                (FSMAccessHelper::can('viewClient', $model->thirdClient) ?
                                    Html::a($model->third_client_name, ['/client/view', 'id' => $model->third_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                    $model->third_client_name
                                )
                            : null).
                            '</div>';
                    },                         
                    'format'=>'raw',
                    //'hiddenFromExport' => true,
                ],            
                [
                    'attribute' => 'signing_date',
                    'headerOptions' => [
                        'class' => 'td-mw-75',
                    ],
                    'value' => function ($model) {
                        return !empty($model->signing_date) ? date('d-M-Y', strtotime($model->signing_date)) : null;
                    },                 
                ],                         
                [
                    'attribute' => 'due_date',
                    'headerOptions' => [
                        'class' => 'td-mw-75',
                    ],
                    'value' => function ($model) {
                        return !empty($model->due_date) ? date('d-M-Y', strtotime($model->due_date)) : null;
                    },                 
                ],                                                 
                [
                    'attribute' => 'summa',
                    'hAlign' => 'right',
                    'headerOptions' => [
                        'class' => 'td-mw-75',
                        //'style' => 'text-align: left;',
                    ],
                ],                 
                [
                    'attribute' => 'conclusion',
                    'headerOptions' => ['class'=>'td-mw-100'],                
                    'value' => function ($model) {
                        return isset($model->status) ? $model->agreementConclusionList[$model->conclusion] : null;
                    },
                    'visible' => $isAdmin,
                ],
            ];
        ?>

        <?= GridView::widget([
            'id' => 'grid-view',
            'tableOptions' => [
                'id' => 'agreement-list',
            ],
            'responsive' => false,
            'condensed' => true,
            //'striped' => false,
            'hover' => true,
            'dataProvider' => $dataProvider,
            'filterModel' => null,
            'columns' => $columns,
            'pjax' => true,
            'floatHeader' => true,
            'export' => false,
        ]);
        ?>

    </div>    
</div>