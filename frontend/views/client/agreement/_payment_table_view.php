<?php

use yii\widgets\Pjax;

use kartik\helpers\Html;
use kartik\grid\GridView;

use common\components\FSMAccessHelper;
use common\models\client\AgreementPayment;
?>

<?php 
    ob_start();
    ob_implicit_flush(false);
?>

<div class="agreement-payment-index">
    <?php 
    $columns = [
        ['class' => '\kartik\grid\SerialColumn'],
        /*
        [
            'attribute' => 'id',
            'width' => '75px',
            'hAlign' => 'center',
            //'pageSummary' => $searchModel->getAttributeLabel('total'),
        ],
         * 
         */
        [
            'attribute'=>'from_bank_id',
            'contentOptions' => [
                'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
            ],
            'value' => function ($model) {
                return 
                    '<div style="overflow-x: auto;">'.
                    (!empty($model->from_bank_name) ? 
                        Html::a($model->from_bank_name, ['/bank/view', 'id' => $model->from_bank_id], ['target' => '_blank', 'data-pjax' => 0])
                        : null
                    ).
                    '</div>';
            },
            'format'=>'raw',
        ],                    
        [
            'attribute'=>'to_bank_id',
            'contentOptions' => [
                'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
            ],
            'value' => function ($model) {
                return 
                    '<div style="overflow-x: auto;">'.
                    (!empty($model->to_bank_name) ? 
                        Html::a($model->to_bank_name, ['/bank/view', 'id' => $model->to_bank_id], ['target' => '_blank', 'data-pjax' => 0])
                        : null
                    ).
                    '</div>';
            },
            'format'=>'raw',
        ], 
        [
            'attribute' => 'paid_date',
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'headerOptions' => ['class'=>'td-mw-75'],
            'width' => '100px',
            'value' => function ($model) {
                return isset($model->paid_date) ? date('d-M-Y', strtotime($model->paid_date)) : null;
            },
        ],              
        [
            'attribute' => 'direction',
            'value' => function ($model) {
                return $model->getPaymentDirectionList()[$model->direction];
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $searchModel->getPaymentDirectionList(),
            'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
            'filterInputOptions' => [
                'id' => 'agrpayment-direction',
                'placeholder' => '...',
            ],                      
        ],                     
        [
            'attribute' => 'summa_eur',
            'hAlign' => 'right',
            'headerOptions' => ['style'=>'text-align: center;'],
            'mergeHeader' => true,
            'value' => function ($model) {
                return isset($model->summa_eur) ? $model->summa_eur : null;
            },
            'format' => ['decimal', 2],
            //'pageSummary' => true,                 
        ],        
        [
            'class' => '\common\components\FSMActionColumn',
            'headerOptions' => ['class'=>'td-mw-125'],
            'dropdown' => true,
            'template' => '{confirm} {update} {delete}',
            'checkPermission' => true,
            'buttons' => [
                'confirm' => function ($params) { 
                    return AgreementPayment::getButtonConfirm($params);
                },                
                'update' => function ($params) { 
                    return AgreementPayment::getButtonUpdate($params);
                },
            ], 
            'controller' => 'agreement-payment',
            'linkedObj' => [
                ['fieldName' => 'agreement_id', 'id' => (!empty($linkedModel->id) ? $linkedModel->id : null)],
            ],             
        ],
    ];
    ?>

    <?= GridView::widget([
        'id' => 'grid-view',
        'tableOptions' => [
            'id' => 'agreement-payment-list',
        ],
        'responsive' => false,
        'striped' => true,
        'hover' => true,
        'bordered' => true,
        'condensed' => true,
        'persistResize' => false,
        'autoXlFormat' => true,
        'dataProvider' => $dataProvider,
        //'showPageSummary' => true,
        'pjax' => true,
        'columns' => $columns,
    ]); ?>  
</div>

<?php
    $body = ob_get_contents();
    ob_get_clean(); 

    $panelContent = [
        'heading' => AgreementPayment::modelTitle(2),
        'preBody' => '<div class="panel-body">',
        'body' => $body,
        'postBody' => '</div>',
    ];
    echo Html::panel(
        $panelContent, 
        'warning', 
        [
            'id' => "panel-payment-data",
        ]
    );
?>