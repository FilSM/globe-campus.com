<?php

use yii\helpers\Url;
use kartik\helpers\Html;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;

use common\components\FSMHtml;
?>

<?php
$emptyModelClassName = get_class($model[0]);
$className = substr(strrchr($emptyModelClassName, "\\"), 1);

ob_start();
ob_implicit_flush(false);
?>

<table class="table table-bordered table-striped margin-b-none">
    <thead>
        <tr>
            <th class="required"><?= Yii::t('client', 'Signing person'); ?></th>
        </tr>
    </thead>
    <tbody class="form-persons-body">
        <tr class="form-persons-item">
            <td>
                <?php 
                    $agreementPerson = $model[0]; 
                ?>
                
                <?= Html::activeHiddenInput($agreementPerson, 'id', [
                    'id' => strtolower($className).'-0', 
                    'name' => $className.'[0][id]', 
                    'value' => $agreementPerson->id,
                ]); ?>
                
                <?= $form->field($agreementPerson, "[0]client_contact_id", [
                    'horizontalCssClasses' => [
                        'wrapper' => 'col-md-12',
                    ],
                ])->widget(DepDrop::class, [
                    'type' => DepDrop::TYPE_SELECT2,
                    'data' => empty($agreementPerson->client_contact_id) || empty($agreementPerson->clientContact) ? null :
                            [$agreementPerson->client_contact_id => $agreementPerson->clientContact->first_name . ' ' . $agreementPerson->clientContact->last_name .
                        (!empty($agreementPerson->clientContact->position_id) ? ' ( ' . $agreementPerson->clientContact->position->name . ' )' : '')],
                    'options' => [
                        'class' => 'person-item-select',
                    ],                        
                    'select2Options' => [
                        'pluginOptions' => [
                            'allowClear' => !$agreementPerson->isAttributeRequired('client_contact_id'),
                        ],
                    ],
                    'pluginOptions' => [
                        'depends' => [$depends],
                        //'initDepends' => [$depends],
                        //'initialize' => !empty($agreementPerson->client_contact_id) && empty($model[1]->id) && ($depends == 'second-client-id'),
                        'url' => Url::to(['/client/ajax-get-client-person-list', 'can_sign' => true, 'doc_date' => ($docDate ?? null)]),
                        'placeholder' => '...',
                    ]
                ])->label(false);
                ?>
            </td>
        </tr>
        
        <tr class="form-persons-item">
            <td>
                <?php 
                    $agreementPerson = $model[1] ?? new $emptyModelClassName; 
                ?>
                
                <?= Html::activeHiddenInput($agreementPerson, 'id', [
                    'id' => strtolower($className).'-1', 
                    'name' => $className.'[1][id]', 
                    'value' => $agreementPerson->id,
                ]); ?>
                
                <?= $form->field($agreementPerson, "[1]client_contact_id", [
                    'horizontalCssClasses' => [
                        'wrapper' => 'col-md-12',
                    ],
                ])->widget(DepDrop::class, [
                    'type' => DepDrop::TYPE_SELECT2,
                    'data' => empty($agreementPerson->client_contact_id) || empty($agreementPerson->clientContact) ? null :
                            [$agreementPerson->client_contact_id => $agreementPerson->clientContact->first_name . ' ' . $agreementPerson->clientContact->last_name .
                        (!empty($agreementPerson->clientContact->position_id) ? ' ( ' . $agreementPerson->clientContact->position->name . ' )' : '')],
                    'options' => [
                        'class' => 'person-item-select',
                    ],                        
                    'select2Options' => [
                        'pluginOptions' => [
                            'allowClear' => !$agreementPerson->isAttributeRequired('client_contact_id'),
                        ],
                    ],
                    'pluginOptions' => [
                        'depends' => [$depends],
                        'initDepends' => [$depends],
                        //'initialize' => !empty($agreementPerson->client_contact_id) && ($depends == 'second-client-id'),
                        'initialize' => true,
                        'url' => Url::to(['/client/ajax-get-client-person-list', 'can_sign' => true, 'doc_date' => ($docDate ?? null)]),
                        'placeholder' => '...',
                    ]
                ])->label(false);
                ?>
            </td>
        </tr>
    </tbody>
</table>

<?php
    $body = ob_get_contents();
    ob_get_clean(); 
    
    //echo $body;

    $panelContent = [
        //'heading' => BillPerson::modelTitle(2),
        'preBody' => '<div class="panel-body">',
        'body' => $body,
        'postBody' => '</div>',
    ];
    echo FSMHtml::panel(
        $panelContent, 
        'default', 
        [
            'id' => $panel_person_id,
            'class' => 'panel-person-data',
        ]
    );
?>