<?php
namespace common\models;

use Yii;
//use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
//use yii\grid\GridView;

use kartik\helpers\Html;
use kartik\grid\GridView;
use kartik\icons\Icon;

use common\components\FSMAccessHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\client\search\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

Icon::map($this);
$this->title = $searchModel->modelTitle(2);
$this->params['breadcrumbs'][] = $this->title;

$forDetail = !empty($forDetail);
?>
<div class="project-index">

    <?= Html::pageHeader(Html::encode($this->title)); ?>
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if(FSMAccessHelper::can('createProject')): ?>
    <p>
        <?= Html::a(Html::icon('plus').'&nbsp;'.$searchModel->modelTitle(), ['create'], ['class' => 'btn btn-success']); ?>
    </p>
    <?php endif; ?>
    
    <?= GridView::widget([
        'responsive' => false,
        //'striped' => false,
        'hover' => true,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'floatHeader' => true,
        'pjax' => true,
        'columns' => [
            //['class' => '\kartik\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'width' => '75px',
                'hAlign' => 'center',
            ],
            [
                'class' => '\kartik\grid\ExpandRowColumn',
                'expandIcon' => Icon::show('chevron-circle-right'),
                'collapseIcon' => Icon::show('chevron-circle-down'),
                'value' => function ($model, $key, $index, $column) {
                    return GridView::ROW_COLLAPSED;
                },
                'detailUrl' => Url::to(['/agreement/project-detail']),
                'visible' => FSMAccessHelper::can('viewAgreement') && !$forDetail,
            ],
            'name',
            [
                'attribute'=>'country_name',
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) {
                    return !empty($model->country_name) ? $model->country_name : null;
                },                         
            ],            
            'address',
            [
                'attribute' => "vat_taxable",                
                'class' => '\kartik\grid\BooleanColumn',
                'trueLabel' => 'Yes', 
                'falseLabel' => 'No',
                'width' => '100px',
            ],
            'comment:ntext',
            //'create_time',
            //'create_user_id',
            //'update_time',
            // 'update_user_id',
            [
                'attribute' => "deleted",                
                'class' => '\kartik\grid\BooleanColumn',
                'trueLabel' => 'Yes', 
                'falseLabel' => 'No',
                'width' => '100px',
                'visible' => $isAdmin,
            ],

            [
                'class' => '\common\components\FSMActionColumn',
                'headerOptions' => ['class'=>'td-mw-125'],
                'dropdown' => true,
                'checkPermission' => true,
            ],
        ],
    ]); ?>
</div>