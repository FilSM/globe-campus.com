<?php
namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;
use kartik\widgets\FileInput;
use kartik\icons\Icon;

use common\models\user\FSMUser;

/* @var $this yii\web\View */
/* @var $model common\models\client\Project */
/* @var $form yii\widgets\ActiveForm */

Icon::map($this);
$isModal = !empty($isModal);
$isAdmin = !empty($isAdmin);
?>

<div class="project-form">
    <?php if($isModal) : Pjax::begin(Yii::$app->params['PjaxModalOptions']); endif; ?>
    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => $model->tableName().'-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'options' => [
            'data-pjax' => $isModal,
        ],         
    ]); ?>

    <?= Html::activeHiddenInput($model, 'abonent_id'); ?>
        
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'country_id')->widget(Select2::class, [
            'data' => $countryList,
            'options' => [
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => true,
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],
            'addon' => [
                'prepend' => $model::getModalButtonContent([
                    'formId' => $form->id,
                    'controller' => 'country',
                ]),
            ],        
        ]); 
    ?>
    
    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vat_taxable')->widget(SwitchInput::class, [
        'pluginOptions' => [
            'onText' => Yii::t('common', 'Yes'),
            'offText' => Yii::t('common', 'No'),
        ],
    ]);?>
        
    <?php
    if(!empty($filesModel)){
        $preview = $previewConfig = [];
        foreach ($filesModel as $key => $file) {
            if(empty($file->id)){
                continue;
            }
            $preview[] = $file->uploadedFileUrl;
            $ext = pathinfo($file->filename, PATHINFO_EXTENSION);
            switch ($ext) {
                default:
                case 'pdf':
                    $type = 'pdf';
                    break;
                case 'doc':
                case 'docx':
                case 'edoc':
                case 'bdoc':
                case 'xls':
                case 'xlsx':
                case 'pptx':
                    $type = 'gdocs';
                    break;
                case 'zip':
                    $type = 'object';
                    break;
            }
            $previewConfig[] = [
                'type' => $type,
                'size' => $file->filesize, 
                'caption' => $file->filename, 
                'url' => Url::to(['/project/attachment-delete', 'deleted_id' => $file->id]), 
                'key' => 101 + $key,
                'downloadUrl' => $file->uploadedFileUrl,
            ];
        }

    echo $form->field($filesModel[0], 'file[]')->widget(FileInput::class, [
            'language' =>  strtolower(substr(Yii::$app->language, 0, 2)),
            'sortThumbs' => true,
            'options' => [
                'id' => 'project-files',
                'multiple' => true,
            ],
            'pluginOptions' => [
                //'uploadUrl' => "/file-upload-batch/2",
                'allowedFileExtensions' => ['png', 'jpg', 'jpeg', 'pdf', 'doc', 'docx', 'edoc', 'bdoc', 'xls', 'xlsx', 'pptx', 'zip'],
                'uploadAsync' => false,
                'maxFileSize' => 40000,
                //'maxFileCount' => 1,
                'showRemove' => false,
                'showUpload' => false,
                'overwriteInitial' => false,
		//'allowedPreviewTypes' => ['pdf'],
                'allowedPreviewTypes' => null, // set to empty, null or false to disable preview for all types                
                'initialPreview' => $preview,
                'initialPreviewAsData' => true,
                'initialPreviewFileType' => 'pdf',
                //'initialPreviewDownloadUrl' => 'http://kartik-v.github.io/bootstrap-fileinput-samples/samples/{filename}',
                'initialPreviewConfig' => $previewConfig,
                'initialPreviewShowDelete' => true,            
                //'purifyHtml' => true,
                
                'preferIconicPreview' => true, // this will force thumbnails to display icons for following file extensions
                'previewFileIcon' => Icon::show('file'),
                'previewFileIconSettings' => [ // configure your icon file extensions
                    'png' => Icon::show('file-image', ['class' => 'text-info']),
                    'jpg' => Icon::show('file-image', ['class' => 'text-info']),
                    'jpeg' => Icon::show('file-image', ['class' => 'text-info']),
                    'pdf' => Icon::show('file-pdf', ['class' => 'text-danger']),
                    'doc' => Icon::show('file-word', ['class' => 'text-primary']),
                    'docx' => Icon::show('file-word', ['class' => 'text-primary']),
                    'edoc' => Icon::show('file-word', ['class' => 'text-primary']),
                    'bdoc' => Icon::show('file-word', ['class' => 'text-primary']),
                    'xls' => Icon::show('file-excel', ['class' => 'text-success']),
                    'xlsx' => Icon::show('file-excel', ['class' => 'text-success']),
                    'zip' => Icon::show('file-archive', ['class' => 'text-muted']),
                ],
            ]
        ])->label(Yii::t('files', 'Attachment'))->hint('Allowed file extensions: .png .jpg .jpeg .pdf .doc .docx .edoc .bdoc .xls .xlsx .pptx .zip'); 
    }
    ?>
    
    <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>
    
    <?php if(!empty($model->id) && !empty($isAdmin)){
        echo $form->field($model, 'deleted')->widget(SwitchInput::class, [
            'pluginOptions' => [
                'onText' => Yii::t('common', 'Yes'),
                'offText' => Yii::t('common', 'No'),
            ],
        ]);
    } ?>

    <div class="form-group <?php if($isModal) : echo 'modal-button-group'; endif; ?>">
        <div class="col-md-offset-2 col-md-10" style="text-align: right;">
            <?= $model->SubmitButton; ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <?php if($isModal) : Pjax::end(); endif; ?>

</div>