<?php

use kartik\helpers\Html;

use common\components\FSMHtml;
use common\models\client\ProjectAttachment;
?>

<?php 
    ob_start();
    ob_implicit_flush(false);
?>

<table class="table table-bordered table-striped margin-b-none">
    <thead>
        <tr>
            <th style="text-align: center; width: 75px;">#</th>
            <th><?= $model[0]->getAttributeLabel('filename'); ?></th>
        </tr>
    </thead>
    <tbody class="form-attachment-body">
        <?php foreach ($model as $index => $attachment): 
            if(empty($attachment->id)){
                continue;
            }
            $ext = strtolower(pathinfo($attachment->filename, PATHINFO_EXTENSION));
            $target = ($ext == 'pdf') ? '_blank' : '_self';
            ?>
            <tr class="form-attachment-item">
                <td style="text-align: center;"><?= $index + 1; ?></td>
                <td>
                    <?= Html::a($attachment->filename, $attachment->fileurl, ['target' => $target, 'data-pjax' => 0,]); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php
    $body = ob_get_contents();
    ob_get_clean(); 

    $panelContent = [
        'heading' => ProjectAttachment::modelTitle(2),
        'preBody' => '<div class="panel-body">',
        'body' => $body,
        'postBody' => '</div>',
    ];
    echo FSMHtml::panel(
        $panelContent, 
        'default', 
        [
            'id' => "panel-attachment-data",
        ]
    );
?>