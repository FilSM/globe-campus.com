<?php

use kartik\helpers\Html;
use kartik\detail\DetailView;

use common\components\FSMAccessHelper;

/* @var $this yii\web\View */
/* @var $model common\models\client\Project */

$this->title = $model->modelTitle() .' #'. $model->id;
if(FSMAccessHelper::checkRoute('/project/index')){
    $this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-view">

    <?= Html::pageHeader(Html::encode($this->title)); ?>

    <p>
        <?php if(FSMAccessHelper::can('updateProject', $model)): ?>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>
        <?php if(FSMAccessHelper::can('deleteProject', $model)): ?>
        <?= \common\components\FSMBtnDialog::button(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'id' => 'btn-dialog-selected',
            'class' => 'btn btn-danger',
        ]); ?> 
        <?php endif; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'country_id',
                'value' => !empty($model->country_id) ? $model->country->name : null,
            ],            
            'address',
            [
                'attribute' => 'vat_taxable',
                'format' => 'boolean',
            ],
            'comment:ntext',
            [
                'attribute' => 'deleted',
                'format' => 'boolean',
                'visible' => $isAdmin,
            ],
        ],
    ]) ?>
    
    <?= $this->render('_attachment_table', [
        'model' => $filesModel,
    ]) ?>      

</div>