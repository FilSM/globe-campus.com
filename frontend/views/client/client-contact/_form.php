<?php
namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;

use common\widgets\EnumInput;

/* @var $this yii\web\View */
/* @var $model common\models\client\ClientContact */
/* @var $form yii\widgets\ActiveForm */
$isModal = !empty($isModal) ? 1 : 0;
?>

<div class="client-contact-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => $model->tableName().'-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>

    <?php if(!empty($model->client_id)){
            echo Html::activeHiddenInput($model, 'client_id');
        }else{
            echo $form->field($model, 'client_id')->textInput();
        }
    ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->widget(MaskedInput::class, [
        'clientOptions' => [
            'greedy' => false,
        ],
        'mask' => '(+9{1,4}) 9{8,15}',
        'options' => [
            'class' => 'form-control',
            'placeholder' => Yii::t('common', 'Enter as') . ' (+999) 9999999999...',
        ],
    ]);
    ?>
    
    <?= $form->field($model, 'email')
        ->widget(MaskedInput::class, [
            'clientOptions' => [
                'alias' => 'email',
            ],
        ]); ?>

    <?= $form->field($model, 'position_id')->widget(Select2::class, [
            'data' => $positionList,
            'options' => [
                'id' => 'position-id',
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => !$model->isAttributeRequired('position_id'),
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],            
            'addon' => [
                'prepend' => $model::getModalButtonContent([
                    'formId' => $form->id,
                    'controller' => 'person-position',
                ]),
            ],        
        ]); 
    ?>
    
    <?= $form->field($model, 'term_from', [
        ])->widget(DatePicker::class, [
        'type' => DatePicker::TYPE_RANGE,
        'attribute' => 'term_from',
        'attribute2' => 'term_till',
        'options' => [
            'placeholder' => Yii::t('common', 'Start date'),
        ],
        'options2' => [
            'placeholder' => Yii::t('common', 'End date'),
        ],
        'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
    ])->label(Yii::t('common', 'Period')); ?>

    <?= $form->field($model, 'top_manager')->widget(SwitchInput::class, [
        'options' => [
            'id' => 'top-manager',
            'data-init_value' => $model->top_manager,
        ],          
        'pluginOptions' => [
            'onText' => Yii::t('common', 'Yes'),
            'offText' => Yii::t('common', 'No'),
        ],
    ]) ?>
    
    <?= $form->field($model, 'can_sign')->widget(SwitchInput::class, [
        'options' => [
            'id' => 'can-sign',
            'data-init_value' => $model->can_sign,
        ],          
        'pluginOptions' => [
            'onText' => Yii::t('common', 'Yes'),
            'offText' => Yii::t('common', 'No'),
        ],
    ]) ?>
    
    <?php
        echo Html::beginTag('div', [
            'id' => 'can-sign-data-container',
        ]);
    ?>

    <?= $form->field($model, 'acting_on_basis')->widget(EnumInput::class, [
            'type' => EnumInput::TYPE_RADIOBUTTON,
            'options' => [
                'translate' => $model->actingTypeList,
            ],
        ]); 
    ?> 
    
    <?php /*
    <?= $form->field($model, 'share')->widget(MaskedInput::class, [
            'mask' => '9{1,3}[.9{1,2}]',
            'options' => [
                'class' => 'form-control number-field',
            ],
    ]) */?>

    <?php
        echo Html::endTag('div');           
    ?>
    
    <?= $form->field($model, 'receive_invoice')->widget(SwitchInput::class, [
        'pluginOptions' => [
            'onText' => Yii::t('common', 'Yes'),
            'offText' => Yii::t('common', 'No'),
        ],
    ]) ?>


    <?= $form->field($model, 'main')->widget(SwitchInput::class, [
        'pluginOptions' => [
            'onText' => Yii::t('common', 'Yes'),
            'offText' => Yii::t('common', 'No'),
        ],
    ]) ?>
    
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10" style="text-align: right;">
            <?= $model->SubmitButton; ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
    var form = $("#client_contact-form");

    form.find('#can-sign').on('init.bootstrapSwitch switchChange.bootstrapSwitch', function(){
        var state = (arguments.length == 1 ? arguments[0] : (arguments[1] != undefined ? arguments[1] : null));
        if(state == 'init'){
            var initValue = $(this).data('init_value');
            state = initValue ? true : false;
        }
        switch (state) {
            case true:
            //default:
                form.find('#can-sign-data-container').show('slow');
                break;
            case false:
                form.find('#can-sign-data-container').hide('slow');
                var inputList = form.find('#can-sign-data-container').find('input');
                inputList.val('');
                break;
        }            
    }).trigger('init.bootstrapSwitch', 'init');
JS;
$this->registerJs($script);
?>