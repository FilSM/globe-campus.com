<?php
namespace common\models;

use Yii;
use yii\helpers\Url;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\client\ClientInvoicePdf */
/* @var $form yii\widgets\ActiveForm */
$isAdmin = !empty($isAdmin);
?>

<div class="client-invoice-pdf-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => $model->tableName().'-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>

    <?= Html::activeHiddenInput($model, 'abonent_id'); ?>

    <?php if(!empty($model->client_id)){
            echo Html::activeHiddenInput($model, 'client_id');
        }else{
            echo $form->field($model, 'client_id')->textInput();
        }
    ?>

    <?= $form->field($model, 'template_name')->widget(Select2::class, [
            'data' => $templateNameList,
            'options' => [
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => !$model->isAttributeRequired('template_name'),
            ],
        ]); 
    ?>    
    
    <?php 
        $preview = !empty($filesModel->uploadedFileUrl) ? [$filesModel->uploadedFileUrl] : [];
        echo $form->field($filesModel, 'file')->widget(FileInput::class, [
            'language' =>  strtolower(substr(Yii::$app->language, 0, 2)),
            'sortThumbs' => false,
            'options' => [
                'id' => 'client-files',
                //'multiple' => true,
            ],
            'pluginOptions' => [
                'allowedFileExtensions' => ['png', 'jpg', 'jpeg'],
                'maxFileSize' => 5000,
                'maxFileCount' => 1,
                'showRemove' => false,
                'showUpload' => false,
                //'showCaption' => false,
                //'browseClass' => 'btn btn-primary btn-block',
                'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                'initialPreview' => $preview,
                'initialPreviewShowDelete' => true,                
                'initialPreviewAsData' => true,
                'initialPreviewConfig' => [
                    [
                        'type' => "image",
                        'size' => $filesModel->filesize, 
                        'caption' => $filesModel->filename, 
                        'url' => Url::to(['/client-invoice-pdf/attachment-delete', 'deleted_id' => $filesModel->id]), 
                        //'key' => 101,
                    ],
                ],
                'overwriteInitial' => false,
            ],
        ])->label($model->getAttributeLabel('uploaded_file_id')); 
    ?>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-10" style="text-align: right;">
            <?= $model->SubmitButton; ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>