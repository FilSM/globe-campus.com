<?php

use kartik\helpers\Html;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\client\ClientInvoicePdf */

$this->title = $model->modelTitle();
$this->params['breadcrumbs'][] = ['label' => $clientModel->modelTitle(2), 'url' => ['client/index']];
$this->params['breadcrumbs'][] = ['label' => $clientModel->name, 'url' => ['client/view', 'id' => $clientModel->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row client-invoice-pdf-view">
    
    <div class="col-md-2">
        <?= $this->render('@frontend/views/client/client/_menu', [
            'client' => $clientModel,
            'activeItem' => $model->tableName(),
        ])
        ?>
    </div>
    
    <div class="col-md-10">
        <?php 
            $body = '<p>'.
                Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id, 'client_id' => $clientModel->id], ['class' => 'btn btn-primary']).'&nbsp;'.
                \common\components\FSMBtnDialog::button(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id, 'client_id' => $clientModel->id], [
                    'id' => 'btn-dialog-selected',
                    'class' => 'btn btn-danger',
                ]).
                '</p>';
            $body .= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'attribute' => 'id',
                        'visible' => $isAdmin,
                    ],
                    [
                        'attribute' => 'template_name',
                        'value' => $model->templateNameList[$model->template_name],
                        'visible' => $isAdmin,
                    ],
                    [
                        'attribute' => 'uploaded_file_id',
                        'value' => !empty($filePath) ? Html::img($filePath, ['width' => '100px']) : null,
                        'format' => 'html',    
                        'visible' => !empty($filePath),
                    ],                     
                ],
            ]); 

            $panelContent = [
                'heading' => Html::encode($this->title),
                'preBody' => '<div class="panel-body">',
                'body' => $body,
                'postBody' => '</div>',
            ];
            echo Html::panel(
                $panelContent, 
                'primary', 
                [
                    'id' => "panel-client-invoice-pdf-data",
                ]
            );        
        ?>        
    </div>
</div>