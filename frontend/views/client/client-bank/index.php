<?php
namespace common\models;

use kartik\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\client\search\ClientBankSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if(empty($clientModel->id)) :
    $this->title = $searchModel->modelTitle(2);
    $this->params['breadcrumbs'][] = $this->title;
endif;

$isAdmin = !empty($isAdmin);
?>
<div class="client-bank-index">

    <?php if(empty($clientModel->id)) : ?>
    <?= Html::pageHeader(Html::encode($this->title)); ?>
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Html::icon('plus').'&nbsp;'.$searchModel->modelTitle(), ['create'], ['class' => 'btn btn-success']); ?>
    </p>
    <?php endif; ?>
    
    <?= GridView::widget([
        'responsive' => false,
        //'striped' => false,
        'hover' => true,
        'floatHeader' => true,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        //'filterModel' => empty($clientModel->id) ? $searchModel : null,
        'columns' => [
            [
                'class' => '\kartik\grid\SerialColumn',
                'visible' => !empty($clientModel->id),
            ],
            [
                'attribute' => 'id',
                'width' => '75px',
                'hAlign' => 'center',
                'visible' => empty($clientModel->id),
            ],
            
            [
                'attribute' => "primary",                
                'class' => '\kartik\grid\BooleanColumn',
                'trueLabel' => 'Yes', 
                'falseLabel' => 'No',
                'width' => '100px',
            ],                            
            [
                'attribute' => "is_active",   
                'class' => '\kartik\grid\BooleanColumn',
                'vAlign' => 'middle',
                'width' => '100px',
                'trueLabel' => 'Yes', 
                'falseLabel' => 'No',
            ],
            
            [
                'attribute' => 'client_id',
                'value' => function ($model) {
                    return !empty($model->client_id) ? Html::a($model->client_name, ['/client/view', 'id' => $model->client_id], ['target' => '_blank', 'data-pjax' => 0]) : null;
                }, 
                'format' => 'raw',
                'visible' => empty($clientModel->id),
            ],              
            [
                'attribute' => 'bank_id',
                'value' => function ($model) {
                    return !empty($model->bank_id) ? Html::a($model->bank_name, ['/bank/view', 'id' => $model->bank_id], ['target' => '_blank', 'data-pjax' => 0]) : null;
                }, 
                'format' => 'raw',
                'filter' => empty($clientModel->id),
            ],
            [
                'attribute' => 'account',
                'filter' => empty($clientModel->id),         
            ],
            [
                'attribute' => 'name',
                'filter' => empty($clientModel->id),         
            ],
            [
                'attribute' => 'services_period_till',
                'value' => function ($model) {
                    return isset($model->services_period_till) ?date('d-M-Y', strtotime($model->services_period_till)) : null;
                }, 
                'filter' => empty($clientModel->id),         
            ],                          
            [
                'attribute' => 'uploaded_file_id',
                'value' => function ($model) {
                    return isset($model->uploaded_file_id) ? Html::a($model->uploadedFile->filename, $model->uploadedFile->fileurl, ['target' => '_blank', 'data-pjax' => 0]) : null;
                },          
                'format' => 'raw',
                'visible' => empty($clientModel->id),
            ],                          
            [
                'attribute' => 'balance',
                'value' => function ($model) {
                    return isset($model->balance) ? $model->balance.''.$model->currency : null;
                },          
                'visible' => empty($clientModel->id),
            ],                          
            
            [
                'attribute' => "deleted",                
                'class' => '\kartik\grid\BooleanColumn',
                'trueLabel' => 'Yes', 
                'falseLabel' => 'No',
                'width' => '100px',
                'visible' => $isAdmin && empty($clientModel->id),
            ],

            [
                'class' => '\common\components\FSMActionColumn',
                'headerOptions' => ['class'=>'td-mw-125'],
                'dropdown' => true,
                'viewOptions' => ['noTarget' => true],
                //'width' => '150px',
                'template' => '{view} {update} {delete}',
                'linkedObj' => [
                    ['fieldName' => 'client_id', 'id' => (!empty($clientModel->id) ? $clientModel->id : null)],
                ],
                'visible' => empty($clientModel->id),
            ],   
        ],
    ]); ?>
</div>