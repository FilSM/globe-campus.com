<?php
namespace common\models;

use Yii;

use kartik\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

use common\components\FSMAccessHelper;
use common\components\FSMHelper;
use common\models\client\ClientBankBalance;
use common\components\FSMExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel common\models\client\search\ClientBankSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('report', 'Bank statements');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-bank-index">

    <?= Html::pageHeader(Html::encode($this->title)); ?>
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <?php 
    $columns = [
        [
            'class' => '\kartik\grid\SerialColumn',
        ],
        [
            'attribute' => 'client_id',
            'headerOptions' => ['class'=>'td-mw-150'],
            'value' => function ($model) {
                return isset($model->client_id) ? Html::a($model->client_name, ['/client/view', 'id' => $model->client_id], ['target' => '_blank', 'data-pjax' => 0,]) : null;
            },                         
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $clientList,
            'filterWidgetOptions' => [
                'options' => [
                    'multiple' => true,
                ],
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => '...'],
            'format' => 'raw',
            'group' => true,
            'pageSummary' => 'Total',
        ],              
        [
            'attribute' => 'bank_id',
            'headerOptions' => ['class'=>'td-mw-150'],
            'value' => function ($model) {
                return isset($model->bank_id) ? Html::a($model->bank_name, ['/bank/view', 'id' => $model->bank_id], ['target' => '_blank', 'data-pjax' => 0,]) : null;
            },                         
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $bankList,
            'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
            'filterInputOptions' => ['placeholder' => '...'],
            'format' => 'raw',
            'group' => true,  // enable grouping
            'subGroupOf' => 1                        
        ],              
        'account',
        [
            //'attribute' => 'file_name_xml',
            'label' => Yii::t('common', 'Attachments'),
            'mergeHeader' => true,
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => [
                'style'=>'max-width:150px; min-height:100px; word-wrap: break-word; white-space: nowrap;'
            ],                
            'value' => function ($model) {
                $result = [];
                $file = $model->uploadedFile;
                $fileName = isset($file) ? 
                    '<span style="font-weight: bold;">XML: </span>'.Html::a($model->file_name_xml, $file->fileurl, ['target' => '_blank', 'data-pjax' => 0,]) : null;
                if($fileName){
                    $result[] = $fileName;
                }
                $file = $model->uploadedPdfFile;
                $fileName = isset($file) ? 
                    '<span style="font-weight: bold;">PDF: </span>'.Html::a($model->file_name_pdf, $file->fileurl, ['target' => '_blank', 'data-pjax' => 0,]) : null;
                if($fileName){
                    $result[] = $fileName;
                }
                return '<div style="overflow-x: auto;">'.implode('<br/>', $result).'</div>';
            },          
            'format' => 'raw',
            'hiddenFromExport' => true,
        ], 
        [
            'attribute' => 'start_date',
            'width' => '100px',
            'value' => function ($model) {
                return isset($model->start_date) ? date('d-M-Y', strtotime($model->start_date)) : '';
            },
        ],                          
        [
            'attribute' => 'end_date',
            'width' => '100px',
            'value' => function ($model) {
                return isset($model->end_date) ? date('d-M-Y', strtotime($model->end_date)) : '';
            },
        ],                            
        [
            'attribute' => "is_active",   
            'class' => '\kartik\grid\BooleanColumn',
            'vAlign' => 'middle',
            'width' => '75px',
            'trueLabel' => 'Yes', 
            'falseLabel' => 'No',
        ],                          
        [
            'hAlign' => 'center',
            'width' => '75px',
            'mergeHeader' => true,
            'value' => function ($model) {
                return Html::a(Html::icon('th-list').' '.Yii::t('report', 'Details'), 
                    [
                        '/client-bank-balance/report', 
                        'account_id' => $model['id'], 
                    ], 
                    ['target' => '_blank', 'data-pjax' => 0,]);
            },
            'format'=>'raw',                           
            'hiddenFromExport' => true,
        ],                         
        [
            'attribute' => 'balance',
            'width' => '150px',
            'hAlign' => 'right',
            'value' => function ($model) {
                return isset($model->balance) ? $model->balance : 0;
            },
            'xlFormat' => '###0.00',
            'exportMenuStyle' => [
                'numberFormat' => ['formatCode' => '###0.00'],
            ],                                                
            'format' => ['decimal', 2],
            'pageSummary' => true,
        ],                          
        [
            'attribute' => 'valuta_id',
            'width' => '75px',
            'value' => function ($model) {
                return isset($model->currency) ? $model->currency : '';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $valutaList,
            'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
            'filterInputOptions' => ['placeholder' => '...'],                      
        ],                          
        [
            //'attribute' => 'home_page',
            'label' => Yii::t('common', 'WWW'),
            'width' => '75px',
            'hAlign' => 'center',
            'mergeHeader' => true,
            'value' => function ($model) {
                return !empty($model->home_page) ? Html::a(Yii::t('bank', 'Connect'), $model->home_page, ['target' => '_blank', 'data-pjax' => 0,]) : '';
            }, 
            'format' => 'raw',
            'hiddenFromExport' => true,
        ],            
    ]; ?>

    <?php if(FSMAccessHelper::can('createClientBankBalance')) :
        echo FSMHelper::vButton(null, [
            'label' => ClientBankBalance::modelTitle(),
            'title' => Yii::t('client', 'Add new bank account balance'),
            'controller' => 'client-bank-balance',
            'action' => 'create',
            'class' => 'success',
            'icon' => 'plus',
            'modal' => true,
            'options' => [
                'data-pjax' => 0,
            ]            
        ]);     
    endif;
    ?>

    <?= FSMExportMenu::widget([
        'options' => ['id' => 'client-bank-index-menu'],
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'showColumnSelector' => false,
        'showConfirmAlert' => false,
        'dropdownOptions' => [
            'label' => Yii::t('kvgrid', 'Export'),
            'class' => 'btn btn-default'
        ],
        'target' => ExportMenu::TARGET_SELF,
        'exportConfig' => [
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_HTML => false,
            ExportMenu::FORMAT_CSV => false,
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_PDF => false,
            ExportMenu::FORMAT_EXCEL => false,
            ExportMenu::FORMAT_EXCEL_X => ['label' => Yii::t('kvgrid', 'Excel'), 'alertMsg' => ''],
        ],
        'pjaxContainerId' => 'client-bank-index',
        'exportFormView' => '@vendor/kartik-v/yii2-export/views/_form',
    ]);?> 
        
    <p/>
    
    <?= GridView::widget([
        'id' => 'grid-view',
        'tableOptions' => [
            'id' => 'client-bank-list',
        ],
        'responsive' => false,
        'striped' => true,
        'hover' => true,
        'bordered' => true,
        'condensed' => true,
        'persistResize' => false,
        'floatHeader' => true,
        'autoXlFormat' => true,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showPageSummary' => true,
        'pjax' => true,
        'columns' => $columns,
    ]);
    ?>
</div>