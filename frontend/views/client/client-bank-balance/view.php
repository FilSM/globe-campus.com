<?php

use kartik\helpers\Html;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\client\ClientBankBalance */

$this->title = $model->modelTitle() .' #'. $model->id;
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$attachmentList = [];
$fileName = isset($model->uploaded_file_id) ? 
    '<span style="font-weight: bold;">XML: </span>'.Html::a($model->fileXml->filename, $model->uploadedFile->fileurl, ['target' => '_blank',]) : null;
if($fileName){
    $attachmentList[] = $fileName;
}
$fileName = isset($model->uploaded_pdf_file_id) ? 
    '<span style="font-weight: bold;">PDF: </span>'.Html::a($model->filePdf->filename, $model->uploadedPdfFile->fileurl, ['target' => '_blank',]) : null;
if($fileName){
    $attachmentList[] = $fileName;
}
$attachmentList = implode('<br/>', $attachmentList);
 
?>
<div class="client-bank-balance-view">

    <?= Html::pageHeader(Html::encode($this->title)); ?>

    <p>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= \common\components\FSMBtnDialog::button(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'id' => 'btn-dialog-selected',
            'class' => 'btn btn-danger',
        ]); ?>            
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'payment_confirm_id',
                'value' => (!empty($model->payment_confirm_id) ? 
                    Html::a($model->paymentConfirm->name, ['/payment-confirm/view', 'id' => $model->payment_confirm_id], ['target' => '_blank', 'data-pjax' => 0]) : 
                    null),
                'format' => 'raw',
            ],
            [
                'attribute' => 'account_id',
                'value' => $model->account->bank->name . ' | ' . $model->account->account . (!empty($model->account->name) ? ' ( '.$model->account->name . ' )' : ''),
            ],
            [
                'attribute' => 'start_date',
                'value' => isset($model->start_date) ? date('d-M-Y', strtotime($model->start_date)) : null,
            ],                          
            [
                'attribute' => 'end_date',
                'value' => isset($model->end_date) ? date('d-M-Y', strtotime($model->end_date)) : null,
            ],
            [
                'label' => Yii::t('common', 'Attachments'),
                'value' => $attachmentList,          
                'format' => 'raw',
            ],            
            [
                'attribute' => 'balance',
                'value' => isset($model->balance) ? number_format($model->balance, 2).(isset($model->currency) ? ' '.$model->currency : '') : null,
            ], 
        ],
    ]) ?>

</div>