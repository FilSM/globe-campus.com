<?php
namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\widgets\MaskedInput;

use kartik\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $model common\models\client\ClientBankBalance */
/* @var $form yii\widgets\ActiveForm */

Icon::map($this);
$isModal = !empty($isModal);
?>

<div class="client-bank-balance-form">
    <?php if($isModal) : Pjax::begin(Yii::$app->params['PjaxModalOptions']); endif; ?>
    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => $model->tableName().'-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'options' => [
            'data-pjax' => $isModal,
        ],        
    ]); ?>

    <?php /* $form->field($model, 'payment_confirm_id')->textInput()*/ ?>
    
    <?= $form->field($accountModel, 'client_id')->widget(Select2::class, [
            'data' => $clientList,
            'options' => [
                //'id' => 'client-id', 
                'placeholder' => '...',
            ],   
            'pluginOptions' => [
                'allowClear' => true,
                'dropdownParent' => !empty($isModal) ? new \yii\web\JsExpression('$("#modalContent")') : null,
            ],            
            'addon' => [
                'prepend' => $model::getModalButtonContent([
                    'formId' => $form->id,
                    'controller' => 'client',
                ]),
            ],        
        ]); 
    ?>
    
    <?= $form->field($model, 'account_id')->widget(DepDrop::class, [
        'type' => DepDrop::TYPE_SELECT2,
        'data' => empty($model->account_id) ? null : 
            [$model->account_id => $model->account->bank->name . ' | ' . $model->account->account . (!empty($model->account->name) ? ' ( '.$model->account->name . ' )' : '')],
        'select2Options' => [
            'pluginOptions' => [
                'allowClear' => false,
            ],
        ],
        'pluginOptions' => [
            'depends' => ['client-id'],
            'initDepends' => ['client-id'],
            'initialize' => true,            
            'url' => Url::to(['/client/ajax-get-client-account-list', 'selected' => $model->account_id]),
            'placeholder' => '...',
        ],
        'pluginEvents' => [
            "depdrop:afterChange" => "function() {
                var form = $('#{$model->tableName()}' + '-form');
                checkRequiredInputs(form);
            }",
        ],        
    ]); ?>
    
    <?= $form->field($model, 'start_date')->widget(DatePicker::class, [
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
        ]); 
    ?>

    <?= $form->field($model, 'end_date')->widget(DatePicker::class, [
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
        ]); 
    ?>

    <?php 
        $preview = $previewConfig = [];
        if(!empty($filesXMLModel->id)){
            $preview[] = $filesXMLModel->uploadedFileUrl;
            $previewConfig[] = [
                'type' => 'xml',
                'size' => $filesXMLModel->filesize, 
                'caption' => $filesXMLModel->filename, 
                'url' => Url::to(['/client-bank-balance/xml-delete', 'deleted_id' => $filesXMLModel->id]), 
                'key' => 101,
                'downloadUrl' => $filesXMLModel->uploadedFileUrl,
            ];
        }
        echo $form->field($filesXMLModel, 'file'/*, [
            'options' => [
                //'class' => 'required form-group',
            ],
        ]*/)->widget(FileInput::class, [
            'language' =>  strtolower(substr(Yii::$app->language, 0, 2)),
            'sortThumbs' => false,
            'options' => [
                'id' => 'xml-file',
            ],
            'pluginOptions' => [
                'allowedFileExtensions' => ['xml'],
                'uploadAsync' => false,
                'maxFileSize' => 20000,
                'showRemove' => false,
                'showUpload' => false,
                
                'overwriteInitial' => false,
                //'initialPreviewAsData' => true,
                'initialPreviewFileType' => 'xml',
                'initialPreview' => $preview,
                'initialPreviewConfig' => $previewConfig,
                'initialPreviewShowDelete' => true,            
                'preferIconicPreview' => true, // this will force thumbnails to display icons for following file extensions
                'previewFileIcon' => Icon::show('file'),
                'allowedPreviewTypes' => null, // set to empty, null or false to disable preview for all types                
                'previewFileIconSettings' => [ // configure your icon file extensions
                    'xml' => Icon::show('file-pdf', ['class' => 'text-danger']),
                ],  
            ],
        ])->label(Yii::t('bill', 'XML filename to import')); 
    ?>
   
    <?php 
        $preview = $previewConfig = [];
        if(!empty($filesPDFModel->id)){
            $preview[] = $filesPDFModel->uploadedFileUrl;
            $previewConfig[] = [
                'type' => 'pdf',
                'size' => $filesPDFModel->filesize, 
                'caption' => $filesPDFModel->filename, 
                'url' => Url::to(['/client-bank-balance/pdf-delete', 'deleted_id' => $filesPDFModel->id]), 
                'key' => 102,
                'downloadUrl' => $filesPDFModel->uploadedFileUrl,
            ];
        }
        echo $form->field($filesPDFModel, 'file'/*, [
            'options' => [
                //'class' => 'required form-group',
            ],
        ]*/)->widget(FileInput::class, [
            'language' =>  strtolower(substr(Yii::$app->language, 0, 2)),
            'sortThumbs' => false,
            'options' => [
                'id' => 'pdf-file',
            ],
            'pluginOptions' => [
                'allowedFileExtensions' => ['pdf'],
                'uploadAsync' => false,
                'maxFileSize' => 20000,
                'showRemove' => false,
                'showUpload' => false,
                'overwriteInitial' => false,
                'initialPreview' => $preview,
                'initialPreviewAsData' => true,
                'initialPreviewFileType' => 'pdf',
                'initialPreviewConfig' => $previewConfig,
                'initialPreviewShowDelete' => true,            
                
                'preferIconicPreview' => true, // this will force thumbnails to display icons for following file extensions
                'previewFileIcon' => Icon::show('file'),
                'allowedPreviewTypes' => null, // set to empty, null or false to disable preview for all types                
                'previewFileIconSettings' => [ // configure your icon file extensions
                    'pdf' => Icon::show('file-pdf', ['class' => 'text-danger']),
                ],                
            ],
        ])->label(Yii::t('bill', 'PDF filename to import')); 
    ?>

    <?= $form->field($model, 'balance', [
        'addon' => [
            'append' => [
                'content' => Select2::widget([
                    'name' => 'valuta_id',
                    'data' => $valutaList, 
                    'value' => $model->valuta_id,
                    'options' => [
                        'id' => 'valuta-id', 
                        'placeholder' => '...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => false,
                    ],            
                    'size' => 'control-width-90',
                ]),
                'asButton' => true,
            ],
        ],
    ])->widget(MaskedInput::class, [
        //'mask' => '9{1,10}[.9{1,2}]',
        'options' => [
            'class' => 'form-control number-field',
            //'readonly' => true,
        ],
        'clientOptions' => [
            'alias' => 'decimal',
            'rightAlign' => false,
            'digits' => 2,
            'allowMinus' => false,
        ],                                    
    ])->label($model->getAttributeLabel('balance').' / '.Yii::t('common', 'Currency'));
    ?>
    
    <div class="form-group <?php if($isModal) : echo 'modal-button-group'; endif; ?>">
        <div class="col-md-offset-2 col-md-10" style="text-align: right;">
            <?= $model->SubmitButton; ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <?php if($isModal) : Pjax::end(); endif; ?>

</div>