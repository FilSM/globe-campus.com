<?php
namespace common\models;

use Yii;

use kartik\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\client\search\ClientBankBalanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $searchModel->modelTitle().': '.$searchModel->account->account;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-bank-balance-index">

    <?= Html::pageHeader(
        Html::tag('small', Yii::t('client', 'Client').': '.
            Html::a($searchModel->account->client->name, ['/client/view', 'id' => $searchModel->account->client_id], ['target' => '_blank', 'data-pjax' => 0])
        ).'<br/>'.
        Html::tag('small', Yii::t('bill', 'Bank').': '.
            Html::a($searchModel->account->bank->name, ['/bank/view', 'id' => $searchModel->account->bank_id], ['target' => '_blank', 'data-pjax' => 0])
        ).'<br/>'.
        Html::tag('small', Html::encode(Yii::t('client', 'Account').': '.$searchModel->account->account))
    ); ?>
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    $columns = [
        [
            'class' => '\kartik\grid\SerialColumn',
            'visible' => !$isAdmin,
        ],
        [
            'attribute' => 'id',
            'width' => '75px',
            'hAlign' => 'center',
            'visible' => $isAdmin,
        ],
        [
            'attribute' => 'payment_confirm_id',
            'value' => function ($model) {
                return !empty($model->payment_confirm_id) ? Html::a($model->paymentConfirm->name, ['/payment-confirm/view', 'id' => $model->payment_confirm_id], ['target' => '_blank', 'data-pjax' => 0]) : null;
            }, 
            'format' => 'raw',
        ],                      
        [
            'attribute' => 'start_date',
            'width' => '100px',
            'value' => function ($model) {
                return isset($model->start_date) ? date('d-M-Y', strtotime($model->start_date)) : null;
            },
        ],                          
        [
            'attribute' => 'end_date',
            'width' => '100px',
            'value' => function ($model) {
                return isset($model->end_date) ? date('d-M-Y', strtotime($model->end_date)) : null;
            },
        ], 
        [
            'attribute' => 'balance',
            'width' => '150px',
            'hAlign' => 'right',
            'value' => function ($model) {
                return isset($model->balance) ? $model->balance : null;
            },
            'format' => ['decimal', 2],
        ],                          
        [
            'attribute' => 'valuta_id',
            'width' => '100px',
            'value' => function ($model) {
                return isset($model->currency) ? $model->currency : null;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $valutaList,
            'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
            'filterInputOptions' => ['placeholder' => '...'],                    
        ], 
        [
            'label' => Yii::t('common', 'Attachments'),
            'mergeHeader' => true,
            'headerOptions' => ['style' => 'text-align: center;'],
            'contentOptions' => ['style' => 'white-space: nowrap;'],
            'value' => function ($model) {
                $result = [];
                $fileName = isset($model->uploaded_file_id) ? 
                    '<span style="font-weight: bold;">XML: </span>'.Html::a($model->file_name_xml, $model->uploadedFile->fileurl, ['target' => '_blank', 'data-pjax' => 0,]) : null;
                if($fileName){
                    $result[] = $fileName;
                }
                $fileName = isset($model->uploaded_pdf_file_id) ? 
                    '<span style="font-weight: bold;">PDF: </span>'.Html::a($model->file_name_pdf, $model->uploadedPdfFile->fileurl, ['target' => '_blank', 'data-pjax' => 0,]) : null;
                if($fileName){
                    $result[] = $fileName;
                }
                return implode('<br/>', $result);
            },          
            'format' => 'raw',
        ],                        

        [
            'class' => '\common\components\FSMActionColumn',
            'headerOptions' => ['class'=>'td-mw-125'],
            'dropdown' => true,
            'checkPermission' => true,
            //'visible' => $isAdmin,
        ], 
    ];?>
    
    <?= GridView::widget([
        'responsive' => false,
        //'striped' => false,
        'hover' => true,
        'floatHeader' => true,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]); ?>
</div>