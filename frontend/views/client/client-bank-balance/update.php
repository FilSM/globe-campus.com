<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\client\ClientBankBalance */

$this->title = Yii::t($model->tableName(), 'Update a '.$model->modelTitle(1, false)) . ': ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Edit');
?>
<div class="client-bank-balance-update">

    <?= Html::pageHeader(Html::encode($this->title)); ?>

    <?= $this->render('_form', [
        'model' => $model,
        'accountModel' => $accountModel,
        'filesXMLModel' => $filesXMLModel,
        'filesPDFModel' => $filesPDFModel,
        'clientList' => $clientList,
        'bankList' => $bankList,
        'valutaList' => $valutaList,
        'isAdmin' => $isAdmin,
        'isModal' => $isModal,
    ]) ?>

</div>