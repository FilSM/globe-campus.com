<?php
namespace common\models;

use Yii;
use yii\helpers\Url;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\client\ClientMailTemplate */
/* @var $form yii\widgets\ActiveForm */
$isModal = !empty($isModal) ? 1 : 0;
?>

<div class="mail-template-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => $model->tableName().'-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>

    <?php if(!empty($model->client_id)){
            echo Html::activeHiddenInput($model, 'client_id');
        }else{
            echo $form->field($model, 'client_id')->textInput();
        }
    ?>

    <?= $form->field($model, 'cc')->textInput(['maxlength' => true])->hint(Yii::t('client', 'Enter CC addresses, separating them with a semicolon')) ?>

    <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'header')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'body')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'footer')->textarea(['rows' => 3]) ?>

    <?php 
        $preview = !empty($filesModel->uploadedFileUrl) ? [$filesModel->uploadedFileUrl] : [];
        echo $form->field($filesModel, 'file')->widget(FileInput::class, [
            'language' =>  strtolower(substr(Yii::$app->language, 0, 2)),
            'sortThumbs' => false,
            'options' => [
                'id' => 'client-files',
                //'multiple' => true,
            ],
            'pluginOptions' => [
                'allowedFileExtensions' => ['png', 'jpg', 'jpeg', 'gif'],
                'maxFileSize' => 5000,
                'maxFileCount' => 1,
                'showRemove' => false,
                'showUpload' => false,
                //'showCaption' => false,
                //'browseClass' => 'btn btn-primary btn-block',
                'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                'initialPreview' => $preview,
                'initialPreviewShowDelete' => true,                
                'initialPreviewAsData' => true,
                'initialPreviewConfig' => [
                    [
                        'type' => "image",
                        'size' => $filesModel->filesize, 
                        'caption' => $filesModel->filename, 
                        'url' => Url::to(['/mail-template/attachment-delete', 'deleted_id' => $filesModel->id]), 
                        //'key' => 101,
                    ],
                ],
                'overwriteInitial' => false,
            ],
        ])->label(Yii::t('files', 'Logo')); 
    ?>

    <?= $form->field($model, 'contacts')->textarea(['rows' => 3]) ?>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-10" style="text-align: right;">
            <?= $model->SubmitButton; ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>