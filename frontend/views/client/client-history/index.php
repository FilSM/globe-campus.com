<?php
namespace common\models;

use kartik\helpers\Html;
use kartik\grid\GridView;

use common\components\FSMAccessHelper;
/* @var $this yii\web\View */
/* @var $searchModel common\models\client\search\ClientHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = $searchModel->modelTitle(2);
$this->params['breadcrumbs'][] = ['label' => $clientModel->modelTitle(2), 'url' => ['client/index']];
$this->params['breadcrumbs'][] = ['label' => $clientModel->name, 'url' => ['client/view', 'id' => $clientModel->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row client-history-index">

    <div class="col-md-2">
        <?= $this->render('@frontend/views/client/client/_menu', [
            'client' => $clientModel,
            'activeItem' => $searchModel->tableName(),
        ])
        ?>
    </div>
    
    <div class="col-md-10">

        <?php /*= Html::pageHeader(Html::encode($this->title)); */?>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?php 
            $body = '';
            if(FSMAccessHelper::can('updateClient', $clientModel)):
                $body .= 
                    '<p>'.
                    Html::a(Html::icon('plus').'&nbsp;'.$searchModel->modelTitle(), ['create', 'client_id' => $clientModel->id], ['class' => 'btn btn-success']).
                    '</p>';
            endif;
            
            $body .= GridView::widget([
                'responsive' => false,
                //'striped' => false,
                'hover' => true,
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'floatHeader' => true,
                'columns' => [
                    //['class' => '\kartik\grid\SerialColumn'],

                    [
                        'attribute' => 'id',
                        'width' => '75px',
                        'hAlign' => 'center',
                    ],
                    //'client_id',
                    'history_date',
                    'history_text',
                    [
                        'attribute' => 'create_time',
                        'width' => '150px',
                        'headerOptions' => ['class'=>'td-mw-100'],
                        'value' => function ($model) {
                            return isset($model->create_time) ? date('d-M-Y H:i:s', strtotime($model->create_time)) : null;
                        },                         
                    ],
                    [
                        'attribute' => 'create_user_id',
                        'headerOptions' => ['class'=>'td-mw-100'],
                        'value' => function ($model) {
                            return isset($model->create_user_id) ? Html::a($model->user_name, ['/user/profile/show', 'id' => $model->create_user_id], ['target' => '_blank', 'data-pjax' => 0]) : null;
                        }, 
                        'format' => 'raw',  
                    ],

                    [
                        'class' => '\common\components\FSMActionColumn',
                        'headerOptions' => ['class' => 'td-mw-150'],
                        'dropdown' => true,
                        'checkCanDo' => true,
                        'template' => '{view} {update} {delete}',
                        'linkedObj' => [
                            ['fieldName' => 'client_id', 'id' => (!empty($clientModel->id) ? $clientModel->id : null)],
                        ],
                    ],                        
                ],
            ]); 

            $panelContent = [
                'heading' => Html::encode($this->title),
                'preBody' => '<div class="panel-body">',
                'body' => $body,
                'postBody' => '</div>',
            ];
            echo Html::panel(
                $panelContent, 
                'primary', 
                [
                    'id' => "panel-client-contact-data",
                ]
            );        
        ?>
    </div>
</div>