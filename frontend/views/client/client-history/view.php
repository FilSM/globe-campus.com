<?php

use kartik\helpers\Html;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\client\ClientHistory */

$this->title = $model->modelTitle();
$this->params['breadcrumbs'][] = ['label' => $clientModel->modelTitle(2), 'url' => ['client/index']];
$this->params['breadcrumbs'][] = ['label' => $clientModel->name, 'url' => ['client/view', 'id' => $clientModel->id]];
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index', 'client_id' => $clientModel->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row client-history-view">
    <div class="col-md-2">
        <?= $this->render('@frontend/views/client/client/_menu', [
            'client' => $clientModel,
            'activeItem' => $model->tableName(),
        ])
        ?>
    </div>
    
    <div class="col-md-10">

        <?php /*= Html::pageHeader(Html::encode($this->title)); */?>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?php 
            $body = '<p>'.
                $model->getBackButton().'&nbsp;'.
                Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id, 'client_id' => $clientModel->id], ['class' => 'btn btn-primary']).'&nbsp;'.
                \common\components\FSMBtnDialog::button(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id, 'client_id' => $clientModel->id], [
                    'id' => 'btn-dialog-selected',
                    'class' => 'btn btn-danger',
                ]).
                '</p>';
            $body .= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    [
                        'attribute' => 'history_date',
                        'width' => '150px',
                        'headerOptions' => ['class'=>'td-mw-100'],
                        'value' => function ($model) {
                            return isset($model->history_date) ? date('d-M-Y', strtotime($model->history_date)) : null;
                        },                         
                    ],
                    'history_text:text',
                    [
                        'attribute' => 'create_time',
                        'width' => '150px',
                        'headerOptions' => ['class'=>'td-mw-100'],
                        'value' => function ($model) {
                            return isset($model->create_time) ? date('d-M-Y H:i:s', strtotime($model->create_time)) : null;
                        },                         
                    ],
                    [
                        'attribute' => 'create_user_id',
                        'headerOptions' => ['class'=>'td-mw-100'],
                        'value' => function ($model) {
                            return isset($model->create_user_id) ? Html::a($model->user_name, ['/user/profile/show', 'id' => $model->create_user_id], ['target' => '_blank', 'data-pjax' => 0]) : null;
                        },          
                        'format' => 'raw',
                    ],
                ],
            ]); 

            $panelContent = [
                'heading' => Html::encode($this->title),
                'preBody' => '<div class="panel-body">',
                'body' => $body,
                'postBody' => '</div>',
            ];
            echo Html::panel(
                $panelContent, 
                'primary', 
                [
                    'id' => "panel-client-contact-data",
                ]
            );        
        ?>
    </div>

</div>