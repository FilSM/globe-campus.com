<?php
namespace common\models;

use Yii;
use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\client\ClientHistory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-history-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => $model->tableName().'-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>

    <?php if(!empty($model->client_id)){
            echo Html::activeHiddenInput($model, 'client_id');
        }else{
            echo $form->field($model, 'client_id')->textInput();
        }
    ?>

    <?= $form->field($model, 'history_date')->widget(DatePicker::class, [
        'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
    ]); 
    ?>       
    
    <?= $form->field($model, 'history_text')->textarea(['rows' => 3]) ?>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-10" style="text-align: right;">
            <?= $model->SubmitButton; ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>