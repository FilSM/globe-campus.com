<?php

use yii\bootstrap\Alert;

/**
 * @var yii\web\View $this
 * @var dektrium\user\Module $module
 */

$this->title = $title;
?>

<?= $this->render('/_alert', [
    'module' => $module,
]) ?>
