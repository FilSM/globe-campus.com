<?php
namespace common\models;

use Yii;
//use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
//use yii\grid\GridView;

use kartik\helpers\Html;
use kartik\grid\GridView;

use common\components\FSMHelper;
use common\components\FSMAccessHelper;
use common\components\FSMHtml;
use common\models\bill\Expense;

/* @var $this yii\web\View */
/* @var $searchModel common\models\bill\search\ExpenseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

ob_start();
ob_implicit_flush(false);
?>

<div class="expense-index">

    <div id="page-content">
        
        <?php
            $columns = [
                //['class' => '\kartik\grid\SerialColumn'],
                [
                    'attribute' => 'id',
                    'width' => '75px',
                    'hAlign' => 'center',
                ],
                [
                    'attribute'=>'doc_number',
                    'width' => '150px',
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'value' => function ($model) {
                        return !empty($model->doc_number) ? 
                            (FSMAccessHelper::can('viewExpense', $model) ?
                                Html::a($model->doc_number, ['/expense/view', 'id' => $model->id], ['target' => '_blank', 'data-pjax' => 0]) :
                                $model->doc_number
                            )
                            : null;
                    },                         
                    'format'=>'raw',                        
                ],
                [
                    'attribute' => 'expense_type_id',
                    'headerOptions' => ['class'=>'td-mw-150'],
                    'value' => function ($model) {
                        return isset($model->expense_type_id) ? $model->expense_type_name : null;
                    },
                ],            
                [
                    'attribute'=>'project_id',
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'value' => function ($model) {
                        return !empty($model->project_id) ? Html::a($model->project_name, ['/project/view', 'id' => $model->project_id], ['target' => '_blank', 'data-pjax' => 0]) : null;
                    },                         
                    'format'=>'raw',
                    'visible' => FSMAccessHelper::can('createExpense'),
                ],                           
                [
                    'attribute'=>'first_client_id',
                    'contentOptions' => [
                        'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                    ],
                    'value' => function ($model) {
                        return  
                            '<div style="overflow-x: auto;">'.
                            (!empty($model->first_client_id) ?  
                            Html::a($model->first_client_name, ['/client/view', 'id' => $model->first_client_id], ['target' => '_blank', 'data-pjax' => 0]) : null).
                            '</div>';
                    },   
                    'format'=>'raw',
                ],                          
                [
                    'attribute'=>'second_client_id',
                    'contentOptions' => [
                        'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                    ],
                    'value' => function ($model) {
                        return  
                            '<div style="overflow-x: auto;">'.
                            (!empty($model->second_client_id) ?  
                            Html::a($model->second_client_name, ['/client/view', 'id' => $model->second_client_id], ['target' => '_blank', 'data-pjax' => 0]) : null).
                            '</div>';
                    },   
                    'format'=>'raw',
                ],                          
                [
                    'attribute' => 'doc_date',
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'value' => function ($model) {
                        return isset($model->doc_date) ? date('d-M-Y', strtotime($model->doc_date)) : null;
                    },
                    'format'=>'raw',                           
                ],                          
                [
                    'attribute' => 'summa',
                    'hAlign' => 'right',
                    'value' => function ($model) {
                        return isset($model->summa) ? number_format($model->summa, 2, '.', ' ') : null;
                    },
                ],                          
                [
                    'attribute' => 'vat',
                    'hAlign' => 'right',
                    'value' => function ($model) {
                        return isset($model->vat) ? number_format($model->vat, 2, '.', ' ') : null;
                    },
                ],                          
                [
                    'attribute' => 'total',
                    'hAlign' => 'right',
                    'value' => function ($model) {
                        return isset($model->valuta_id) ? number_format($model->total, 2, '.', ' ') . ' ' . $model->valuta->name : number_format($model->total, 2, '.', ' ');
                    },
                ],                      
                [
                    'attribute' => "report_plus",   
                    'vAlign' => 'middle',
                    'class' => '\kartik\grid\BooleanColumn',
                    'trueLabel' => 'Yes', 
                    'falseLabel' => 'No',
                    'width' => '100px',
                    'visible' => FSMAccessHelper::can('changeExpenseEbitda'),
                ],   
                [
                    'class' => '\common\components\FSMActionColumn',
                    'headerOptions' => ['class'=>'td-mw-125'],
                    'dropdown' => true,
                    'checkPermission' => true,
                    'template' => '{attachment} {view} {copy} {update} {delete}',
                    'buttons' => [
                        'attachment' => function (array $params) { 
                            return Expense::getButtonAttachment($params);
                        },                            
                        'copy' => function (array $params) { 
                            return Expense::getButtonCopy($params);
                        },
                    ],                
                    'controller' => 'expense',
                ],                            
            ];
        ?>

        <?= GridView::widget([
            'id' => 'expense-grid-view',
            'tableOptions' => [
                'id' => 'expense-list',
            ],
            'responsive' => false,
            'condensed' => true,
            //'striped' => false,
            'hover' => true,
            'dataProvider' => $dataProvider,
            'filterModel' => null,
            'columns' => $columns,
            'pjax' => true,
            'floatHeader' => true,
            'export' => false,
        ]);
        ?>

    </div>    
</div>


<?php
    $body = ob_get_contents();
    ob_get_clean(); 

    $panelContent = [
        'heading' => Expense::modelTitle(2),
        'preBody' => '<div class="panel-body">',
        'body' => $body,
        'postBody' => '</div>',
    ];
    echo FSMHtml::panel(
        $panelContent, 
        'success', 
        [
            'id' => "panel-expense-data",
        ]
    );
?>