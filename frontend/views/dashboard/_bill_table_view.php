<?php
namespace common\models;

use Yii;
//use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
//use yii\grid\GridView;

use kartik\helpers\Html;
use kartik\grid\GridView;

use common\components\FSMHelper;
use common\components\FSMAccessHelper;
use common\components\FSMHtml;
use common\models\bill\Bill;

/* @var $this yii\web\View */
/* @var $searchModel common\models\bill\search\BillSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

ob_start();
ob_implicit_flush(false);

?>

<div id="bill-index">

    <div id="page-content">
        
        <?php
            $columns = [
                [
                    'attribute' => 'id',
                    'width' => '75px',
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                ],
                [
                    'attribute'=>'doc_number',
                    'width' => '150px',
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'value' => function ($model) {
                        return !empty($model->doc_number) ? 
                            (isset($model->doc_type) ? 
                                $model->billDocTypeList[$model->doc_type].'<br/>' : null).
                                (FSMAccessHelper::can('viewBill', $model) ?
                                    Html::a($model->doc_number, ['/bill/view', 'id' => $model->id], ['target' => '_blank', 'data-pjax' => 0]) :
                                    $model->doc_number
                                )
                            : null;
                    },                         
                    'format'=>'raw',
                ],                      
                [
                    'attribute' => 'status',
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'value' => function ($model) {
                        return $model->statusHTML;
                    },
                    'format'=>'raw',
                ],                         
                [
                    'header' => Yii::t('common', 'Progress'),
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'mergeHeader' => true,
                    'value' => function ($model) {
                        return in_array($model->doc_type, [
                            Bill::BILL_DOC_TYPE_AVANS,
                            Bill::BILL_DOC_TYPE_BILL,
                            Bill::BILL_DOC_TYPE_INVOICE,
                            Bill::BILL_DOC_TYPE_CESSION,
                            ]) ? $model->getProgressButtons() : '&nbsp;';
                    },
                    'format' => 'raw',
                    'visible' => FSMAccessHelper::can('changeBillStatus'),
                ],                        
                [
                    'attribute'=>'first_client_id',
                    //'headerOptions' => ['class'=>'td-mw-100'],
                    'contentOptions' => [
                        'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                    ],
                    'value' => function ($model) {
                        return 
                            '<div style="overflow-x: auto;">'.
                            (!empty($model->first_client_name) ? 
                            (!empty($model->first_client_role_name) ? $model->first_client_role_name.'<br/>' : '') . 
                                (FSMAccessHelper::can('viewClient', $model->firstClient) ?
                                    Html::a($model->first_client_name, ['/client/view', 'id' => $model->first_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                    $model->first_client_name
                                )
                            : null).
                            '</div>';
                    },
                    'format'=>'raw',
                ],                          
                [
                    'attribute'=>'second_client_id',
                    //'headerOptions' => ['class'=>'td-mw-100'],
                    'contentOptions' => [
                        'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                    ],
                    'value' => function ($model) {
                        return  
                            '<div style="overflow-x: auto;">'.
                            (!empty($model->second_client_name) ?  
                            (!empty($model->second_client_role_name) ? $model->second_client_role_name.'<br/>' : '') . 
                                (FSMAccessHelper::can('viewClient', $model->secondClient) ?
                                    Html::a($model->second_client_name, ['/client/view', 'id' => $model->second_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                    $model->second_client_name
                                )
                            : null).
                            '</div>';
                    },
                    'format'=>'raw',
                ],       
                [
                    'attribute' => 'total',
                    'hAlign' => 'right',
                    'vAlign' => 'middle',
                    'mergeHeader' => true,
                    'headerOptions' => ['style'=>'text-align: center;'],
                    //'label' => Yii::t('bill', 'Sum'),
                    'value' => function ($model) {
                        return isset($model->valuta_id) ? number_format($model->total, 2, '.', ' ') . ' ' . $model->valuta->name : $model->total;
                    },
                ],                         
                [
                    'attribute' => 'doc_date',
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'headerOptions' => ['class'=>'td-mw-75'],
                    //'label' => Yii::t('bill', 'Sum'),
                    'value' => function ($model) {
                        return isset($model->doc_date) && ($model->doc_date == date('Y-m-d')) ? 
                            Html::a(Html::badge(Yii::t('common', 'Today'), ['class' => 'badge-success']), 
                                Url::to(['/bill/index']).'?BillSearch[doc_date]='.$model->doc_date, 
                                ['target' => '_blank', 'data-pjax' => 0]) 
                            : date('d-M-Y', strtotime($model->doc_date));
                    },
                    'format'=>'raw',        
                ],                         
                [
                    'attribute' => 'pay_date',
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'headerOptions' => ['class'=>'td-mw-75'],
                    'value' => function ($model) {
                        $class = 'badge-success';
                        if(in_array($model->status, [
                            Bill::BILL_STATUS_CANCELED,
                            Bill::BILL_STATUS_COMPLETE,
                            ]))
                        {
                            $badge = '';
                        }else{
                            $firstDate = new \DateTime(date('Y-m-d'));
                            $secondDate = new \DateTime($model->pay_date);
                            $dateDiff = date_diff($firstDate, $secondDate, true)->days;

                            $firstDate = $firstDate->format('Y-m-d');
                            $secondDate = $secondDate->format('Y-m-d');
                            if($firstDate > $secondDate){
                                $dateDiff = -1 * $dateDiff;
                                $class = 'badge-danger';
                            }elseif($dateDiff >= 0){
                                $class = 'badge-success';
                            }

                            $badge = isset($model->pay_date) && ($model->pay_date == date('Y-m-d')) ? 
                                '' : Html::badge($dateDiff, ['class' => $class]);
                        }
                        return isset($model->pay_date) && ($model->pay_date == date('Y-m-d')) ? 
                            Html::a(Html::badge(Yii::t('common', 'Today'), ['class' => $class]) , 
                                Url::to(['/bill/index']).'?BillSearch[pay_date]='.$model->pay_date, 
                                ['target' => '_blank', 'data-pjax' => 0]) 
                            : date('d-M-Y', strtotime($model->pay_date)).(!empty($badge) ? '<br/>' : '').$badge;
                    },
                    'format'=>'raw',
                ],
                [
                    'attribute' => 'mail_status',
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'value' => function ($model) {
                        return $model->mailStatusHTML;
                    },
                    'format'=>'raw',
                ],                         
                [
                    'attribute' => 'transfer_status',
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'value' => function ($model) {
                        return $model->transferStatusHTML;
                    },
                    'format'=>'raw',
		    'visible' => !empty(Yii::$app->params['ENABLE_BILL_TRANSFER']),
                ],                         
                [
                    'attribute' => 'paid_date',
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'headerOptions' => ['class'=>'td-mw-75'],
                    'value' => function ($model) {
                        return isset($model->paid_date) ? date('d-M-Y', strtotime($model->paid_date)) : null;
                    },
                ],                                          
                [
                    'attribute' => 'pay_status',
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'value' => function ($model) {
                        return $model->payStatusHTML;
                    },
                    'format'=>'raw',
                ],          
                [
                    'class' => '\common\components\FSMActionColumn',
                    'header' => Yii::t('common', 'Options'),
                    'headerOptions' => ['class'=>'td-mw-125'],
                    'dropdown' => true,
                    'dropdownButton' => [
                        'class' => 'btn btn-default',
                        'label' => Yii::t('common', 'Options'),
                    ],
                    'isDropdownActionColumn' => true,
                    'dropdownDefaultBtn' => 'pay',
                    'template' => '{pay} {write-on-basis} {credit-invoice} {mutual-settlement} {cession} {debt-relief} {cancel}',
                    'buttons' => $searchModel->getOptionsActionButtons('xs'),
                    'visible' => FSMAccessHelper::can('updateBill'),
                ],    
                [
                    'class' => '\common\components\FSMActionColumn',
                    'headerOptions' => ['class'=>'td-mw-125'],
                    'dropdown' => true,
                    'dropdownDefaultBtn' => 'view-attachment',
                    'checkPermission' => true,
                    'template' => !empty(Yii::$app->params['ENABLE_BILL_TRANSFER']) ?
		    	'{view-pdf} {view-attachment} {ajax-add-attachment} {view} {send-other-abonent} {receive-other-abonent} {send-mail-client} {send-mail-agent} {copy} {update} {delete}' :
		    	'{view-pdf} {view-attachment} {ajax-add-attachment} {view} {send-mail-client} {send-mail-agent} {copy} {update} {delete}',
                    'buttons' => [
                        'ajax-add-attachment' => function (array $params) { 
                            return Bill::getButtonAddAttachment($params);
                        },
                        'view-attachment' => function (array $params) { 
                            return Bill::getButtonViewAttachment($params);
                        },
                        'view-pdf' => function (array $params) { 
                            return Bill::getButtonPrint($params);
                        },
                        'send-other-abonent' => function (array $params) { 
                            return Bill::getButtonSendOtherAbonent($params);
                        },
                        'receive-other-abonent' => function (array $params) { 
                            return Bill::getButtonReceiveOtherAbonent($params);
                        },
                        'send-mail-client' => function (array $params) { 
                            return Bill::getButtonSendMailClient($params);
                        },
                        'send-mail-agent' => function (array $params) { 
                            return Bill::getButtonSendMailAgent($params);
                        },
                        'copy' => function (array $params) { 
                            return Bill::getButtonCopy($params);
                        },
                    ],
                    'controller' => 'bill',
                ],                            
            ];
        ?>

        <?= GridView::widget([
            'id' => 'bill-grid-view',
            'tableOptions' => [
                'id' => 'bill-list',
            ],
            'responsive' => false,
            'condensed' => true,
            //'striped' => false,
            'hover' => true,
            'dataProvider' => $dataProvider,
            'filterModel' => null,
            'columns' => $columns,
            'pjax' => true,
            'floatHeader' => true,
            'export' => false,
        ]);
        ?>

    </div>    
</div>


<?php
    $body = ob_get_contents();
    ob_get_clean(); 

    $panelContent = [
        'heading' => Bill::modelTitle(2),
        'preBody' => '<div class="panel-body">',
        'body' => $body,
        'postBody' => '</div>',
    ];
    echo FSMHtml::panel(
        $panelContent, 
        'success', 
        [
            'id' => "panel-bill-data",
        ]
    );
?>