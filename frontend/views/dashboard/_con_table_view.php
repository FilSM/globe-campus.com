<?php
namespace common\models;

use Yii;
//use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
//use yii\grid\GridView;

use kartik\helpers\Html;
use kartik\grid\GridView;

use common\components\FSMHelper;
use common\components\FSMAccessHelper;
use common\components\FSMHtml;
use common\models\bill\Convention;

/* @var $this yii\web\View */
/* @var $searchModel common\models\bill\search\ConventionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

ob_start();
ob_implicit_flush(false);

?>

<div class="convention-index">

    <div id="page-content">
        
        <?php
            $columns = [
                //['class' => '\kartik\grid\SerialColumn'],
                [
                    'attribute' => 'id',
                    'width' => '75px',
                    'hAlign' => 'center',
                ],
                [
                    'attribute'=>'doc_number',
                    'width' => '150px',
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'value' => function ($model) {
                        return !empty($model->doc_number) ? 
                            (FSMAccessHelper::can('viewConvention', $model) ?
                                Html::a($model->doc_number, ['/convention/view', 'id' => $model->id], ['target' => '_blank', 'data-pjax' => 0]) :
                                $model->doc_number
                            ) : null;
                    },                         
                    'format'=>'raw',
                ],  
                [
                    'attribute' => 'doc_type',
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'value' => function ($model) {
                        return isset($model->doc_type) ? $model->ConventionTypeList[$model->doc_type] : null;
                    },
                    'format'=>'raw',
                ],                             
                [
                    'attribute'=>'first_client_id',
                    //'headerOptions' => ['class'=>'td-mw-100'],
                    'contentOptions' => [
                        'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                    ],
                    'value' => function ($model) {
                        return 
                            '<div style="overflow-x: auto;">'.
                            (!empty($model->first_client_name) ? 
                            (FSMAccessHelper::can('viewClient', $model->firstClient) ?
                                Html::a($model->first_client_name, ['/client/view', 'id' => $model->first_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                $model->first_client_name
                            )
                            : null).
                            '</div>';
                    },                           
                    'format'=>'raw',
                ],                          
                [
                    'attribute'=>'second_client_id',
                    //'headerOptions' => ['class'=>'td-mw-100'],
                    'contentOptions' => [
                        'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                    ],
                    'value' => function ($model) {
                        return  
                            '<div style="overflow-x: auto;">'.
                            (!empty($model->second_client_name) ?  
                            (FSMAccessHelper::can('viewClient', $model->secondClient) ?
                                Html::a($model->second_client_name, ['/client/view', 'id' => $model->second_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                $model->second_client_name
                            )
                            : null).
                            '</div>';
                    },
                    'format'=>'raw',
                ],
                [
                    'attribute'=>'third_client_id',
                    //'headerOptions' => ['class'=>'td-mw-100'],
                    'contentOptions' => [
                        'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                    ],
                    'value' => function ($model) {
                        return  
                            '<div style="overflow-x: auto;">'.
                            (!empty($model->third_client_name) ?  
                            (FSMAccessHelper::can('viewClient', $model->thirdClient) ?
                                Html::a($model->third_client_name, ['/client/view', 'id' => $model->third_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                $model->third_client_name
                            )
                            : null).
                            '</div>';
                    },
                    'format'=>'raw',
                ],            
                [
                    'attribute' => 'doc_date',
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'headerOptions' => ['class'=>'td-mw-75'],
                    'value' => function ($model) {
                        return isset($model->doc_date) ? date('d-M-Y', strtotime($model->doc_date)) : null;
                    },
                    'format'=>'raw',        
                ],
                [
                    'class' => '\common\components\FSMActionColumn',
                    'headerOptions' => ['class'=>'td-mw-125'],
                    'dropdown' => true,
                    'checkPermission' => true,
                    'controller' => 'convention',
                ], 
            ];
        ?>

        <?= GridView::widget([
            'id' => 'convention-grid-view',
            'tableOptions' => [
                'id' => 'convention-list',
            ],
            'responsive' => false,
            'condensed' => true,
            //'striped' => false,
            'hover' => true,
            'dataProvider' => $dataProvider,
            'filterModel' => null,
            'columns' => $columns,
            'pjax' => true,
            'floatHeader' => true,
            'export' => false,
        ]);
        ?>

    </div>    
</div>


<?php
    $body = ob_get_contents();
    ob_get_clean(); 

    $panelContent = [
        'heading' => Convention::modelTitle(2),
        'preBody' => '<div class="panel-body">',
        'body' => $body,
        'postBody' => '</div>',
    ];
    echo FSMHtml::panel(
        $panelContent, 
        'success', 
        [
            'id' => "panel-convention-data",
        ]
    );
?>