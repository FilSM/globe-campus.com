<?php

use yii\helpers\Url;

use kartik\helpers\Html;
use kartik\detail\DetailView;

use common\components\FSMAccessHelper;
use common\components\FSMHelper;
use common\models\bill\Bill;

/* @var $this yii\web\View */
/* @var $model common\models\bill\Bill */

$this->title = Yii::t('common', 'New documents');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dashboard-index">
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= Html::pageHeader(Html::encode($this->title)); ?>

    <div class='col-md-12'>
        <?= $this->render('_bill_table_view', [
            'dataProvider' => $billDataProvider,
            'searchModel' => $billSearchModel,
            'isAdmin' => $isAdmin,
        ]) ?>         
    </div>

    <div class='col-md-12'>
        <?= $this->render('_agr_table_view', [
            'dataProvider' => $agrDataProvider,
            'searchModel' => $agrSearchModel,
            'isAdmin' => $isAdmin,
        ]) ?>         
    </div>

    <div class='col-md-12'>
        <?= $this->render('_exp_table_view', [
            'dataProvider' => $expDataProvider,
            'searchModel' => $expSearchModel,
            'isAdmin' => $isAdmin,
        ]) ?>         
    </div>

    <div class='col-md-12'>
        <?= $this->render('_con_table_view', [
            'dataProvider' => $conDataProvider,
            'searchModel' => $conSearchModel,
            'isAdmin' => $isAdmin,
        ]) ?>         
    </div>
</div>
