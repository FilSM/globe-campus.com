<?php
namespace common\models;

use Yii;
//use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
//use yii\grid\GridView;

use kartik\helpers\Html;
use kartik\grid\GridView;
use kartik\icons\Icon;

use common\components\FSMHelper;
use common\components\FSMAccessHelper;
use common\components\FSMHtml;
use common\models\client\Agreement;

/* @var $this yii\web\View */
/* @var $searchModel common\models\client\search\AgreementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

Icon::map($this);
ob_start();
ob_implicit_flush(false);
?>

<div class="agreement-index">

    <div id="page-content">
        
        <?php
            $columns = [
                //['class' => '\kartik\grid\SerialColumn'],

                [
                    'attribute' => 'id',
                    'width' => '75px',
                    'hAlign' => 'center',
                ],
                [
                    'attribute' => 'number',
                    'width' => '150px',
                    'contentOptions' => [
                        'style'=>'max-width:100px; min-height:75px; word-wrap: break-word;'
                    ],
                    'value' => function ($model) {
                        return 
                            '<div style="overflow-x: auto;">'.
                            (!empty($model->number) ? (FSMAccessHelper::can('viewAgreement', $model) ?
                                Html::a($model->number, ['/agreement/view', 'id' => $model->id], ['target' => '_blank', 'data-pjax' => 0]) :
                                $model->number
                            )
                            : null).
                            '</div>';
                    },                        
                    'format' => 'raw',
                ],
                [
                    'class' => '\kartik\grid\ExpandRowColumn',
                    'expandIcon' => Icon::show('chevron-circle-right'),
                    'collapseIcon' => Icon::show('chevron-circle-down'),
                    'value' => function ($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                    },
                    'detailUrl' => Url::to(['/bill/bill-detail']),
                    'visible' => FSMAccessHelper::checkRoute('/bill/*'),
                ],
                [
                    'attribute' => 'agreement_type',
                    //'headerOptions' => ['class'=>'td-mw-100'],
                    'value' => function ($model) {
                        return isset($model->agreement_type) ? $model->agreementTypeList[$model->agreement_type] : null;
                    },
                    'visible' => $isAdmin,
                    'format'=>'raw',
                ],
                [
                    'attribute'=>'project_name',
                    'contentOptions' => [
                        'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                    ],

                    'value' => function ($model) {
                        return 
                            '<div style="overflow-x: auto;">'.
                            (!empty($model->project_id) ? (FSMAccessHelper::can('viewProject', $model->project) ?
                                Html::a($model->project_name, ['/project/view', 'id' => $model->project_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                $model->project_name 
                            )
                            : null).
                            '</div>';                    
                    },                         
                    'format'=>'raw',
                    'visible' => FSMAccessHelper::can('viewProject'),
                ],            
                [
                    'attribute' => 'status',
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'headerOptions' => ['class'=>'td-mw-100'],
                    'value' => function ($model) {
                        return isset($model->status) ? Html::badge($model->agreementStatusList[$model->status], ['class' => $model->statusBackgroundColor.' status-badge']) : null;
                    },
                    'visible' => $isAdmin,
                    'format'=>'raw',
                ],
                [
                    'attribute'=>'first_client_name',
                    //'headerOptions' => ['class'=>'td-mw-100'],
                    'contentOptions' => [
                        'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                    ],
                    'value' => function ($model) {
                        return 
                            '<div style="overflow-x: auto;">'.
                            (!empty($model->first_client_id) ? 
                            (!empty($model->first_client_role_id) ? $model->firstClientRole->name.':<br/>' : '') . 
                                (FSMAccessHelper::can('viewClient', $model->firstClient) ?
                                    Html::a($model->first_client_name, ['/client/view', 'id' => $model->first_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                    $model->first_client_name
                                )
                            : null).
                            '</div>';
                    },                         
                    'format'=>'raw',
                ],            
                [
                    'attribute'=>'second_client_name',
                    //'headerOptions' => ['class'=>'td-mw-100'],
                    'contentOptions' => [
                        'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                    ],
                    'value' => function ($model) {
                        return 
                            '<div style="overflow-x: auto;">'.
                            (!empty($model->second_client_id) ?  
                            (!empty($model->second_client_role_id) ? $model->secondClientRole->name.':<br/>' : '') . 
                                (FSMAccessHelper::can('viewClient', $model->secondClient) ?
                                    Html::a($model->second_client_name, ['/client/view', 'id' => $model->second_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                    $model->second_client_name
                                )
                            : null).
                            '</div>';
                    },                         
                    'format'=>'raw',
                    //'hiddenFromExport' => true,
                ],            
                [
                    'attribute'=>'third_client_name',
                    //'headerOptions' => ['class'=>'td-mw-100'],
                    'contentOptions' => [
                        'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                    ],
                    'value' => function ($model) {
                        return 
                            '<div style="overflow-x: auto;">'.
                            (!empty($model->third_client_id) ?   
                            (!empty($model->third_client_role_id) ? $model->thirdClientRole->name.':<br/>' : '') . 
                                (FSMAccessHelper::can('viewClient', $model->thirdClient) ?
                                    Html::a($model->third_client_name, ['/client/view', 'id' => $model->third_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                                    $model->third_client_name
                                )
                            : null).
                            '</div>';
                    },                         
                    'format'=>'raw',
                    //'hiddenFromExport' => true,
                ],            
                [
                    'attribute' => 'signing_date',
                    'headerOptions' => [
                        'class' => 'td-mw-75',
                    ],
                    'value' => function ($model) {
                        return !empty($model->signing_date) ? date('d-M-Y', strtotime($model->signing_date)) : null;
                    },                 
                ],                         
                [
                    'attribute' => 'due_date',
                    'headerOptions' => [
                        'class' => 'td-mw-75',
                    ],
                    'value' => function ($model) {
                        return !empty($model->due_date) ? date('d-M-Y', strtotime($model->due_date)) : null;
                    },                 
                ],                                                 
                [
                    'attribute' => 'summa',
                    'hAlign' => 'right',
                    'headerOptions' => [
                        'class' => 'td-mw-75',
                        //'style' => 'text-align: left;',
                    ],
                ],                 
                [
                    'attribute' => 'conclusion',
                    'headerOptions' => ['class'=>'td-mw-100'],                
                    'value' => function ($model) {
                        return isset($model->status) ? $model->agreementConclusionList[$model->conclusion] : null;
                    },
                    'visible' => $isAdmin,
                ],
                [
                    'class' => '\common\components\FSMActionColumn',
                    'headerOptions' => ['class'=>'td-mw-125'],
                    'dropdown' => true,
                    'dropdownDefaultBtn' => 'attachment',
                    'checkPermission' => true,
                    'template' => !empty(Yii::$app->params['ENABLE_BILL_TRANSFER']) ?
                        '{attachment} {view} {payment} {send-other-abonent} {receive-other-abonent} {update} {delete}' :
                        '{attachment} {view} {payment} {update} {delete}',
                    'buttons' => [
                        'attachment' => function (array $params) { 
                            return Agreement::getButtonAttachment($params);
                        },
                        'payment' => function (array $params) { 
                            return Agreement::getButtonPayment($params);
                        },
                        'send-other-abonent' => function (array $params) { 
                            return Agreement::getButtonSendOtherAbonent($params);
                        },
                        'receive-other-abonent' => function (array $params) { 
                            return Agreement::getButtonReceiveOtherAbonent($params);
                        },
                    ],
                    'controller' => 'agreement',
                ],                            
            ];
        ?>

        <?= GridView::widget([
            'id' => 'agreement-grid-view',
            'tableOptions' => [
                'id' => 'agreement-list',
            ],
            'responsive' => false,
            'condensed' => true,
            //'striped' => false,
            'hover' => true,
            'dataProvider' => $dataProvider,
            'filterModel' => null,
            'columns' => $columns,
            'pjax' => true,
            'floatHeader' => true,
            'export' => false,
        ]);
        ?>

    </div>    
</div>


<?php
    $body = ob_get_contents();
    ob_get_clean(); 

    $panelContent = [
        'heading' => Agreement::modelTitle(2),
        'preBody' => '<div class="panel-body">',
        'body' => $body,
        'postBody' => '</div>',
    ];
    echo FSMHtml::panel(
        $panelContent, 
        'success', 
        [
            'id' => "panel-agreement-data",
        ]
    );
?>