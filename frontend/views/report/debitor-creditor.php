<?php
//namespace common\models;

//use Yii;
//use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\grid\GridView;
use yii\helpers\Url;

use kartik\helpers\Html;
use kartik\grid\GridView;

use common\models\bill\Bill;
use common\components\FSMHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\bill\search\BillSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('report', 'Debtors/Creditors');
$this->params['breadcrumbs'][] = $this->title;
//$dateFrom = isset($_GET['from']) ? date("Y-m-d", strtotime($_GET['from'])) : date('Y-01-01');
$dateFrom = '1900-01-01';
$dateTill = isset($_GET['till']) ? (!empty($_GET['till']) ? date("Y-m-d", strtotime($_GET['till'])) : date("Y-m-t")) : date("Y-m-t");
?>
<div id="bill-index">

    <div id="page-content">
        <div>
            <?= Html::pageHeader(Html::encode($this->title)); ?>
        </div>
        
        <?= $this->render('_search-date-client', [
            'model' => $searchModel,
            'clientList' => $clientList,
            'action' => 'debitor-creditor',
        ])?>

        <p></p>
        
        <?php
            $columns = [
                ['class' => '\kartik\grid\SerialColumn'],
                [
                    'attribute' => 'client_id',
                    'headerOptions' => ['class' => 'td-mw-150'],
                    'value' => function ($model) {
                        return !empty($model['client_id']) ?
                            Html::a($model['client_name'], ['/client/view', 'id' => $model['client_id']], ['target' => '_blank', 'data-pjax' => 0,]) : null;
                    },
                    'format' => 'raw',
                    'pageSummary' => $searchModel->getAttributeLabel('total'),
                ],
                [
                    'attribute' => 'debtors_summa',
                    'hAlign' => 'right',
                    'mergeHeader' => true,
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'value' => function ($model) {
                        return !empty($model['debtors_summa']) ? $model['debtors_summa'] : 0;
                    },
                    'format' => ['decimal', 2],
                    'pageSummary' => true,                            
                ],                         
                [
                    'hAlign' => 'center',
                    'width' => '75px',
                    'mergeHeader' => true,
                    'value' => function ($model) use($dateFrom, $dateTill) {
                        if(empty((float)$model['debtors_summa'])){
                            return '';
                        }
                        return Html::a(Html::icon('th-list').' '.Yii::t('report', 'Details'), 
                            [
                                '/bill/report-details-debitor-creditor', 
                                'client_id' => $model['client_id'],
                                'from' => $dateFrom,
                                'till' => $dateTill,
                                'direction' => 'in',
                            ], 
                            ['target' => '_blank', 'data-pjax' => 0,]);
                    },
                    'format'=>'raw',
                    'hiddenFromExport' => true,
                ],
                [
                    'attribute' => 'creditors_summa',
                    'hAlign' => 'right',
                    'mergeHeader' => true,
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'value' => function ($model) {
                        return !empty($model['creditors_summa']) ? $model['creditors_summa'] : 0;
                    },
                    'format' => ['decimal', 2],
                    'pageSummary' => true,                            
                ],                         
                [
                    'hAlign' => 'center',
                    'width' => '75px',
                    'mergeHeader' => true,
                    'value' => function ($model) use($dateFrom, $dateTill) {
                        if(empty((float)$model['creditors_summa'])){
                            return '';
                        }
                        return Html::a(Html::icon('th-list').' '.Yii::t('report', 'Details'), 
                            [
                                '/bill/report-details-debitor-creditor', 
                                'client_id' => $model['client_id'], 
                                'from' => $dateFrom,
                                'till' => $dateTill,
                                'direction' => 'out',
                            ], 
                            ['target' => '_blank', 'data-pjax' => 0,]);
                    },
                    'format' => 'raw',
                    'hiddenFromExport' => true,
                ],                         
            ];
        ?>

        <?= GridView::widget([
            'id' => 'debitor-creditor-grid',
            'responsive' => true,
            'striped' => true,
            'hover' => true,
            'bordered' => true,
            'condensed' => true,
            'persistResize' => false,
            'floatHeader' => true,
            'autoXlFormat' => false,
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'showPageSummary' => true,
            'pjax' => true,
            'columns' => $columns,
        ]);        
        ?>
        
    </div>    
</div>