<?php
//namespace common\models;

use kartik\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\bill\search\BillSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('report', 'EBITDA by Projects');
$this->params['breadcrumbs'][] = $this->title;
$dateFrom = isset($_GET['from']) ? date("Y-m-d", strtotime($_GET['from'])) : date('Y-01-01');
$dateTill = isset($_GET['till']) ? (!empty($_GET['till']) ? date("Y-m-d", strtotime($_GET['till'])) : date("Y-m-t")) : date("Y-m-t");
?>
<div id="bill-index">

    <div id="page-content">
        <div>
            <?= Html::pageHeader(Html::encode($this->title)); ?>
        </div>
        
        <?= $this->render('_search-from-till_project', [
            'model' => $searchModel,
            'projectList' => $projectList,
            'action' => 'ebitda',
        ])?>

        <p></p>
        
        <?php
            $columns = [
                ['class' => '\kartik\grid\SerialColumn'],
                [
                    'attribute'=>'project_name',
                    'headerOptions' => ['class'=>'td-mw-150'],
                    'value' => function ($model) {
                        return !empty($model['project_id']) ? 
                            Html::a($model['project_name'], ['/project/view', 'id' => $model['project_id']], ['target' => '_blank', 'data-pjax' => 0,]) : null;
                    },
                    'format'=>'raw',
                    'pageSummary' => $searchModel->getAttributeLabel('total'),
                ],                          
                [
                    'attribute' => 'project_sales',
                    'hAlign' => 'right',
                    'mergeHeader' => true,
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'value' => function ($model) {
                        return !empty($model['project_sales']) ? $model['project_sales'] : 0;
                    },
                    'format' => ['decimal', 2],
                    'pageSummary' => true,                            
                ],                         
                [
                    'hAlign' => 'center',
                    'width' => '75px',
                    'mergeHeader' => true,
                    'value' => function ($model) use($dateFrom, $dateTill) {
                        if(empty((float)$model['project_sales'])){
                            return '';
                        }
                        return Html::a(Html::icon('th-list').' '.Yii::t('report', 'Details'), 
                            [
                                '/bill/report-details-ebitda', 
                                'project_id' => $model['project_id'], 
                                'from' => $dateFrom,
                                'till' => $dateTill,
                                'direction' => 'in',
                            ], 
                            ['target' => '_blank', 'data-pjax' => 0,]);
                    },
                    'format'=>'raw',                           
                    'hiddenFromExport' => true,
                ],                         
                [
                    'attribute' => 'project_purchases',
                    'hAlign' => 'right',
                    'mergeHeader' => true,
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'value' => function ($model) {
                        return !empty($model['project_purchases']) ? $model['project_purchases'] : 0;
                    },
                    'format' => ['decimal', 2],
                    'pageSummary' => true,                            
                ],
                [
                    'hAlign' => 'center',
                    'width' => '75px',
                    'mergeHeader' => true,
                    'value' => function ($model) use($dateFrom, $dateTill) {
                        if(empty((float)$model['project_purchases'])){
                            return '';
                        }
                        return Html::a(Html::icon('th-list').' '.Yii::t('report', 'Details'), 
                            [
                                '/bill/report-details-ebitda', 
                                'project_id' => $model['project_id'],
                                'from' => $dateFrom,
                                'till' => $dateTill,
				'direction' => 'out', 
                            ], 
                            ['target' => '_blank', 'data-pjax' => 0,]);
                    },
                    'format'=>'raw',                           
                    'hiddenFromExport' => true,
                ],                         
                [
                    'attribute' => 'project_profit',
                    'hAlign' => 'right',
                    'mergeHeader' => true,
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'value' => function ($model) {
                        return !empty($model['project_profit']) ? $model['project_profit'] : 0;
                    },
                    'format' => ['decimal', 2],
                    'pageSummary' => true,                            
                ],
            ];
        ?>

        <?= GridView::widget([
            'id' => 'ebitda-grid',
            'responsive' => true,
            'striped' => true,
            'hover' => true,
            'bordered' => true,
            'condensed' => true,
            'persistResize' => false,
            'floatHeader' => true,
            'autoXlFormat' => false,
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'showPageSummary' => true,
            'pjax' => true,
            'columns' => $columns,
        ]);
        ?>

    </div>    
</div>