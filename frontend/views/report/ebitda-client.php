<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use kartik\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

use common\models\bill\Bill;
use common\components\FSMHelper;
use common\components\FSMExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel common\models\bill\search\BillSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('report', 'EBITDA by Clients');
$this->params['breadcrumbs'][] = $this->title;
$dateFrom = isset($_GET['from']) ? date("Y-m-d", strtotime($_GET['from'])) : date('Y-01-01');
$dateTill = isset($_GET['till']) ? (!empty($_GET['till']) ? date("Y-m-d", strtotime($_GET['till'])) : date("Y-m-t")) : date("Y-m-t");
$base = !empty($base);

$columns = [
    ['class' => '\kartik\grid\SerialColumn'],
    [
        'attribute'=>'client_name',
        'headerOptions' => ['class'=>'td-mw-150'],
        'value' => function ($model) {
            return !empty($model['client_id']) ? 
                Html::a($model['client_name'], ['/client/view', 'id' => $model['client_id']], ['target' => '_blank', 'data-pjax' => 0,]) : null;
        },
        'format'=>'raw',
        'pageSummary' => $searchModel->getAttributeLabel('total'),
    ],                          
    [
        'attribute' => 'client_sales',
        'hAlign' => 'right',
        'mergeHeader' => true,
        'headerOptions' => ['style' => 'text-align: center;'],
        'value' => function ($model) {
            return !empty($model['client_sales']) ? $model['client_sales'] : 0;
        },
        'format' => ['decimal', 2],
        'pageSummary' => true,                            
    ],                         
    [
        'hAlign' => 'center',
        'width' => '75px',
        'mergeHeader' => true,
        'value' => function ($model) use($dateFrom, $dateTill) {
            if(empty((float)$model['client_sales'])){
                return '';
            }
            return Html::a(Html::icon('th-list').' '.Yii::t('report', 'Details'), 
                [
                    '/bill/report-details-ebitda-client', 
                    'client_id' => $model['client_id'], 
                    'from' => $dateFrom,
                    'till' => $dateTill,
                    'direction' => 'in',
                ], 
                ['target' => '_blank', 'data-pjax' => 0,]);
        },
        'format'=>'raw',                           
        'hiddenFromExport' => true,
    ],                         
    [
        'attribute' => 'client_purchases',
        'hAlign' => 'right',
        'mergeHeader' => true,
        'headerOptions' => ['style' => 'text-align: center;'],
        'value' => function ($model) {
            return !empty($model['client_purchases']) ? $model['client_purchases'] : 0;
        },
        'format' => ['decimal', 2],
        'pageSummary' => true,                            
    ],
    [
        'hAlign' => 'center',
        'width' => '75px',
        'mergeHeader' => true,
        'value' => function ($model) use($dateFrom, $dateTill, $base) {
            if(empty((float)$model['client_purchases'])){
                return '';
            }
            return Html::a(Html::icon('th-list').' '.Yii::t('report', 'Details'), 
                [
                    '/bill/report-details-ebitda-client', 
                    'client_id' => $model['client_id'],
                    'from' => $dateFrom,
                    'till' => $dateTill,
                    'direction' => 'out',
                    'plus' => empty($base),
                ], 
                ['target' => '_blank', 'data-pjax' => 0,]);
        },
        'format'=>'raw',                           
        'hiddenFromExport' => true,
    ],                         
    [
        'attribute' => 'client_profit',
        'hAlign' => 'right',
        'mergeHeader' => true,
        'headerOptions' => ['style' => 'text-align: center;'],
        'value' => function ($model) {
            return !empty($model['client_profit']) ? $model['client_profit'] : 0;
        },
        'format' => ['decimal', 2],
        'pageSummary' => true,                            
    ],
];
?>

<div id="bill-index">

    <div id="page-content">
        <div>
            <?= Html::pageHeader(Html::encode($this->title).(empty($base) ? ' (PLUS)' : '')); ?>
        </div>
        
        <div style="display: flex;">
            <?= $this->render('_search-from-till_client', [
                'model' => $searchModel,
                'clientList' => $clientList,
                'action' => 'ebitda-client-'.(empty($base) ? 'plus' : 'base'),
            ])?>

            <div style="margin-left: auto;">
                <?= FSMExportMenu::widget([
                    'options' => ['id' => 'bill-index-menu'],
                    'dataProvider' => $dataProvider,
                    'columns' => $columns,
                    'showColumnSelector' => false,
                    'showConfirmAlert' => false,
                    'dropdownOptions' => [
                        'label' => Yii::t('kvgrid', 'Export'),
                        'class' => 'btn btn-default',
                        'menuOptions' => [
                            'class' => 'pull-right',
                        ],
                    ],
                    'target' => ExportMenu::TARGET_SELF,
                    'exportConfig' => [
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_HTML => false,
                        ExportMenu::FORMAT_CSV => false,
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_EXCEL => false,
                        ExportMenu::FORMAT_EXCEL_X => ['label' => Yii::t('kvgrid', 'Excel'), 'alertMsg' => ''],
                    ],
                    'pjaxContainerId' => 'bill-index',
                    'exportFormView' => '@vendor/kartik-v/yii2-export/views/_form',
                ]);?> 
            </div>
        </div>

        <p></p>

        <?= GridView::widget([
            'id' => 'ebitda-client-grid',
            'tableOptions' => [
                'id' => 'bill-list',
            ],
            'responsive' => false,
            'striped' => true,
            'hover' => true,
            'bordered' => true,
            'condensed' => true,
            'persistResize' => false,
            'floatHeader' => true,
            'autoXlFormat' => true,
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'showPageSummary' => true,
            'pjax' => true,
            'columns' => $columns,
            'pjaxSettings' => ['options' => ['id' => 'bill-index']],
        ]);
        ?>

    </div>    
</div>