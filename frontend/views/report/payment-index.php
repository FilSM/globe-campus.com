<?php
use yii\helpers\Url;

use kartik\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

use common\models\bill\Bill;
use common\components\FSMAccessHelper;
use common\components\FSMExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel common\models\bill\search\BillSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$reportTitle = ($tableId == 'not-paid' ? Yii::t('report', 'Not paid invoices') : Yii::t('report', 'Paid invoices'));
$fromDate = !empty($_GET['from']) ? date('Y-m-d', strtotime($_GET['from'])) : date('Y-m-01');
$tillDate = !empty($_GET['till']) ? date('Y-m-d', strtotime($_GET['till'])) : date('Y-m-t');

$exportMode = !empty($_POST['export_type']);
?>

<div id="bill-index-<?= $tableId?>">

    <?= Html::tag('H1', Html::tag('small', $reportTitle));?>
    
    <?php
        $columns = [
            //['class' => '\kartik\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'width' => '75px',
                'hAlign' => 'center',
            ],                      
            [
                'attribute'=>'doc_number',
                'headerOptions' => ['class'=>'td-mw-100'],
                'hAlign' => 'left',
                'value' => function ($model) {
                    return !empty($model->doc_number) ? $model->doc_number : null;
                },                         
                'format'=>'raw',
                'visible' => $exportMode,                        
            ],
            [
                'attribute'=>'doc_number',
                'headerOptions' => ['class'=>'td-mw-100'],
                'value' => function ($model) {
                    return !empty($model->doc_number) ? 
                        (isset($model->doc_type) ? 
                            $model->billDocTypeList[$model->doc_type].'<br/>' : null).
                            (FSMAccessHelper::can('viewBill', $model) ?
                                Html::a($model->doc_number, ['/bill/view', 'id' => $model->id], ['target' => '_blank', 'data-pjax' => 0]) :
                                $model->doc_number
                            )
                        : null;
                },                         
                'format'=>'raw',
                'hiddenFromExport' => true,    
            ],                      
            [
                'attribute' => 'status',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) {
                    return $model->statusHTML;
                },
                'format'=>'raw',
            ],                          
            [
                'attribute'=>'first_client_name',
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) {
                    return !empty($model->first_client_name) ? $model->first_client_name : null;
                },                         
                'format'=>'raw',
                'visible' => $exportMode,
            ],                        
            [
                'attribute'=>'first_client_name',
                'contentOptions' => [
                    'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                ],
                'value' => function ($model) {
                    return 
                    '<div style="overflow-x: auto;">'.
                        (!empty($model->first_client_name) ? 
                        (!empty($model->first_client_role_name) ? $model->first_client_role_name.'<br/>' : '') . 
                        Html::a($model->first_client_name, ['/client/view', 'id' => $model->first_client_id], ['target' => '_blank', 'data-pjax' => 0]) : null).
                    '</div>';
                },                         
                'format'=>'raw',
                'hiddenFromExport' => true,
            ],       
            [
                'attribute'=>'second_client_name',
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) {
                    return !empty($model->second_client_name) ? $model->second_client_name : null;
                },                         
                'visible' => $exportMode,
            ],                          
            [
                'attribute'=>'second_client_name',
                'contentOptions' => [
                    'style'=>'max-width:150px; min-height:100px; word-wrap: break-word;'
                ],
                'value' => function ($model) {
                    return  
                    '<div style="overflow-x: auto;">'.
                        (!empty($model->second_client_name) ?  
                        (!empty($model->second_client_role_name) ? $model->second_client_role_name.'<br/>' : '') . 
                        Html::a($model->second_client_name, ['/client/view', 'id' => $model->second_client_id], ['target' => '_blank', 'data-pjax' => 0]) : null).
                    '</div>';
                },                         
                'format'=>'raw',
                'hiddenFromExport' => true,
            ],       
            [
                'attribute' => 'summa_eur',
                'hAlign' => 'right',
                'headerOptions' => ['style'=>'text-align: center;'],
                'mergeHeader' => true,
                'value' => function ($model) use ($tableId, $fromDate, $tillDate) {
                    if($tableId == 'not-paid'){
                        $summa = $model->billNotPaidSumma;
                    }else{
                        $paymentList = $model->getConfirmedPaymentsToDate(['from' => $fromDate, 'till' => $tillDate]);
                        $summa = 0;
                        foreach ($paymentList as $payment) {
                            $summa += $payment->summa_origin;
                        }                        
                    }
                    $div = (($model->total_eur != '0.00') ? (($model->vat_eur * 100) / $model->total_eur) : 0);
                    $vat = ($summa * $div) / 100;
                    $result = ($summa - $vat);
                    return $result;
                },
                'xlFormat' => '###0.00',
                'exportMenuStyle' => [
                    'numberFormat' => ['formatCode' => '###0.00'],
                ],                  
                'format' => ['decimal', 2],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'vat_eur',
                'hAlign' => 'right',
                'headerOptions' => ['style'=>'text-align: center;'],
                'mergeHeader' => true,
                'value' => function ($model) use ($tableId, $fromDate, $tillDate) {
                    if($tableId == 'not-paid'){
                        $summa = $model->billNotPaidSumma;
                    }else{
                        $paymentList = $model->getConfirmedPaymentsToDate(['from' => $fromDate, 'till' => $tillDate]);
                        $summa = 0;
                        foreach ($paymentList as $payment) {
                            $summa += $payment->summa_origin;
                        } 
                    }
                    $div = (($model->total_eur != '0.00') ? (($model->vat_eur * 100) / $model->total_eur) : 0);
                    $result = ($summa * $div) / 100;
                    return $result;
                },
                'xlFormat' => '###0.00',
                'exportMenuStyle' => [
                    'numberFormat' => ['formatCode' => '###0.00'],
                ],                  
                'format' => ['decimal', 2],
                'enableSorting' => false, 
            ],       
            [
                'attribute' => 'total_eur',
                'hAlign' => 'right',
                'headerOptions' => ['style'=>'text-align: center;'],
                'mergeHeader' => true,
                'value' => function ($model) use ($tableId, $fromDate, $tillDate) {
                    if($tableId == 'not-paid'){
                        $result = $model->billNotPaidSumma;
                    }else{
                        $paymentList = $model->getConfirmedPaymentsToDate(['from' => $fromDate, 'till' => $tillDate]);
                        $summa = 0;
                        foreach ($paymentList as $payment) {
                            $summa += $payment->summa_origin;
                        } 
                        $result = $summa;
                    }
                    return $result;
                },
                'xlFormat' => '###0.00',
                'exportMenuStyle' => [
                    'numberFormat' => ['formatCode' => '###0.00'],
                ],                  
                'format' => ['decimal', 2],
                'enableSorting' => false,           
            ],                        
            [
                'attribute' => 'doc_date',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class'=>'td-mw-100'],
                'value' => function ($model) {
                    return isset($model->doc_date) && ($model->doc_date == date('Y-m-d')) ? 
                        Html::a(Html::badge(Yii::t('common', 'Today'), ['class' => 'badge-success']), 
                            Url::to(['/bill/index']).'?BillSearch[doc_date]='.$model->doc_date, 
                            ['target' => '_blank', 'data-pjax' => 0]) 
                        : date('d-M-Y', strtotime($model->doc_date));
                },
                'format'=>'raw',        
            ],                         
            [
                'attribute' => 'pay_date',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class'=>'td-mw-100'],
                'value' => function ($model) {
                    $class = 'badge-success';
                    if(in_array($model->status, [
                        Bill::BILL_STATUS_CANCELED,
                        Bill::BILL_STATUS_COMPLETE,
                        ]))
                    {
                        $badge = '';
                    }else{
                        $firstDate = new DateTime(date('Y-m-d'));
                        $secondDate = new DateTime($model->pay_date);
                        $dateDiff = date_diff($firstDate, $secondDate, true)->days;

                        $firstDate = $firstDate->format('Y-m-d');
                        $secondDate = $secondDate->format('Y-m-d');
                        if($firstDate > $secondDate){
                            $dateDiff = -1 * $dateDiff;
                            $class = 'badge-danger';
                        }elseif($dateDiff >= 0){
                            $class = 'badge-success';
                        }

                        $badge = isset($model->pay_date) && ($model->pay_date == date('Y-m-d')) ? 
                            '' : Html::badge($dateDiff, ['class' => $class]);
                    }
                    return isset($model->pay_date) && ($model->pay_date == date('Y-m-d')) ? 
                        Html::a(Html::badge(Yii::t('common', 'Today'), ['class' => $class]) , 
                            Url::to(['/bill/index']).'?BillSearch[pay_date]='.$model->pay_date, 
                            ['target' => '_blank', 'data-pjax' => 0]) 
                        : date('d-M-Y', strtotime($model->pay_date)).(!empty($badge) ? '<br/>&nbsp;' : '').$badge;
                },
                'format'=>'raw',
            ],              
            [
                'attribute' => 'paid_date',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class'=>'td-mw-100'],
                'value' => function ($model) {
                    return isset($model->paid_date) ? date('d-M-Y', strtotime($model->paid_date)) : null;
                },
            ],                                          
            [
                'attribute' => 'pay_status',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) {
                    return $model->payStatusHTML;
                },
                'format'=>'raw',
            ],
        ];
    ?>

    <?= FSMExportMenu::widget([
        'options' => ['id' => 'bill-index-menu-'.$tableId],
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'showColumnSelector' => false,
        'showConfirmAlert' => false,
        'dropdownOptions' => [
            'label' => Yii::t('kvgrid', 'Export'),
            'class' => 'btn btn-default'
        ],
        'target' => ExportMenu::TARGET_SELF,
        'exportConfig' => [
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_HTML => false,
            ExportMenu::FORMAT_CSV => false,
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_PDF => false,
            ExportMenu::FORMAT_EXCEL => false,
            ExportMenu::FORMAT_EXCEL_X => ['label' => Yii::t('kvgrid', 'Excel'), 'alertMsg' => ''],
        ],
        'pjaxContainerId' => 'pjax-bill-index-'.$tableId,
        'exportFormView' => '@vendor/kartik-v/yii2-export/views/_form',
    ]);?> 

    <p/>    
        
    <?php if($tableId == 'paid') :
        echo $this->render('_search-from-till', [
            'model' => $searchModel,
            'action' => 'payment',
        ]);
    endif; ?>

    <p/>

    <?= GridView::widget([
        'id' => $tableId.'-grid-view',
        'tableOptions' => [
            'id' => $tableId.'-list',
        ],
        'responsive' => false,
        'striped' => true,
        'hover' => true,
        'bordered' => true,
        'condensed' => true,
        'persistResize' => false,
        'autoXlFormat' => true,
        'dataProvider' => $dataProvider,
        'filterModel' => null,
        'pjax' => true,
        'columns' => $columns,
        'pjaxSettings' => ['options' => ['id' => 'pjax-bill-index-'.$tableId]],
    ]);
    ?>

</div>