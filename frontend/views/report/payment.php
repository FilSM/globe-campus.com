<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\bill\Bill */
$this->title = Yii::t('report', 'Payment of invoices');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-details">

    <?= Html::pageHeader(Html::encode($this->title)); ?>
    
    <?= $this->render('payment-index', [
        'dataProvider' => $notPaidDataProvider,
        'searchModel' => $searchModel,
        'client_id' => null,
        'tableId' => 'not-paid',
    ]);?>
    
    <?= $this->render('payment-index', [
        'dataProvider' => $paidDataProvider,
        'searchModel' => $searchModel,
        'client_id' => null,
        'tableId' => 'paid',
    ]);?>

</div>