<?php
use yii\helpers\Url;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;

use common\models\bill\Bill;

/* @var $this yii\web\View */
/* @var $model common\models\bill\search\BillSearch */
/* @var $form yii\widgets\ActiveForm */
$dateTill = isset($_GET['till']) ? (!empty($_GET['till']) ? date("d-m-Y", strtotime($_GET['till'])) : date("t-m-Y")) : date("t-m-Y");
?>

<div class="bill-search">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_INLINE,
        'id' => 'bill-search',
        'action' => [$action],
        'method' => 'get',
    ]); ?>
    
    <div class="input-group" style="width: 500px; min-width: 300px;">
    <?= Select2::widget([
        'name' => 'client_id',
        'data' => $clientList,
        'value' => !empty($_GET['client_id']) ? $_GET['client_id'] : null,
        'options' => [
            'placeholder' => Yii::t('report', 'Select client ...'),
            'multiple' => true,
            //'style' => 'width: 200px;',
        ],
    ]); ?>
    </div>
    
    <div class="input-group">
    <?= DatePicker::widget([
        'name' => 'till',
        'value' => $dateTill,
        'options' => ['placeholder' => Yii::t('common', 'Till date')],
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
    ]); 
    ?>
    </div>
    
    <div class="form-group">
        <div style="text-align: right;">
            <?= Html::submitButton(Yii::t('common', 'Select'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('common', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>