<?php

namespace frontend\assets;

class BillUIAsset extends \common\components\FSMAssetBundle {

    public function init() {
        $this->setSourcePath('@frontend/views/bill/assets');
        $this->setupAssets('js', [
            'js/filJSBill',
        ]);
        
        $this->setupAssets('css', [
            'css/bill',
        ]);

        $this->depends = [
            'common\assets\UIAsset',
        ];
        
        $this->checkNeedReloadAssets();

        parent::init();
    }

}
