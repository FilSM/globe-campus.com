<?php

namespace frontend\assets;

class ClientUIAsset extends \common\components\FSMAssetBundle {

    public function init() {
        $this->setSourcePath('@frontend/views/client/client/assets');
        $this->setupAssets('js', [
            'js/filJSClient',
        ]);
        
        /*
        $this->setupAssets('css', [
            'css/gmap.control',
        ]);
         * 
         */
        $this->depends = [
            'common\assets\UIAsset',
        ];
        
        $this->checkNeedReloadAssets();
 
        parent::init();
    }

}
