var show_minutes = (enableLogoutTimer && !empty(globalShowMinutes) ? globalShowMinutes : 0);
var timer_time = 0;

(function ($) {
    
    $(document).ready(function () {

        $('body').off('click.newwindow').on('click.newwindow', '.new-window', openNewWindow);

        function openNewWindow(e, options) {
            var href = this.href;
            var width = $(this).data('width');
            var height = $(this).data('height');
            if (empty(width)) {
                width = 1800;
            }
            if (empty(height)) {
                height = 900;
            }
            window.open(href, 'newwindow', 'width=' + width + ', height=' + height);
            return false;
        }

        function update_modal() {
            if (timer_time >= inactivitySeconds) {
                // console.log('update', show_minutes);

                var timerModal = $('#modal-timer');
                var open = timerModal.hasClass('in');
                if (!open) {
                    var dialogMessage = dialogSessionTimer.options.message;
                    dialogSessionTimer.dialog(
                        dialogMessage,
                        function (result) {
                            if (result) { // ok button was pressed
                                // execute your code on dialog confirmation
                            } else { // dialog dialog was cancelled
                                // execute your code for cancellation
                            }
                        }
                    );
                    var timerModal = $('#modal-timer');
                }

                var timeToLogout = timerModal.find('#time_to_logout');
                timeToLogout.html(show_minutes).animate({fontSize: '2em'}).animate({fontSize: '1.5em'});
                if(show_minutes == 1){
                    var minuteTxt = dialogSessionTimer.options.minute_txt;
                    var timeToLogoutTxt = timerModal.find('#time_to_logout_txt');
                    timeToLogoutTxt.html(minuteTxt);
                }

                if (show_minutes === 0) {
                    $.post(
                        logoutUrl,
                        'json'
                    );
                }
                show_minutes--;
            }
        }

        var auth = $('body').hasClass('user');
        if (enableLogoutTimer && auth) {
            // console.log('timer');
            $(document).bind('mousemove keydown scroll', function () {
                // console.log('show minutes', show_minutes);
                /*
                 if ($('#modal-timer').hasClass('in')) {
                 $('#modal-timer').modal('hide');
                 } 
                 */
                timer_time = 0;
                show_minutes = globalShowMinutes;
            });
            setInterval('timer_time++;', 1000);
            setInterval(update_modal, 60000);
        }

    });

})(jQuery);