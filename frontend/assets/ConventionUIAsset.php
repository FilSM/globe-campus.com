<?php

namespace frontend\assets;

class ConventionUIAsset extends \common\components\FSMAssetBundle {

    public function init() {
        $this->setSourcePath('@frontend/views/bill/assets');
        $this->setupAssets('js', [
            'js/filJSConvention',
        ]);

        $this->depends = [
            'common\assets\UIAsset',
        ];
        
        $this->checkNeedReloadAssets();
   
        parent::init();
    }

}
