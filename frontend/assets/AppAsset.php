<?php

namespace frontend\assets;

use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\components\FSMAssetBundle;

/**
 * Main frontend application asset bundle.
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 */
class AppAsset extends FSMAssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/frontend/assets';
    
    /**
     * @inheritdoc
     */
    public function init() {
        //require_once('frontend/assets/js/globalJSVars.php');

        $this->setSourcePath('@webroot/frontend/assets');
        $this->setupAssets('css', [
            'css/site',
            'css/frontend',
        ]);
        $this->setupAssets('js', [
            'js/filJSFrontend',
        ]);

        $this->depends = ArrayHelper::merge(
            $this->depends, [
                'common\assets\CommonAsset',
            ]
        );

        $this->checkNeedReloadAssets();

        parent::init();
    }    
}
