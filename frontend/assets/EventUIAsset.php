<?php

namespace frontend\assets;

class EventUIAsset extends \common\components\FSMAssetBundle {

    public function init() {
        $this->setSourcePath('@frontend/views/event/assets');
        $this->setupAssets('js', [
            'js/filJSEvent',
        ]);
        
        /*
        $this->setupAssets('css', [
            'css/gmap.control',
        ]);
         * 
         */
        $this->depends = [
            'common\assets\UIAsset',
        ];
        
        $this->checkNeedReloadAssets();

        parent::init();
    }

}
