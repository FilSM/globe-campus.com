<?php

namespace frontend\assets;

class AgreementUIAsset extends \common\components\FSMAssetBundle {

    public function init() {
        $this->setSourcePath('@frontend/views/client/agreement/assets');
        $this->setupAssets('js', [
            'js/filJSAgreement',
        ]);
        
        /*
        $this->setupAssets('css', [
            'css/gmap.control',
        ]);
         * 
         */
        $this->depends = [
            'common\assets\UIAsset',
        ];
        
        $this->checkNeedReloadAssets();

        parent::init();
    }

}
