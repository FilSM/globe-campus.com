<?php

namespace frontend\controllers\client;

use Yii;

use common\controllers\FilSMController;
use common\models\user\FSMProfile;
use common\models\client\Agreement;
use common\models\client\AgreementHistory;
use common\models\client\search\AgreementHistorySearch;
use common\models\mainclass\FSMBaseModel;

/**
 * AgreementHistoryController implements the CRUD actions for AgreementHistory model.
 */
class AgreementHistoryController extends FilSMController
{

    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = AgreementHistory::class;
        $this->defaultSearchModel = AgreementHistorySearch::class;
    }

    /**
     * Lists all models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new $this->defaultSearchModel;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        $userList = FSMProfile::getNameArr(['deleted' => false]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'userList' => $userList,
        ]);
    }

    protected function changeAgreementStatus($id, $action_id, $status_id)
    {
        $model = new $this->defaultModel;
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            if ($model->saveHistory($id, $action_id)) {
                $agreementlModel = Agreement::findOne($id);
                $agreementlModel->changeStatus($status_id);
            }else{    
                throw new Exception('Unable to save data! '.$model->errorMessage);
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $transaction->rollBack();
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
        } finally {
            return $this->redirect(FSMBaseModel::getBackUrl());
        }
    }

    public function actionAgreementNew($id = null)
    {
        return $this->changeAgreementStatus($id, AgreementHistory::AgreementHistory_ACTIONS['ACTION_STATUS_UP_NEW'], Agreement::AGREEMENT_STATUS_NEW);
    }

    public function actionAgreementReady($id = null)
    {
        return $this->changeAgreementStatus($id, AgreementHistory::AgreementHistory_ACTIONS['ACTION_STATUS_UP_READY'], Agreement::AGREEMENT_STATUS_READY);
    }

    public function actionAgreementSign($id = null)
    {
        return $this->changeAgreementStatus($id, AgreementHistory::AgreementHistory_ACTIONS['ACTION_STATUS_UP_SIGNED'], Agreement::AGREEMENT_STATUS_SIGNED);
    }

    public function actionAgreementOverdue($id = null)
    {
        return $this->changeAgreementStatus($id, AgreementHistory::AgreementHistory_ACTIONS['ACTION_STATUS_UP_OVERDUE'], Agreement::AGREEMENT_STATUS_OVERDUE);
    }

    public function actionAgreementArchive($id = null)
    {
        return $this->changeAgreementStatus($id, AgreementHistory::AgreementHistory_ACTIONS['ACTION_STATUS_UP_ARCHIVED'], Agreement::AGREEMENT_STATUS_ARCHIVED);
    }

    public function actionAgreementCancel($id = null)
    {
        return $this->changeAgreementStatus($id, AgreementHistory::AgreementHistory_ACTIONS['ACTION_STATUS_UP_CANCELED'], Agreement::AGREEMENT_STATUS_CANCELED);
    }

    public function actionAgreementRollbackPreparing($id = null)
    {
        return $this->changeAgreementStatus($id, AgreementHistory::AgreementHistory_ACTIONS_DOWN['ACTION_STATUS_DOWN_PREPAR'], Agreement::AGREEMENT_STATUS_POTENCIAL);
    }

    public function actionAgreementRollbackNew($id = null)
    {
        return $this->changeAgreementStatus($id, AgreementHistory::AgreementHistory_ACTIONS_DOWN['ACTION_STATUS_DOWN_NEW'], Agreement::AGREEMENT_STATUS_NEW);
    }

    public function actionAgreementRollbackReady($id = null)
    {
        return $this->changeAgreementStatus($id, AgreementHistory::AgreementHistory_ACTIONS_DOWN['ACTION_STATUS_DOWN_READY'], Agreement::AGREEMENT_STATUS_READY);
    }

    public function actionAgreementRollbackSign($id = null)
    {
        return $this->changeAgreementStatus($id, AgreementHistory::AgreementHistory_ACTIONS_DOWN['ACTION_STATUS_DOWN_SIGN'], Agreement::AGREEMENT_STATUS_SIGNED);
    }

}