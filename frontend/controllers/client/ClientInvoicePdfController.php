<?php

namespace frontend\controllers\client;

use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\controllers\FilSMController;
use common\models\Files;
use common\models\user\FSMUser;
use common\models\client\Client;
use common\assets\ButtonDeleteAsset;

/**
 * ClientInvoicePdfController implements the CRUD actions for ClientInvoicePdf model.
 */
class ClientInvoicePdfController extends FilSMController
{

    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\client\ClientInvoicePdf';
        $this->defaultSearchModel = 'common\models\client\search\ClientInvoicePdfSearch';
    }
    
    /**
     * Lists all models.
     * @return mixed
     */
    public function actionIndex($client_id = null) {
        ButtonDeleteAsset::register(Yii::$app->getView());
        $isAdmin = FSMUser::getIsPortalAdmin();
        
        $searchModel = new $this->defaultSearchModel;
        $params = Yii::$app->request->getQueryParams();
        $dataProvider = $searchModel->search($params);

        $clientModel = !empty($client_id) ? Client::findOne($client_id) : new Client();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'clientModel' => $clientModel,
            'isAdmin' => $isAdmin,
        ]);
    } 
    
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($client_id = null) {
        $model = new $this->defaultModel;
        $filesModel = new Files();
        
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxValidation($model);
        }

        if ($model->load($requestPost)) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $file = $filesModel->uploadFile('footer/'.$model->client_id);
                if (!empty($file)) {
                    $filesModel->save();
                }
                $model->uploaded_file_id = $filesModel->id;            
                $model->save();
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirect(empty($client_id) ? 'index' : ['view', 'id' => $model->id, 'client_id' => $client_id]);
            }
        } else {
            $isAdmin = FSMUser::getIsPortalAdmin();
            $clientModel = !empty($client_id) ? Client::findOne($client_id) : new Client();
            $model->abonent_id = $model->userAbonentId;
            $model->client_id = $clientModel->id;
            $model->template_name = 'bill-base';
            
            $templateNameList = $model->templateNameList;
            return $this->render('create', [
                'model' => $model,
                'clientModel' => $clientModel,
                'filesModel' => $filesModel,
                'templateNameList' => $templateNameList,
                'isAdmin' => $isAdmin,
            ]);
        }
    }

    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $client_id = null) {
        $model = $this->findModel($id);
        $filesModel = $model->uploadedFile;
        $filesModel = (!empty($filesModel) ? $filesModel : new Files());
        
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxValidation($model);
        }

        if ($model->load($requestPost)) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $session = Yii::$app->session;
                $deletedIDs = (array)($session->has('file_delete') ? $session['file_delete'] : []);
                if(!empty($deletedIDs) && (in_array($filesModel->id, $deletedIDs))){
                    $filesModel->delete();
                    $filesModel = new Files();
                    $model->uploaded_file_id = null;
                }
                
                $file = $filesModel->uploadFile('footer/'.$model->client_id);
                if (!empty($file)) {
                    $filesModel->save();
                    $model->uploaded_file_id = $filesModel->id;
                }   
                $model->save();
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirectToBackUrl($id);
            }            
        } else {
            $this->rememberBackUrl($model->backURL, $id);            
            
            $isAdmin = FSMUser::getIsPortalAdmin();
            $clientModel = !empty($client_id) ? Client::findOne($client_id) : new Client();
            
            $templateNameList = $model->templateNameList;
            return $this->render('update', [
                'model' => $model,
                'clientModel' => $clientModel,
                'filesModel' => $filesModel,
                'templateNameList' => $templateNameList,
                'isAdmin' => $isAdmin,
            ]);
        }
    }  
        
    /**
     * Displays a single model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $client_id = null) {
        if(empty($id)){
            return $this->redirect(['create', 'client_id' => $client_id]);
        }
        
        ButtonDeleteAsset::register(Yii::$app->getView());
        
        $model = $this->findModel($id, true);
        $isAdmin = FSMUser::getIsPortalAdmin();
        $clientModel = !empty($client_id) ? Client::findOne($client_id) : new Client();
        $fileModel = $model->uploadedFile;
        $filePath = !empty($fileModel) ? $fileModel->uploadedFileUrl : null;
        
        return $this->render('view', [
            'model' => $this->findModel($id),
            'clientModel' => $clientModel,
            'filePath' => $filePath,
            'isAdmin' => $isAdmin,
        ]);
    }
    
    /**
     * Deletes an existing single model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $client_id = null) {
        $this->findModel($id)->delete();
        if(!empty($client_id)){
            return $this->redirect(['/client/view', 'id' => $client_id]);
        }else{
            return $this->redirect(['index']);
        }
    }      
}