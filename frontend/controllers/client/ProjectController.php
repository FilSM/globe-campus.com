<?php

namespace frontend\controllers\client;

use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

use common\controllers\FilSMController;
use common\components\FSMAccessHelper;
use common\models\client\Project;
use common\models\client\ProjectAttachment;
use common\assets\ButtonDeleteAsset;
use common\models\user\FSMUser;
use common\models\address\Country;
use common\models\Files;

/**
 * ProjectController implements the CRUD actions for Project model.
 */
class ProjectController extends FilSMController
{

    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\client\Project';
        $this->defaultSearchModel = 'common\models\client\search\ProjectSearch';
        $this->pjaxIndex = true;
    }

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [];
        $_get = Yii::$app->request->get();
        $id = isset($_get['id']) ? $_get['id'] : null;
        $model = !empty($id) ? $this->findModel($id) : null;
        $behaviors = ArrayHelper::merge(
            $behaviors, [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'actions' => ['ajax-get-model', 'ajax-get-agreement-list'],
                            'allow' => true,
                        ],                        
                        [
                            'actions' => ['index'],
                            'allow' => FSMAccessHelper::checkRoute('/project/*'),
                        ],
                        [
                            'actions' => ['view'],
                            'allow' => FSMAccessHelper::can('viewProject', $model),
                        ],
                        [
                            'actions' => ['create'],
                            'allow' => FSMAccessHelper::can('createProject'),
                        ],
                        [
                            'actions' => ['update', 'attachment-delete'],
                            'allow' => FSMAccessHelper::can('updateProject', $model),
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => FSMAccessHelper::can('deleteProject', $model),
                        ],
                    ],
                ],                
            ]
        );
        return $behaviors;
    }
    
    public function actionAjaxGetAgreementList() {
        Yii::$app->response->format = Response::FORMAT_JSON;        
        $out = [];
        $selected = '';
        if (isset($_POST['depdrop_parents'])) {  
            $id = end($_POST['depdrop_parents']);  
            if(!is_numeric($id) && !is_array($id)){  
                return ['output' => '', 'selected' => ''];  
            } 
            if(!is_array($id)){
                $list = $this->findModel($id)->activeAgreements; 
            }else{
                $projectList = Project::findAll($id);
                $resultList = [];
                foreach ($projectList as $project) {
                    $list = $project->activeAgreements;
                    foreach ($list as $i => $item) { 
                        $resultList[] = $item;
                    }
                }
                $list = $resultList;
            }
            if (count($list) > 0) {  
                foreach ($list as $i => $item) {  
                    if(!empty($_GET['without-id']) && ($_GET['without-id'] == $item['id'])){
                        continue;
                    }
                    $firstClient = $item->firstClient;
                    $secondClient = $item->secondClient;
                    $out[] = [
                        'id' => $item['id'], 
                        'name' => $item['number'].' ( '.$firstClient->name.' '.\IntlChar::chr(0x23E9).' '.$secondClient->name.' )', //"&#9205;"
                        /*
                        'options' => [
                            'style' => 'color:gray', 
                            'disabled' => true,
                        ]
                         * 
                         */
                    ];  
                }  
                if(empty($_GET['without-selected'])){
                    $selected = (count($list) == 1 ? strval($list[0]['id']) : '');
                }else{
                    $selected = '';
                }
            }else{
                $selected = '';  
            }
        }  
        return ['output' => $out, 'selected' => $selected];
    } 

    /**
     * Lists all models.
     * @return mixed
     */
    public function actionIndex() {
        ButtonDeleteAsset::register(Yii::$app->getView());
        
        $searchModel = new $this->defaultSearchModel;
        if(!$searchModel){
            $className = $this->className();
            throw new Exception("For the {$className} defaultSearchModel not defined.");
        }        
        $params = Yii::$app->request->getQueryParams();
        $params['deleted'] = (empty($params) || empty($params['ProjectSearch'])) ? 0 : (int)!empty($params['ProjectSearch']['deleted']);

        $dataProvider = $searchModel->search($params);

        $isAdmin = FSMUser::getIsPortalAdmin();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'isAdmin' => $isAdmin,
        ]);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new $this->defaultModel;
        $filesModel = [new Files()];

        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxValidation($model);
        }

        if ($model->load($requestPost)) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                if(!$model->save()){
                    throw new Exception('Unable to save data! ' . $model->errorMessage);
                }

                $files = UploadedFile::getInstances($filesModel[0], 'file');
                foreach ($files as $uploadedFile) {
                    $filesModel = new Files();
                    $file = $filesModel->uploadFileMultiply($uploadedFile, 'pdf/projects/'.$model->id);
                    if (!empty($file)) {
                        if(!$filesModel->save()){
                            throw new Exception('Can not save the PDF file! '.$filesModel->errorMessage);
                        }
                        $attachment = new ProjectAttachment();
                        $attachment->project_id = $model->id;
                        $attachment->uploaded_file_id = $filesModel->id;
                        if(!$attachment->save()){
                            throw new Exception('Can not save the Attachment! '.$attachment->errorMessage);
                        }
                    }
                }
                
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                if ($isPjax) {
                    return $this->actionAjaxModalNameList(['selected_id' => $model->id]);
                } else {
                    return $this->redirect('index');
                }            
            }            
        } else {
            Yii::$app->session->remove('file_delete');
            
            $isAdmin = FSMUser::getIsPortalAdmin();
            $countryList = Country::getNameArr();
            $model->abonent_id = $model->userAbonentId;
            
            $data = [
                'model' => $model,
                'filesModel' => $filesModel,
                'countryList' => $countryList,
                'isAdmin' => $isAdmin,
                'isModal' => $isAjax,
            ];
            if ($isAjax) {
                return $this->renderAjax('create', $data);
            } else {
                return $this->render('create', $data);
            }
        }
    }    

    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id, true);
        $filesModel = $model->attachmentFiles;
        $filesModel = (!empty($filesModel) ? $filesModel : [new Files()]);
        
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxValidation($model);
        }
        
        if ($model->load($requestPost)) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                if (!$model->save()) {
                    throw new Exception('Unable to save data! '.$model->errorMessage);
                }
                
                $session = Yii::$app->session;
                $deletedIDs = (array)($session->has('file_delete') ? $session['file_delete'] : []);
                if(!empty($deletedIDs)){
                    $attachmentList = $model->attachments;
                    foreach ($attachmentList as $attachment) {
                        if(in_array($attachment->uploaded_file_id, $deletedIDs)){
                            $attachment->delete();
                        }
                    }
                }

                $files = UploadedFile::getInstances($filesModel[0], 'file');
                foreach ($files as $uploadedFile) {
                    $filesModel = new Files();
                    $file = $filesModel->uploadFileMultiply($uploadedFile, 'pdf/projects/'.$model->id);
                    if (!empty($file)) {
                        if(!$filesModel->save()){
                            throw new Exception('Can not save the PDF file! '.$filesModel->errorMessage);
                        }
                        $attachment = new ProjectAttachment();
                        $attachment->project_id = $model->id;
                        $attachment->uploaded_file_id = $filesModel->id;
                        if(!$attachment->save()){
                            throw new Exception('Can not save the Attachment! '.$attachment->errorMessage);
                        }
                    }
                }
                
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirectToBackUrl($id);          
            }
        } else {
            $this->rememberBackUrl($model->backURL, $id);
            Yii::$app->session->remove('file_delete');
            
            $isAdmin = FSMUser::getIsPortalAdmin();
            $countryList = Country::getNameArr();
            return $this->render('update', [
                'model' => $model,
                'filesModel' => $filesModel,
                'countryList' => $countryList,
                'isAdmin' => $isAdmin,
            ]);
        }
    }      
    
    /**
     * Displays a single model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        ButtonDeleteAsset::register(Yii::$app->getView());
        $model = $this->findModel($id, true);
        $filesModel = $model->attachmentFiles;
        $filesModel = (!empty($filesModel) ? $filesModel : [new Files()]);
        
        $isAdmin = FSMUser::getIsPortalAdmin();
        return $this->render('view', [
            'model' => $model,
            'filesModel' => $filesModel,
            'isAdmin' => $isAdmin,
        ]);
    }    
    
}