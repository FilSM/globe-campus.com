<?php

namespace frontend\controllers\client;

use Yii;
use yii\web\Response;
use yii\helpers\Json;

use common\controllers\FilSMController;
use common\assets\ButtonDeleteAsset;
use common\models\mainclass\FSMBaseModel;
use common\models\user\FSMUser;
use common\models\Bank;
use common\models\Valuta;
use common\models\FileXML;
use common\models\FilePDF;
use common\models\client\Client;
use common\models\client\ClientBank;
use common\models\client\ClientBankBalance;

/**
 * ClientBankBalanceController implements the CRUD actions for ClientBankBalance model.
 */
class ClientBankBalanceController extends FilSMController
{

    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\client\ClientBankBalance';
        $this->defaultSearchModel = 'common\models\client\search\ClientBankBalanceSearch';
    }
    
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($project_id = null) {
        $model = new $this->defaultModel;
        $accountModel = new ClientBank();
        $filesXMLModel = new FileXML();
        $filesPDFModel = new FilePDF();

        $modelArr = [
            'ClientBankBalance' => $model,
            //'FilesXML' => $filesXMLModel,
            //'FilesPDF' => $filesPDFModel,
        ];
        
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            $result = $this->performAjaxMultipleValidation($modelArr);
            return $result;
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $client_id = !empty($model->account_id) ? $model->account->client_id : null;
                $file = $filesPDFModel->uploadFile('bank-balance'.(isset($client_id) ? '/'.$client_id : ''));
                if($file && !$filesPDFModel->save()){
                    throw new Exception(Yii::t('bill', 'The PDF file was saved with error!'));
                }
                $file = $filesXMLModel->uploadFile('bank-balance'.(isset($client_id) ? '/'.$client_id : ''));
                if($file && !$filesXMLModel->save()){
                    throw new Exception(Yii::t('bill', 'The XML file was saved with error!'));
                }
                $model->start_date = date('Y-m-d', strtotime($model->start_date));
                $model->end_date = date('Y-m-d', strtotime($model->end_date));                
                $model->uploaded_file_id = $filesXMLModel->id;
                $model->uploaded_pdf_file_id = $filesPDFModel->id;
                $model->currency = !empty($_POST['valuta_id']) ? Valuta::findOne($_POST['valuta_id'])->name : '';
                if (!$model->save()) {
                    throw new Exception(Yii::t('bill', 'The XML file data was saved with error!'));
                }

                $clientBankAccount = ClientBank::findOne($model->account_id);
                if(!$clientBankAccount->updateAttributes([
                        'uploaded_file_id' => $model->uploaded_file_id,
                        'uploaded_pdf_file_id' => $model->uploaded_pdf_file_id,
                        'start_date' => $model->start_date, 
                        'end_date' => $model->end_date,
                        'balance' => $model->balance,
                        'currency' => $model->currency,
                    ])){
                    throw new Exception(Yii::t('bill', 'The bank account data was saved with error!'));
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $filesXMLModel->delete();
                $filesPDFModel->delete();
                $transaction->rollBack();
                $message = $e->getMessage();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                if ($isPjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return 'reload';
                }else{
                    return $this->redirect('index');
                }                 
            }                
        } else {
            $isAdmin = FSMUser::getIsPortalAdmin();
            
            $model->valuta_id = Valuta::VALUTA_DEFAULT;
            
            $clientList = Client::getNameArr(['client.deleted' => 0]);
            $bankList = Bank::getNameArr(['enabled' => true], '', '', function($bank){return $bank['name'].(!empty($bank['swift']) ? ' | '.$bank['swift'] : '');});
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            
            $data = [
                'model' => $model,
                'accountModel' => $accountModel,
                'filesXMLModel' => $filesXMLModel,
                'filesPDFModel' => $filesPDFModel,
                'clientList' => $clientList,
                'bankList' => $bankList,
                'valutaList' => $valutaList,
                'isAdmin' => $isAdmin,
                'isModal' => $isAjax,
            ];
            
            return ($isAjax ? $this->renderAjax('create', $data) : $this->render('create', $data));
        }
    }
    
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $accountModel = $model->account;
        $filesXMLModel = $model->fileXml;
        $filesXMLModel = !empty($filesXMLModel) ? $filesXMLModel : new FileXML();
        $filesPDFModel = $model->filePdf;
        $filesPDFModel = !empty($filesPDFModel) ? $filesPDFModel : new FilePDF();

        $modelArr = [
            'ClientBankBalance' => $model,
        ];
        
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && $isAjax) {
            return $this->performAjaxMultipleValidation($modelArr);
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $client_id = !empty($model->account_id) ? $model->account->client_id : null;
                $session = Yii::$app->session;
                $deletedIDs = (array)($session->has('xml_delete') ? $session['xml_delete'] : []);
                if(!empty($deletedIDs) && (in_array($filesXMLModel->id, $deletedIDs))){
                    $filesXMLModel->delete();
                    $filesXMLModel = new FileXML();
                    $model->uploaded_file_id = null;
                }
                $file = $filesXMLModel->uploadFile('bank-balance'.(isset($client_id) ? '/'.$client_id : ''));
                if (!empty($file)) {
                    if ($filesXMLModel->save()){
                        $model->uploaded_file_id = $filesXMLModel->id;
                    }else{
                        throw new Exception(Yii::t('bill', 'The XML file was saved with error!'));
                    }
                }
                
                $deletedIDs = (array)($session->has('pdf_delete') ? $session['pdf_delete'] : []);
                if(!empty($deletedIDs) && (in_array($filesPDFModel->id, $deletedIDs))){
                    $filesPDFModel->delete();
                    $filesPDFModel = new FilePDF();
                    $model->uploaded_pdf_file_id = null;
                }
                $file = $filesPDFModel->uploadFile('bank-balance'.(isset($client_id) ? '/'.$client_id : ''));
                if (!empty($file)) {
                    if($filesPDFModel->save()){
                        $model->uploaded_file_id = $filesPDFModel->id;
                    }else{
                        throw new Exception(Yii::t('bill', 'The PDF file was saved with error!'));
                    }
                }
                
                $model->start_date = date('Y-m-d', strtotime($model->start_date));
                $model->end_date = date('Y-m-d', strtotime($model->end_date));                
                $model->uploaded_file_id = $filesXMLModel->id;
                $model->uploaded_pdf_file_id = $filesPDFModel->id;
                $model->currency = !empty($_POST['valuta_id']) ? Valuta::findOne($_POST['valuta_id'])->name : '';
                if (!$model->save()) {
                    throw new Exception(Yii::t('bill', 'The XML file data was saved with error!'));
                }
                $account = $model->account->account;
                $clientBankAccount = ClientBank::findOne([
                    'account' => $account,
                    'deleted' => false,
                ]);
                if(!$clientBankAccount->updateAttributes([
                        'uploaded_file_id' => $model->uploaded_file_id,
                        'uploaded_pdf_file_id' => $model->uploaded_pdf_file_id,
                        'start_date' => $model->start_date, 
                        'end_date' => $model->end_date,
                        'balance' => $model->balance,
                        'currency' => $model->currency,
                    ])){
                    throw new Exception(Yii::t('bill', 'The bank account data was saved with error!'));
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirectToBackUrl($id);
            }                
        } else {
            $this->rememberBackUrl($model->backURL, $id);
            Yii::$app->session->remove('xml_delete');
            Yii::$app->session->remove('pdf_delete');
            
            $model->start_date = date('d-m-Y', strtotime($model->start_date));
            $model->end_date = date('d-m-Y', strtotime($model->end_date));            
            $model->valuta_id = !empty($model->currency) ? Valuta::findOne(['name' => $model->currency])->id : null;
                    
            $isAdmin = FSMUser::getIsPortalAdmin();
            $clientList = Client::getNameArr(['client.deleted' => 0]);
            $bankList = Bank::getNameArr(['enabled' => true], '', '', function($bank){return $bank['name'].(!empty($bank['swift']) ? ' | '.$bank['swift'] : '');});
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            return $this->render('update', [
                'model' => $model,
                'accountModel' => $accountModel,
                'filesXMLModel' => $filesXMLModel,
                'filesPDFModel' => $filesPDFModel,
                'clientList' => $clientList,
                'bankList' => $bankList,
                'valutaList' => $valutaList,
                'isAdmin' => $isAdmin,
                'isModal' => false,
            ]);
        }
    }
    
    /**
     * Lists all models.
     * @return mixed
     */
    public function actionReport($account_id = null) {
        ButtonDeleteAsset::register(Yii::$app->getView());
        
        $searchModel = new $this->defaultSearchModel;
        $params = Yii::$app->request->getQueryParams();
        $dataProvider = $searchModel->search($params);
        
        $isAdmin = FSMUser::getIsPortalAdmin();
        $valutaList = Valuta::getNameArr(['enabled' => true]);
        return $this->render('report-bank-statement', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'valutaList' => $valutaList,
            'isAdmin' => $isAdmin,
        ]);
    } 

    public function actionXmlDelete($deleted_id)
    {
        $session = Yii::$app->session;
        $deletedIds = (array)($session->has('xml_delete') ? $session['xml_delete'] : []);
        if(!in_array($deleted_id, $deletedIds)){
            array_push($deletedIds, $deleted_id);
        }
        $session->set('xml_delete', $deletedIds);
        echo Json::encode(['output' => true]);
        return false;
    }   

    public function actionPdfDelete($deleted_id)
    {
        $session = Yii::$app->session;
        $deletedIds = (array)($session->has('pdf_delete') ? $session['pdf_delete'] : []);
        if(!in_array($deleted_id, $deletedIds)){
            array_push($deletedIds, $deleted_id);
        }
        $session->set('pdf_delete', $deletedIds);
        echo Json::encode(['output' => true]);
        return false;
    }   

    public function actionDelete($id)
    {
        $model = $this->findModel($id, true);
        $transaction = Yii::$app->getDb()->beginTransaction();
        $this->rememberBackUrl($model->backURL, $id);
        try {
            $account = $model->account;
            if ($model->delete()) {
                $clientBankBalanceList = ClientBankBalance::findByCondition(['account_id' => $account->id])
                    ->orderBy('id DESC')
                    ->asArray()
                    ->all();
                $clientBankAccount = ClientBank::findOne([
                    'account' => $account->account,
                    'deleted' => false,
                ]);
                
                if(!empty($clientBankAccount)){
                    if(count($clientBankBalanceList) == 0){
                        $data = [
                            'uploaded_file_id' => null,
                            'uploaded_pdf_file_id' => null,
                            'start_date' => null, 
                            'end_date' => null,
                            'balance' => null,
                            'currency' => null,
                        ];
                    }else{
                        $lastClientBankBalance = $clientBankBalanceList[0];
                        $data = [
                            'uploaded_file_id' => $lastClientBankBalance['uploaded_file_id'],
                            'uploaded_pdf_file_id' => $lastClientBankBalance['uploaded_pdf_file_id'],
                            'start_date' => $lastClientBankBalance['start_date'], 
                            'end_date' => $lastClientBankBalance['end_date'],
                            'balance' => $lastClientBankBalance['balance'],
                            'currency' => $lastClientBankBalance['currency'],
                        ];
                    }
                    $clientBankAccount->updateAttributes($data);
                }
                $transaction->commit();
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $message = $e->getMessage();
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
        } finally {
            return $this->redirectToBackUrl($id);
        }
    }    
}