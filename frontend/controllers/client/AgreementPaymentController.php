<?php

namespace frontend\controllers\client;

use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use common\assets\ButtonDeleteAsset;
use common\controllers\FilSMController;
use common\models\client\AgreementHistory;
use common\models\client\Agreement;
use common\models\client\AgreementPayment;
use common\models\Bank;
use common\models\Valuta;
use common\models\mainclass\FSMBaseModel;
use common\models\user\FSMUser;
use common\models\bill\BillConfirm;
use common\models\bill\PaymentConfirm;

/**
 * AgreementPaymentController implements the CRUD actions for AgreementPayment model.
 */
class AgreementPaymentController extends FilSMController
{

    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\client\AgreementPayment';
        $this->defaultSearchModel = 'common\models\client\search\AgreementPaymentSearch';
        $this->pjaxIndex = true;
    }

    public function actionAjaxConfirmPayment($id)
    {
        $model = $this->findModel($id);
        $agreementModel = $model->agreement;
        $agreementModel->summa = number_format($model->summa, 2, '.', '') . (!empty($model->valuta_id) ? ' ' . $model->valuta->name : '');

        $historyModel = new AgreementHistory();
        $historyModel->agreement_id = $agreementModel->id;
        $historyModel->action_id = AgreementHistory::AgreementHistory_ACTIONS['ACTION_CONFIRM_PAYMENT'];
        $historyModel->create_time = date('d-m-Y H:i:s');
        $historyModel->create_user_id = Yii::$app->user->id;

        $modelArr = [
            'AgreementPayment' => $model,
        ];

        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxMultipleValidation($modelArr);
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $historyModel->create_time = date('Y-m-d H:i:s', strtotime($historyModel->create_time));
                $historyModel->comment = Yii::t('common', 'Sum') . ': ' . $agreementModel->summa;
                if (!$historyModel->save()) {
                    throw new Exception('Unable to save data! ' . $historyModel->errorMessage);
                }
                $model->paid_date = date('Y-m-d', strtotime($model->paid_date));
                $model->confirmed = date('Y-m-d');
                if (!$model->save()) {
                    throw new Exception('Unable to save data! ' . $model->errorMessage);
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return 'reload';
            }
        } else {
            $model->paid_date = date('d-m-Y');
            return $this->renderAjax('confirm-payment', [
                'model' => $model,
                'agreementModel' => $agreementModel,
                'historyModel' => $historyModel,
                'isModal' => true,
            ]);
        }
    }

    /**
     * Lists all models.
     * @return mixed
     */
    public function actionIndex()
    {
        ButtonDeleteAsset::register(Yii::$app->getView());

        $searchModel = new $this->defaultSearchModel;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $bankList = Bank::getNameArr(['enabled' => true], '', '', function($bank){return $bank['name'].(!empty($bank['swift']) ? ' | '.$bank['swift'] : '');});

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'bankList' => $bankList,
        ]);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAjaxCreate($id)
    {
        $model = new $this->defaultModel;
        $model->scenario = 'create';
        $historyModel = new AgreementHistory();
        $agreementModel = !empty($id) ? Agreement::findOne($id) : new Agreement();

        $model->agreement_id = !empty($id) ? $id : null;

        $historyModel->agreement_id = !empty($id) ? $id : null;
        $historyModel->action_id = AgreementHistory::AgreementHistory_ACTIONS['ACTION_PAYMENT_CREDIT'];
        $historyModel->create_time = date('d-m-Y H:i:s');
        $historyModel->create_user_id = Yii::$app->user->id;

        $modelArr = [
            'AgreementPayment' => $model,
            'AgreementHistory' => $historyModel,
        ];

        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxMultipleValidation($modelArr);
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $historyModel->action_id = ($model->direction == AgreementPayment::DIRECTION_CREDIT ?
                        AgreementHistory::AgreementHistory_ACTIONS['ACTION_PAYMENT_CREDIT'] :
                        AgreementHistory::AgreementHistory_ACTIONS['ACTION_PAYMENT_DEBET']);
                $historyModel->create_time = date('Y-m-d H:i:s', strtotime($historyModel->create_time));
                $historyModel->comment .= (!empty($historyModel->comment) ? "\n" : '') . Yii::t('common', 'Sum') . ': ' . number_format($model->summa, 2, '.', ' ') .
                        ($model->rate != 1 ? "\n" . $model->getAttributeLabel('rate') . ': ' . $model->rate . ' | ' .
                        $model->getAttributeLabel('summa_eur') . ': ' . $model->summa_eur : '');
                if (!$historyModel->save()) {
                    $transaction->rollBack();
                    throw new Exception('Unable to save data! ' . $historyModel->errorMessage);
                }
                $model->agreement_history_id = $historyModel->id;
                $model->rate = ($model->valuta_id == Valuta::VALUTA_DEFAULT) ? 1 : $model->rate;
                $model->summa_eur = $model->summa / $model->rate;
                $model->paid_date = date('Y-m-d', strtotime($model->paid_date));
                if (!$model->save()) {
                    $transaction->rollBack();
                    throw new Exception('Unable to save data! ' . $model->errorMessage);
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return 'reload';
            }
        } else {
            $agreementSumma = $agreementModel->summa;
            $paymentsSummaCredit = $agreementModel->paymentsSummaCredit;
            $paymentsSummaDebet = $agreementModel->paymentsSummaDebet;
            if ($agreementSumma > $paymentsSummaCredit) {
                $model->summa = number_format($agreementModel->summa - $paymentsSummaCredit, 2, '.', '');
                $model->direction = AgreementPayment::DIRECTION_CREDIT;
            } elseif ($agreementSumma > $paymentsSummaDebet) {
                $model->summa = number_format($agreementModel->summa - $paymentsSummaDebet, 2, '.', '');
                $model->direction = AgreementPayment::DIRECTION_DEBET;
            } elseif (($agreementSumma <= $paymentsSummaDebet) && ($agreementSumma <= $paymentsSummaDebet)) {
                Yii::$app->getSession()->setFlash('error', Yii::t('agreement', 'No payment can be made for this agreement.'));
                Yii::$app->response->format = Response::FORMAT_JSON;
                return 'reload';
            }
            $canCredit = ($agreementSumma > $paymentsSummaCredit);
            $canDebet = ($agreementSumma > $paymentsSummaDebet);
            $showDirection = $canCredit && $canDebet;
            $model->valuta_id = Valuta::VALUTA_DEFAULT;
            $valutaList = Valuta::getNameArr(['enabled' => true]);

            if (!empty($id)) {
                $bankFromList = $agreementModel->secondClient->clientActiveBanks;
                $bankToList = $agreementModel->firstClient->clientActiveBanks;

                $arr = [];
                foreach ($bankFromList as $bank) {
                    $bankModel = $bank->bank;
                    $arr[$bank->bank_id] = $bankModel->name.(!empty($bankModel->swift) ? ' | '.$bankModel->swift : '');
                }
                $bankFromArr = $arr;

                $arr = [];
                foreach ($bankToList as $bank) {
                    $bankModel = $bank->bank;
                    $arr[$bank->bank_id] = $bankModel->name.(!empty($bankModel->swift) ? ' | '.$bankModel->swift : '');
                }
                $bankToArr = $arr;

                $bankFromList = $bankToList = $bankFromArr + $bankToArr;

                $model->to_bank_id = !empty($bankFromArr) ? array_keys($bankFromArr)[0] : null;
                $model->from_bank_id = !empty($bankToArr) ? array_keys($bankToArr)[0] : null;
            } else {
                $bankFromList = $bankToList = Bank::getNameArr(['enabled' => true], '', '', function($bank){return $bank['name'].(!empty($bank['swift']) ? ' | '.$bank['swift'] : '');});
            }

            return $this->renderAjax('create', [
                        'model' => $model,
                        'historyModel' => $historyModel,
                        'bankFromList' => $bankFromList,
                        'bankToList' => $bankToList,
                        'valutaList' => $valutaList,
                        'showDirection' => $showDirection,
                        'isModal' => true,
            ]);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAjaxUpdate($id)
    {
        $agreement_id = !empty($_GET['agreement_id']) ? $_GET['agreement_id'] : null;
        $model = $this->findModel($id, true);
        $model->scenario = 'update';
        $agreementModel = $model->agreement;

        $historyModel = new AgreementHistory();
        $historyModel->agreement_id = !empty($agreementModel->id) ? $agreementModel->id : null;
        $historyModel->action_id = AgreementHistory::AgreementHistory_ACTIONS['ACTION_EDIT_PAYMENT'];
        $historyModel->create_time = date('d-m-Y H:i:s');
        $historyModel->create_user_id = Yii::$app->user->id;

        $modelArr = [
            'AgreementPayment' => $model,
            'AgreementHistory' => $historyModel,
        ];

        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxMultipleValidation($modelArr);
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $model->summa_eur = $model->summa / $model->rate;
                $model->paid_date = date('Y-m-d', strtotime($model->paid_date));

                $historyModel->comment .= (!empty($historyModel->comment) ? "\n" : '') . Yii::t('common', 'Sum') . ': ' . number_format($model->summa, 2, '.', ' ') .
                        ($model->rate != 1 ? "\n" . $model->getAttributeLabel('rate') . ': ' . $model->rate . ' | ' .
                        $model->getAttributeLabel('summa_eur') . ': ' . $model->summa_eur : '');
                $historyModel->create_time = date('Y-m-d H:i:s', strtotime($historyModel->create_time));
                if (!$historyModel->save()) {
                    $transaction->rollBack();
                    throw new Exception('Unable to save data! ' . $historyModel->errorMessage);
                }
                $model->agreement_history_id = $historyModel->id;
                if (!$model->save()) {
                    $transaction->rollBack();
                    throw new Exception('Unable to save data! ' . $model->errorMessage);
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirectToBackUrl($id);
            }
        } else {
            $this->rememberBackUrl($model->backURL, $id);

            $agreementSumma = $agreementModel->summa;
            $paymentsSummaCredit = $agreementModel->paymentsSummaCredit;
            $paymentsSummaDebet = $agreementModel->paymentsSummaDebet;

            if ($model->direction == AgreementPayment::DIRECTION_CREDIT) {
                $paymentsSummaCredit -= $model->summa;
            } else {
                $paymentsSummaDebet -= $model->summa;
            }

            $canCredit = ($agreementSumma > $paymentsSummaCredit);
            $canDebet = ($agreementSumma > $paymentsSummaDebet);
            $showDirection = $canCredit && $canDebet;

            $valutaList = Valuta::getNameArr(['enabled' => true]);

            $bankFromList = $agreementModel->secondClient->clientActiveBanks;
            $bankToList = $agreementModel->firstClient->clientActiveBanks;

            $arr = [];
            foreach ($bankFromList as $bank) {
                $bankModel = $bank->bank;
                $arr[$bank->bank_id] = $bankModel->name.(!empty($bankModel->swift) ? ' | '.$bankModel->swift : '');
            }
            $bankFromArr = $arr;

            $arr = [];
            foreach ($bankToList as $bank) {
                $bankModel = $bank->bank;
                $arr[$bank->bank_id] = $bankModel->name.(!empty($bankModel->swift) ? ' | '.$bankModel->swift : '');
            }
            $bankToArr = $arr;

            $bankFromList = $bankToList = $bankFromArr + $bankToArr;

            $model->to_bank_id = !empty($bankFromArr) ? array_keys($bankFromArr)[0] : null;
            $model->from_bank_id = !empty($bankToArr) ? array_keys($bankToArr)[0] : null;

            $model->paid_date = date('d-m-Y', strtotime($model->paid_date));
            return $this->renderAjax('update', [
                        'model' => $model,
                        'historyModel' => $historyModel,
                        'bankFromList' => $bankFromList,
                        'bankToList' => $bankToList,
                        'valutaList' => $valutaList,
                        'showDirection' => $showDirection,
                        'isModal' => true,
            ]);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, true);
        $model->scenario = 'update';
        $agreementModel = $model->agreement;

        $historyModel = new AgreementHistory();
        $historyModel->agreement_id = !empty($agreementModel->id) ? $agreementModel->id : null;
        $historyModel->action_id = AgreementHistory::AgreementHistory_ACTIONS['ACTION_EDIT_PAYMENT'];
        $historyModel->create_time = date('d-m-Y H:i:s');
        $historyModel->create_user_id = Yii::$app->user->id;

        $modelArr = [
            'AgreementPayment' => $model,
            'AgreementHistory' => $historyModel,
        ];

        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxMultipleValidation($modelArr);
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $model->summa_eur = $model->summa / $model->rate;
                $model->paid_date = date('Y-m-d', strtotime($model->paid_date));

                $historyModel->comment .= (!empty($historyModel->comment) ? "\n" : '') . Yii::t('common', 'Sum') . ': ' . number_format($model->summa, 2, '.', ' ') .
                        ($model->rate != 1 ? "\n" . $model->getAttributeLabel('rate') . ': ' . $model->rate . ' | ' .
                        $model->getAttributeLabel('summa_eur') . ': ' . $model->summa_eur : '');
                $historyModel->create_time = date('Y-m-d H:i:s', strtotime($historyModel->create_time));
                if (!$historyModel->save()) {
                    $transaction->rollBack();
                    throw new Exception('Unable to save data! ' . $historyModel->errorMessage);
                }
                $model->agreement_history_id = $historyModel->id;
                if (!$model->save()) {
                    $transaction->rollBack();
                    throw new Exception('Unable to save data! ' . $model->errorMessage);
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirectToBackUrl($id);
            }
        } else {
            $this->rememberBackUrl($model->backURL, $id);

            $agreementSumma = $agreementModel->summa;
            $paymentsSummaCredit = $agreementModel->paymentsSummaCredit;
            $paymentsSummaDebet = $agreementModel->paymentsSummaDebet;

            if ($model->direction == AgreementPayment::DIRECTION_CREDIT) {
                $paymentsSummaCredit -= $model->summa;
            } else {
                $paymentsSummaDebet -= $model->summa;
            }

            $canCredit = ($agreementSumma > $paymentsSummaCredit);
            $canDebet = ($agreementSumma > $paymentsSummaDebet);
            $showDirection = $canCredit && $canDebet;

            $valutaList = Valuta::getNameArr(['enabled' => true]);

            $bankFromList = $agreementModel->secondClient->clientActiveBanks;
            $bankToList = $agreementModel->firstClient->clientActiveBanks;

            $arr = [];
            foreach ($bankFromList as $bank) {
                $bankModel = $bank->bank;
                $arr[$bank->bank_id] = $bankModel->name.(!empty($bankModel->swift) ? ' | '.$bankModel->swift : '');
            }
            $bankFromArr = $arr;

            $arr = [];
            foreach ($bankToList as $bank) {
                $bankModel = $bank->bank;
                $arr[$bank->bank_id] = $bankModel->name.(!empty($bankModel->swift) ? ' | '.$bankModel->swift : '');
            }
            $bankToArr = $arr;

            $bankFromList = $bankToList = $bankFromArr + $bankToArr;

            $model->to_bank_id = !empty($bankFromArr) ? array_keys($bankFromArr)[0] : null;
            $model->from_bank_id = !empty($bankToArr) ? array_keys($bankToArr)[0] : null;

            $model->paid_date = date('d-m-Y', strtotime($model->paid_date));
            return $this->render('update', [
                        'model' => $model,
                        'historyModel' => $historyModel,
                        'bankFromList' => $bankFromList,
                        'bankToList' => $bankToList,
                        'valutaList' => $valutaList,
                        'showDirection' => $showDirection,
            ]);
        }
    }

    /**
     * Deletes an existing single model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $backUrl = $model->backURL;
        $backUrl = is_array($backUrl) ? $backUrl[0] : $backUrl;
        $pjax = $this->pjaxIndex && (strpos($backUrl, 'agreement-payment/view') === false);

        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            $historyModel = new AgreementHistory();
            $historyModel->agreement_id = $model->agreement_id;
            $historyModel->action_id = AgreementHistory::AgreementHistory_ACTIONS['ACTION_DELETE_PAYMENT'];
            $historyModel->create_time = date('d-m-Y H:i:s');
            $historyModel->create_user_id = Yii::$app->user->id;
            $historyModel->comment = (isset($model->getPaymentDirectionList()[$model->direction]) ? $model->getPaymentDirectionList()[$model->direction] . ' ' : '') .
                Yii::t('common', 'Sum') . ': ' . number_format($model->summa, 2, '.', ' ') .
                ($model->rate != 1 ? "\n" . $model->getAttributeLabel('rate') . ': ' . $model->rate . ' | ' .
                $model->getAttributeLabel('summa_eur') . ': ' . $model->summa_eur : '');

            if (!$historyModel->save()) {
                $transaction->rollBack();
                throw new Exception('Unable to save data! ' . $historyModel->errorMessage);
            }

            $model->delete();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            $message = $e->getMessage();
            if (!$pjax) {
                Yii::$app->getSession()->setFlash('error', $message);
            }
            Yii::error($message, __METHOD__);
        } finally {
            if (!$pjax) {
                return $this->redirect($backUrl);
            } else {
                return;
            }
        }
    }

}
