<?php

namespace frontend\controllers\client;

use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;

use kartik\helpers\Html;

use common\controllers\FilSMController;
use common\components\FSMAccessHelper;
use common\models\mainclass\FSMBaseModel;
use common\models\user\FSMUser;
use common\models\user\FSMProfile;
use common\models\Bank;
use common\models\Valuta;
use common\models\Language;
use common\models\Files;
use common\models\abonent\Abonent;
use common\models\abonent\AbonentClient;
use common\models\client\Client;
use common\models\client\ClientGroup;
use common\models\address\Country;
use common\models\client\ClientBank;
use common\models\client\search\ClientBankSearch;
use common\models\client\ClientContact;
use common\components\FSMLursoft;
use Naucon\Iban\Iban;
use frontend\assets\ClientUIAsset;
use common\assets\ButtonDeleteAsset;

/**
 * ClientController implements the CRUD actions for Client model.
 */
class ClientController extends FilSMController
{

    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\client\Client';
        $this->defaultSearchModel = 'common\models\client\search\ClientSearch';
        $this->pjaxIndex = true;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [];
        $_get = Yii::$app->request->get();
        $id = isset($_get['id']) ? $_get['id'] : null;
        $model = !empty($id) ? $this->findModel($id) : null;
        $behaviors = ArrayHelper::merge(
            $behaviors, [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'actions' => [
                                'ajax-get-model', 
                                'ajax-check-iban',
                                'ajax-modal-name-list',
                                'ajax-get-client-account-list',
                                'ajax-get-client-bank-list',
                                'ajax-get-client-list-by-abonent',
                                'ajax-get-client-person-list',
                                'ajax-get-lursoft-data',
                                'ajax-get-project-list',
                                'ajax-get-tax',
                                'ajax-get-vies-data',
                                'ajax-main-client-name-list',
                                'ajax-get-main-contact',
                                'ajax-name-list',
                                'logo-delete',
                                'register',
                            ],
                            'allow' => true,
                        ],
                        [
                            'actions' => ['index'],
                            'allow' => FSMAccessHelper::checkRoute('/client/*'),
                        ],
                        [
                            'actions' => ['view'],
                            'allow' => FSMAccessHelper::can('viewClient', $model),
                        ],
                        [
                            'actions' => ['create'],
                            'allow' => FSMAccessHelper::can('createClient'),
                        ],
                        [
                            'actions' => ['update'],
                            'allow' => FSMAccessHelper::can('updateClient', $model),
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => FSMAccessHelper::can('deleteClient', $model),
                        ],
                        [
                            'actions' => ['staff'],
                            'allow' => FSMAccessHelper::can('viewStaff', $model),
                        ],
                    ],
                ],                
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'ajax-get-model' => ['get'],
                    ],
                ],
            ]
        );
        return $behaviors;
    }

    public function actionAjaxGetModel($id)
    {
        if (empty($id)) {
            return [];
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out['client'] = Client::findOne($id);
        $out['client_address_text'] = $out['client']->legal_address;
        return $out;
    }

    public function actionAjaxGetLursoftData()
    {
        $params = Yii::$app->request->get();
        $service = new FSMLursoft();
        $data = $service->search($params);
        unset($service);
        if (!empty($data['result']) && !empty($data['answer'])) {
            $answer = (array) $data['answer'];
            $person = (array) $answer['person'];
            $result = [];
            foreach ($person as $key => $value) {
                if (!is_string($value)) {
                    continue;
                }
                $result[$key] = $value;
            }
            $result['address'] = (array) $person['address'];
            $data = $result;
        } else {
            $data = !empty($data['message']) ? ['message' => $data['message']] : [];
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $data;
    }

    public function actionAjaxGetViesData()
    {
        $params = Yii::$app->request->get();
        $model = new $this->defaultModel;
        $data = $model->checkViesData($params);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $data;
    }

    public function actionAjaxCheckIban()
    {
        $params = Yii::$app->request->get();
        $result = false;
        if (!empty($params['iban'])) {
            $iban = new Iban($params['iban']);
            $result = $iban->isValid();
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $result;
    }

    public function actionAjaxGetMainContact($id)
    {
        if (empty($id)) {
            return [];
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        $clientModel = Client::findOne($id);
        $out['client_type'] = $clientModel->client_type;
        $out['contact'] = ClientContact::findOne(['client_id' => $id, 'main' => 1]);
        return $out;
    }
    
    /**
     * Displays the registration page.
     * After successful registration if enableConfirmation is enabled shows info message otherwise redirects to home page.
     *
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionRegister()
    {
        $userModule = Yii::createObject(\dektrium\user\Module::class, ['dektrium-user-module']);
        if (!$userModule->enableRegistration) {
            throw new Exception();
        }

        $model = new $this->defaultModel;
        $userModel = new FSMUser();
        $userModel->setScenario('from-client');

        $modelArr = [
            'Client' => $model,
            'FSMUser' => $userModel,
        ];

        // ajax validation
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && $isAjax) {
            return $this->performAjaxMultipleValidation($modelArr);
        }
	
        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {

                if (!$model->save()) {
                    throw new Exception('Unable to save data! ' . $model->errorMessage);
                }

                if (empty($userModel->username)) {
                    $userModel->username = $userModel->generateUsername();
                }
                if (!$userModel->register/* create */()) {
                    throw new Exception('Unable to save data! ' . $userModel->errorMessage);
                } else {
                    if ($profile = $userModel->profile) {
                        $profile->updateAttributes([
                            'client_id' => $model->id,
                            'language_id' => $model->language_id,
                        ]);
                        if ($model->client_type == Client::CLIENT_TYPE_PHYSICAL) {
                            $profile->updateAttributes([
                                'name' => $model->name,
                            ]);
                        }
                    }
                    Yii::$app->session->setFlash(
                            'info', Yii::t('user', 'Your account has been created and a message with further instructions has been sent to your email')
                    );
                }

                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->goHome();
            }
        } else {
            ClientUIAsset::register(Yii::$app->getView());

            $model['status'] = Client::CLIENT_STATUS_ACTIVE;
            //$model['it_is'] = Client::IT_IS_CLIENT;

            $countryList = Country::getNameArr();
            $bankList = Bank::getNameArr(['enabled' => true], '', '', function($bank){return $bank['name'].(!empty($bank['swift']) ? ' | '.$bank['swift'] : '');});
            $languageList = \common\models\Language::getEnabledLanguageList();
            return $this->render('create', [
                'model' => $model,
                'userModel' => $userModel,
                'countryList' => $countryList,
                'languageList' => $languageList,
                'bankList' => $bankList,
                'managerList' => [],
                'isAdmin' => false,
                'itIs' => null,
                'isModal' => false,
                'registerAction' => true,
            ]);
        }
    }

    /**
     * Lists all models.
     * @return mixed
     */
    public function actionIndex()
    {
        ButtonDeleteAsset::register(Yii::$app->getView());

        $searchModel = new $this->defaultSearchModel;
        $params = Yii::$app->request->getQueryParams();
        $params['deleted'] = (empty($params) || empty($params['ClientSearch'])) ? 0 : (int)!empty($params['ClientSearch']['deleted']);

        $dataProvider = $searchModel->search($params);

        $withoutTypes = [];
        if (!FSMUser::getIsPortalAdmin()) {
            $withoutTypes[] = Client::CLIENT_IT_IS_OWNER;
        }
        $itIsList = $searchModel->clientItIsList;
        foreach ($withoutTypes as $type) {
            unset($itIsList[$type]);
        }

        $isAdmin = FSMUser::getIsPortalAdmin();
        $bankList = Bank::getNameArr(['enabled' => true], '', '', function($bank){return $bank['name'].(!empty($bank['swift']) ? ' | '.$bank['swift'] : '');});
        $languageList = Language::getEnabledLanguageList();
        $countryList = Country::getNameArr();
        $managerList = FSMProfile::getProfileListByRole([
            FSMUser::USER_ROLE_ADMIN,
            FSMUser::USER_ROLE_OPERATOR, 
            FSMUser::USER_ROLE_BOOKER,
            FSMUser::USER_ROLE_COORDINATOR,
            FSMUser::USER_ROLE_MANAGER,
            FSMUser::USER_ROLE_TEHNICAL,
            FSMUser::USER_ROLE_LEGAL_TAX,
        ]);
        $agentList = FSMProfile::getProfileListByRole([
            FSMUser::USER_ROLE_AGENT,
        ]);
        $profile = Yii::$app->user->identity->profile;
        $profileId = isset($profile) ? $profile->id : null;
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'itIsList' => $itIsList,
            'languageList' => $languageList,
            'bankList' => $bankList,
            'countryList' => $countryList,
            'managerList' => $managerList,
            'agentList' => $agentList,
            'isAdmin' => $isAdmin,
            'profileId' => $profileId,
        ]);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new $this->defaultModel;
        $filesModel = new Files();
        $abonentModel = new AbonentClient();
        $mainContactModel = new ClientContact();
        $mainContactModel->scenario = 'client';

        $modelArr = [
            'Client' => $model,
            'AbonentClient' => $abonentModel,
            'ClientContact' => $mainContactModel,
        ];

        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            // ajax validation
            $ajaxModelArr = $modelArr;
            $clientBankModel = FSMBaseModel::createMultiple(ClientBank::class);
            FSMBaseModel::loadMultiple($clientBankModel, $requestPost);
            foreach ($clientBankModel as $index => $clientBank) {
                if (empty($clientBank->bank_id) && empty($clientBank->account) && empty($clientBank->name)) {
                    unset($clientBankModel[$index]);
                    continue;
                }
            }
            if (!empty($clientBankModel)) {
                $ajaxModelArr = ArrayHelper::merge($modelArr, ['ClientBank' => $clientBankModel]);
            }
            return $this->performAjaxMultipleValidation($ajaxModelArr);
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {

            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $file = $filesModel->uploadFile('logo');
                if (!empty($file)) {
                    $filesModel->save();
                }
                $model->uploaded_file_id = $filesModel->id;

                if (!empty($_POST['use_legal_address'])) {
                    $model->office_address = $model->legal_address;
                    $model->office_country_id = $model->legal_country_id;
                }
                
                if(!$model->vat_payer){
                    $model->vat_number = null;
                    $model->tax = 0;
                }

                if ($existClient = Client::findByCondition(['reg_number' => $model->reg_number])->notDeleted()->one()) {
                    $model->id = $existClient->id;
                    $model->setIsNewRecord(false);
                }

                if (!$model->save()) {
                    throw new Exception('Unable to save data! ' . $model->errorMessage);
                } else {
                    if (!empty($abonentModel->parent_id)) {
                        $parent = $abonentModel->parent;
                        $abonentModel->it_is = (!empty($parent->abonents) ? Client::CLIENT_IT_IS_ABONENT : $abonentModel->it_is);
                    }
                    $abonentModel->client_id = $model->id;
                    if (!$abonentModel->save()) {
                        throw new Exception('Unable to save data! ' . $abonentModel->errorMessage);
                    }
                    
                    $model->saveMainContact($mainContactModel, $abonentModel->abonent_id);

                    $clientBankModel = FSMBaseModel::createMultiple(ClientBank::class);
                    FSMBaseModel::loadMultiple($clientBankModel, $requestPost);
                    foreach ($clientBankModel as $index => $clientBank) {
                        if (empty($clientBank->bank_id) && empty($clientBank->account) && empty($clientBank->name)) {
                            unset($clientBankModel[$index]);
                            continue;
                        }
                        $clientBank->client_id = $model->id;
                    }

                    if (!empty($clientBankModel)) {
                        if (FSMBaseModel::validateMultiple($clientBankModel)) {
                            $hasPrimary = 0;
                            foreach ($clientBankModel as $clientBank) {
                                if($hasPrimary){
                                   $clientBank->primary = 0; 
                                }
                                if (!$clientBank->save(false)) {
                                    throw new Exception('Unable to save data! ' . $clientBank->errorMessage);
                                }
                                if($clientBank->primary){
                                   $hasPrimary = 1; 
                                }
                            }
                        } else {
                            Exception('Unable to save data!');
                        }
                    }
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                if ($isPjax) {
                    return $this->actionAjaxModalNameList(['selected_id' => $model->id]);
                } else {
                    return $this->redirect('index');
                }
            }
        } else {
            ClientUIAsset::register(Yii::$app->getView());

            $model->tax = Client::CLIENT_DEFAULT_VAT_TAX;

            $abonentModel->status = Client::CLIENT_STATUS_ACTIVE;
            //$abonentModel->it_is = Client::IT_IS_CLIENT;
            $abonentModel->debit_valuta_id = Valuta::VALUTA_DEFAULT;
            
            $mainContactModel->phone = (!empty(Yii::$app->params['defaultPhoneCode']) ? Yii::$app->params['defaultPhoneCode'] : null);

            $clientBankModel = [new ClientBank()];

            $isAdmin = FSMUser::getIsPortalAdmin();
            $managerList = FSMProfile::getProfileListByRole([
                FSMUser::USER_ROLE_ADMIN,
                FSMUser::USER_ROLE_OPERATOR,
                FSMUser::USER_ROLE_BOOKER,
                FSMUser::USER_ROLE_COORDINATOR,
                FSMUser::USER_ROLE_MANAGER,
                FSMUser::USER_ROLE_TEHNICAL,
                FSMUser::USER_ROLE_LEGAL_TAX,
            ]);
            $profileId = Yii::$app->user->identity->profile->id;
            if (array_key_exists($profileId, $managerList)) {
                $abonentModel->manager_id = $profileId;
            }
            
            $countryList = Country::getNameArr();
            $clientGroupList = ClientGroup::getNameArr(['enabled' => true]);
            $bankList = Bank::getNameArr(['enabled' => true], '', '', function($bank){return $bank['name'].(!empty($bank['swift']) ? ' | '.$bank['swift'] : '');});
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            $languageList = Language::getEnabledLanguageList();
            
            $data = [
                'model' => $model,
                'abonentModel' => $abonentModel,
                'clientBankModel' => $clientBankModel,
                'filesModel' => $filesModel,
                'mainContactModel' => $mainContactModel,
                'countryList' => $countryList,
                'languageList' => $languageList,
                'bankList' => $bankList,
                'valutaList' => $valutaList,
                'managerList' => $managerList,
                'agentList' => [],
                'clientGroupList' => $clientGroupList,
                'isAdmin' => $isAdmin,
                'isModal' => $isAjax,
            ];
            return ($isAjax ? $this->renderAjax('create', $data) : $this->render('create', $data));
        }
    }

    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, true);
        $abonentModel = AbonentClient::findOne(['abonent_id' => $model->userAbonentId, 'client_id' => $model->id]);
        $abonentModel = !empty($abonentModel) ? $abonentModel : new AbonentClient();
        $clientBankModel = $model->clientBanks;
        $clientBankModel = !empty($clientBankModel) ? $clientBankModel : [new ClientBank()];
        $filesModel = $model->logo;
        $filesModel = (!empty($filesModel) ? $filesModel : new Files());
        $mainContactModel = ClientContact::findOne(['client_id' => $id, 'main' => 1]);
        $mainContactModel = !empty($mainContactModel) ? $mainContactModel : new ClientContact();
        $mainContactModel->scenario = 'client';

        $modelArr = [
            'Client' => $model,
            'AbonentClient' => $abonentModel,
            'ClientContact' => $mainContactModel,
        ];
        
        // ajax validation
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && $isAjax) {
            // ajax validation
            $ajaxModelArr = $modelArr;
            $clientBankModel = FSMBaseModel::createMultiple(ClientBank::class);
            FSMBaseModel::loadMultiple($clientBankModel, $requestPost);
            foreach ($clientBankModel as $index => $clientBank) {
                if (empty($clientBank->bank_id) && empty($clientBank->account) && empty($clientBank->name)) {
                    unset($clientBankModel[$index]);
                    continue;
                }
            }
            if (!empty($clientBankModel)) {
                $ajaxModelArr = ArrayHelper::merge($modelArr, ['ClientBank' => $clientBankModel]);
            }
            return $this->performAjaxMultipleValidation($ajaxModelArr);
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $model->saveMainContact($mainContactModel);

                if (!empty($abonentModel->parent_id)) {
                    $parent = $abonentModel->parent;
                    $abonentModel->it_is = (!empty($parent->abonents) ? Client::CLIENT_IT_IS_ABONENT : $abonentModel->it_is);
                }
                $session = Yii::$app->session;
                $deletedIDs = (array)($session->has('file_delete') ? $session['file_delete'] : []);
                if(!empty($deletedIDs) && (in_array($filesModel->id, $deletedIDs))){
                    $filesModel->delete();
                    $filesModel = new Files();
                    $model->uploaded_file_id = null;
                }
                
                $file = $filesModel->uploadFile('logo');
                if (!empty($file)) {
                    $filesModel->save();
                    $model->uploaded_file_id = $filesModel->id;
                }

                if (!empty($_POST['use_legal_address'])) {
                    $model->office_address = $model->legal_address;
                    $model->office_country_id = $model->legal_country_id;
                }

                if(!$model->vat_payer){
                    $model->vat_number = null;
                    $model->tax = 0;
                }
                
                if (!$model->save()) {
                    throw new Exception('Unable to save data! ' . $model->errorMessage);
                } else {
                    if (!$abonentModel->save()) {
                        throw new Exception('Unable to save data! ' . $abonentModel->errorMessage);
                    }
                    
                    $oldBankIDs = isset($clientBankModel[0]) && !empty($clientBankModel[0]->id) ? ArrayHelper::map($clientBankModel, 'id', 'id') : [];

                    $clientBankModel = FSMBaseModel::createMultiple(ClientBank::class, $clientBankModel);
                    FSMBaseModel::loadMultiple($clientBankModel, $requestPost);
                    $deletedBankIDs = array_diff($oldBankIDs, array_filter(ArrayHelper::map($clientBankModel, 'id', 'id')));
                    if (!empty($deletedBankIDs)) {
                        if(!ClientBank::deleteByIDs($deletedBankIDs)){
                            throw new Exception('Unable to delete client bank!');
                        }
                    }

                    foreach ($clientBankModel as $index => $clientBank) {
                        if (empty($clientBank->bank_id) && empty($clientBank->account) && empty($clientBank->name)) {
                            unset($clientBankModel[$index]);
                            continue;
                        }
                        $clientBank->client_id = $model->id;
                    }
                    if (!empty($clientBankModel)) {
                        if (FSMBaseModel::validateMultiple($clientBankModel)) {
                            $hasPrimary = 0;
                            foreach ($clientBankModel as $clientBank) {
                                if($hasPrimary){
                                   $clientBank->primary = 0; 
                                }
                                if (!$clientBank->save(false)) {
                                    throw new Exception('Unable to save data! ' . $clientBank->errorMessage);
                                }
                                if($clientBank->primary){
                                   $hasPrimary = 1; 
                                }
                            }
                        }else{
                            throw new Exception('Unable to save data!');
                        }
                    }
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirectToBackUrl($id);
            }
        } else {
            $this->rememberBackUrl($model->backURL, $id);
            ClientUIAsset::register(Yii::$app->getView());

            $mainContactModel->phone = (!empty($mainContactModel->phone)? 
                $mainContactModel->phone :
                (!empty(Yii::$app->params['defaultPhoneCode']) ? Yii::$app->params['defaultPhoneCode'] : null)
            );
            
            $isAdmin = FSMUser::getIsPortalAdmin();
            $clientGroupList = ClientGroup::getNameArr(['enabled' => true]);
            $countryList = Country::getNameArr();
            $bankList = Bank::getNameArr(['enabled' => true], '', '', function($bank){return $bank['name'].(!empty($bank['swift']) ? ' | '.$bank['swift'] : '');});
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            $languageList = Language::getEnabledLanguageList();
            $managerList = FSMProfile::getProfileListByRole([
                FSMUser::USER_ROLE_ADMIN,
                FSMUser::USER_ROLE_OPERATOR, 
                FSMUser::USER_ROLE_BOOKER,
                FSMUser::USER_ROLE_COORDINATOR,
                FSMUser::USER_ROLE_MANAGER,
                FSMUser::USER_ROLE_TEHNICAL,
                FSMUser::USER_ROLE_LEGAL_TAX,
            ]);

            foreach ($clientBankModel as $clientBank) {
                if(!empty($clientBank->services_period_till)){
                    $clientBank->services_period_till = date('d-m-Y', strtotime($clientBank->services_period_till));
                }
            }

            $agentList = [];
            $profileList = $model->profiles;
            foreach ($profileList as $profile) {
                if(empty($profile->user)){
                    continue;
                }
                if($profile->user->hasRole([FSMUser::USER_ROLE_AGENT])){
                    $agentList[$profile->id] = $profile->name;
                }
            }

            return $this->render('update', [
                'model' => $model,
                'abonentModel' => $abonentModel,
                'clientBankModel' => $clientBankModel,
                'filesModel' => $filesModel,
                'mainContactModel' => $mainContactModel,
                'countryList' => $countryList,
                'languageList' => $languageList,
                'bankList' => $bankList,
                'valutaList' => $valutaList,
                'managerList' => $managerList,
                'agentList' => $agentList,
                'clientGroupList' => $clientGroupList,
                'isAdmin' => $isAdmin,
                'isModal' => false,
            ]);
        }
    }

    /**
     * Displays a single model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        ButtonDeleteAsset::register(Yii::$app->getView());

        $model = $this->findModel($id, true);
        $abonentModel = AbonentClient::findOne(['abonent_id' => $model->userAbonentId, 'client_id' => $model->id]);

        $logoModel = $model->logo;
        $logoPath = !empty($logoModel) ? $logoModel->uploadedFileUrl : null;

        $isAdmin = FSMUser::getIsPortalAdmin();

        $params = Yii::$app->request->getQueryParams();
        $params['client_id'] = $id;
        $params['deleted'] = 0;
        unset($params['id']);
        $clientBankSearchModel = new ClientBankSearch();
        $clientBankdataProvider = $clientBankSearchModel->search($params);

        return $this->render('view', [
            'model' => $model,
            'abonentModel' => $abonentModel,
            'logoPath' => $logoPath,
            'clientBankdataProvider' => $clientBankdataProvider,
            'clientBankSearchModel' => $clientBankSearchModel,
            'isAdmin' => $isAdmin,
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id, true);
        $abonent = $model->mainForAbonent;
        if (!empty($abonent)) {
            Yii::$app->getSession()->setFlash('error', Yii::t('client', 'The client Cannot be removed. This is the main client of the abonent.'));
            $previousUrl = $model->getBackURL()/* Url::previous() */;
            return $this->redirect($previousUrl);
        }
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            if ($model->delete()) {
                $transaction->commit();
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $message = $e->getMessage();
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
        } finally {
            if(!$this->pjaxIndex){
                return $this->redirect(['index']);
            }else{
                return;
            }            
        }
    }

    public function actionStaff($id)
    {
        $model = $this->findModel($id, true);

        $userList = [];
        foreach ($model->profiles as $profile) {
            $user = $profile->user;
            if (!$user) {
                continue;
            }
            $roleList = Yii::$app->authManager->getUserRoleList($user->id);
            $roleArr = [];
            foreach ($roleList as $role) {
                if ($role->name == FSMUser::USER_ROLE_USER) {
                    continue;
                }
                $roleArr[] = $role->description;
            }
            $gravatar_id = $profile->gravatar_id;
            $userList[] = [
                'content' => ($gravatar_id ? "<img src='http://gravatar.com/avatar/{$gravatar_id}>?s=28' alt='{$user->username}' style='margin-right: 5px;'/>" : '') . 
                    (!empty($profile->name) ? Html::encode($profile->name) : $user->username),
                'url' => ['/user/profile/show', 'id' => $profile->user_id],
                'badge' => implode(', ', $roleArr),
                'options' => ['target' => '_blank', 'data-pjax' => 0],
            ];
        }

        $isAdmin = FSMUser::getIsPortalAdmin();
        return $this->render('_staff', [
            'model' => $model,
            'userList' => $userList,
            'isAdmin' => $isAdmin,
        ]);
    }

    public function actionAjaxNameList($q = null)
    {
        $q = trim($q);
        $args = $_GET;
        if (isset($args['q'])) {
            unset($args['q']);
        }
        $args['deleted'] = false;

        $out = [];
        $out['results'][] = ['id' => '', 'text' => ''];

        $data = Client::getNameList($q, $args);
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $out['results'][] = ['id' => $key, 'text' => $value]; // !!! 'text' is needed for Select2 templateResult & templateSelection functions
            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $out;
    }

    public function actionAjaxMainClientNameList($q = null)
    {
        $q = trim($q);
        $args = $_GET;
        if (isset($args['q'])) {
            unset($args['q']);
        }
        $args['deleted'] = false;

        $out = [];
        $out['results'][] = ['id' => '', 'text' => ''];

        $abonentList = Abonent::find()->all();
        $mainClientList = [];
        foreach ($abonentList as $abonent) {
            $mainClientList[] = $abonent->main_client_id;
        }
        $data = Client::getNameList($q, $args);
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                if(!in_array($key, $mainClientList)){
                    continue;
                }
                $out['results'][] = ['id' => $key, 'text' => $value]; // !!! 'text' is needed for Select2 templateResult & templateSelection functions
            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $out;
    }
    
    public function actionAjaxGetTax($id = null)
    {
        if (!$id) {
            $id = !empty($_POST['id']) ? $_POST['id'] : null;
        }
        $client = Client::findOne($id);
        $out[] = [
            'tax' => !empty($client) ? number_format($client->tax, 2) : 0,
            'vat_payer' => !empty($client) ? $client->vat_payer : 0,
        ];
        echo Json::encode($out);
        return false;
    }

    public function actionAjaxGetClientListByAbonent()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;        
        $out = [];
        $selected = null;
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            if (empty($id) || !is_numeric($id)) {
                return ['output' => '', 'selected' => $selected];
            }
            $list = Client::find(true)->notDeleted()
                ->select('сlient.id, сlient.name')
                ->leftJoin(['ac' => 'abonent_сlient'], 'ac.сlient_id = сlient.id')
                ->where(['ac.abonent_id' => $id])
                ->asArray()
                ->all();
            if (count($list) > 0) {
                foreach ($list as $i => $item) {
                    $out[] = ['id' => $item['id'], 'name' => $item['name']];
                }
            }
            $selected = (count($list) == 1 ? strval($list[0]['id']) : '');
        }
        // Shows how you can preselect a value  
        return ['output' => $out, 'selected' => $selected];
    }

    public function actionAjaxGetProjectList()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;        
        $out = [];
        $selected = null;
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            if (empty($id) || !is_numeric($id)) {
                return ['output' => '', 'selected' => $selected];
            }
            $list = $this->findModel($id)->clientProjects;
            if (count($list) > 0) {
                foreach ($list as $i => $item) {
                    $out[] = ['id' => $item['id'], 'name' => $item['name']];
                }
            }
            $selected = (count($list) == 1 ? strval($list[0]['id']) : '');
        }
        // Shows how you can preselect a value  
        return ['output' => $out, 'selected' => $selected];
    }

    public function actionAjaxGetClientBankList()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;        
        $out = [];
        $selected = null;
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            if (empty($id) || !is_numeric($id)) {
                return ['output' => '', 'selected' => $selected];
            }
            $list = $this->findModel($id)->clientActiveBanks;
            if (count($list) > 0) {
                foreach ($list as $i => $item) {
                    $out[] = ['id' => $item->bank->id, 'name' => $item->bank->name.(!empty($item->bank->swift) ? ' | '.$item->bank->swift : '')];
                }
            }
            $selected = (count($out) == 1 ? strval($out[0]['id']) : '');
        }
        // Shows how you can preselect a value  
        return ['output' => $out, 'selected' => $selected];
    }

    public function actionAjaxGetClientAccountList($selected = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;        
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            if (empty($id) || !is_numeric($id)) {
                return ['output' => '', 'selected' => $selected];
            }
            //$list = $this->findModel($id)->clientBanks;
            $list = ClientBank::find(true)
                ->andWhere(['client_id' => $id])
                ->andWhere('services_period_till IS NULL OR (services_period_till >= DATE(NOW()))')
                ->all();            
            if (count($list) > 0) {
                foreach ($list as $i => $item) {
                    $bank = $item->bank;
                    $account = $bank->name.(!empty($bank->swift) ? ' | '.$bank->swift : '').' | '.$item->account.(!empty($item->name) ? ' ( ' . $item->name . ' )' : '');
                    $out[] = ['id' => $item['id'], 'name' => $account];
                    if(!isset($selected) && $item->primary){
                        $selected = $item['id'];
                    }
                }
            }
            $selected = isset($selected) ? $selected : (count($list) == 1 ? strval($list[0]['id']) : '');
        }
        // Shows how you can preselect a value  
        return ['output' => $out, 'selected' => $selected];
    }

    public function actionAjaxGetClientPersonList()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;        
        $out = [];
        $selected = null;
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            if (empty($id) || !is_numeric($id)) {
                return ['output' => '', 'selected' => $selected];
            }
            $args = $_GET;
            if (!empty($args)) {
                if(!empty($args['doc_date'])){
                    $docDate = date('Y-m-d', strtotime($args['doc_date']));
                    unset($args['doc_date']);
                }else{
                    $docDate = date('Y-m-d');
                }
                $args = ArrayHelper::merge(
                    $args, 
                    [
                        'client_id' => $id,
                        'deleted' => 0,
                    ]
                );
                $list = ClientContact::find(true)
                    ->andWhere($args)
                    ->andWhere('((term_from IS NULL) OR (term_from <= "'.$docDate.'"))')
                    ->andWhere('((term_till IS NULL) OR (term_till >= "'.$docDate.'"))')
                    ->all();
            } else {
                $list = $this->findModel($id)->clientContacts;
            }
            if (count($list) > 0) {
                foreach ($list as $i => $item) {
                    $person = $item->first_name . ' ' . $item->last_name . (!empty($item->position_id) ? ' ( ' . $item->position->name . ' )' : '');
                    $out[] = ['id' => $item['id'], 'name' => $person];
                }
                //$selected = (count($list) == 1 ? strval($list[0]['id']) : '');
                $selected = '';
            }
        }
        // Shows how you can preselect a value  
        return ['output' => $out, 'selected' => $selected];
    }

    public function actionLogoDelete($deleted_id)
    {
        $session = Yii::$app->session;
        $session->set('file_delete', $deleted_id);
        echo Json::encode(['output' => true]);
        return false;
    }    
}
