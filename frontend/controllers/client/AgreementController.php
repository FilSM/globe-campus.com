<?php

namespace frontend\controllers\client;

use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\web\BadRequestHttpException;

use common\controllers\FilSMController;
use common\components\FSMAccessHelper;
use common\assets\ButtonDeleteAsset;
use common\models\mainclass\FSMBaseModel;
use common\models\user\FSMUser;
use common\models\Valuta;
use common\models\Files;
use common\models\abonent\Abonent;
use common\models\client\Client;
use common\models\client\ClientRole;
use common\models\client\Project;
use common\models\client\Agreement;
use common\models\client\AgreementAttachment;
use common\models\client\AgreementPayment;
use common\models\client\FirstAgreementPerson;
use common\models\client\SecondAgreementPerson;
use common\models\abonent\AbonentAgreement;
use common\models\abonent\AbonentAgreementProject;
use common\models\bill\Bill;
use common\models\abonent\AbonentBill;
use frontend\assets\AgreementUIAsset;

/**
 * AgreementController implements the CRUD actions for Agreement model.
 */
class AgreementController extends FilSMController
{

    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\client\Agreement';
        $this->defaultSearchModel = 'common\models\client\search\AgreementSearch';
        $this->pjaxIndex = true;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [];
        $_get = Yii::$app->request->get();
        $id = isset($_get['id']) ? $_get['id'] : null;
        $model = !empty($id) ? $this->findModel($id) : null;
        $behaviors = ArrayHelper::merge(
            $behaviors, [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'actions' => [
                                'ajax-get-full-agreement-list',
                                'ajax-get-model',
                                'ajax-modal-name-list',
                                'ajax-get-active-bill-list',
                                'ajax-get-doc-confirm',
                                'ajax-validate-unique-number',
                            ],
                            'allow' => true,
                        ],
                        [
                            'actions' => ['index', 'project-detail'],
                            'allow' => FSMAccessHelper::checkRoute('/agreement/*'),
                        ],
                        [
                            'actions' => ['view', 'print-pdf'],
                            'allow' => FSMAccessHelper::can('viewAgreement', $model),
                        ],
                        [
                            'actions' => ['create'],
                            'allow' => FSMAccessHelper::can('createAgreement'),
                        ],
                        [
                            'actions' => ['update', 'attachment-delete'],
                            'allow' => FSMAccessHelper::can('updateAgreement', $model),
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => FSMAccessHelper::can('deleteAgreement', $model),
                        ],
                    ],
                ],
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'ajax-get-model' => ['get'],
                    ],
                ],
            ]
        );
        return $behaviors;
    }

    public function actionAjaxGetModel($id)
    {
        if (empty($id)) {
            return [];
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out['agreement'] = Agreement::findOne($id);
        $out['first_client'] = $out['agreement']->firstClient;
        $out['first_client_role'] = !empty($out['agreement']->first_client_role_id) ? $out['agreement']->firstClientRole->name : '';
        $out['first_client_address'] = $out['first_client']->legal_address;
        $out['second_client'] = $out['agreement']->secondClient;
        $out['second_client_role'] = !empty($out['agreement']->second_client_role_id) ? $out['agreement']->secondClientRole->name : '';
        $out['second_client_address'] = $out['second_client']->legal_address;

        $get = Yii::$app->request->get();
        if (!empty($get['reservedId']) && !empty($out['first_client']->gen_number)) {
            $lastNumber = $out['first_client']->getLastNumber($get['reservedId']);
            $out['first_client']['lastNumber'] = $out['first_client']->gen_number . $lastNumber;
        }
        return $out;
    }

    public function actionAjaxGetFullAgreementList()
    {
        $out = [];
        $list = Agreement::getNameArr(['agreement.deleted' => false]);
        if (count($list) > 0) {
            foreach ($list as $id => $name) {
                $out[] = [
                    'id' => $id,
                    'name' => $name,
                ];
            }
            $selected = (count($list) == 1 ? strval($list[0]['id']) : '');
        } else {
            $selected = '';
        }
        // Shows how you can preselect a value  
        echo Json::encode(['output' => $out, 'selected' => $selected]);
        return false;
    }

    public function actionAjaxGetActiveBillList($id)
    {
        //Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        if (isset($id)) {
            if (!is_numeric($id) && !is_array($id)) {
                return [];
            }
            if (!is_array($id)) {
                $model = $this->findModel($id);
                $list = $model->activeBillList;
            } else {
                $resultList = [];
                foreach ($id as $value) {
                    $model = $this->findModel($value);
                    $billList = $model->activeBillList;
                    foreach ($billList as $bill) {
                        $resultList[] = $bill;
                    }
                }
                $list = $resultList;
            }

            if (count($list) > 0) {
                foreach ($list as $i => $item) {
                    if (!empty($_GET['without-id']) && ($_GET['without-id'] == $item[$i]->id)) {
                        unset($list[$i]);
                    }
                }
                $out = $list;
            }
        }
        echo Json::encode(['output' => $out]);
        return false;
    }
    
    public function actionAjaxGetDocConfirm($id, $for_client_id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (empty($id)) {
            return [];
        }
        $model = $this->findModel($id);
        
        $agreementSumma = $model->summa;
        $paymentsSummaCredit = $model->paymentsSummaCredit;
        $paymentsSummaDebet = $model->paymentsSummaDebet;
        if($agreementSumma > $paymentsSummaCredit){
            $summa = number_format($agreementSumma - $paymentsSummaCredit, 2, '.', '');
            $direction = AgreementPayment::DIRECTION_CREDIT;
        }elseif($agreementSumma > $paymentsSummaDebet){
            $summa = number_format($agreementSumma - $paymentsSummaDebet, 2, '.', '');
            $direction = AgreementPayment::DIRECTION_DEBET;
        }elseif(($agreementSumma <= $paymentsSummaDebet) && ($agreementSumma <= $paymentsSummaDebet)){
            Yii::$app->getSession()->setFlash('error', Yii::t('agreement', 'No payment can be made for this agreement.'));
            Yii::$app->response->format = Response::FORMAT_JSON;
            return 'reload';
        }
        $canCredit = ($agreementSumma > $paymentsSummaCredit);
        $canDebet = ($agreementSumma > $paymentsSummaDebet);
        $showDirection = $canCredit && $canDebet;
            
        $secondClientModel = $model->secondClient;
        
        $result['notPaidSumma'] = $summa;
        $result['rate'] = $model->rate;
        $result['currency'] = $model->valuta_id;
        $result['firstClientBankAccount'] = null;
        $result['secondClientId'] = $secondClientModel->id;
        $result['secondClientName'] = $secondClientModel->name;
        $result['secondClientReg'] = $secondClientModel->reg_number;
        $result['secondClientBankAccount'] = null;
        
        return $result;
    }
    
    public function actionAjaxValidateUniqueNumber($doc_number, $id = null) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (empty($doc_number)) {
            return [];
        }
        
        $baseTableName = Agreement::tableName();
        $query = Agreement::find(true);
        $query->andWhere([$baseTableName.'.number' => $doc_number]);
        if(!empty($id)){
            $query->andWhere(['not', [$baseTableName.'.id' => $id]]);
        }
        $exists = $query->exists();
        if ($exists) {
            $message = Yii::t('yii', '{attribute} "{value}" has already been taken.', ['attribute' => Yii::t('bill', 'Doc.number'), 'value' => $doc_number]);
        }
        
        return [
            'exists' => $exists,
            'message' => $message ?? null,
        ];
    } 
    
    /**
     * Url action - /project/project-detail
     */
    public function actionProjectDetail()
    {
        if (isset($_POST['expandRowKey'])) {
            $searchModel = new $this->defaultSearchModel;
            $params = Yii::$app->request->getQueryParams();
            $params['deleted'] = 0;
            $params['project_id'] = $_POST['expandRowKey'];

            $dataProvider = $searchModel->search($params);

            $isAdmin = FSMUser::getIsPortalAdmin();
            return $this->renderPartial('prj-detail-index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'isAdmin' => $isAdmin,
                'forDetail' => true,
            ]);
        } else {
            return '<div class="alert alert-danger">No data found</div>';
        }
    }

    /**
     * Lists all models.
     * @return mixed
     */
    public function actionIndex($client_id = null)
    {
        if (isset($client_id) && (($fromClientModel = Client::findOne($client_id)) === null)) {
            throw new Exception('The requested page does not exist.');
        }
        ButtonDeleteAsset::register(Yii::$app->getView());

        $searchModel = new $this->defaultSearchModel;
        $params = Yii::$app->request->getQueryParams();
        $params['deleted'] = (empty($params) || empty($params['AgreementSearch'])) ? 0 : (int) !empty($params['AgreementSearch']['deleted']);

        $dataProvider = $searchModel->search($params);

        $isAdmin = FSMUser::getIsPortalAdmin();
        $clientList = Client::getNameArr(['client.deleted' => 0]);
        return $this->render((empty($client_id) ? 'index' : 'client/index'), [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'fromClientModel' => (!empty($client_id) ? $fromClientModel : null),
                'clientList' => $clientList,
                'isAdmin' => $isAdmin,
            ]
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($project_id = null, $client_id = null)
    {
        if (isset($client_id) && (($fromClientModel = Client::findOne($client_id)) === null)) {
            throw new Exception('The requested page does not exist.');
        }        
        $model = new $this->defaultModel;
        $filesModel = [new Files()];

        $modelArr = [
            'Agreement' => $model,
        ];

        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            $ajaxModelArr = $modelArr;

            $personModel = FSMBaseModel::createMultiple(FirstAgreementPerson::class);
            FSMBaseModel::loadMultiple($personModel, $requestPost);
            foreach ($personModel as $index => $agreementlPerson) {
                if(empty($agreementlPerson->client_contact_id)){
                    unset($personModel[$index]);
                    continue;
                }
            }
            if (!empty($personModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['FirstAgreementPerson' => $personModel]);
            }
            
            $personModel = FSMBaseModel::createMultiple(SecondAgreementPerson::class);
            FSMBaseModel::loadMultiple($personModel, $requestPost);
            foreach ($personModel as $index => $agreementlPerson) {
                if(empty($agreementlPerson->client_contact_id)){
                    unset($personModel[$index]);
                    continue;
                }
            }
            if (!empty($personModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['SecondAgreementPerson' => $personModel]);
            }
            
            return $this->performAjaxMultipleValidation($ajaxModelArr);
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            if (($model->first_client_id == $model->second_client_id) ||
                ($model->first_client_id == $model->third_client_id) ||
                ($model->second_client_id == $model->third_client_id)) {
                throw new Exception('The same clients are selected!');
            }
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $fieldArr = $requestPost['Agreement']['consideration'];
                $data = [];
                foreach ($fieldArr as $value) {
                    $data[] = $value;
                }
                $model->consideration = json_encode($data);
                if (!$model->save()) {
                    throw new Exception('Unable to save data! ' . $model->errorMessage);
                }
                
                $abonentAgreementModel = new AbonentAgreement();
                $abonentAgreementModel->abonent_id = $model->userAbonentId;
                $abonentAgreementModel->agreement_id = $model->id;
                $abonentAgreementModel->save();
                
                if(!empty($requestPost['Agreement']['project_id'])){
                    foreach ($requestPost['Agreement']['project_id'] as $projectId) {
                        $abonentAgreementProjectModel = new AbonentAgreementProject();
                        $abonentAgreementProjectModel->abonent_agreement_id = $abonentAgreementModel->id;
                        $abonentAgreementProjectModel->project_id = $projectId;
                        $abonentAgreementProjectModel->save();
                    }
                }

                $files = UploadedFile::getInstances($filesModel[0], 'file');
                foreach ($files as $uploadedFile) {
                    $filesModel = new Files();
                    $file = $filesModel->uploadFileMultiply($uploadedFile, 'pdf/agreements/' . $model->id);
                    if (!empty($file)) {
                        if (!$filesModel->save()) {
                            throw new Exception('Can not save the PDF file! ' . $filesModel->errorMessage);
                        }
                        $attachment = new AgreementAttachment();
                        $attachment->agreement_id = $model->id;
                        $attachment->uploaded_file_id = $filesModel->id;
                        if (!$attachment->save()) {
                            throw new Exception('Can not save the Attachment! ' . $attachment->errorMessage);
                        }
                    }
                }

                $personModel = FSMBaseModel::createMultiple(FirstAgreementPerson::class);
                FSMBaseModel::loadMultiple($personModel, $requestPost);
                foreach ($personModel as $index => $agreementlPerson) {
                    if(empty($agreementlPerson->client_contact_id)){
                        unset($personModel[$index]);
                        continue;
                    }
                    $agreementlPerson->agreement_id = $model->id;                      
                    $agreementlPerson->person_order = 1;                      
                }
                if(!empty($personModel)){
                    if ($flag = FSMBaseModel::validateMultiple($personModel)) {
                        foreach ($personModel as $agreementlPerson) {
                            if (!$agreementlPerson->save(false)) {
                                throw new Exception('Unable to save data! '.$agreementlPerson->errorMessage);
                            }
                        }
                    }
                }

                $personModel = FSMBaseModel::createMultiple(SecondAgreementPerson::class);
                FSMBaseModel::loadMultiple($personModel, $requestPost);
                foreach ($personModel as $index => $agreementlPerson) {
                    if(empty($agreementlPerson->client_contact_id)){
                        unset($personModel[$index]);
                        continue;
                    }
                    $agreementlPerson->agreement_id = $model->id;                      
                    $agreementlPerson->person_order = 2;                      
                }
                if(!empty($personModel)){
                    if ($flag = FSMBaseModel::validateMultiple($personModel)) {
                        foreach ($personModel as $agreementlPerson) {
                            if (!$agreementlPerson->save(false)) {
                                throw new Exception('Unable to save data! '.$agreementlPerson->errorMessage);
                            }
                        }
                    }
                }

                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                if ($isPjax) {
                    return $this->actionAjaxModalNameList(['selected_id' => $model->id]);
                } else {
                    return $this->redirect((empty($client_id) ? 'index' : ['index', 'client_id' => $client_id]));
                }
            }
        } else {
            Yii::$app->session->remove('file_delete');

            AgreementUIAsset::register(Yii::$app->getView());

            $model->valuta_id = Valuta::VALUTA_DEFAULT;
            $model->prepayment_valuta_id = Valuta::VALUTA_DEFAULT;
            $model->project_id = (!empty($project_id) ? $project_id : null);

            $firstPersonModel = [new FirstAgreementPerson(), new FirstAgreementPerson()];
            $secondPersonModel = [new SecondAgreementPerson(), new SecondAgreementPerson()];
            
            $isAdmin = FSMUser::getIsPortalAdmin();
            $projectList = Project::getNameArr(['project.deleted' => 0]);
            $clientList = Client::getNameArr(['client.deleted' => 0]);
            $clientRoleList = ClientRole::getNameArr();
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            
            $data = [
                'model' => $model,
                'filesModel' => $filesModel,
                'fromClientModel' => (!empty($client_id) ? $fromClientModel : null),
                'firstPersonModel' => $firstPersonModel,
                'secondPersonModel' => $secondPersonModel,
                'projectList' => $projectList,
                'valutaList' => $valutaList,
                'clientList' => $clientList,
                'clientRoleList' => $clientRoleList,
                'isAdmin' => $isAdmin,
                'isModal' => $isAjax,
            ];
            if ($isAjax) {
                return $this->renderAjax((empty($client_id) ? 'create' : 'client/create'), $data);
            } else {
                return $this->render((empty($client_id) ? 'create' : 'client/create'), $data);
            }
        }
    }

    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $client_id = null)
    {
        if (isset($client_id) && (($fromClientModel = Client::findOne($client_id)) === null)) {
            throw new Exception('The requested page does not exist.');
        }           
        $model = $this->findModel($id, true);
        
        $filesModel = $model->attachmentFiles;
        $filesModel = (!empty($filesModel) ? $filesModel : [new Files()]);
        $firstPersonModel = $model->firstClientPerson;        
        $firstPersonModel = !empty($firstPersonModel) ? $firstPersonModel : [new FirstAgreementPerson(), new FirstAgreementPerson()];
        $secondPersonModel = $model->secondClientPerson;        
        $secondPersonModel = !empty($secondPersonModel) ? $secondPersonModel : [new SecondAgreementPerson(),new SecondAgreementPerson()];

        $modelArr = [
            'Agreement' => $model,
        ];

        // ajax validation
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && $isAjax) {
            $ajaxModelArr = $modelArr;

            $personModel = FSMBaseModel::createMultiple(FirstAgreementPerson::class);
            FSMBaseModel::loadMultiple($personModel, $requestPost);
            foreach ($personModel as $index => $agreementlPerson) {
                if(empty($agreementlPerson->client_contact_id)){
                    unset($personModel[$index]);
                    continue;
                }
            }
            if (!empty($personModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['FirstAgreementPerson' => $personModel]);
            }
            
            $personModel = FSMBaseModel::createMultiple(SecondAgreementPerson::class);
            FSMBaseModel::loadMultiple($personModel, $requestPost);
            foreach ($personModel as $index => $agreementlPerson) {
                if(empty($agreementlPerson->client_contact_id)){
                    unset($personModel[$index]);
                    continue;
                }
            }
            if (!empty($personModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['SecondAgreementPerson' => $personModel]);
            }
            
            return $this->performAjaxMultipleValidation($ajaxModelArr);
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            if (($model->first_client_id == $model->second_client_id) ||
                ($model->first_client_id == $model->third_client_id) ||
                ($model->second_client_id == $model->third_client_id)) {
                throw new Exception('The same clients are selected!');
            }
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $fieldArr = $requestPost['Agreement']['consideration'];
                $data = [];
                foreach ($fieldArr as $value) {
                    $data[] = $value;
                }
                $model->consideration = json_encode($data);
                
                if (!$model->save()) {
                    throw new Exception('Unable to save data! ' . $model->errorMessage);
                }
                
                if(!$abonentAgreementModel = AbonentAgreement::findOne([
                    'abonent_id' => $model->userAbonentId,
                    'agreement_id' => $model->id,
                ])){
                    $abonentAgreementModel = new AbonentAgreement();
                    $abonentAgreementModel->abonent_id = $model->userAbonentId;
                    $abonentAgreementModel->agreement_id = $model->id;
                    $abonentAgreementModel->save();
                }
                
                $abonentAgreementProjectModel = AbonentAgreementProject::findByCondition([
                    'abonent_agreement_id' => $abonentAgreementModel->id,
                ])->all();
                
                $oldProjectIds = [];
                foreach ($abonentAgreementProjectModel as $abonent) {
                    $oldProjectIds[] = $abonent->project_id;
                }
                
                $deletedIDs = array_diff($oldProjectIds, $requestPost['Agreement']['project_id']);
                foreach ($abonentAgreementProjectModel as $abonent) {
                    if(in_array($abonent->project_id, $deletedIDs)){
                        $abonent->delete();
                    }
                }
                
                $newIDs = array_diff($requestPost['Agreement']['project_id'], $oldProjectIds);
                foreach ($newIDs as $projectId) {
                    $abonentAgreementProjectModel = new AbonentAgreementProject();
                    $abonentAgreementProjectModel->abonent_agreement_id = $abonentAgreementModel->id;
                    $abonentAgreementProjectModel->project_id = $projectId;
                    $abonentAgreementProjectModel->save();
                }

                $session = Yii::$app->session;
                $deletedIDs = (array) ($session->has('file_delete') ? $session['file_delete'] : []);
                if (!empty($deletedIDs)) {
                    $attachmentList = $model->attachments;
                    foreach ($attachmentList as $attachment) {
                        if (in_array($attachment->uploaded_file_id, $deletedIDs)) {
                            $attachment->delete();
                        }
                    }
                }

                $files = UploadedFile::getInstances($filesModel[0], 'file');
                foreach ($files as $uploadedFile) {
                    $filesModel = new Files();
                    $file = $filesModel->uploadFileMultiply($uploadedFile, 'pdf/agreements/' . $model->id);
                    if (!empty($file)) {
                        if (!$filesModel->save()) {
                            throw new Exception('Can not save the PDF file! ' . $filesModel->errorMessage);
                        }
                        $attachment = new AgreementAttachment();
                        $attachment->agreement_id = $model->id;
                        $attachment->uploaded_file_id = $filesModel->id;
                        if (!$attachment->save()) {
                            throw new Exception('Can not save the Attachment! ' . $attachment->errorMessage);
                        }
                    }
                }                    
                
                $oldPersonIDs = isset($firstPersonModel[0]) && !empty($firstPersonModel[0]->id) ? ArrayHelper::map($firstPersonModel, 'id', 'id') : [];
                $personModel = FSMBaseModel::createMultiple(FirstAgreementPerson::class, (!empty($oldPersonIDs) ? $firstPersonModel : []));
                FSMBaseModel::loadMultiple($personModel, $requestPost);
                $deletedIDs = array_diff($oldPersonIDs, array_filter(ArrayHelper::map($personModel, 'id', 'id')));

                foreach ($personModel as $index => $agreementlPerson) {
                    if(empty($agreementlPerson->client_contact_id)){
                        if(!empty($agreementlPerson->id)){
                            $agreementlPerson->delete();
                        }
                        unset($personModel[$index]);
                        continue;
                    }
                    $agreementlPerson->agreement_id = $model->id;                      
                    $agreementlPerson->person_order = 1;                      
                }

                if(!empty($personModel)){
                    if ($flag = FSMBaseModel::validateMultiple($personModel)) {
                        if (!empty($deletedIDs)) {
                            $flag = FirstAgreementPerson::deleteByIDs($deletedIDs);
                        }     
                        if ($flag) {
                            foreach ($personModel as $agreementlPerson) {
                                if (!$agreementlPerson->save(false)) {
                                    throw new Exception('Unable to save data! '.$agreementlPerson->errorMessage);
                                    break;
                                }
                            }
                        }
                    }
                }

                $oldPersonIDs = isset($secondPersonModel[0]) && !empty($secondPersonModel[0]->id) ? ArrayHelper::map($secondPersonModel, 'id', 'id') : [];
                $personModel = FSMBaseModel::createMultiple(SecondAgreementPerson::class, (!empty($oldPersonIDs) ? $secondPersonModel : []));
                FSMBaseModel::loadMultiple($personModel, $requestPost);
                $deletedIDs = array_diff($oldPersonIDs, array_filter(ArrayHelper::map($personModel, 'id', 'id')));

                foreach ($personModel as $index => $agreementlPerson) {
                    if(empty($agreementlPerson->client_contact_id)){
                        if(!empty($agreementlPerson->id)){
                            $agreementlPerson->delete();
                        }
                        unset($personModel[$index]);
                        continue;
                    }
                    $agreementlPerson->agreement_id = $model->id;                      
                    $agreementlPerson->person_order = 2;                      
                }

                if(!empty($personModel)){
                    if ($flag = FSMBaseModel::validateMultiple($personModel)) {
                        if (!empty($deletedIDs)) {
                            $flag = SecondAgreementPerson::deleteByIDs($deletedIDs);
                        }     
                        if ($flag) {
                            foreach ($personModel as $agreementlPerson) {
                                if (!$agreementlPerson->save(false)) {
                                    throw new Exception('Unable to save data! '.$agreementlPerson->errorMessage);
                                    break;
                                }
                            }
                        }
                    }
                }                

                $transaction->commit();
            } catch (Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirectToBackUrl($id);
            }
        } else {
            $this->rememberBackUrl($model->backURL, $id);
            Yii::$app->session->remove('file_delete');

            AgreementUIAsset::register(Yii::$app->getView());
            
            $abonentModel = AbonentAgreement::findOne([
                'abonent_id' => $model->userAbonentId,
                'agreement_id' => $id,
            ]);
            foreach ($abonentModel->abonentAgreementProjects as $abonentModel) {
                $model->project_id[] = $abonentModel->project_id;
            };
        
            $model->signing_date = !empty($model->signing_date) ? date('d-m-Y', strtotime($model->signing_date)) : null;
            $model->due_date = !empty($model->due_date) ? date('d-m-Y', strtotime($model->due_date)) : null;
            $model->rate_from_date = !empty($model->rate_from_date) ? date('d-m-Y', strtotime($model->rate_from_date)) : null;
            $model->rate_till_date = !empty($model->rate_till_date) ? date('d-m-Y', strtotime($model->rate_till_date)) : null;

            $isAdmin = FSMUser::getIsPortalAdmin();
            $projectList = Project::getNameArr(['project.deleted' => 0]);
            $clientList = Client::getNameArr(['client.deleted' => 0]);
            $clientRoleList = ClientRole::getNameArr();
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            return $this->render((empty($client_id) ? 'update' : 'client/update'), [
                'model' => $model,
                'filesModel' => $filesModel,
                'fromClientModel' => (!empty($client_id) ? $fromClientModel : null),
                'firstPersonModel' => $firstPersonModel,
                'secondPersonModel' => $secondPersonModel,
                'projectList' => $projectList,
                'valutaList' => $valutaList,
                'clientList' => $clientList,
                'clientRoleList' => $clientRoleList,
                'isAdmin' => $isAdmin,
            ]);
        }
    }

    /**
     * Displays a single model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $client_id = null)
    {
        if (isset($client_id) && (($fromClientModel = Client::findOne($client_id)) === null)) {
            throw new Exception('The requested page does not exist.');
        }        
        ButtonDeleteAsset::register(Yii::$app->getView());
        $model = $this->findModel($id, true);

        $filesModel = $model->attachmentFiles;
        $filesModel = (!empty($filesModel) ? $filesModel : [new Files()]);

        $paymentModel = new \common\models\client\search\AgreementPaymentSearch();
        $params = Yii::$app->request->getQueryParams();
        unset($params['id']);
        $params['agreement_id'] = $id;
        $paymentDataProvider = $paymentModel->search($params);

        $historyModel = new \common\models\client\search\AgreementHistorySearch();
        $historyDataProvider = $historyModel->search($params);

        $isAdmin = FSMUser::getIsPortalAdmin();
        return $this->render((empty($client_id) ? 'view' : 'client/view'), [
            'model' => $model,
            'paymentModel' => $paymentModel,
            'historyModel' => $historyModel,
            'paymentDataProvider' => $paymentDataProvider,
            'historyDataProvider' => $historyDataProvider,
            'filesModel' => $filesModel,
            'fromClientModel' => (!empty($client_id) ? $fromClientModel : null),
            'isAdmin' => $isAdmin,
        ]);
    }
    
    public function actionAjaxModalNameList($param = [])
    {
        if(!isset($this->defaultModel)){
            return [];
        }
        
        $model = new $this->defaultModel;
        
        $selectedId = null;
        if(isset($param['selected_id'])){
            $selectedId = $param['selected_id'];
            unset($param['selected_id']);
        }
        $param = !empty($param) ? $param : null;
        
        if($model->hasAttribute('deleted') && !isset($param['deleted'])){
            $param['deleted'] = false;
        }
        
        // JSON response is expected in case of successful save
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $result = [];
        $source = !empty($_GET['source']) ? $_GET['source'] : null;
        switch ($source) {
            case 'bill-confirm':
                $billConfirmModel = 
                    !empty($_GET['bill_confirm_id']) ? 
                        \common\models\bill\BillConfirm::findOne($_GET['bill_confirm_id']) : 
                        new \common\models\bill\BillConfirm();
                $data = $billConfirmModel->getAvailableAgreementList();
                unset($billConfirmModel);
                break;

            default:
                $data = $model::getNameArr($param, $model::$nameField, $model::$keyField, $model::$nameField);
                break;
        }
        unset($model);
        
        foreach ($data as $key => $item) {
            $result[] = [
                'id' => $key,
                'text' => $item,
            ];
        }
        
        return [
            'success' => true,
            'selected' => $selectedId,
            'data' => $result,
        ];        
    } 

    public function actionPrintPdf($id)
    {
        try {
            $model = $this->findModel($id);
            if(!$firstClientModel = $model->firstClient){
                Yii::error('$firstClientModel empty', __METHOD__);
            }
            if(!$secondClientModel = $model->secondClient){
                Yii::error('$secondClientModel empty', __METHOD__);
            }
            if(!$firstClientRoleModel = $model->firstClientRole){
                Yii::error('$firstClientRoleModel empty', __METHOD__);
            }
            if(!$secondClientRoleModel = $model->secondClientRole){
                Yii::error('$secondClientRoleModel empty', __METHOD__);
            }
            $firstClientPersonModel = $model->firstClientPerson;
            if(count($firstClientPersonModel) < 1){
                Yii::error('$firstClientPersonModel empty', __METHOD__);
            }
            $secondClientPersonModel = $model->secondClientPerson;
            if(count($secondClientPersonModel) < 1){
                Yii::error('$secondClientPersonModel empty', __METHOD__);
            }
            
            $this->rememberBackUrl($model->backURL, $id);
            
            if (!$firstClientModel || 
                !$secondClientModel || 
                !$firstClientRoleModel || 
                !$secondClientRoleModel || 
                (count($firstClientPersonModel) < 1) || 
                (count($secondClientPersonModel) < 1)
            ) {
                throw new BadRequestHttpException('Not enough data to complete the request.');
            }
            
            $logoModel = $firstClientModel->logo;
            $logoPath = !empty($logoModel) ? $logoModel->uploadedFilePath : null;

            $data = [
                'model' => $model,
                'firstClientModel' => $firstClientModel,
                'secondClientModel' => $secondClientModel,
                'firstClientRoleModel' => $firstClientRoleModel,
                'secondClientRoleModel' => $secondClientRoleModel,
                'firstClientPersonModel' => $firstClientPersonModel,
                'secondClientPersonModel' => $secondClientPersonModel,
                'logoPath' => $logoPath,
            ];

            $content = $this->renderPartial('cooperation_contract_template_eng', $data);

            $pdf = Yii::$app->pdf;
            $css = $pdf->getCss();
            $api = $pdf->api;
            $options = $pdf->options;
            
            $mpdf = new \Mpdf\Mpdf($options);
            $mpdf->SetFooter('Page {PAGENO}');
            $mpdf->WriteHTML($css, 1);
            $mpdf->WriteHTML($content, 2);
            $pageTotal = $mpdf->page; //$mpdf->docPageNumTotal();
            unset($mpdf);

            $f = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);
            $pageTotal = $f->format($pageTotal);
            $data['pageTotal'] = $pageTotal;

            $content = $this->renderPartial('cooperation_contract_template_eng', $data);
            
            $pdf->methods = [
                'SetFooter' => ['Page {PAGENO}'],
            ];
            $pdf->filename = 'AGREEMENT-'.$model->create_time.'.pdf';
            $pdf->content = $content;
            return $pdf->render();
            
        } catch (Exception $e) {
            $message = $e->getMessage();
            Yii::error($message, __METHOD__);
            Yii::$app->session->removeAllFlashes();
            Yii::$app->session->setFlash('error', $message);
            return $this->redirectToBackUrl($id);
        }        
    }
}
