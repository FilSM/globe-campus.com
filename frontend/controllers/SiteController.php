<?php

namespace frontend\controllers;

use Yii;
use yii\base\Exception;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

use common\models\LoginForm;
use common\controllers\FilSMController;

use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

use common\models\user\FSMUser;
/**
 * Site controller
 */
class SiteController extends FilSMController
{
    public function init()
    {
        parent::init();
        #add your logic: read the cookie and then set the language
	/*
        if (!empty(Yii::$app->session->get('user-after-login'))) {
            Yii::$app->getSession()->remove('user-after-login');
            $user = Yii::$app->user->identity;
            if (!isset($user)) {
                return;
            }
            $languageList = Yii::$app->urlManager->languages;
            $profile = $user->profile;
            if(!empty($profile->language_id)){
                $userLanguage = $profile->language->language;
                Yii::$app->language = !empty($languageList[$userLanguage]) ? $languageList[$userLanguage] : Yii::$app->language;
            }
        }
	*/
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                       'actions' => ['captcha'],
                       'allow' => true,
                    ],                    
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::behaviors();
        $actions = ArrayHelper::merge(
            $actions, [
                'error' => [
                    'class' => 'common\components\actions\ErrorAction',
                ],
                'captcha' => [
                    'class' => 'yii\captcha\CaptchaAction',
                    'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                ],
            ]
        );
        return $actions;
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $user = Yii::$app->user->identity;
        /*
        if(isset($user) && $user->hasRole([
                FSMUser::USER_ROLE_AGENT,
            ]
        )){
            return $this->redirect('dashboard');
        } 
         * 
         */       
        if(isset($user)){
            return $this->redirect('dashboard');
        }        
        return parent::actionIndex();
        //return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->user->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new Exception($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Displays error page.
     *
     * @return mixed
     */
    public function actionError()
    {
        $fileName = 'error';
        
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {

            if ($exception instanceof \yii\web\HttpException) {
                $code = $exception->statusCode;
            } else {
                $code = $exception->getCode();
            }
            $name = $exception->getName();
            if ($name === null) {
                $name = 'Error';
            }
            if ($code) {
                $name .= " (#$code)";
            }

            if ($exception instanceof \yii\base\UserException) {
                $message = $exception->getMessage();
            } else {
                $message = 'An internal server error occurred.';
            }

            switch ($exception->statusCode) {
                case 400:
                case 403:
                case 404:
                case 503:
                    $fileName .= $exception->statusCode;
                    break;

                default:
                    break;
            }
            
            $user = Yii::$app->user;
            return $this->render($fileName, [
                'exception' => $exception,
                'code' => $exception->statusCode,
                'name' => $name,
                'message' => $message,
                'user' => $user,
            ]);
        }        
            
        return $this->render($fileName);
    }
}
