<?php

namespace frontend\controllers\dashboard;

use Yii;

use common\controllers\FilSMController;
use common\models\user\FSMUser;
use common\models\bill\search\BillSearch;
use common\models\client\search\AgreementSearch;
use common\models\bill\search\ExpenseSearch;
use common\models\bill\search\ConventionSearch;

use common\assets\ButtonDeleteAsset;

class DashboardController extends FilSMController
{
    public function actionIndex()
    {
        ButtonDeleteAsset::register(Yii::$app->getView());
        
        $user = Yii::$app->user->identity;
        $lastLoginDate = !empty($user->last_login_at) ? date('Y-m-d', $user->last_login_at) : date('Y-m-d');
        
        //$params = Yii::$app->request->getQueryParams();
        $billSearchModel = new BillSearch();
        $params['deleted'] = 0;
        $params['start_date'] = $lastLoginDate;
        $billDataProvider = $billSearchModel->searchLast($params);
        
        $agrSearchModel = new AgreementSearch();
        $agrDataProvider = $agrSearchModel->searchLast($params);
        
        $expSearchModel = new ExpenseSearch();
        $expDataProvider = $expSearchModel->searchLast($params);
        
        $conSearchModel = new ConventionSearch();
        $conDataProvider = $conSearchModel->searchLast($params);
        
        return $this->render('index', [
            'billDataProvider' => $billDataProvider,
            'billSearchModel' => $billSearchModel,
            'agrDataProvider' => $agrDataProvider,
            'agrSearchModel' => $agrSearchModel,
            'expDataProvider' => $expDataProvider,
            'expSearchModel' => $expSearchModel,
            'conDataProvider' => $conDataProvider,
            'conSearchModel' => $conSearchModel,
            'isAdmin' => FSMUser::getIsPortalAdmin(),
        ]);
    }

}
