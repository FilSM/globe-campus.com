<?php

namespace frontend\controllers\bill;

use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\filters\AccessControl;

use common\controllers\FilSMAttachmentController;
use common\components\FSMToken;
use common\components\FSMAccessHelper;
use common\models\mainclass\FSMBaseModel;
use common\models\user\FSMUser;
use common\models\user\FSMProfile;
use common\models\Valuta;
use common\models\abonent\AbonentBill;
use common\models\client\Project;
use common\models\client\Agreement;
use common\models\client\Client;
use common\models\client\ClientRole;
use common\models\bill\Bill;
use common\models\bill\search\BillSearch;
use common\models\bill\BillHistory;
use common\models\bill\BillProduct;
use common\models\bill\BillProductProject;
use common\models\bill\FirstBillPerson;
use common\models\bill\SecondBillPerson;
use common\models\bill\ReservedId;
use common\models\Bank;
use common\models\Product;
use common\models\Measure;
use common\models\Language;
use common\models\Files;
use common\assets\ButtonDeleteAsset;
use common\assets\ButtonMultiActionAsset;
use common\printDocs\PrintModule;
use frontend\assets\BillUIAsset;

/**
 * BillController implements the CRUD actions for Bill model.
 */
class BillController extends FilSMAttachmentController
{
    public $attachmentDir = 'bill';
    public $attachmentClass = 'common\models\bill\BillAttachment';
    public $foreignKeyField = 'bill_id';

    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = Bill::class;
        $this->defaultSearchModel = BillSearch::class;
        $this->pjaxIndex = true;
        
        \lajax\translatemanager\helpers\Language::registerAssets();
    }

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [];
        $_get = Yii::$app->request->get();
        $id = isset($_get['id']) ? $_get['id'] : null;
        $model = !empty($id) ? $this->findModel($id) : null;
        $behaviors = ArrayHelper::merge(
            $behaviors, [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'actions' => [
                                'ajax-get-model',
                                'ajax-get-doc-confirm',
                                'ajax-bill-pay',
                                'check-delayed',
                                'ajax-clear-reserved-id',
                                'view-mail-pdf',
                                'receive-mail-gif',
                                'ajax-modal-name-list',
                                'ajax-validate-unique-number',
                            ],
                            'allow' => true,
                        ],                        
                        [
                            'actions' => [
                                'index',
                                'bill-detail',
                            ],
                            'allow' => FSMAccessHelper::checkRoute('/bill/*'),
                        ],
                        [
                            'actions' => [
                                'view', 
                                'send-mail-client',
                                'send-mail-agent',
                            ],
                            'allow' => FSMAccessHelper::can('viewBill', $model),
                        ],
                        [
                            'actions' => [
                                'report-details-debitor-creditor', 
                                'report-details-ebitda', 
                                'report-details-ebitda-client', 
                                'report-details-vat',
                            ],
                            'allow' => FSMAccessHelper::checkRoute('/bill/*'),
                        ],
                        [
                            'actions' => [
                                'view-pdf', 
                            ],
                            'allow' => 
                                FSMAccessHelper::can('viewBill', $model) || 
                                FSMAccessHelper::can('viewBillPDF', $model),
                        ],
                        [
                            'actions' => [
                                'view-act-pdf', 
                            ],
                            'allow' => 
                                FSMAccessHelper::can('viewBill', $model) || 
                                FSMAccessHelper::can('viewBillActPDF', $model),
                        ],
                        [
                            'actions' => [
                                'create', 
                                'copy', 
                                'bill-cession', 
                                'bill-credit-invoice', 
                                'bill-write-on-basis', 
                                'bill-write-on-basis-many',
                            ],
                            'allow' => FSMAccessHelper::can('createBill'),
                        ],
                        [
                            'actions' => ['ajax-add-attachment', 'attachment-delete'],
                            'allow' => FSMAccessHelper::can('updateBill', $model),
                        ],
                        [
                            'actions' => ['update'],
                            'allow' => (FSMAccessHelper::can('updateBill', $model) && empty($model->blocked)) || FSMUser::getIsPortalAdmin(),
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => FSMAccessHelper::can('updateBill', $model) && empty($model->blocked),
                        ],
                        [
                            'actions' => ['block'],
                            'allow' => FSMAccessHelper::can('blockBill', $model) && empty($model->blocked),
                        ],
                        [
                            'actions' => ['unblock'],
                            'allow' => FSMAccessHelper::can('blockBill', $model) && !empty($model->blocked),
                        ],
                    ],
                ],                
            ]
        );
        return $behaviors;
    }
    
    public function actionAjaxGetModel($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (empty($id)) {
            return [];
        }
        $model = $this->findModel($id);
        $model->notPaidSumma = $model->billNotPaidSumma;
        return $model;
    } 
    
    public function actionAjaxGetDocConfirm($id, $for_client_id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (empty($id)) {
            return [];
        }
        $model = $this->findModel($id);
        $agreementModel = $model->agreement;
        
        $secondClientModel = ($model->doc_type != Bill::BILL_DOC_TYPE_CESSION) ? $agreementModel->secondClient : ($model->cession_direction == 'D' ? $agreementModel->thirdClient : $agreementModel->secondClient);
        $firstClientBankAccount = !empty($model->first_client_bank_id) ? $model->firstClientBank->account : null;
        $secondClientBankAccount = !empty($model->second_client_bank_id) ? $model->secondClientBank->account : null;
        
        $result['notPaidSumma'] = $model->billNotPaidSumma;
        $result['rate'] = $model->rate;
        $result['currency'] = $model->valuta_id;
        $result['firstClientBankAccount'] = $firstClientBankAccount;
        $result['secondClientId'] = $secondClientModel->id;
        $result['secondClientName'] = $secondClientModel->name;
        $result['secondClientReg'] = $secondClientModel->reg_number;
        $result['secondClientBankAccount'] = $secondClientBankAccount;
        
        return $result;
    }
    
    public function actionAjaxValidateUniqueNumber($doc_number, $id = null) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (empty($doc_number)) {
            return [];
        }
        
        $baseTableName = Bill::tableName();
        $query = Bill::find(true);
        $query->andWhere([$baseTableName.'.doc_number' => $doc_number]);
        $query->andWhere([$baseTableName.'.deleted' => 0]);
        if(!empty($id)){
            $query->andWhere(['not', [$baseTableName.'.id' => $id]]);
        }
        $exists = $query->exists();
        if ($exists) {
            $message = Yii::t('yii', '{attribute} "{value}" has already been taken.', ['attribute' => Yii::t('bill', 'Doc.number'), 'value' => $doc_number]);
        }
        
        return [
            'exists' => $exists,
            'message' => $message ?? null,
        ];
    } 
    
    /**
     * Url action - /bill/bill-detail
     */
    public function actionBillDetail() {
        if (isset($_POST['expandRowKey'])) {
            $searchModel = new $this->defaultSearchModel;
            $params = Yii::$app->request->getQueryParams();
            $params['deleted'] = (empty($params) || empty($params['BillSearch'])) ?
                0 : (int)!empty($params['BillSearch']['deleted']);
            $params['agreement_id'] = $_POST['expandRowKey'];
            
            $dataProvider = $searchModel->search($params);

            $filter = isset($params['BillSearch']) ? $params['BillSearch'] : [];
            $isAdmin = FSMUser::getIsPortalAdmin();
            return $this->renderPartial('agr-detail-index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'isAdmin' => $isAdmin,
            ]);
        } else {
            return '<div class="alert alert-danger">No data found</div>';
        }
    }
    
    /**
     * Lists all models.
     * @return mixed
     */
    public function actionIndex() 
    {
        BillUIAsset::register(Yii::$app->getView());
        ButtonDeleteAsset::register(Yii::$app->getView());
        ButtonMultiActionAsset::register(Yii::$app->getView());
        
        $searchModel = new $this->defaultSearchModel;
        $params = Yii::$app->request->getQueryParams();
        $params['deleted'] = (empty($params) || empty($params['BillSearch'])) ?
            0 : (int)!empty($params['BillSearch']['deleted']);

        $dataProvider = $searchModel->search($params);

        $filter = isset($params['BillSearch']) ? $params['BillSearch'] : [];
        $isAdmin = FSMUser::getIsPortalAdmin();
        $projectList = Project::getNameArr(['project.deleted' => 0]);
        $agreementFilter = [
            'agreement.deleted' => 0,
        ];
        if(!empty($filter['project_id'])){
            $agreementFilter['project_id'] = $filter['project_id'];
        }
        $agreementList = Agreement::getNameArr($agreementFilter);
        $clientList = Client::getNameArr(['client.deleted' => 0]);
        $managerList = FSMProfile::getProfileListByRole([
            FSMUser::USER_ROLE_ADMIN,
            FSMUser::USER_ROLE_OPERATOR, 
            FSMUser::USER_ROLE_BOOKER,
            FSMUser::USER_ROLE_COORDINATOR,
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'projectList' => $projectList,
            'agreementList' => $agreementList,
            'clientList' => $clientList,
            'managerList' => $managerList,
            'isAdmin' => $isAdmin,
        ]);
    }

    public function actionReportDetailsEbitda($project_id)
    {
        BillUIAsset::register(Yii::$app->getView());
        ButtonDeleteAsset::register(Yii::$app->getView());
        ButtonMultiActionAsset::register(Yii::$app->getView());
        
        $params = Yii::$app->request->getQueryParams();
        
        $reportTitle = Yii::t('report', 'EBITDA report details: ').Project::findOne($project_id)->name.
            " ({$params['from']} / {$params['till']})";
        unset($params['from'], $params['till']);
        
        $searchModel = new $this->defaultSearchModel;
        $params['ids'] = $searchModel->getIdsByProject($params);
        $dataProvider = $searchModel->search($params);

        $searchExpenseModel = new \common\models\bill\search\ExpenseSearch();
        $params['expense_ids'] = $params['direction'] == 'out' ? $searchExpenseModel->getIdsByProject($params) : [];
        $dataExpenseProvider = $searchExpenseModel->search($params);
        
        $isAdmin = FSMUser::getIsPortalAdmin();

        if(isset($_POST['export_type'])){
            if(isset($_POST['exportFull_bill-index-menu'])){
                return $this->render('ebitda-index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'isAdmin' => $isAdmin,
                ]);          
            }elseif(isset($_POST['exportFull_expense-index-menu'])){
                return $this->render('@frontend/views/bill/expense/ebitda-index', [
                    'dataProvider' => $dataExpenseProvider,
                    'searchModel' => $searchExpenseModel,
                    'isAdmin' => $isAdmin,
                ]);
            }
        }else{
            return $this->render('ebitda-details', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'dataExpenseProvider' => $dataExpenseProvider,
                'searchExpenseModel' => $searchExpenseModel,
                'isAdmin' => $isAdmin,
                'reportTitle' => $reportTitle,
                'direction' => !empty($params['direction']) ? $params['direction'] : null,
            ]);       
        }
    }

    public function actionReportDetailsEbitdaClient($client_id)
    {
        BillUIAsset::register(Yii::$app->getView());
        ButtonDeleteAsset::register(Yii::$app->getView());
        ButtonMultiActionAsset::register(Yii::$app->getView());
        
        $params = Yii::$app->request->getQueryParams();
        
        $reportTitle = 
            (!empty($params['direction']) ? ($params['direction'] == 'in' ? Yii::t('report', 'Sales') : Yii::t('report', 'Purchases')).'. ' : '').
            Yii::t('report', 'EBITDA report details: ').
            Client::findOne($client_id)->name.
            " ({$params['from']} / {$params['till']})";
        unset($params['from'], $params['till']);
        
        $searchModel = new $this->defaultSearchModel;
        $params['ids'] = $searchModel->getIdsByClient($params);
        $dataProvider = $searchModel->search($params);

        $searchExpenseModel = new \common\models\bill\search\ExpenseSearch();
        $params['expense_ids'] = $params['direction'] == 'out' ? $searchExpenseModel->getIdsByClient($params) : [];
        $dataExpenseProvider = $searchExpenseModel->search($params);
        
        $isAdmin = FSMUser::getIsPortalAdmin();
        if(isset($_POST['export_type'])){
            if(isset($_POST['exportFull_bill-index-menu'])){
                return $this->render('ebitda-index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'client_id' => $client_id,
                    'isAdmin' => $isAdmin,
                ]);          
            }elseif(isset($_POST['exportFull_expense-index-menu'])){
                return $this->render('@frontend/views/bill/expense/ebitda-index', [
                    'dataProvider' => $dataExpenseProvider,
                    'searchModel' => $searchExpenseModel,
                    'client_id' => $client_id,
                    'isAdmin' => $isAdmin,
                ]);
            }
        }else{
            return $this->render('ebitda-details', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'dataExpenseProvider' => $dataExpenseProvider,
                'searchExpenseModel' => $searchExpenseModel,
                'isAdmin' => $isAdmin,
                'reportTitle' => $reportTitle,
                'client_id' => $client_id,
                'direction' => !empty($params['direction']) ? $params['direction'] : null,
            ]);       
        }        
    }
    
    public function actionReportDetailsDebitorCreditor($client_id)
    {
        BillUIAsset::register(Yii::$app->getView());
        ButtonDeleteAsset::register(Yii::$app->getView());
        ButtonMultiActionAsset::register(Yii::$app->getView());
        
        $searchModel = new $this->defaultSearchModel;
        $params = Yii::$app->request->getQueryParams();

        $reportTitle = Yii::t('report', 'Debtors/Creditors report details: ').Client::findOne($client_id)->name.'. '. Yii::t('common', 'Date till').': '.$params['till'];
        $tillDate = $params['till'];
        unset($params['from'], $params['till']);
        $params['pay_status'] = ['not', 'part'];
        $params['doc_type'] = [
            Bill::BILL_DOC_TYPE_BILL, 
            Bill::BILL_DOC_TYPE_INVOICE,
        ];
        
        $params['ids'] = $searchModel->getIdsByClient($params);
        unset($params['pay_status'], $params['doc_type']);
        $dataProvider = $searchModel->search($params);
        unset($params['ids']);
        
        $searchAgrPaymentModel = new \common\models\client\search\AgreementPaymentSearch();
        $params['agr_ids'] = $searchAgrPaymentModel->getIdsByClient($params);
        unset($params['direction']);
        $dataAgrPaymentProvider = $searchAgrPaymentModel->search($params);
        unset($params['agr_ids']);
                
        $filter = isset($params['BillSearch']) ? $params['BillSearch'] : [];
        $isAdmin = FSMUser::getIsPortalAdmin();
        $projectList = Project::getNameArr(['project.deleted' => 0]);
        $agreementFilter = [
            'deleted' => 0,
        ];
        if(!empty($filter['project_id'])){
            $agreementFilter['project_id'] = $filter['project_id'];
        }
        $agreementList = Agreement::getNameArr($agreementFilter);
        $clientList = Client::getNameArr(['client.deleted' => 0]);
        $managerList = FSMProfile::getProfileListByRole([
            FSMUser::USER_ROLE_ADMIN,
            FSMUser::USER_ROLE_OPERATOR, 
            FSMUser::USER_ROLE_BOOKER,
            FSMUser::USER_ROLE_COORDINATOR,
        ]);
        $bankList = Bank::getNameArr(['enabled' => true], '', '', function($bank){return $bank['name'].(!empty($bank['swift']) ? ' | '.$bank['swift'] : '');});
        $valutaList = Valuta::getNameArr(['enabled' => true]);
        
        if(isset($_POST['export_type'])){
            if(isset($_POST['exportFull_bill-index-menu'])){
                return $this->render('debitor-creditor-index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'isAdmin' => $isAdmin,
                    'tillDate' => $tillDate,
                ]);          
            }elseif(isset($_POST['exportFull_agr-payment-index-menu'])){
                return $this->render('@frontend/views/client/agreement-payment/debitor-creditor-index', [
                    'dataProvider' => $dataAgrPaymentProvider,
                    'searchModel' => $searchAgrPaymentModel,
                    'bankList' => $bankList,
                    'isAdmin' => $isAdmin,
                ]);
            }
        }else{
            return $this->render('debitor-creditor-details', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'dataAgrPaymentProvider' => $dataAgrPaymentProvider,
                'searchAgrPaymentModel' => $searchAgrPaymentModel,
                'projectList' => $projectList,
                'agreementList' => $agreementList,
                'clientList' => $clientList,
                'managerList' => $managerList,
                'bankList' => $bankList,
                'valutaList' => $valutaList,
                'isAdmin' => $isAdmin,
                'reportTitle' => $reportTitle,
                'tillDate' => $tillDate,
            ]);       
        }
    }
    
    public function actionReportDetailsVat($client_id)
    {
        BillUIAsset::register(Yii::$app->getView());
        ButtonDeleteAsset::register(Yii::$app->getView());
        ButtonMultiActionAsset::register(Yii::$app->getView());
        
        $params = Yii::$app->request->getQueryParams();

        $reportTitle = Yii::t('report', 'VAT report details: ').Client::findOne($params['client_id'])->name.
            " ({$params['from']} / {$params['till']})";
        
        $searchModel = new $this->defaultSearchModel;
        $params['ids'] = $searchModel->getIdsForVatReport($params);
        $dataProvider = $searchModel->search($params);
        unset($params['client_id'], $params['from'], $params['till']);
        
        $params['expense_ids'] = !empty($params['expense_ids']) ? explode(',', $params['expense_ids']) : [];
        foreach ($params['expense_ids'] as $key => $id) {
            if(empty($id)){
                unset($params['expense_ids'][$key]);
            }
        }           
        $searchExpenseModel = new \common\models\bill\search\ExpenseSearch();
        $dataExpenseProvider = $searchExpenseModel->search($params);
        
        $filter = isset($params['BillSearch']) ? $params['BillSearch'] : [];
        $isAdmin = FSMUser::getIsPortalAdmin();
        $projectList = Project::getNameArr(['project.deleted' => 0]);
        $agreementFilter = [
            'deleted' => 0,
        ];
        if(!empty($filter['project_id'])){
            $agreementFilter['project_id'] = $filter['project_id'];
        }
        $agreementList = Agreement::getNameArr($agreementFilter);
        $clientList = Client::getNameArr(['client.deleted' => 0]);
        $managerList = FSMProfile::getProfileListByRole([
            FSMUser::USER_ROLE_ADMIN,
            FSMUser::USER_ROLE_OPERATOR, 
            FSMUser::USER_ROLE_BOOKER,
            FSMUser::USER_ROLE_COORDINATOR,
        ]);
        $valutaList = Valuta::getNameArr(['enabled' => true]);
        if(isset($_POST['export_type'])){
            if(isset($_POST['exportFull_bill-index-menu'])){
                return $this->render('ebitda-index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'client_id' => $client_id,
                    'isAdmin' => $isAdmin,
                ]);          
            }elseif(isset($_POST['exportFull_expense-index-menu'])){
                return $this->render('@frontend/views/bill/expense/ebitda-index', [
                    'dataProvider' => $dataExpenseProvider,
                    'searchModel' => $searchExpenseModel,
                    'client_id' => $client_id,
                    'isAdmin' => $isAdmin,
                ]);
            }
        }else{
            return $this->render('vat-details', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'dataExpenseProvider' => $dataExpenseProvider,
                'searchExpenseModel' => $searchExpenseModel,
                'projectList' => $projectList,
                'agreementList' => $agreementList,
                'clientList' => $clientList,
                'managerList' => $managerList,
                'valutaList' => $valutaList,
                'isAdmin' => $isAdmin,
                'reportTitle' => $reportTitle,
                'client_id' => $client_id,
                'direction' => !empty($params['direction']) ? $params['direction'] : null,
            ]);     
        }        
    }
    
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($bill_template_id = null) {
        $model = new $this->defaultModel;
        $agreementModel = new Agreement();
        $abonentModel = new AbonentBill();
        $filesModel = [new Files()];

        if(isset($bill_template_id)){
            $model->bill_template_id = $bill_template_id;
            $billTemplate = $model->billTemplate; 
        }
        
        $modelArr = [
            'Bill' => $model,
            'AbonentBill' => $abonentModel,
        ];
        
        // ajax validation
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            $ajaxModelArr = $modelArr;
            
            $billProductModel = FSMBaseModel::createMultiple(BillProduct::class);
            FSMBaseModel::loadMultiple($billProductModel, $requestPost);
            $billProductProjectModel = FSMBaseModel::createMultiple(BillProductProject::class);
            FSMBaseModel::loadMultiple($billProductProjectModel, $requestPost);
            $hasEmptyProject = false;
            foreach ($billProductModel as $index => $billProduct) {
                if(empty($billProduct->product_id) && empty($billProduct->amount) && empty($billProduct->price)){
                    unset($billProductModel[$index]);
                    continue;
                }

                if(empty($billProductProjectModel[$index]->project_id) &&
                    empty($requestPost['AbonentBill']['project_id'])
                ){
                    $billProductProjectModel[$index]->needValidateProject = $hasEmptyProject = true;
                }
            }
            if (!empty($billProductModel)) {
                $ajaxModelArr = ArrayHelper::merge($modelArr, ['BillProduct' => $billProductModel]);
            }
            if (!empty($billProductProjectModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['BillProductProject' => $billProductProjectModel]);
            }
            $ajaxModelArr['AbonentBill']->needValidateProject = $hasEmptyProject;

            $billPersonModel = FSMBaseModel::createMultiple(FirstBillPerson::class);
            FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
            foreach ($billPersonModel as $index => $billPerson) {
                if(empty($billPerson->client_person_id)){
                    unset($billPersonModel[$index]);
                    continue;
                }
            }
            if (!empty($billPersonModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['FirstBillPerson' => $billPersonModel]);
            }
            
            $billPersonModel = FSMBaseModel::createMultiple(SecondBillPerson::class);
            FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
            foreach ($billPersonModel as $index => $billPerson) {
                if(empty($billPerson->client_person_id)){
                    unset($billPersonModel[$index]);
                    continue;
                }
            }
            if (!empty($billPersonModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['SecondBillPerson' => $billPersonModel]);
            }
            
            return $this->performAjaxMultipleValidation($ajaxModelArr);            
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                if(empty($model->agreement_id)){
                    throw new Exception('Agreement was not defined!');
                }
                
                if (!$model->save()) {
                    throw new Exception('Unable to save data! '.$model->errorMessage);
                }
                $abonentModel->abonent_id = $model->userAbonentId;
                $abonentModel->bill_id = $model->id;
                $abonentModel->save();

                if(!$this->createAttachment($model, $filesModel)){
                    throw new Exception('Unable to save attachment! '.$filesModel->errorMessage);
                }

                $historyModule = new BillHistory();
                $historyModule->saveHistory($model->id, BillHistory::BillHistory_ACTIONS['ACTION_CREATE']);
                unset($historyModule);

                $flag = true;
                
                $billProductModel = FSMBaseModel::createMultiple(BillProduct::class);
                FSMBaseModel::loadMultiple($billProductModel, $requestPost);
                $billProductProjectModel = FSMBaseModel::createMultiple(BillProductProject::class);
                FSMBaseModel::loadMultiple($billProductProjectModel, $requestPost);            
                foreach ($billProductModel as $index => $billProduct) {
                    if(empty($billProduct->product_id) && empty($billProduct->amount) && empty($billProduct->price)){
                        unset($billProductModel[$index]);
                        if(isset($billProductProjectModel[$index])){
                            unset($billProductProjectModel[$index]);
                        }
                        continue;
                    }
                    $billProduct->bill_id = $model->id;
                    $billProduct->price_eur = $billProduct->price / $model->rate;
                    $billProduct->summa_eur = $billProduct->summa / $model->rate;
                    $billProduct->summa_vat_eur = $billProduct->summa_vat / $model->rate;
                    $billProduct->total_eur = $billProduct->total / $model->rate;

                    if($model->according_contract){
                        $billProduct->product_id = null;
                    }
                }

                if(!empty($billProductModel)){
                    if ($flag = FSMBaseModel::validateMultiple($billProductModel)) {
                        foreach ($billProductModel as $index => $billProduct) {
                            if (!$billProduct->save(false)) {
                                throw new Exception('Unable to save data! '.$billProduct->errorMessage);
                            }
                            $billProductProjectModel[$index]->bill_id = $model->id;
                            $billProductProjectModel[$index]->bill_product_id = $billProduct->id;
                            if(empty($billProductProjectModel[$index]->project_id)){
                                $billProductProjectModel[$index]->project_id = $abonentModel->project_id;
                            }
                            if (!$billProductProjectModel[$index]->save(false)) {
                                throw new Exception('Unable to save data! '.$billProductProjectModel[$index]->errorMessage);
                            }
                        }
                    }
                }

                if(empty($model->e_signing)){
                    $isEmptyFirstPersonList = $isEmptySecondPersonList = false;

                    $billPersonModel = FSMBaseModel::createMultiple(FirstBillPerson::class);
                    FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
                    foreach ($billPersonModel as $index => $billPerson) {
                        if(empty($billPerson->client_person_id)){
                            unset($billPersonModel[$index]);
                            continue;
                        }
                        $billPerson->bill_id = $model->id;                      
                        $billPerson->person_order = 1;                      
                    }
                    if(!empty($billPersonModel)){
                        if ($flag = FSMBaseModel::validateMultiple($billPersonModel)) {
                            foreach ($billPersonModel as $billPerson) {
                                if (!$billPerson->save(false)) {
                                    throw new Exception('Unable to save data! '.$billPerson->errorMessage);
                                }
                            }
                        }
                    }else{
                        $isEmptyFirstPersonList = true;
                    }

                    $billPersonModel = FSMBaseModel::createMultiple(SecondBillPerson::class);
                    FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
                    foreach ($billPersonModel as $index => $billPerson) {
                        if(empty($billPerson->client_person_id)){
                            unset($billPersonModel[$index]);
                            continue;
                        }
                        $billPerson->bill_id = $model->id;                      
                        $billPerson->person_order = 2;                      
                    }
                    if(!empty($billPersonModel)){
                        if ($flag = FSMBaseModel::validateMultiple($billPersonModel)) {
                            foreach ($billPersonModel as $billPerson) {
                                if (!$billPerson->save(false)) {
                                    throw new Exception('Unable to save data! '.$billPerson->errorMessage);
                                }
                            }
                        }
                    }else{
                        $isEmptySecondPersonList = true;
                    }
                    if($isEmptyFirstPersonList && $isEmptySecondPersonList && !in_array($model->doc_type, [
                        Bill::BILL_DOC_TYPE_INVOICE, 
                    ])){
                        $model->updateAttributes(['e_signing' => true]);
                    }                        
                }

                if(!empty($requestPost['Bill']['reserved_id'])){
                    ReservedId::deleteAll(['id' => $requestPost['Bill']['reserved_id']]);
                }
                if (!$flag) {
                    throw new Exception('Unable to save data!');
                }else{
                    $transaction->commit();
                } 
            } catch (Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                if ($isPjax) {
                    return $this->actionAjaxModalNameList(['selected_id' => $model->id]);
                } else {
                    return $this->redirect('index');
                }
            }                
        } else {
            BillUIAsset::register(Yii::$app->getView());

            $agreementModel = isset($billTemplate) ? $billTemplate->agreement : $agreementModel;
            if(isset($billTemplate)){
                $tempAbonentModel = \common\models\abonent\AbonentBillTemplate::findOne([
                    'abonent_id' => $model->userAbonentId, 
                    'bill_template_id' => $bill_template_id,
                ]);
                $abonentModel->project_id = $tempAbonentModel->project_id;
                $model->agreement_id = $billTemplate->agreement_id;
                $model->first_client_bank_id = $billTemplate->first_client_bank_id;
                $model->second_client_bank_id = $billTemplate->second_client_bank_id;
                $model->according_contract = $billTemplate->according_contract;
                $model->justification = $billTemplate->justification;
                $model->place_service = $billTemplate->place_service;
                $model->comment_special = $billTemplate->comment_special;
                $model->comment = $billTemplate->comment;
                $model->e_signing = $billTemplate->e_signing;
                $model->language_id = $billTemplate->language_id;
                $model->summa = $billTemplate->summa;
                $model->vat = $billTemplate->vat;
                $model->total = $billTemplate->total;
            }else{
                $model->summa = '0.00';
            }
            
            $model->doc_date = date('d-m-Y');
            $model->pay_date = date('d-m-Y', strtotime("+".Bill::BILL_DEFAULT_PAYMENT_DAYS." days", strtotime($model->doc_date)));
            $model->valuta_id = isset($billTemplate) ? $billTemplate->valuta_id : Valuta::VALUTA_DEFAULT;
            $model->reserved_id = Yii::$app->security->generateRandomString();
            
            if($rateModel = \common\models\ValutaRate::getByDate($model->valuta->name, date('Y-m-d'))){
                $model->rate = number_format($rateModel->rate, 4, '.', '');
            }
            
            if(isset($billTemplate)){
                $billTemplateProductList = $billTemplate->billProducts;
                if(empty($billTemplateProductList)){
                    $billProductModel = [new BillProduct()];
		    $billProductProjectModel = [new BillProductProject()];
                }else{
                    $billProductModel = $billProductProjectModel = [];
                    foreach ($billTemplateProductList as $billTemplateProduct) {
                        $billProduct = new BillProduct();
                        $billProduct->product_id = $billTemplateProduct->product_id;
                        $billProduct->product_name = $billTemplateProduct->product_name;
                        $billProduct->measure_id = $billTemplateProduct->measure_id;
                        $billProduct->amount = $billTemplateProduct->amount;
                        $billProduct->price = $billTemplateProduct->price;
                        $billProduct->vat = $billTemplateProduct->vat;
                        $billProduct->revers = $billTemplateProduct->revers;
                        $billProduct->summa = $billTemplateProduct->summa;
                        $billProduct->summa_vat = $billTemplateProduct->summa_vat;
                        $billProduct->total = $billTemplateProduct->total;
                        $billProduct->price_eur = $billTemplateProduct->price_eur;
                        $billProduct->summa_eur = $billTemplateProduct->summa_eur;
                        $billProduct->summa_vat_eur = $billTemplateProduct->summa_vat_eur;
                        $billProduct->total_eur = $billTemplateProduct->total_eur;
                        $billProduct->comment = $billTemplateProduct->comment;  
                        $billProductModel[] = $billProduct;
                    }
                }
            }else{
                $billProductModel = [new BillProduct()];
                $billProductProjectModel = [new BillProductProject()];
            }
            
            if(isset($billTemplate)){
                $billTemplatePersonList = $billTemplate->firstClientPerson;
                if(empty($billTemplatePersonList)){
                    $billFirstPersonModel = [new FirstBillPerson(), new FirstBillPerson()];
                }else{
                    $billFirstPersonModel = [];
                    foreach ($billTemplatePersonList as $billTemplatePerson) {
                        $billPerson = new FirstBillPerson();
                        $billPerson->client_person_id = $billTemplatePerson->client_person_id;
                        $billPerson->person_order = $billTemplatePerson->person_order;
                        $billFirstPersonModel[] = $billPerson;
                    }
                    if(count($billFirstPersonModel) == 1){
                        $billFirstPersonModel[] = new FirstBillPerson();
                    }
                }
            }else{
                $billFirstPersonModel = [new FirstBillPerson(), new FirstBillPerson()];
            }
            
            if(isset($billTemplate)){
                $billTemplatePersonList = $billTemplate->secondClientPerson;
                if(empty($billTemplatePersonList)){
                    $billSecondPersonModel = [new SecondBillPerson(), new SecondBillPerson()];
                }else{
                    $billSecondPersonModel = [];
                    foreach ($billTemplatePersonList as $billTemplatePerson) {
                        $billPerson = new SecondBillPerson();
                        $billPerson->client_person_id = $billTemplatePerson->client_person_id;
                        $billPerson->person_order = $billTemplatePerson->person_order;
                        $billSecondPersonModel[] = $billPerson;
                    }   
                    if(count($billSecondPersonModel) == 1){
                        $billSecondPersonModel[] = new SecondBillPerson();
                    }
                }
            }else{
                $billSecondPersonModel = [new SecondBillPerson(), new SecondBillPerson()];
            }

            $isAdmin = FSMUser::getIsPortalAdmin();
            $agreementList = Agreement::getNameArr(['agreement.deleted' => false]);
            $projectList = Project::getNameArr(['project.deleted' => 0]);
            $clientRoleList = ClientRole::getNameArr();
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            $productList = Product::getNameArr();
            $measureList = Measure::getNameArr();
            $languageList = Language::getEnabledLanguageList();
            $managerList = FSMProfile::getProfileListByRole([
                FSMUser::USER_ROLE_ADMIN,
                FSMUser::USER_ROLE_OPERATOR, 
                FSMUser::USER_ROLE_BOOKER,
                FSMUser::USER_ROLE_COORDINATOR,
            ]);
            if(!Yii::$app->user->isGuest && empty($model->manager_id)){
                $profile = Yii::$app->user->identity->profile;
                if(array_key_exists($profile->id, $managerList)){
                    $model->manager_id = $profile->id;
                }                
            }
            
            $data = [
                'model' => $model,
                'abonentModel' => $abonentModel,
                'agreementModel' => $agreementModel,
                'firstClientModel' => new Client(),
                'secondClientModel' => new Client(),
                'filesModel' => $filesModel,
                'agreementList' => $agreementList,
                'projectList' => $projectList,
                'clientRoleList' => $clientRoleList,
                'valutaList' => $valutaList,
                'managerList' => $managerList,
                'billProductModel' => $billProductModel,
                'billProductProjectModel' => $billProductProjectModel ?? [],
                'billFirstPersonModel' => $billFirstPersonModel,
                'billSecondPersonModel' => $billSecondPersonModel,
                'productList' => $productList,
                'measureList' => $measureList,
                'languageList' => $languageList,
                'isAdmin' => $isAdmin,
                'isModal' => $isAjax,
            ];
            if ($isAjax) {
                return $this->renderAjax('create', $data);
            } else {
                return $this->render('create', $data);
            }            
        }
    }    

    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $isAdmin = FSMUser::getIsPortalAdmin();
        
        $model = $this->findModel($id, true);
        $abonentModel = AbonentBill::findOne([
            'abonent_id' => $model->userAbonentId, 
            'bill_id' => $id,
        ]);        
        $agreementModel = $model->agreement;
        $agreementModel = (!empty($agreementModel) ? $agreementModel : new Agreement());
        $billProductModel = $model->billProducts;        
        $billProductModel = !empty($billProductModel) ? $billProductModel : [new BillProduct()];
        $billFirstPersonModel = $model->firstClientPerson;        
        $billFirstPersonModel = !empty($billFirstPersonModel) ? $billFirstPersonModel : [new FirstBillPerson(), new FirstBillPerson()];
        $billSecondPersonModel = $model->secondClientPerson;        
        $billSecondPersonModel = !empty($billSecondPersonModel) ? $billSecondPersonModel : [new SecondBillPerson(),new SecondBillPerson()];
        $billProductProjectModel = $model->billProductProjectsByAbonent;
        $billProductProjectModel = !empty($billProductProjectModel) ? $billProductProjectModel : [new BillProductProject()];
        
        $filesModel = $model->attachmentFiles;
        $filesModel = (!empty($filesModel) ? $filesModel : [new Files()]);
        
        $modelArr = [
            'Bill' => $model,
            'AbonentBill' => $abonentModel,
        ];
        
        // ajax validation
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && $isAjax) {
            $ajaxModelArr = $modelArr;
            
            $billProductModel = FSMBaseModel::createMultiple(BillProduct::class);
            FSMBaseModel::loadMultiple($billProductModel, $requestPost);
            $billProductProjectModel = FSMBaseModel::createMultiple(BillProductProject::class);
            FSMBaseModel::loadMultiple($billProductProjectModel, $requestPost);
            $hasEmptyProject  = false;
            foreach ($billProductModel as $index => $billProduct) {
                if(empty($billProduct->product_id) && empty($billProduct->amount) && empty($billProduct->price)){
                    unset($billProductModel[$index]);
                    if(isset($billProductProjectModel[$index])){
                        unset($billProductProjectModel[$index]);
                    }
                    continue;
                }

                if(empty($billProductProjectModel[$index]->project_id) &&
                    empty($requestPost['AbonentBill']['project_id'])
                ){
                    $billProductProjectModel[$index]->needValidateProject = $hasEmptyProject = true;
                }
            }
            if (!empty($billProductModel)) {
                $ajaxModelArr = ArrayHelper::merge($modelArr, ['BillProduct' => $billProductModel]);
            }
            if (!empty($billProductProjectModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['BillProductProject' => $billProductProjectModel]);
            }
            $ajaxModelArr['AbonentBill']->needValidateProject = $hasEmptyProject;

            $billPersonModel = FSMBaseModel::createMultiple(FirstBillPerson::class);
            FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
            foreach ($billPersonModel as $index => $billPerson) {
                if(empty($billPerson->client_person_id)){
                    unset($billPersonModel[$index]);
                    continue;
                }
            }
            if (!empty($billPersonModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['FirstBillPerson' => $billPersonModel]);
            }
            
            $billPersonModel = FSMBaseModel::createMultiple(SecondBillPerson::class);
            FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
            foreach ($billPersonModel as $index => $billPerson) {
                if(empty($billPerson->client_person_id)){
                    unset($billPersonModel[$index]);
                    continue;
                }
            }
            if (!empty($billPersonModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['SecondBillPerson' => $billPersonModel]);
            }
            
            $result = $this->performAjaxMultipleValidation($ajaxModelArr);
            return $result;
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                if(empty($model->agreement_id)){
                    throw new Exception('Agreement was not defined!');
                }
                
                //if(!$isAdmin){
                    $paymentsSumma = $model->billPaidSumma;
                    if($paymentsSumma == 0){
                        $model->pay_status = Bill::BILL_PAY_STATUS_NOT;
                    }else{
                        $model->changeStatus(Bill::BILL_STATUS_PAID);                    
                    }
                //}

                $flag = true;
                if(empty($model->e_signing)){
                    $isEmptyFirstPersonList = $isEmptySecondPersonList = false;
                    
                    $oldPersonIDs = isset($billFirstPersonModel[0]) && !empty($billFirstPersonModel[0]->id) ? ArrayHelper::map($billFirstPersonModel, 'id', 'id') : [];
                    $billPersonModel = FSMBaseModel::createMultiple(FirstBillPerson::class, (!empty($oldPersonIDs) ? $billFirstPersonModel : []));
                    FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
                    $deletedIDs = array_diff($oldPersonIDs, array_filter(ArrayHelper::map($billPersonModel, 'id', 'id')));

                    foreach ($billPersonModel as $index => $billPerson) {
                        if(empty($billPerson->client_person_id)){
                            if(!empty($billPerson->id)){
                                $billPerson->delete();
                            }
                            unset($billPersonModel[$index]);
                            continue;
                        }
                        $billPerson->bill_id = $model->id;                      
                        $billPerson->person_order = 1;                      
                    }

                    if(!empty($billPersonModel)){
                        if ($flag = FSMBaseModel::validateMultiple($billPersonModel)) {
                            if (!empty($deletedIDs)) {
                                $flag = FirstBillPerson::deleteByIDs($deletedIDs);
                            }     
                            if ($flag) {
                                foreach ($billPersonModel as $billPerson) {
                                    if (!$billPerson->save(false)) {
                                        throw new Exception('Unable to save data! '.$billPerson->errorMessage);
                                    }
                                }
                            }
                        }
                    }else{
                        $isEmptyFirstPersonList = true;
                    }

                    $oldPersonIDs = isset($billSecondPersonModel[0]) && !empty($billSecondPersonModel[0]->id) ? ArrayHelper::map($billSecondPersonModel, 'id', 'id') : [];
                    $billPersonModel = FSMBaseModel::createMultiple(SecondBillPerson::class, (!empty($oldPersonIDs) ? $billSecondPersonModel : []));
                    FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
                    $deletedIDs = array_diff($oldPersonIDs, array_filter(ArrayHelper::map($billPersonModel, 'id', 'id')));

                    foreach ($billPersonModel as $index => $billPerson) {
                        if(empty($billPerson->client_person_id)){
                            if(!empty($billPerson->id)){
                                $billPerson->delete();
                            }
                            unset($billPersonModel[$index]);
                            continue;
                        }
                        $billPerson->bill_id = $model->id;                      
                        $billPerson->person_order = 2;                      
                    }

                    if(!empty($billPersonModel)){
                        if ($flag = FSMBaseModel::validateMultiple($billPersonModel)) {
                            if (!empty($deletedIDs)) {
                                $flag = SecondBillPerson::deleteByIDs($deletedIDs);
                            }     
                            if ($flag) {
                                foreach ($billPersonModel as $billPerson) {
                                    if (!$billPerson->save(false)) {
                                        throw new Exception('Unable to save data! '.$billPerson->errorMessage);
                                    }
                                }
                            }
                        }
                    }else{
                        $isEmptySecondPersonList = true;
                    }
                    
                    if($isEmptyFirstPersonList && $isEmptySecondPersonList && !in_array($model->doc_type, [
                        Bill::BILL_DOC_TYPE_INVOICE, 
                    ])){
                        $model->e_signing = 1;
                    }
                }else{
                    $deletedIDs = isset($billFirstPersonModel[0]) && !empty($billFirstPersonModel[0]->id) ? ArrayHelper::map($billFirstPersonModel, 'id', 'id') : [];
                    if (!empty($deletedIDs)) {
                        $flag = FirstBillPerson::deleteByIDs($deletedIDs);
                    }                          
                    $deletedIDs = isset($billSecondPersonModel[0]) && !empty($billSecondPersonModel[0]->id) ? ArrayHelper::map($billSecondPersonModel, 'id', 'id') : [];
                    if (!empty($deletedIDs)) {
                        $flag = SecondBillPerson::deleteByIDs($deletedIDs);
                    }     
                }
                
                $model->mail_status = (empty($model->mail_status) ? null : $model->mail_status);
                $model->transfer_status = (empty($model->transfer_status) ? null : $model->transfer_status);
                //$model->pay_date = date('Y-m-d', strtotime("+".Bill::BILL_DEFAULT_PAYMENT_DAYS." days", strtotime($model->doc_date)));
                if (!$model->save()) {
                    throw new Exception('Unable to save data! '.$model->errorMessage);
                }
                $abonentModel->save();

                $historyModule = new BillHistory();
                $historyModule->saveHistory($model->id, BillHistory::BillHistory_ACTIONS['ACTION_EDIT']);
                unset($historyModule);

                if($model->doc_type == Bill::BILL_DOC_TYPE_CRBILL){
                    $parentInvoice = $model->parent;
                    $parentInvoice->updateAttributes([
                        'pay_status' => (($parentInvoice->total == ($model->total * -1)) ? Bill::BILL_PAY_STATUS_FULL : Bill::BILL_PAY_STATUS_PART), 
                    ]);
                }

                $oldProductIDs = isset($billProductModel[0]) && !empty($billProductModel[0]->id) ? ArrayHelper::map($billProductModel, 'id', 'id') : [];

                $billProductModel = FSMBaseModel::createMultiple(BillProduct::class, $billProductModel);
                FSMBaseModel::loadMultiple($billProductModel, $requestPost);  
                $deletedIDs = array_diff($oldProductIDs, array_filter(ArrayHelper::map($billProductModel, 'id', 'id')));
                
                $billProductProjectModel = FSMBaseModel::createMultiple(BillProductProject::class, $billProductProjectModel);
                FSMBaseModel::loadMultiple($billProductProjectModel, $requestPost);

                foreach ($billProductModel as $index => $billProduct) {
                    if(empty($billProduct->productName) && empty($billProduct->amount) && empty($billProduct->price)){
                        unset($billProductModel[$index]);
                        continue;
                    }
                    $billProduct->bill_id = $model->id;
                    $billProduct->price_eur = $billProduct->price / $model->rate;
                    $billProduct->summa_eur = $billProduct->summa / $model->rate;
                    $billProduct->summa_vat_eur = $billProduct->summa_vat / $model->rate;
                    $billProduct->total_eur = $billProduct->total / $model->rate;
                    if($model->according_contract){
                        $billProduct->product_id = null;
                    }
                }
                if(!empty($billProductModel)){
                    if ($flag = FSMBaseModel::validateMultiple($billProductModel)) {
                        if (!empty($deletedIDs)) {
                            $flag = BillProduct::deleteByIDs($deletedIDs);
                        }     
                        if ($flag) {
                            foreach ($billProductModel as $index => $billProduct) {
                                if (!$billProduct->save(false)) {
                                    throw new Exception('Unable to save data! '.$billProduct->errorMessage);
                                }
                                $billProductProjectModel[$index]->bill_id = $model->id;
                                $billProductProjectModel[$index]->bill_product_id = $billProduct->id;
                                if(empty($billProductProjectModel[$index]->project_id)){
                                    $billProductProjectModel[$index]->project_id = $abonentModel->project_id;
                                }
                                if (!$billProductProjectModel[$index]->save(false)) {
                                    throw new Exception('Unable to save data! '.$billProductProjectModel[$index]->errorMessage);
                                }
                            }
                        }
                    }
                }
                
                if(!$this->updateAttacment($model, $filesModel)){
                    throw new Exception('Unable to save attachment! '.$filesModel->errorMessage);
                }
                
                if (!$flag) {
                    throw new Exception('Unable to save data!');
                }else{
                    $transaction->commit();
                }          
            } catch (Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirectToBackUrl($id);              
            }                
        } else {
            $this->rememberBackUrl($model->backURL, $id);
            Yii::$app->session->remove('file_delete');
            
            BillUIAsset::register(Yii::$app->getView());

            $model->doc_date = date('d-m-Y', strtotime($model->doc_date));
            $model->pay_date = date('d-m-Y', strtotime($model->pay_date));
            $model->services_period_from = !empty($model->services_period_from) ? date('d-m-Y', strtotime($model->services_period_from)) : null;
            $model->services_period_till = !empty($model->services_period_till) ? date('d-m-Y', strtotime($model->services_period_till)) : null;
            
            if($model->doc_type == Bill::BILL_DOC_TYPE_CRBILL){
                $model->summa = $model->summa * -1;
                $model->vat = $model->vat * -1;
                $model->total = $model->total * -1;
            }
            
            $agreementList = Agreement::getNameArr(['agreement.deleted' => false]);
            $projectList = Project::getNameArr(['project.deleted' => 0]);
            $clientRoleList = ClientRole::getNameArr();
            $productList = Product::getNameArr();
            $measureList = Measure::getNameArr();
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            $languageList = Language::getEnabledLanguageList();
            $managerList = FSMProfile::getProfileListByRole([
                FSMUser::USER_ROLE_ADMIN,
                FSMUser::USER_ROLE_OPERATOR, 
                FSMUser::USER_ROLE_BOOKER,
                FSMUser::USER_ROLE_COORDINATOR,
            ]);
            if(!Yii::$app->user->isGuest && empty($model->manager_id)){
                $profile = Yii::$app->user->identity->profile;
                if(array_key_exists($profile->id, $managerList)){
                    $model->manager_id = $profile->id;
                }                
            }
            $hideDocType = !empty($model->child) || !empty($model->parent) || !empty($model->parents);
            
            $firstClientModel = ($model->doc_type != Bill::BILL_DOC_TYPE_CESSION) ? $agreementModel->firstClient : ($model->cession_direction == 'D' ? $agreementModel->firstClient : $agreementModel->thirdClient);
            $secondClientModel = ($model->doc_type != Bill::BILL_DOC_TYPE_CESSION) ? $agreementModel->secondClient : ($model->cession_direction == 'D' ? $agreementModel->thirdClient : $agreementModel->secondClient);
            
            if(count($billFirstPersonModel) == 1){
                $billFirstPersonModel[1] = new FirstBillPerson();
            }
            if(count($billSecondPersonModel) == 1){
                $billSecondPersonModel[1] = new SecondBillPerson();
            }
            
            foreach ($billProductModel as $billProduct) {
                $billProduct->summa = abs($billProduct->summa);
                $billProduct->summa_vat = abs($billProduct->summa_vat);
                $billProduct->total = abs($billProduct->total);
                $billProduct->summa_eur = abs($billProduct->summa_eur);
                $billProduct->summa_vat_eur = abs($billProduct->summa_vat_eur);
                $billProduct->total_eur = abs($billProduct->total_eur);  
            }
                        
            return $this->render('update', [
                'model' => $model,
                'abonentModel' => $abonentModel,
                'agreementModel' => $agreementModel,
                'firstClientModel' => $firstClientModel,
                'secondClientModel' => $secondClientModel,
                'billProductModel' => $billProductModel,
                'billProductProjectModel' => $billProductProjectModel ?? [],
                'billFirstPersonModel' => $billFirstPersonModel,
                'billSecondPersonModel' => $billSecondPersonModel,
                'filesModel' => $filesModel,
                'projectList' => $projectList,
                'agreementList' => $agreementList,
                'clientRoleList' => $clientRoleList,
                'valutaList' => $valutaList,
                'managerList' => $managerList,
                'productList' => $productList,
                'measureList' => $measureList,
                'languageList' => $languageList,
                'isAdmin' => $isAdmin,
                'hideDocType' => $hideDocType,
            ]);
        }
    }    
    
    /**
     * Displays a single model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        ButtonDeleteAsset::register(Yii::$app->getView());
        
        $model = $this->findModel($id, true);
        $agreementModel = $model->agreement;
        $agreementModel = (!empty($agreementModel) ? $agreementModel : new Agreement());
        $firstClientModel = ($model->doc_type != Bill::BILL_DOC_TYPE_CESSION) ? $agreementModel->firstClient : ($model->cession_direction == 'D' ? $agreementModel->firstClient : $agreementModel->thirdClient);
        $firstClientModel = (!empty($firstClientModel) ? $firstClientModel : new Client());
        $secondClientModel = ($model->doc_type != Bill::BILL_DOC_TYPE_CESSION) ? $agreementModel->secondClient : ($model->cession_direction == 'D' ? $agreementModel->thirdClient : $agreementModel->secondClient);
        $secondClientModel = (!empty($secondClientModel) ? $secondClientModel : new Client());
        $billProductModel = $model->billProducts;        
        $billProductModel = !empty($billProductModel) ? $billProductModel : [new BillProduct()];
        $billProductProjectModel = $model->billProductProjectsByAbonent;

        $filesModel = $model->attachmentFiles;
        $filesModel = (!empty($filesModel) ? $filesModel : [new Files()]);
        
        $paymentModel = new \common\models\bill\search\BillPaymentSearch();
        $params = Yii::$app->request->getQueryParams();
        unset($params['id']);
        $params['bill_id'] = $id;
        $paymentDataProvider = $paymentModel->search($params);
        
        $historyModel = new \common\models\bill\search\BillHistorySearch();
        $historyDataProvider = $historyModel->search($params);
        
        $isAdmin = FSMUser::getIsPortalAdmin();
        return $this->render('view', [
            'model' => $model,
            'agreementModel' => $agreementModel,
            'firstClientModel' => $firstClientModel,
            'secondClientModel' => $secondClientModel,
            'billProductModel' => $billProductModel,
            'billProductProjectModel' => $billProductProjectModel,
            'paymentModel' => $paymentModel,
            'historyModel' => $historyModel,
            'filesModel' => $filesModel,
            'paymentDataProvider' => $paymentDataProvider,
            'historyDataProvider' => $historyDataProvider,
            'isAdmin' => $isAdmin,
            'time' => date('H:i:s'),
        ]);
    }    
    
    public function actionViewPdf($id, $recursive = false) {
        $model = $this->findModel($id);
        $agreementModel = $model->agreement;
        $firstClient = ($model->doc_type != Bill::BILL_DOC_TYPE_CESSION) ? $agreementModel->firstClient : ($model->cession_direction == 'D' ? $agreementModel->firstClient : $agreementModel->thirdClient);

        $model->checkPDF();

        $filename = "invoice-{$model->printDocNumber}-{$model->doc_key}.pdf";
        $storagePath = Yii::getAlias(PrintModule::INVOICES_DIR.'/'.$firstClient->id);
        $filepath = "$storagePath/$filename";
        
        if (file_exists($filepath)) {
            return Yii::$app->response->sendFile($filepath, $filename, ['inline'=>true]);
        } else {
            // PDF doesn't exist so throw an error or something
            if(!$recursive){
                if(!$model->createPdf()){
                    Yii::$app->getSession()->setFlash('error', Yii::t('bill', 'Cannot to create PDF file')); 
                    return $this->redirect(FSMBaseModel::getBackUrl());
                }
                $this->actionViewPdf($id, true);
            }else{
                return $this->redirect(FSMBaseModel::getBackUrl());
            }            
        }
    }    
    
    public function actionViewActPdf($id) {
        try {
            $model = $this->findModel($id);
            $this->rememberBackUrl($model->backURL, $id);
            
            if(!$agreementModel = $model->agreement){
                Yii::error('$agreementModel empty', __METHOD__);
            }
            $firstClientModel = ($model->doc_type != Bill::BILL_DOC_TYPE_CESSION) ? 
                $agreementModel->firstClient : ($model->cession_direction == 'D' ? 
                    $agreementModel->firstClient : 
                    $agreementModel->thirdClient
                );
            if(!$firstClientModel){
                Yii::error('$firstClientModel empty', __METHOD__);
            }
            if(!$secondClientModel = $agreementModel->secondClient){
                Yii::error('$secondClientModel empty', __METHOD__);
            }
            if(!$firstClientRoleModel = $agreementModel->firstClientRole){
                Yii::error('$firstClientRoleModel empty', __METHOD__);
            }
            if(!$secondClientRoleModel = $agreementModel->secondClientRole){
                Yii::error('$secondClientRoleModel empty', __METHOD__);
            }
            $firstClientPersonModel = $agreementModel->firstClientPerson;
            if(count($firstClientPersonModel) < 1){
                Yii::error('$firstClientPersonModel empty', __METHOD__);
            }
            $secondClientPersonModel = $agreementModel->secondClientPerson;
            if(count($secondClientPersonModel) < 1){
                Yii::error('$secondClientPersonModel empty', __METHOD__);
            }
            
            if (!$agreementModel || 
                !$firstClientModel || 
                !$secondClientModel || 
                !$firstClientRoleModel || 
                !$secondClientRoleModel || 
                (count($firstClientPersonModel) < 1) || 
                (count($secondClientPersonModel) < 1)
            ) {
                throw new BadRequestHttpException('Not enough data to complete the request.');
            }

            $data = [
                'model' => $model,
                'agreementModel' => $agreementModel,
                'firstClientModel' => $firstClientModel,
                'secondClientModel' => $secondClientModel,
                'firstClientRoleModel' => $firstClientRoleModel,
                'secondClientRoleModel' => $secondClientRoleModel,
                'firstClientPersonModel' => $firstClientPersonModel,
                'secondClientPersonModel' => $secondClientPersonModel,
            ];

            $content = $this->renderPartial('bill-act', $data);
            
            $pdf = Yii::$app->pdf;
            $pdf->filename = 'BILL-ACT-'.$model->create_time.'.pdf';
            $pdf->content = $content;
            return $pdf->render();
            
        } catch (Exception $e) {
            $message = $e->getMessage();
            Yii::error($message, __METHOD__);
            Yii::$app->session->removeAllFlashes();
            Yii::$app->session->setFlash('error', $message);
            return $this->redirectToBackUrl($id);
        }
    }

    private function changeStatus($id, $status) {
        $model = $this->findModel($id, true);
        $model->changeStatus($status);
    }
    
    public function actionBillWriteOnBasis($id) {
        $model = $this->findModel($id, true);
        $abonentModel = AbonentBill::findOne([
            'abonent_id' => $model->userAbonentId, 
            'bill_id' => $id,
        ]);        
        $parentInvoice = $this->findModel($id);

        $agreementModel = $model->agreement;
        $agreementModel = (!empty($agreementModel) ? $agreementModel : new Agreement());
        $billProductModel = $model->billProducts;        
        $billProductModel = !empty($billProductModel) ? $billProductModel : [new BillProduct()];
        $billFirstPersonModel = $model->firstClientPerson;        
        $billFirstPersonModel = !empty($billFirstPersonModel) ? $billFirstPersonModel : [new FirstBillPerson(), new FirstBillPerson()];
        $billSecondPersonModel = $model->secondClientPerson;        
        $billSecondPersonModel = !empty($billSecondPersonModel) ? $billSecondPersonModel : [new SecondBillPerson(), new SecondBillPerson()];
        $billProductProjectModel = $model->billProductProjectsByAbonent;
        $billProductProjectModel = !empty($billProductProjectModel) ? $billProductProjectModel : [new BillProductProject()];
        
        $filesModel = $model->attachmentFiles;
        $filesModel = (!empty($filesModel) ? $filesModel : [new Files()]);
        
        $model->id = null;
        $model->setIsNewRecord(true);
        
        $modelArr = [
            'Bill' => $model,
            'AbonentBill' => $abonentModel,
        ];
        
        // ajax validation
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && $isAjax) {
            $ajaxModelArr = $modelArr;
            $billProductModel = FSMBaseModel::createMultiple(BillProduct::class);
            FSMBaseModel::loadMultiple($billProductModel, $requestPost);
            $billProductProjectModel = FSMBaseModel::createMultiple(BillProductProject::class);
            FSMBaseModel::loadMultiple($billProductProjectModel, $requestPost);
            $hasEmptyProject = false;
            foreach ($billProductModel as $index => $billProduct) {
                if(empty($billProduct->product_id) && empty($billProduct->amount) && empty($billProduct->price)){
                    unset($billProductModel[$index]);
                    if(isset($billProductProjectModel[$index])){
                        unset($billProductProjectModel[$index]);
                    }
                    continue;
                }

                if(empty($billProductProjectModel[$index]->project_id) &&
                    empty($requestPost['AbonentBill']['project_id'])
                ){
                    $billProductProjectModel[$index]->needValidateProject = $hasEmptyProject = true;
                }
            }
            if (!empty($billProductModel)) {
                $ajaxModelArr = ArrayHelper::merge($modelArr, ['BillProduct' => $billProductModel]);
            }
            if (!empty($billProductProjectModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['BillProductProject' => $billProductProjectModel]);
            }
            $ajaxModelArr['AbonentBill']->needValidateProject = $hasEmptyProject;

            $billPersonModel = FSMBaseModel::createMultiple(FirstBillPerson::class);
            FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
            foreach ($billPersonModel as $index => $billPerson) {
                if(empty($billPerson->client_person_id)){
                    unset($billPersonModel[$index]);
                    continue;
                }
            }
            if (!empty($billPersonModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['FirstBillPerson' => $billPersonModel]);
            }
            
            $billPersonModel = FSMBaseModel::createMultiple(SecondBillPerson::class);
            FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
            foreach ($billPersonModel as $index => $billPerson) {
                if(empty($billPerson->client_person_id)){
                    unset($billPersonModel[$index]);
                    continue;
                }
            }
            if (!empty($billPersonModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['SecondBillPerson' => $billPersonModel]);
            }
            
            return $this->performAjaxMultipleValidation($ajaxModelArr);            
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $flag = true;
                if(empty($model->agreement_id)){
                    throw new Exception('Agreement was not defined!');
                }
                $model->doc_key = null;
                if (!$model->save()) {
                    throw new Exception('Unable to save data! '.$model->errorMessage);
                }
                
                $parentInvoice->updateAttributes(['child_id' => $model->id]);
                $model->changeStatus(Bill::BILL_STATUS_PAID, ['paid_date' => $parentInvoice->paid_date]);

                $abonentModel->id = null;
                $abonentModel->setIsNewRecord(true);
                $abonentModel->bill_id = $model->id;                
                $abonentModel->save();

                $billProductModel = FSMBaseModel::createMultiple(BillProduct::class);
                FSMBaseModel::loadMultiple($billProductModel, $requestPost);            
                
                $billProductProjectModel = FSMBaseModel::createMultiple(BillProductProject::class);
                FSMBaseModel::loadMultiple($billProductProjectModel, $requestPost);
                foreach ($billProductModel as $index => $billProduct) {
                    if(empty($billProduct->product_id) && empty($billProduct->amount) && empty($billProduct->price)){
                        unset($billProductModel[$index]);
                        if(isset($billProductProjectModel[$index])){
                            unset($billProductProjectModel[$index]);
                        }
                        continue;
                    }
                    $billProduct->id = null;
                    $billProduct->price_eur = $billProduct->price / $model->rate;
                    $billProduct->summa_eur = $billProduct->summa / $model->rate;
                    $billProduct->summa_vat_eur = $billProduct->summa_vat / $model->rate;
                    $billProduct->total_eur = $billProduct->total / $model->rate;
                    $billProduct->setIsNewRecord(true);
                    $billProduct->bill_id = $model->id;
                    if($model->according_contract){
                        $billProduct->product_id = null;
                    }
                }
                if(!empty($billProductModel)){
                    if ($flag && ($flag = FSMBaseModel::validateMultiple($billProductModel))) {
                        foreach ($billProductModel as $index => $billProduct) {
                            if (!$billProduct->save(false)) {
                                throw new Exception('Unable to save data! '.$billProduct->errorMessage);
                            }
                                
                            $billProductProjectModel[$index]->bill_id = $model->id;
                            $billProductProjectModel[$index]->bill_product_id = $billProduct->id;
                            if(empty($billProductProjectModel[$index]->project_id)){
                                $billProductProjectModel[$index]->project_id = $abonentModel->project_id;
                            }
                            if (!$billProductProjectModel[$index]->save(false)) {
                                throw new Exception('Unable to save data! '.$billProductProjectModel[$index]->errorMessage);
                            }
                        }
                    }
                }  

                $billPersonModel = FSMBaseModel::createMultiple(FirstBillPerson::class);
                FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
                foreach ($billPersonModel as $index => $billPerson) {
                    if(empty($billPerson->client_person_id)){
                        unset($billPersonModel[$index]);
                        continue;
                    }
                    $billPerson->id = null;
                    $billPerson->setIsNewRecord(true);                    
                    $billPerson->bill_id = $model->id;                      
                    $billPerson->person_order = 1;                      
                }
                if(!empty($billPersonModel)){
                    if ($flag && ($flag = FSMBaseModel::validateMultiple($billPersonModel))) {
                        foreach ($billPersonModel as $billPerson) {
                            if (!$billPerson->save(false)) {
                                throw new Exception('Unable to save data! '.$billPerson->errorMessage);
                            }
                        }
                    }
                }

                $billPersonModel = FSMBaseModel::createMultiple(SecondBillPerson::class);
                FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
                foreach ($billPersonModel as $index => $billPerson) {
                    if(empty($billPerson->client_person_id)){
                        unset($billPersonModel[$index]);
                        continue;
                    }
                    $billPerson->id = null;
                    $billPerson->setIsNewRecord(true);                    
                    $billPerson->bill_id = $model->id;                      
                    $billPerson->person_order = 2;                      
                }
                if(!empty($billPersonModel)){
                    if ($flag && ($flag = FSMBaseModel::validateMultiple($billPersonModel))) {
                        foreach ($billPersonModel as $billPerson) {
                            if (!$billPerson->save(false)) {
                                throw new Exception('Unable to save data! '.$billPerson->errorMessage);
                            }
                        }
                    }
                }

                if(!empty($requestPost['Bill']['reserved_id'])){
                    ReservedId::deleteAll(['id' => $requestPost['Bill']['reserved_id']]);
                }
                
                if(!$this->updateAttacment($model, $filesModel)){
                    throw new Exception('Unable to save attachment! '.$filesModel->errorMessage);
                }
                
                if (!$flag) {
                    throw new Exception('Unable to save data!');
                }else{
                    $transaction->commit();
                }     
            } catch (Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirect('index');              
            }                
        } else {
            BillUIAsset::register(Yii::$app->getView());
            
            $model->reserved_id = Yii::$app->security->generateRandomString();
            $firstClient = $agreementModel->firstClient;
            if(!empty($firstClient->gen_number)){
                $lastNumber = $firstClient->getLastNumber($model->reserved_id);
                $model->doc_number = $firstClient->gen_number.$lastNumber;
            }else{
                $model->doc_number = $firstClient->id.'/'.date('Y-m-d');
            }        

            //$model->parent_id = $id;
            $model->status = Bill::BILL_STATUS_PREPAR;
            $model->pay_status = Bill::BILL_PAY_STATUS_NOT;
            $model->mail_status = null;
            $model->transfer_status = null;
            $model->doc_type = Bill::BILL_DOC_TYPE_BILL;
            $model->doc_date = date('d-m-Y');
            $model->services_period_from = !empty($model->services_period_from) ? date('d-m-Y', strtotime($model->services_period_from)) : null;
            $model->services_period_till = !empty($model->services_period_till) ? date('d-m-Y', strtotime($model->services_period_till)) : null;
            $model->pay_date = date('d-m-Y', strtotime("+".Bill::BILL_DEFAULT_PAYMENT_DAYS." days", strtotime($model->doc_date)));
            //$model->summa = '0.00';
            //$model->vat = '0.00';
            //$model->total = '0.00';
            $model->delayed = 0;
            $model->blocked = 0;
            $model->doc_key = '';

            $isAdmin = FSMUser::getIsPortalAdmin();
            $agreementList = Agreement::getNameArr(['agreement.deleted' => false]);
            $projectList = Project::getNameArr(['project.deleted' => 0]);
            $clientList = Client::getNameArr(['client.deleted' => 0]);
            $clientRoleList = ClientRole::getNameArr();
            $productList = Product::getNameArr();
            $measureList = Measure::getNameArr();
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            $languageList = Language::getEnabledLanguageList();
            $managerList = FSMProfile::getProfileListByRole([
                FSMUser::USER_ROLE_ADMIN,
                FSMUser::USER_ROLE_OPERATOR, 
                FSMUser::USER_ROLE_BOOKER,
                FSMUser::USER_ROLE_COORDINATOR,
            ]);
            if(!Yii::$app->user->isGuest){
                $profile = Yii::$app->user->identity->profile;
                if(array_key_exists($profile->id, $managerList)){
                    $model->manager_id = $profile->id;
                }                
            }

            if(count($billFirstPersonModel) == 1){
                $billFirstPersonModel[1] = new FirstBillPerson();
            }
            if(count($billSecondPersonModel) == 1){
                $billSecondPersonModel[1] = new SecondBillPerson();
            }
            
            $availableDocTypeList = Bill::getBillDocTypeListByIds([
                Bill::BILL_DOC_TYPE_BILL, 
                Bill::BILL_DOC_TYPE_INVOICE, 
            ]);
            
            return $this->render('create', [
                'model' => $model,
                'abonentModel' => $abonentModel,
                'agreementModel' => $agreementModel,
                'firstClientModel' => $agreementModel->firstClient,
                'secondClientModel' => $agreementModel->secondClient,
                'filesModel' => $filesModel,
                'projectList' => $projectList,
                'agreementList' => $agreementList,
                'clientList' => $clientList,
                'clientRoleList' => $clientRoleList,
                'valutaList' => $valutaList,
                'managerList' => $managerList,
                'billProductModel' => $billProductModel,
                'billProductProjectModel' => $billProductProjectModel ?? [],
                'billFirstPersonModel' => $billFirstPersonModel,
                'billSecondPersonModel' => $billSecondPersonModel,                
                'productList' => $productList,
                'measureList' => $measureList,
                'languageList' => $languageList,
                'isAdmin' => $isAdmin,
                'availableDocTypeList' => $availableDocTypeList,
            ]);
        }
    }    
    
    public function actionBillWriteOnBasisMany($ids) {
        $ids = explode(',', $ids);
        $model = $this->findModel($ids[0], true);
        $abonentModel = AbonentBill::findOne([
            'abonent_id' => $model->userAbonentId, 
            'bill_id' => $ids[0],
        ]);        
        $parentInvoice = $this->findModel($ids[0]);

        $agreementModel = $model->agreement;
        $agreementModel = (!empty($agreementModel) ? $agreementModel : new Agreement());
        $billProductModel = $model->billProducts;        
        $billProductModel = !empty($billProductModel) ? $billProductModel : [new BillProduct()];
        $billFirstPersonModel = $model->firstClientPerson;        
        $billFirstPersonModel = !empty($billFirstPersonModel) ? $billFirstPersonModel : [new FirstBillPerson(), new FirstBillPerson()];
        $billSecondPersonModel = $model->secondClientPerson;        
        $billSecondPersonModel = !empty($billSecondPersonModel) ? $billSecondPersonModel : [new SecondBillPerson(), new SecondBillPerson()];
        
        $billProductProjectModel = $model->billProductProjectsByAbonent;
        $billProductProjectModel = !empty($billProductProjectModel) ? $billProductProjectModel : [new BillProductProject()];
        
        $filesModel = $model->attachmentFiles;
        $filesModel = (!empty($filesModel) ? $filesModel : [new Files()]);
        
        $model->id = null;
        $model->setIsNewRecord(true);
        
        $modelArr = [
            'Bill' => $model,
            'AbonentBill' => $abonentModel,
        ];
        
        // ajax validation
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && $isAjax) {
            $ajaxModelArr = $modelArr;
            $billProductModel = FSMBaseModel::createMultiple(BillProduct::class);
            FSMBaseModel::loadMultiple($billProductModel, $requestPost);

            $billProductProjectModel = FSMBaseModel::createMultiple(BillProductProject::class);
            FSMBaseModel::loadMultiple($billProductProjectModel, $requestPost);
            $hasEmptyProject = false;
            foreach ($billProductModel as $index => $billProduct) {
                if(empty($billProduct->product_id) && empty($billProduct->amount) && empty($billProduct->price)){
                    unset($billProductModel[$index]);
                    if(isset($billProductProjectModel[$index])){
                        unset($billProductProjectModel[$index]);
                    }
                    continue;
                }

                if(empty($billProductProjectModel[$index]->project_id) &&
                    empty($requestPost['AbonentBill']['project_id'])
                ){
                    $billProductProjectModel[$index]->needValidateProject = $hasEmptyProject = true;
                }
            }
            if (!empty($billProductModel)) {
                $ajaxModelArr = ArrayHelper::merge($modelArr, ['BillProduct' => $billProductModel]);
            }
            if (!empty($billProductProjectModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['BillProductProject' => $billProductProjectModel]);
            }
            $ajaxModelArr['AbonentBill']->needValidateProject = $hasEmptyProject;

            $billPersonModel = FSMBaseModel::createMultiple(FirstBillPerson::class);
            FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
            foreach ($billPersonModel as $index => $billPerson) {
                if(empty($billPerson->client_person_id)){
                    unset($billPersonModel[$index]);
                    continue;
                }
            }
            if (!empty($billPersonModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['FirstBillPerson' => $billPersonModel]);
            }
            
            $billPersonModel = FSMBaseModel::createMultiple(SecondBillPerson::class);
            FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
            foreach ($billPersonModel as $index => $billPerson) {
                if(empty($billPerson->client_person_id)){
                    unset($billPersonModel[$index]);
                    continue;
                }
            }
            if (!empty($billPersonModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['SecondBillPerson' => $billPersonModel]);
            }
            
            return $this->performAjaxMultipleValidation($ajaxModelArr);            
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $flag = true;
                if(empty($model->agreement_id)){
                    throw new Exception('Agreement was not defined!');
                }
                $model->doc_key = null;
                $paymentsSumma = 0;
                if (!$model->save()) {
                    throw new Exception('Unable to save data! '.$model->errorMessage);
                }
                
                $abonentModel->id = null;
                $abonentModel->setIsNewRecord(true);
                $abonentModel->bill_id = $model->id;                
                $abonentModel->save();

                $paidDate = '';
                for($i = 0; $i < count($ids); $i++){
                    $parentInvoice = $this->findModel($ids[$i]);
                    $parentInvoice->updateAttributes(['child_id' => $model->id]);
                    if($paidDate < $parentInvoice->paid_date){
                        $paidDate = $parentInvoice->paid_date;
                    }
                }
                $model->changeStatus(Bill::BILL_STATUS_PAID, ['paid_date' => $paidDate]); 

                $billProductModel = FSMBaseModel::createMultiple(BillProduct::class);
                FSMBaseModel::loadMultiple($billProductModel, $requestPost);            
                
                $billProductProjectModel = FSMBaseModel::createMultiple(BillProductProject::class);
                FSMBaseModel::loadMultiple($billProductProjectModel, $requestPost);
                foreach ($billProductModel as $index => $billProduct) {
                    if(empty($billProduct->product_id) && empty($billProduct->amount) && empty($billProduct->price)){
                        unset($billProductModel[$index]);
                        if(isset($billProductProjectModel[$index])){
                            unset($billProductProjectModel[$index]);
                        }
                        continue;
                    }
                    $billProduct->id = null;
                    $billProduct->price_eur = $billProduct->price / $model->rate;
                    $billProduct->summa_eur = $billProduct->summa / $model->rate;
                    $billProduct->summa_vat_eur = $billProduct->summa_vat / $model->rate;
                    $billProduct->total_eur = $billProduct->total / $model->rate;
                    $billProduct->setIsNewRecord(true);
                    $billProduct->bill_id = $model->id;
                    if($model->according_contract){
                        $billProduct->product_id = null;
                    }
                }
                if(!empty($billProductModel)){
                    if ($flag && ($flag = FSMBaseModel::validateMultiple($billProductModel))) {
                        foreach ($billProductModel as $index => $billProduct) {
                            if (!$billProduct->save(false)) {
                                throw new Exception('Unable to save data! '.$billProduct->errorMessage);
                            }
                                
                            $billProductProjectModel[$index]->bill_id = $model->id;
                            $billProductProjectModel[$index]->bill_product_id = $billProduct->id;
                            if(empty($billProductProjectModel[$index]->project_id)){
                                $billProductProjectModel[$index]->project_id = $abonentModel->project_id;
                            }
                            if (!$billProductProjectModel[$index]->save(false)) {
                                throw new Exception('Unable to save data! '.$billProductProjectModel[$index]->errorMessage);
                            }
                        }
                    }
                }  

                $billPersonModel = FSMBaseModel::createMultiple(FirstBillPerson::class);
                FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
                foreach ($billPersonModel as $index => $billPerson) {
                    if(empty($billPerson->client_person_id)){
                        unset($billPersonModel[$index]);
                        continue;
                    }
                    $billPerson->id = null;
                    $billPerson->setIsNewRecord(true);
                    $billPerson->bill_id = $model->id;                      
                    $billPerson->person_order = 1;                      
                }
                if(!empty($billPersonModel)){
                    if ($flag && ($flag = FSMBaseModel::validateMultiple($billPersonModel))) {
                        foreach ($billPersonModel as $billPerson) {
                            if (($flag = $billPerson->save(false)) === false) {
                                throw new Exception('Unable to save data! '.$billPerson->errorMessage);
                            }
                        }
                    }
                }

                $billPersonModel = FSMBaseModel::createMultiple(SecondBillPerson::class);
                FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
                foreach ($billPersonModel as $index => $billPerson) {
                    if(empty($billPerson->client_person_id)){
                        unset($billPersonModel[$index]);
                        continue;
                    }
                    $billPerson->id = null;
                    $billPerson->setIsNewRecord(true);
                    $billPerson->bill_id = $model->id;                      
                    $billPerson->person_order = 2;                      
                }
                if(!empty($billPersonModel)){
                    if ($flag && ($flag = FSMBaseModel::validateMultiple($billPersonModel))) {
                        foreach ($billPersonModel as $billPerson) {
                            if (!$billPerson->save(false)) {
                                throw new Exception('Unable to save data! '.$billPerson->errorMessage);
                            }
                        }
                    }
                }

                if(!empty($requestPost['Bill']['reserved_id'])){
                    ReservedId::deleteAll(['id' => $requestPost['Bill']['reserved_id']]);
                }
                
                if(!$this->updateAttacment($model, $filesModel)){
                    throw new Exception('Unable to save attachment! '.$filesModel->errorMessage);
                }
                
                if (!$flag) {
                    throw new Exception('Unable to save data!');
                }else{
                    $transaction->commit();
                }     
            } catch (Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirect('index');              
            }                
        } else {
            BillUIAsset::register(Yii::$app->getView());
            
            $model->reserved_id = Yii::$app->security->generateRandomString();
            $firstClient = $agreementModel->firstClient;
            if(!empty($firstClient->gen_number)){
                $lastNumber = $firstClient->getLastNumber($model->reserved_id);
                $model->doc_number = $firstClient->gen_number.$lastNumber;
            }else{
                $model->doc_number = $firstClient->id.'/'.date('Y-m-d');
            }        

            //$model->parent_id = $id;
            $model->status = Bill::BILL_STATUS_PREPAR;
            $model->pay_status = Bill::BILL_PAY_STATUS_NOT;
            $model->mail_status = null;
            $model->transfer_status = null;
            $model->doc_type = Bill::BILL_DOC_TYPE_BILL;
            $model->doc_date = date('d-m-Y');
            $model->services_period_from = !empty($model->services_period_from) ? date('d-m-Y', strtotime($model->services_period_from)) : null;
            $model->services_period_till = !empty($model->services_period_till) ? date('d-m-Y', strtotime($model->services_period_till)) : null;
            $model->pay_date = date('d-m-Y', strtotime("+".Bill::BILL_DEFAULT_PAYMENT_DAYS." days", strtotime($model->doc_date)));
            //$model->summa = '0.00';
            //$model->vat = '0.00';
            //$model->total = '0.00';
            $model->delayed = 0;
            $model->blocked = 0;
            $model->doc_key = '';
            
            for($i = 1; $i < count($ids); $i++){
                $nextModel = $this->findModel($ids[$i]);
                $model->summa += $nextModel->summa;
                $model->vat += $nextModel->vat;
                $model->total += $nextModel->total;
                $nextBillProductModel = $nextModel->billProducts;
                $billProductModel = ArrayHelper::merge($billProductModel, $nextBillProductModel);
            }

            $isAdmin = FSMUser::getIsPortalAdmin();
            $agreementList = Agreement::getNameArr(['agreement.deleted' => false]);
            $projectList = Project::getNameArr(['project.deleted' => 0]);
            $clientList = Client::getNameArr(['client.deleted' => 0]);
            $clientRoleList = ClientRole::getNameArr();
            $productList = Product::getNameArr();
            $measureList = Measure::getNameArr();
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            $languageList = Language::getEnabledLanguageList();
            $managerList = FSMProfile::getProfileListByRole([
                FSMUser::USER_ROLE_ADMIN,
                FSMUser::USER_ROLE_OPERATOR, 
                FSMUser::USER_ROLE_BOOKER,
                FSMUser::USER_ROLE_COORDINATOR,
            ]);
            if(!Yii::$app->user->isGuest){
                $profile = Yii::$app->user->identity->profile;
                if(array_key_exists($profile->id, $managerList)){
                    $model->manager_id = $profile->id;
                }                
            }

            if(count($billFirstPersonModel) == 1){
                $billFirstPersonModel[1] = new FirstBillPerson();
            }
            if(count($billSecondPersonModel) == 1){
                $billSecondPersonModel[1] = new SecondBillPerson();
            }
            
            return $this->render('create', [
                'model' => $model,
                'abonentModel' => $abonentModel,
                'agreementModel' => $agreementModel,
                'firstClientModel' => $agreementModel->firstClient,
                'secondClientModel' => $agreementModel->secondClient,
                'filesModel' => $filesModel,
                'projectList' => $projectList,
                'agreementList' => $agreementList,
                'clientList' => $clientList,
                'clientRoleList' => $clientRoleList,
                'valutaList' => $valutaList,
                'managerList' => $managerList,
                'billProductModel' => $billProductModel,
                'billProductProjectModel' => $billProductProjectModel ?? [],
                'billFirstPersonModel' => $billFirstPersonModel,
                'billSecondPersonModel' => $billSecondPersonModel,                
                'productList' => $productList,
                'measureList' => $measureList,
                'languageList' => $languageList,
                'isAdmin' => $isAdmin,
                'hideDocType' => true,
            ]);
        }
    }    
    
    public function actionBillCreditInvoice($id) {
        $model = $this->findModel($id, true);
        $abonentModel = AbonentBill::findOne([
            'abonent_id' => $model->userAbonentId, 
            'bill_id' => $id,
        ]);        
        $parentInvoice = $this->findModel($id);
        
        $agreementModel = $model->agreement;
        $agreementModel = (!empty($agreementModel) ? $agreementModel : new Agreement());
        $billProductModel = $model->billProducts;        
        $billProductModel = !empty($billProductModel) ? $billProductModel : [new BillProduct()];
        $billFirstPersonModel = $model->firstClientPerson;        
        $billFirstPersonModel = !empty($billFirstPersonModel) ? $billFirstPersonModel : [new FirstBillPerson(), new FirstBillPerson()];
        $billSecondPersonModel = $model->secondClientPerson;        
        $billSecondPersonModel = !empty($billSecondPersonModel) ? $billSecondPersonModel : [new SecondBillPerson(), new SecondBillPerson()];
        
        $billProductProjectModel = $model->billProductProjectsByAbonent;
        $billProductProjectModel = !empty($billProductProjectModel) ? $billProductProjectModel : [new BillProductProject()];
        
        $filesModel = $model->attachmentFiles;
        $filesModel = (!empty($filesModel) ? $filesModel : [new Files()]);
        
        $model->id = null;
        $model->setIsNewRecord(true);
        
        $modelArr = [
            'Bill' => $model,
            'AbonentBill' => $abonentModel,
        ];
        
        // ajax validation
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && $isAjax) {
            $ajaxModelArr = $modelArr;
            $billProductModel = FSMBaseModel::createMultiple(BillProduct::class);
            FSMBaseModel::loadMultiple($billProductModel, $requestPost);

            $billProductProjectModel = FSMBaseModel::createMultiple(BillProductProject::class);
            FSMBaseModel::loadMultiple($billProductProjectModel, $requestPost);
            $hasEmptyProject = false;
            foreach ($billProductModel as $index => $billProduct) {
                if(empty($billProduct->product_id) && empty($billProduct->amount) && empty($billProduct->price)){
                    unset($billProductModel[$index]);
                    if(isset($billProductProjectModel[$index])){
                        unset($billProductProjectModel[$index]);
                    }
                    continue;
                }

                if(empty($billProductProjectModel[$index]->project_id) &&
                    empty($requestPost['AbonentBill']['project_id'])
                ){
                    $billProductProjectModel[$index]->needValidateProject = $hasEmptyProject = true;
                }
            }
            if (!empty($billProductModel)) {
                $ajaxModelArr = ArrayHelper::merge($modelArr, ['BillProduct' => $billProductModel]);
            }
            if (!empty($billProductProjectModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['BillProductProject' => $billProductProjectModel]);
            }
            $ajaxModelArr['AbonentBill']->needValidateProject = $hasEmptyProject;

            $billPersonModel = FSMBaseModel::createMultiple(FirstBillPerson::class);
            FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
            foreach ($billPersonModel as $index => $billPerson) {
                if(empty($billPerson->client_person_id)){
                    unset($billPersonModel[$index]);
                    continue;
                }
            }
            if (!empty($billPersonModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['FirstBillPerson' => $billPersonModel]);
            }
            
            $billPersonModel = FSMBaseModel::createMultiple(SecondBillPerson::class);
            FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
            foreach ($billPersonModel as $index => $billPerson) {
                if(empty($billPerson->client_person_id)){
                    unset($billPersonModel[$index]);
                    continue;
                }
            }
            if (!empty($billPersonModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['SecondBillPerson' => $billPersonModel]);
            }
            
            return $this->performAjaxMultipleValidation($ajaxModelArr);            
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $flag = true;
                if(empty($model->agreement_id)){
                    throw new Exception('Agreement was not defined!');
                }
                $model->parent_id = $id;
                $model->complete_date = date('Y-m-d');
                $model->doc_key = null;
                $model->mail_status = null;
                $model->transfer_status = null;
                if (!$model->save()) {
                    throw new Exception('Unable to save data! '.$model->errorMessage);
                }
                $abonentModel->id = null;
                $abonentModel->setIsNewRecord(true);
                $abonentModel->bill_id = $model->id;                
                $abonentModel->save();
                
                $historyModule = new BillHistory();
                $historyModule->saveHistory($model->id, BillHistory::BillHistory_ACTIONS['ACTION_STATUS_UP_COMPLETE']);
                unset($historyModule);

                $debt = $parentInvoice->paymentsSummaLess;
                $arrToUpdate = [
                    'pay_status' => (
                        ($debt == 0) ? 
                            Bill::BILL_PAY_STATUS_FULL : 
                            (($debt > 0) ? 
                                Bill::BILL_PAY_STATUS_PART : 
                                Bill::BILL_PAY_STATUS_OVER)
                            ),
                    'pay_date' => date('Y-m-d'),
                ];
                if($arrToUpdate['pay_status'] == Bill::BILL_PAY_STATUS_FULL){
                    $arrToUpdate['status'] = Bill::BILL_STATUS_COMPLETE;
                    $arrToUpdate['complete_date'] = date('Y-m-d');

                    $historyModule = new BillHistory();
                    $historyModule->saveHistory($parentInvoice->id, BillHistory::BillHistory_ACTIONS['ACTION_STATUS_UP_COMPLETE']);
                    unset($historyModule);                        
                }
                $parentInvoice->updateAttributes($arrToUpdate);

                $billProductModel = FSMBaseModel::createMultiple(BillProduct::class);
                FSMBaseModel::loadMultiple($billProductModel, $requestPost);            
                
                $billProductProjectModel = FSMBaseModel::createMultiple(BillProductProject::class);
                FSMBaseModel::loadMultiple($billProductProjectModel, $requestPost);
                foreach ($billProductModel as $index => $billProduct) {
                    if(empty($billProduct->product_id) && empty($billProduct->amount) && empty($billProduct->price)){
                        unset($billProductModel[$index]);
                        if(isset($billProductProjectModel[$index])){
                            unset($billProductProjectModel[$index]);
                        }
                        continue;
                    }
                    $billProduct->id = null;
                    $billProduct->price_eur = $billProduct->price / $model->rate;
                    $billProduct->summa_eur = $billProduct->summa / $model->rate;
                    $billProduct->summa_vat_eur = $billProduct->summa_vat / $model->rate;
                    $billProduct->total_eur = $billProduct->total / $model->rate;
                    $billProduct->setIsNewRecord(true);
                    $billProduct->bill_id = $model->id;
                    if($model->according_contract){
                        $billProduct->product_id = null;
                    }
                }
                if(!empty($billProductModel)){
                    if ($flag = FSMBaseModel::validateMultiple($billProductModel)) {
                        foreach ($billProductModel as $index => $billProduct) {
                            if (!$billProduct->save(false)) {
                                throw new Exception('Unable to save data! '.$billProduct->errorMessage);
                            }
                                
                            $billProductProjectModel[$index]->bill_id = $model->id;
                            $billProductProjectModel[$index]->bill_product_id = $billProduct->id;
                            if(empty($billProductProjectModel[$index]->project_id)){
                                $billProductProjectModel[$index]->project_id = $abonentModel->project_id;
                            }
                            if (!$billProductProjectModel[$index]->save(false)) {
                                throw new Exception('Unable to save data! '.$billProductProjectModel[$index]->errorMessage);
                            }
                        }
                    }
                }  

                $billPersonModel = FSMBaseModel::createMultiple(FirstBillPerson::class);
                FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
                foreach ($billPersonModel as $index => $billPerson) {
                    if(empty($billPerson->client_person_id)){
                        unset($billPersonModel[$index]);
                        continue;
                    }
                    $billPerson->id = null;
                    $billPerson->setIsNewRecord(true);                    
                    $billPerson->bill_id = $model->id;                      
                    $billPerson->person_order = 1;                      
                }
                if(!empty($billPersonModel)){
                    if ($flag = FSMBaseModel::validateMultiple($billPersonModel)) {
                        foreach ($billPersonModel as $billPerson) {
                            if (!$billPerson->save(false)) {
                                throw new Exception('Unable to save data! '.$billPerson->errorMessage);
                            }
                        }
                    }
                }

                $billPersonModel = FSMBaseModel::createMultiple(SecondBillPerson::class);
                FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
                foreach ($billPersonModel as $index => $billPerson) {
                    if(empty($billPerson->client_person_id)){
                        unset($billPersonModel[$index]);
                        continue;
                    }
                    $billPerson->id = null;
                    $billPerson->setIsNewRecord(true);                    
                    $billPerson->bill_id = $model->id;                      
                    $billPerson->person_order = 2;                      
                }
                if(!empty($billPersonModel)){
                    if ($flag = FSMBaseModel::validateMultiple($billPersonModel)) {
                        foreach ($billPersonModel as $billPerson) {
                            if (!$billPerson->save(false)) {
                                throw new Exception('Unable to save data! '.$billPerson->errorMessage);
                            }
                        }
                    }
                }

                if(!empty($requestPost['Bill']['reserved_id'])){
                    ReservedId::deleteAll(['id' => $requestPost['Bill']['reserved_id']]);
                }
                
                if(!$this->updateAttacment($model, $filesModel)){
                    throw new Exception('Unable to save attachment! '.$filesModel->errorMessage);
                }
                
                if (!$flag) {
                    throw new Exception('Unable to save data!');
                }else{
                    $transaction->commit();
                }                
            } catch (Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirect('index');              
            }                
        } else {
            BillUIAsset::register(Yii::$app->getView());
            
            $model->reserved_id = Yii::$app->security->generateRandomString();
            $firstClient = $agreementModel->firstClient;
            if(!empty($firstClient->gen_number)){
                $lastNumber = $firstClient->getLastNumber($model->reserved_id);
                $model->doc_number = $firstClient->gen_number.$lastNumber;
            }else{
                $model->doc_number = $firstClient->id.'/'.date('Y-m-d');
            }        

            $model->status = Bill::BILL_STATUS_COMPLETE;
            $model->pay_status = Bill::BILL_PAY_STATUS_FULL;
            $model->doc_type = Bill::BILL_DOC_TYPE_CRBILL;
            $model->doc_date = date('d-m-Y');
            $model->services_period_from = !empty($model->services_period_from) ? date('d-m-Y', strtotime($model->services_period_from)) : null;
            $model->services_period_till = !empty($model->services_period_till) ? date('d-m-Y', strtotime($model->services_period_till)) : null;
            $model->pay_date = date('d-m-Y');
            //$model->summa = $model->summa * -1;
            //$model->vat = $model->vat * -1;
            //$model->total = $model->total * -1;
            $model->delayed = 0;
            $model->doc_key = '';

            $isAdmin = FSMUser::getIsPortalAdmin();
            $agreementList = Agreement::getNameArr(['agreement.deleted' => false]);
            $projectList = Project::getNameArr(['project.deleted' => 0]);
            $clientList = Client::getNameArr(['client.deleted' => 0]);
            $clientRoleList = ClientRole::getNameArr();
            $productList = Product::getNameArr();
            $measureList = Measure::getNameArr();
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            $languageList = Language::getEnabledLanguageList();
            $managerList = FSMProfile::getProfileListByRole([
                FSMUser::USER_ROLE_ADMIN,
                FSMUser::USER_ROLE_OPERATOR, 
                FSMUser::USER_ROLE_BOOKER,
                FSMUser::USER_ROLE_COORDINATOR,
            ]);
            if(!Yii::$app->user->isGuest){
                $profile = Yii::$app->user->identity->profile;
                if(array_key_exists($profile->id, $managerList)){
                    $model->manager_id = $profile->id;
                }                
            }

            if(count($billFirstPersonModel) == 1){
                $billFirstPersonModel[1] = new FirstBillPerson();
            }
            if(count($billSecondPersonModel) == 1){
                $billSecondPersonModel[1] = new SecondBillPerson();
            }
            
            return $this->render('create', [
                'model' => $model,
                'abonentModel' => $abonentModel,
                'agreementModel' => $agreementModel,
                'firstClientModel' => $agreementModel->firstClient,
                'secondClientModel' => $agreementModel->secondClient,
                'filesModel' => $filesModel,
                'projectList' => $projectList,
                'agreementList' => $agreementList,
                'clientList' => $clientList,
                'clientRoleList' => $clientRoleList,
                'valutaList' => $valutaList,
                'managerList' => $managerList,
                'billProductModel' => $billProductModel,
                'billProductProjectModel' => $billProductProjectModel ?? [],
                'billFirstPersonModel' => $billFirstPersonModel,
                'billSecondPersonModel' => $billSecondPersonModel,                
                'productList' => $productList,
                'measureList' => $measureList,
                'languageList' => $languageList,
                'isAdmin' => $isAdmin,
            ]);
        }
    }    
    
    public function actionBillCession($id) {
        $model = $this->findModel($id, true);
        $abonentModel = AbonentBill::findOne([
            'abonent_id' => $model->userAbonentId, 
            'bill_id' => $id,
        ]);        
        $parentInvoice = $this->findModel($id);
        
        $agreementModel = $model->agreement;
        $agreementModel = (!empty($agreementModel) ? $agreementModel : new Agreement());
        
        $filesModel = $model->attachmentFiles;
        $filesModel = (!empty($filesModel) ? $filesModel : [new Files()]);
        
        $debt = $parentInvoice->billNotPaidSummaa;
        
        $model->id = null;
        $model->setIsNewRecord(true);
        
        $modelArr = [
            'Bill' => $model,
            'AbonentBill' => $abonentModel,
        ];
        
        // ajax validation
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && $isAjax) {
            $ajaxModelArr = $modelArr;
            $billProductModel = FSMBaseModel::createMultiple(BillProduct::class);
            FSMBaseModel::loadMultiple($billProductModel, $requestPost);
            foreach ($billProductModel as $index => $billProduct) {
                if(empty($billProduct->product_id) && empty($billProduct->amount) && empty($billProduct->price)){
                    unset($billProductModel[$index]);
                    continue;
                }
            }
            if (!empty($billProductModel)) {
                $ajaxModelArr = ArrayHelper::merge($modelArr, ['BillProduct' => $billProductModel]);
            }

            $billPersonModel = FSMBaseModel::createMultiple(FirstBillPerson::class);
            FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
            foreach ($billPersonModel as $index => $billPerson) {
                if(empty($billPerson->client_person_id)){
                    unset($billPersonModel[$index]);
                    continue;
                }
            }
            if (!empty($billPersonModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['FirstBillPerson' => $billPersonModel]);
            }
            
            $billPersonModel = FSMBaseModel::createMultiple(SecondBillPerson::class);
            FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
            foreach ($billPersonModel as $index => $billPerson) {
                if(empty($billPerson->client_person_id)){
                    unset($billPersonModel[$index]);
                    continue;
                }
            }
            if (!empty($billPersonModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['SecondBillPerson' => $billPersonModel]);
            }
                        
            return $this->performAjaxMultipleValidation($ajaxModelArr);            
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $flag = true;
                if(empty($model->agreement_id)){
                    throw new Exception('Agreement was not defined!');
                }
                $model->parent_id = $id;
                $model->summa = $model->summa * -1;
                $model->vat = $model->vat * -1;
                $model->total = $model->total * -1;
                $model->complete_date = date('Y-m-d');
                $model->doc_key = null;
                $model->mail_status = null;
                $model->transfer_status = null;
                if (!$model->save()) {
                    throw new Exception('Unable to save data! '.$model->errorMessage);
                }else{
                    $abonentModel->id = null;
                    $abonentModel->setIsNewRecord(true);
                    $abonentModel->bill_id = $model->id;                
                    $abonentModel->save();

                    $historyModule = new BillHistory();
                    $historyModule->saveHistory($model->id, BillHistory::BillHistory_ACTIONS['ACTION_STATUS_UP_COMPLETE']);
                    unset($historyModule);                        

                    $parentInvoice->updateAttributes([
                        'status' => Bill::BILL_STATUS_COMPLETE, 
                        'pay_status' => Bill::BILL_PAY_STATUS_FULL, 
                        'pay_date' => date('Y-m-d'),
                    ]);
                    $historyModule = new BillHistory();
                    $historyModule->saveHistory($parentInvoice->id, BillHistory::BillHistory_ACTIONS['ACTION_STATUS_UP_COMPLETE']);
                    unset($historyModule);                        
                }

                $billProductModel = FSMBaseModel::createMultiple(BillProduct::class);
                FSMBaseModel::loadMultiple($billProductModel, $requestPost);            
                foreach ($billProductModel as $index => $billProduct) {
                    if(empty($billProduct->product_id) && empty($billProduct->amount) && empty($billProduct->price)){
                        unset($billProductModel[$index]);
                        continue;
                    }
                    $billProduct->id = null;
                    $billProduct->price_eur = $billProduct->price / $model->rate;
                    $billProduct->summa_eur = $billProduct->summa / $model->rate;
                    $billProduct->summa_vat_eur = $billProduct->summa_vat / $model->rate;
                    $billProduct->total_eur = $billProduct->total / $model->rate;
                    $billProduct->setIsNewRecord(true);
                    $billProduct->bill_id = $model->id;
                    if($model->according_contract){
                        $billProduct->product_id = null;
                    }
                }
                if(!empty($billProductModel)){
                    if ($flag = FSMBaseModel::validateMultiple($billProductModel)) {
                        foreach ($billProductModel as $billProduct) {
                            if (!$billProduct->save(false)) {
                                throw new Exception('Unable to save data! '.$billProduct->errorMessage);
                                break;
                            }
                        }
                    }
                } 

                $billPersonModel = FSMBaseModel::createMultiple(FirstBillPerson::class);
                FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
                foreach ($billPersonModel as $index => $billPerson) {
                    if(empty($billPerson->client_person_id)){
                        unset($billPersonModel[$index]);
                        continue;
                    }
                    $billPerson->id = null;
                    $billPerson->setIsNewRecord(true);
                    $billPerson->bill_id = $model->id;                      
                    $billPerson->person_order = 1;                      
                }
                if(!empty($billPersonModel)){
                    if ($flag = FSMBaseModel::validateMultiple($billPersonModel)) {
                        foreach ($billPersonModel as $billPerson) {
                            if (($flag = $billPerson->save(false)) === false) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                }

                $billPersonModel = FSMBaseModel::createMultiple(SecondBillPerson::class);
                FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
                foreach ($billPersonModel as $index => $billPerson) {
                    if(empty($billPerson->client_person_id)){
                        unset($billPersonModel[$index]);
                        continue;
                    }
                    $billPerson->id = null;
                    $billPerson->setIsNewRecord(true);
                    $billPerson->bill_id = $model->id;                      
                    $billPerson->person_order = 2;                      
                }
                if(!empty($billPersonModel)){
                    if ($flag = FSMBaseModel::validateMultiple($billPersonModel)) {
                        foreach ($billPersonModel as $billPerson) {
                            if (($flag = $billPerson->save(false)) === false) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                }

                $cessionDebet = new $this->defaultModel;
                $cessionKredit = new $this->defaultModel;
                $agreementList = Agreement::findAll([
                    'first_client_id' => $model->firstClient->id, 
                    'second_client_id' => $model->secondClient->id, 
                    'agreement_type' => Agreement::AGREEMENT_TYPE_CESSION,
                    'status' => Agreement::AGREEMENT_STATUS_SIGNED,
                    'deleted' => 0]);
                if(empty($agreementList)){
                    throw new Exception('Can not find an appropriate agreement!');
                }                    
                $agreement = $agreementList[0];
                $thirdClient = $agreement->thirdClient;
                $thirdClientBankList = $thirdClient->clientActiveBanks;
                $thirdClientBank = !empty($thirdClientBankList) ? $thirdClientBankList[0] : null;

                $model->doc_type = Bill::BILL_DOC_TYPE_CESSION;
                $cessionDebet->project_id = $agreement->project_id;
                $cessionDebet->agreement_id = $agreement->id;
                $cessionDebet->first_client_bank_id = $model->first_client_bank_id;
                $cessionDebet->second_client_bank_id = $thirdClientBank ? $thirdClientBank->id : null;
                $cessionDebet->parent_id = $id;
                $cessionDebet->doc_type = Bill::BILL_DOC_TYPE_CESSION;
                $cessionDebet->cession_direction = 'D';
                $cessionDebet->doc_date = date('Y-m-d');
                $cessionDebet->pay_date = date('Y-m-d', strtotime("+".(!empty($agreement->deferment_payment) ? $agreement->deferment_payment : Bill::BILL_DEFAULT_PAYMENT_DAYS)." days", strtotime($cessionDebet->doc_date)));
                $cessionDebet->according_contract = true;
                $cessionDebet->summa = abs($agreement->summa);
                $cessionDebet->vat = 0;
                $cessionDebet->total = abs($agreement->summa);
                $cessionDebet->valuta_id = $agreement->valuta_id;
                $cessionDebet->manager_id = $model->manager_id;
                $cessionDebet->language_id = $model->language_id;
                $cessionDebet->services_period = $model->services_period;
                if (!$cessionDebet->save()) {
                    throw new Exception('Unable to save data! '.$cessionDebet->errorMessage);
                }
                $abonentCModel = new AbonentBill();
                $abonentCModel->abonent_id = $model->userAbonentId;
                $abonentCModel->bill_id = $cessionDebet->id;
                $abonentCModel->project_id = $abonentModel->project_id;
                $abonentCModel->save();

                $cessionKredit->project_id = $agreement->project_id;
                $cessionKredit->agreement_id = $agreement->id;
                $cessionKredit->first_client_bank_id = $thirdClientBank ? $thirdClientBank->id : null;
                $cessionKredit->second_client_bank_id = $model->second_client_bank_id;
                $cessionKredit->parent_id = $id;
                $cessionKredit->doc_type = Bill::BILL_DOC_TYPE_CESSION;
                $cessionKredit->cession_direction = 'K';
                $cessionKredit->doc_date = date('Y-m-d');
                $cessionKredit->pay_date = date('Y-m-d', strtotime("+".(!empty($agreement->deferment_payment) ? $agreement->deferment_payment : Bill::BILL_DEFAULT_PAYMENT_DAYS)." days", strtotime($cessionKredit->doc_date)));
                $cessionKredit->according_contract = true;
                $cessionKredit->summa = abs($model->total);
                $cessionKredit->vat = 0;
                $cessionKredit->total = abs($model->total);
                $cessionKredit->valuta_id = $model->valuta_id;
                $cessionKredit->manager_id = $model->manager_id;
                $cessionKredit->language_id = $model->language_id;
                $cessionKredit->services_period = $model->services_period;
                if (!$cessionKredit->save()) {
                    throw new Exception('Unable to save data! '.$cessionKredit->errorMessage);
                }
                $abonentKModel = new AbonentBill();
                $abonentKModel->abonent_id = $model->userAbonentId;
                $abonentKModel->bill_id = $cessionKredit->id;
                $abonentKModel->project_id = $abonentModel->project_id;
                $abonentKModel->save();
                
                if(!empty($billProductModel)){
                    foreach ($billProductModel as $billProduct) {
                        $billProduct->id = null;
                        $billProduct->setIsNewRecord(true);
                        $billProduct->bill_id = $cessionDebet->id;
                        if (!$billProduct->save(false)) {
                            throw new Exception('Unable to save data! '.$billProduct->errorMessage);
                            break;
                        }
                    }

                    foreach ($billProductModel as $billProduct) {
                        $billProduct->id = null;
                        $billProduct->setIsNewRecord(true);
                        $billProduct->bill_id = $cessionKredit->id;
                        if (!$billProduct->save(false)) {
                            throw new Exception('Unable to save data! '.$billProduct->errorMessage);
                            break;
                        }
                    }
                }

                $billPersonModel = FSMBaseModel::createMultiple(FirstBillPerson::class);
                FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
                foreach ($billPersonModel as $index => $billPerson) {
                    if(empty($billPerson->client_person_id)){
                        unset($billPersonModel[$index]);
                        continue;
                    }
                    $billPerson->id = null;
                    $billPerson->setIsNewRecord(true);
                    $billPerson->bill_id = $cessionKredit->id;                      
                    $billPerson->person_order = 1;                      
                }
                if(!empty($billPersonModel)){
                    if ($flag = FSMBaseModel::validateMultiple($billPersonModel)) {
                        foreach ($billPersonModel as $billPerson) {
                            if (($flag = $billPerson->save(false)) === false) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                }

                $billPersonModel = FSMBaseModel::createMultiple(SecondBillPerson::class);
                FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
                foreach ($billPersonModel as $index => $billPerson) {
                    if(empty($billPerson->client_person_id)){
                        unset($billPersonModel[$index]);
                        continue;
                    }
                    $billPerson->id = null;
                    $billPerson->setIsNewRecord(true);
                    $billPerson->bill_id = $cessionKredit->id;                      
                    $billPerson->person_order = 2;                      
                }
                if(!empty($billPersonModel)){
                    if ($flag = FSMBaseModel::validateMultiple($billPersonModel)) {
                        foreach ($billPersonModel as $billPerson) {
                            if (($flag = $billPerson->save(false)) === false) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                }

                if(!empty($requestPost['Bill']['reserved_id'])){
                    ReservedId::deleteAll(['id' => $requestPost['Bill']['reserved_id']]);
                }
                
                if(!$this->updateAttacment($model, $filesModel)){
                    throw new Exception('Unable to save attachment! '.$filesModel->errorMessage);
                }
                
                if (!$flag) {
                    throw new Exception('Unable to save data!');
                }else{
                    $transaction->commit();
                } 
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirect('index');              
            }                
        } else {
            BillUIAsset::register(Yii::$app->getView());
            
            $billProductModel = [new BillProduct()];  
            $billProductModel[0]->product_name = $parentInvoice->doc_number .' '.Yii::t('bill', 'Debt').': '.$debt.' '.$model->valuta->name;
            $billProductModel[0]->amount = 1;
            $billProductModel[0]->price = $debt;
            $billProductModel[0]->vat = 0;
            
            $billFirstPersonModel = [new FirstBillPerson(), new FirstBillPerson()];
            $billSecondPersonModel = [new SecondBillPerson(), new SecondBillPerson()];
            
            $model->reserved_id = Yii::$app->security->generateRandomString();
            $firstClient = $agreementModel->firstClient;
            if(!empty($firstClient->gen_number)){
                $lastNumber = $firstClient->getLastNumber($model->reserved_id);
                $model->doc_number = $firstClient->gen_number.$lastNumber;
            }else{
                $model->doc_number = $firstClient->id.'/'.date('Y-m-d');
            }     
            
            $model->status = Bill::BILL_STATUS_COMPLETE;
            $model->pay_status = Bill::BILL_PAY_STATUS_FULL;
            $model->doc_type = Bill::BILL_DOC_TYPE_DEBT;
            $model->doc_date = date('d-m-Y');
            $model->pay_date = date('d-m-Y');
            $model->services_period_from = !empty($model->services_period_from) ? date('d-m-Y', strtotime($model->services_period_from)) : null;
            $model->services_period_till = !empty($model->services_period_till) ? date('d-m-Y', strtotime($model->services_period_till)) : null;
            $model->delayed = 0;
            $model->doc_key = '';
            $model->according_contract = true;

            $isAdmin = FSMUser::getIsPortalAdmin();
            $projectList = Project::getNameArr(['project.deleted' => 0]);
            $clientList = Client::getNameArr(['client.deleted' => 0]);
            $clientRoleList = ClientRole::getNameArr();
            $productList = Product::getNameArr();
            $measureList = Measure::getNameArr();
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            $languageList = Language::getEnabledLanguageList();
            $managerList = FSMProfile::getProfileListByRole([
                FSMUser::USER_ROLE_ADMIN,
                FSMUser::USER_ROLE_OPERATOR, 
                FSMUser::USER_ROLE_BOOKER,
                FSMUser::USER_ROLE_COORDINATOR,
            ]);
            if(!Yii::$app->user->isGuest){
                $profile = Yii::$app->user->identity->profile;
                if(array_key_exists($profile->id, $managerList)){
                    $model->manager_id = $profile->id;
                }                
            }
            return $this->render('create', [
                'model' => $model,
                'abonentModel' => $abonentModel,
                'agreementModel' => $agreementModel,
                'firstClientModel' => $agreementModel->firstClient,
                'secondClientModel' => $agreementModel->secondClient,
                'filesModel' => $filesModel,
                'projectList' => $projectList,
                'clientList' => $clientList,
                'clientRoleList' => $clientRoleList,
                'valutaList' => $valutaList,
                'managerList' => $managerList,
                'billProductModel' => $billProductModel,
                'billFirstPersonModel' => $billFirstPersonModel,
                'billSecondPersonModel' => $billSecondPersonModel,                
                'productList' => $productList,
                'measureList' => $measureList,
                'languageList' => $languageList,
                'isAdmin' => $isAdmin,
            ]);
        }
    }    
    
    public function actionCopy($id) {
        $model = $this->findModel($id, true);
        $abonentModel = AbonentBill::findOne([
            'abonent_id' => $model->userAbonentId, 
            'bill_id' => $id,
        ]);        
        
        $agreementModel = $model->agreement;
        $agreementModel = (!empty($agreementModel) ? $agreementModel : new Agreement());
        $billProductModel = $model->billProducts;        
        $billProductModel = !empty($billProductModel) ? $billProductModel : [new BillProduct()];
        $billFirstPersonModel = $model->firstClientPerson;        
        $billFirstPersonModel = !empty($billFirstPersonModel) ? $billFirstPersonModel : [new FirstBillPerson(), new FirstBillPerson()];
        $billSecondPersonModel = $model->secondClientPerson;        
        $billSecondPersonModel = !empty($billSecondPersonModel) ? $billSecondPersonModel : [new SecondBillPerson(), new SecondBillPerson()];
        
        $billProductProjectModel = $model->billProductProjectsByAbonent;
        $billProductProjectModel = !empty($billProductProjectModel) ? $billProductProjectModel : [new BillProductProject()];
        
        $filesModel = [new Files()];
        
        $model->id = null;
        $model->setIsNewRecord(true);
        
        $modelArr = [
            'Bill' => $model,
            'AbonentBill' => $abonentModel,
        ];
        
        // ajax validation
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && $isAjax) {
            $ajaxModelArr = $modelArr;
            $billProductModel = FSMBaseModel::createMultiple(BillProduct::class);
            FSMBaseModel::loadMultiple($billProductModel, $requestPost);

            $billProductProjectModel = FSMBaseModel::createMultiple(BillProductProject::class);
            FSMBaseModel::loadMultiple($billProductProjectModel, $requestPost);
            $hasEmptyProject = false;
            foreach ($billProductModel as $index => $billProduct) {
                if(empty($billProduct->product_id) && empty($billProduct->amount) && empty($billProduct->price)){
                    unset($billProductModel[$index]);
                    if(isset($billProductProjectModel[$index])){
                        unset($billProductProjectModel[$index]);
                    }
                    continue;
                }

                if(empty($billProductProjectModel[$index]->project_id) &&
                    empty($requestPost['AbonentBill']['project_id'])
                ){
                    $billProductProjectModel[$index]->needValidateProject = $hasEmptyProject = true;
                }
            }
            if (!empty($billProductModel)) {
                $ajaxModelArr = ArrayHelper::merge($modelArr, ['BillProduct' => $billProductModel]);
            }
            if (!empty($billProductProjectModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['BillProductProject' => $billProductProjectModel]);
            }
            $ajaxModelArr['AbonentBill']->needValidateProject = $hasEmptyProject;

            $billPersonModel = FSMBaseModel::createMultiple(FirstBillPerson::class);
            FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
            foreach ($billPersonModel as $index => $billPerson) {
                if(empty($billPerson->client_person_id)){
                    unset($billPersonModel[$index]);
                    continue;
                }
            }
            if (!empty($billPersonModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['FirstBillPerson' => $billPersonModel]);
            }
            
            $billPersonModel = FSMBaseModel::createMultiple(SecondBillPerson::class);
            FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
            foreach ($billPersonModel as $index => $billPerson) {
                if(empty($billPerson->client_person_id)){
                    unset($billPersonModel[$index]);
                    continue;
                }
            }
            if (!empty($billPersonModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['SecondBillPerson' => $billPersonModel]);
            }
            
            return $this->performAjaxMultipleValidation($ajaxModelArr);            
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $flag = true;
                if(empty($model->agreement_id)){
                    throw new Exception('Agreement was not defined!');
                }
                $model->doc_key = 
                $model->mail_status = 
                $model->transfer_status = 
                $model->paid_date = 
                $model->complete_date = null;
                $model->delayed = $model->returned = $model->blocked = 0;
                $model->doc_key = '';
                
                if (!$model->save()) {
                    throw new Exception('Unable to save data! '.$model->errorMessage);
                }
                $abonentModel->id = null;
                $abonentModel->setIsNewRecord(true);
                $abonentModel->bill_id = $model->id;                
                $abonentModel->save();
                
                if(!$this->createAttachment($model, $filesModel)){
                    throw new Exception('Unable to save attachment! '.$filesModel->errorMessage);
                }
                                
                $historyModule = new BillHistory();
                $historyModule->saveHistory($model->id, BillHistory::BillHistory_ACTIONS['ACTION_CREATE']);
                unset($historyModule);

                $billProductModel = FSMBaseModel::createMultiple(BillProduct::class);
                FSMBaseModel::loadMultiple($billProductModel, $requestPost);            
                
                $billProductProjectModel = FSMBaseModel::createMultiple(BillProductProject::class);
                FSMBaseModel::loadMultiple($billProductProjectModel, $requestPost);
                foreach ($billProductModel as $index => $billProduct) {
                    if(empty($billProduct->product_id) && empty($billProduct->amount) && empty($billProduct->price)){
                        unset($billProductModel[$index]);
                        if(isset($billProductProjectModel[$index])){
                            unset($billProductProjectModel[$index]);
                        }
                        continue;
                    }
                    $billProduct->id = null;
                    $billProduct->price_eur = $billProduct->price / $model->rate;
                    $billProduct->summa_eur = $billProduct->summa / $model->rate;
                    $billProduct->summa_vat_eur = $billProduct->summa_vat / $model->rate;
                    $billProduct->total_eur = $billProduct->total / $model->rate;
                    $billProduct->setIsNewRecord(true);
                    $billProduct->bill_id = $model->id;
                    if($model->according_contract){
                        $billProduct->product_id = null;
                    }
                }
                if(!empty($billProductModel)){
                    if ($flag = FSMBaseModel::validateMultiple($billProductModel)) {
                        foreach ($billProductModel as $index => $billProduct) {
                            if (!$billProduct->save(false)) {
                                throw new Exception('Unable to save data! '.$billProduct->errorMessage);
                            }
                                
                            $billProductProjectModel[$index]->bill_id = $model->id;
                            $billProductProjectModel[$index]->bill_product_id = $billProduct->id;
                            if(empty($billProductProjectModel[$index]->project_id)){
                                $billProductProjectModel[$index]->project_id = $abonentModel->project_id;
                            }
                            if (!$billProductProjectModel[$index]->save(false)) {
                                throw new Exception('Unable to save data! '.$billProductProjectModel[$index]->errorMessage);
                            }
                        }
                    }
                }  

                $billPersonModel = FSMBaseModel::createMultiple(FirstBillPerson::class);
                FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
                foreach ($billPersonModel as $index => $billPerson) {
                    if(empty($billPerson->client_person_id)){
                        unset($billPersonModel[$index]);
                        continue;
                    }
                    $billPerson->id = null;
                    $billPerson->setIsNewRecord(true);                    
                    $billPerson->bill_id = $model->id;                      
                    $billPerson->person_order = 1;                      
                }
                if(!empty($billPersonModel)){
                    if ($flag = FSMBaseModel::validateMultiple($billPersonModel)) {
                        foreach ($billPersonModel as $billPerson) {
                            if (!$billPerson->save(false)) {
                                throw new Exception('Unable to save data! '.$billPerson->errorMessage);
                            }
                        }
                    }
                }

                $billPersonModel = FSMBaseModel::createMultiple(SecondBillPerson::class);
                FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
                foreach ($billPersonModel as $index => $billPerson) {
                    if(empty($billPerson->client_person_id)){
                        unset($billPersonModel[$index]);
                        continue;
                    }
                    $billPerson->id = null;
                    $billPerson->setIsNewRecord(true);                    
                    $billPerson->bill_id = $model->id;                      
                    $billPerson->person_order = 2;                      
                }
                if(!empty($billPersonModel)){
                    if ($flag = FSMBaseModel::validateMultiple($billPersonModel)) {
                        foreach ($billPersonModel as $billPerson) {
                            if (!$billPerson->save(false)) {
                                throw new Exception('Unable to save data! '.$billPerson->errorMessage);
                            }
                        }
                    }
                }

                if(!empty($requestPost['Bill']['reserved_id'])){
                    ReservedId::deleteAll(['id' => $requestPost['Bill']['reserved_id']]);
                }
                
                if (!$flag) {
                    throw new Exception('Unable to save data!');
                }else{
                    $transaction->commit();
                }                
            } catch (Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirect('index');              
            }                
        } else {
            BillUIAsset::register(Yii::$app->getView());

            $model->reserved_id = Yii::$app->security->generateRandomString();
            $firstClient = $agreementModel->firstClient;
            if(!empty($firstClient->gen_number)){
                $lastNumber = $firstClient->getLastNumber($model->reserved_id);
                $model->doc_number = $firstClient->gen_number.$lastNumber;
            }else{
                $model->doc_number = $firstClient->id.'/'.date('Y-m-d');
            }        
            
            $model->status = Bill::BILL_STATUS_PREPAR;
            $model->pay_status = Bill::BILL_PAY_STATUS_NOT;
            $model->doc_date = date('d-m-Y');
            $model->services_period_from = !empty($model->services_period_from) ? date('d-m-Y', strtotime($model->services_period_from)) : null;
            $model->services_period_till = !empty($model->services_period_till) ? date('d-m-Y', strtotime($model->services_period_till)) : null;
            $model->pay_date = date('d-m-Y', strtotime("+".Bill::BILL_DEFAULT_PAYMENT_DAYS." days", strtotime($model->doc_date)));

            $isAdmin = FSMUser::getIsPortalAdmin();
            $agreementList = Agreement::getNameArr(['agreement.deleted' => false]);
            $projectList = Project::getNameArr(['project.deleted' => 0]);
            $clientList = Client::getNameArr(['client.deleted' => 0]);
            $clientRoleList = ClientRole::getNameArr();
            $productList = Product::getNameArr();
            $measureList = Measure::getNameArr();
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            $languageList = Language::getEnabledLanguageList();
            $managerList = FSMProfile::getProfileListByRole([
                FSMUser::USER_ROLE_ADMIN,
                FSMUser::USER_ROLE_OPERATOR, 
                FSMUser::USER_ROLE_BOOKER,
                FSMUser::USER_ROLE_COORDINATOR,
            ]);
            if(!Yii::$app->user->isGuest){
                $profile = Yii::$app->user->identity->profile;
                if(array_key_exists($profile->id, $managerList)){
                    $model->manager_id = $profile->id;
                }                
            }

            if(count($billFirstPersonModel) == 1){
                $billFirstPersonModel[1] = new FirstBillPerson();
            }
            if(count($billSecondPersonModel) == 1){
                $billSecondPersonModel[1] = new SecondBillPerson();
            }
            
            return $this->render('create', [
                'model' => $model,
                'abonentModel' => $abonentModel,
                'agreementModel' => $agreementModel,
                'firstClientModel' => $agreementModel->firstClient,
                'secondClientModel' => $agreementModel->secondClient,
                'filesModel' => $filesModel,
                'projectList' => $projectList,
                'agreementList' => $agreementList,
                'clientList' => $clientList,
                'clientRoleList' => $clientRoleList,
                'valutaList' => $valutaList,
                'managerList' => $managerList,
                'billProductModel' => $billProductModel,
                'billProductProjectModel' => $billProductProjectModel ?? [],
                'billFirstPersonModel' => $billFirstPersonModel,
                'billSecondPersonModel' => $billSecondPersonModel,                
                'productList' => $productList,
                'measureList' => $measureList,
                'languageList' => $languageList,
                'isAdmin' => $isAdmin,
            ]);
        }
    }    
        
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAjaxBillPay($id) {
        $model = $this->findModel($id, true);
        $lastPayment = $model->billLastPayment;
        $historyModel = new BillHistory();
        $historyModel->bill_id = !empty($id) ? $id : null;
        $historyModel->action_id = BillHistory::BillHistory_ACTIONS['ACTION_STATUS_UP_PAID'];
        $historyModel->create_time = date('d-m-Y H:i:s');
        $historyModel->create_user_id = Yii::$app->user->id;

        $modelArr = [
            'Bill' => $model,
            'BillHistory' => $historyModel,
        ];
        
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxMultipleValidation($modelArr);
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $historyModel->create_time = date('Y-m-d H:i:s', strtotime($historyModel->create_time));
                $historyModel->comment .= (!empty($historyModel->comment) ? "\n" : '').Yii::t('common', 'Sum').': '.
                    (!empty($lastPayment) ?
                        number_format($lastPayment->summa, 2, '.', ' ').(!empty($lastPayment->valuta_id) ? ' '.$lastPayment->valuta->name : '')
                        : ''
                    );
                if (!$historyModel->save()) {
                    throw new Exception('Unable to save data! '.$historyModel->errorMessage);
                }
                if(isset($lastPayment)){
                    $lastPayment->updateAttributes(['confirmed' => date('Y-m-d')]);
                }
                if (!$model->changeStatus(Bill::BILL_STATUS_PAID, ['paid_date' => date('Y-m-d', strtotime($model->paid_date))])) {
                    throw new Exception('Unable to save data! '.$model->errorMessage);
                }
                $transaction->commit();
            } catch (Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return 'reload';
            }
        } else {
            $model->paid_date = date('d-m-Y');
            $model->summa = !empty($lastPayment) ?
                (number_format($lastPayment->summa, 2, '.', '').(!empty($lastPayment->valuta_id) ? ' '.$lastPayment->valuta->name : ''))
                : 0;
            return $this->renderAjax('create-bill-pay', [
                'model' => $model,
                'historyModel' => $historyModel,
                'isModal' => true,
            ]);
        }
    }
    
    /**
     * Deletes an existing single model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id, true);
        if($parentInvoice = $model->parent){
            $parentInvoice->updateAttributes([
                'status' => Bill::BILL_STATUS_SIGNED, 
                'pay_status' => Bill::BILL_PAY_STATUS_NOT, 
                'pay_date' => date('Y-m-d', strtotime("+".Bill::BILL_DEFAULT_PAYMENT_DAYS." days", strtotime($model->doc_date))),
            ]);
        }
        if($parentInvoices = $model->parents){
            foreach ($parentInvoices as $bill) {
                $bill->updateAttributes([
                    'child_id' => null, 
                ]);
            }
        }       
        
        return parent::actionDelete($id);
    }    
    
    public function actionCheckDelayed($isCron = true)
    {
        $billModel = new $this->defaultModel;
        $billCount = $billModel->checkDelayed();
        unset($billModel);
        if($isCron){
            return true;
        }else{
            return $this->render('check-delayed', [
                'billCount' => $billCount,
            ]);        
        }
    }
    
    public function actionSendMailClient($id)
    {
        $model = $this->findModel($id, true);
        $model->checkPDF();
        $result = $model->sendMailClient();
        if($result){
            $historyModule = new BillHistory();
            $historyModule->saveHistory($model->id, BillHistory::BillHistory_ACTIONS['ACTION_SENT_EMAIL_CLIENT']);
            unset($historyModule);            
            Yii::$app->getSession()->setFlash('success', Yii::t('bill', 'Invoice sent to client.'));
        }else{
            Yii::$app->getSession()->setFlash('error', Yii::t('bill', 'Invoice does not sent to client.'));
        }
        return $this->redirect(FSMBaseModel::getBackUrl());     
    }    
    
    public function actionSendMailAgent($id)
    {
        $model = $this->findModel($id, true);
        $model->checkPDF();
        $result = $model->sendMailAgent();
        if($result){
            $historyModule = new BillHistory();
            $historyModule->saveHistory($model->id, BillHistory::BillHistory_ACTIONS['ACTION_SENT_EMAIL_AGENT']);
            unset($historyModule);            
            Yii::$app->getSession()->setFlash('success', Yii::t('bill', 'Invoice sent to agent.'));
        }else{
            Yii::$app->getSession()->setFlash('error', Yii::t('bill', 'Invoice does not sent to agent.'));
        }
        return $this->redirect(FSMBaseModel::getBackUrl());     
    }
    
    public function actionAjaxClearReservedId($reservedId)
    {
        if(!empty($reservedId)){
            ReservedId::deleteAll(['id' => $reservedId]);
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return true;
    }
    
    public function actionViewMailPdf($bill_id, $user_id, $code)
    {
        $token = FSMToken::findOne([
            'user_id' => $user_id,
            'code'    => $code,
            'type'    => Bill::TOKEN_TYPE_NEW_BILL,
        ]);

        if ($token instanceof FSMToken) {
            //$token->delete();
            if($model = $this->findModel($bill_id)){
                $model->updateAttributes(['mail_status' => Bill::BILL_MAIL_STATUS_RECEIVED]);
                return $this->actionViewPdf($bill_id);
            }else{
                $message = Yii::t('bill', 'Can not find invoice.');
            }
        } else {
            $message = Yii::t('bill', 'The view link is invalid or expired.');
        }
        $user = FSMUser::findOne($user_id);
        Yii::$app->session->setFlash('danger', $message);

        return $this->render('/message', [
            'title'  => Yii::t('bill', 'Invoice viewing'),
            'module' => isset($user) ? $user->module : Yii::$app->getModule('user'),
        ]);        
    }
    
    public function actionReceiveMailGif($bill_id, $user_id, $code)
    {
        $token = FSMToken::findOne([
            'user_id' => $user_id,
            'code'    => $code,
            'type'    => Bill::TOKEN_TYPE_NEW_BILL,
        ]);

        if ($token instanceof FSMToken) {
            //$token->delete();
            if($model = $this->findModel($bill_id)){
                $model->updateAttributes(['mail_status' => Bill::BILL_MAIL_STATUS_RECEIVED]);
            }
        }
        
        $imgUrl = Url::to('@web/frontend/assets/images', true).'/';
        $imgPath = Url::to('@webroot/frontend/assets/images').'/';
        $imgName = 'blank.gif';
        //Full URI to the image
        $filename = $imgPath . $imgName;
        
        if (!file_exists($filename)) {
            echo "File {$filename} not exist!";
            Yii::$app->end();
        }else{
            //Get the filesize of the image for headers
            $filesize = filesize( $imgPath.'blank.gif' );
        }
        $filename = $imgUrl . $imgName;

        /*
        //Now actually output the image requested (intentionally disregarding if the database was affected)
        header( 'Pragma: public' );
        header( 'Expires: 0' );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
        header( 'Cache-Control: private',false );
        header( 'Content-Disposition: attachment; filename="blank.gif"' );
        header( 'Content-Transfer-Encoding: binary' );
        header( 'Content-Length: '.$filesize );
        readfile( $filename );
        
        Yii::$app->end();
         * 
         */
        return Yii::$app->response->sendFile($imgPath, $filename, ['inline'=>true]);
    }
    
    public function actionBlock($id)
    {
        $model = $this->findModel($id, true);
        if($model->createPdf()){
            $model->updateAttributes(['blocked' => 1]);

            $historyModule = new BillHistory();
            $historyModule->saveHistory($model->id, BillHistory::BillHistory_ACTIONS['ACTION_BLOCK']);
            unset($historyModule);            
            Yii::$app->getSession()->setFlash('success', Yii::t('bill', 'Invoice was blocked.'));
        }else{
            Yii::$app->getSession()->setFlash('error', Yii::t('bill', 'Could not create PDF file!'));
        }
        return $this->redirect(FSMBaseModel::getBackUrl());     
    }    
    
    public function actionUnblock($id)
    {
        $model = $this->findModel($id, true);
        $model->updateAttributes(['blocked' => 0]);

        $historyModule = new BillHistory();
        $historyModule->saveHistory($model->id, BillHistory::BillHistory_ACTIONS['ACTION_UNBLOCK']);
        unset($historyModule);            
        Yii::$app->getSession()->setFlash('success', Yii::t('bill', 'Invoice was unblocked.'));
        return $this->redirect(FSMBaseModel::getBackUrl());     
    }    
    
    public function actionAjaxModalNameList($param = [])
    {
        if(!isset($this->defaultModel)){
            return [];
        }
        
        $model = new $this->defaultModel;
        
        $selectedId = null;
        if(isset($param['selected_id'])){
            $selectedId = $param['selected_id'];
            unset($param['selected_id']);
        }
        $param = !empty($param) ? $param : null;
        
        if($model->hasAttribute('deleted') && !isset($param['deleted'])){
            $param['deleted'] = false;
        }
        
        // JSON response is expected in case of successful save
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $result = [];
        $source = !empty($_GET['source']) ? $_GET['source'] : null;
        switch ($source) {
            case 'bill-confirm':
                $billConfirmModel = 
                    !empty($_GET['bill_confirm_id']) ? 
                        \common\models\bill\BillConfirm::findOne($_GET['bill_confirm_id']) : 
                        new \common\models\bill\BillConfirm();
                $data = $billConfirmModel->getAvailableBillList();
                unset($billConfirmModel);
                break;

            default:
                $data = $model::getNameArr($param, $model::$nameField, $model::$keyField, $model::$nameField);
                break;
        }
        unset($model);
        
        foreach ($data as $key => $item) {
            $result[] = [
                'id' => $key,
                'text' => $item,
            ];
        }
        
        return [
            'success' => true,
            'selected' => $selectedId,
            'data' => $result,
        ];        
    }    
    
    public function actionAjaxAddAttachment($id) {
        $isAdmin = FSMUser::getIsPortalAdmin();
        
        $model = $this->findModel($id, true);
        $filesModel = $model->attachmentFiles;
        $filesModel = (!empty($filesModel) ? $filesModel : [new Files()]);
        
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();

        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxValidation($model);
        }

        if ($model->load($requestPost)) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                if(!$this->updateAttacment($model, $filesModel)){
                    throw new Exception('Unable to save data! '.$filesModel->errorMessage);
                }
                $transaction->commit();
            } catch (Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return 'reload';
            }
        } else {
            return $this->renderAjax('add-attachment', [
                'model' => $model,
                'filesModel' => $filesModel,
                'isAdmin' => $isAdmin,
                'isModal' => true,
            ]);
        }
    }    
}