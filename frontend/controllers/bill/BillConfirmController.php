<?php

namespace frontend\controllers\bill;

use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Response;

use common\controllers\FilSMController;
use common\models\mainclass\FSMBaseModel;
use common\models\user\FSMUser;
use common\models\bill\Bill;
use common\models\bill\BillConfirm;
use common\models\bill\BillConfirmLink;
use common\models\client\Client;
use common\models\client\ClientBank;
use common\models\bill\PaymentConfirm;
use common\models\client\Agreement;
use common\models\Valuta;

use frontend\assets\BillConfirmUIAsset;
/**
 * BillConfirmController implements the CRUD actions for BillConfirm model.
 */
class BillConfirmController extends FilSMController
{
    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\bill\BillConfirm';
        $this->defaultSearchModel = 'common\models\bill\search\BillConfirmSearch';
    }

    public function actionAjaxGetDocList()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];  
        if (isset($_POST['depdrop_parents'])) {  
            $id = $_POST['depdrop_parents'];  
            if(!is_numeric($id) && !is_array($id)){  
                echo Json::encode(['output' => '', 'selected' => '']);  
                return false;  
            } 
            $requestGet = Yii::$app->request->get();
            
            $model = !empty($requestGet['bill_confirm_id']) ? $this->findModel($requestGet['bill_confirm_id']) : new BillConfirm();
            
            $resultList = [];
            switch ($id[1]) {
                case BillConfirmLink::DOC_TYPE_BILL:
                    $resultList = $model->getAvailableBillList();
                    break;
                case BillConfirmLink::DOC_TYPE_AGREEMENT:
                    $resultList = $model->getAvailableAgreementList();
                    break;
                case BillConfirmLink::DOC_TYPE_EXPENSE:
                    $resultList = $model->getAvailableExpenseList();
                    break;
                case BillConfirmLink::DOC_TYPE_DIRECT:
                    $resultList = $model->getAvailableDirectExpenseList();
                    break;

                default:
                    break;
            }
            
            $list = $resultList;
            if (count($list) > 0) {  
                foreach ($list as $i => $item) {  
                    $out[] = [
                        'id' => $i, 
                        'name' => $item,
                    ];  
                }  
                $selected = (count($out) == 1 ? strval($out[0]['id']) : '');
            }else{
                $selected = '';  
            }  
            // Shows how you can preselect a value  
            return ['output' => $out, 'selected' => $selected];  
        }  
        return ['output' => '', 'selected'=>''];        
    }
    
    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionCreate($id = null)
    {
        $paymentConfirmModel = PaymentConfirm::findOne($id);
        if (empty($paymentConfirmModel->id)) {
            throw new Exception('The requested page does not exist.');
        }        
        $model = new $this->defaultModel;
        $model->scenario = 'create';
        $confirmLinkModel = [new BillConfirmLink()];

        $modelArr = [
            'BillConfirm' => $model,
        ];
        
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            $ajaxModelArr = $modelArr;
            $confirmLinkModel = FSMBaseModel::createMultiple(BillConfirmLink::class);
            FSMBaseModel::loadMultiple($confirmLinkModel, $requestPost);
            foreach ($confirmLinkModel as $index => $confirmLink) {
                if(empty($confirmLink->doc_type) && empty($confirmLink->doc_id) && empty($confirmLink->summa)){
                    unset($confirmLinkModel[$index]);
                    continue;
                }
            }
            if (!empty($confirmLinkModel)) {
                $ajaxModelArr = ArrayHelper::merge($modelArr, ['BillConfirmLink' => $confirmLinkModel]);
            }            
            
            return $this->performAjaxMultipleValidation($ajaxModelArr); 
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $confirmLinkModel = FSMBaseModel::createMultiple(BillConfirmLink::class);
                FSMBaseModel::loadMultiple($confirmLinkModel, $requestPost);            

                $secondClientModel = null;
                if(!empty($model->second_client_id)){
                    $secondClientModel = Client::findOne($model->second_client_id);
                }else{
                    if((count($confirmLinkModel) == 1) && ($confirmLinkModel[0]->doc_type == BillConfirmLink::DOC_TYPE_DIRECT)){
                        $bankModel = \common\models\Bank::findOne($paymentConfirmModel->bank_id);
                        $secondClientModel = !empty($bankModel->reg_number) ?
                            Client::findOne(['reg_number' => $bankModel->reg_number]) :
                            null;                        
                    }
                } 
                if(!isset($secondClientModel)){
                    throw new Exception('Unable find Client!');
                }

                $model->second_client_id = $secondClientModel->id;
                $model->manual_input = 1;
                if(!$model->save()){
                    throw new Exception('Unable to save data! '.$model->errorMessage);
                }

                foreach ($confirmLinkModel as $index => $confirmLink) {
                    if(empty($confirmLink->doc_type) && empty($confirmLink->doc_id) && empty($confirmLink->summa)){
                        unset($confirmLinkModel[$index]);
                        continue;
                    }

                    $confirmModel = null;
                    if($confirmLink->doc_type == BillConfirmLink::DOC_TYPE_BILL){
                        $confirmModel = Bill::findOne($confirmLink->doc_id);
                        $paymentList = $confirmModel->notConfirmedPayments;
                    }elseif($confirmLink->doc_type == BillConfirmLink::DOC_TYPE_AGREEMENT){
                        $confirmModel = Agreement::findOne($confirmLink->doc_id);
                        $paymentList = ($model->direction == 'D') ? $confirmModel->notConfirmedPaymentsDebet : $confirmModel->notConfirmedPaymentsCredit;
                    }
                    if(!empty($paymentList)){
                        foreach ($paymentList as $payment) {
                            if($payment->summa == $confirmLink->summa){
                                $confirmLink->payment_id = $payment->id;
                                break;
                            }
                        }
                    }

                    $confirmLink->bill_confirm_id = $model->id;
                }

                if(!empty($confirmLinkModel)){
                    if ($flag = FSMBaseModel::validateMultiple($confirmLinkModel)) {
                        foreach ($confirmLinkModel as $confirmLink) {
                            if (!$confirmLink->save(false)) {
                                throw new Exception('Unable to save data! '.$confirmLink->errorMessage);
                            }
                        }
                    }
                }            
                
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirectToBackUrl($id);
            }              
        } else {
            $this->rememberBackUrl($model->backURL, $id);
            BillConfirmUIAsset::register(Yii::$app->getView());
            
            $isAdmin = FSMUser::getIsPortalAdmin();
            
            $model->payment_confirm_id = $paymentConfirmModel->id;
            $model->valuta_id = Valuta::VALUTA_DEFAULT;

            $billList = $model->getAvailableBillList();
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            //return $this->renderAjax('create', [
            return $this->render('create', [
                'model' => $model,
                'paymentConfirmModel' => $paymentConfirmModel,
                'confirmLinkModel' => $confirmLinkModel,
                'billList' => $billList,
                'agreementList' => [],
                'expenseList' => [],
                'purchaseList' => [],
                'valutaList' => $valutaList,
                'isAdmin' => $isAdmin,
                //'isModal' => true,
                'isModal' => false,
            ]);
        }
    }
    
    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $payment_confirm_id = null)
    {
        $model = $this->findModel($id);
        if (!empty($payment_confirm_id)) {
            $paymentConfirmModel = PaymentConfirm::findOne($payment_confirm_id);
        } else {
            $paymentConfirmModel = $model->paymentConfirm;
        }
        $confirmLinkModel = $model->confirmLinks;
        $confirmLinkModel = !empty($confirmLinkModel) ? $confirmLinkModel : [new BillConfirmLink()];
        
        $modelArr = [
            'BillConfirm' => $model,
        ];
        
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            $ajaxModelArr = $modelArr;
            $confirmLinkModel = FSMBaseModel::createMultiple(BillConfirmLink::class);
            FSMBaseModel::loadMultiple($confirmLinkModel, $requestPost);
            foreach ($confirmLinkModel as $index => $confirmLink) {
                if(empty($confirmLink->doc_type) && empty($confirmLink->doc_id) && empty($confirmLink->summa)){
                    unset($confirmLinkModel[$index]);
                    continue;
                }
            }
            if (!empty($confirmLinkModel)) {
                $ajaxModelArr = ArrayHelper::merge($modelArr, ['BillConfirmLink' => $confirmLinkModel]);
            }            
            
            return $this->performAjaxMultipleValidation($ajaxModelArr); 
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $oldIDs = isset($confirmLinkModel[0]) && !empty($confirmLinkModel[0]->id) ? ArrayHelper::map($confirmLinkModel, 'id', 'id') : [];

                $confirmLinkModel = FSMBaseModel::createMultiple(BillConfirmLink::class, $confirmLinkModel);
                FSMBaseModel::loadMultiple($confirmLinkModel, $requestPost);  
                $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($confirmLinkModel, 'id', 'id')));

                $secondClientModel = null;
                if(!empty($model->second_client_id)){
                    $secondClientModel = Client::findOne($model->second_client_id);
                }else{
                    if((count($confirmLinkModel) == 1) && ($confirmLinkModel[0]->doc_type == BillConfirmLink::DOC_TYPE_DIRECT)){
                        $bankModel = \common\models\Bank::findOne($paymentConfirmModel->bank_id);
                        $secondClientModel = !empty($bankModel->reg_number) ?
                            Client::findOne(['reg_number' => $bankModel->reg_number]) :
                            null;                        
                    }
                } 
                if(!isset($secondClientModel)){
                    throw new Exception('Unable find Client!');
                }

                $model->second_client_id = $secondClientModel->id;
                $findPart = (int)(!$model->compareFloatNumbers($model->summa, $requestPost['total'], '='));
                $model->find_part = $findPart;
                if(!$model->save()){
                    throw new Exception('Unable to save data! '.$model->errorMessage);
                }

                foreach ($confirmLinkModel as $index => $confirmLink) {
                    if(empty($confirmLink->doc_type) && empty($confirmLink->doc_id) && empty($confirmLink->summa)){
                        unset($confirmLinkModel[$index]);
                        continue;
                    }
                    
                    $confirmModel = null;
                    if($confirmLink->doc_type == BillConfirmLink::DOC_TYPE_BILL){
                        if($confirmModel = Bill::findOne($confirmLink->doc_id)){
                            $payments = $confirmModel->notConfirmedPayments;
                        }
                    }elseif($confirmLink->doc_type == BillConfirmLink::DOC_TYPE_AGREEMENT){
                        if($confirmModel = Agreement::findOne($confirmLink->doc_id)){
                            $payments = ($model->direction == 'D') ? $confirmModel->notConfirmedPaymentsDebet : $confirmModel->notConfirmedPaymentsCredit;
                        }
                    }elseif($confirmLink->doc_type == BillConfirmLink::DOC_TYPE_DIRECT){
                        $confirmLink->doc_subtype = $confirmLink->doc_id;
                        $confirmLink->doc_id = null;
                    }
                    if(isset($confirmModel)){
                        $payment = !empty($payments) ? $payments[0] : null;
                        $confirmLink->payment_id = isset($payment) ? $payment->id : null;
                    }

                    $confirmLink->bill_confirm_id = $model->id;
                }
                
                if(!empty($confirmLinkModel)){
                    if (FSMBaseModel::validateMultiple($confirmLinkModel)) {
                        if (!empty($deletedIDs)) {
                            BillConfirmLink::deleteByIDs($deletedIDs);
                        }     
                        foreach ($confirmLinkModel as $confirmLin) {
                            if (!$confirmLin->save(false)) {
                                throw new Exception('Unable to save data! '.$confirmLin->errorMessage);
                                break;
                            }
                        }
                    }else{
                        throw new Exception('Unable to save data! '.$confirmLinkModel->errorMessage);
                    }
                }
                
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirectToBackUrl($id);
            } 
        } else {
            $this->rememberBackUrl($model->backURL, $id);
            BillConfirmUIAsset::register(Yii::$app->getView());
            
            if(empty($model->valuta_id)){
                $model->valuta_id = Valuta::findOne(['name' => $model->currency]);
            }
            
            if(!isset($confirmLinkModel[0]->id)){
                $confirmLinkModel[0]->summa = $model->summa;
            }elseif($confirmLinkModel[0]->doc_type == BillConfirmLink::DOC_TYPE_DIRECT){
                $confirmLinkModel[0]->doc_id = $confirmLinkModel[0]->doc_subtype;
            }
            
            $isAdmin = FSMUser::getIsPortalAdmin();
            $billList = $model->getAvailableBillList();
            $agreementList = $model->getAvailableAgreementList();
            $expenseList = $model->getAvailableExpenseList();
            $purchaseList = $model->getAvailableDirectExpenseList();
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            $clientList = Client::getNameArr(['client.deleted' => 0]);
            //return $this->renderAjax('update', [
            return $this->render('update', [
                'model' => $model,
                'paymentConfirmModel' => $paymentConfirmModel,
                'confirmLinkModel' => $confirmLinkModel,
                'billList' => $billList,
                'agreementList' => $agreementList,
                'expenseList' => $expenseList,
                'purchaseList' => $purchaseList,
                'valutaList' => $valutaList,
                'clientList' => $clientList,
                'isAdmin' => $isAdmin,
                //'isModal' => true,
                'isModal' => false,
            ]);
        }
    }

    /**
     * Deletes an existing single model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            $model = $this->findModel($id);
            $backUrl = $model->backURL;
            $backUrl = is_array($backUrl) ? $backUrl[0] : $backUrl;
            $pjax = $this->pjaxIndex && (strpos($backUrl, 'view') === false);
            if(!$model->delete()){
                throw new Exception('Can not delete this item!');
            }
            $transaction->commit();            
        } catch (\Exception $e) {
            $transaction->rollBack();
            $message = $e->getMessage();
            if(!$pjax){
                Yii::$app->getSession()->setFlash('error', $message);
            }            
            Yii::error($message, __METHOD__);
        } finally {
            if(!$pjax){
                return $this->redirect($backUrl);
            }else{
                return;
            }
        }
        /*
        $model = $this->findModel($id);
        $billModel = $model->bill;
        $historyModel = $model->billHistory;
        if ($model->delete()) {
            if ($historyModel) {
                $historyModel->delete();
            }
            if ($billModel->status == Bill::BILL_STATUS_PAID) {
                $billModel->changeStatus(Bill::BILL_STATUS_PAYMENT);
            }
        }
        return $this->redirect(FSMBaseModel::getBackUrl());
         * 
         */
    }

}
