<?php

namespace frontend\controllers\bill;

use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\Response;

use common\controllers\FilSMController;
use common\models\mainclass\FSMBaseModel;
use common\models\user\FSMUser;
use common\models\user\FSMProfile;
use common\models\bill\Bill;
use common\models\bill\PaymentOrder;
use common\models\Bank;
use common\models\Files;
use common\models\abonent\Abonent;
use common\printDocs\PrintModule;

use common\assets\ButtonDeleteAsset;

/**
 * PaymentOrderController implements the CRUD actions for PaymentOrder model.
 */
class PaymentOrderController extends FilSMController
{

    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\bill\PaymentOrder';
        $this->defaultSearchModel = 'common\models\bill\search\PaymentOrderSearch';
    }

    /**
     * Lists all models.
     * @return mixed
     */
    public function actionIndex()
    {
        ButtonDeleteAsset::register(Yii::$app->getView());

        $searchModel = new $this->defaultSearchModel;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        $bankList = Bank::getNameArr(['enabled' => true], '', '', function($bank){return $bank['name'].(!empty($bank['swift']) ? ' | '.$bank['swift'] : '');});
        $userList = FSMProfile::getNameArr(['deleted' => false]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'bankList' => $bankList,
            'userList' => $userList,
        ]);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new $this->defaultModel;

        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxValidation($model);
        }

        if ($model->load($requestPost)) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                if(!$model->save()){
                    $this->refresh();
                }elseif(empty($model->number)){
                    $number = date('dmY'). $model->id;
                    $model->updateAttributes(['number' => $number]);
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirect('index');
            }                  
        } else {
            $number = $model->getLastNumber();
            $model->abonent_id = $model->userAbonentId;
            $model->pay_date = date('d-m-Y');
            $model->number = $number;
            
            $isAdmin = FSMUser::getIsPortalAdmin();
            $bankList = Bank::getNameArr(['enabled' => true], '', '', function($bank){return $bank['name'].(!empty($bank['swift']) ? ' | '.$bank['swift'] : '');});
	    
            $data = [
                'model' => $model,
                'bankList' => $bankList,
                'isAdmin' => $isAdmin,
                'isModal' => $isAjax,
            ];
            if ($isAjax) {
                return $this->renderAjax('create', $data);
            }else{
                return $this->render('create', $data);
            }    	    
        }
    }    

    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id, true);
        
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxValidation($model);
        }

        if ($model->load($requestPost)) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                if(!$model->save()){
                    $this->refresh();
                }elseif(empty($model->number)){
                    $number = date('dmY'). $model->id;
                    $model->updateAttributes(['number' => $number]);
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirectToBackUrl($id);
            }            
        } else {
            $this->rememberBackUrl($model->backURL, $id);            
            
            $isAdmin = FSMUser::getIsPortalAdmin();
            $model->pay_date = date('d-m-Y', strtotime($model->pay_date));
            $bankList = Bank::getNameArr(['enabled' => true], '', '', function($bank){return $bank['name'].(!empty($bank['swift']) ? ' | '.$bank['swift'] : '');});
            return $this->render('update', [
                'model' => $model,
                'bankList' => $bankList,
                'isAdmin' => $isAdmin,
            ]);
        }
    }
        
    /**
     * Displays a single model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        ButtonDeleteAsset::register(Yii::$app->getView());
        
        $model = $this->findModel($id, true);
        $billModel = $model->bills;        
        $billModel = !empty($billModel) ? $billModel : [new Bill()];
        
        $billPaymentModel = new \common\models\bill\search\BillPaymentSearch;
        $params = Yii::$app->request->getQueryParams();
        unset($params['id']);
        $params['payment_order_id'] = $id;
        $billPaymentDataProvider = $billPaymentModel->search($params);
        
        $isAdmin = FSMUser::getIsPortalAdmin();
        return $this->render('view', [
            'model' => $model,
            'billPaymentModel' => $billPaymentModel,
            'billPaymentDataProvider' => $billPaymentDataProvider,
            'isAdmin' => $isAdmin,
        ]);
    }     
    
    public function actionSend($id)
    {
        $model = $this->findModel($id, true);
        try{
            if($model->createXML()){
                $model->updateAttributes([
                    'status' => PaymentOrder::EXPORT_STATE_SENT, 
                    'action_time' => date('Y-m-d H:i'),
                    'action_user_id' => Yii::$app->user->identity->getId(),
                ]);                
                Yii::$app->getSession()->setFlash('success', Yii::t('bill', 'The XML file has generated!'));
            }else{
                throw new Exception(Yii::t('bill', 'The export of the XML file was completed with error!'));
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
        } finally {
            return $this->redirect(FSMBaseModel::getBackUrl());
        }         
    }
    
    public function actionCreateXml($id)
    {
        $model = $this->findModel($id, true);
        try{
            if($model->createXML()){
                Yii::$app->getSession()->setFlash('success', Yii::t('bill', 'The XML file has been updated!'));
            }else{
                throw new Exception(Yii::t('bill', 'XML file update failed!'));
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
        } finally {
            return $this->redirect(FSMBaseModel::getBackUrl());
        }         
    }
    
    /**
     * Deletes an existing single model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id, true);
        $paymentList = $model->billPayments;
        $result = parent::actionDelete($id);
        foreach ($paymentList as $payment) {
            $bill = $payment->bill;
            $bill->changeStatus(Bill::BILL_STATUS_PREP_PAYMENT);
        }
        return $result;
    }      
    
    public function actionDownloadXml($id) {
        $model = $this->findModel($id);
        $fileModel = $model->file;
        $filename = $fileModel->filename;
        $filename = str_replace('-', '', substr($filename, 2, 12)).'.xml';
        $filepath = $fileModel->filepath;
        
        if (file_exists($filepath)) {
            return Yii::$app->response->sendFile($filepath, $filename);
        }else{
            $message = Yii::t('bill', 'XML file does not exist!');
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
            return $this->redirect(FSMBaseModel::getBackUrl());
        }
    }     
}