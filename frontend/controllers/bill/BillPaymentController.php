<?php

namespace frontend\controllers\bill;

use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\filters\AccessControl;

use common\assets\ButtonDeleteAsset;
use common\components\FSMAccessHelper;
use common\controllers\FilSMController;
use common\models\bill\BillHistory;
use common\models\bill\Bill;
use common\models\bill\BillPayment;
use common\models\bill\PaymentOrder;
use common\models\Bank;
use common\models\Valuta;
use common\models\mainclass\FSMBaseModel;
use common\models\user\FSMUser;
use frontend\assets\BillPaymentUIAsset;

/**
 * BillPaymentController implements the CRUD actions for BillPayment model.
 */
class BillPaymentController extends FilSMController
{

    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\bill\BillPayment';
        $this->defaultSearchModel = 'common\models\bill\search\BillPaymentSearch';
        $this->pjaxIndex = true;
    }
    
    public function actionAjaxConfirmPayment($id)
    {
        $model = $this->findModel($id);
        $billModel = $model->bill;
        $billModel->summa = number_format($model->summa, 2, '.', '') . (!empty($model->valuta_id) ? ' ' . $model->valuta->name : '');

        $historyModel = new BillHistory();
        $historyModel->bill_id = $billModel->id;
        $historyModel->action_id = BillHistory::BillHistory_ACTIONS['ACTION_STATUS_UP_PAID'];
        $historyModel->create_time = date('d-m-Y H:i:s');
        $historyModel->create_user_id = Yii::$app->user->id;

        $modelArr = [
            'BillPayment' => $model,
        ];

        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxMultipleValidation($modelArr);
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $historyModel->create_time = date('Y-m-d H:i:s', strtotime($historyModel->create_time));
                $historyModel->comment = Yii::t('common', 'Sum') . ': ' . $billModel->summa;
                if (!$historyModel->save()) {
                    throw new Exception('Unable to save data! ' . $historyModel->errorMessage);
                }
                $model->paid_date = date('Y-m-d', strtotime($model->paid_date));
                $model->confirmed = date('Y-m-d');
                if (!$model->save()) {
                    throw new Exception('Unable to save data! ' . $model->errorMessage);
                }
                if (!$billModel->changeStatus(Bill::BILL_STATUS_PAID, ['paid_date' => date('Y-m-d', strtotime($model->paid_date))])) {
                    throw new Exception('Unable to save data! ' . $billModel->errorMessage);
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return 'reload';
            }
        } else {
            $model->paid_date = date('d-m-Y');
            return $this->renderAjax('confirm-payment', [
                'model' => $model,
                'billModel' => $billModel,
                'historyModel' => $historyModel,
                'isModal' => true,
            ]);
        }
    }
    
    /**
     * Lists all models.
     * @return mixed
     */
    public function actionIndex() {
        ButtonDeleteAsset::register(Yii::$app->getView());
        
        $searchModel = new $this->defaultSearchModel;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $bankList = Bank::getNameArr(['enabled' => true], '', '', function($bank){return $bank['name'].(!empty($bank['swift']) ? ' | '.$bank['swift'] : '');});
        
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'bankList' => $bankList,
        ]);
    }
    
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAjaxCreate($id) {
        $model = new $this->defaultModel;
        $historyModel = new BillHistory();
        $billModel = !empty($id) ? Bill::findOne($id) : new Bill();

        $model->bill_id = !empty($billModel->id) ? $billModel->id : null;

        $historyModel->bill_id = !empty($id) ? $id : null;
        $historyModel->action_id = BillHistory::BillHistory_ACTIONS['ACTION_STATUS_UP_PAYMENT'];
        $historyModel->create_time = date('d-m-Y H:i:s');
        $historyModel->create_user_id = Yii::$app->user->id;

        $modelArr = [
            'BillPayment' => $model,
            'BillHistory' => $historyModel,
        ];
        
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxMultipleValidation($modelArr);
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $historyModel->create_time = date('Y-m-d H:i:s', strtotime($historyModel->create_time));
                $historyModel->comment .= 
                    (!empty($historyModel->comment) ? "\n" : '').Yii::t('common', 'Sum').': '.number_format($model->summa, 2, '.', ' ').
                        ($model->rate != 1 ? "\n".$model->getAttributeLabel('rate').': '.$model->rate.' | '.
                            $model->getAttributeLabel('summa_eur').': '.number_format($model->summa_eur, 2, '.', ' ') : '');
                if (!$historyModel->save()) {
                    throw new Exception('Unable to save data! '.$historyModel->errorMessage);
                }
                $model->bill_history_id = $historyModel->id;
                $model->confirmed = !empty($model->paid_date) ? date('Y-m-d') : null;
                
                if ($model->save()) {
                    if(empty($model->paid_date)){
                        $billModel->changeStatus(Bill::BILL_STATUS_PAYMENT);
                    }else{
                        $historyModel = new BillHistory();
                        $historyModel->bill_id = !empty($id) ? $id : null;
                        $historyModel->action_id = BillHistory::BillHistory_ACTIONS['ACTION_STATUS_UP_PAID'];
                        $historyModel->create_time = date('d-m-Y H:i:s');
                        $historyModel->create_user_id = Yii::$app->user->id;
                        $historyModel->comment = Yii::t('common', 'Sum').': '.number_format($model->summa, 2, '.', ' ').
                            ($model->rate != 1 ? "\n".$model->getAttributeLabel('rate').': '.$model->rate.' | '.
                                $model->getAttributeLabel('summa_eur').': '.$model->summa_eur : '');
                        $historyModel->save();

                        $paid_date = date('Y-m-d', strtotime($model->paid_date));
                        $billModel = Bill::findOne($id);
                        $billModel->changeStatus(Bill::BILL_STATUS_PAID, ['paid_date' => $paid_date]);
                    }
                } else {
                    throw new Exception('Unable to save data! '.$model->errorMessage);
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return 'reload';
            }
        } else {
            $model->summa = number_format($billModel->billNotPaidSumma, 2, '.', '');
            $model->rate = $billModel->rate;
            $model->valuta_id = $billModel->valuta_id;
            $paymentOrderList = PaymentOrder::getNameArr(['status' => PaymentOrder::EXPORT_STATE_PREPARE]);
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            
            if(!empty($id)){
                $bankFromList = $billModel->agreement->secondClient->clientActiveBanks;
                $bankToList = $billModel->agreement->firstClient->clientActiveBanks;
                
                $thirdClientBankList = !empty($billModel->agreement->third_client_id) ? $billModel->agreement->thirdClient->clientActiveBanks : null;
                 
                $arr = [];
                foreach ($bankFromList as $bank) {
                    $bankModel = $bank->bank;
                    $arr[$bank->bank_id] = (!empty($thirdClientBankList) ? $billModel->agreement->secondClient->name.' | ': '').$bankModel->name.(!empty($bankModel->swift) ? ' | '.$bankModel->swift : '');
                }
                if(!empty($thirdClientBankList)){
                    foreach ($thirdClientBankList as $bank) {
                        $bankModel = $bank->bank;
                        $arr[$bank->bank_id] = $billModel->agreement->thirdClient->name.' | '.$bankModel->name.(!empty($bankModel->swift) ? ' | '.$bankModel->swift : '');
                    }
                }
                $bankFromList = $arr;
                
                $arr = [];
                foreach ($bankToList as $bank) {
                    $bankModel = $bank->bank;
                    $arr[$bank->bank_id] = $bankModel->name.(!empty($bankModel->swift) ? ' | '.$bankModel->swift : '');
                }
                $bankToList = $arr;
                
                $model->from_bank_id = (!empty($billModel->second_client_bank_id) ? $billModel->secondClientBank->bank_id : null);
                $model->to_bank_id = $billModel->firstClientBank->bank_id;
            }else{
                $bankFromList = $bankToList = Bank::getNameArr(['enabled' => true], '', '', function($bank){return $bank['name'].(!empty($bank['swift']) ? ' | '.$bank['swift'] : '');});
            }
            
            return $this->renderAjax('create', [
                'model' => $model,
                'historyModel' => $historyModel,
                'paymentOrderList' => $paymentOrderList,
                'bankFromList' => $bankFromList,
                'bankToList' => $bankToList,
                'valutaList' => $valutaList,
                'isModal' => true,
            ]);
        }
    }
   
    public function actionAjaxCreateMany($ids) {
        if(empty($ids)){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return 'reload';
        }
        $idArr = explode(',', $ids);
        $model = [];
        foreach ($idArr as $index => $id) {
            $model[] = new $this->defaultModel;      
            $model[$index]->bill_id = $id;
        }
        
        $paymentOrderModel = new PaymentOrder();
        
        $historyModel = new BillHistory();
        $historyModel->action_id = BillHistory::BillHistory_ACTIONS['ACTION_STATUS_UP_PAYMENT'];
        $historyModel->create_time = date('d-m-Y H:i:s');
        $historyModel->create_user_id = Yii::$app->user->id;

        $modelArr = [
            'BillPayment' => $model,
            'BillHistory' => $historyModel,
        ];
        
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (isset($requestPost['close'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return 'reload';
        }        
        if (!empty($requestPost['new_payment_order'])) {
            $modelArr = ArrayHelper::merge($modelArr, ['PaymentOrder' => $paymentOrderModel]);
        }
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxMultipleValidation($modelArr);
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                if (empty($requestPost['new_payment_order'])) {
                    $paymentOrderId = $requestPost['PaymentOrder']['id'];
                    if(empty($paymentOrderId)){
                        throw new Exception('Payment order not defined!');
                    }                
                }else{
                    $paymentOrderModel->abonent_id = $paymentOrderModel->userAbonentId;
                    if(!$paymentOrderModel->save()){
                        throw new Exception('Unable to save data! '.$paymentOrderModel->errorMessage);
                    }
                    $paymentOrderId = $paymentOrderModel->id;
                }
                $comment = $historyModel->comment;
                foreach ($model as $billPayment) {
                    $historyModel->id = null;
                    $historyModel->setIsNewRecord(true);
                    $historyModel->bill_id = $billPayment['bill_id'];
                    $historyModel->create_time = date('Y-m-d H:i:s', strtotime($historyModel->create_time));
                    $historyModel->comment = (!empty($comment) ? $comment."\n" : '').Yii::t('common', 'Sum').': '.number_format($billPayment['summa'], 2, '.', ' ');
                    if (!$historyModel->save()) {
                        throw new Exception('Unable to save data! '.$historyModel->errorMessage);
                    }
                    
                    $payment = new $this->defaultModel;
                    $payment->bill_history_id = $historyModel->id;
                    $payment->payment_order_id = $paymentOrderId;
                    $payment->bill_id = $billPayment['bill_id'];
                    $payment->summa = $billPayment['summa'];
                    $payment->rate = $billPayment['rate'];
                    $payment->valuta_id = $billPayment['valuta_id'];
                    $payment->combine = $billPayment['combine'];
                    $payment->need_agreement = $billPayment['need_agreement'];
                    if ($payment->save()) {
                        $billModel = Bill::findOne($payment->bill_id);
                        $billModel->changeStatus(Bill::BILL_STATUS_PAYMENT);
                    } else {
                        throw new Exception('Unable to save data! '.$payment->errorMessage);
                    }
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                if (isset($requestPost['download_xml']) && !empty($paymentOrderModel->id)) {
                    if($paymentOrderModel->createXML()){
                        return $this->redirect(['payment-order/download-xml', 'id' => $paymentOrderModel->id]);
                    }
                }else{
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return 'reload';
                }
            }
        } else {
            BillPaymentUIAsset::register(Yii::$app->getView());
            
            $model = [];
            $idsArr = explode(',', $ids);
            foreach ($idsArr as $id) {
                $bill = Bill::findOne($id);
                $payment = new $this->defaultModel;
                $payment->payment_order_id = null;
                $payment->bill_id = $id;
                $payment->bill_number = $bill->doc_number;
                $payment->first_client_id = $bill->firstClient->id;
                $payment->first_client_name = $bill->firstClient->name;
                $payment->summa = number_format($bill->billNotPaidSumma, 2, '.', '');
                $payment->rate = $bill->rate;
                $payment->valuta_id = $bill->valuta_id;
                
                $model[] =  $payment;
            }
            
            $number = $paymentOrderModel->getLastNumber();
            $paymentOrderModel->pay_date = date('d-m-Y');
            $paymentOrderModel->number = $number;
            
            $isAdmin = FSMUser::getIsPortalAdmin();
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            $paymentOrderList = PaymentOrder::getNameArr(['status' => PaymentOrder::EXPORT_STATE_PREPARE]);
            $bankList = Bank::getNameArr($arrToFind, '', '', function($bank){return $bank['name'].(!empty($bank['swift']) ? ' | '.$bank['swift'] : '');});
            return $this->renderAjax('create_many', [
            //return $this->render('create_many', [
                'model' => $model,
                'historyModel' => $historyModel,
                'paymentOrderModel' => $paymentOrderModel,
                'paymentOrderList' => $paymentOrderList,
                'valutaList' => $valutaList,
                'bankList' => $bankList,
                'isAdmin' => $isAdmin,
                'isModal' => true,
            ]);
        }
    }

    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionAjaxUpdate($id) {
        $payment_order_id = !empty($_GET['payment_order_id']) ? $_GET['payment_order_id'] : null;
        $model = $this->findModel($id, true);
        $billModel = $model->bill;
        
        $historyModel = new BillHistory();
        $historyModel->bill_id = !empty($billModel->id) ? $billModel->id : null;
        $historyModel->action_id = BillHistory::BillHistory_ACTIONS['ACTION_EDIT_PAYMENT'];
        $historyModel->create_time = date('d-m-Y H:i:s');
        $historyModel->create_user_id = Yii::$app->user->id;
        
        //$historyModel = $model->billHistory;
        //$historyModel->create_time = date('d-m-Y H:i:s');
        //$historyModel->create_user_id = Yii::$app->user->id;

        $modelArr = [
            'BillPayment' => $model,
            'BillHistory' => $historyModel,
        ];
        
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxMultipleValidation($modelArr);
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $historyModel->comment .= 
                    (!empty($historyModel->comment) ? "\n" : '').
                        Yii::t('common', 'Sum').': '.number_format($model->summa, 2, '.', ' ').
                        ($model->rate != 1 ? "\n".$model->getAttributeLabel('rate').': '.$model->rate.' | '.
                                $model->getAttributeLabel('summa_eur').': '.number_format($model->summa_eur, 2, '.', ' ') : '');
                
                $historyModel->create_time = date('Y-m-d H:i:s', strtotime($historyModel->create_time));
                if (!$historyModel->save()) {
                    throw new Exception('Unable to save data! '.$historyModel->errorMessage);
                }
                
                $model->bill_history_id = $historyModel->id;
                $model->confirmed = !empty($model->paid_date) ? date('Y-m-d') : null;
                
                if ($model->save()) {
                    if(empty($model->paid_date)){
                        $billModel->changeStatus(Bill::BILL_STATUS_PAYMENT);
                    }else{
                        $paid_date = date('Y-m-d', strtotime($model->paid_date));
                        $billModel->changeStatus(Bill::BILL_STATUS_PAID, ['paid_date' => $paid_date]);
                    }
                } else {
                    throw new Exception('Unable to save data! '.$model->errorMessage);
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return 'reload';
            }
        } else {
            $isAdmin = FSMUser::getIsPortalAdmin();
            $paymentOrderList = PaymentOrder::getNameArr(['status' => PaymentOrder::EXPORT_STATE_PREPARE]);
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            
            $bankFromList = $billModel->agreement->secondClient->clientActiveBanks;
            $bankToList = $billModel->agreement->firstClient->clientActiveBanks;

            $arr = [];
            foreach ($bankFromList as $bank) {
                $bankModel = $bank->bank;
                $arr[$bank->bank_id] = $bankModel->name.(!empty($bankModel->swift) ? ' | '.$bankModel->swift : '');
            }
            $bankFromList = $arr;

            $arr = [];
            foreach ($bankToList as $bank) {
                $bankModel = $bank->bank;
                $arr[$bank->bank_id] = $bankModel->name.(!empty($bankModel->swift) ? ' | '.$bankModel->swift : '');
            }
            $bankToList = $arr;

            if(empty($payment_order_id) && empty($model->payment_order_id)){
                $model->from_bank_id = !empty($model->from_bank_id) ? $model->from_bank_id : 
                    (!empty($billModel->second_client_bank_id) ? $billModel->secondClientBank->bank_id : null);
                $model->to_bank_id = !empty($model->to_bank_id) ? $model->to_bank_id : $billModel->firstClientBank->bank_id;
            }
            $model->paid_date = date('d-m-Y', strtotime($model->paid_date));
            $model->confirmed = date('d-m-Y', strtotime($model->confirmed));
                
            return $this->renderAjax('update', [
                'model' => $model,
                'historyModel' => $historyModel,
                'paymentOrderList' => $paymentOrderList,
                'bankFromList' => $bankFromList,
                'bankToList' => $bankToList,
                'valutaList' => $valutaList,
                'isAdmin' => $isAdmin,
                'isModal' => true,
            ]);
        }
    } 
    
    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id, true);
        $billModel = $model->bill;
        
        $historyModel = new BillHistory();
        $historyModel->bill_id = !empty($billModel->id) ? $billModel->id : null;
        $historyModel->action_id = BillHistory::BillHistory_ACTIONS['ACTION_EDIT_PAYMENT'];
        $historyModel->create_time = date('d-m-Y H:i:s');
        $historyModel->create_user_id = Yii::$app->user->id;
        
        $modelArr = [
            'BillPayment' => $model,
            'BillHistory' => $historyModel,
        ];
        
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxMultipleValidation($modelArr);
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $historyModel->comment .= 
                    (!empty($historyModel->comment) ? "\n" : '').
                        Yii::t('common', 'Sum').': '.number_format($model->summa, 2, '.', ' ').
                        ($model->rate != 1 ? "\n".$model->getAttributeLabel('rate').': '.$model->rate.' | '.
                                $model->getAttributeLabel('summa_eur').': '.number_format($model->summa_eur, 2, '.', ' ') : '');
                
                $historyModel->create_time = date('Y-m-d H:i:s', strtotime($historyModel->create_time));
                if (!$historyModel->save()) {
                    throw new Exception('Unable to save data! '.$historyModel->errorMessage);
                }
                
                $model->bill_history_id = $historyModel->id;
                $model->confirmed = !empty($model->paid_date) ? date('Y-m-d') : null;
                
                if ($model->save()) {
                    if(empty($model->paid_date)){
                        $billModel->changeStatus(Bill::BILL_STATUS_PAYMENT);
                    }else{
                        $paid_date = date('Y-m-d', strtotime($model->paid_date));
                        $billModel->changeStatus(Bill::BILL_STATUS_PAID, ['paid_date' => $paid_date]);
                    }
                } else {
                    throw new Exception('Unable to save data! '.$model->errorMessage);
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirectToBackUrl($id);              
            }
        } else {
            $this->rememberBackUrl($model->backURL, $id);
            
            $isAdmin = FSMUser::getIsPortalAdmin();
            $paymentOrderList = PaymentOrder::getNameArr(['status' => PaymentOrder::EXPORT_STATE_PREPARE]);
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            
            $bankFromList = $billModel->agreement->secondClient->clientActiveBanks;
            $bankToList = $billModel->agreement->firstClient->clientActiveBanks;

            $arr = [];
            foreach ($bankFromList as $bank) {
                $bankModel = $bank->bank;
                $arr[$bank->bank_id] = $bankModel->name.(!empty($bankModel->swift) ? ' | '.$bankModel->swift : '');
            }
            $bankFromList = $arr;

            $arr = [];
            foreach ($bankToList as $bank) {
                $bankModel = $bank->bank;
                $arr[$bank->bank_id] = $bankModel->name.(!empty($bankModel->swift) ? ' | '.$bankModel->swift : '');
            }
            $bankToList = $arr;

            if(empty($payment_order_id) && empty($model->payment_order_id)){
                $model->from_bank_id = !empty($model->from_bank_id) ? $model->from_bank_id :  
                    (!empty($billModel->second_client_bank_id) ? $billModel->secondClientBank->bank_id : null);
                $model->to_bank_id = !empty($model->to_bank_id) ? $model->to_bank_id : $billModel->firstClientBank->bank_id;
            }
            $model->paid_date = date('d-m-Y', strtotime($model->paid_date));
                
            return $this->render('update', [
                'model' => $model,
                'historyModel' => $historyModel,
                'paymentOrderList' => $paymentOrderList,
                'bankFromList' => $bankFromList,
                'bankToList' => $bankToList,
                'valutaList' => $valutaList,
                'isAdmin' => $isAdmin,
            ]);
        }
    }    
    
    /**
     * Deletes an existing single model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            $model = $this->findModel($id, true);
            $backUrl = $model->backURL;
            $backUrl = is_array($backUrl) ? $backUrl[0] : $backUrl;
            $pjax = $this->pjaxIndex && (strpos($backUrl, 'bill-payment/view') === false);
            $billModel = $model->bill;
            if($model->delete()){
                $paymentList = $billModel->billPayments;
                if(empty($paymentList)){
                    $billModel->changeStatus(Bill::BILL_STATUS_PREP_PAYMENT);
                }            
            }
            $transaction->commit();            
        } catch (\Exception $e) {
            $transaction->rollBack();
            $message = $e->getMessage();
            if(!$pjax){
                Yii::$app->getSession()->setFlash('error', $message);
            }            
            Yii::error($message, __METHOD__);
        } finally {
            if(!$pjax){
                return $this->redirect(['index']);
            }else{
                return;
            }
        }          
    }    
}