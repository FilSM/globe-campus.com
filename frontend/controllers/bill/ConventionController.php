<?php

namespace frontend\controllers\bill;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

use common\controllers\FilSMAttachmentController;
use common\components\FSMAccessHelper;
use common\models\user\FSMUser;
use common\models\Files;
use common\models\Valuta;
use common\models\mainclass\FSMBaseModel;
use common\models\bill\ConventionBill;
use common\models\bill\ConventionAttachment;
use common\models\client\Client;
use common\assets\ButtonDeleteAsset;
use frontend\assets\ConventionUIAsset;

/**
 * ConventionController implements the CRUD actions for Convention model.
 */
class ConventionController extends FilSMAttachmentController
{
    public $attachmentDir = 'convention';
    public $attachmentClass = 'common\models\bill\ConventionAttachment';
    public $foreignKeyField = 'convention_id';
    
    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\bill\Convention';
        $this->defaultSearchModel = 'common\models\bill\search\ConventionSearch';
    }

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [];
        $_get = Yii::$app->request->get();
        $id = isset($_get['id']) ? $_get['id'] : null;
        $model = !empty($id) ? $this->findModel($id) : null;
        $behaviors = ArrayHelper::merge(
            $behaviors, [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'actions' => [
                                'index',
                            ],
                            'allow' => FSMAccessHelper::checkRoute('/convention/*'),
                        ],
                        [
                            'actions' => [
                                'view', 
                            ],
                            'allow' => FSMAccessHelper::can('viewConvention', $model),
                        ],
                        [
                            'actions' => [
                                'create', 
                            ],
                            'allow' => FSMAccessHelper::can('createConvention'),
                        ],
                        [
                            'actions' => ['update', 'attachment-delete'],
                            'allow' => FSMAccessHelper::can('updateConvention', $model),
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => FSMAccessHelper::can('deleteConvention', $model),
                        ],
                    ],
                ],                
            ]
        );
        return $behaviors;
    }
    
    /**
     * Lists all models.
     * @return mixed
     */
    public function actionIndex() {
        ButtonDeleteAsset::register(Yii::$app->getView());
        
        $searchModel = new $this->defaultSearchModel;
        $params = Yii::$app->request->getQueryParams();
        $params['deleted'] = (empty($params) || empty($params['ConventionSearch'])) ?
            0 : (int)!empty($params['ConventionSearch']['deleted']);

        $dataProvider = $searchModel->search($params);

        $isAdmin = FSMUser::getIsPortalAdmin();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'isAdmin' => $isAdmin,
        ]);
    }
    
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new $this->defaultModel;
        $filesModel = [new Files()];

        $modelArr = [
            'Convention' => $model,
        ];
        
        // ajax validation
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && $isAjax) {
            return $this->performAjaxMultipleValidation($modelArr);            
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                if (!$model->save()) {
                    throw new Exception('Unable to save data! '.$model->errorMessage);
                }

                $this->createAttachment($model, $filesModel);

                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirect('index');
            }                
        } else {
            ConventionUIAsset::register(Yii::$app->getView());

            $model->abonent_id = $model->userAbonentId;             
            $model->doc_date = date('d-m-Y');
            
            $isAdmin = FSMUser::getIsPortalAdmin();
            $clientList = Client::getNameArr(['client.deleted' => 0]);
            
            return $this->render('create', [
                'model' => $model,
                'firstClientModel' => new Client(),
                'secondClientModel' => new Client(),
                'thirdClientModel' => new Client(),
                'filesModel' => $filesModel,
                'clientList' => $clientList,
                'isAdmin' => $isAdmin,
            ]);
        }
    }    

    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $isAdmin = FSMUser::getIsPortalAdmin();
        
        $model = $this->findModel($id, true);
        
        $filesModel = $model->attachmentFiles;
        $filesModel = (!empty($filesModel) ? $filesModel : [new Files()]);
        
        $modelArr = [
            'Convention' => $model,
        ];
        
        // ajax validation
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && $isAjax) {
            return $this->performAjaxMultipleValidation($modelArr); 
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                if (!$model->save()) {
                    throw new Exception('Unable to save data! '.$model->errorMessage);
                }

                $flag = $this->updateAttacment($model, $filesModel);
                
                if (!$flag) {
                    $transaction->rollBack();
                    throw new Exception('Unable to save data!');
                }else{
                    $transaction->commit();
                }          
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirectToBackUrl($id);              
            }                
        } else {
            $this->rememberBackUrl($model->backURL, $id);
            Yii::$app->session->remove('file_delete');
            
            ConventionUIAsset::register(Yii::$app->getView());

            $model->doc_date = date('d-m-Y', strtotime($model->doc_date));

            $isAdmin = FSMUser::getIsPortalAdmin();
            $clientList = Client::getNameArr(['client.deleted' => 0]);
            $firstClientModel = $model->firstClient;
            $secondClientModel = $model->secondClient;
            $thirdClientModel = $model->thirdClient;
            
            return $this->render('update', [
                'model' => $model,
                'firstClientModel' => $firstClientModel,
                'secondClientModel' => $secondClientModel,
                'thirdClientModel' => $thirdClientModel,
                'filesModel' => $filesModel,
                'clientList' => $clientList,
                'isAdmin' => $isAdmin,
            ]);
        }
    }
    
    /**
     * Displays a single model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        ButtonDeleteAsset::register(Yii::$app->getView());
        
        $model = $this->findModel($id, true);
        $firstClientModel = $model->firstClient;
        $firstClientModel = (!empty($firstClientModel) ? $firstClientModel : new Client());
        $secondClientModel = $model->secondClient;
        $secondClientModel = (!empty($secondClientModel) ? $secondClientModel : new Client());
        $thirdClientModel = $model->thirdClient;
        $thirdClientModel = (!empty($thirdClientModel) ? $thirdClientModel : new Client());

        $filesModel = $model->attachmentFiles;
        $filesModel = (!empty($filesModel) ? $filesModel : [new Files()]);
        
        $isAdmin = FSMUser::getIsPortalAdmin();
        return $this->render('view', [
            'model' => $model,
            'firstClientModel' => $firstClientModel,
            'secondClientModel' => $secondClientModel,
            'thirdClientModel' => $thirdClientModel,
            'filesModel' => $filesModel,
            'isAdmin' => $isAdmin,
            'time' => date('H:i:s'),
        ]);
    }      
}