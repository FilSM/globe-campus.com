<?php

namespace frontend\controllers\bill;

use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\filters\AccessControl;

use common\controllers\FilSMController;
use common\components\FSMAccessHelper;
use common\models\mainclass\FSMBaseModel;
use common\models\user\FSMUser;
use common\models\user\FSMProfile;
use common\models\Valuta;
use common\models\abonent\Abonent;
use common\models\abonent\AbonentBillTemplate;
use common\models\abonent\AbonentAgreement;
use common\models\client\Project;
use common\models\client\Agreement;
use common\models\client\Client;
use common\models\client\ClientRole;
use common\models\bill\BillTemplate;
use common\models\bill\BillTemplateProduct;
use common\models\bill\FirstBillTemplatePerson;
use common\models\bill\SecondBillTemplatePerson;
use common\models\Product;
use common\models\Measure;
use common\models\Language;
use common\assets\ButtonDeleteAsset;
use frontend\assets\BillUIAsset;

/**
 * BillTemplateController implements the CRUD actions for BillTemplate model.
 */
class BillTemplateController extends FilSMController
{

    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\bill\BillTemplate';
        $this->defaultSearchModel = 'common\models\bill\search\BillTemplateSearch';
        $this->pjaxIndex = true;
    }

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [];
        $_get = Yii::$app->request->get();
        $id = isset($_get['id']) ? $_get['id'] : null;
        $model = !empty($id) ? $this->findModel($id) : null;
        $behaviors = ArrayHelper::merge(
            $behaviors, [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => FSMAccessHelper::checkRoute('/bill-template/*'),
                        ],
                        [
                            'actions' => [
                                'view', 
                            ],
                            'allow' => FSMAccessHelper::can('viewBill', $model),
                        ],
                        [
                            'actions' => [
                                'create', 
                                'create-bill', 
                            ],
                            'allow' => FSMAccessHelper::can('createBill'),
                        ],
                        [
                            'actions' => ['update'],
                            'allow' => FSMAccessHelper::can('updateBill', $model),
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => FSMAccessHelper::can('deleteBill', $model),
                        ],
                    ],
                ],                
            ]
        );
        return $behaviors;
    }    
    
    /**
     * Lists all models.
     * @return mixed
     */
    public function actionIndex() {
        BillUIAsset::register(Yii::$app->getView());
        ButtonDeleteAsset::register(Yii::$app->getView());
        
        $searchModel = new $this->defaultSearchModel;
        $params = Yii::$app->request->getQueryParams();
        $dataProvider = $searchModel->search($params);

        $filter = isset($params['BillTemplateSearch']) ? $params['BillTemplateSearch'] : [];
        $isAdmin = FSMUser::getIsPortalAdmin();
        $projectList = Project::getNameArr(['project.deleted' => 0]);
        $agreementFilter = [
            'agreement.deleted' => 0,
        ];
        if(!empty($filter['project_id'])){
            $agreementFilter['project_id'] = $filter['project_id'];
        }
        $agreementList = Agreement::getNameArr($agreementFilter);
        $clientList = Client::getNameArr(['client.deleted' => 0]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'projectList' => $projectList,
            'agreementList' => $agreementList,
            'clientList' => $clientList,
            'isAdmin' => $isAdmin,
        ]);
    }
    
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new $this->defaultModel;
        $agreementModel = new Agreement();
        $abonentModel = new AbonentBillTemplate();
        
        $modelArr = [
            'BillTemplate' => $model,
            'Agreement' => $agreementModel,
            'AbonentBillTemplate' => $abonentModel,
        ];
        
        // ajax validation
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && $isAjax) {
            $ajaxModelArr = $modelArr;
            $billProductModel = FSMBaseModel::createMultiple(BillTemplateProduct::class);
            FSMBaseModel::loadMultiple($billProductModel, $requestPost);
            foreach ($billProductModel as $index => $billProduct) {
                if(empty($billProduct->product_id) && empty($billProduct->amount) && empty($billProduct->price)){
                    unset($billProductModel[$index]);
                    continue;
                }
            }
            if (!empty($billProductModel)) {
                $ajaxModelArr = ArrayHelper::merge($modelArr, ['BillTemplateProduct' => $billProductModel]);
            }
            
            $billPersonModel = FSMBaseModel::createMultiple(FirstBillTemplatePerson::class);
            FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
            foreach ($billPersonModel as $index => $billPerson) {
                if(empty($billPerson->client_person_id)){
                    unset($billPersonModel[$index]);
                    continue;
                }
            }
            if (!empty($billPersonModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['FirstBillTemplatePerson' => $billPersonModel]);
            }
            
            $billPersonModel = FSMBaseModel::createMultiple(SecondBillTemplatePerson::class);
            FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
            foreach ($billPersonModel as $index => $billPerson) {
                if(empty($billPerson->client_person_id)){
                    unset($billPersonModel[$index]);
                    continue;
                }
            }
            if (!empty($billPersonModel)) {
                $ajaxModelArr = ArrayHelper::merge($ajaxModelArr, ['SecondBillTemplatePerson' => $billPersonModel]);
            }
                        
            return $this->performAjaxMultipleValidation($ajaxModelArr);            
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                if(empty($model->agreement_id)){
                    throw new Exception('Agreement was not defined!');
                }
                
                if (!$model->save()) {
                    throw new Exception('Unable to save data! '.$model->errorMessage);
                }
                $abonentModel->abonent_id = $model->userAbonentId;
                $abonentModel->bill_template_id = $model->id;
                $abonentModel->save();

                $billProductModel = FSMBaseModel::createMultiple(BillTemplateProduct::class);
                FSMBaseModel::loadMultiple($billProductModel, $requestPost);            
                foreach ($billProductModel as $index => $billProduct) {
                    if(empty($billProduct->product_id) && empty($billProduct->amount) && empty($billProduct->price)){
                        unset($billProductModel[$index]);
                        continue;
                    }
                    $billProduct->bill_template_id = $model->id;
                    $billProduct->price_eur = $billProduct->price / $model->rate;
                    $billProduct->summa_eur = $billProduct->summa / $model->rate;
                    $billProduct->summa_vat_eur = $billProduct->summa_vat / $model->rate;
                    $billProduct->total_eur = $billProduct->total / $model->rate;

                    if($model->according_contract){
                        $billProduct->product_id = null;
                    }
                }

                if(!empty($billProductModel)){
                    if (FSMBaseModel::validateMultiple($billProductModel)) {
                        foreach ($billProductModel as $billProduct) {
                            if (!$billProduct->save(false)) {
                                throw new Exception('Unable to save data! '.$billProduct->errorMessage);
                                break;
                            }
                        }
                    }else{
                        throw new Exception('Unable to save data! '.$billProductModel->errorMessage);
                    }
                    
                }
                if(empty($model->e_signing)){
                    $isEmptyFirstPersonList = $isEmptySecondPersonList = false;

                    $billPersonModel = FSMBaseModel::createMultiple(FirstBillTemplatePerson::class);
                    FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
                    foreach ($billPersonModel as $index => $billPerson) {
                        if(empty($billPerson->client_person_id)){
                            unset($billPersonModel[$index]);
                            continue;
                        }
                        $billPerson->bill_template_id = $model->id;                      
                        $billPerson->person_order = 1;                      
                    }
                    if(!empty($billPersonModel)){
                        if (FSMBaseModel::validateMultiple($billPersonModel)) {
                            foreach ($billPersonModel as $billPerson) {
                                if (!$billPerson->save(false)) {
                                    throw new Exception('Unable to save data! '.$billPerson->errorMessage);
                                    break;
                                }
                            }
                        }else{
                            throw new Exception('Unable to save data! '.$billPersonModel->errorMessage);
                        }
                    }else{
                        $isEmptyFirstPersonList = true;
                    }

                    $billPersonModel = FSMBaseModel::createMultiple(SecondBillTemplatePerson::class);
                    FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
                    foreach ($billPersonModel as $index => $billPerson) {
                        if(empty($billPerson->client_person_id)){
                            unset($billPersonModel[$index]);
                            continue;
                        }
                        $billPerson->bill_template_id = $model->id;                      
                        $billPerson->person_order = 2;                      
                    }
                    if(!empty($billPersonModel)){
                        if (FSMBaseModel::validateMultiple($billPersonModel)) {
                            foreach ($billPersonModel as $billPerson) {
                                if (!$billPerson->save(false)) {
                                    throw new Exception('Unable to save data! '.$billPerson->errorMessage);
                                    break;
                                }
                            }
                        }else{
                            throw new Exception('Unable to save data! '.$billPersonModel->errorMessage);
                        }
                    }else{
                        $isEmptySecondPersonList = true;
                    }
                    if($isEmptyFirstPersonList && $isEmptySecondPersonList){
                        $model->updateAttributes(['e_signing' => true]);
                    }                        
                }

                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirect('index');
            }                
        } else {
            BillUIAsset::register(Yii::$app->getView());

            $model->summa = '0.00';
            $model->valuta_id = Valuta::VALUTA_DEFAULT;
            $model->periodical_day = 25;
            $model->periodicity = BillTemplate::BILL_PERIODICITY_MONTH;
            
            $billProductModel = [new BillTemplateProduct()];
            $billFirstPersonModel = [new FirstBillTemplatePerson(), new FirstBillTemplatePerson()];
            $billSecondPersonModel = [new SecondBillTemplatePerson(), new SecondBillTemplatePerson()];

            $isAdmin = FSMUser::getIsPortalAdmin();
            $projectList = Project::getNameArr(['project.deleted' => 0]);
            $agreementList = Agreement::getNameArr(['agreement.deleted' => 0]);
            $clientRoleList = ClientRole::getNameArr();
            $languageList = Language::getEnabledLanguageList();            
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            $productList = Product::getNameArr();
            $measureList = Measure::getNameArr();
            return $this->render('create', [
                'model' => $model,
                'abonentModel' => $abonentModel,
                'agreementModel' => $agreementModel,
                'firstClientModel' => new Client(),
                'secondClientModel' => new Client(),
                'projectList' => $projectList,
                'agreementList' => $agreementList,
                'clientRoleList' => $clientRoleList,
                'languageList' => $languageList,
                'valutaList' => $valutaList,
                'billProductModel' => $billProductModel,
                'billFirstPersonModel' => $billFirstPersonModel,
                'billSecondPersonModel' => $billSecondPersonModel,
                'productList' => $productList,
                'measureList' => $measureList,
                'isAdmin' => $isAdmin,
            ]);
        }
    }    

    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $isAdmin = FSMUser::getIsPortalAdmin();
        
        $model = $this->findModel($id);
        $abonentModel = AbonentBillTemplate::findOne([
            'abonent_id' => $model->userAbonentId, 
            'bill_template_id' => $id,
        ]);        
        $agreementModel = $model->agreement;
        $agreementModel = (!empty($agreementModel) ? $agreementModel : new Agreement());
        $billProductModel = $model->billProducts;        
        $billProductModel = !empty($billProductModel) ? $billProductModel : [new BillTemplateProduct()];
        $billFirstPersonModel = $model->firstClientPerson;        
        $billFirstPersonModel = !empty($billFirstPersonModel) ? $billFirstPersonModel : [new FirstBillTemplatePerson(), new FirstBillTemplatePerson()];
        $billSecondPersonModel = $model->secondClientPerson;        
        $billSecondPersonModel = !empty($billSecondPersonModel) ? $billSecondPersonModel : [new SecondBillTemplatePerson(),new SecondBillTemplatePerson()];

        $modelArr = [
            'BillTemplate' => $model,
            'Agreement' => $agreementModel,
            'AbonentBillTemplate' => $abonentModel,
        ];
        
        // ajax validation
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && $isAjax) {
            $ajaxModelArr = $modelArr;
            $billProductModel = FSMBaseModel::createMultiple(BillTemplateProduct::class);
            FSMBaseModel::loadMultiple($billProductModel, $requestPost);
            foreach ($billProductModel as $index => $billProduct) {
                if(empty($billProduct->product_id) && empty($billProduct->amount) && empty($billProduct->price)){
                    unset($billProductModel[$index]);
                    continue;
                }
            }
            if (!empty($billProductModel)) {
                $ajaxModelArr = ArrayHelper::merge($modelArr, ['BillTemplateProduct' => $billProductModel]);
            }
            $result = $this->performAjaxMultipleValidation($ajaxModelArr);
            return $result;
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                if(empty($model->agreement_id)){
                    throw new Exception('Agreement was not defined!');
                }
                if (!$model->save()) {
                    throw new Exception('Unable to save data! '.$model->errorMessage);
                }
                $abonentModel->save();

                $oldProductIDs = isset($billProductModel[0]) && !empty($billProductModel[0]->id) ? ArrayHelper::map($billProductModel, 'id', 'id') : [];

                $billProductModel = FSMBaseModel::createMultiple(BillTemplateProduct::class, $billProductModel);
                FSMBaseModel::loadMultiple($billProductModel, $requestPost);  
                $deletedIDs = array_diff($oldProductIDs, array_filter(ArrayHelper::map($billProductModel, 'id', 'id')));

                foreach ($billProductModel as $index => $billProduct) {
                    if(empty($billProduct->productName) && empty($billProduct->amount) && empty($billProduct->price)){
                        unset($billProductModel[$index]);
                        continue;
                    }
                    $billProduct->bill_template_id = $model->id;
                    $billProduct->price_eur = $billProduct->price / $model->rate;
                    $billProduct->summa_eur = $billProduct->summa / $model->rate;
                    $billProduct->summa_vat_eur = $billProduct->summa_vat / $model->rate;
                    $billProduct->total_eur = $billProduct->total / $model->rate;
                    if($model->according_contract){
                        $billProduct->product_id = null;
                    }
                }
                if(!empty($billProductModel)){
                    if (FSMBaseModel::validateMultiple($billProductModel)) {
                        if (!empty($deletedIDs)) {
                            BillTemplateProduct::deleteByIDs($deletedIDs);
                        }     
                        foreach ($billProductModel as $billProduct) {
                            if (!$billProduct->save(false)) {
                                throw new Exception('Unable to save data! '.$billProduct->errorMessage);
                                break;
                            }
                        }
                    }else{
                        throw new Exception('Unable to save data! '.$billProductModel->errorMessage);
                    }
                }
                
                if(empty($model->e_signing)){
                    $isEmptyFirstPersonList = $isEmptySecondPersonList = false;
                    
                    $oldPersonIDs = isset($billFirstPersonModel[0]) && !empty($billFirstPersonModel[0]->id) ? ArrayHelper::map($billFirstPersonModel, 'id', 'id') : [];
                    $billPersonModel = FSMBaseModel::createMultiple(FirstBillTemplatePerson::class, (!empty($oldPersonIDs) ? $billFirstPersonModel : []));
                    FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
                    $deletedIDs = array_diff($oldPersonIDs, array_filter(ArrayHelper::map($billPersonModel, 'id', 'id')));

                    foreach ($billPersonModel as $index => $billPerson) {
                        if(empty($billPerson->client_person_id)){
                            unset($billPersonModel[$index]);
                            continue;
                        }
                        $billPerson->bill_template_id = $model->id;                      
                        $billPerson->person_order = 1;                      
                    }

                    if(!empty($billPersonModel)){
                        if (FSMBaseModel::validateMultiple($billPersonModel)) {
                            if (!empty($deletedIDs)) {
                                FirstBillTemplatePerson::deleteByIDs($deletedIDs);
                            }     
                            foreach ($billPersonModel as $billPerson) {
                                if (!$billPerson->save(false)) {
                                    throw new Exception('Unable to save data! '.$billPerson->errorMessage);
                                    break;
                                }
                            }
                        }else{
                            throw new Exception('Unable to save data! '.$billPersonModel->errorMessage);
                        }
                    }else{
                        $isEmptyFirstPersonList = true;
                    }

                    $oldPersonIDs = isset($billSecondPersonModel[0]) && !empty($billSecondPersonModel[0]->id) ? ArrayHelper::map($billSecondPersonModel, 'id', 'id') : [];
                    $billPersonModel = FSMBaseModel::createMultiple(SecondBillTemplatePerson::class, (!empty($oldPersonIDs) ? $billSecondPersonModel : []));
                    FSMBaseModel::loadMultiple($billPersonModel, $requestPost);
                    $deletedIDs = array_diff($oldPersonIDs, array_filter(ArrayHelper::map($billPersonModel, 'id', 'id')));

                    foreach ($billPersonModel as $index => $billPerson) {
                        if(empty($billPerson->client_person_id)){
                            unset($billPersonModel[$index]);
                            continue;
                        }
                        $billPerson->bill_template_id = $model->id;                      
                        $billPerson->person_order = 2;                      
                    }

                    if(!empty($billPersonModel)){
                        if (FSMBaseModel::validateMultiple($billPersonModel)) {
                            if (!empty($deletedIDs)) {
                                SecondBillTemplatePerson::deleteByIDs($deletedIDs);
                            }     
                            foreach ($billPersonModel as $billPerson) {
                                if (!$billPerson->save(false)) {
                                    throw new Exception('Unable to save data! '.$billPerson->errorMessage);
                                    break;
                                }
                            }
                        }else{
                            throw new Exception('Unable to save data! '.$billPersonModel->errorMessage);
                        }
                    }else{
                        $isEmptySecondPersonList = true;
                    }
                    
                    if($isEmptyFirstPersonList && $isEmptySecondPersonList){
                        $model->e_signing = 1;
                    }
                }else{
                    $deletedIDs = isset($billFirstPersonModel[0]) && !empty($billFirstPersonModel[0]->id) ? ArrayHelper::map($billFirstPersonModel, 'id', 'id') : [];
                    if (!empty($deletedIDs)) {
                        FirstBillTemplatePerson::deleteByIDs($deletedIDs);
                    }                          
                    $deletedIDs = isset($billSecondPersonModel[0]) && !empty($billSecondPersonModel[0]->id) ? ArrayHelper::map($billSecondPersonModel, 'id', 'id') : [];
                    if (!empty($deletedIDs)) {
                        SecondBillTemplatePerson::deleteByIDs($deletedIDs);
                    }     
                }
                
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirectToBackUrl($id);              
            }                
        } else {
            $this->rememberBackUrl($model->backURL, $id);
            
            BillUIAsset::register(Yii::$app->getView());

            $model->periodical_last_date = !empty($model->periodical_last_date) ? date('d-m-Y', strtotime($model->periodical_last_date)) : null;
            $model->periodical_next_date = !empty($model->periodical_next_date) ? date('d-m-Y', strtotime($model->periodical_next_date)) : null;
            $model->periodical_finish_date = !empty($model->periodical_finish_date) ? date('d-m-Y', strtotime($model->periodical_finish_date)) : null;

            $projectList = Project::getNameArr(['project.deleted' => 0]);
            $agreementList = Agreement::getNameArr(['agreement.deleted' => 0]);
            $clientRoleList = ClientRole::getNameArr();
            $productList = Product::getNameArr();
            $measureList = Measure::getNameArr();
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            $languageList = Language::getEnabledLanguageList();
            
            $firstClientModel = $agreementModel->firstClient;
            $secondClientModel = $agreementModel->secondClient;
            
            if(count($billFirstPersonModel) == 1){
                $billFirstPersonModel[1] = new FirstBillTemplatePerson();
            }
            if(count($billSecondPersonModel) == 1){
                $billSecondPersonModel[1] = new SecondBillTemplatePerson();
            }
                        
            return $this->render('update', [
                'model' => $model,
                'abonentModel' => $abonentModel,
                'agreementModel' => $agreementModel,
                'firstClientModel' => $firstClientModel,
                'secondClientModel' => $secondClientModel,
                'projectList' => $projectList,
                'agreementList' => $agreementList,
                'clientRoleList' => $clientRoleList,
                'languageList' => $languageList,
                'valutaList' => $valutaList,
                'billProductModel' => $billProductModel,
                'billFirstPersonModel' => $billFirstPersonModel,
                'billSecondPersonModel' => $billSecondPersonModel,
                'productList' => $productList,
                'measureList' => $measureList,
                'isAdmin' => $isAdmin,
            ]);
        }
    }    
    
    /**
     * Displays a single model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        ButtonDeleteAsset::register(Yii::$app->getView());
        
        $model = $this->findModel($id);
        $agreementModel = $model->agreement;
        $agreementModel = (!empty($agreementModel) ? $agreementModel : new Agreement());
        $firstClientModel = $agreementModel->firstClient;
        $firstClientModel = (!empty($firstClientModel) ? $firstClientModel : new Client());
        $secondClientModel = $agreementModel->secondClient;
        $secondClientModel = (!empty($secondClientModel) ? $secondClientModel : new Client());
        $billProductModel = $model->billProducts;        
        $billProductModel = !empty($billProductModel) ? $billProductModel : [new BillTemplateProduct()];

        $isAdmin = FSMUser::getIsPortalAdmin();
        return $this->render('view', [
            'model' => $model,
            'agreementModel' => $agreementModel,
            'firstClientModel' => $firstClientModel,
            'secondClientModel' => $secondClientModel,
            'billProductModel' => $billProductModel,
            'isAdmin' => $isAdmin,
            'time' => date('H:i:s'),
        ]);
    }           
}