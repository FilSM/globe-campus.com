<?php

namespace frontend\controllers\bill;

use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\Response;

use common\controllers\FilSMController;
use common\models\user\FSMUser;
use common\models\user\FSMProfile;
use common\models\abonent\Abonent;
use common\models\client\Client;
use common\models\client\ClientBank;
use common\models\client\ClientBankBalance;
use common\models\bill\Bill;
use common\models\bill\BillHistory;
use common\models\bill\BillPayment;
use common\models\bill\BillConfirm;
use common\models\bill\BillConfirmLink;
use common\models\client\Agreement;
use common\models\client\AgreementHistory;
use common\models\client\AgreementPayment;
use common\models\bill\search\BillConfirmSearch;
use common\models\bill\PaymentConfirm;
use common\models\Bank;
use common\models\Valuta;
use common\models\FileXML;
use common\models\FilePDF;
use common\models\mainclass\FSMBaseModel;

use common\assets\ButtonDeleteAsset;

/**
 * PaymentConfirmController implements the CRUD actions for PaymentConfirm model.
 */
class PaymentConfirmController extends FilSMController
{

    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\bill\PaymentConfirm';
        $this->defaultSearchModel = 'common\models\bill\search\PaymentConfirmSearch';
    }

    /**
     * Lists all models.
     * @return mixed
     */
    public function actionIndex()
    {
        ButtonDeleteAsset::register(Yii::$app->getView());

        $searchModel = new $this->defaultSearchModel;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        $bankList = Bank::getNameArr(['enabled' => true], '', '', function($bank){return $bank['name'].(!empty($bank['swift']) ? ' | '.$bank['swift'] : '');});
        $clientList = Client::getNameArr(['client.deleted' => 0]);
        $userList = FSMProfile::getNameArr(['deleted' => false]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'bankList' => $bankList,
            'clientList' => $clientList,
            'userList' => $userList,
        ]);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new $this->defaultModel;
        $filesXMLModel = new FileXML();
        $filesPDFModel = new FilePDF();

        $modelArr = [
            'PaymentConfirm' => $model,
        ];
        
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxMultipleValidation($modelArr);
        }
        $modelArr = ArrayHelper::merge($modelArr, [
            'PaymentConfirm' => $model,
            'FilesXML' => $filesXMLModel,
            'FilesPDF' => $filesPDFModel,
        ]);

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $filePDF = $filesPDFModel->uploadFile('payment/payment-confirm'.(!empty($model->client_id) ? '/'.$model->client_id : ''));
                if($filePDF && !$filesPDFModel->save()){
                    throw new Exception(Yii::t('bill', 'The PDF file was saved with error!'));
                }elseif(!empty ($filesPDFModel->id)){
                    $model->uploaded_pdf_file_id = $filesPDFModel->id;
                }
                $fileXML = $filesXMLModel->uploadFile('payment/payment-confirm'.(!empty($model->client_id) ? '/'.$model->client_id : ''));
                if(!empty($fileXML)){
                    if($filesXMLModel->save()){
                        if($result = $model->parseImportXML($filesXMLModel)){
                            $model->uploaded_file_id = $filesXMLModel->id;
                            if (!$model->save()) {
                                throw new Exception(Yii::t('bill', 'The XML file data was saved with error! ').' '.$model->errorMessage);
                            }else{
                                foreach ($result as $item) {
                                    /*
                                    if(mb_strpos($item['PmtInfo'], '190359') !== false){
                                        $ii=1;
                                    }
                                     * 
                                     */
                                    
                                    /*
                                    $billPayment = null;
                                    $extId = !empty($item['PmtExtId']) ? $item['PmtExtId'] : (!empty($item['ExtId']) ? $item['ExtId'] : null);
                                    if(isset($extId)){
                                        $billPayment = BillPayment::findOne($extId);
                                    }elseif(!empty($item['DocNo']) && ($item['DocNo'] != 'NULL')){
                                        $billPayment = BillPayment::findOne($item['DocNo']);
                                    }
                                     * 
                                     */
                                    
                                    $secondClientId = null;
                                    if(!empty($item['PayeeExtId'])){
                                        $secondClientId = $item['PayeeExtId'];
                                    }elseif(!empty($item['PayeeLegalID'])){
                                        $client = Client::findOne(['reg_number' => $item['PayeeLegalID']]);
                                        if(!empty($client)){
                                            $secondClientId = $client->id;
                                        }
                                    }elseif(!empty($item['PayeeAccount'])){
                                        $clientBank = ClientBank::findOne(['account' => $item['PayeeAccount']]);
                                        if(!empty($clientBank)){
                                            $secondClientId = $clientBank->client_id;
                                        }
                                    }
                                    
                                    $billConfirm = new BillConfirm();
                                    $billConfirm->payment_confirm_id = $model->id;
                                    $billConfirm->first_client_account = $item['BeneficiaryAccount'];
                                    $billConfirm->second_client_name = $item['PayeeName'];
                                    $billConfirm->second_client_reg_number = 
                                        (!empty($item['PayeeLegalID']) && ($item['PayeeLegalID'] != 'NULL') ? $item['PayeeLegalID'] : null);
                                    $billConfirm->second_client_account = $item['PayeeAccount'];
                                    $billConfirm->second_client_id = $secondClientId;
                                    $billConfirm->doc_date = 
                                        (!empty($item['PmtDate']) && ($item['PmtDate'] != 'NULL') ? $item['PmtDate'] : null);
                                    $billConfirm->doc_number = 
                                        (!empty($item['PmtDocNo']) && ($item['PmtDocNo'] != 'NULL') ? $item['PmtDocNo'] : 
                                            (!empty($item['DocNo'] && ($item['DocNo'] != 'NULL')) ? $item['DocNo'] : null));
                                    $billConfirm->bank_ref = $item['PmtBankRef'];
                                    $billConfirm->direction = $item['CorD'];
                                    $billConfirm->summa = $item['PmtAmount'];
                                    $billConfirm->currency = $item['PmtCurrency'];
                                    $billConfirm->valuta_id = 
                                        (!empty($item['PmtCurrency']) ? 
                                            (($valuta_id = Valuta::findOne(['name' => $item['PmtCurrency']])->id) ? $valuta_id : null)
                                        : null);
                                    $billConfirm->comment = !empty($item['PmtInfo']) ? $item['PmtInfo'] : '';
                                    
                                    if(!$billConfirm->save()){
                                        throw new Exception(Yii::t('bill', 'Cannot save XML file data! ').' '.$billConfirm->errorMessage);
                                    }
                                    
                                    $billArr = [];
                                    //if(empty($billPayment) && !empty($item['PmtInfo'])){
                                    if(!empty($item['PmtInfo'])){
                                        $billList = $billConfirm->getAvailableBillList(false);
                                        foreach ($billList as $key => $bill) {
                                            if(mb_strlen($bill->doc_number) < 5){
                                                continue;
                                            }
                                            if(mb_strpos(mb_strtolower(preg_replace('/\s+/', '', $item['PmtInfo'])), mb_strtolower($bill->doc_number)) !== false){
                                                $billArr[] = $bill;
                                            }
                                        }                                        
                                    }
                                    
                                    //if(!empty($billArr) || !empty($billPayment)){
                                    if(!empty($billArr)){
                                        /*
                                        if(!empty($billPayment)){
                                            $billConfirmLink = new BillConfirmLink();
                                            $billConfirmLink->bill_confirm_id = $billConfirm->id;
                                            $billConfirmLink->doc_type = BillConfirmLink::DOC_TYPE_BILL;
                                            $billConfirmLink->doc_id = $billPayment->bill_id;
                                            $billConfirmLink->payment_id = $billPayment->id;
                                            $billConfirmLink->summa = $billPayment->summa;
                                            if(!$billConfirmLink->save()){
                                                throw new Exception(Yii::t('bill', 'Cannot save XML file data! ').' '.$billConfirmLink->errorMessage);
                                            }
                                            $findPart = (int)($billPayment->summa != $billConfirm->summa);
                                            $billConfirm->updateAttributes(['find_part' => $findPart]);                                            
                                        }else{
                                         * 
                                         */
                                            $total = 0;
                                            foreach ($billArr as $bill) {
                                                $summa = 0;
                                                $billPayments = $bill->notConfirmedPayments;
                                                /*
                                                if(!empty($billPayments)){
                                                    $billPayment = $billPayments[0];
                                                    $summa = $billPayment->summa;
                                                }
                                                 * 
                                                 */
                                                if(count($billArr) == 1){
                                                    $summa = $billConfirm->summa;
                                                }else{
                                                    $summa = $bill->billNotPaidSumma;
                                                    $summa = ($summa >= 0) ? $summa : $summa;
                                                }
                                                $total += $summa;
                                                $billConfirmLink = new BillConfirmLink();
                                                $billConfirmLink->bill_confirm_id = $billConfirm->id;
                                                $billConfirmLink->doc_type = BillConfirmLink::DOC_TYPE_BILL;
                                                $billConfirmLink->doc_id = $bill->id;
                                                //$billConfirmLink->payment_id = isset($billPayment) ? $billPayment->id : null;
                                                $billConfirmLink->summa = $summa;
                                                if(!$billConfirmLink->save()){
                                                    throw new Exception(Yii::t('bill', 'Cannot save XML file data! ').' '.$billConfirmLink->errorMessage);
                                                }
                                                
                                            }
                                            $findPart = (int)(!$billConfirm->compareFloatNumbers($total, $billConfirm->summa, '='));
                                            $billConfirm->updateAttributes(['find_part' => $findPart]);
                                        //}
                                    }
                                    
                                    $clientBankAccount = ClientBank::findOne([
                                        'client_id' => $model->client_id, 
                                        'bank_id' => $model->bank_id, 
                                        'account' => $item['BeneficiaryAccount'],
                                        'deleted' => false,
                                    ]);
                                    if(!$clientBankAccount){
                                        $clientBankAccount = new ClientBank();
                                        $clientBankAccount->client_id = $model->client_id;
                                        $clientBankAccount->bank_id = $model->bank_id;
                                        $clientBankAccount->account = $item['BeneficiaryAccount'];
                                        if(!$clientBankAccount->save()){
                                            throw new Exception(Yii::t('bill', 'Cannot save new bank account! ').' '.$clientBankAccount->errorMessage);
                                        }
                                    }
                                    $clientBankAccount->updateAttributes([
                                        'uploaded_file_id' => $model->uploaded_file_id,
                                        'uploaded_pdf_file_id' => $model->uploaded_pdf_file_id,
                                        'start_date' => $model->start_date, 
                                        'end_date' => $model->end_date,
                                        'balance' => preg_replace('/\s+/', '', $item['closeBal']),
                                        'currency' => $item['currencyName'],
                                    ]);
                                    
                                    $clientBankBalance = ClientBankBalance::findOne([
                                        'account_id' => $clientBankAccount->id, 
                                        'start_date' => $model->start_date, 
                                        'end_date' => $model->end_date,
                                    ]);
                                    if(!$clientBankBalance){
                                        $clientBankBalance = new ClientBankBalance();
                                        $clientBankBalance->payment_confirm_id = $model->id;
                                        $clientBankBalance->account_id = $clientBankAccount->id;
                                        $clientBankBalance->start_date = $model->start_date;
                                        $clientBankBalance->end_date = $model->end_date;
                                        $clientBankBalance->uploaded_file_id = $model->uploaded_file_id;
                                        $clientBankBalance->uploaded_pdf_file_id = $model->uploaded_pdf_file_id;
                                        $clientBankBalance->balance = preg_replace('/\s+/', '', $item['closeBal']);
                                        $clientBankBalance->currency = $item['currencyName'];
                                        if(!$clientBankBalance->save()){
                                            throw new Exception(Yii::t('bill', 'Cannot save XML file data!').' '.$clientBankBalance->errorMessage);
                                        }
                                    }
                                    unset($billConfirm, $clientBankAccount, $clientBankBalance);
                                }
                                Yii::$app->getSession()->setFlash('success', Yii::t('bill', 'The XML file data saving is complete!'));
                            }
                        }else{
                            throw new Exception(Yii::t('bill', 'Cannot parse XML file!'));
                        }
                    }else{
                        throw new Exception(Yii::t('bill', 'The XML file data was saved with error!'));
                    }
                //}else{
                    //throw new Exception(Yii::t('bill', 'The XML file was not uploaded!'));
                }
                
                if(empty($filePDF) && empty($fileXML)){
                    throw new Exception(Yii::t('bill', 'You must specify a PDF or XML file!'));
                }elseif(!empty($filePDF) && empty($fileXML)){
                    $model->start_date = date('Y-m-d', strtotime($model->start_date));
                    $model->end_date = date('Y-m-d', strtotime($model->end_date));                    
                    $model->pay_date = date('Y-m-d', strtotime($model->pay_date));                    
                    $model->client_name = !empty($model->client_id) ? $model->client->name : null;
                    $model->client_reg_number = !empty($model->client_id) ? $model->client->reg_number : null;
                    if (!$model->save()) {
                        throw new Exception(Yii::t('bill', 'Cannot save data!'));
                    }
                }
                
                $transaction->commit();
                return $this->redirect('index');
            } catch (\Exception $e) {
                $filesXMLModel->delete();
                $filesPDFModel->delete();
                $transaction->rollBack();
                $message = $e->getMessage();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirect('index');
            }         
        } else {
            $model->abonent_id = $model->userAbonentId;
            
            $isAdmin = FSMUser::getIsPortalAdmin();
            $bankList = Bank::getNameArr(['enabled' => true], '', '', function($bank){return $bank['name'].(!empty($bank['swift']) ? ' | '.$bank['swift'] : '');});
            $list = Client::find(true)
                ->select(['client.id', 'client.name'])
                ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = client.id AND ac.abonent_id = '. $model->abonent_id)
                ->andWhere(['client.deleted' => 0])
                ->andWhere(['not', ['ac.client_group_id' => null]])
                ->orderBy('client.name')
                ->asArray()
                ->all();
            $clientList = ArrayHelper::map($list, 'id', 'name');
        
            if ($isAjax) {
                return $this->renderAjax('create', [
                    'model' => $model,
                    'filesXMLModel' => $filesXMLModel,
                    'filesPDFModel' => $filesPDFModel,
                    'bankList' => $bankList,
                    'clientList' => $clientList,
                    'isAdmin' => $isAdmin,
                    'isModal' => true,
                ]);
            }else{
                return $this->render('create', [
                    'model' => $model,
                    'filesXMLModel' => $filesXMLModel,
                    'filesPDFModel' => $filesPDFModel,
                    'bankList' => $bankList,
                    'clientList' => $clientList,
                    'isAdmin' => $isAdmin,
                    'isModal' => false,
                ]);
            }
        }
    }      
    
    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id, true);
        $model->scenario = 'update';
        
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxValidation($model);
        }

        if ($model->load($requestPost)){ 
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                if (!$model->save()) {
                    throw new Exception(Yii::t('bill', 'Cannot save data!'));
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirectToBackUrl($id);
            }              
        } else {
            $this->rememberBackUrl($model->backURL, $id);            
            
            $model->start_date = date('d-m-Y', strtotime($model->start_date));
            $model->end_date = date('d-m-Y', strtotime($model->end_date));                    
            $model->pay_date = date('d-m-Y', strtotime($model->pay_date));                    
            
            $isAdmin = FSMUser::getIsPortalAdmin();
            $bankList = Bank::getNameArr(['enabled' => true], '', '', function($bank){return $bank['name'].(!empty($bank['swift']) ? ' | '.$bank['swift'] : '');});
            $list = Client::find(true)
                ->select(['client.id', 'client.name'])
                ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = client.id AND ac.abonent_id = '. $model->userAbonentId)
                ->andWhere(['client.deleted' => 0])
                ->andWhere(['not', ['ac.client_group_id' => null]])
                ->orderBy('client.name')
                ->asArray()
                ->all();
            $clientList = ArrayHelper::map($list, 'id', 'name');
          
            return $this->render('update', [
                'model' => $model,
                'filesXMLModel' => null,
                'filesPDFModel' => null,
                'bankList' => $bankList,
                'clientList' => $clientList,
                'isAdmin' => $isAdmin,
                'isModal' => false,
            ]);
        }
    }
    
    /**
     * Displays a single model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        ButtonDeleteAsset::register(Yii::$app->getView());
        
        $model = $this->findModel($id, true);
        
        $billConfirmModel = new BillConfirmSearch;
        $params = Yii::$app->request->getQueryParams();
        unset($params['id']);
        $params['payment_confirm_id'] = $id;
        $billConfirmDataProvider = $billConfirmModel->search($params);
        
        $isAdmin = FSMUser::getIsPortalAdmin();
        return $this->render('view', [
            'model' => $model,
            'billConfirmModel' => $billConfirmModel,
            'billConfirmDataProvider' => $billConfirmDataProvider,
            'isAdmin' => $isAdmin,
        ]);
    }      
    
    public function actionImport($id)
    {
        $model = $this->findModel($id, true);
        
        $billConfirmModel = new BillConfirmSearch;
        $params = Yii::$app->request->getQueryParams();
        unset($params['id']);
        $params['payment_confirm_id'] = $id;
        $billConfirmDataProvider = $billConfirmModel->search($params);
        $data = $billConfirmDataProvider->query->all();
        
        foreach ($data as $key => $item) {
            $confirmLinkModel = $item->confirmLinks;
            if(empty($confirmLinkModel)){
                unset($data[$key]);
                continue;
            }
        }
        if(empty($data)){
            Yii::$app->getSession()->setFlash('error', Yii::t('bill', 'Cannot import data without invoices'));
            return $this->redirect(FSMBaseModel::getBackUrl());
        }

        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            foreach ($data as $confirmation) {
                $confirmLinkModel = $confirmation->confirmLinks;
                foreach ($confirmLinkModel as $confirmLink) {
                    switch ($confirmLink->doc_type) {
                        case BillConfirmLink::DOC_TYPE_BILL:
                            $historyModel = new BillHistory();
                            $historyModel->bill_id = $confirmLink->doc_id;
                            $historyModel->action_id = BillHistory::BillHistory_ACTIONS['ACTION_STATUS_UP_PAID'];
                            $historyModel->create_time = date('Y-m-d H:i:s');
                            $historyModel->create_user_id = Yii::$app->user->id;
                            $historyModel->comment = (!empty($confirmation->comment) ? $confirmation->comment."\n" : '').
                                Yii::t('common', 'Sum').': '.number_format($confirmLink->summa, 2, '.', ' ').(!empty($confirmation->currency) ? ' '.$confirmation->currency : '');
                            if (!$historyModel->save()) {
                                throw new Exception('Unable to save data! '.$historyModel->errorMessage);
                            }
                            $confirmLink->history_id = $historyModel->id;

                            $docModel = $confirmLink->bill;
                            $docPaymentModel = $confirmLink->billPayment;
                            if(empty($docPaymentModel)){
                                $docPaymentList = $docModel->notConfirmedPayments;
                                if(!empty($docPaymentList)){
                                    foreach ($docPaymentList as $payment) {
                                        if($payment->summa == $confirmLink->summa){
                                            $docPaymentModel = $payment;
                                            break;
                                        }
                                    }                                    
                                }
                            }
                            if(empty($docPaymentModel)){
                                $firstClientBank = ClientBank::findOne(['account' => $confirmation->first_client_account]);
                                $secondClientBank = !empty($confirmation->second_client_account) ? ClientBank::findOne(['account' => $confirmation->second_client_account]) : null;
                                $firstClientBankId = !empty($firstClientBank->id) ? $firstClientBank->bank_id : null;
                                $secondClientBankId = !empty($secondClientBank->id) ? $secondClientBank->bank_id : null;
                                
                                $docPaymentModel = new BillPayment();
                                $docPaymentModel->bill_history_id = $confirmLink->history_id;
                                $docPaymentModel->paid_date = $confirmation->doc_date;
                                $docPaymentModel->from_bank_id = ($confirmation->direction == 'C') ? $secondClientBankId : $firstClientBankId;
                                $docPaymentModel->to_bank_id = ($confirmation->direction == 'C') ? $firstClientBankId : $secondClientBankId;
                                $docPaymentModel->bill_id = $confirmLink->doc_id;
                                $docPaymentModel->summa = $confirmLink->summa;
                                $docPaymentModel->valuta_id = !empty($confirmation->valuta_id) ? $confirmation->valuta_id : null;
                                $docPaymentModel->rate = !empty($confirmation->rate) ? $confirmation->rate : 1;
                                $docPaymentModel->confirmed = date('Y-m-d');
                                $docPaymentModel->manual_input = 0;
                                if (!$docPaymentModel->save()) {
                                    throw new Exception('Unable to save data! '.$docPaymentModel->errorMessage);
                                }                                    
                            }
                            if(!empty($docPaymentModel)){
                                $docPaymentModel->updateAttributes(['confirmed' => date('Y-m-d')]);
                                $confirmLink->payment_id = $docPaymentModel->id;
                                $docModel->__unset('billPayments');
                                $docModel->changeStatus(Bill::BILL_STATUS_PAID, ['paid_date' => $confirmation->doc_date]);
                            }
                            if (!$confirmLink->save()) {
                                throw new Exception('Unable to save data! '.$confirmLink->errorMessage);
                            }                             
                            break;

                        case BillConfirmLink::DOC_TYPE_AGREEMENT:
                            $historyModel = new AgreementHistory();
                            $historyModel->agreement_id = $confirmLink->doc_id;
                            $historyModel->action_id = AgreementHistory::AgreementHistory_ACTIONS['ACTION_CONFIRM_PAYMENT'];
                            $historyModel->create_time = date('Y-m-d H:i:s');
                            $historyModel->create_user_id = Yii::$app->user->id;
                            $historyModel->comment = (!empty($confirmation->comment) ? $confirmation->comment."\n" : '').
                                Yii::t('common', 'Sum').': '.number_format($confirmLink->summa, 2, '.', ' ').(!empty($confirmation->currency) ? ' '.$confirmation->currency : '');
                            if (!$historyModel->save()) {
                                throw new Exception('Unable to save data! '.$historyModel->errorMessage);
                            }
                            $confirmLink->history_id = $historyModel->id;

                            $docModel = $confirmLink->agreement;
                            $docPaymentModel = $confirmLink->agreementPayment;
                            if(empty($docPaymentModel)){
                                $docPaymentList = ($confirmation->direction == 'D') ? $docModel->notConfirmedPaymentsDebet : $docModel->notConfirmedPaymentsCredit;
                                if(!empty($docPaymentList)){
                                    foreach ($docPaymentList as $payment) {
                                        if($payment->summa == $confirmLink->summa){
                                            $docPaymentModel = $payment;
                                            break;
                                        }
                                    }                                       
                                }
                            }
                            if(empty($docPaymentModel)){
                                $firstClientBank = ClientBank::findOne(['account' => $confirmation->first_client_account]);
                                $secondClientBank = !empty($confirmation->second_client_account) ? ClientBank::findOne(['account' => $confirmation->second_client_account]) : null;
                                $firstClientBankId = !empty($firstClientBank->id) ? $firstClientBank->bank_id : null;
                                $secondClientBankId = !empty($secondClientBank->id) ? $secondClientBank->bank_id : null;
                                
                                $docPaymentModel = new AgreementPayment();
                                $docPaymentModel->agreement_history_id = $confirmLink->history_id;
                                $docPaymentModel->paid_date = $confirmation->doc_date;
                                $docPaymentModel->from_bank_id = ($confirmation->direction == 'C') ? $secondClientBankId : $firstClientBankId;
                                $docPaymentModel->to_bank_id = ($confirmation->direction == 'C') ? $firstClientBankId : $secondClientBankId;
                                $docPaymentModel->agreement_id = $confirmLink->doc_id;
                                $docPaymentModel->summa = $confirmLink->summa;
                                $docPaymentModel->valuta_id = !empty($confirmation->valuta_id) ? $confirmation->valuta_id : null;
                                $docPaymentModel->rate = !empty($confirmation->rate) ? $confirmation->rate : 1;
                                $docPaymentModel->direction = ($confirmation->direction == 'C') ? AgreementPayment::DIRECTION_CREDIT : AgreementPayment::DIRECTION_DEBET;
                                $docPaymentModel->confirmed = date('Y-m-d');
                                $docPaymentModel->manual_input = 0;
                                if (!$docPaymentModel->save()) {
                                    throw new Exception('Unable to save data! '.$docPaymentModel->errorMessage);
                                } 
                            }
                            if(!empty($docPaymentModel)){
                                $docPaymentModel->updateAttributes(['confirmed' => date('Y-m-d')]);
                                $confirmLink->payment_id = $docPaymentModel->id;
                            }
                            if (!$confirmLink->save()) {
                                throw new Exception('Unable to save data! '.$confirmLink->errorMessage);
                            } 
                            break;

                        case BillConfirmLink::DOC_TYPE_EXPENSE:
                            $docModel = $confirmLink->expense;
                            $docModel->updateAttributes(['confirmed' => $confirmation->doc_date]);
                            if (!$docModel->save()) {
                                throw new Exception('Unable to save data! '.$docModel->errorMessage);
                            } 
                            break;

                        case BillConfirmLink::DOC_TYPE_DIRECT:
                            if(!$docModel = $confirmLink->expense){
                                $projectModel = \common\models\client\Project::find()->notDeleted()
                                    ->andWhere(['abonent_id' => $model->userAbonentId])
                                    ->andWhere(['name' => 'Fixed costs'])
                                    ->one();
                                if(empty($projectModel)){
                                    $projectModel = new \common\models\client\Project();
                                    $projectModel->abonent_id = $model->userAbonentId;
                                    $projectModel->name = 'Fixed costs';
                                    if (!$projectModel->save()) {
                                        throw new Exception('Unable to save data! '.$projectModel->errorMessage);
                                    }                                 
                                }

                                $bankModel = Bank::findOne($model->bank_id);
                                $clientModel = !empty($bankModel->reg_number) ?
                                    Client::findOne(['reg_number' => $bankModel->reg_number]) :
                                    null;
                                if(isset($clientModel)){
                                    $docModel = new \common\models\bill\Expense();
                                    $docModel->abonent_id = $model->userAbonentId;
                                    $docModel->expense_type_id = $confirmLink->doc_subtype;
                                    $docModel->project_id = $projectModel->id;
                                    $docModel->doc_number = !
                                        empty($confirmation->doc_number) && ($confirmation->doc_number != 'NULL') ? 
                                            $confirmation->doc_number : 
                                            $confirmation->bank_ref;
                                    $docModel->doc_date = $confirmation->doc_date;
                                    $docModel->confirmed = date('Y-m-d');
                                    $docModel->pay_type = \common\models\bill\Expense::PAY_TYPE_CARD;
                                    $docModel->first_client_id = $clientModel->id;
                                    $docModel->second_client_id = $model->client_id;
                                    $docModel->summa = $confirmLink->summa - $confirmLink->vat;
                                    $docModel->vat = !empty($confirmLink->vat) ? $confirmLink->vat : 0;
                                    $docModel->total = $confirmLink->summa;
                                    $docModel->valuta_id = $confirmation->valuta_id;
                                    $docModel->rate = $confirmation->rate;
                                    $docModel->manual_input = 0;
                                    if (!$docModel->save()) {
                                        throw new Exception('Unable to save data! '.$docModel->errorMessage);
                                    } 

                                    $confirmLink->doc_id = $docModel->id;
                                    if (!$confirmLink->save()) {
                                        throw new Exception('Unable to save data! '.$confirmLink->errorMessage);
                                    } 
                                }
                            }
                            break;

                        case BillConfirmLink::DOC_TYPE_TAX:
                            if(!$docModel = $confirmLink->taxPayment){
                                $clientModel = !empty($confirmation->second_client_id) ?
                                    Client::findOne($confirmation->second_client_id) :
                                    null;
                                if(isset($clientModel)){
                                    $docModel = new \common\models\bill\TaxPayment();
                                    $docModel->abonent_id = $model->userAbonentId;
                                    $docModel->doc_number = !
                                        empty($confirmation->doc_number) && ($confirmation->doc_number != 'NULL') ? 
                                            $confirmation->doc_number : 
                                            $confirmation->bank_ref;
                                    $docModel->paid_date = $confirmation->doc_date;
                                    $docModel->from_client_id = $model->client_id;
                                    $docModel->to_client_id = $confirmation->second_client_id;
                                    $docModel->summa = $confirmLink->summa;
                                    $docModel->valuta_id = $confirmation->valuta_id;
                                    $docModel->rate = $confirmation->rate;
                                    $docModel->confirmed = date('Y-m-d');
                                    $docModel->manual_input = 0;
                                    $docModel->comment = $confirmation->comment;
                                    if (!$docModel->save()) {
                                        throw new Exception('Unable to save data! '.$docModel->errorMessage);
                                    } 

                                    $confirmLink->doc_id = $docModel->id;
                                    if (!$confirmLink->save()) {
                                        throw new Exception('Unable to save data! '.$confirmLink->errorMessage);
                                    } 
                                }
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
            $model->updateAttributes([
                'status' => PaymentConfirm::IMPORT_STATE_COMPLETE, 
                'action_time' => date('Y-m-d H:i'),
                'action_user_id' => Yii::$app->user->identity->getId(),
            ]);
            Yii::$app->getSession()->setFlash('success', Yii::t('bill', 'The import from the XML file is complete!'));
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            $message = $e->getMessage();
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
        } finally {
            return $this->redirect(FSMBaseModel::getBackUrl());
        }        
    }    
    
    public function actionGenerate($id)
    {
        $model = $this->findModel($id, true);
        $startDate = date('Y-m-d', strtotime($model->start_date));
        $endDate = date('Y-m-d', strtotime($model->end_date));
        
        $query = BillPayment::find(true);
        $query->select = [
            'bill_payment.*',
        ];
        $query->leftJoin(['bill' => 'bill'], 'bill.id = bill_payment.bill_id')
            ->innerJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id and ab.abonent_id = '.$model->userAbonentId)
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->leftJoin(['bill_confirm_link' => 'bill_confirm_link'], 'bill_confirm_link.payment_id = bill_payment.id')
                
            ->andWhere(['bill_confirm_link.payment_id' => null])
            ->andWhere(['bill_payment.to_bank_id' => $model->bank_id])
            ->andWhere(['or', 
                ['a.second_client_id' => $model->client_id],
                ['a.third_client_id' => $model->client_id],
            ])
            ->andWhere(['between', 'bill_payment.confirmed', $startDate, $endDate.' 23:59:59'])
            ->addOrderBy('bill_payment.confirmed');
        
        $paymentList = $query->all();;

        $this->rememberBackUrl($model->backURL, $id);
        
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            /*
            if (!$model->save()) {
                throw new Exception(Yii::t('bill', 'Cannot save data!'));
            }
             * 
             */
            $transaction->commit();
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $transaction->rollBack();
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
        } finally {
            return $this->redirectToBackUrl($id);
        }  
    }
}