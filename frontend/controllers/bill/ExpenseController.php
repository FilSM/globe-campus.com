<?php

namespace frontend\controllers\bill;

use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\filters\AccessControl;

use common\controllers\FilSMAttachmentController;
use common\components\FSMAccessHelper;
use common\models\mainclass\FSMBaseModel;
use common\models\user\FSMUser;
use common\models\abonent\Abonent;
use common\models\client\Client;
use common\models\bill\ExpenseType;
use common\models\bill\ExpenseAttachment;
use common\models\client\Project;
use common\models\Valuta;
use common\models\Files;
use common\assets\ButtonDeleteAsset;
use frontend\assets\ExpenseUIAsset;

/**
 * ExpenseController implements the CRUD actions for Expense model.
 */
class ExpenseController extends FilSMAttachmentController
{

    public $attachmentDir = 'expense';
    public $attachmentClass = 'common\models\bill\ExpenseAttachment';
    public $foreignKeyField = 'expense_id';

    //public $attachmentField = 'uploaded_file_id';

    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\bill\Expense';
        $this->defaultSearchModel = 'common\models\bill\search\ExpenseSearch';
        $this->pjaxIndex = true;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [];
        $_get = Yii::$app->request->get();
        $id = isset($_get['id']) ? $_get['id'] : null;
        $model = !empty($id) ? $this->findModel($id) : null;
        $behaviors = ArrayHelper::merge(
            $behaviors, [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'actions' => [
                                'ajax-get-model',
                                'ajax-get-doc-confirm',
                                'ajax-modal-name-list',
                            ],
                            'allow' => true,
                        ],
                        [
                            'actions' => ['index'],
                            'allow' => FSMAccessHelper::checkRoute('/expense/*'),
                        ],
                        [
                            'actions' => ['view'],
                            'allow' => FSMAccessHelper::can('viewExpense', $model),
                        ],
                        [
                            'actions' => ['create', 'copy'],
                            'allow' => FSMAccessHelper::can('createExpense'),
                        ],
                        [
                            'actions' => ['update', 'attachment-delete'],
                            'allow' => FSMAccessHelper::can('updateExpense', $model),
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => FSMAccessHelper::can('deleteExpense', $model),
                        ],
                    ],
                ],
            ]
        );
        return $behaviors;
    }

    public function actionAjaxGetDocConfirm($id, $for_client_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (empty($id)) {
            return [];
        }
        $model = $this->findModel($id);
        $clientModel = $model->firstClient;

        $result['notPaidSumma'] = $model->summa;
        $result['rate'] = $model->rate;
        $result['currency'] = $model->valuta_id;
        $result['firstClientBankAccount'] = null;
        $result['secondClientId'] = $clientModel->id;
        $result['secondClientName'] = $clientModel->name;
        $result['secondClientReg'] = $clientModel->reg_number;
        $result['secondClientBankAccount'] = null;

        return $result;
    }

    /**
     * Lists all models.
     * @return mixed
     */
    public function actionIndex()
    {
        ButtonDeleteAsset::register(Yii::$app->getView());

        $params = Yii::$app->request->getQueryParams();

        $user = Yii::$app->user->identity;
        if (FSMUser::getIsPortalAdmin() || $user->hasRole([
                FSMUser::USER_ROLE_BOSS,
                FSMUser::USER_ROLE_ADMIN,
                FSMUser::USER_ROLE_COORDINATOR,
            ]
        )) {
            
        } else {
            $params['report_plus'] = 0;
        }
        $searchModel = new $this->defaultSearchModel;
        $dataProvider = $searchModel->search($params);

        $projectList = Project::getNameArr(['project.deleted' => 0]);
        $clientList = Client::getNameArr(['client.deleted' => 0]);
        $expenseTypeList = ExpenseType::getNameArr();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'projectList' => $projectList,
            'clientList' => $clientList,
            'expenseTypeList' => $expenseTypeList,
        ]);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new $this->defaultModel;
        $filesModel = [new Files()];

        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxValidation($model);
        }

        if ($model->load($requestPost)) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                if (!$model->save()) {
                    throw new Exception('Unable to save data! ' . $model->errorMessage);
                }

                if($this->createAttachment($model, $filesModel)){
                    $transaction->commit();
                }
            } catch (Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                if ($isPjax) {
                    return $this->actionAjaxModalNameList(['selected_id' => $model->id]);
                } else {
                    return $this->redirect('index');
                }
            }
        } else {
            ExpenseUIAsset::register(Yii::$app->getView());

            $model->abonent_id = $model->userAbonentId;
            $model->doc_date = date('d-m-Y');
            $model->valuta_id = Valuta::VALUTA_DEFAULT;

            $isAdmin = FSMUser::getIsPortalAdmin();
            $projectList = Project::getNameArr(['project.deleted' => 0]);
            $clientList = Client::getNameArr(['client.deleted' => 0]);
            $expenseTypeList = ExpenseType::getNameArr();
            $valutaList = Valuta::getNameArr(['enabled' => true]);

            $data = [
                'model' => $model,
                'filesModel' => $filesModel,
                'projectList' => $projectList,
                'clientList' => $clientList,
                'expenseTypeList' => $expenseTypeList,
                'valutaList' => $valutaList,
                'isAdmin' => $isAdmin,
                'isModal' => $isAjax,
            ];
            if ($isAjax) {
                return $this->renderAjax('create', $data);
            } else {
                return $this->render('create', $data);
            }
        }
    }

    public function actionCopy($id)
    {
        $model = $this->findModel($id, true);
        $model->id = null;
        $model->setIsNewRecord(true);

        $filesModel = [new Files()];

        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxValidation($model);
        }

        if ($model->load($requestPost)) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                if (!$model->save()) {
                    throw new Exception('Unable to save data! ' . $model->errorMessage);
                }

                if($this->createAttachment($model, $filesModel)){
                    $transaction->commit();
                }
            } catch (Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirect('index');
            }
        } else {
            ExpenseUIAsset::register(Yii::$app->getView());

            $model->doc_date = date('d-m-Y');
            
            $isAdmin = FSMUser::getIsPortalAdmin();
            $projectList = Project::getNameArr(['project.deleted' => 0]);
            $clientList = Client::getNameArr(['client.deleted' => 0]);
            $expenseTypeList = ExpenseType::getNameArr();
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            return $this->render('create', [
                'model' => $model,
                'filesModel' => $filesModel,
                'projectList' => $projectList,
                'clientList' => $clientList,
                'expenseTypeList' => $expenseTypeList,
                'valutaList' => $valutaList,
                'isAdmin' => $isAdmin,
                'isModal' => false,
            ]);
        }
    }

    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, true);

        $filesModel = $model->attachmentFiles;
        $filesModel = (!empty($filesModel) ? $filesModel : [new Files()]);

        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxValidation($model);
        }

        if ($model->load($requestPost)) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                if (!$model->save()) {
                    throw new Exception('Unable to save data! ' . $model->errorMessage);
                }
                
                if($this->updateAttacment($model, $filesModel)){
                    $transaction->commit();
                }
            } catch (Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirectToBackUrl($id);
            }
        } else {
            $this->rememberBackUrl($model->backURL, $id);

            ExpenseUIAsset::register(Yii::$app->getView());

            $model->doc_date = date('d-m-Y', strtotime($model->doc_date));
            $model->confirmed = !empty($model->confirmed) ? date('d-m-Y', strtotime($model->confirmed)) : null;

            $isAdmin = FSMUser::getIsPortalAdmin();
            $projectList = Project::getNameArr(['project.deleted' => 0]);
            $clientList = Client::getNameArr(['client.deleted' => 0]);
            $expenseTypeList = ExpenseType::getNameArr();
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            return $this->render('update', [
                'model' => $model,
                'filesModel' => $filesModel,
                'projectList' => $projectList,
                'clientList' => $clientList,
                'expenseTypeList' => $expenseTypeList,
                'valutaList' => $valutaList,
                'isAdmin' => $isAdmin,
            ]);
        }
    }

    /**
     * Displays a single model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        ButtonDeleteAsset::register(Yii::$app->getView());

        $model = $this->findModel($id, true);

        $filesModel = $model->attachmentFiles;
        $filesModel = (!empty($filesModel) ? $filesModel : [new Files()]);

        $isAdmin = FSMUser::getIsPortalAdmin();
        return $this->render('view', [
            'model' => $model,
            'filesModel' => $filesModel,
            'isAdmin' => $isAdmin,
        ]);
    }

    public function actionAjaxModalNameList($param = [])
    {
        if (!isset($this->defaultModel)) {
            return [];
        }

        $model = new $this->defaultModel;

        $selectedId = null;
        if (isset($param['selected_id'])) {
            $selectedId = $param['selected_id'];
            unset($param['selected_id']);
        }
        $param = !empty($param) ? $param : null;

        if ($model->hasAttribute('deleted') && !isset($param['deleted'])) {
            $param['deleted'] = false;
        }

        // JSON response is expected in case of successful save
        Yii::$app->response->format = Response::FORMAT_JSON;

        $result = [];
        $source = !empty($_GET['source']) ? $_GET['source'] : null;
        switch ($source) {
            case 'bill-confirm':
                $billConfirmModel = !empty($_GET['bill_confirm_id']) ?
                    \common\models\bill\BillConfirm::findOne($_GET['bill_confirm_id']) :
                    new \common\models\bill\BillConfirm();
                $data = $billConfirmModel->getAvailableExpenseList();
                unset($billConfirmModel);
                break;

            default:
                $data = $model::getNameArr($param, $model::$nameField, $model::$keyField, $model::$nameField);
                break;
        }
        unset($model);

        foreach ($data as $key => $item) {
            $result[] = [
                'id' => $key,
                'text' => $item,
            ];
        }

        return [
            'success' => true,
            'selected' => $selectedId,
            'data' => $result,
        ];
    }

}
