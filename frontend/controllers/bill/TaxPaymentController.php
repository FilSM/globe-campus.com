<?php

namespace frontend\controllers\bill;

use Yii;
//use yii\base\Exception;
//use yii\helpers\Url;
//use yii\helpers\ArrayHelper;
//use yii\web\Response;
//use yii\filters\AccessControl;

use common\controllers\FilSMController;
use common\models\mainclass\FSMBaseModel;
use common\assets\ButtonDeleteAsset;
use common\models\user\FSMUser;
use common\models\client\Client;
use common\models\Valuta;

/**
 * TaxPaymentController implements the CRUD actions for TaxPayment model.
 */
class TaxPaymentController extends FilSMController
{

    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\bill\TaxPayment';
        $this->defaultSearchModel = 'common\models\bill\search\TaxPaymentSearch';
    }
    
    /**
     * Lists all models.
     * @return mixed
     */
    public function actionIndex() {
        ButtonDeleteAsset::register(Yii::$app->getView());
        
        $searchModel = new $this->defaultSearchModel;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $clientList = Client::getNameArr(['client.deleted' => 0]);
        
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'clientList' => $clientList,
        ]);
    }
    
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new $this->defaultModel;
        $modelArr = [
            'TaxPayment' => $model,
        ];
        
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxMultipleValidation($modelArr);
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $model->manual_input = 1;
                if ($model->save()) {
                } else {
                    throw new Exception('Unable to save data! '.$model->errorMessage);
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirect('index');
            }
        } else {
            $model->abonent_id = $model->userAbonentId;
            $model->valuta_id = Valuta::VALUTA_DEFAULT;
            $isAdmin = FSMUser::getIsPortalAdmin();
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            $clientList = Client::getNameArr(['client.deleted' => 0]);
            
            return $this->render('create', [
                'model' => $model,
                'clientList' => $clientList,
                'valutaList' => $valutaList,
                'isAdmin' => $isAdmin,
                'isModal' => false,
            ]);
        }
    }
    
    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionAjaxUpdate($id) {
        $model = $this->findModel($id, true);
        $modelArr = [
            'TaxPayment' => $model,
        ];
        
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxMultipleValidation($modelArr);
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                if ($model->save()) {
                    if(empty($model->paid_date)){
                        $billModel->changeStatus(Bill::BILL_STATUS_PAYMENT);
                    }else{
                        $paid_date = date('Y-m-d', strtotime($model->paid_date));
                        $billModel->changeStatus(Bill::BILL_STATUS_PAID, ['paid_date' => $paid_date]);
                    }
                } else {
                    throw new Exception('Unable to save data! '.$model->errorMessage);
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return 'reload';
            }
        } else {
            $isAdmin = FSMUser::getIsPortalAdmin();
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            $clientList = Client::getNameArr(['client.deleted' => 0]);
            $model->paid_date = date('d-m-Y', strtotime($model->paid_date));
            $model->confirmed = isset($model->confirmed) ? date('d-m-Y', strtotime($model->confirmed)) : null;
                
            return $this->renderAjax('update', [
                'model' => $model,
                'clientList' => $clientList,
                'valutaList' => $valutaList,
                'isAdmin' => $isAdmin,
                'isModal' => true,
            ]);
        }
    }     
}