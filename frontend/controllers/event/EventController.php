<?php

namespace frontend\controllers\event;

use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Response;
use yii\filters\AccessControl;

use kartik\helpers\Html;

use sammaye\qtip\QtipAsset;

use common\components\FSMGoogleCalendarApi;
use common\components\FSMToken;
use common\components\FSMAccessHelper;
use common\controllers\FilSMController;
use common\models\mainclass\FSMBaseModel;
use common\models\user\FSMUser;
use common\models\user\FSMProfile;
use common\models\client\Client;
use common\models\event\Event;
use common\models\event\EventProfile;
use common\models\event\EventContact;
use common\models\event\EventResurs;
use common\models\event\EventEventResurs;
use frontend\assets\EventUIAsset;
use common\assets\ButtonDeleteAsset;

/**
 * EventController implements the CRUD actions for Event model.
 */
class EventController extends FilSMController
{

    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\event\Event';
        $this->defaultSearchModel = 'common\models\event\search\EventSearch';
        $this->pjaxIndex = true;
        
        \lajax\translatemanager\helpers\Language::registerAssets();
    }

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [];
        $_get = Yii::$app->request->get();
        $id = isset($_get['id']) ? $_get['id'] : null;
        $model = !empty($id) ? $this->findModel($id) : null;
        $behaviors = ArrayHelper::merge(
            $behaviors, 
            [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'actions' => [
                                'accept', 
                                'accept-event', 
                                'accept-event-contact',
                                'refuse-event',
                                'refuse-event-contact',
                                'send-remind',
                            ],
                            'allow' => true,
                        ],                    
                        [
                            'actions' => ['index', 'calendar'],
                            'allow' => FSMAccessHelper::checkRoute('/event/*'),
                        ],
                        [
                            'actions' => ['view', 'ajax-events'],
                            'allow' => FSMAccessHelper::can('viewEvent', $model),
                        ],
                        [
                            'actions' => ['create', 'google-auth'],
                            'allow' => FSMAccessHelper::can('createEvent'),
                        ],
                        [
                            'actions' => ['update'],
                            'allow' => FSMAccessHelper::can('updateEvent', $model),
                        ],
                        [
                            'actions' => ['delete'],
                            'allow' => FSMAccessHelper::can('deleteEvent', $model),
                        ],                        
                    ]
                ]                
            ]
        );
        return $behaviors;        
    }
    
    /**
     * Lists all models.
     * @return mixed
     */
    public function actionCalendar($start = null, $end = null, $_ = null)
    {
        QtipAsset::register(Yii::$app->getView());
        $isAdmin = FSMUser::getIsPortalAdmin();
        return $this->render('calendar', [
            'eventModel' => new Event(),
            'isAdmin' => $isAdmin,
        ]);
    }

    /**
     * Lists all models.
     * @return mixed
     */
    public function actionAjaxEvents($start = null, $end = null, $_ = null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $timezone = new \DateTimeZone('Europe/Riga');
            
        if (!empty($start) and !empty($end)) {
            $eventList = Event::find(true)
                ->andWhere(['!=', 'deleted', 1])
                ->andWhere(['>=', 'event_datetime', date('Y-m-d 00:00:01', strtotime($start))])
                ->andWhere(['<=', 'event_datetime', date('Y-m-d 23:59:59', strtotime($end))])
                ->all();
            $startPeriod = $start;
            $endPeriod = $end;
        } else {
            $eventList = Event::find()
                ->andWhere(['!=', 'deleted', 1])
                ->all();
            $startPeriod = date("Y-m-d", strtotime('monday this week'));
            $endPeriod = date("Y-m-d", strtotime('sunday this week'));
        }
        
        $events = [];
        
        $emptyStart = new \DateTime($startPeriod, $timezone);
        $emptyEnd = new \DateTime($endPeriod, $timezone);
        $interval = $emptyStart->diff($emptyEnd, true);
        $interval = $interval->format('%d');

        if($interval <= 7){
            $day = $startPeriod;
            do {
                $nextHour = 9;
                for ($i = 9; $i <= 17; $i++) {
                    $nextHour++;
                    $item = new \yii2fullcalendar\models\Event();
                    $item->id = (double)strtotime($day." $i:00:00") * -1;
                    $item->url = '#';
                    $item->title = '#';
                    $item->start = date('Y-m-d\TH:i:s\Z', strtotime($day." $i:00:00"));
                    $item->end = date('Y-m-d\TH:i:s\Z', strtotime($day." $nextHour:00:00"));

                    $html = 
                    '<div id="event-emp-'.$day.'-'.$i.'" class="eventWrapper blockMinHeight emptyEvent" style="min-height:31px;">
                        <div class="left eventWrapperLeft">
                            <div class="event_starttime">'.date('H:i', strtotime($day." $i:00:00")).'</div>
                        </div>
                        <div class="eventWrapperRight"></div>
                        <div style="clear: both;"></div>
                    </div>';

                    $item->nonstandard = [
                        'client' => null,
                        'description' => null,
                        'partOur' => '',
                        'partClient' => '',
                        'html' => $html,
                    ];                
                    $events[] = $item;
                }
                $day = date('Y-m-d', strtotime($day . ' +1 day'));
            }while ($day <= $endPeriod);
        }
        
        $eventTypeList = Event::getEventTypeList();
        foreach ($eventList AS $event) {
            $startTime = $event->event_date . ' ' . $event->event_time;
            $endTime = !empty($event->event_date_finish) ? $event->event_date_finish . ' ' . $event->event_time_finish : null;

            if(($interval <= 7)){
                foreach ($events as $key => $emptyEvent) {
                    $emptyEvStart = new \DateTime($emptyEvent->start, $timezone);
                    $emptyEvEnd = new \DateTime($emptyEvent->end, $timezone);
                    $emptyEvStart = $emptyEvStart->format('Y-m-d H:i:s');
                    $emptyEvEnd = $emptyEvEnd->format('Y-m-d H:i:s');
                    if($emptyEvent->id < 0){
                        if(
                            (($startTime >= $emptyEvStart) && ($startTime < $emptyEvEnd)) ||
                            (($endTime > $emptyEvStart) && ($endTime <= $emptyEvEnd)) ||
                            (($emptyEvStart >= $startTime) && ($emptyEvEnd <= $endTime))
                        ){
                            unset($events[$key]);
                        }
                    }
                }
            }
            
            $item = new \yii2fullcalendar\models\Event();
            $item->id = $event->id;
            $item->url = Url::to(['/event/view', 'id' => $event->id, /*'ajax' => true,*/]);
            $item->title = $eventTypeList[$event->event_type];
            $item->start = date('Y-m-d\TH:i:s\Z', strtotime($startTime));
            $item->end = !empty($endTime) ? date('Y-m-d\TH:i:s\Z', strtotime($endTime)) : null;
            /*
            if($event->event_type == Event::EVENT_TYPE_CALL){
                $item->textColor = 'yellow';
            }
             * 
             */
            $eventProfileModel = $event->eventProfiles;
            $eventContactModel = $event->eventContacts;
            $partOurArr = $partClientArr = [];
            if(!empty($eventProfileModel[0]->id)){
                foreach ($eventProfileModel as $eventProfile) {
                    $short_name = $eventProfile->profile->short_name;
                    $short_name = !empty($short_name) ? $short_name : $eventProfile->profile->name;
                    if(!empty($short_name)){
                        $color = !empty($eventProfile->accepted_at) ? '#04fb0e' : (!empty($eventProfile->refused_at) ? '#FF9800' : 'yellow');
                        $partOurArr[] = '<span class="badge" style="color: '.$color.'; font-size: 75%;">'.$short_name.'</span>';
                    }
                }
            }
        
            if(!empty($eventContactModel[0]->id)){
                foreach ($eventContactModel as $eventContact) {
                    $short_name = $eventContact->short_name;
                    $partClientArr[] = '<span class="badge" style="color: white; font-weight: bold; font-size: 75%">'.
                        (!empty($eventContact->short_name) ? $eventContact->short_name : $eventContact->clientContactName).'</span>';
                }
            }
            
            $eventResurs = $event->eventResurs;
            $eventResurs = !empty($eventResurs) ? $eventResurs[0]->eventResurs : null;
            
            $html = 
            '<div id="event-'.$event->id.'" class="eventWrapper blockMinHeight" style="min-height:31px;">
                <div class="left eventWrapperLeft">
                    <div class="event_starttime">'.date('H:i', strtotime($event->event_time)).'</div>'.
                    (!empty($event->event_time_finish) ? '<div class="event_endtime">'.date('H:i', strtotime($event->event_time_finish)).'</div>' : '').
                '</div>
                <div class="eventWrapperRight">
                    <div class="partOur">'.
                        implode(' ', $partOurArr).
                    '</div>
                    <div class="activity">'.
                        (empty($eventResurs->color_code) ?
                            $eventTypeList[$event->event_type] :
                            Html::badge($eventTypeList[$event->event_type], ['style' => "background-color: {$eventResurs->color_code}; font-size: 100%;"])
                        ) .
                    '</div>'.
                    (!empty($event->name) ?
                    '<div class="event-name">
                        <span style="color: black;">'.$event->name.'</span>
                    </div>' : '').
                    (!empty($event->client_id) || !empty($partClientArr) ? '<hr>' : '').
                    '<div class="client-name left">'.
                        (!empty($event->client_id) ? '<span style="color: black; font-weight: bold;">'.$event->client->name.'</span>' : '').
                    '</div>
                    <div class="partClient left">'.
                        implode(' ', $partClientArr).
                    '</div>
                </div>
                <div style="clear: both;"></div>
            </div>';
            
            $item->nonstandard = [
                'client' => !empty($event->client_id) ? '<span style="color: black; font-weight: bold;">'.$event->client->name.'</span>' : null,
                'description' => !empty($event->name) ? '<span style="color: black;">'.$event->name.'</span>' : null,
                'partOur' => implode(' ', $partOurArr),
                'partClient' => implode(' ', $partClientArr),
                'html' => $html,
            ];
            $events[] = $item;
        }

        return array_values($events);
    }
    
    /**
     * Attempts user confirmation.
     *
     * @param integer $event_id
     * @param integer $user_id
     * @param string $code
     *
     * @return boolean
     */
    public function actionAccept($event_id, $user_id, $code)
    {
        $token = FSMToken::findOne([
            'user_id' => $user_id,
            'code'    => $code,
            'type'    => Event::TOKEN_TYPE_NEW_EVENT,
        ]);

        if ($token instanceof FSMToken) {
            //$token->delete();
            $user = FSMUser::findOne($user_id);
            $profile = $user->profile;
            $eventProfile = EventProfile::findOne([
                'event_id' => $event_id,
                'profile_id' => $profile->id,
            ]);
            if(isset($eventProfile)){
                $success = (bool) $eventProfile->updateAttributes(['accepted_at' => date('Y-m-d H:i:s')]);
                if ($eventProfile && $success) {
                    //Yii::$app->user->login($user, $user->module->rememberFor);
                    $message = Yii::t('event', 'Thank you, your acceptance is now completed.');
                } else {
                    $message = Yii::t('event', 'Something went wrong and your acceptance has not been completed.');
                }
            }else{
                $success = true;
                $message = Yii::t('event', 'Thank you, your acceptance is now completed.');
            }
        } else {
            $success = false;
            $message = Yii::t('event', 'The acceptance link is invalid or expired.');
        }

        Yii::$app->session->setFlash($success ? 'success' : 'danger', $message);
        
        return $this->render('/message', [
            'title'  => \Yii::t('event', 'Event acceptance'),
            'module' => isset($user) ? $user->module : Yii::$app->getModule('user'),
        ]);
    }
    
    /**
     * Attempts user confirmation.
     *
     * @return boolean
     */
    public function actionAcceptEvent($idProfile)
    {
        $eventProfile = EventProfile::findOne($idProfile);
        if ($eventProfile) {
            $eventProfile->updateAttributes(['accepted_at' => date('Y-m-d H:i:s')]);
            $message = Yii::t('event', 'Thank you, your acceptance is now completed.');
            $success = true;
        } else {
            $message = Yii::t('event', 'Something went wrong and your acceptance has not been completed.');
            $success = false;
        }
        Yii::$app->session->setFlash($success ? 'success' : 'danger', $message);
        return $this->redirect(FSMBaseModel::getBackUrl());
    }
    
    public function actionRefuseEvent($idProfile)
    {
        $model = EventProfile::findOne($idProfile);
        if ($model->load(Yii::$app->request->post())){
            $model->accepted_at = null;
            $model->refused_at = date('Y-m-d H:i:s');
            $model->save();
            Yii::$app->response->format = Response::FORMAT_JSON;
            return 'reload';
        } else {
            return $this->renderAjax('_form_refuse', [
                'model' => $model,
            ]);
        }
    }
    
    /**
     * Attempts user confirmation.
     *
     * @return boolean
     */
    public function actionAcceptEventContact($idContact)
    {
        $eventContact = EventContact::findOne($idContact);
        if ($eventContact) {
            $eventContact->updateAttributes(['accepted_at' => date('Y-m-d H:i:s')]);
            $message = Yii::t('event', 'Thank you, your acceptance is now completed.');
            $success = true;
        } else {
            $message = Yii::t('event', 'Something went wrong and your acceptance has not been completed.');
            $success = false;
        }
        Yii::$app->session->setFlash($success ? 'success' : 'danger', $message);
        return $this->redirect(FSMBaseModel::getBackUrl());
    }
    
    public function actionRefuseEventContact($idContact)
    {
        $model = EventContact::findOne($idContact);
        if ($model->load(Yii::$app->request->post())){
            $model->accepted_at = null;
            $model->refused_at = date('Y-m-d H:i:s');
            $model->save();
            Yii::$app->response->format = Response::FORMAT_JSON;
            return 'reload';
        } else {
            return $this->renderAjax('_form_refuse', [
                'model' => $model,
            ]);
        }
    }
    
    /**
     * Lists all models.
     * @return mixed
     */
    public function actionIndex($client_id = null)
    {
        if (isset($client_id) && (($clientModel = Client::findOne($client_id)) === null)) {
            throw new Exception('The requested page does not exist.');
        }        
        ButtonDeleteAsset::register(Yii::$app->getView());

        $searchModel = new $this->defaultSearchModel;
        $params = Yii::$app->request->getQueryParams();
        $params['deleted'] = (empty($params) || empty($params['EventSearch'])) ? 0 : (int)!empty($params['EventSearch']['deleted']);
        $dataProvider = $searchModel->search($params);

        $isAdmin = FSMUser::getIsPortalAdmin();
        $managerList = FSMProfile::getProfileListByRole([
            FSMUser::USER_ROLE_ADMIN,
            FSMUser::USER_ROLE_OPERATOR, 
            FSMUser::USER_ROLE_BOOKER,
            FSMUser::USER_ROLE_COORDINATOR,
            FSMUser::USER_ROLE_MANAGER,
            FSMUser::USER_ROLE_TEHNICAL,
            FSMUser::USER_ROLE_LEGAL_TAX,
            FSMUser::USER_ROLE_BOSS,
        ]);
        $userList = FSMProfile::getNameArr(['deleted' => false]);
        return $this->render((empty($client_id) ? 'index' : 'client/index'), [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'clientModel' => (!empty($client_id) ? $clientModel : null),
            'managerList' => $managerList,
            'userList' => $userList,
            'isAdmin' => $isAdmin,
        ]);        
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($date = null, $client_id = null, $manager_id = null)
    {
        if (isset($client_id) && (($fromClientModel = Client::findOne($client_id)) === null)) {
            throw new Exception('The requested page does not exist.');
        }          
                
        $model = new $this->defaultModel;

        $modelArr = [
            'Event' => $model,
        ];

        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            // ajax validation
            return $this->performAjaxMultipleValidation($modelArr);
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                if(!empty($model->remind_day) || !empty($model->remind_hours)){
                    $seconds = (!empty($model->remind_day) ? $model->remind_day : 0) * 86400;
                    $parsed = (!empty($model->remind_hours) ? date_parse($model->remind_hours) : [
                        'hour' => 0,
                        'minute' => 0,
                        'second' => 0,
                    ]);
                    $hourSeconds = $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];
                    $seconds = !empty($model->remind_day) ? $seconds : $hourSeconds;
                    $eventDateTime = new \DateTime($model->event_datetime);
                    $eventDateTime->sub(new \DateInterval("PT{$seconds}S"));
                    $model->remind_datetime = $eventDateTime->format('Y-m-d H:i:s');
                }
                if (!$model->save()) {
                    throw new Exception('Unable to save data! ' . $model->errorMessage);
                } else {
                    $eventProfileModel = FSMBaseModel::createMultiple(EventProfile::class);
                    FSMBaseModel::loadMultiple($eventProfileModel, $requestPost);
                    $addedIds = [];
                    foreach ($eventProfileModel as $index => $eventProfile) {
                        if (empty($eventProfile->profile_id) || in_array($eventProfile->profile_id, $addedIds)) {
                            unset($eventProfileModel[$index]);
                            continue;
                        } elseif (!in_array($eventProfile->profile_id, $addedIds)) {
                            $addedIds[] = $eventProfile->profile_id;
                        }
                        $eventProfile->event_id = $model->id;
                    }
                    
                    $eventContactModel = FSMBaseModel::createMultiple(EventContact::class);
                    FSMBaseModel::loadMultiple($eventContactModel, $requestPost);
                    $addedIds = [];
                    foreach ($eventContactModel as $index => $eventContact) {
                        if(empty($eventContact->contact_id) && empty($eventContact->name)){
                            unset($eventContactModel[$index]);
                            continue;
                        } elseif (!in_array($eventContact->clientContactNamePosition, $addedIds)) {
                            $addedIds[] = $eventContact->clientContactNamePosition;
                        } else {
                            unset($eventContactModel[$index]);
                            continue;
                        }
                        $eventContact->event_id = $model->id;
                    }

                    if(!empty($eventContactModel)){
                        if (FSMBaseModel::validateMultiple($eventContactModel)) {
                            foreach ($eventContactModel as $eventContact) {
                                if (!$eventContact->save(false)) {
                                    $message = $eventContact->getErrorSummary();
                                    $message = implode('; ', $message);
                                    throw new Exception('Unable to save data! ' . $message);
                                }
                            }
                        }
                    }

                    $recipientArr = [$model->manager->user->id];
                    if (!empty($eventProfileModel)) {
                        if (FSMBaseModel::validateMultiple($eventProfileModel)) {
                            foreach ($eventProfileModel as $eventProfile) {
                                if (!$eventProfile->save(false)) {
                                    $message = $eventProfile->getErrorSummary();
                                    $message = implode('; ', $message);
                                    throw new Exception('Unable to save data! ' . $message);
                                }elseif(!empty($model->send_email)){
                                    $user = $eventProfile->profile->user;
                                    if (!in_array($user->id, $recipientArr)) {
                                        $recipientArr[] = $user->id;
                                    }
                                }
                            }
                        }
                    }

                    $eventResursModel = FSMBaseModel::createMultiple(EventEventResurs::class);
                    FSMBaseModel::loadMultiple($eventResursModel, $requestPost);
                    $addedIds = [];
                    foreach ($eventResursModel as $index => $eventResurs) {
                        if (empty($eventResurs->event_resurs_id) || in_array($eventResurs->event_resurs_id, $addedIds)) {
                            unset($eventResursModel[$index]);
                            continue;
                        } elseif (!in_array($eventResurs->event_resurs_id, $addedIds)) {
                            $addedIds[] = $eventResurs->event_resurs_id;
                        }
                        $eventResurs->event_id = $model->id;
                    }

                    if (!empty($eventResursModel)) {
                        if (FSMBaseModel::validateMultiple($eventResursModel)) {
                            foreach ($eventResursModel as $eventResurs) {
                                if (!$eventResurs->save(false)) {
                                    $message = $eventResurs->getErrorSummary();
                                    $message = implode('; ', $message);
                                    throw new Exception('Unable to save data! ' . $message);
                                }
                            }
                        }
                    }
                    
                    if(!empty(Yii::$app->params['ENABLE_GOOGLE_CALENDAR'])){
                        $redirectUrl = Url::to(['/google-api/auth'], true);
                        $calendarData = Event::getGoogleCalendarData($model->manager_id);
                        if(!empty($calendarData['calendarId'])){
                            $googleApi = new FSMGoogleCalendarApi($calendarData['username'], $calendarData['calendarId'], $redirectUrl);
                            if ($googleApi->checkIfCredentialFileExists()) {
                                $event = $model->buildGoogleCalendarEvent($googleApi, $eventProfileModel, $eventContactModel);

                                $calEvent = $googleApi->createGoogleCalendarEvent($event);
                                if(!empty($calEvent)){
                                    $newId = $calEvent->getId();
                                    $model->updateAttributes(['external_id' => $newId]);
                                    //Yii::$app->response->data = "New event added with id: " . $newId;
                                }else{
                                    $message = Yii::t('event', 'New event not added to Google Calendar!');
                                    //Yii::$app->response->data = $message;
                                    throw new Exception($message);
                                }
                            }  else {
                                return $this->redirect(['/event/google-auth', 'profile_id' => $model->manager_id]);
                            }
                        }
                    }else{
                        $model->sendNotification($recipientArr);
                    }

                    $transaction->commit();
                }
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                if ($isPjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return 'reload';
                }else{
                    return $this->redirect((empty($client_id) ? 'index' : ['index', 'client_id' => $client_id]));
                }                   
            }
        } else {
            EventUIAsset::register(Yii::$app->getView());

            $eventProfileModel = [new EventProfile()];
            $eventContactModel = [new EventContact()];
            $eventResursModel = [new EventEventResurs()];

            $model->abonent_id = $model->userAbonentId;            
            if(empty($date)){
                $model->event_datetime = date('d-m-Y 9:00');
                $model->next_datetime = date('d-m-Y 9:00');
            }else{
                $model->event_datetime = date('d-m-Y 9:00', strtotime($date));
                $model->next_datetime = date('d-m-Y 9:00', strtotime($date));
            }
            $model->remind_day = 0;
            $model->remind_hours = 0;
            $model->client_id = (!empty($client_id) ? $client_id : null);
            //$model->send_email = 0;

            $isAdmin = FSMUser::getIsPortalAdmin();
            $eventResursList = EventResurs::getNameArr();
            $clientList = Client::getNameArr(['client.deleted' => 0]);
            $managerList = FSMProfile::getProfileListByRole([
                FSMUser::USER_ROLE_ADMIN,
                FSMUser::USER_ROLE_OPERATOR, 
                FSMUser::USER_ROLE_BOOKER,
                FSMUser::USER_ROLE_COORDINATOR,
                FSMUser::USER_ROLE_MANAGER,
                FSMUser::USER_ROLE_TEHNICAL,
                FSMUser::USER_ROLE_LEGAL_TAX,
                FSMUser::USER_ROLE_BOSS,
            ]);
            $profileId = Yii::$app->user->identity->profile->id;
            if(!empty($manager_id)){
                $model->manager_id = $manager_id;
            }elseif (array_key_exists($profileId, $managerList)) {
                $model->manager_id = $profileId;
            }
            
            if(!empty(Yii::$app->params['ENABLE_GOOGLE_CALENDAR'])){
                $this->rememberBackUrl(Yii::$app->request->url, 'google-api');
                if(!empty($model->manager_id)){
                    $calendarData = Event::getGoogleCalendarData($model->manager_id);
                    if(!empty($calendarData['calendarId'])){
                        $googleApi = new FSMGoogleCalendarApi($calendarData['username'], $calendarData['calendarId']);
                        if (!$googleApi->checkIfCredentialFileExists()) {
                            return $this->redirect(['google-auth']);
                        }
                    }                
                }
            }
                        
            $participantList = FSMProfile::getProfileList();
            $data = [
                'model' => $model,
                'eventProfileModel' => $eventProfileModel,
                'eventContactModel' => $eventContactModel,
                'eventResursModel' => $eventResursModel,
                'fromClientModel' => (!empty($client_id) ? $fromClientModel : null),
                'eventResursList' => $eventResursList,
                'clientList' => $clientList,
                'managerList' => $managerList,
                'participantList' => $participantList,
                'isAdmin' => $isAdmin,
                'isModal' => $isAjax,
            ];
            if ($isAjax) {
                return $this->renderAjax((empty($client_id) ? 'create' : 'client/create'), $data);
            } else {
                return $this->render((empty($client_id) ? 'create' : 'client/create'), $data);
            }
        }
    }

    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $client_id = null)
    {
        if (isset($client_id) && (($fromClientModel = Client::findOne($client_id)) === null)) {
            throw new Exception('The requested page does not exist.');
        }        
        $model = $this->findModel($id);
        
        $clientModel = !empty($fromClientModel) ? $fromClientModel : (!empty($model->client_id) ? $model->client : new Client());
        $eventProfileModel = $model->eventProfiles;
        $eventProfileModel = !empty($eventProfileModel) ? $eventProfileModel : [new EventProfile()];
        $eventContactModel = $model->eventContacts;
        $eventContactModel = !empty($eventContactModel) ? $eventContactModel : [new EventContact()];
        $eventResursModel = $model->eventResurs;
        $eventResursModel = !empty($eventResursModel) ? $eventResursModel : [new EventEventResurs()];
        
        $modelArr = [
            'Event' => $model,
        ];

        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && $isAjax) {
            // ajax validation
            return $this->performAjaxMultipleValidation($modelArr);
        }

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                if (!$model->save()) {
                    throw new Exception('Unable to save data! ' . $model->errorMessage);
                }
                        
                $oldIDs = isset($eventProfileModel[0]) && !empty($eventProfileModel[0]->id) ? ArrayHelper::map($eventProfileModel, 'id', 'id') : [];

                $eventProfileModel = FSMBaseModel::createMultiple(EventProfile::class, $eventProfileModel);
                FSMBaseModel::loadMultiple($eventProfileModel, $requestPost);
                $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($eventProfileModel, 'id', 'id')));
                if (!empty($deletedIDs)) {
                    EventProfile::deleteByIDs($deletedIDs);
                }

                $addedIds = $newIds = [];
                foreach ($eventProfileModel as $index => $eventProfile) {
                    if (empty($eventProfile->profile_id) || in_array($eventProfile->profile_id, $addedIds)) {
                        unset($eventProfileModel[$index]);
                        continue;
                    } elseif (!in_array($eventProfile->profile_id, $addedIds)) {
                        $addedIds[] = $eventProfile->profile_id;
                        if(empty($eventProfile->id)){
                            $newIds[] = $eventProfile->profile_id;
                        }
                    }
                    $eventProfile->event_id = $model->id;
                }

                $oldIDs = isset($eventContactModel[0]) && !empty($eventContactModel[0]->id) ? ArrayHelper::map($eventContactModel, 'id', 'id') : [];

                $eventContactModel = FSMBaseModel::createMultiple(EventContact::class, $eventContactModel);
                FSMBaseModel::loadMultiple($eventContactModel, $requestPost);
                $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($eventContactModel, 'id', 'id')));
                if (!empty($deletedIDs)) {
                    EventContact::deleteByIDs($deletedIDs);
                }

                $addedIds = [];
                foreach ($eventContactModel as $index => $eventContact) {
                    if(empty($eventContact->contact_id) && empty($eventContact->name)){
                        unset($eventContactModel[$index]);
                        continue;
                    } elseif (!in_array($eventContact->clientContactNamePosition, $addedIds)) {
                        $addedIds[] = $eventContact->clientContactNamePosition;
                    } else {
                        unset($eventContactModel[$index]);
                        continue;
                    }
                    $eventContact->event_id = $model->id;
                }
                    
                $recipientArr = [];
                if (!empty($eventProfileModel)) {
                    if (FSMBaseModel::validateMultiple($eventProfileModel)) {
                        foreach ($eventProfileModel as $eventProfile) {
                            if (!$eventProfile->save(false)) {
                                $message = $eventProfile->getErrorSummary();
                                $message = implode('; ', $message);
                                throw new Exception('Unable to save data! ' . $message);
                            }elseif(!empty($model->send_email) && 
                                in_array($eventProfile->profile_id, $newIds) &&
                                in_array($model->status, [
                                    Event::EVENT_STATUS_PLANNED,
                                ])
                            ){
                                $user = $eventProfile->profile->user;
                                $recipientArr[] = $user->id;
                            }
                        }
                    }
                }

                if(!empty($eventContactModel)){
                    if (FSMBaseModel::validateMultiple($eventContactModel)) {
                        foreach ($eventContactModel as $eventContact) {
                            if (!$eventContact->save(false)) {
                                $message = $eventContact->getErrorSummary();
                                $message = implode('; ', $message);
                                throw new Exception('Unable to save data! ' . $message);
                            }
                        }
                    }
                }
                
                $oldIDs = isset($eventResursModel[0]) && !empty($eventResursModel[0]->id) ? ArrayHelper::map($eventResursModel, 'id', 'id') : [];

                $eventResursModel = FSMBaseModel::createMultiple(EventEventResurs::class, $eventResursModel);
                FSMBaseModel::loadMultiple($eventResursModel, $requestPost);
                $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($eventResursModel, 'id', 'id')));
                if (!empty($deletedIDs)) {
                    EventEventResurs::deleteByIDs($deletedIDs);
                }

                $addedIds = [];
                foreach ($eventResursModel as $index => $eventResurs) {
                    if (empty($eventResurs->event_resurs_id) || in_array($eventResurs->event_resurs_id, $addedIds)) {
                        unset($eventResursModel[$index]);
                        continue;
                    } elseif (!in_array($eventResurs->event_resurs_id, $addedIds)) {
                        $addedIds[] = $eventResurs->event_resurs_id;
                    }
                    $eventResurs->event_id = $model->id;
                }

                if (!empty($eventResursModel)) {
                    if (FSMBaseModel::validateMultiple($eventResursModel)) {
                        foreach ($eventResursModel as $eventResurs) {
                            if (!$eventResurs->save(false)) {
                                $message = $eventResurs->getErrorSummary();
                                $message = implode('; ', $message);
                                throw new Exception('Unable to save data! ' . $message);
                            }
                        }
                    }
                }
                
                if(in_array($model->status, [
                    Event::EVENT_STATUS_NEGATIVE,
                    Event::EVENT_STATUS_LAST,
                ])){
                }
                
                if(!empty(Yii::$app->params['ENABLE_GOOGLE_CALENDAR']) && !empty($model->external_id)){
                    $redirectUrl = Url::to(['/google-api/auth'], true);
                    $calendarData = Event::getGoogleCalendarData($model->manager_id);
                    if(!empty($calendarData['calendarId'])){
                        $googleApi = new FSMGoogleCalendarApi($calendarData['username'], $calendarData['calendarId'], $redirectUrl);
                        if ($googleApi->checkIfCredentialFileExists()) {
                            $event = $model->buildGoogleCalendarEvent($googleApi, $eventProfileModel, $eventContactModel);

                            if(!$googleApi->editGoogleCalendarEvent($model->external_id, $event)){
                                $message = Yii::t('event', 'Google Calendar Event is not updated!');
                                //Yii::$app->response->data = $message;
                                throw new Exception($message);
                            }
                        } else {
                            return $this->redirect(['/event/google-auth', 'profile_id' => $model->manager_id]);
                        }                            
                    }
                }else{
                    $model->sendNotification($recipientArr);
                }                

                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirectToBackUrl($id);
            }
        } else {
            $this->rememberBackUrl($model->backURL, $id);
            EventUIAsset::register(Yii::$app->getView());

            $model->event_datetime = !empty($model->event_datetime) ? date('d-m-Y, H:i', strtotime($model->event_datetime)) : null;
            $model->event_datetime_finish = !empty($model->event_datetime_finish) ? date('d-m-Y, H:i', strtotime($model->event_datetime_finish)) : null;
            if(!empty($model->next_datetime)) {
                $model->next_datetime = date('d-m-Y, H:i', strtotime($model->next_datetime));
            }else{
                $model->next_datetime = date('d-m-Y, H:i');
            }
            $model->periodical_last_date = !empty($model->periodical_last_date) ? date('d-m-Y', strtotime($model->periodical_last_date)) : null;
            $model->periodical_next_date = !empty($model->periodical_next_date) ? date('d-m-Y', strtotime($model->periodical_next_date)) : null;
            $model->periodical_finish_date = !empty($model->periodical_finish_date) ? date('d-m-Y', strtotime($model->periodical_finish_date)) : null;
            
            $isAdmin = FSMUser::getIsPortalAdmin();
            $eventResursList = EventResurs::getNameArr();
            $clientList = Client::getNameArr(['client.deleted' => 0]);
            $managerList = FSMProfile::getProfileListByRole([
                FSMUser::USER_ROLE_ADMIN,
                FSMUser::USER_ROLE_OPERATOR, 
                FSMUser::USER_ROLE_BOOKER,
                FSMUser::USER_ROLE_COORDINATOR,
                FSMUser::USER_ROLE_MANAGER,
                FSMUser::USER_ROLE_TEHNICAL,
                FSMUser::USER_ROLE_LEGAL_TAX,
                FSMUser::USER_ROLE_BOSS,
            ]);
            $participantList = FSMProfile::getProfileList();
            return $this->render((empty($client_id) ? 'update' : 'client/update'), [
                'model' => $model,
                'eventProfileModel' => $eventProfileModel,
                'eventContactModel' => $eventContactModel,
                'eventResursModel' => $eventResursModel,
                'fromClientModel' => (!empty($client_id) ? $fromClientModel : null),
                'eventResursList' => $eventResursList,
                'clientList' => $clientList,
                'managerList' => $managerList,
                'participantList' => $participantList,
                'isAdmin' => $isAdmin,
                'isModal' => false,
            ]);
        }
    }

    /**
     * Displays a single model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $client_id = null, $ajax = false)
    {
        if (isset($client_id) && (($clientModel = Client::findOne($client_id)) === null)) {
            throw new Exception('The requested page does not exist.');
        }        
        ButtonDeleteAsset::register(Yii::$app->getView());
        $model = $this->findModel($id);
        $eventProfileModel = $model->eventProfiles;
        $eventProfileModel = !empty($eventProfileModel) ? $eventProfileModel : [new EventProfile()];
        $eventContactModel = $model->eventContacts;
        $eventContactModel = !empty($eventContactModel) ? $eventContactModel : [new EventContact()];
        $eventResursModel = $model->eventResurs;
        $eventResursModel = !empty($eventResursModel) ? $eventResursModel : [new EventEventResurs()];
        $isAdmin = FSMUser::getIsPortalAdmin();
        if ($ajax) {
            return $this->renderAjax((empty($client_id) ? 'view' : 'client/view'), [
                'model' => $model,
                'eventProfileModel' => $eventProfileModel,
                'eventContactModel' => $eventContactModel,
                'eventResursModel' => $eventResursModel,
                'clientModel' => (!empty($client_id) ? $clientModel : null),
                'isAdmin' => $isAdmin,
                'isModal' => true,
            ]);
        } else {
            return $this->render((empty($client_id) ? 'view' : 'client/view'), [
                'model' => $model,
                'eventProfileModel' => $eventProfileModel,
                'eventContactModel' => $eventContactModel,
                'eventResursModel' => $eventResursModel,
                'clientModel' => (!empty($client_id) ? $clientModel : null),
                'isAdmin' => $isAdmin,
                'isModal' => false,
            ]);
        }        
    }

    public function actionSendRemind()
    {
        $event = new Event();
        $event->sendEmailRemind();
        return $this->goHome();
    }
    
    
    /**
     * Deletes an existing single model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            $model = $this->findModel($id);
            $backUrl = $model->backURL;
            $backUrl = is_array($backUrl) ? $backUrl[0] : $backUrl;
            $pjax = $this->pjaxIndex && (strpos($backUrl, 'view') === false);
            if($model->delete()){
                if(!empty($model->external_id) && !empty(Yii::$app->params['ENABLE_GOOGLE_CALENDAR'])){
                    $calendarData = Event::getGoogleCalendarData($model->manager_id);
                    if(!empty($calendarData['calendarId'])){
                        $googleApi = new FSMGoogleCalendarApi($calendarData['username'], $calendarData['calendarId']);
                        if ($googleApi->checkIfCredentialFileExists()) {
                            $eventId = $model->external_id;
                            $googleApi->deleteGoogleCalendarEvent($eventId);
                            //Yii::$app->response->data = "Event deleted";
                        } else {
                            return $this->redirect(['/event/google-auth', 'profile_id' => $model->manager_id]);
                        }                
                    }
                }
            }
            $transaction->commit();            
        } catch (\Exception $e) {
            $transaction->rollBack();
            $message = $e->getMessage();
            if(!$pjax){
                Yii::$app->getSession()->setFlash('error', $message);
            }            
            Yii::error($message, __METHOD__);
        } finally {
            if(!$pjax){
                return $this->redirect(['index']);
            }else{
                return;
            }
        }         
    }
    
    public function actionGoogleAuth($profile_id = null)
    {
        $redirectUrl = Url::to(['/event/google-auth', 'profile_id' => $profile_id], true);
        $calendarData = Event::getGoogleCalendarData($profile_id);
        if(!empty($calendarData['calendarId'])){
            $googleApi = new FSMGoogleCalendarApi($calendarData['username'], $calendarData['calendarId'], $redirectUrl);
            if (!$googleApi->checkIfCredentialFileExists()) {
                $googleApi->generateGoogleApiAccessToken();
            }
            $message = Yii::t('event', "Google calendar API authorization done!");
            //Yii::$app->response->data = $message;
            if(!empty($_GET['code'])){
                Yii::$app->getSession()->setFlash('success', $message);
                return $this->redirectToBackUrl('google-api');
            }
        }else{
            $message = Yii::t('event', "Google calendar for {username} is not defined!", ['username' => $calendarData['username']]);
            Yii::$app->response->data = $message;
        }
    }    
}
