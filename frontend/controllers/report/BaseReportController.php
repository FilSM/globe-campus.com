<?php

namespace frontend\controllers\report;

use Yii;
use yii\helpers\ArrayHelper;

use common\controllers\FilSMController;
use common\models\bill\search\BillSearch;
use common\models\user\FSMUser;
use common\models\client\Project;
use common\models\client\Client;

class BaseReportController extends FilSMController
{
    private function getClientIds($clientList = null)
    {
        $user = Yii::$app->user->identity;
        if(isset($user) && 
            $user->hasRole([
                FSMUser::USER_ROLE_AGENT, 
                FSMUser::USER_ROLE_CLIENT,
                FSMUser::USER_ROLE_LIMIT_USER,
            ])
        ){
            if(empty($clientList)){
                $clientList = Client::getNameArr(['client.deleted' => 0]);
            }
            $clientIds = array_keys($clientList);
        }else{
            $clientIds = [];        
        }
        return $clientIds;
    }
    
    public function actionDebitorCreditor()
    {
        $userAbonentId = Yii::$app->session->get('user_current_abonent_id');
        $list = Client::find(true)
            ->select(['client.id', 'client.name'])
            ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = client.id AND ac.abonent_id = '. $userAbonentId)
            ->andWhere(['client.deleted' => 0])
            ->andWhere(['not', ['ac.client_group_id' => null]])
            ->orderBy('client.name')
            ->asArray()
            ->all();
        $clientList = ArrayHelper::map($list, 'id', 'name');
        
        $params = Yii::$app->request->getQueryParams();
        $clientIds = !empty($params['client_id']) ? $params['client_id'] : [];
        if(empty($clientIds)){
            $clientIds = $this->getClientIds($clientList);
        }else{
            unset($params['client_id']);
        }
        
        $searchModel = new BillSearch();
        $dataProvider = $searchModel->searchDebitorCreditorReport($params, $clientIds);
        
        return $this->render('debitor-creditor', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'clientList' => $clientList,
        ]);        
    }

    public function actionEbitda()
    {
        $clientIds = $this->getClientIds();
        
        $params = Yii::$app->request->getQueryParams();
        $searchModel = new BillSearch();
        $dataProvider = $searchModel->searchEbitdaReport($params, $clientIds);        
        
        $projectList = Project::getNameArr(['project.deleted' => false]);
        return $this->render('ebitda', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'projectList' => $projectList,
        ]);        
    }

    public function actionEbitdaClientBase()
    {
        $clientList = Client::getNameArr(['client.deleted' => 0]);

        $params = Yii::$app->request->getQueryParams();
        $clientIds = !empty($params['client_id']) ? $params['client_id'] : [];
        if(empty($clientIds)){
            $clientIds = $this->getClientIds($clientList);
        }else{
            unset($params['client_id']);
        }

        $searchModel = new BillSearch();
        $dataProvider = $searchModel->searchEbitdaClientBaseReport($params, $clientIds);        
        return $this->render('ebitda-client', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'clientList' => $clientList,
            'base' => true,
        ]);        
    }
    
    public function actionEbitdaClientPlus()
    {
        $clientList = Client::getNameArr(['client.deleted' => 0]);

        $params = Yii::$app->request->getQueryParams();
        $clientIds = !empty($params['client_id']) ? $params['client_id'] : [];
        if(empty($clientIds)){
            $clientIds = $this->getClientIds($clientList);
        }else{
            unset($params['client_id']);
        }

        $searchModel = new BillSearch();
        $dataProvider = $searchModel->searchEbitdaClientPlusReport($params, $clientIds);        
        return $this->render('ebitda-client', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'clientList' => $clientList,
        ]);        
    }
    
    public function actionVat()
    {
        $clientList = Client::getNameArr(['client.deleted' => 0]);
        
        $params = Yii::$app->request->getQueryParams();
        $clientIds = !empty($params['client_id']) ? $params['client_id'] : [];
        if(empty($clientIds)){
            $clientIds = $this->getClientIds($clientList);
        }else{
            unset($params['client_id']);
        }
        
        $searchModel = new BillSearch();
        $dataProvider = $searchModel->searchVatReport($params, $clientIds);        
        
        return $this->render('vat', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'clientList' => $clientList,
        ]);
    }

    public function actionPayment()
    {
        $params = Yii::$app->request->getQueryParams();
        $searchModel = new BillSearch();
        $notPaidDataProvider = $searchModel->searchNotPaidReport($params);
        $paidDataProvider = $searchModel->searchPaidReport($params);

        if(isset($_POST['export_type'])){
            return $this->render('payment-index', [
                'dataProvider' => isset($_POST['exportFull_bill-index-menu-not-paid']) ? $notPaidDataProvider : (isset($_POST['exportFull_bill-index-menu-paid']) ? $paidDataProvider : null),
                'searchModel' => $searchModel,
                'client_id' => null,
                'tableId' => isset($_POST['exportFull_bill-index-menu-not-paid']) ? 'not-paid' : (isset($_POST['exportFull_bill-index-menu-paid']) ? 'paid' : null),
            ]);
        }else{
            return $this->render('payment', [
                'notPaidDataProvider' => $notPaidDataProvider,
                'paidDataProvider' => $paidDataProvider,
                'searchModel' => $searchModel,
            ]);        
        }
    }

}
