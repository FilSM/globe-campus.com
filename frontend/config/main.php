<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'practical-a-frontend',
        ],
    ],
    
    'modules' => [
        'admin' => [
            'class' => 'mdm\admin\Module',
            'layout' => 'top-menu',
            'controllerMap' => [
                'user' => 'backend\controllers\mdm\FSMUserController',
                'assignment' => 'backend\controllers\mdm\FSMAssignmentController',
                'role' => 'backend\controllers\mdm\FSMRoleController',
                'permission' => 'backend\controllers\mdm\FSMPermissionController',
                'route' => 'backend\controllers\mdm\FSMRouteController',
                'rule' => 'backend\controllers\mdm\FSMRuleController',
            ]
        ],
    ],
    
    'params' => $params,
];
