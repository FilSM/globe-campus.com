<?php

namespace console\controllers;

use Yii;
use yii\helpers\Url;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\base\Exception;

use common\models\bill\ReservedId;
use common\models\user\FSMUser;

class DaemonController extends Controller
{
    const CRON_USER_ID = 1;
    
    private $user;

    public $execDate;

    public function init() {
        $this->user = FSMUser::findOne(DaemonController::CRON_USER_ID);
        Yii::$app->user->setIdentity($this->user);
        parent::init();
    }

    /**
     * Returns the names of valid options for the action (id)
     * An option requires the existence of a public member variable whose
     * name is the option name.
     * Child classes may override this method to specify possible options.
     *
     * Note that the values setting via options are not available
     * until [[beforeAction()]] is being called.
     *
     * @param string $actionID the action id of the current request
     * @return string[] the names of the options valid for the action
     */
    public function options($actionID)
    {
        return ArrayHelper::merge(parent::options($actionID), [
            'execDate',
        ]);
    }
    
    public function actionIndex()
    {
        echo 'execDate: '.($this->execDate ?? '').PHP_EOL;
        echo "Yes, cron service is running.";
    }

    // called every five minutes
    public function actionFrequent()
    {
    
//exec('whoami;', $out);
//exec('chmod -R 777 /opt/web/globe-campus.com/web/console/runtime/queue', $out);
//print_r($out);
//exit;

        // */5 * * * *     php /var/www/www-root/data/www/DOMAIN-NAME/yii daemon/frequent >> /var/www/www-root/data/www/DOMAIN-NAME/console/runtime/logs/cron.log 2>&1
        $time_start = microtime(true);
	
	try {
            $eventModel = new \common\models\event\Event();
            $countSent = $eventModel->sendEmailRemind($time_start);
            unset($eventModel);
            $time_end = microtime(true);
            if(!empty($countSent)){
                echo PHP_EOL.date('Y-m-d H:i:s').': "sendEmailRemind" processing for ' . ($time_end - $time_start) . ' seconds';
            }

            // Обработка очереди 
            $queue = Yii::$app->queue; 
            $queue->run(false);

            $date = new \DateTime();
            $date->sub(new \DateInterval('PT2H'));
            ReservedId::deleteAll(['<=', 'create_time', $date->format('Y-m-d H:i:s')]);

            //$time_end = microtime(true);
            //echo PHP_EOL.date('Y-m-d H:i:s').': "actionFrequent" processing for ' . ($time_end - $time_start) . ' seconds';
	} catch (Exception $exc) {
            echo PHP_EOL.date('Y-m-d H:i:s').': "actionFrequent" processing with error: '.PHP_EOL.$exc->getTraceAsString();
            echo PHP_EOL;
        } finally {
            //echo PHP_EOL;
        }

    }

    // called every fifteen minutes
    public function actionQuarter()
    {
        
    }

    // called every hour
    public function actionHourly()
    {
        $current_hour = date('G');
        if ($current_hour % 4) {
            // every four hours
        }
        if ($current_hour % 6) {
            // every six hours
        }
    }

    // called every day
    public function actionDaily()
    {
    	// 0 4 * * *       php /var/www/www-root/data/www/DOMAIN-NAME/yii daemon/daily >> /var/www/www-root/data/www/DOMAIN-NAME/console/runtime/logs/cron.log 2>&1
        $time_start = microtime(true);
        $execDate = ($this->execDate ?? null);
        
        echo PHP_EOL.date('Y-m-d H:i:s'). str_repeat('=', 50);
        
        try {
            $billModel = new \common\models\bill\Bill();
            $totalUpdated = $billModel->checkDelayed();
            echo PHP_EOL.date('Y-m-d H:i:s').': "checkDelayed" updated ' . $totalUpdated . ' bills';
        } catch (Exception | \yii\base\ErrorException | \Throwable $exc) {
            echo PHP_EOL.date('Y-m-d H:i:s').': "actionDaily" processing with error: '.PHP_EOL.$exc->getTraceAsString();
        } finally {
            echo PHP_EOL;
        }
        
        try {
            $agreementModel = new \common\models\client\Agreement();
            $totalUpdated = $agreementModel->checkOverdue();
            echo PHP_EOL.date('Y-m-d H:i:s').': "checkOverdue" updated ' . $totalUpdated . ' agreements';
        } catch (Exception | \yii\base\ErrorException | \Throwable $exc) {
            echo PHP_EOL.date('Y-m-d H:i:s').': "actionDaily" processing with error: '.PHP_EOL.$exc->getTraceAsString();
        } finally {
            echo PHP_EOL;
        }
        
        try {
            $billTemplateModel = new \common\models\bill\BillTemplate();
            $totalGenerated = $billTemplateModel->generateBill($execDate);
            echo PHP_EOL.date('Y-m-d H:i:s').': "generateBill" created ' . $totalGenerated . ' bills';
        } catch (Exception | \yii\base\ErrorException | \Throwable $exc) {
            echo PHP_EOL.date('Y-m-d H:i:s').': "actionDaily" processing with error: '.PHP_EOL.$exc->getTraceAsString();
        } finally {
            echo PHP_EOL;
        }
        
        try {
            $eventModel = new \common\models\event\Event();
            $totalGenerated = $eventModel->generateEvent($execDate);
            echo PHP_EOL.date('Y-m-d H:i:s').': "generateEvent" created ' . $totalGenerated . ' events';
        } catch (Exception | \yii\base\ErrorException | \Throwable $exc) {
            echo PHP_EOL.date('Y-m-d H:i:s').': "actionDaily" processing with error: '.PHP_EOL.$exc->getTraceAsString();
        } finally {
            echo PHP_EOL;
        }
        
        try {
            $currencyRateModel = new \common\models\ValutaRate();
            $totalInserted = $currencyRateModel->loadFromLB();
            echo PHP_EOL.date('Y-m-d H:i:s').': '.$totalInserted.' currency rates was loaded';
        } catch (Exception | \yii\base\ErrorException | \Throwable $exc) {
            echo PHP_EOL.date('Y-m-d H:i:s').': "actionDaily" processing with error: '.PHP_EOL.$exc->getTraceAsString();
        } finally {
            echo PHP_EOL;
        }
        
        $time_end = microtime(true);
        echo PHP_EOL.date('Y-m-d H:i:s').': "actionDaily" processing for ' . ($time_end - $time_start) . ' seconds';
        echo PHP_EOL;
    }
}
