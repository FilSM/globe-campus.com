<?php

use yii\db\Migration;

/**
 * Class m170822_101012_init_db
 */
class m170822_101012_init_db extends Migration
{

    /**
     * @inheritdoc
     */
    public function safeUp()
    {

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
    
    // Use up()/down() to run migration code without a transaction.
    /**
     * @inheritdoc
     */
    public function up()
    {
        $sqlExternal = 
"
SET SQL_MODE = \"NO_AUTO_VALUE_ON_ZERO\";
SET time_zone = \"+00:00\";

-- --------------------------------------------------------

ALTER TABLE `user` CHANGE `password_hash` `password_hash` VARCHAR(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `user` ADD `superuser` TINYINT NOT NULL DEFAULT '0' AFTER `email`;
ALTER TABLE `user` ADD `confirmed_at` INT NULL DEFAULT NULL AFTER `superuser`;
ALTER TABLE `user` ADD `unconfirmed_email` VARCHAR(255) NULL DEFAULT NULL AFTER `confirmed_at`;
ALTER TABLE `user` ADD `blocked_at` INT NULL DEFAULT NULL AFTER `unconfirmed_email`;
ALTER TABLE `user` ADD `registration_ip` VARCHAR(45) NULL DEFAULT NULL AFTER `blocked_at`;
ALTER TABLE `user` ADD `last_login_at` INT NULL DEFAULT NULL AFTER `registration_ip`;

-- внещние модули

--
-- Структура таблицы `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Структура таблицы `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


--
-- Структура таблицы `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Структура таблицы `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Структура таблицы `language`
--

CREATE TABLE `language` (
  `language_id` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `name_ascii` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Структура таблицы `language_source`
--

CREATE TABLE `language_source` (
  `id` int(11) NOT NULL,
  `category` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `from_variable` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Структура таблицы `language_translate`
--

CREATE TABLE `language_translate` (
  `id` int(11) NOT NULL,
  `language` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `translation` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Структура таблицы `profile`
--

CREATE TABLE `profile` (
  `id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `short_name` varchar(5) DEFAULT NULL,
  `phone` varchar(20) NOT NULL,
  `public_email` varchar(255) DEFAULT NULL,
  `gravatar_email` varchar(255) DEFAULT NULL,
  `gravatar_id` varchar(32) DEFAULT NULL,
  `gcalendar_id` varchar(100) NULL DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `bio` text,
  `timezone` varchar(50) DEFAULT NULL,
  `sms_auth` TINYINT NULL DEFAULT '0',
  `sms_code` CHAR(4) NULL DEFAULT NULL,
  `sms_auth_exp` DATETIME NULL DEFAULT,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Структура таблицы `social_account`
--

CREATE TABLE `social_account` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Структура таблицы `token`
--

CREATE TABLE `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Индексы таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Индексы таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Индексы таблицы `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`language_id`);

--
-- Индексы таблицы `language_source`
--
ALTER TABLE `language_source`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `language_translate`
--
ALTER TABLE `language_translate`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `language_translate_idx_language` (`language`);

--
-- Индексы таблицы `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profile_user_id` (`user_id`),
  ADD KEY `profile_create_user_id` (`create_user_id`),
  ADD KEY `profile_update_user_id` (`update_user_id`),
  ADD KEY `profile_language_id` (`language_id`);
  
--
-- Индексы таблицы `social_account`
--
ALTER TABLE `social_account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_unique` (`provider`,`client_id`),
  ADD UNIQUE KEY `account_unique_code` (`code`),
  ADD KEY `fk_user_account` (`user_id`);

--
-- Индексы таблицы `token`
--
ALTER TABLE `token`
  ADD UNIQUE KEY `token_unique` (`user_id`,`code`,`type`);

--
-- AUTO_INCREMENT для таблицы `language_source`
--
ALTER TABLE `language_source`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `social_account`
--
ALTER TABLE `social_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `language_translate`
--
ALTER TABLE `language_translate`
  ADD CONSTRAINT `language_translate_ibfk_1` FOREIGN KEY (`language`) REFERENCES `language` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `language_translate_ibfk_2` FOREIGN KEY (`id`) REFERENCES `language_source` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `social_account`
--
ALTER TABLE `social_account`
  ADD CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;  

-- DONE EXTERNAL MODULES
";
        $sqlTables = 
"
--
-- Table structure for table `abonent`
--

CREATE TABLE `abonent` (
  `id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL,
  `main_client_id` int(11) NOT NULL,
  `subscription_end_date` date NOT NULL,
  `subscription_type` enum('silver','gold','platinum') NOT NULL DEFAULT 'silver',
  `manager_id` int(11) DEFAULT NULL,
  `simple_workflow` tinyint(1) NULL DEFAULT '0',
  `comment` text,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `abonent_agreement`
--

CREATE TABLE `abonent_agreement` (
  `id` int(11) NOT NULL,
  `abonent_id` int(11) NOT NULL,
  `agreement_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `primary_abonent` tinyint(1) DEFAULT 1,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `abonent_bill`
--

CREATE TABLE `abonent_bill` (
  `id` int(11) NOT NULL,
  `abonent_id` int(11) NOT NULL,
  `bill_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `primary_abonent` tinyint(1) DEFAULT 1,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `abonent_bill_template`
--

CREATE TABLE `abonent_bill_template` (
  `id` int(11) NOT NULL,
  `abonent_id` int(11) NOT NULL,
  `bill_template_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `abonent_client`
--

CREATE TABLE `abonent_client` (
  `id` int(11) NOT NULL,
  `abonent_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `client_group_id` int(11) DEFAULT NULL,
  `it_is` enum('owner','abonent','client') DEFAULT 'client',
  `manager_id` int(11) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `status` enum('active','potential','archived') DEFAULT 'active',
  `debit` decimal(10,2) DEFAULT '0.00',
  `debit_valuta_id` int(11) DEFAULT NULL,
  `uploaded_file_id` int(11) DEFAULT NULL,
  `gen_number` varchar(10) DEFAULT NULL,
  `comment` text
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `abonent_product`
--

CREATE TABLE `abonent_product` (
  `id` int(11) NOT NULL,
  `abonent_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `account_map`
--

CREATE TABLE `account_map` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` INT(4) NOT NULL,
  `description` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `account_map_unique` (`code` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `address_type` enum('start','finish','interim','customs','user','company') NOT NULL DEFAULT 'start',
  `company_name` varchar(100) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `customer_address` tinytext,
  `contact_person` varchar(50) DEFAULT NULL,
  `contact_phone` varchar(20) DEFAULT NULL,
  `contact_email` varchar(50) DEFAULT NULL,
  `apartment_number` varchar(10) DEFAULT NULL,
  `street_number` varchar(10) NOT NULL,
  `route` varchar(100) NOT NULL,
  `district` varchar(100) DEFAULT NULL,
  `political` varchar(100) DEFAULT NULL,
  `sublocality_level_1` varchar(100) DEFAULT NULL,
  `sublocality` varchar(100) DEFAULT NULL,
  `locality` varchar(50) NOT NULL,
  `administrative_area_level_1` varchar(100) DEFAULT NULL,
  `country` varchar(50) NOT NULL,
  `postal_code` varchar(10) NOT NULL,
  `latitude` double(11,8) DEFAULT NULL,
  `longitude` double(11,8) DEFAULT NULL,
  `formated_address` tinytext,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `agreement`
--

CREATE TABLE `agreement` (
  `id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `agreement_type` enum('cooperation','loan','cession','agency','employment') DEFAULT 'cooperation',
  `parent_id` int(11) DEFAULT NULL,
  `first_client_id` int(11) NOT NULL,
  `first_client_role_id` int(11) DEFAULT NULL,
  `second_client_id` int(11) NOT NULL,
  `second_client_role_id` int(11) DEFAULT NULL,
  `third_client_id` int(11) DEFAULT NULL,
  `third_client_role_id` int(11) DEFAULT NULL,
  `number` varchar(50) NOT NULL,
  `signing_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `deferment_payment` smallint(6) NOT NULL DEFAULT '10',
  `summa` decimal(10,2) NOT NULL,
  `valuta_id` int(11) NOT NULL,
  `rate` decimal(10,4) DEFAULT '1.0000',
  `summa_eur` decimal(10,2) DEFAULT NULL,
  `rate_rate` decimal(10,2) DEFAULT NULL,
  `rate_summa` decimal(10,2) DEFAULT NULL,
  `rate_from_date` date DEFAULT NULL,
  `rate_till_date` date DEFAULT NULL,
  `status` enum('potencial','new','signed','overdue','canceled') DEFAULT 'potencial',
  `transfer_status` enum('sent', 'received') DEFAULT NULL,
  `conclusion` enum('oral','written') DEFAULT 'written',
  `comment` text,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `agreement_attachment`
--

CREATE TABLE `agreement_attachment` (
  `id` int(11) NOT NULL,
  `agreement_id` int(11) NOT NULL,
  `uploaded_file_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `agreement_history`
--

CREATE TABLE `agreement_history` (
  `id` INT(11) NOT NULL,
  `agreement_id` INT(11) NOT NULL,
  `action_id` INT(11) NOT NULL,
  `comment` TEXT NULL DEFAULT NULL,
  `create_time` DATETIME NULL DEFAULT NULL,
  `create_user_id` INT(11) NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `agreement_payment`
--

CREATE TABLE `agreement_payment` (
  `id` int(11) NOT NULL,
  `agreement_history_id` int(11) NOT NULL,
  `from_bank_id` int(11) DEFAULT NULL,
  `to_bank_id` int(11) DEFAULT NULL,
  `agreement_id` int(11) NOT NULL,
  `paid_date` DATE NOT NULL,
  `summa` decimal(10,2) NOT NULL,
  `valuta_id` int(11) NOT NULL,
  `rate` decimal(10,4) NOT NULL DEFAULT '1.0000',
  `summa_eur` decimal(10,2) NOT NULL,
  `direction` enum('credit','debet') NOT NULL DEFAULT 'debet',
  `confirmed` DATE DEFAULT NULL,
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `reg_number` varchar(30) DEFAULT NULL,
  `swift` varchar(11) NOT NULL,
  `address` tinytext,
  `home_page` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bill`
--

CREATE TABLE `bill` (
  `id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `agreement_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `child_id` int(11) DEFAULT NULL,
  `doc_type` enum('avans','bill','cr_bill','invoice','debt','cession') NOT NULL DEFAULT 'bill',
  `cession_direction` enum('D','K') DEFAULT NULL,
  `doc_number` varchar(40) NOT NULL,
  `doc_date` date NOT NULL,
  `pay_date` date NOT NULL,
  `paid_date` date DEFAULT NULL,
  `status` enum('prepar','new','ready','signed','prepar_payment','payment','paid','complete','canceled') NOT NULL DEFAULT 'prepar',
  `pay_status` enum('not','part','full','over','setoff','part_closed') DEFAULT 'not',
  `transfer_status` enum('sent', 'received') DEFAULT NULL,
  `mail_status` enum('sent', 'received') DEFAULT NULL,
  `complete_date` date DEFAULT NULL,
  `delayed` tinyint(1) DEFAULT '0',
  `first_client_bank_id` int(11) NOT NULL,
  `second_client_bank_id` int(11) DEFAULT NULL,
  `first_client_pvn_payer` tinyint(1) DEFAULT NULL,
  `second_client_pvn_payer` tinyint(1) DEFAULT NULL,
  `according_contract` tinyint(1) DEFAULT '0',
  `summa` decimal(10,2) NOT NULL,
  `vat` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `valuta_id` int(11) NOT NULL,
  `rate` decimal(10,4) DEFAULT '1.0000',
  `summa_eur` decimal(10,2) DEFAULT NULL,
  `vat_eur` decimal(10,2) DEFAULT NULL,
  `total_eur` decimal(10,2) DEFAULT NULL,
  `manager_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `services_period_from` DATE NULL DEFAULT NULL,
  `services_period_till` DATE NULL DEFAULT NULL,
  `services_period` varchar(50) DEFAULT NULL,
  `doc_key` varchar(32) DEFAULT NULL,
  `loading_address` varchar(100) DEFAULT NULL,
  `unloading_address` varchar(100) DEFAULT NULL,
  `carrier` varchar(50) DEFAULT NULL,
  `transport` varchar(20) DEFAULT NULL,
  `e_signing` tinyint(1) DEFAULT '0',
  `has_act` tinyint(1) DEFAULT '0',
  `justification` varchar(100) DEFAULT NULL,
  `place_service` varchar(255) DEFAULT NULL,
  `comment_special` text,
  `comment` text,
  `returned` tinyint(1) DEFAULT '0',
  `blocked` tinyint(1) DEFAULT '0',
  `bill_template_id` INT(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bill_template`
--

CREATE TABLE `bill_template` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `agreement_id` INT(11) NOT NULL,
  `first_client_bank_id` INT(11) NOT NULL,
  `second_client_bank_id` INT(11) NULL DEFAULT NULL,
  `according_contract` TINYINT(1) NULL DEFAULT '0',
  `summa` DECIMAL(10,2) NOT NULL,
  `vat` DECIMAL(10,2) NOT NULL,
  `total` DECIMAL(10,2) NOT NULL,
  `valuta_id` INT(11) NOT NULL,
  `rate` DECIMAL(10,4) NULL DEFAULT '1.0000',
  `summa_eur` DECIMAL(10,2) NULL DEFAULT NULL,
  `vat_eur` DECIMAL(10,2) NULL DEFAULT NULL,
  `total_eur` DECIMAL(10,2) NULL DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `e_signing` tinyint(1) DEFAULT '0',
  `justification` VARCHAR(100) NULL DEFAULT NULL,
  `place_service` VARCHAR(255) NULL DEFAULT NULL,
  `comment_special` TEXT NULL DEFAULT NULL,
  `comment` TEXT NULL DEFAULT NULL,
  `periodical_day` SMALLINT(6) NULL DEFAULT NULL,
  `periodicity` ENUM('month', 'quarter', 'year') NULL DEFAULT NULL,
  `periodical_last_date` DATE NULL DEFAULT NULL,
  `periodical_next_date` DATE NULL DEFAULT NULL,
  `periodical_finish_date` DATE NULL DEFAULT NULL,
  `create_time` DATETIME NULL DEFAULT NULL,
  `create_user_id` INT(11) NULL DEFAULT NULL,
  `update_time` DATETIME NULL DEFAULT NULL,
  `update_user_id` INT(11) NULL DEFAULT NULL
ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;
-- --------------------------------------------------------

--
-- Table structure for table `bill_history`
--

CREATE TABLE `bill_history` (
  `id` int(11) NOT NULL,
  `bill_id` int(11) NOT NULL,
  `action_id` int(11) NOT NULL,
  `comment` text,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bill_person`
--

CREATE TABLE `bill_person` (
  `id` int(11) NOT NULL,
  `bill_id` int(11) NOT NULL,
  `client_person_id` int(11) NOT NULL,
  `person_order` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bill_product`
--

CREATE TABLE `bill_product` (
  `id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `bill_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_name` varchar(100) DEFAULT NULL,
  `measure_id` int(11) DEFAULT NULL,
  `amount` decimal(10,3) DEFAULT NULL,
  `price` decimal(10,3) DEFAULT NULL,
  `vat` decimal(10,2) DEFAULT NULL,
  `revers` tinyint(1) DEFAULT '0',
  `summa` decimal(10,2) DEFAULT NULL,
  `summa_vat` decimal(10,2) DEFAULT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  `price_eur` decimal(10,3) DEFAULT NULL,
  `summa_eur` decimal(10,2) DEFAULT NULL,
  `summa_vat_eur` decimal(10,2) DEFAULT NULL,
  `total_eur` decimal(10,2) DEFAULT NULL,
  `comment` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bill_template_product`
--

CREATE TABLE `bill_template_product` (
  `id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `bill_template_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_name` varchar(100) DEFAULT NULL,
  `measure_id` int(11) DEFAULT NULL,
  `amount` decimal(10,3) DEFAULT NULL,
  `price` decimal(10,3) DEFAULT NULL,
  `vat` decimal(10,2) DEFAULT NULL,
  `revers` tinyint(1) DEFAULT '0',
  `summa` decimal(10,2) DEFAULT NULL,
  `summa_vat` decimal(10,2) DEFAULT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  `price_eur` decimal(10,3) DEFAULT NULL,
  `summa_eur` decimal(10,2) DEFAULT NULL,
  `summa_vat_eur` decimal(10,2) DEFAULT NULL,
  `total_eur` decimal(10,2) DEFAULT NULL,
  `comment` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `client_type` enum('physical','legal') DEFAULT 'legal',
  `name` varchar(100) NOT NULL,
  `reg_number` varchar(30) NOT NULL,
  `vat_payer` tinyint(1) DEFAULT '0',
  `vat_number` varchar(30) DEFAULT NULL,
  `tax` tinyint(4) DEFAULT '0',
  `legal_country_id` int(11) NOT NULL,
  `legal_address` tinytext NOT NULL,
  `office_country_id` int(11) DEFAULT NULL,
  `office_address` tinytext,
  `uploaded_file_id` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

--
-- Table structure for table `client_bank`
--

CREATE TABLE `client_bank` (
  `id` int(11) NOT NULL,
  `abonent_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `primary` tinyint(1) NOT NULL DEFAULT '0',
  `client_id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `account` varchar(34) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `uploaded_file_id` int(11) DEFAULT NULL,
  `uploaded_pdf_file_id` int(11) DEFAULT NULL,
  `balance` decimal(10,2) DEFAULT NULL,
  `currency` char(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `client_contact`
--

CREATE TABLE `client_contact` (
  `id` int(11) NOT NULL,
  `abonent_id` INT(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `can_sign` tinyint(1) DEFAULT '0',
  `receive_invoice` tinyint(1) DEFAULT '0',
  `term_from` date DEFAULT NULL,
  `term_till` date DEFAULT NULL,
  `share` decimal(5,2) DEFAULT NULL,
  `top_manager` tinyint(1) DEFAULT '0',
  `main` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `client_group`
--

CREATE TABLE `client_group` (
  `id` int(11) NOT NULL,
  `abonent_id` int(11) DEFAULT NULL,
  `name` varchar(64) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `client_role`
--

CREATE TABLE `client_role` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `abonent_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent_event_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `manager_id` int(11) NOT NULL,
  `event_type` enum('meeting','call','email','sms','task','bus_trip','vacation') NOT NULL DEFAULT 'meeting',
  `status` enum('planned','positive','negative','transfer') NOT NULL DEFAULT 'planned',
  `address` varchar(255) DEFAULT NULL,
  `event_date` date DEFAULT NULL,
  `event_time` time DEFAULT NULL,
  `event_datetime` datetime NOT NULL,
  `event_date_finish` date DEFAULT NULL,
  `event_time_finish` time DEFAULT NULL,
  `event_datetime_finish` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `next_event_type` enum('meeting','call','email','sms','task','bus_trip','vacation') DEFAULT NULL,
  `next_date` date DEFAULT NULL,
  `next_time` time DEFAULT NULL,
  `next_datetime` datetime DEFAULT NULL,
  `remind_day` int(11) DEFAULT NULL,
  `remind_hours` time DEFAULT NULL,
  `remind_datetime` datetime DEFAULT NULL,
  `sent_remind_at` datetime DEFAULT NULL,
  `send_email` tinyint(4) DEFAULT NULL,
  `comment` text,
  `result_comment` text,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `event_contact`
--

CREATE TABLE `event_contact` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `short_name` varchar(5) DEFAULT NULL,
  `accepted_at` datetime DEFAULT NULL,
  `refused_at` datetime DEFAULT NULL,
  `comment` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `event_event_resurs`
--

CREATE TABLE `event_event_resurs` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `event_resurs_id` int(11) NOT NULL,
  `color_code` varchar(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `event_profile`
--

CREATE TABLE `event_profile` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `accepted_at` datetime DEFAULT NULL,
  `refused_at` datetime DEFAULT NULL,
  `comment` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `event_resurs`
--

CREATE TABLE `event_resurs` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE `expense` (
  `id` int(11) NOT NULL,
  `abonent_id` int(11) NOT NULL,
  `expense_type_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `doc_number` varchar(20) NOT NULL,
  `doc_date` date NOT NULL,
  `pay_status` tinyint(1) DEFAULT '0',
  `pay_type` enum('cash','card','bank') DEFAULT NULL,
  `first_client_id` int(11) NOT NULL,
  `second_client_id` int(11) NOT NULL,
  `first_client_pvn_payer` tinyint(1) DEFAULT '1',
  `second_client_pvn_payer` tinyint(1) DEFAULT '1',
  `summa` decimal(10,2) NOT NULL,
  `vat` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `valuta_id` int(11) NOT NULL,
  `rate` decimal(10,3) DEFAULT '1.000',
  `summa_eur` decimal(10,2) DEFAULT NULL,
  `vat_eur` decimal(10,2) DEFAULT NULL,
  `total_eur` decimal(10,2) DEFAULT NULL,
  `report_plus` tinyint(1) DEFAULT '0',
  `comment` text,
  `confirmed` date DEFAULT NULL,
  `manual_input` tinyint(1) DEFAULT '1',
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `expense_attachment`
--

CREATE TABLE `expense_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `expense_id` int(11) NOT NULL,
  `uploaded_file_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `expense_attachment_bill_id` (`expense_id`),
  KEY `expense_attachment_uploaded_file_id` (`uploaded_file_id`),
  CONSTRAINT `fk_expense_attachment_bill_id` FOREIGN KEY (`expense_id`) REFERENCES `expense` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_expense_attachment_uploaded_file_id` FOREIGN KEY (`uploaded_file_id`) REFERENCES `files` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `expense_type`
--

CREATE TABLE `expense_type` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `filepath` varchar(255) NOT NULL,
  `fileurl` varchar(255) NOT NULL,
  `filemime` varchar(255) NOT NULL,
  `filesize` int(11) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `language` char(2) NOT NULL,
  `name` varchar(64) NOT NULL,
  `native` varchar(64) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `location_city`
--

CREATE TABLE `location_city` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `region_id` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `location_country`
--

CREATE TABLE `location_country` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `short_name` char(2) DEFAULT NULL,
  `currency` char(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `location_district`
--

CREATE TABLE `location_district` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `region_id` int(11) DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `location_region`
--

CREATE TABLE `location_region` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `measure`
--

CREATE TABLE `measure` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment_confirm`
--

CREATE TABLE `payment_confirm` (
  `id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `client_name` varchar(100) NOT NULL,
  `client_reg_number` varchar(30) NOT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `pay_date` date NOT NULL,
  `status` enum('process','complete') NOT NULL DEFAULT 'process',
  `uploaded_file_id` int(11) DEFAULT NULL,
  `uploaded_pdf_file_id` int(11) DEFAULT NULL,
  `comment` text,
  `action_time` datetime DEFAULT NULL,
  `action_user_id` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

--
-- Table structure for table `bill_confirm`
--

CREATE TABLE `bill_confirm` (
  `id` int(11) NOT NULL,
  `abonent_id` int(11) NOT NULL,
  `payment_confirm_id` int(11) NOT NULL,
  `first_client_account` varchar(34) NOT NULL,
  `second_client_name` varchar(100) DEFAULT NULL,
  `second_client_reg_number` varchar(30) DEFAULT NULL,
  `second_client_account` varchar(34) DEFAULT NULL,
  `second_client_id` int(11) DEFAULT NULL,
  `doc_date` date NOT NULL,
  `doc_number` varchar(35) DEFAULT NULL,
  `bank_ref` varchar(35) NOT NULL,
  `direction` enum('C','D') NOT NULL DEFAULT 'C',
  `summa` decimal(10,2) NOT NULL,
  `currency` char(3) NOT NULL,
  `valuta_id` int(11) DEFAULT NULL,
  `rate` decimal(10,4) DEFAULT '1.0000',
  `comment` tinytext,  
  `manual_input` tinyint(1) DEFAULT '0',
  `find_part` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

--
-- Table structure for table `bill_confirm_link`
--

CREATE TABLE `bill_confirm_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_confirm_id` int(11) NOT NULL,
  `doc_type` enum('bill','agreement','expense','direct','tax') NOT NULL DEFAULT 'bill',
  `doc_subtype` int(11) DEFAULT NULL,
  `history_id` int(11) DEFAULT NULL,
  `payment_id` int(11) DEFAULT NULL,
  `doc_id` int(11) NOT NULL,
  `summa` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bill_confirm_link_bill_confirm_id` (`bill_confirm_id`),
  KEY `bill_confirm_link_history_id` (`history_id`),
  KEY `bill_confirm_link_payment_id` (`payment_id`),
  KEY `bill_confirm_link_doc_id` (`doc_id`),
  KEY `bill_confirm_link_doc_type` (`doc_type`),
  CONSTRAINT `fk_bill_confirm_link_bill_confirm_id` FOREIGN KEY (`bill_confirm_id`) REFERENCES `bill_confirm` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

--
-- Table structure for table `client_bank_balance`
--

CREATE TABLE `client_bank_balance` (
  `id` int(11) NOT NULL,
  `payment_confirm_id` int(11) DEFAULT NULL,
  `account_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `uploaded_file_id` int(11) DEFAULT NULL,
  `uploaded_pdf_file_id` int(11) DEFAULT NULL,
  `balance` decimal(10,2) NOT NULL,
  `currency` char(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment_order`
--

CREATE TABLE `payment_order` (
  `id` int(11) NOT NULL,
  `abonent_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `client_bank_id` int(11) NOT NULL,
  `number` varchar(50) NOT NULL,
  `pay_date` date NOT NULL,
  `status` enum('prepare','sent') NOT NULL DEFAULT 'prepare',
  `file_id` int(11) DEFAULT NULL,
  `comment` text,
  `action_time` datetime DEFAULT NULL,
  `action_user_id` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bill_payment`
--

CREATE TABLE `bill_payment` (
  `id` int(11) NOT NULL,
  `bill_history_id` int(11) NOT NULL,
  `payment_order_id` int(11) DEFAULT NULL,
  `paid_date` date DEFAULT NULL,
  `from_bank_id` int(11) DEFAULT NULL,
  `to_bank_id` int(11) DEFAULT NULL,
  `bill_id` int(11) NOT NULL,
  `summa` decimal(10,2) NOT NULL,
  `valuta_id` int(11) DEFAULT NULL,
  `rate` decimal(10,4) DEFAULT '1.0000',
  `summa_eur` decimal(10,2) DEFAULT NULL,
  `summa_origin` decimal(10,2) DEFAULT NULL,
  `confirmed` date DEFAULT NULL,
  `manual_input` tinyint(1) DEFAULT '1',
  `combine` tinyint(1) DEFAULT '0',
  `need_agreement` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `person_position`
--

CREATE TABLE `person_position` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `measure_id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `prinded` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `profile_client`
--

CREATE TABLE `profile_client` (
  `id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `abonent_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL,
  `abonent_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `vat_taxable` tinyint(1) NOT NULL DEFAULT '1',
  `comment` text,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `project_attachment`
--

CREATE TABLE `project_attachment` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `uploaded_file_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `client_reg_doc`
--

CREATE TABLE `client_reg_doc` (
  `id` int(11) NOT NULL,
  `abonent_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `reg_doc_type_id` int(11) NOT NULL,
  `doc_number` varchar(20) NOT NULL,
  `doc_date` date NOT NULL,
  `expiration_date` date DEFAULT NULL,
  `placement` varchar(100) DEFAULT NULL,
  `notification_days` smallint(6) DEFAULT NULL,
  `uploaded_file_id` int(11) DEFAULT NULL,
  `comment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reg_doc_type`
--

CREATE TABLE `reg_doc_type` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `shareholder`
--

CREATE TABLE `shareholder` (
  `id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `client_id` int(11) NOT NULL,
  `shareholder_id` int(11) NOT NULL,
  `share` decimal(5,2) NOT NULL,
  `term_from` date DEFAULT NULL,
  `term_till` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `valuta`
--

CREATE TABLE `valuta` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `int_0` varchar(20) DEFAULT NULL,
  `int_1` varchar(20) DEFAULT NULL,
  `int_2` varchar(20) DEFAULT NULL,
  `floor_0` varchar(20) DEFAULT NULL,
  `floor_1` varchar(20) DEFAULT NULL,
  `floor_2` varchar(20) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
CREATE TABLE `bill_attachment` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `bill_id` INT(11) NOT NULL,
  `uploaded_file_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `bill_attachment_bill_id` (`bill_id` ASC),
  INDEX `bill_attachment_uploaded_file_id` (`uploaded_file_id` ASC),
  CONSTRAINT `fk_bill_attachment_bill_id`
    FOREIGN KEY (`bill_id`)
    REFERENCES `bill` (`id`)
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_bill_attachment_uploaded_file_id`
    FOREIGN KEY (`uploaded_file_id`)
    REFERENCES `files` (`id`)
    ON DELETE CASCADE
    ON UPDATE RESTRICT)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE `bill_account` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `bill_id` INT(4) NOT NULL,
  `first_account_id` INT(4) NOT NULL,
  `second_account_id` INT(4) NOT NULL,
  `summa` DECIMAL(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `bill_account_first_account_id` (`first_account_id` ASC),
  INDEX `bill_account_second_account_id` (`second_account_id` ASC),
  UNIQUE INDEX `bill_account_unique` (`bill_id` ASC, `first_account_id` ASC, `second_account_id` ASC),
  INDEX `bill_account_bill_id` (`bill_id` ASC),
  CONSTRAINT `fk_bill_account_bill_id`
    FOREIGN KEY (`bill_id`)
    REFERENCES `bill` (`id`)
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_bill_account_first_account_id`
    FOREIGN KEY (`first_account_id`)
    REFERENCES `account_map` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_bill_account_second_account_id`
    FOREIGN KEY (`second_account_id`)
    REFERENCES `account_map` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE `client_invoice_pdf` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `abonent_id` INT(11) NOT NULL,
  `client_id` INT(11) NOT NULL,
  `template_name` VARCHAR(30) NOT NULL,
  `uploaded_file_id` INT(11) NULL DEFAULT NULL,
  `create_time` DATETIME NULL DEFAULT NULL,
  `create_user_id` INT(11) NULL DEFAULT NULL,
  `update_time` DATETIME NULL DEFAULT NULL,
  `update_user_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `client_invoice_pdf_abonent_id` (`abonent_id` ASC),
  INDEX `client_invoice_pdf_client_id` (`client_id` ASC),
  INDEX `client_invoice_pdf_uploaded_file_id` (`uploaded_file_id` ASC),
  INDEX `client_invoice_pdf_create_user_id` (`create_user_id` ASC),
  INDEX `client_invoice_pdf_update_user_id` (`update_user_id` ASC),
  CONSTRAINT `fk_client_invoice_pdf_abonent_id`
    FOREIGN KEY (`abonent_id`)
    REFERENCES `abonent` (`id`)
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_client_invoice_pdf_client_id`
    FOREIGN KEY (`client_id`)
    REFERENCES `client` (`id`)
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_client_invoice_pdf_uploaded_file_id`
    FOREIGN KEY (`uploaded_file_id`)
    REFERENCES `files` (`id`)
    ON DELETE SET NULL
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_client_invoice_pdf_create_user_id`
    FOREIGN KEY (`create_user_id`)
    REFERENCES `user` (`id`)
    ON DELETE SET NULL
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_client_invoice_pdf_update_user_id`
    FOREIGN KEY (`update_user_id`)
    REFERENCES `user` (`id`)
    ON DELETE SET NULL
    ON UPDATE RESTRICT)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE `client_mail_template` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `abonent_id` INT(11) DEFAULT NULL,
  `client_id` INT(11) NOT NULL,
  `cc` VARCHAR(255) NULL,
  `subject` VARCHAR(255) NULL,
  `header` TINYTEXT NULL,
  `body` TINYTEXT NULL,
  `footer` TINYTEXT NULL,
  `uploaded_file_id` INT(11) NULL,
  `create_time` DATETIME NULL DEFAULT NULL,
  `create_user_id` INT(11) NULL DEFAULT NULL,
  `update_time` DATETIME NULL DEFAULT NULL,
  `update_user_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `client_mail_template_abonent_id` (`abonent_id` ASC),
  INDEX `client_mail_template_client_id` (`client_id` ASC),
  INDEX `client_mail_template_uploaded_file_id` (`uploaded_file_id` ASC),
  INDEX `client_mail_template_create_user_id` (`create_user_id` ASC),
  INDEX `client_mail_template_update_user_id` (`update_user_id` ASC);  
  CONSTRAINT `fk_client_mail_template_abonent_id` 
    FOREIGN KEY (`abonent_id`) 
    REFERENCES `abonent` (`id`) 
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_client_mail_template_client_id`
    FOREIGN KEY (`client_id`)
    REFERENCES `client` (`id`)
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_client_mail_template_uploaded_file_id`
    FOREIGN KEY (`uploaded_file_id`)
    REFERENCES `files` (`id`)
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_client_mail_template_create_user_id`
    FOREIGN KEY (`create_user_id`)
    REFERENCES `user` (`id`)
    ON DELETE SET NULL
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_client_mail_template_update_user_id`
    FOREIGN KEY (`update_user_id`)
    REFERENCES `user` (`id`)
    ON DELETE SET NULL
    ON UPDATE RESTRICT)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE `convention` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `abonent_id` int(11) NOT NULL,
  `agreement_id` int(11) NOT NULL,
  `doc_type` enum('set_off','assignment','meeting','settlement') NOT NULL DEFAULT 'set_off',
  `doc_number` varchar(40) NOT NULL,
  `doc_date` date NOT NULL,
  `summa_eur` decimal(10,2) DEFAULT NULL,
  `vat_eur` decimal(10,2) DEFAULT NULL,
  `total_eur` decimal(10,2) DEFAULT NULL,
  `comment` text,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `convention_abonent_id` (`abonent_id`),
  KEY `convention_agreement_id` (`agreement_id`),
  KEY `convention_valuta_id` (`valuta_id`),
  KEY `convention_create_user_id` (`create_user_id`),
  KEY `convention_update_user_id` (`update_user_id`),
  CONSTRAINT `fk_convention_abonent_id` FOREIGN KEY (`abonent_id`) REFERENCES `abonent` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_convention_agreement_id` FOREIGN KEY (`agreement_id`) REFERENCES `agreement` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_convention_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `fk_convention_update_user_id` FOREIGN KEY (`update_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `fk_convention_valuta_id` FOREIGN KEY (`valuta_id`) REFERENCES `valuta` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `convention_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `convention_id` int(11) NOT NULL,
  `uploaded_file_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `convention_attachment_convention_id` (`convention_id`),
  KEY `convention_attachment_uploaded_file_id` (`uploaded_file_id`),
  CONSTRAINT `fk_convention_attachment_convention_id` FOREIGN KEY (`convention_id`) REFERENCES `convention` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_convention_attachment_uploaded_file_id` FOREIGN KEY (`uploaded_file_id`) REFERENCES `files` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `bill_template_person` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `bill_template_id` INT(11) NOT NULL,
    `client_person_id` INT(11) NOT NULL,
    `person_order` SMALLINT(6) NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `bill_template_person_bill_template_id` (`bill_template_id` ASC),
    INDEX `bill_template_person_client_person_id` (`client_person_id` ASC),
    CONSTRAINT `fk_bill_template_person_bill_template_id` FOREIGN KEY (`bill_template_id`)
        REFERENCES `bill_template` (`id`)
        ON DELETE CASCADE ON UPDATE RESTRICT,
    CONSTRAINT `fk_bill_template_person_client_person_id` FOREIGN KEY (`client_person_id`)
        REFERENCES `client_contact` (`id`)
        ON DELETE CASCADE ON UPDATE RESTRICT
);

CREATE TABLE `password_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `create_date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `password_history_user_id` (`user_id`),
  CONSTRAINT `fk_password_history_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tax_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `abonent_id` int(11) NOT NULL,
  `paid_date` date NOT NULL,
  `from_client_id` int(11) NOT NULL,
  `to_client_id` int(11) NOT NULL,
  `summa` decimal(10,2) NOT NULL,
  `valuta_id` int(11) NOT NULL,
  `rate` decimal(10,4) NOT NULL DEFAULT '1.0000',
  `summa_eur` decimal(10,2) DEFAULT NULL,
  `confirmed` DATE DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tax_payment_from_client_id` (`from_client_id`),
  KEY `tax_payment_to_client_id` (`to_client_id`),
  KEY `tax_payment_valuta_id` (`valuta_id`),
  KEY `tax_payment_abonent_id` (`abonent_id`),
  CONSTRAINT `fk_tax_payment_abonent_id` FOREIGN KEY (`abonent_id`) REFERENCES `abonent` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_tax_payment_from_client_id` FOREIGN KEY (`from_client_id`) REFERENCES `client` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_tax_payment_to_client_id` FOREIGN KEY (`to_client_id`) REFERENCES `client` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_tax_payment_valuta_id` FOREIGN KEY (`valuta_id`) REFERENCES `valuta` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- DONE TABLES
";
        $sqlIndex = 
"
--
-- Индексы сохранённых таблиц
--

--
-- Indexes for table `abonent`
--
ALTER TABLE `abonent`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `abonent_main_client_id` (`main_client_id`,`name`),
  ADD KEY `abonent_manager_id` (`manager_id`),
  ADD KEY `abonent_create_user_id` (`create_user_id`),
  ADD KEY `abonent_update_user_id` (`update_user_id`);

--
-- Indexes for table `abonent_agreement`
--
ALTER TABLE `abonent_agreement`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `abonent_agreement_unique` (`abonent_id`,`agreement_id`,`project_id`),
  ADD KEY `abonent_agreement_abonent_id` (`abonent_id`),
  ADD KEY `abonent_agreement_agreement_id` (`agreement_id`),
  ADD KEY `abonent_agreement_project_id` (`project_id`),
  ADD KEY `abonent_agreement_create_user_id` (`create_user_id`);

--
-- Indexes for table `abonent_bill`
--
ALTER TABLE `abonent_bill`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `abonent_bill_unique` (`abonent_id`,`bill_id`,`project_id`),
  ADD KEY `abonent_bill_abonent_id` (`abonent_id`),
  ADD KEY `abonent_bill_bill_id` (`bill_id`),
  ADD KEY `abonent_bill_project_id` (`project_id`),
  ADD KEY `abonent_bill_create_user_id` (`create_user_id`);

--
-- Indexes for table `abonent_bill_template`
--
ALTER TABLE `abonent_bill_template`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `abonent_bill_template_unique` (`abonent_id`,`bill_id`,`project_id`),
  ADD KEY `abonent_bill_template_abonent_id` (`abonent_id`),
  ADD KEY `abonent_bill_template_bill_template_id` (`bill_template_id`),
  ADD KEY `abonent_bill_template_project_id` (`project_id`),
  ADD KEY `abonent_bill_template_create_user_id` (`create_user_id`);
  
--
-- Indexes for table `abonent_client`
--
ALTER TABLE `abonent_client`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `abonent_client_unique` (`abonent_id`,`client_id`),
  ADD KEY `abonent_client_abonent_id` (`abonent_id`),
  ADD KEY `abonent_client_client_id` (`client_id`),
  ADD KEY `abonent_client_client_group_id` (`client_group_id`),
  ADD KEY `abonent_client_manager_id` (`manager_id`),
  ADD KEY `abonent_client_agent_id` (`agent_id`),
  ADD KEY `abonent_client_language_id` (`language_id`),
  ADD KEY `abonent_client_debit_valuta_id` (`debit_valuta_id`),
  ADD KEY `abonent_client_parent_id` (`parent_id`),
  ADD KEY `abonent_client_create_user_id` (`create_user_id`);

--
-- Indexes for table `abonent_product`
--
ALTER TABLE `abonent_product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `abonent_product_unique` (`abonent_id`,`product_id`),
  ADD KEY `abonent_product_abonent_id` (`abonent_id`),
  ADD KEY `abonent_product_product_id` (`product_id`),
  ADD KEY `abonent_product_order_create_user_id` (`create_user_id`);

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `address_country_id` (`country_id`),
  ADD KEY `address_region_id` (`region_id`),
  ADD KEY `address_city_id` (`city_id`),
  ADD KEY `address_district_id` (`district_id`),
  ADD KEY `address_create_user_id` (`create_user_id`),
  ADD KEY `address_update_user_id` (`update_user_id`);

--
-- Indexes for table `agreement`
--
ALTER TABLE `agreement`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `agreement_number_unique` (`first_client_id`, `second_client_id`, `number`, `deleted`),
  ADD KEY `agreement_first_client_id` (`first_client_id`),
  ADD KEY `agreement_second_client_id` (`second_client_id`),
  ADD KEY `agreement_third_client_id` (`third_client_id`),
  ADD KEY `agreement_create_user_id` (`create_user_id`),
  ADD KEY `agreement_update_user_id` (`update_user_id`),
  ADD KEY `agreement_valuta_id` (`valuta_id`),
  ADD KEY `agreement_first_client_role_id` (`first_client_role_id`),
  ADD KEY `agreement_second_client_role_id` (`second_client_role_id`),
  ADD KEY `agreement_third_client_role_id` (`third_client_role_id`),
  ADD KEY `agreement_parent_id` (`parent_id`);

--
-- Indexes for table `agreement_attachment`
--
ALTER TABLE `agreement_attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `agreement_attachment_agreement_id` (`agreement_id`),
  ADD KEY `agreement_attachment_uploaded_file_id` (`uploaded_file_id`);

--
-- Indexes for table `agreement_history`
--
ALTER TABLE `agreement_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `agreement_history_agreement_id` (`agreement_id`),
  ADD KEY `agreement_history_action_id` (`action_id`),
  ADD KEY `agreement_history_create_user_id` (`create_user_id`);

--
-- Indexes for table `agreement_payment`
--
ALTER TABLE `agreement_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `agreement_payment_agreement_history_id` (`agreement_history_id`),
  ADD KEY `agreement_payment_from_bank_id` (`from_bank_id`),
  ADD KEY `agreement_payment_to_bank_id` (`to_bank_id`),
  ADD KEY `agreement_payment_agreement_id` (`agreement_id`),
  ADD KEY `agreement_payment_valuta_id` (`aluta_id`);
  
--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bank_name_UNIQUE` (`swift`);

--
-- Indexes for table `bill`
--
ALTER TABLE `bill`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bill_doc_number` (`doc_number`,`deleted`),
  ADD KEY `bill_agreement_id` (`agreement_id`),
  ADD KEY `bill_parent_id` (`parent_id`),
  ADD KEY `bill_second_client_bank_id` (`second_client_bank_id`),
  ADD KEY `bill_valuta_id` (`valuta_id`),
  ADD KEY `bill_create_user_id` (`create_user_id`),
  ADD KEY `bill_update_user_id` (`update_user_id`),
  ADD KEY `bill_manager_id` (`manager_id`),
  ADD KEY `bill_first_client_bank_id` (`first_client_bank_id`),
  ADD KEY `bill_language_id` (`language_id`),
  ADD KEY `bill_child_id` (`child_id`);

--
-- Indexes for table `bill_template`
--
ALTER TABLE `bill_template`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bill_template_agreement_id` (`agreement_id`),
  ADD KEY `bill_template_first_client_bank_id` (`first_client_bank_id`),
  ADD KEY `bill_template_second_client_bank_id` (`second_client_bank_id`),
  ADD KEY `bill_template_valuta_id` (`valuta_id`),
  ADD KEY `bill_template_language_id` (`language_id`),
  ADD KEY `bill_template_create_user_id` (`create_user_id`),
  ADD KEY `bill_template_update_user_id` (`update_user_id`);
  
--
-- Indexes for table `bill_confirm`
--
ALTER TABLE `bill_confirm`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bill_confirm_payment_confirm_id` (`payment_confirm_id`),
  ADD KEY `bill_confirm_second_client_id` (`second_client_id`),
  ADD KEY `bill_confirm_valuta_id` (`valuta_id`);

--
-- Indexes for table `bill_history`
--
ALTER TABLE `bill_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bill_history_bill_id` (`bill_id`),
  ADD KEY `bill_history_create_user_id` (`create_user_id`),
  ADD KEY `bill_history_action_id` (`action_id`);

--
-- Indexes for table `bill_payment`
--
ALTER TABLE `bill_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bill_paymentl_bill_id` (`bill_id`),
  ADD KEY `bill_payment_payment_order_id` (`payment_order_id`),
  ADD KEY `bill_payment_bill_history_id` (`bill_history_id`),
  ADD KEY `bill_payment_from_bank_id` (`from_bank_id`),
  ADD KEY `bill_payment_to_bank_id` (`to_bank_id`),
  ADD KEY `bill_payment_valuta_id` (`valuta_id`);

--
-- Indexes for table `bill_person`
--
ALTER TABLE `bill_person`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bill_person_bill_id` (`bill_id`),
  ADD KEY `bill_person_client_person_id` (`client_person_id`);

--
-- Indexes for table `bill_product`
--
ALTER TABLE `bill_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bill_product_bill_id` (`bill_id`),
  ADD KEY `bill_product_product_id` (`product_id`),
  ADD KEY `bill_product_measure_id` (`measure_id`);

--
-- Indexes for table `bill_template_product`
--
ALTER TABLE `bill_template_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bill_template_product_bill_template_id` (`bill_id`),
  ADD KEY `bill_template_product_product_id` (`product_id`),
  ADD KEY `bill_template_product_measure_id` (`measure_id`);
  
--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `client_vat_number_unique` (`deleted`,`vat_number`),
  ADD KEY `client_uploaded_file_id` (`uploaded_file_id`),
  ADD KEY `client_legal_country_id` (`legal_country_id`),
  ADD KEY `client_office_country_id` (`office_country_id`);
  
--
-- Indexes for table `client_bank`
--
ALTER TABLE `client_bank`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_bank_abonent_id` (`abonent_id`),
  ADD KEY `client_bank_bank_id` (`bank_id`),
  ADD KEY `client_bank_client_id` (`client_id`),
  ADD KEY `client_bank_uploaded_file_id` (`uploaded_file_id`),
  ADD KEY `client_bank_uploaded_pdf_file_id` (`uploaded_pdf_file_id`);

--
-- Indexes for table `client_bank_balance`
--
ALTER TABLE `client_bank_balance`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `client_bank_balance_unique` (`account_id`,`start_date`,`end_date`),
  ADD KEY `client_bank_balance_uploaded_file_id` (`uploaded_file_id`),
  ADD KEY `client_bank_balance_account_id` (`account_id`),
  ADD KEY `client_bank_balance_payment_confirm_id` (`payment_confirm_id`),
  ADD KEY `client_bank_balance_uploaded_pdf_file_id` (`uploaded_pdf_file_id`);

--
-- Indexes for table `client_contact`
--
ALTER TABLE `client_contact`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_contact_client_id` (`client_id`),
  ADD KEY `client_contact_abonent_id` (`abonent_id`),
  ADD KEY `client_contact_position_id` (`position_id`);

--
-- Indexes for table `client_group`
--
ALTER TABLE `client_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `client_group_UNIQUE` (`name`)
  ADD INDEX `client_group_abonent_id` (`abonent_id`);

--
-- Indexes for table `client_role`
--
ALTER TABLE `client_role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `client_role_UNIQUE` (`name`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_abonent_id` (`abonent_id`),
  ADD KEY `event_manager_id` (`manager_id`),
  ADD KEY `event_parent_event_id` (`parent_event_id`),
  ADD KEY `event_create_user_id` (`create_user_id`),
  ADD KEY `event_update_user_id` (`update_user_id`);

--
-- Indexes for table `event_contact`
--
ALTER TABLE `event_contact`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `event_contact_unique` (`event_id`,`name`),
  ADD KEY `event_contact_event_id` (`event_id`),
  ADD KEY `event_contact_contact_id` (`contact_id`);

--
-- Indexes for table `event_event_resurs`
--
ALTER TABLE `event_event_resurs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `event_event_resurs_unique` (`event_id`,`event_resurs_id`),
  ADD KEY `event_event_resurs_event_id` (`event_id`),
  ADD KEY `event_event_resurs_event_resurs_id` (`event_resurs_id`);

--
-- Indexes for table `event_profile`
--
ALTER TABLE `event_profile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `event_profile_unique` (`event_id`,`profile_id`),
  ADD KEY `event_profile_event_id` (`event_id`),
  ADD KEY `event_profile_profile_id` (`profile_id`);

--
-- Indexes for table `event_resurs`
--
ALTER TABLE `event_resurs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_resurs_UNIQUE` (`name`);

--
-- Indexes for table `expense`
--
ALTER TABLE `expense`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `expense_doc_number` (`abonent_id`, `doc_number`),
  ADD KEY `expensel_abonent_id` (`abonent_id`),
  ADD KEY `expensel_project_id` (`project_id`),
  ADD KEY `expense_first_client_id` (`first_client_id`),
  ADD KEY `expense_second_client_id` (`second_client_id`),
  ADD KEY `expense_valuta_id` (`valuta_id`),
  ADD KEY `expense_create_user_id` (`create_user_id`),
  ADD KEY `expense_update_user_id` (`update_user_id`),
  ADD KEY `expense_expense_type_id` (`expense_type_id`);

--
-- Indexes for table `expense_type`
--
ALTER TABLE `expense_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `expense_type_UNIQUE` (`name`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `language_UNIQUE` (`language`);

--
-- Indexes for table `location_city`
--
ALTER TABLE `location_city`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `city_name_UNIQUE` (`name`,`region_id`,`country_id`),
  ADD KEY `city_country_id` (`country_id`),
  ADD KEY `city_region_id` (`region_id`);

--
-- Indexes for table `location_country`
--
ALTER TABLE `location_country`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `country_name_UNIQUE` (`name`,`short_name`);

--
-- Indexes for table `location_district`
--
ALTER TABLE `location_district`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `district_name_UNIQUE` (`name`,`region_id`,`country_id`,`city_id`),
  ADD KEY `district_country_id` (`country_id`),
  ADD KEY `district_region_id` (`region_id`),
  ADD KEY `district_city_id` (`city_id`);

--
-- Indexes for table `location_region`
--
ALTER TABLE `location_region`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `region_name_UNIQUE` (`name`,`country_id`),
  ADD KEY `region_country_id` (`country_id`);

--
-- Indexes for table `measure`
--
ALTER TABLE `measure`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `valuta_UNIQUE` (`name`);

--
-- Indexes for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_confirm_create_user_id` (`create_user_id`),
  ADD KEY `payment_confirm_update_user_id` (`update_user_id`),
  ADD KEY `payment_confirm_action_user_id` (`action_user_id`),
  ADD KEY `payment_confirm_abonent_id` (`abonent_id`),
  ADD KEY `payment_confirm_bank_id` (`bank_id`),
  ADD KEY `payment_confirm_client_id` (`client_id`),
  ADD KEY `payment_confirm_uploaded_file_id` (`uploaded_file_id`),
  ADD KEY `payment_confirm_uploaded_pdf_file_id` (`uploaded_pdf_file_id`);

--
-- Indexes for table `payment_order`
--
ALTER TABLE `payment_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `payment_order_unique` (`abonent_id`,`number`),
  ADD KEY `payment_order_create_user_id` (`create_user_id`),
  ADD KEY `payment_order_file_id` (`file_id`),
  ADD KEY `payment_order_update_user_id` (`update_user_id`),
  ADD KEY `payment_order_action_user_id` (`action_user_id`),
  ADD KEY `payment_order_abonent_id` (`abonent_id`),
  ADD KEY `payment_order_client_id` (`client_id`),
  ADD KEY `payment_order_client_bank_id` (`client_bank_id`);

--
-- Indexes for table `person_position`
--
ALTER TABLE `person_position`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `person_position_UNIQUE` (`name`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_UNIQUE` (`name`),
  ADD KEY `product_measure_id` (`measure_id`);

--
-- Indexes for table `profile_client`
--
ALTER TABLE `profile_client`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `profile_client_unique` (`profile_id`,`client_id`, `abonent_id`),
  ADD KEY `profile_client_profile_id` (`profile_id`),
  ADD KEY `profile_client_client_id` (`client_id`),
  ADD KEY `profile_client_abonent_id` (`abonent_id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `project_name_unique` (`name`,`country_id`,`abonent_id`),
  ADD KEY `project_abonent_id` (`abonent_id`),
  ADD KEY `project_create_user_id` (`create_user_id`),
  ADD KEY `project_update_user_id` (`update_user_id`),
  ADD KEY `project_country_id` (`country_id`);
  
--
-- Indexes for table `project_attachment`
--
ALTER TABLE `project_attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_attachment_project_id` (`project_id`),
  ADD KEY `fk_project_attachment_uploaded_file_id_idx` (`uploaded_file_id`);

--
-- Indexes for table `client_reg_doc`
--
ALTER TABLE `client_reg_doc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_reg_doc_abonent_id` (`abonent_id`),
  ADD KEY `client_reg_doc_client_id` (`client_id`),
  ADD KEY `client_reg_doc_reg_doc_type_id` (`reg_doc_type_id`),
  ADD KEY `client_reg_doc_uploaded_file_id` (`uploaded_file_id`);

--
-- Indexes for table `reg_doc_type`
--
ALTER TABLE `reg_doc_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reg_doc_type_UNIQUE` (`name`);

--
-- Indexes for table `shareholder`
--
ALTER TABLE `shareholder`
  ADD PRIMARY KEY (`id`),
  ADD KEY `share_client_id` (`client_id`),
  ADD KEY `share_shareholder_id` (`shareholder_id`);

--
-- Indexes for table `valuta`
--
ALTER TABLE `valuta`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `valuta_UNIQUE` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abonent`
--
ALTER TABLE `abonent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `abonent_agreement`
--
ALTER TABLE `abonent_agreement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `abonent_bill`
--
ALTER TABLE `abonent_bill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `abonent_bill`
--
ALTER TABLE `abonent_bill_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;  
--
-- AUTO_INCREMENT for table `abonent_client`
--
ALTER TABLE `abonent_client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `abonent_product`
--
ALTER TABLE `abonent_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `agreement`
--
ALTER TABLE `agreement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `agreement_attachment`
--
ALTER TABLE `agreement_attachment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `agreement_history`
--
ALTER TABLE `agreement_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT; 
--
-- AUTO_INCREMENT for table `agreement_payment`
--
ALTER TABLE `agreement_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;   
--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bill`
--
ALTER TABLE `bill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bill_template`
--
ALTER TABLE `bill_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bill_confirm`
--
ALTER TABLE `bill_confirm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bill_history`
--
ALTER TABLE `bill_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bill_payment`
--
ALTER TABLE `bill_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bill_person`
--
ALTER TABLE `bill_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bill_product`
--
ALTER TABLE `bill_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bill_product`
--
ALTER TABLE `bill_template_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;  
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `client_bank`
--
ALTER TABLE `client_bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `client_bank_balance`
--
ALTER TABLE `client_bank_balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `client_contact`
--
ALTER TABLE `client_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `client_group`
--
ALTER TABLE `client_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `client_role`
--
ALTER TABLE `client_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event_contact`
--
ALTER TABLE `event_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event_event_resurs`
--
ALTER TABLE `event_event_resurs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event_profile`
--
ALTER TABLE `event_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event_resurs`
--
ALTER TABLE `event_resurs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `expense`
--
ALTER TABLE `expense`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `expense_type`
--
ALTER TABLE `expense_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `location_city`
--
ALTER TABLE `location_city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `location_country`
--
ALTER TABLE `location_country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `location_district`
--
ALTER TABLE `location_district`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `location_region`
--
ALTER TABLE `location_region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `measure`
--
ALTER TABLE `measure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_order`
--
ALTER TABLE `payment_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `person_position`
--
ALTER TABLE `person_position`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profile_client`
--
ALTER TABLE `profile_client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `project_attachment`
--
ALTER TABLE `project_attachment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `client_reg_doc`
--
ALTER TABLE `client_reg_doc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reg_doc_type`
--
ALTER TABLE `reg_doc_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shareholder`
--
ALTER TABLE `shareholder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `valuta`
--
ALTER TABLE `valuta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- DONE INDEXES
";
        $sqlForeign = 
"  
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `fk_profile_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_profile_update_user_id` FOREIGN KEY (`update_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_profile_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_profile_language_id` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE SET NULL;
  
--
-- Constraints for table `abonent`
--
ALTER TABLE `abonent`
  ADD CONSTRAINT `fk_abonent_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_abonent_main_client_id` FOREIGN KEY (`main_client_id`) REFERENCES `client` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_abonent_manager_id` FOREIGN KEY (`manager_id`) REFERENCES `profile` (`id`),
  ADD CONSTRAINT `fk_abonent_update_user_id` FOREIGN KEY (`update_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `abonent_agreement`
--
ALTER TABLE `abonent_agreement`
  ADD CONSTRAINT `fk_abonent_agreement_abonent_id` FOREIGN KEY (`abonent_id`) REFERENCES `abonent` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_abonent_agreement_agreement_id` FOREIGN KEY (`agreement_id`) REFERENCES `agreement` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_abonent_agreement_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_abonent_agreement_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `abonent_bill`
--
ALTER TABLE `abonent_bill`
  ADD CONSTRAINT `fk_abonent_bill_abonent_id` FOREIGN KEY (`abonent_id`) REFERENCES `abonent` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_abonent_bill_bill_id` FOREIGN KEY (`bill_id`) REFERENCES `bill` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_abonent_bill_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_abonent_bill_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `abonent_bill_template`
--
ALTER TABLE `abonent_bill`
  ADD CONSTRAINT `fk_abonent_bill_template_abonent_id` FOREIGN KEY (`abonent_id`) REFERENCES `abonent` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_abonent_bill_template_bill_template_id` FOREIGN KEY (`bill_template_id`) REFERENCES `bill` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_abonent_bill_template_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_abonent_bill_template_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL;
--
-- Constraints for table `abonent_client`
--
ALTER TABLE `abonent_client`
  ADD CONSTRAINT `fk_abonent_client_abonent_id` FOREIGN KEY (`abonent_id`) REFERENCES `abonent` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_abonent_client_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_abonent_client_client_group_id` FOREIGN KEY (`client_group_id`)  REFERENCES `client_group` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_abonent_client_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_abonent_client_debit_valuta_id` FOREIGN KEY (`debit_valuta_id`) REFERENCES `valuta` (`id`),
  ADD CONSTRAINT `fk_abonent_client_language_id` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`),
  ADD CONSTRAINT `fk_abonent_client_manager_id` FOREIGN KEY (`manager_id`) REFERENCES `profile` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_abonent_client_agent_id` FOREIGN KEY (`agent_id`) REFERENCES `profile` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_abonent_client_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `client` (`id`) ON DELETE CASCADE;
  
--
-- Constraints for table `abonent_product`
--
ALTER TABLE `abonent_product`
  ADD CONSTRAINT `fk_abonent_product_abonent_id` FOREIGN KEY (`abonent_id`) REFERENCES `abonent` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_abonent_product_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_abonent_product_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `address`
--
ALTER TABLE `address`
  ADD CONSTRAINT `fk_address_city_id` FOREIGN KEY (`city_id`) REFERENCES `location_city` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_address_country_id` FOREIGN KEY (`country_id`) REFERENCES `location_country` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_address_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_address_district_id` FOREIGN KEY (`district_id`) REFERENCES `location_district` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_address_region_id` FOREIGN KEY (`region_id`) REFERENCES `location_region` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_address_update_user_id` FOREIGN KEY (`update_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `agreement`
--
ALTER TABLE `agreement`
  ADD CONSTRAINT `fk_agreement_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_agreement_first_client_id` FOREIGN KEY (`first_client_id`) REFERENCES `client` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_agreement_first_client_role_id` FOREIGN KEY (`first_client_role_id`) REFERENCES `client_role` (`id`),
  ADD CONSTRAINT `fk_agreement_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `agreement` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_agreement_second_client_id` FOREIGN KEY (`second_client_id`) REFERENCES `client` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_agreement_second_client_role_id` FOREIGN KEY (`second_client_role_id`) REFERENCES `client_role` (`id`),
  ADD CONSTRAINT `fk_agreement_third_client_id` FOREIGN KEY (`third_client_id`) REFERENCES `client` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_agreement_third_client_role_id` FOREIGN KEY (`third_client_role_id`) REFERENCES `client_role` (`id`),
  ADD CONSTRAINT `fk_agreement_update_user_id` FOREIGN KEY (`update_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_agreement_valuta_id` FOREIGN KEY (`valuta_id`) REFERENCES `valuta` (`id`);

--
-- Constraints for table `agreement_attachment`
--
ALTER TABLE `agreement_attachment`
  ADD CONSTRAINT `fk_agreement_attachment_agreement_id` FOREIGN KEY (`agreement_id`) REFERENCES `agreement` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_agreement_attachment_uploaded_file_id` FOREIGN KEY (`uploaded_file_id`) REFERENCES `files` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `agreement_history`
--
ALTER TABLE `agreement_history`
  ADD CONSTRAINT `fk_agreement_history_agreement_id` FOREIGN KEY (`agreement_id`) REFERENCES `agreement` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_agreement_history_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `agreement_payment`
--
ALTER TABLE `agreement_payment`
  ADD CONSTRAINT `fk_agreement_payment_agreement_history_id` FOREIGN KEY (`agreement_history_id`) REFERENCES `agreement_history` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_agreement_payment_from_bank_id` FOREIGN KEY (`from_bank_id`) REFERENCES `bank` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_agreement_payment_to_bank_id` FOREIGN KEY (`to_bank_id`) REFERENCES `bank` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_agreement_payment_agreement_id` FOREIGN KEY (`agreement_id`) REFERENCES `agreement` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_agreement_payment_valuta_id` FOREIGN KEY (`valuta_id`) REFERENCES `valuta` (`id`);

--
-- Constraints for table `bill`
--
ALTER TABLE `bill`
  ADD CONSTRAINT `fk_bill_agreement_id` FOREIGN KEY (`agreement_id`) REFERENCES `agreement` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_bill_child_id` FOREIGN KEY (`child_id`) REFERENCES `bill` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_bill_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_bill_first_client_bank_id` FOREIGN KEY (`first_client_bank_id`) REFERENCES `client_bank` (`id`),
  ADD CONSTRAINT `fk_bill_language_id` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`),
  ADD CONSTRAINT `fk_bill_manager_id` FOREIGN KEY (`manager_id`) REFERENCES `profile` (`id`),
  ADD CONSTRAINT `fk_bill_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `bill` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_bill_second_client_bank_id` FOREIGN KEY (`second_client_bank_id`) REFERENCES `client_bank` (`id`),
  ADD CONSTRAINT `fk_bill_update_user_id` FOREIGN KEY (`update_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_bill_valuta_id` FOREIGN KEY (`valuta_id`) REFERENCES `valuta` (`id`);

--
-- Constraints for table `bill_template`
--
ALTER TABLE `bill_template`
  ADD CONSTRAINT `fk_bill_template_agreement_id` FOREIGN KEY (`agreement_id`) REFERENCES `agreement` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_bill_template_first_client_bank_id` FOREIGN KEY (`first_client_bank_id`) REFERENCES `client_bank` (`id`),
  ADD CONSTRAINT `fk_bill_template_second_client_bank_id` FOREIGN KEY (`second_client_bank_id`) REFERENCES `client_bank` (`id`),
  ADD CONSTRAINT `fk_bill_template_valuta_id` FOREIGN KEY (`valuta_id`) REFERENCES `valuta` (`id`),
  ADD CONSTRAINT `fk_bill_template_language_id` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`),
  ADD CONSTRAINT `fk_bill_template_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_bill_template_update_user_id` FOREIGN KEY (`update_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL;
  
--
-- Constraints for table `bill_confirm`
--
ALTER TABLE `bill_confirm`
  ADD CONSTRAINT `fk_bill_confirm_payment_confirm_id` FOREIGN KEY (`payment_confirm_id`) REFERENCES `payment_confirm` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_bill_confirm_second_client_id` FOREIGN KEY (`second_client_id`) REFERENCES `client` (`id`),
  ADD CONSTRAINT `fk_bill_confirm_valuta_id` FOREIGN KEY (`valuta_id`) REFERENCES `valuta` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `bill_history`
--
ALTER TABLE `bill_history`
  ADD CONSTRAINT `fk_bill_history_bill_id` FOREIGN KEY (`bill_id`) REFERENCES `bill` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_bill_history_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `bill_payment`
--
ALTER TABLE `bill_payment`
  ADD CONSTRAINT `fk_payment_order_action_user_id` FOREIGN KEY (`action_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_payment_order_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_payment_order_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_payment_order_file_id` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_payment_order_update_user_id` FOREIGN KEY (`update_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_payment_payment_order_id` FOREIGN KEY (`abonent_id`) REFERENCES `abonent` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `bill_person`
--
ALTER TABLE `bill_person`
  ADD CONSTRAINT `fk_bill_person_bill_id` FOREIGN KEY (`bill_id`) REFERENCES `bill` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_bill_person_client_person_id` FOREIGN KEY (`client_person_id`) REFERENCES `client_contact` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `bill_product`
--
ALTER TABLE `bill_product`
  ADD CONSTRAINT `fk_bill_product_bill_id` FOREIGN KEY (`bill_id`) REFERENCES `bill` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_bill_product_measure_id` FOREIGN KEY (`measure_id`) REFERENCES `measure` (`id`),
  ADD CONSTRAINT `fk_bill_product_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

--
-- Constraints for table `bill_template_product`
--
ALTER TABLE `bill_template_product`
  ADD CONSTRAINT `fk_bill_template_product_bill_template_id` FOREIGN KEY (`bill_template_id`) REFERENCES `bill_template` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_bill_template_product_measure_id` FOREIGN KEY (`measure_id`) REFERENCES `measure` (`id`),
  ADD CONSTRAINT `fk_bill_template_product_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);
  
--
-- Constraints for table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `fk_client_legal_country_id` FOREIGN KEY (`legal_country_id`) REFERENCES `location_country` (`id`),
  ADD CONSTRAINT `fk_client_office_country_id` FOREIGN KEY (`office_country_id`) REFERENCES `location_country` (`id`),
  ADD CONSTRAINT `fk_client_uploaded_file_id` FOREIGN KEY (`uploaded_file_id`) REFERENCES `files` (`id`) ON DELETE SET NULL;
  
--
-- Constraints for table `client_bank`
--
ALTER TABLE `client_bank`
  ADD CONSTRAINT `fk_client_bank_abonent_id` FOREIGN KEY (`abonent_id`) REFERENCES `abonent` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_client_bank_bank_id` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_client_bank_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_client_bank_uploaded_pdf_file_id` FOREIGN KEY (`uploaded_pdf_file_id`) REFERENCES `files` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `client_bank_balance`
--
ALTER TABLE `client_bank_balance`
  ADD CONSTRAINT `fk_client_bank_balance_account_id` FOREIGN KEY (`account_id`) REFERENCES `client_bank` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_client_bank_balance_payment_confirm_id` FOREIGN KEY (`payment_confirm_id`) REFERENCES `payment_confirm` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_client_bank_balance_uploaded_file_id` FOREIGN KEY (`uploaded_file_id`) REFERENCES `files` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_client_bank_balance_uploaded_pdf_file_id` FOREIGN KEY (`uploaded_pdf_file_id`) REFERENCES `files` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `client_contact`
--
ALTER TABLE `client_contact`
  ADD CONSTRAINT `fk_client_contact_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_client_contact_abonent_id` FOREIGN KEY (`abonent_id`) REFERENCES `abonent` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_client_contact_position_id` FOREIGN KEY (`position_id`) REFERENCES `person_position` (`id`);

--
-- Constraints for table `client_group`
--
ALTER TABLE `client_group` 
  ADD CONSTRAINT `fk_client_group_abonent_id` FOREIGN KEY (`abonent_id`) REFERENCES `abonent` (`id`) ON DELETE CASCADE;
  
--
-- Constraints for table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `fk_event_abonent_id` FOREIGN KEY (`abonent_id`) REFERENCES `abonent` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_event_manager_id` FOREIGN KEY (`manager_id`) REFERENCES `profile` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_event_parent_event_id` FOREIGN KEY (`parent_event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_event_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `fk_event_update_user_id` FOREIGN KEY (`update_user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `event_event_resurs`
--
ALTER TABLE `event_event_resurs`
  ADD CONSTRAINT `fk_event_event_resurs_event_id` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_event_event_resurs_event_resurs_id` FOREIGN KEY (`event_resurs_id`) REFERENCES `event_resurs` (`id`);

--
-- Constraints for table `event_profile`
--
ALTER TABLE `event_profile`
  ADD CONSTRAINT `fk_event_profile_event_id` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_event_profile_profile_id` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`id`);

--
-- Constraints for table `event_contact`
--
ALTER TABLE `event_contact`
  ADD CONSTRAINT `fk_event_contact_contact_id` FOREIGN KEY (`contact_id`) REFERENCES `client_contact` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_event_contact_event_id` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `expense`
--
ALTER TABLE `expense`
  ADD CONSTRAINT `fk_expense_abonent_id` FOREIGN KEY (`abonent_id`) REFERENCES `abonent` (`id`) ON DELETE CASCADE;
  ADD CONSTRAINT `fk_expense_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_expense_expense_type_id` FOREIGN KEY (`expense_type_id`) REFERENCES `expense_type` (`id`),
  ADD CONSTRAINT `fk_expense_first_client_id` FOREIGN KEY (`first_client_id`) REFERENCES `client` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_expense_second_client_id` FOREIGN KEY (`second_client_id`) REFERENCES `client` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_expense_update_user_id` FOREIGN KEY (`update_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_expense_valuta_id` FOREIGN KEY (`valuta_id`) REFERENCES `valuta` (`id`),
  ADD CONSTRAINT `fk_expensel_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `location_city`
--
ALTER TABLE `location_city`
  ADD CONSTRAINT `fk_city_country_id` FOREIGN KEY (`country_id`) REFERENCES `location_country` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_city_region_id` FOREIGN KEY (`region_id`) REFERENCES `location_region` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `location_district`
--
ALTER TABLE `location_district`
  ADD CONSTRAINT `fk_district_city_id` FOREIGN KEY (`city_id`) REFERENCES `location_city` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_district_country_id` FOREIGN KEY (`country_id`) REFERENCES `location_country` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_district_region_id` FOREIGN KEY (`region_id`) REFERENCES `location_region` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `location_region`
--
ALTER TABLE `location_region`
  ADD CONSTRAINT `fk_region_country_id` FOREIGN KEY (`country_id`) REFERENCES `location_country` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  ADD CONSTRAINT `fk_payment_confirm_action_user_id` FOREIGN KEY (`action_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_payment_confirm_abonent_id` FOREIGN KEY (`abonent_id`) REFERENCES `abonent` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_payment_confirm_bank_id` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_payment_confirm_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_payment_confirm_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_payment_confirm_update_user_id` FOREIGN KEY (`update_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_payment_confirm_uploaded_file_id` FOREIGN KEY (`uploaded_file_id`) REFERENCES `files` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_payment_confirm_uploaded_pdf_file_id` FOREIGN KEY (`uploaded_pdf_file_id`) REFERENCES `files` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `payment_order`
--
ALTER TABLE `payment_order`
  ADD CONSTRAINT `fk_payment_payment_order_id` FOREIGN KEY (`abonent_id`) REFERENCES `abonent` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_payment_order_action_user_id` FOREIGN KEY (`action_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_payment_order_bank_id` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_payment_order_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_payment_order_file_id` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_payment_order_update_user_id` FOREIGN KEY (`update_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_product_measure_id` FOREIGN KEY (`measure_id`) REFERENCES `measure` (`id`);

--
-- Constraints for table `profile_client`
--
ALTER TABLE `profile_client`
  ADD CONSTRAINT `fk_profile_client_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_profile_client_profile_id` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_profile_client_abonent_id` FOREIGN KEY (`abonent_id`) REFERENCES `abonent` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `fk_project_abonent_id` FOREIGN KEY (`abonent_id`) REFERENCES `abonent` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_project_country_id` FOREIGN KEY (`country_id`) REFERENCES `location_country` (`id`),
  ADD CONSTRAINT `fk_project_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_project_update_user_id` FOREIGN KEY (`update_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `project_attachment`
--
ALTER TABLE `project_attachment`
  ADD CONSTRAINT `fk_project_attachment_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_project_attachment_uploaded_file_id` FOREIGN KEY (`uploaded_file_id`) REFERENCES `files` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `client_reg_doc`
--
ALTER TABLE `client_reg_doc`
  ADD CONSTRAINT `fk_client_reg_doc_abonent_id` FOREIGN KEY (`abonent_id`) REFERENCES `abonent` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_client_reg_doc_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_client_reg_doc_reg_doc_type_id` FOREIGN KEY (`reg_doc_type_id`) REFERENCES `reg_doc_type` (`id`),
  ADD CONSTRAINT `fk_client_reg_doc_uploaded_file_id` FOREIGN KEY (`uploaded_file_id`) REFERENCES `files` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `shareholder`
--
ALTER TABLE `shareholder`
  ADD CONSTRAINT `fk_share_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_share_shareholder_id` FOREIGN KEY (`shareholder_id`) REFERENCES `client` (`id`) ON DELETE CASCADE;

-- DONE FOREIGN KEYS
";
        $sqlInsert = 
"
INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `superuser`, `confirmed_at`, `registration_ip`, `created_at`, `updated_at`) VALUES
(1, 'FILSM', 'filsm@inbox.lv', '".Yii::$app->getSecurity()->generatePasswordHash('Qw_321828712')."', 'db7UVRs512jaaysxbe3hJNZJsLH1N9CM', 1, '2147483647', '::1', '1488359569', '1488359569');

INSERT INTO `profile` (`id`, `user_id`, `name`, `phone`) VALUES
(1, 1, 1, 'Philipp Smelov', '+37129136605');

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('superuser', '1', 1489310558),
('booker', '1', 1489310558),
('user', '1', 1489310558);

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, 'Company admin', NULL, NULL, 1432200486, 1432227000),
('booker', 1, 'Booker', NULL, NULL, 1432200445, 1432200445),
('boss', 1, 'Company boss', NULL, NULL, 1432200499, 1432227021),
('operator', 1, 'Operator', NULL, NULL, 1488540315, 1488540315),
('portal_admin', 1, 'Domain admin', NULL, NULL, 1432200517, 1482156660),
('showBackend', 2, 'Allow to show backend of site', NULL, NULL, 1434463699, 1434464392),
('superuser', 1, 'Superuser', NULL, NULL, 1432200528, 1434464402),
('user', 1, 'Registered user', NULL, NULL, 1432200359, 1432227039);

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('admin', 'showBackend'),
('portal_admin', 'showBackend'),
('superuser', 'showBackend'),
('showBackend', 'showSectionMainLists'),
('showBackend', 'showSectionUserCompanies'),
('showBackend', 'showSectionUsers');

INSERT INTO `auth_rule` (`name`, `data`, `created_at`, `updated_at`) VALUES
('User role rule', 'O:20:\"rbac\\ShowBackendRule\":3:{s:4:\"name\";s:14:\"User role rule\";s:9:\"createdAt\";i:1436540346;s:9:\"updatedAt\";i:1436540346;}', 1436540346, 1436540346);
    
INSERT INTO `language` (`language_id`, `language`, `country`, `name`, `name_ascii`, `status`) VALUES
('en-GB', 'en', 'gb', 'English (UK)', 'English (UK)', 0),
('lv-LV', 'lv', 'lv', 'Latviešu', 'Latvian', 1),
('ru-RU', 'ru', 'ru', 'Русский', 'Russian', 1);


INSERT INTO `language_source` (`id`, `category`, `message`, `from_variable`) VALUES
(1, 'common', 'ID', NULL),
(2, 'common', 'Name', NULL),
(4, 'common', 'Enabled', NULL),
(5, 'common', ' not updated due to validation error.', NULL),
(6, 'common', 'Cancel', NULL),
(7, 'common', 'Save', NULL),
(8, 'common', 'Create', NULL),
(9, 'common', 'Update', NULL),
(10, 'common', 'Version', NULL),
(11, 'common', 'Comment', NULL),
(12, 'common', 'Deleted', NULL),
(13, 'common', 'Currenсy', NULL),
(15, 'common', 'Create Time', NULL),
(16, 'common', 'Create User', NULL),
(17, 'common', 'Update Time', NULL),
(18, 'common', 'Update User', NULL),
(20, 'common', 'Currency', NULL),
(21, 'common', 'Account name', NULL),
(22, 'common', 'It is', NULL),
(23, 'common', 'Status', NULL),
(24, 'common', 'Yes', NULL),
(25, 'common', 'No', NULL),
(26, 'common', 'Abonent name', NULL),
(27, 'common', 'Abonent comment', NULL),
(28, 'common', 'Delete', NULL),
(29, 'common', 'Are you sure to delete this ', NULL),
(30, 'common', 'Are you sure to delete this item?', NULL),
(31, 'common', 'About us', NULL),
(32, 'common', 'Contacts', NULL),
(33, 'common', 'Admin', NULL),
(34, 'common', 'Login', NULL),
(35, 'common', 'Log Out', NULL),
(36, 'common', 'Common data', NULL),
(37, 'common', 'Today', NULL),
(38, 'common', 'Other data', NULL),
(39, 'common', 'Search', NULL),
(40, 'common', 'Reset', NULL),
(41, 'common', 'Summary', NULL),
(42, 'common', 'Enter as', NULL),
(43, 'common', 'Print', NULL),
(44, 'common', 'Start date', NULL),
(45, 'common', 'End date', NULL),
(47, 'common', 'Locations', NULL),
(48, 'common', 'Application', NULL),
(49, 'common', 'Delete selected', NULL),
(50, 'common', 'Are you sure you want to delete selected item(s)?', NULL),
(51, 'common', 'Any...', NULL),
(52, 'common', 'Type', NULL),
(53, 'common', '{count, number} {count, plural, =1{entry} other{entries}} remove!.', NULL),
(54, 'measure', 'Description', NULL),
(55, 'measure', 'Measure', NULL),
(56, 'fsmuser', 'Role', NULL),
(57, 'fsmuser', 'User client', NULL),
(58, 'fsmuser', 'Phone', NULL),
(59, 'fsmuser', 'My company', NULL),
(60, 'fsmuser', 'Add user to the client company', NULL),
(61, 'fsmuser', 'Leave it blank and we will generate your username from of your email address.', NULL),
(62, 'client', 'Full name', NULL),
(64, 'client', 'Abonent', NULL),
(65, 'client', 'First party', NULL),
(66, 'client', 'Second party', NULL),
(67, 'client', 'Third party', NULL),
(72, 'client', 'Number', NULL),
(73, 'client', 'Signing date', NULL),
(74, 'client', 'Term', NULL),
(76, 'client', 'Rate %', NULL),
(77, 'client', 'Rate (summa)', NULL),
(78, 'client', 'Date from', NULL),
(79, 'client', 'Date till', NULL),
(80, 'client', 'Status', NULL),
(82, 'client', 'Potential', NULL),
(83, 'client', 'New', NULL),
(84, 'client', 'Concluded', NULL),
(85, 'client', 'Overdue', NULL),
(86, 'client', 'Canceled', NULL),
(87, 'client', 'Oral', NULL),
(88, 'client', 'Written', NULL),
(89, 'client', 'Client', NULL),
(91, 'client', 'Account', NULL),
(92, 'client', 'Bank name', NULL),
(93, 'client', 'SWIFT code', NULL),
(94, 'client', 'This registration number has already been taken.', NULL),
(95, 'client', 'Parent company', NULL),
(96, 'client', 'Client type', NULL),
(100, 'client', 'Invoice Email', NULL),
(101, 'client', 'Reg.number', NULL),
(102, 'client', 'VAT payer', NULL),
(103, 'client', 'VAT number', NULL),
(104, 'client', 'Legal address', NULL),
(105, 'client', 'Office address', NULL),
(106, 'client', 'Debit / Credit (+/-)', NULL),
(107, 'client', 'Owner', NULL),
(108, 'client', 'Simple client', NULL),
(109, 'client', 'Physical person', NULL),
(110, 'client', 'Legal person', NULL),
(111, 'client', 'Active', NULL),
(112, 'client', 'Archived', NULL),
(113, 'client', 'Main Client', NULL),
(115, 'client', 'Add new client', NULL),
(116, 'client', 'All clients', NULL),
(117, 'client', 'My clients', NULL),
(118, 'client', 'External clients', NULL),
(122, 'client', 'Add new project', NULL),
(123, 'client', 'Add new agreement', NULL),
(125, 'client', 'Firstname, Lastname', NULL),
(126, 'client', 'Personal code', NULL),
(127, 'client', 'Registration address', NULL),
(128, 'client', 'Home address', NULL),
(129, 'client', 'About client', NULL),
(130, 'client', 'Staff', NULL),
(131, 'client', 'Client staff', NULL),
(132, 'client', 'For office address to use the legal address', NULL),
(133, 'client', 'For home address to use the registration address', NULL),
(135, 'client', 'Add account', NULL),
(136, 'client', 'Interest period', NULL),
(138, 'languages', 'Communication language', NULL),
(139, 'languages', 'Language', NULL),
(140, 'languages', 'Name', NULL),
(141, 'languages', 'Native', NULL),
(142, 'languages', 'Translation', NULL),
(143, 'bill', 'Invoice', NULL),
(144, 'bill', 'Product', NULL),
(145, 'bill', 'Amount', NULL),
(146, 'bill', 'Price', NULL),
(149, 'bill', 'VAT', NULL),
(150, 'bill', 'Total', NULL),
(151, 'bill', 'Version', NULL),
(152, 'bill', 'Project', NULL),
(153, 'bill', 'Agreement', NULL),
(155, 'bill', 'Abonent', NULL),
(157, 'bill', 'Doc.type', NULL),
(158, 'bill', 'Doc.number', NULL),
(159, 'bill', 'Doc.date', NULL),
(160, 'bill', 'Payment date', NULL),
(161, 'bill', 'First party bank account', NULL),
(162, 'bill', 'Second party bank account', NULL),
(163, 'bill', 'Status', NULL),
(164, 'bill', 'Payment status', NULL),
(165, 'bill', 'Delayed', NULL),
(166, 'bill', 'Author', NULL),
(167, 'bill', 'First party', NULL),
(168, 'bill', 'Second party', NULL),
(174, 'bill', 'New', NULL),
(175, 'bill', 'Ready', NULL),
(176, 'bill', 'Signed', NULL),
(177, 'bill', 'Paid', NULL),
(178, 'bill', 'Canceled', NULL),
(179, 'bill', 'Not paid', NULL),
(180, 'bill', 'Full paid', NULL),
(181, 'bill', 'Partially paid', NULL),
(183, 'bill', 'Add new invoice', NULL),
(184, 'bill', 'Bank account', NULL),
(185, 'bill', 'First party data', NULL),
(186, 'bill', 'Second party data', NULL),
(189, 'bill', 'Payment data', NULL),
(190, 'bill', 'Add product', NULL),
(192, 'bill', 'Party role', NULL),
(194, 'product', 'Measure', NULL),
(196, 'product', 'Description', NULL),
(197, 'address', 'Country', NULL),
(198, 'address', 'Address', NULL),
(199, 'address', 'Start', NULL),
(200, 'address', 'Finish', NULL),
(201, 'address', 'Interim', NULL),
(202, 'address', 'User', NULL),
(203, 'address', 'Company', NULL),
(204, 'address', 'Customs', NULL),
(206, 'address', 'Type address into the text field and select postal address from Google list.', NULL),
(207, 'address', 'Type address into the text field and select postal address from Google list or select location on the map.', NULL),
(208, 'address', 'Open map', NULL),
(209, 'address', 'Get my Location', NULL),
(210, 'files', 'Attachment', NULL),
(211, 'files', 'File name', NULL),
(212, 'files', 'File path', NULL),
(213, 'files', 'File Url', NULL),
(214, 'files', 'File Mime', NULL),
(215, 'files', 'File size', NULL),
(216, 'cargo', 'Tax %', NULL),
(217, 'cargo', 'Attachment pdf file doesn`t exist!', NULL),
(218, 'abonent', 'Abonent', NULL),
(219, 'abonent', 'Main client', NULL),
(220, 'abonent', 'Subscription end date', NULL),
(221, 'abonent', 'Subscription type', NULL),
(223, 'abonent', 'Silver', NULL),
(224, 'abonent', 'Gold', NULL),
(225, 'abonent', 'Platinum', NULL),
(227, 'location', 'Short Name', NULL),
(228, 'location', 'Owner', NULL),
(229, 'location', 'Address type', NULL),
(230, 'location', 'Country', NULL),
(231, 'location', 'State / Province / Region', NULL),
(232, 'location', 'City', NULL),
(233, 'location', 'District', NULL),
(234, 'location', 'Customer Address', NULL),
(235, 'location', 'Company name', NULL),
(236, 'location', 'Contact person', NULL),
(237, 'location', 'Contact phone', NULL),
(238, 'location', 'Contact Email', NULL),
(239, 'location', 'Apartment number', NULL),
(240, 'location', 'Street Number', NULL),
(241, 'location', 'Street', NULL),
(242, 'location', 'Sub District', NULL),
(243, 'location', 'Postal code', NULL),
(244, 'location', 'Latitude', NULL),
(245, 'location', 'Longitude', NULL),
(246, 'location', 'Formatted Address', NULL),
(247, 'app', 'ID', NULL),
(248, 'app', 'Create Time', NULL),
(249, 'app', 'Create User', NULL),
(250, 'admin', 'Admin', NULL),
(251, 'admin', 'Main lists', NULL),
(252, 'admin', 'RBAC', NULL),
(253, 'admin', 'Assignments', NULL),
(254, 'admin', 'Roles', NULL),
(255, 'admin', 'Permissions', NULL),
(256, 'admin', 'Routes', NULL),
(257, 'admin', 'Rules', NULL),
(258, 'admin', 'Menu', NULL),
(259, 'array', 'Inactive', NULL),
(260, 'array', 'Active', NULL),
(261, 'array', 'Beta', NULL),
(262, 'javascript', 'Translation Language: {name}', NULL),
(263, 'javascript', 'Save', NULL),
(264, 'javascript', 'Close', NULL),
(265, 'javascript', 'Are you sure you want to delete these items?', NULL),
(266, 'javascript', 'Are you sure you want to delete this item?', NULL),
(289, 'common', '{count, plural, =0{ euros} =1{ euro} other{ euros}}', NULL),
(290, 'common', '{count, plural, =0{ cents} =1{ cent} other{ cents}}', NULL),
(291, 'common', 'Action', NULL),
(292, 'common', 'Are you sure to do this action?', NULL),
(293, 'common', 'Main agreement', NULL),
(294, 'common', 'Back', NULL),
(295, 'common', 'Add new', NULL),
(296, 'common', 'Refresh', NULL),
(297, 'common', 'Progress', NULL),
(298, 'common', 'Options', NULL),
(299, 'common', 'Period', NULL),
(300, 'common', 'From', NULL),
(301, 'common', 'Till', NULL),
(302, 'client', 'Can\'t to start procedure of searching.', NULL),
(303, 'client', 'Can\'t to connect to Lursoft server.', NULL),
(304, 'client', 'Can\'t find any data.', NULL),
(305, 'client', 'Curator', NULL),
(306, 'client', 'This VAT number has already been taken.', NULL),
(307, 'client', 'Country', NULL),
(308, 'client', 'First name', NULL),
(309, 'client', 'Last name', NULL),
(310, 'client', 'Phone', NULL),
(311, 'client', 'Email', NULL),
(312, 'client', 'Position', NULL),
(313, 'client', 'Can sign', NULL),
(314, 'client', 'Term from', NULL),
(315, 'client', 'Term till', NULL),
(316, 'client', 'Share', NULL),
(317, 'client', 'Is top manager', NULL),
(318, 'client', 'Shareholder', NULL),
(319, 'client', 'Share (%)', NULL),
(320, 'client', 'Signing person', NULL),
(321, 'client', 'Loan agreement', NULL),
(322, 'client', 'Check IBAN', NULL),
(323, 'client', 'Get data from Lursoft service', NULL),
(324, 'client', 'Get data from VIES service', NULL),
(325, 'client', 'Our shares', NULL),
(326, 'client', 'Our shareholders', NULL),
(327, 'client', 'All projects', NULL),
(328, 'client', 'All agreements', NULL),
(329, 'bill', '\"Parent\"', NULL),
(330, 'bill', '\"Child\"', NULL),
(331, 'bill', 'First party signing person', NULL),
(332, 'bill', 'Second party signing person', NULL),
(333, 'bill', 'According contract', NULL),
(334, 'bill', 'Service period', NULL),
(335, 'bill', 'Loading address', NULL),
(336, 'bill', 'Unloading address', NULL),
(337, 'bill', 'Carrier', NULL),
(338, 'bill', 'Transport', NULL),
(339, 'bill', 'Credit invoice', NULL),
(343, 'bill', 'Send for signature', NULL),
(346, 'bill', 'Sign', NULL),
(350, 'bill', 'Write out on this basis', NULL),
(352, 'bill', 'Cession', NULL),
(353, 'bill', 'Debt relief', NULL),
(354, 'bill', 'Revers', NULL),
(355, 'bill', 'The same clients are selected!', NULL),
(356, 'bill', 'The same shareholder are selected!', NULL),
(357, 'bill', 'revers', NULL),
(358, 'bill', 'Agreement No.', NULL),
(359, 'bill', 'VAT reverse charge intra community supply', NULL),
(361, 'bill', 'delayed {count, plural, =1{one day} other{# days}}', NULL),
(362, 'bill', 'after {count, plural, =1{one day} other{# days}}', NULL),
(363, 'bill', 'All invoices', NULL),
(364, 'agreement', 'First party', NULL),
(365, 'agreement', 'Second party', NULL),
(366, 'agreement', 'Third party', NULL),
(367, 'agreement', 'First party role', NULL),
(368, 'agreement', 'Second party role', NULL),
(369, 'agreement', 'Third party role', NULL),
(370, 'agreement', 'Project name', NULL),
(371, 'agreement', 'VAT taxable transaction', NULL),
(372, 'files', 'Logo', NULL),
(379, 'action', 'History', NULL),
(385, 'bill', 'Invoice number', NULL),
(386, 'bill', 'Action', NULL),
(387, 'bill', 'Comment', NULL),
(388, 'bill', 'Action time', NULL),
(390, 'bill', 'Performer', NULL),
(391, 'javascript', 'This project is non taxable and you will not have the opportunity to charge VAT', NULL),
(392, 'common', 'Export', NULL),
(393, 'action', 'Creation', NULL),
(394, 'action', 'Editing', NULL),
(395, 'action', 'Deleting', NULL),
(396, 'action', 'Change status to \"New\"', NULL),
(397, 'action', 'Change status to \"Ready\"', NULL),
(398, 'action', 'Change status to \"Signed\"', NULL),
(399, 'action', 'Change status to \"Payment\"', NULL),
(400, 'action', 'Confirmed payment', NULL),
(401, 'action', 'Change status to \"Canceled\"', NULL),
(402, 'action', 'Rollback status to \"Preparing\"', NULL),
(403, 'action', 'Rollback status to \"New\"', NULL),
(404, 'action', 'Rollback status to \"Ready\"', NULL),
(405, 'action', 'Rollback status to \"Signed\"', NULL),
(406, 'action', 'Rollback status to \"Payment\"', NULL),
(407, 'bill', 'Invoices history', NULL),
(408, 'bill', 'Payments', NULL),
(409, 'bill', 'Check delayed invoices', NULL),
(410, 'bill', 'Preparation', NULL),
(411, 'bill', 'Payment', NULL),
(412, 'bill', 'Complete', NULL),
(413, 'bill', 'Overpaid', NULL),
(414, 'bill', 'Register', NULL),
(415, 'bill', 'To preparing', NULL),
(416, 'bill', 'Rollback to preparing', NULL),
(417, 'bill', 'To signature', NULL),
(418, 'bill', 'To verification', NULL),
(419, 'bill', 'Rollback to verification', NULL),
(420, 'bill', 'To signing', NULL),
(421, 'bill', 'Rollback to signing', NULL),
(423, 'bill', 'To payment preparation', NULL),
(424, 'bill', 'Rollback to payment preparation', NULL),
(425, 'bill', 'Confirm payment', NULL),
(427, 'bill', 'Rollback to payment', NULL),
(428, 'bill', 'Invoice history ID', NULL),
(430, 'bill', 'Invoice number', NULL),
(431, 'bill', 'Number', NULL),
(432, 'bill', 'State', NULL),
(433, 'bill', 'File name', NULL),
(434, 'bill', 'Sent time', NULL),
(435, 'bill', 'Create Time', NULL),
(436, 'bill', 'Create User ID', NULL),
(437, 'bill', 'Update Time', NULL),
(438, 'bill', 'Update User ID', NULL),
(439, 'bill', 'XML file name', NULL),
(440, 'bill', 'Sent', NULL),
(441, 'bill', 'There {n, plural, =0{are not found delayed invoices} =1{is found <span style=\"font-size: 1.5em; color: red;\">one</span> invoice} other{are was found <span style=\"font-size: 1.5em; color: red;\">#</span> invoices}}!', NULL),
(442, 'bill', 'Status was be changed to \"Delayed\"', NULL),
(443, 'bill', 'Debt', NULL),
(446, 'bill', 'Payment confirmations', NULL),
(447, 'agreement', 'Agreement type', NULL),
(448, 'agreement', 'Cooperation', NULL),
(449, 'agreement', 'Loan', NULL),
(450, 'agreement', 'Cession', NULL),
(451, 'agreement', 'Agency', NULL),
(460, 'report', 'EBITDA report details: ', NULL),
(461, 'report', 'Debtors/Creditors report details: ', NULL),
(462, 'report', 'VAT report details: ', NULL),
(463, 'report', 'Reports', NULL),
(464, 'report', 'Debtors/Creditors', NULL),
(465, 'report', 'EBITDA', NULL),
(466, 'report', 'VAT Report', NULL),
(467, 'report', 'Bank statements', NULL),
(468, 'report', 'Details', NULL),
(469, 'report', 'Select project ...', NULL),
(470, 'report', 'VAT report', NULL),
(471, 'report', 'Select client ...', NULL),
(472, 'report', 'Sales', NULL),
(473, 'report', 'Purchases', NULL),
(474, 'report', 'Profit or Loss', NULL),
(475, 'report', 'VAT+', NULL),
(476, 'report', 'VAT-', NULL),
(477, 'report', 'VAT result', NULL),
(478, 'report', 'Debtors', NULL),
(479, 'report', 'Creditors', NULL),
(480, 'bill', 'Cannot to create PDF file', NULL),
(481, 'bill', 'Cannot to export XML file without invoices', NULL),
(482, 'bill', 'The XML file has generated!', NULL),
(483, 'bill', 'The export of the XML file was completed with error!', NULL),
(484, 'bill', 'The PDF file was saved with error!', NULL),
(485, 'bill', 'The XML file data was saved with error!', NULL),
(486, 'bill', 'Cannot save XML file data!', NULL),
(487, 'bill', 'Cannot save new bank account!', NULL),
(488, 'bill', 'The XML file data saving is complete!', NULL),
(489, 'bill', 'Cannot parse XML file!', NULL),
(490, 'bill', 'The XML file was not uploaded!', NULL),
(491, 'bill', 'Cannot import data without invoices', NULL),
(492, 'bill', 'The import from the XML file is complete!', NULL),
(493, 'bill', 'The import was completed with error!', NULL),
(494, 'bill', 'The XML file was saved with error!', NULL),
(495, 'bill', 'The bank account data was saved with error!', NULL),
(496, 'bill', 'Add to prepared payment', NULL),
(497, 'bill', 'Invoice list', NULL),
(498, 'bill', 'Start-End dates', NULL),
(499, 'bill', 'XML filename to import', NULL),
(500, 'bill', 'PDF filename to import', NULL),
(501, 'bill', 'Export / Import', NULL),
(502, 'bill', 'Prepared payments', NULL),
(503, 'bill', 'Add new expense', NULL),
(504, 'bill', 'All expenses', NULL),
(505, 'bill', 'Bank', NULL),
(506, 'bill', 'Payment confirmation', NULL),
(507, 'bill', 'History ID', NULL),
(508, 'bill', 'Invoice payment', NULL),
(509, 'bill', 'Client account', NULL),
(510, 'bill', 'Second client name', NULL),
(511, 'bill', 'Second client reg.number', NULL),
(512, 'bill', 'Second client account', NULL),
(513, 'bill', 'Second client', NULL),
(514, 'bill', 'Bank reference', NULL),
(515, 'bill', 'Direction', NULL),
(516, 'bill', 'Sum', NULL),
(517, 'bill', 'Debet', NULL),
(518, 'bill', 'Credit', NULL),
(519, 'bill', 'Prepared payment', NULL),
(520, 'bill', 'Confirmed', NULL),
(521, 'bill', 'Due date', NULL),
(522, 'bill', 'Final payment date', NULL),
(523, 'bill', 'Without signing', NULL),
(524, 'bill', 'Payment prep.', NULL),
(525, 'bill', 'Set-off', NULL),
(526, 'bill', 'Prepare payment', NULL),
(527, 'bill', 'To payment', NULL),
(528, 'bill', 'Complete invoice', NULL),
(529, 'bill', 'Expense type', NULL),
(530, 'bill', 'Create User', NULL),
(531, 'bill', 'Update User', NULL),
(532, 'bill', 'Client name', NULL),
(533, 'bill', 'Client reg.number', NULL),
(534, 'bill', 'Name', NULL),
(535, 'bill', 'Start date', NULL),
(536, 'bill', 'End date', NULL),
(537, 'bill', 'Preparation date', NULL),
(538, 'bill', 'PDF file name', NULL),
(539, 'bill', 'Import time', NULL),
(540, 'bill', 'In process', NULL),
(541, 'bill', 'Cannot find bank by its reg.number!', NULL),
(542, 'bill', 'Cannot find client by its reg.number!', NULL),
(543, 'bill', 'Līguma No.', NULL),
(544, 'client', 'The client Cannot be removed. This is the main client of the abonent.', NULL),
(545, 'client', 'Our clients', NULL),
(546, 'client', 'Add new bank account balance', NULL),
(547, 'client', 'Sum', NULL),
(548, 'client', 'Conclusion', NULL),
(549, 'client', 'Reg.document type', NULL),
(550, 'client', 'Doc.number', NULL),
(551, 'client', 'Doc.date', NULL),
(552, 'client', 'Expiration date', NULL),
(553, 'client', 'Placement', NULL),
(554, 'client', 'Notification days', NULL),
(555, 'client', 'Comment', NULL),
(556, 'client', 'XML file', NULL),
(557, 'client', 'PDF file', NULL),
(558, 'client', 'Balance', NULL),
(559, 'client', 'Client group', NULL),
(560, 'client', 'SOAPClient is\'t installed!', NULL),
(561, 'agreement', 'Deferment of payment', NULL),
(562, 'common', 'Attachments', NULL),
(563, 'common', 'Select', NULL),
(564, 'common', 'Print language', NULL),
(565, 'common', 'Import', NULL),
(566, 'common', 'Ok', NULL),
(567, 'app', 'Designer', NULL),
(568, 'bank', 'Connect', NULL),
(569, 'bank', 'SWIFT code', NULL),
(570, 'bank', 'WWW', NULL),
(571, 'action', 'Change status to \"Payment preparation\"', NULL),
(572, 'action', 'Change status to \"Complete\"', NULL),
(573, 'action', 'Rollback status to \"Payment preparation\"', NULL),
(574, 'database', 'Austria', NULL),
(575, 'database', 'Cyprus', NULL),
(576, 'database', 'Estonia', NULL),
(577, 'database', 'Germany', NULL),
(578, 'database', 'Latvia', NULL),
(579, 'database', 'Lithuania', NULL),
(580, 'database', 'Malta', NULL),
(581, 'database', 'Panama', NULL),
(582, 'database', 'Poland', NULL),
(583, 'database', 'United Kingdom', NULL),
(584, 'database', 'Hungary', NULL);

INSERT INTO `language_translate` (`id`, `language`, `translation`) VALUES
(289, 'lv-LV', '{count, plural, =0{ eiro} =1{ eiro} other{ eiro}}'),
(290, 'lv-LV', '{count, plural, =0{ centu} =1{ cents} other{ centi}}'),
(574, 'lv-LV', 'Austrija'),
(575, 'lv-LV', 'Kipra'),
(576, 'lv-LV', 'Igaunija'),
(577, 'lv-LV', 'Vācija'),
(578, 'lv-LV', 'Latvija'),
(579, 'lv-LV', 'Lietuva'),
(580, 'lv-LV', 'Malta'),
(581, 'lv-LV', 'Panama'),
(582, 'lv-LV', 'Polija'),
(583, 'lv-LV', 'Lielbritānija');

--
-- Дамп данных таблицы `bank`
--

INSERT INTO `bank` (`id`, `name`, `reg_number`, `swift`, `address`, `home_page`, `enabled`) VALUES
(1, 'Aizkraukles banka', '---', 'AIZKLV22', 'Balasta dambis 1a, Rīga, LV-1048', NULL, 1),
(2, 'DnB Banka', '', 'RIKOLV2X', '', NULL, 1),
(3, 'Latvijas Banka', '', 'LACBLV2X', '', NULL, 1),
(4, 'Latvijas Hipotēku un zemes banka', '', 'LHZBLV22', '', NULL, 1),
(5, 'PRIVATBANK', '', 'PRTTLV22', '', NULL, 1),
(6, 'Citadele banka', '', 'PARXLV22', '', NULL, 1),
(7, 'Rietumu Banka', '', 'RTMBLV2X', '', NULL, 1),
(8, 'SEB Banka', '40003151743', 'UNLALV2X', 'K. Valdemāra.62, Rīga, LV-1013', NULL, 1),
(9, 'SMP Bank', '', 'MULTLV2X', '', NULL, 1),
(10, 'Swedbank', '40003074764', 'HABALV22', 'Balasta dambis 15, Riga, LV-1048, Latvia', NULL, 1),
(11, 'Valsts Kase', '', 'TRELLV22', '', NULL, 1),
(12, 'MEINL BANK', '', 'MEINATWW', '', NULL, 1),
(13, 'BlueOrange Bank', '---', 'CBBRLV22', '---', NULL, 1),
(14, 'Nordea', '', 'NDEALV2X', '', NULL, 1),
(15, 'Pasta Norēķinu Konts', '', 'PN223366', '', NULL, 1),
(16, 'Swedbank AS', '40003074764', 'HABALV22', 'Balasta dambis 15, Riga, LV-1048, Latvia', NULL, 1),
(17, 'Danske Bank', '', 'MARALV22', '', NULL, 1),
(18, 'ING Bank', '5050505', '21212121', 'Poland', NULL, 1);

--
-- Дамп данных таблицы `client_role`
--

INSERT INTO `client_role` (`id`, `name`, `enabled`) VALUES
(1, 'Seller', 1),
(2, 'Buyer', 1),
(3, 'Agent', 1),
(4, 'Renter', 1),
(5, 'Performer', 1),
(6, 'Customer', 1),
(7, 'Borrower', 1),
(8, 'Guarantor', 1),
(9, 'Cessioner', 1),
(10, 'Lender', 1),
(11, 'Executor', 1),
(12, 'Client', 1),
(13, 'Assignor', 1),
(14, 'Assignee', 1),
(15, 'Debtor', 1),
(16, 'Party 1', 1),
(17, 'Party 2', 1),
(22, 'Bank', 1),
(23, 'Broker', 1),
(24, 'Diler', 1),
(25, 'Principal', 1);

--
-- Дамп данных таблицы `expense_type`
--

INSERT INTO `expense_type` (`id`, `name`) VALUES
(2, 'Business trip'),
(3, 'Check'),
(5, 'Depreciation of fixed assets'),
(6, 'Reserve'),
(1, 'Salary'),
(4, 'Tax');

--
-- Дамп данных таблицы `languages`
--

INSERT INTO `languages` (`id`, `language`, `name`, `native`, `enabled`) VALUES
(1, 'en', 'English', 'English', 1),
(2, 'ru', 'Russian', 'Русский', 1),
(3, 'lv', 'Latvian', 'Latviešu', 1);

--
-- Дамп данных таблицы `measure`
--

INSERT INTO `measure` (`id`, `name`, `description`) VALUES
(1, 'd', 'Days'),
(2, 'mon.', 'Months'),
(3, 'y', 'Years'),
(4, 'pcs', 'Pieces'),
(5, 'kg', 'Kilograms'),
(6, 'm', 'Meters'),
(7, 'l', 'Liters'),
(8, 'm2', 'Square meter'),
(9, 'Atlīdzība', NULL),
(10, 'Travel', NULL),
(11, 'm3', NULL);

--
-- Дамп данных таблицы `person_position`
--

INSERT INTO `person_position` (`id`, `name`) VALUES
(2, 'Director'),
(1, 'Valdes loceklis');

--
-- Дамп данных таблицы `reg_doc_type`
--

INSERT INTO `reg_doc_type` (`id`, `name`) VALUES
(2, 'Charter'),
(4, 'Power of attorney'),
(3, 'Protocol'),
(1, 'Registration certificate');

--
-- Дамп данных таблицы `valuta`
--

INSERT INTO `valuta` (`id`, `name`, `int_0`, `int_1`, `int_2`, `floor_0`, `floor_1`, `floor_2`, `enabled`) VALUES
(1, 'EUR', 'euros', 'euro', 'euros', 'cents', 'cent', 'cents', 1),
(2, 'USD', 'dollars', 'dollar', 'dollars', 'cents', 'cent', 'cents', 1),
(3, 'RUB', 'рублей', 'рубль', 'рублей', 'копеек', 'копейка', 'копеек', 1),
(4, 'PLN', 'zloty', 'zloty', 'zloty', 'grosz', 'grosz', 'grosz', 1);

-- DONE INSERTS
";
        $this->execute($sqlExternal);
        $this->execute($sqlTables);
        $this->execute($sqlInsert);
        $this->execute($sqlIndex);
        $this->execute($sqlForeign);
        return true;
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        echo "m170822_101012_init_db cannot be reverted.\n";
        
        $sql =
" 
--
-- Ограничения внешнего ключа таблицы `language_translate`
--
ALTER TABLE `language_translate`
    DROP FOREIGN KEY `language_translate_ibfk_1`,
    DROP FOREIGN KEY `language_translate_ibfk_2`;

--
-- Ограничения внешнего ключа таблицы `profile`
--
ALTER TABLE `profile`
    DROP FOREIGN KEY `fk_profile_create_user_id`,
    DROP FOREIGN KEY `fk_profile_update_user_id`,
    DROP FOREIGN KEY `fk_profile_user_id`,
    DROP FOREIGN KEY `fk_profile_language_id`;

--
-- Ограничения внешнего ключа таблицы `social_account`
--
ALTER TABLE `social_account`
    DROP FOREIGN KEY `fk_user_account`;

--
-- Ограничения внешнего ключа таблицы `token`
--
ALTER TABLE `token`
    DROP FOREIGN KEY `fk_user_token`;  
  
--
-- Ограничения внешнего ключа таблицы `abonent`
--
ALTER TABLE abonent 
    DROP FOREIGN KEY fk_abonent_create_user_id,
    DROP FOREIGN KEY fk_abonent_main_client_id,
    DROP FOREIGN KEY fk_abonent_manager_id,
    DROP FOREIGN KEY fk_abonent_update_user_id;

--
-- Ограничения внешнего ключа таблицы `address`
--
ALTER TABLE `address`
  DROP FOREIGN KEY `fk_address_city_id`,
  DROP FOREIGN KEY `fk_address_country_id`,
  DROP FOREIGN KEY `fk_address_create_user_id`,
  DROP FOREIGN KEY `fk_address_district_id`,
  DROP FOREIGN KEY `fk_address_region_id`,
  DROP FOREIGN KEY `fk_address_update_user_id`;

--
-- Ограничения внешнего ключа таблицы `agreement`
--
ALTER TABLE `agreement`
  DROP FOREIGN KEY `fk_agreement_create_user_id`,
  DROP FOREIGN KEY `fk_agreement_first_client_id`,
  DROP FOREIGN KEY `fk_agreement_first_client_role_id`,
  DROP FOREIGN KEY `fk_agreement_parent_id`,
  DROP FOREIGN KEY `fk_agreement_second_client_id`,
  DROP FOREIGN KEY `fk_agreement_second_client_role_id`,
  DROP FOREIGN KEY `fk_agreement_third_client_id`,
  DROP FOREIGN KEY `fk_agreement_third_client_role_id`,
  DROP FOREIGN KEY `fk_agreement_update_user_id`,
  DROP FOREIGN KEY `fk_agreement_valuta_id`;

--
-- Ограничения внешнего ключа таблицы `bill`
--
ALTER TABLE `bill`
  DROP FOREIGN KEY `fk_bill_agreement_id`,
  DROP FOREIGN KEY `fk_bill_child_id`,
  DROP FOREIGN KEY `fk_bill_create_user_id`,
  DROP FOREIGN KEY `fk_bill_first_client_bank_id`,
  DROP FOREIGN KEY `fk_bill_language_id`,
  DROP FOREIGN KEY `fk_bill_manager_id`,
  DROP FOREIGN KEY `fk_bill_parent_id`,
  DROP FOREIGN KEY `fk_bill_second_client_bank_id`,
  DROP FOREIGN KEY `fk_bill_update_user_id`,
  DROP FOREIGN KEY `fk_bill_valuta_id`;

--
-- Ограничения внешнего ключа таблицы `bill_confirm`
--
ALTER TABLE `bill_confirm`
  DROP FOREIGN KEY `fk_bill_confirm_payment_confirm_id`,
  DROP FOREIGN KEY `fk_bill_confirm_second_client_id`,
  DROP FOREIGN KEY `fk_bill_confirm_valuta_id`;

--
-- Ограничения внешнего ключа таблицы `bill_payment`
--
ALTER TABLE `bill_payment`
  DROP FOREIGN KEY `fk_bill_payment_bill_history_id`,
  DROP FOREIGN KEY `fk_bill_payment_payment_order_id`,
  DROP FOREIGN KEY `fk_bill_paymentl_bill_id`,
  DROP FOREIGN KEY fk_bill_payment_from_bank_id,
  DROP FOREIGN KEY fk_bill_payment_to_bank_id,
  DROP FOREIGN KEY fk_bill_payment_valuta_id;

--
-- Ограничения внешнего ключа таблицы `bill_person`
--  
ALTER TABLE `bill_person`
  DROP FOREIGN KEY `fk_bill_person_bill_id`,
  DROP FOREIGN KEY `fk_bill_person_client_person_id`;
  
--
-- Ограничения внешнего ключа таблицы `bill_product`
--
ALTER TABLE `bill_product`
  DROP FOREIGN KEY `fk_bill_product_bill_id`,
  DROP FOREIGN KEY `fk_bill_product_measure_id`,
  DROP FOREIGN KEY `fk_bill_product_product_id`;
  
--
-- Ограничения внешнего ключа таблицы `bill_template_product`
--
ALTER TABLE `bill_template_product`
  DROP FOREIGN KEY `fk_bill_template_product_bill_template_id`,
  DROP FOREIGN KEY `fk_bill_template_product_measure_id`,
  DROP FOREIGN KEY `fk_bill_template_product_product_id`;
  

--
-- Ограничения внешнего ключа таблицы `bill_template`
--
ALTER TABLE `bill_template`
  DROP FOREIGN KEY `fk_bill_template_agreement_id`,
  DROP FOREIGN KEY `fk_bill_template_first_client_bank_id`,
  DROP FOREIGN KEY `fk_bill_template_second_client_bank_id`,
  DROP FOREIGN KEY `fk_bill_template_valuta_id`,
  DROP FOREIGN KEY `fk_bill_template_language_id`,
  DROP FOREIGN KEY `fk_bill_template_create_user_id`,
  DROP FOREIGN KEY `fk_bill_template_update_user_id`;
  
--
-- Ограничения внешнего ключа таблицы `client`
--
ALTER TABLE `client`
  DROP FOREIGN KEY `fk_client_legal_country_id`,
  DROP FOREIGN KEY `fk_client_office_country_id`,
  DROP FOREIGN KEY `fk_client_uploaded_file_id`;
  
--
-- Ограничения внешнего ключа таблицы `abonent_client`
--
ALTER TABLE `abonent_client`
  DROP FOREIGN KEY `fk_abonent_client_abonent_id`,
  DROP FOREIGN KEY `fk_abonent_client_client_id`,
  DROP FOREIGN KEY `fk_abonent_client_client_group_id`,
  DROP FOREIGN KEY `fk_abonent_client_create_user_id`,
  DROP FOREIGN KEY `fk_abonent_client_debit_valuta_id`,
  DROP FOREIGN KEY `fk_abonent_client_language_id`,
  DROP FOREIGN KEY `fk_abonent_client_manager_id`,
  DROP FOREIGN KEY `fk_abonent_client_agent_id`,
  DROP FOREIGN KEY `fk_abonent_client_parent_id`;
  
--
-- Ограничения внешнего ключа таблицы `client_bank`
--
ALTER TABLE `client_bank`
  DROP FOREIGN KEY `fk_client_bank_abonent_id`,
  DROP FOREIGN KEY `fk_client_bank_bank_id`,
  DROP FOREIGN KEY `fk_client_bank_client_id`,
  DROP FOREIGN KEY `fk_client_bank_uploaded_pdf_file_id`;

--
-- Ограничения внешнего ключа таблицы `client_bank_balance`
--
ALTER TABLE `client_bank_balance`
  DROP FOREIGN KEY `fk_client_bank_balance_account_id`,
  DROP FOREIGN KEY `fk_client_bank_balance_payment_confirm_id`,
  DROP FOREIGN KEY `fk_client_bank_balance_uploaded_file_id`,
  DROP FOREIGN KEY `fk_client_bank_balance_uploaded_pdf_file_id`;

--
-- Ограничения внешнего ключа таблицы `client_contact`
--
ALTER TABLE `client_contact`
  DROP FOREIGN KEY `fk_client_contact_client_id`,
  DROP FOREIGN KEY `fk_client_contact_abonent_id`,
  DROP FOREIGN KEY `fk_client_contact_position_id`;
  
--
-- Ограничения внешнего ключа таблицы `client_invoice_pdf`
--
ALTER TABLE `client_invoice_pdf`
  DROP FOREIGN KEY `fk_client_invoice_pdf_abonent_id`,
  DROP FOREIGN KEY `fk_client_invoice_pdf_client_id`,
  DROP FOREIGN KEY `fk_client_invoice_pdf_uploaded_file_id`,
  DROP FOREIGN KEY `fk_client_invoice_pdf_create_user_id`,
  DROP FOREIGN KEY `fk_client_invoice_pdf_update_user_id`;
  
--
-- Ограничения внешнего ключа таблицы `client_reg_doc`
--
ALTER TABLE `client_reg_doc`
  DROP FOREIGN KEY `fk_client_reg_doc_abonent_id`,
  DROP FOREIGN KEY `fk_client_reg_doc_client_id`,
  DROP FOREIGN KEY `fk_client_reg_doc_reg_doc_type_id`,
  DROP FOREIGN KEY `fk_client_reg_doc_uploaded_file_id`;
  
--
-- Ограничения внешнего ключа таблицы `client_mail_template`
--
ALTER TABLE `client_mail_template`
  DROP FOREIGN KEY `fk_client_mail_template_abonent_id`,
  DROP FOREIGN KEY `fk_client_mail_template_client_id`,
  DROP FOREIGN KEY `fk_client_mail_template_uploaded_file_id`,
  DROP FOREIGN KEY `fk_client_mail_template_create_user_id`,
  DROP FOREIGN KEY `fk_client_mail_template_update_user_id`;

--
-- Ограничения внешнего ключа таблицы `expense`
--
ALTER TABLE `expense`
  DROP FOREIGN KEY `fk_expense_abonent_id`,
  DROP FOREIGN KEY `fk_expense_create_user_id`,
  DROP FOREIGN KEY `fk_expense_expense_type_id`,
  DROP FOREIGN KEY `fk_expense_first_client_id`,
  DROP FOREIGN KEY `fk_expense_second_client_id`,
  DROP FOREIGN KEY `fk_expense_update_user_id`,
  DROP FOREIGN KEY `fk_expense_valuta_id`,
  DROP FOREIGN KEY `fk_expensel_project_id`;

--
-- Ограничения внешнего ключа таблицы `event`
--
ALTER TABLE `expense`
  DROP FOREIGN KEY `fk_event_abonent_id`,
  DROP FOREIGN KEY `fk_event_create_user_id`,
  DROP FOREIGN KEY `fk_event_manager_id`,
  DROP FOREIGN KEY `fk_event_parent_event_id`,
  DROP FOREIGN KEY `fk_event_update_user_id`;
  
--
-- Ограничения внешнего ключа таблицы `bill_history`
--
ALTER TABLE `bill_history`
  DROP FOREIGN KEY `fk_bill_history_bill_id`,
  DROP FOREIGN KEY `fk_bill_history_create_user_id`;

--
-- Ограничения внешнего ключа таблицы `location_city`
--
ALTER TABLE `location_city`
  DROP FOREIGN KEY `fk_city_country_id`,
  DROP FOREIGN KEY `fk_city_region_id`;

--
-- Ограничения внешнего ключа таблицы `location_district`
--
ALTER TABLE `location_district`
  DROP FOREIGN KEY `fk_district_city_id`,
  DROP FOREIGN KEY `fk_district_country_id`,
  DROP FOREIGN KEY `fk_district_region_id`;

--
-- Ограничения внешнего ключа таблицы `location_region`
--
ALTER TABLE `location_region`
  DROP FOREIGN KEY `fk_region_country_id`;

--
-- Ограничения внешнего ключа таблицы `payment_confirm`
--
ALTER TABLE `password_history`
  DROP FOREIGN KEY `fk_password_history_user_id`;
--
-- Ограничения внешнего ключа таблицы `payment_confirm`
--
ALTER TABLE `payment_confirm`
  DROP FOREIGN KEY `fk_payment_confirm_action_user_id`,
  DROP FOREIGN KEY `fk_payment_confirm_abonent_id`,
  DROP FOREIGN KEY `fk_payment_confirm_bank_id`,
  DROP FOREIGN KEY `fk_payment_confirm_client_id`,
  DROP FOREIGN KEY `fk_payment_confirm_create_user_id`,
  DROP FOREIGN KEY `fk_payment_confirm_update_user_id`,
  DROP FOREIGN KEY `fk_payment_confirm_uploaded_file_id`,
  DROP FOREIGN KEY `fk_payment_confirm_uploaded_pdf_file_id`;

--
-- Ограничения внешнего ключа таблицы `payment_order`
--
ALTER TABLE `payment_order`
  DROP FOREIGN KEY `fk_payment_payment_order_id`,
  DROP FOREIGN KEY `fk_payment_order_client_id`,
  DROP FOREIGN KEY `fk_payment_order_file_id`,
  DROP FOREIGN KEY `fk_payment_order_action_user_id`,
  DROP FOREIGN KEY `fk_payment_order_create_user_id`,
  DROP FOREIGN KEY `fk_payment_order_update_user_id`;

--
-- Ограничения внешнего ключа таблицы `product`
--
ALTER TABLE `product`
  DROP FOREIGN KEY `fk_product_measure_id`;

--
-- Ограничения внешнего ключа таблицы `project`
--
ALTER TABLE `project`
  DROP FOREIGN KEY `fk_project_country_id`,
  DROP FOREIGN KEY `fk_project_create_user_id`,
  DROP FOREIGN KEY `fk_project_update_user_id`;

--
-- Ограничения внешнего ключа таблицы `shareholder`
--
ALTER TABLE `shareholder`
  DROP FOREIGN KEY `fk_share_client_id`,
  DROP FOREIGN KEY `fk_share_shareholder_id`;
  
ALTER TABLE agreement_attachment 
    DROP FOREIGN KEY fk_agreement_attachment_agreement_id,
    DROP FOREIGN KEY fk_agreement_attachment_uploaded_file_id;
  
ALTER TABLE agreement_history 
    DROP FOREIGN KEY fk_agreement_history_agreement_id,
    DROP FOREIGN KEY fk_agreement_history_create_user_id;
  
ALTER TABLE agreement_payment 
    DROP FOREIGN KEY fk_agreement_payment_agreement_history_id,
    DROP FOREIGN KEY fk_agreement_payment_from_bank_id,
    DROP FOREIGN KEY fk_agreement_payment_to_bank_id,
    DROP FOREIGN KEY fk_agreement_payment_agreement_id,
    DROP FOREIGN KEY fk_agreement_payment_valuta_id;
    
ALTER TABLE project_attachment 
    DROP FOREIGN KEY fk_project_attachment_project_id,
    DROP FOREIGN KEY fk_project_attachment_uploaded_file_id;

ALTER TABLE bill_attachment 
    DROP FOREIGN KEY fk_bill_attachment_bill_id,
    DROP FOREIGN KEY fk_bill_attachment_uploaded_file_id;

ALTER TABLE bill_account 
    DROP FOREIGN KEY fk_bill_account_bill_id,
    DROP FOREIGN KEY fk_bill_account_first_account_id,
    DROP FOREIGN KEY fk_bill_account_second_account_id;
    
ALTER TABLE profile_client 
    DROP FOREIGN KEY fk_profile_client_client_id,
    DROP FOREIGN KEY fk_profile_client_profile_id,
    DROP FOREIGN KEY fk_profile_client_abonent_id;

ALTER TABLE event_event_resurs 
    DROP FOREIGN KEY fk_event_event_resurs_event_id,
    DROP FOREIGN KEY fk_event_event_resurs_event_resurs_id;

ALTER TABLE tax_payment 
    DROP FOREIGN KEY fk_tax_payment_abonent_id,
    DROP FOREIGN KEY fk_tax_payment_from_client_id,
    DROP FOREIGN KEY fk_tax_payment_to_client_id,
    DROP FOREIGN KEY fk_tax_payment_valuta_id;    
-- --------------------------------------------------------

DROP TABLE abonent_agreement;
DROP TABLE abonent_client;
DROP TABLE abonent_bill;
DROP TABLE abonent_bill_template;
DROP TABLE abonent_product;

DROP TABLE IF EXISTS `abonent`;
DROP TABLE IF EXISTS `address`;
DROP TABLE IF EXISTS `agreement`;
DROP TABLE IF EXISTS `agreement_attachment`;
DROP TABLE IF EXISTS `agreement_history`;
DROP TABLE IF EXISTS `agreement_payment`;
DROP TABLE IF EXISTS `bank`;
DROP TABLE IF EXISTS `bill`;
DROP TABLE IF EXISTS `bill_account`;
DROP TABLE IF EXISTS `bill_template`;
DROP TABLE IF EXISTS `bill_attachment`;
DROP TABLE IF EXISTS `bill_confirm`;
DROP TABLE IF EXISTS `bill_payment`;
DROP TABLE IF EXISTS `bill_person`;
DROP TABLE IF EXISTS `bill_product`;
DROP TABLE IF EXISTS `bill_template_person`;
DROP TABLE IF EXISTS `bill_template_product`;
DROP TABLE IF EXISTS `client`;
DROP TABLE IF EXISTS `client_bank`;
DROP TABLE IF EXISTS `client_bank_balance`;
DROP TABLE IF EXISTS `client_contact`;
DROP TABLE IF EXISTS `client_invoice_pdf`;
DROP TABLE IF EXISTS `client_reg_doc`;
DROP TABLE IF EXISTS `client_mail_template`;
DROP TABLE IF EXISTS `client_group`;
DROP TABLE IF EXISTS `client_role`;
DROP TABLE IF EXISTS `event_profile`;
DROP TABLE IF EXISTS `event`;
DROP TABLE IF EXISTS `event_contact`;
DROP TABLE IF EXISTS `event_event_resurs`;
DROP TABLE IF EXISTS `event_resurs`;
DROP TABLE IF EXISTS `expense`;
DROP TABLE IF EXISTS `expense_attachment`;
DROP TABLE IF EXISTS `expense_type`;
DROP TABLE IF EXISTS `files`;
DROP TABLE IF EXISTS `bill_history`;
DROP TABLE IF EXISTS `languages`;
DROP TABLE IF EXISTS `location_city`;
DROP TABLE IF EXISTS `location_country`;
DROP TABLE IF EXISTS `location_district`;
DROP TABLE IF EXISTS `location_region`;
DROP TABLE IF EXISTS `log_abonent`;
DROP TABLE IF EXISTS `log_address`;
DROP TABLE IF EXISTS `log_agreement`;
DROP TABLE IF EXISTS `log_bill`;
DROP TABLE IF EXISTS `log_client`;
DROP TABLE IF EXISTS `log_event`;
DROP TABLE IF EXISTS `log_profile`;
DROP TABLE IF EXISTS `log_project`;
DROP TABLE IF EXISTS `measure`;
DROP TABLE IF EXISTS `password_history`;
DROP TABLE IF EXISTS `payment_confirm`;
DROP TABLE IF EXISTS `payment_order`;
DROP TABLE IF EXISTS `person_position`;
DROP TABLE IF EXISTS `product`;
DROP TABLE IF EXISTS `profile_client`;
DROP TABLE IF EXISTS `project`;
DROP TABLE IF EXISTS `project_attachment`;
DROP TABLE IF EXISTS `reg_doc_type`;
DROP TABLE IF EXISTS `shareholder`;
DROP TABLE IF EXISTS `tax_payment`;
DROP TABLE IF EXISTS `valuta`;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `token`;
DROP TABLE IF EXISTS `social_account`;
DROP TABLE IF EXISTS `profile`;
DROP TABLE IF EXISTS `language_translate`;
DROP TABLE IF EXISTS `language_source`;
DROP TABLE IF EXISTS `language`;
DROP TABLE IF EXISTS `auth_rule`;
DROP TABLE IF EXISTS `auth_item_child`;
DROP TABLE IF EXISTS `auth_item`;
DROP TABLE IF EXISTS `auth_assignment`;

--
-- Очистить таблицу `user`
--

DELETE FROM `user`;

ALTER TABLE `user` CHANGE `password_hash` `password_hash` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `user` DROP COLUMN `superuser`;
ALTER TABLE `user` DROP COLUMN `confirmed_at`;
ALTER TABLE `user` DROP COLUMN `unconfirmed_email`;
ALTER TABLE `user` DROP COLUMN `blocked_at`;
ALTER TABLE `user` DROP COLUMN `registration_ip`;
ALTER TABLE `user` DROP COLUMN `last_login_at`;
ALTER TABLE `user` auto_increment = 1;

DELETE FROM `migration` WHERE `version`='m170822_101012_init_db';

-- --------------------------------------------------------
";

        $this->execute($sql);
        
        return true;
    }
    
}
