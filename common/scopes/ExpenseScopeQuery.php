<?php

namespace common\scopes;

use Yii;
use yii\db\ActiveQuery;

use common\components\FSMAccessHelper;
use common\models\user\FSMUser;

class ExpenseScopeQuery extends AbonentFieldScopeQuery
{
    public function init()
    {
        parent::init();
    }

    public function forClient()
    {
        $user = Yii::$app->user->identity;
        if(!isset($user)){
            return $this;
        }
        
        $user_client_id = Yii::$app->session->get('user_client_id');
        if(empty($user_client_id)){
            return $this;
        }
        $userClientId = is_array($user_client_id) ? $user_client_id : [$user_client_id];
        
        if($user->hasRole([
                FSMUser::USER_ROLE_AGENT, 
                FSMUser::USER_ROLE_CONTRAGENT, 
                FSMUser::USER_ROLE_CLIENT,
                FSMUser::USER_ROLE_LIMIT_USER,
            ])
        ){
            $this->andWhere(['or', 
                ['expense.first_client_id' => $userClientId], 
                ['expense.second_client_id' => $userClientId],
            ]);                
            $this->andWhere(['expense.report_plus' => 0]);            
        }
        
        if($user->hasRole([
                FSMUser::USER_ROLE_SUPPLIER, 
            ])
        ){
            $this->andWhere(['expense.second_client_id' => $userClientId]);                
            $this->andWhere(['expense.report_plus' => 0]);            
        }        
        return $this;        
    } 
}
