<?php

namespace common\scopes;

use Yii;
use yii\db\ActiveQuery;

use common\components\FSMAccessHelper;
use common\models\user\FSMUser;

class BillPaymentScopeQuery extends BaseScopeQuery
{
    public function init()
    {
        parent::init();
    }
    
    public function onlyAuthor()
    {
        $user = Yii::$app->user->identity;
        if(isset($user)){
            $modelClass = \yii\helpers\StringHelper::basename($this->modelClass);
            if(Yii::$app->authManager->hasPermission($user->id, "view{$modelClass}Self")){
                $this->leftJoin(['scope_bill' => 'bill'], 'scope_bill.id = '.$this->tableName.'.bill_id');
                $this->andWhere(['scope_bill.create_user_id' => $user->id]);
            }
        }
        return $this;        
    }

}
