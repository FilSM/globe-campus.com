<?php

namespace common\scopes;

use Yii;
use yii\db\ActiveQuery;

use common\components\FSMAccessHelper;
use common\models\user\FSMUser;

class AgreementScopeQuery extends AbonentScopeQuery
{
    public function init()
    {
        parent::init();
    }
    
    public function onlyAuthor()
    {
        $user = Yii::$app->user->identity;
        if(isset($user)){
            $modelClass = \yii\helpers\StringHelper::basename($this->modelClass);
            if(Yii::$app->authManager->hasPermission($user->id, "view{$modelClass}Self")){
                $this->leftJoin(['scope_bill' => 'bill'], 'scope_bill.agreement_id = '.$this->tableName.'.id');
                $this->andWhere(['or', 
                    [$this->tableName.'.create_user_id' => $user->id],
                    ['scope_bill.create_user_id' => $user->id]
                ]);
            }        
        }
        return $this;        
    }

    public function forClient()
    {
        $user = Yii::$app->user->identity;
        if(!isset($user)){
            return $this;
        }
        
        $user_client_id = Yii::$app->session->get('user_client_id');
        if(empty($user_client_id)){
            return $this;
        }
        $userClientId = is_array($user_client_id) ? $user_client_id : [$user_client_id];
        
        if($user->hasRole([
                FSMUser::USER_ROLE_AGENT, 
                FSMUser::USER_ROLE_CONTRAGENT, 
                FSMUser::USER_ROLE_CLIENT,
                FSMUser::USER_ROLE_LIMIT_USER,
            ])
        ){
            $this->andWhere(['or', 
                ['agreement.first_client_id' => $userClientId], 
                ['agreement.second_client_id' => $userClientId],
                ['agreement.third_client_id' => $userClientId],
            ]);
        }
        
        if($user->hasRole([
                FSMUser::USER_ROLE_SUPPLIER, 
            ])
        ){
            $this->andWhere(['or', 
                ['agreement.second_client_id' => $userClientId],
                ['agreement.third_client_id' => $userClientId],
            ]);
        }        
        return $this;        
    } 
}
