<?php

namespace common\scopes;

use Yii;
use yii\db\ActiveQuery;

use common\components\FSMAccessHelper;
use common\models\user\FSMUser;

class BaseScopeQuery extends ActiveQuery
{

    protected $tableName;
    protected $model;
    protected $user;
    protected $profile;

    public function init()
    {
        $modelClass = $this->modelClass;
        $this->tableName = $modelClass::tableName();
        $this->model = new $modelClass();
        parent::init();
    }

    public function find()
    {
        return parent::find();
    }

    public function all($db = null)
    {
        return parent::all($db);
    }

    public function notDeleted()
    {
        $this->andWhere(['!=', $this->tableName . '.deleted', 1]);
        return $this;
    }

    public function onlyAuthor()
    {
        $user = Yii::$app->user->identity;
        if(isset($user)){
            $modelClass = \yii\helpers\StringHelper::basename($this->modelClass);
            if(Yii::$app->authManager->hasPermission($user->id, "view{$modelClass}Self")){
                $this->andWhere([$this->tableName . '.create_user_id' => $user->id]);
            }
        }
        return $this;
    }

}
