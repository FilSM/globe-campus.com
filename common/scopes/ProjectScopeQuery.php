<?php

namespace common\scopes;

use Yii;
use yii\db\ActiveQuery;

use common\models\user\FSMUser;

class ProjectScopeQuery extends AbonentFieldScopeQuery
{
    
    public function onlyAuthor()
    {
        $user = Yii::$app->user->identity;
        $modelClass = \yii\helpers\StringHelper::basename($this->modelClass);
        if(isset($user) && 
            Yii::$app->authManager->hasPermission($user->id, "view{$modelClass}Self")
        ){
            $this->leftJoin(['scope_bill' => 'bill'], 'scope_bill.project_id = '.$this->tableName.'.id');
            $this->andWhere(['or', 
                [$this->tableName.'.create_user_id' => $user->id],
                ['scope_bill.create_user_id' => $user->id]
            ]);
        }
        return $this;        
    }

    public function forClient()
    {
        $user = Yii::$app->user->identity;
        if(!isset($user)){
            return $this;
        }
        
        $user_client_id = Yii::$app->session->get('user_client_id');
        if(empty($user_client_id)){
            return $this;
        }
        $userClientId = is_array($user_client_id) ? $user_client_id : [$user_client_id];
        
        if($user->hasRole([
                FSMUser::USER_ROLE_AGENT, 
                FSMUser::USER_ROLE_CONTRAGENT, 
                FSMUser::USER_ROLE_CLIENT,
                FSMUser::USER_ROLE_LIMIT_USER,
            ])
        ){
            $this->leftJoin(['scope_abonent_agreement_project' => 'abonent_agreement_project'], 'scope_abonent_agreement_project.project_id = project.id');
            $this->leftJoin(['scope_abonent_agreement' => 'abonent_agreement'], 'scope_abonent_agreement.id = scope_abonent_agreement_project.abonent_agreement_id');
            $this->leftJoin(['scope_agreement' => 'agreement'], 'scope_agreement.id = scope_abonent_agreement.agreement_id');
            $this->andWhere(['or', 
                ['scope_agreement.first_client_id' => $userClientId], 
                ['scope_agreement.second_client_id' => $userClientId],
                ['scope_agreement.third_client_id' => $userClientId],
            ]);                
        }
        
        if($user->hasRole([
                FSMUser::USER_ROLE_SUPPLIER, 
            ])
        ){
            $this->leftJoin(['scope_abonent_agreement_project' => 'abonent_agreement_project'], 'scope_abonent_agreement_project.project_id = project.id');
            $this->leftJoin(['scope_abonent_agreement' => 'abonent_agreement'], 'scope_abonent_agreement.id = scope_abonent_agreement_project.abonent_agreement_id');
            $this->leftJoin(['scope_agreement' => 'agreement'], 'scope_agreement.id = scope_abonent_agreement.agreement_id');
            $this->andWhere(['or', 
                ['scope_agreement.second_client_id' => $userClientId],
                ['scope_agreement.third_client_id' => $userClientId],
            ]);                
        }        
        return $this;        
    }    
}
