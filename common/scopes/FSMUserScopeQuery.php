<?php

namespace common\scopes;

use Yii;
use yii\db\ActiveQuery;

use common\components\FSMAccessHelper;
use common\models\user\FSMUser;

class FSMUserScopeQuery extends BaseScopeQuery
{
    static $user_abonent_id;

    public function init() {
        parent::init();

        $userAbonentId = Yii::$app->session->get('user_current_abonent_id');
        if (!empty($userAbonentId)) {
            self::$user_abonent_id = $userAbonentId;
        } else {
            static $user;
            if (!isset($user)) {
                $user = Yii::$app->user->identity;
                self::$user_abonent_id = !empty($user) ? [] : [-1];
            }
        }

        if (!empty(self::$user_abonent_id)) {
            $this->leftJoin(['scope_profile' => 'profile'], 'scope_profile.user_id = '.$this->tableName.'.id');
            $this->leftJoin(['scope_profile_client' => 'profile_client'], 'scope_profile_client.profile_id = scope_profile.id');
            $this->leftJoin(['scope_abonent_client' => 'abonent_client'], 'scope_abonent_client.client_id = scope_profile_client.client_id');
            $this->andWhere(['scope_abonent_client.abonent_id' => self::$user_abonent_id]);
        }
    }
    
    public function forClient()
    {
        return $this;        
    } 

}
