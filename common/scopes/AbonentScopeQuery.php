<?php

namespace common\scopes;

use Yii;
//use yii\db\ActiveQuery;
use common\models\user\FSMProfile;
use common\models\client\Client;

class AbonentScopeQuery extends BaseScopeQuery
{

    static $user_abonent_id;

    public function init() {
        parent::init();

        $userAbonentId = Yii::$app->session->get('user_current_abonent_id');
        if (!empty($userAbonentId)) {
            self::$user_abonent_id = $userAbonentId;
        } else {
            static $user;
            if (!isset($user)) {
                $user = Yii::$app->user->identity;
                self::$user_abonent_id = !empty($user) ? [] : [-1];
            }
        }

        if (!empty(self::$user_abonent_id)) {
            $this->innerJoin(['abonent_'.$this->tableName.'_scope' => 'abonent_'.$this->tableName], 'abonent_'.$this->tableName.'_scope.'.$this->tableName.'_id = '.$this->tableName.'.'.$this->model::$keyField);
            $this->andWhere(['abonent_'.$this->tableName.'_scope.abonent_id' => self::$user_abonent_id]);
        }
    }

}
