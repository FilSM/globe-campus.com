<?php

namespace common\scopes;

use Yii;
use yii\db\ActiveQuery;

use common\components\FSMAccessHelper;
use common\models\user\FSMUser;
use common\models\bill\Bill;

class BillScopeQuery extends AbonentScopeQuery
{
    public function init()
    {
        parent::init();
    }

    public function forClient()
    {
        $user = Yii::$app->user->identity;
        if(!isset($user)){
            return $this;
        }
        
        $user_client_id = Yii::$app->session->get('user_client_id');
        if(empty($user_client_id)){
            return $this;
        }
        $userClientId = is_array($user_client_id) ? $user_client_id : [$user_client_id];
        
        if($user->hasRole([
                FSMUser::USER_ROLE_AGENT, 
                FSMUser::USER_ROLE_CONTRAGENT, 
                FSMUser::USER_ROLE_CLIENT,
                FSMUser::USER_ROLE_LIMIT_USER,
            ])
        ){
            $this->leftJoin(['scope_agreement' => 'agreement'], 'scope_agreement.id = bill.agreement_id');
            $this->andWhere(['or', 
                ['scope_agreement.first_client_id' => $userClientId], 
                ['scope_agreement.second_client_id' => $userClientId],
                ['scope_agreement.third_client_id' => $userClientId],
            ]);
            
            if($user->hasRole([
                FSMUser::USER_ROLE_AGENT,
            ])){
                $this->andWhere(['bill.status' => [
                    Bill::BILL_STATUS_PREP_PAYMENT,
                    Bill::BILL_STATUS_PAYMENT,
                    Bill::BILL_STATUS_PAID,
                    Bill::BILL_STATUS_COMPLETE
                ]]);
            }
            
            if($user->hasRole([
                FSMUser::USER_ROLE_CONTRAGENT,
            ])){
                $this->andWhere(['bill.status' => [
                    Bill::BILL_STATUS_SIGNED,
                    Bill::BILL_STATUS_PREP_PAYMENT,
                    Bill::BILL_STATUS_PAYMENT,
                    Bill::BILL_STATUS_PAID,
                    Bill::BILL_STATUS_COMPLETE
                ]]);
            }
        }
        
        if($user->hasRole([
                FSMUser::USER_ROLE_SUPPLIER, 
            ])
        ){
            $this->leftJoin(['scope_agreement' => 'agreement'], 'scope_agreement.id = bill.agreement_id');            
            $this->andWhere(['or', 
                ['scope_agreement.second_client_id' => $userClientId],
                ['scope_agreement.third_client_id' => $userClientId],
            ]);
        }          
        return $this;        
    } 
}
