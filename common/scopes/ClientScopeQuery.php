<?php

namespace common\scopes;

use Yii;
use yii\db\ActiveQuery;

use common\components\FSMAccessHelper;
use common\models\user\FSMUser;
use common\models\user\FSMProfile;
use common\models\client\Client;

class ClientScopeQuery extends AbonentScopeQuery
{
    static $user_client_id = null;
    
    protected $userAbonentId;

    public function init()
    {
        parent::init();
    }
    
    public function onlyAuthor()
    {
        $user = Yii::$app->user->identity;
        $modelClass = \yii\helpers\StringHelper::basename($this->modelClass);
        if(isset($user) && 
            Yii::$app->authManager->hasPermission($user->id, "view{$modelClass}Self")
        ){
            $this->leftJoin(['scope_agreement' => 'agreement'], 
                '(scope_agreement.first_client_id = '.$this->tableName.'.id)'.
                ' OR '.
                '(scope_agreement.second_client_id = '.$this->tableName.'.id)'.
                ' OR '.
                '(scope_agreement.third_client_id = '.$this->tableName.'.id)'
            );
            $this->leftJoin(['scope_bill' => 'bill'], 'scope_bill.agreement_id = scope_agreement.id');
            $this->andWhere(['or', 
                [$this->tableName.'.create_user_id' => $user->id],
                ['scope_bill.create_user_id' => $user->id]
            ]);
        }
        return $this;        
    }

    public function forClient()
    {
        $user = Yii::$app->user->identity;
        if(isset($user) && 
            $user->hasRole([
                FSMUser::USER_ROLE_AGENT, 
                FSMUser::USER_ROLE_CONTRAGENT, 
                FSMUser::USER_ROLE_CLIENT,
                FSMUser::USER_ROLE_LIMIT_USER,
            ])
        ){
            $user_client_id = Yii::$app->session->get('user_client_id');
            if(!empty($user_client_id)){
                $userClientId = is_array($user_client_id) ? $user_client_id : [$user_client_id];
                $this->andWhere(['client.id' => $userClientId]);
            }
        }
        return $this;        
    }     
}
