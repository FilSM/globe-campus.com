<?php

namespace common\scopes;

use Yii;
use yii\db\ActiveQuery;

use common\components\FSMAccessHelper;
use common\models\user\FSMUser;

class AgreementPaymentScopeQuery extends BaseScopeQuery
{
    public function init()
    {
        parent::init();
    }
    
    public function onlyAuthor()
    {
        $user = Yii::$app->user->identity;
        if(isset($user)){
            $modelClass = \yii\helpers\StringHelper::basename($this->modelClass);
            if(Yii::$app->authManager->hasPermission($user->id, "view{$modelClass}Self")){
                $this->leftJoin(['scope_agreement' => 'agreement'], 'scope_agreement.id = '.$this->tableName.'.agreement_id');
                $this->andWhere(['scope_agreement.create_user_id' => $user->id]);
            }
        }
        return $this;        
    }

}
