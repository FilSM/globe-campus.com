<?php

namespace common\scopes;

use Yii;
use yii\db\ActiveQuery;

use common\components\FSMAccessHelper;
use common\models\user\FSMUser;

class ConventionScopeQuery extends AbonentFieldScopeQuery
{
    public function init()
    {
        parent::init();
    }
    
    public function onlyAuthor()
    {
        $user = Yii::$app->user->identity;
        $modelClass = \yii\helpers\StringHelper::basename($this->modelClass);
        if(isset($user) && 
            Yii::$app->authManager->hasPermission($user->id, "view{$modelClass}Self")
        ){
            $this->andWhere([$this->tableName.'.create_user_id' => $user->id]);
        }
        return $this;        
    }

    public function forClient()
    {
        $user = Yii::$app->user->identity;
        if(!isset($user)){
            return $this;
        }
        
        $user_client_id = Yii::$app->session->get('user_client_id');
        if(empty($user_client_id)){
            return $this;
        }
        $userClientId = is_array($user_client_id) ? $user_client_id : [$user_client_id];
        
        if($user->hasRole([
                FSMUser::USER_ROLE_AGENT, 
                FSMUser::USER_ROLE_CONTRAGENT, 
                FSMUser::USER_ROLE_CLIENT,
                FSMUser::USER_ROLE_LIMIT_USER,
            ])
        ){
            $this->andWhere(['or', 
                [$this->tableName.'.first_client_id' => $userClientId], 
                [$this->tableName.'.second_client_id' => $userClientId],
                [$this->tableName.'.third_client_id' => $userClientId],
            ]);                
        }
        return $this;        
    }     
}
