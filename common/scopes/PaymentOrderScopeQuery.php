<?php

namespace common\scopes;

use Yii;
use yii\db\ActiveQuery;

use common\components\FSMAccessHelper;
use common\models\user\FSMUser;

class PaymentOrderScopeQuery extends AbonentFieldScopeQuery
{
    public function init()
    {
        parent::init();
    }
    
    public function onlyAuthor()
    {
        $user = Yii::$app->user->identity;
        $modelClass = \yii\helpers\StringHelper::basename($this->modelClass);
        if(isset($user) && 
            Yii::$app->authManager->hasPermission($user->id, "view{$modelClass}Self")
        ){
            $this->leftJoin(['scope_bill_payment' => 'bill_payment'], 'scope_bill_payment.payment_order_id = '.$this->tableName.'.id');
            $this->leftJoin(['scope_bill' => 'bill'], 'scope_bill.id = scope_bill_payment.bill_id');
            $this->andWhere(['or', 
                [$this->tableName.'.create_user_id' => $user->id],
                ['scope_bill.create_user_id' => $user->id]
            ]);
        }
        return $this;        
    }

}
