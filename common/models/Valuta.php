<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "valuta".
 *
 * @property integer $id
 * @property string $name
 * @property string $int_0
 * @property string $int_1
 * @property string $int_2
 * @property string $floor_0
 * @property string $floor_1
 * @property string $floor_2
 * @property integer $enabled
 *
 */
class Valuta extends \common\models\mainclass\FSMBaseModel
{
    const VALUTA_DEFAULT = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'valuta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['enabled'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['int_0', 'int_1', 'int_2', 'floor_0', 'floor_1', 'floor_2'], 'string', 'max' => 20],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true)
    {
        return parent::label('common', 'Currency|Currency', $n, $translate);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Name'),
            'int_0' => Yii::t('currency', 'Integer part "0"'), 
            'int_1' => Yii::t('currency', 'Integer part "one"'), 
            'int_2' => Yii::t('currency', 'Integer part "many"'), 
            'floor_0' => Yii::t('currency', 'Decimal part "0"'), 
            'floor_1' => Yii::t('currency', 'Decimal part "one"'), 
            'floor_2' => Yii::t('currency', 'Decimal part "many"'),
            'enabled' => Yii::t('common', 'Enabled'),
        ];
    }

}
