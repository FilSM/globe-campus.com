<?php

namespace common\models\client;

use Yii;
use yii\base\Exception;

use common\models\Action;
use common\models\user\FSMUser;
use common\models\user\FSMProfile;

/**
 * This is the model class for table "agreement_history".
 *
 * @property integer $id
 * @property integer $agreement_id
 * @property integer $action_id
 * @property string $comment
 * @property string $create_time
 * @property integer $create_user_id
 *
 * @property Action $action
 * @property Agreement $agreement
 * @property FSMUser $createUser
 */
class AgreementHistory extends \common\models\mainclass\FSMCreateModel
{
    const AgreementHistory_ACTIONS = [
        'ACTION_CREATE'             => 1,
        'ACTION_EDIT'               => 2,
        'ACTION_DELETE'             => 3,
        'ACTION_PAYMENT_CREDIT'     => 4,
        'ACTION_PAYMENT_DEBET'      => 5,
        'ACTION_CONFIRM_PAYMENT'    => 6,
        'ACTION_EDIT_PAYMENT'       => 7,
        'ACTION_DELETE_PAYMENT'     => 8,
        'ACTION_STATUS_UP_NEW'      => 9,
        'ACTION_STATUS_UP_READY'    => 10,
        'ACTION_STATUS_UP_SIGNED'   => 11,
        'ACTION_STATUS_UP_OVERDUE'  => 12,
        'ACTION_STATUS_UP_ARCHIVED' => 13,
        'ACTION_STATUS_UP_CANCELED' => 14,
        
    ];
    const AgreementHistory_ACTIONS_DOWN = [
        'ACTION_STATUS_DOWN_PREPAR' => 15,
        'ACTION_STATUS_DOWN_NEW'    => 16,
        'ACTION_STATUS_DOWN_READY'  => 17,
        'ACTION_STATUS_DOWN_SIGN'   => 18,
    ];
    
    protected $_externalFields = [
        'agreement_number',
        'user_name',
    ]; 
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agreement_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agreement_id', 'action_id', 'create_user_id'], 'integer'],
            [['comment'], 'string'],
            [['create_time'], 'safe'],
            [['agreement_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agreement::class, 'targetAttribute' => ['agreement_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['create_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('agreement', 'Agreement history|Agreements history', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'agreement_id' => Yii::t('agreement', 'Agreement number'),
            'action_id' => Yii::t('common', 'Action'),
            'comment' => Yii::t('common', 'Comment'),
            'create_time' => Yii::t('common', 'Action time'),
            'create_user_id' => Yii::t('common', 'Performer'),
            
            'agreement_number' => Yii::t('agreement', 'Agreement number'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreement()
    {
        return $this->hasOne(Agreement::class, ['id' => 'agreement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'create_user_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUserProfile()
    {
        return $this->hasOne(FSMProfile::class, ['user_id' => 'create_user_id']);
    }    
    
    static public function getAgreementActionList() {
        return [
            AgreementHistory::AgreementHistory_ACTIONS['ACTION_CREATE'] => Yii::t('action', 'Creation'),
            AgreementHistory::AgreementHistory_ACTIONS['ACTION_EDIT'] => Yii::t('action', 'Editing'),
            AgreementHistory::AgreementHistory_ACTIONS['ACTION_DELETE'] => Yii::t('action', 'Deleting'),
            AgreementHistory::AgreementHistory_ACTIONS['ACTION_PAYMENT_CREDIT'] => Yii::t('action', 'Credit'),
            AgreementHistory::AgreementHistory_ACTIONS['ACTION_PAYMENT_DEBET'] => Yii::t('action', 'Debet'),
            AgreementHistory::AgreementHistory_ACTIONS['ACTION_CONFIRM_PAYMENT'] => Yii::t('action', 'Confirm payment'),
            AgreementHistory::AgreementHistory_ACTIONS['ACTION_EDIT_PAYMENT'] => Yii::t('action', 'Payment editing'),
            AgreementHistory::AgreementHistory_ACTIONS['ACTION_DELETE_PAYMENT'] => Yii::t('action', 'Payment deleting'),

            AgreementHistory::AgreementHistory_ACTIONS['ACTION_STATUS_UP_NEW'] => Yii::t('action', 'Change status to "New"'),
            AgreementHistory::AgreementHistory_ACTIONS['ACTION_STATUS_UP_READY'] => Yii::t('action', 'Change status to "Ready"'),
            AgreementHistory::AgreementHistory_ACTIONS['ACTION_STATUS_UP_SIGNED'] => Yii::t('action', 'Change status to "Signed"'),
            AgreementHistory::AgreementHistory_ACTIONS['ACTION_STATUS_UP_OVERDUE'] => Yii::t('action', 'Change status to "Overdue"'),
            AgreementHistory::AgreementHistory_ACTIONS['ACTION_STATUS_UP_ARCHIVED'] => Yii::t('action', 'Change status to "Archived"'),
            AgreementHistory::AgreementHistory_ACTIONS['ACTION_STATUS_UP_CANCELED'] => Yii::t('action', 'Change status to "Canceled"'),
            AgreementHistory::AgreementHistory_ACTIONS_DOWN['ACTION_STATUS_DOWN_PREPAR'] => Yii::t('action', 'Rollback status to "Preparing"'),
            AgreementHistory::AgreementHistory_ACTIONS_DOWN['ACTION_STATUS_DOWN_NEW'] => Yii::t('action', 'Rollback status to "New"'),
            AgreementHistory::AgreementHistory_ACTIONS_DOWN['ACTION_STATUS_DOWN_READY'] => Yii::t('action', 'Rollback status to "Ready"'),
            AgreementHistory::AgreementHistory_ACTIONS_DOWN['ACTION_STATUS_DOWN_SIGN'] => Yii::t('action', 'Rollback status to "Signed"'),
        ];
    }    
    
    public function saveHistory($agreement_id, $action_id, $comment = null)
    {
        $this->agreement_id = !empty($agreement_id) ? $agreement_id : null;
        $this->action_id = $action_id;
        $this->comment = !empty($comment) ? $comment : $this->comment;
        $this->create_time = date('d-m-Y H:i:s');
        $this->create_user_id = Yii::$app->user->id;
        
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            if (!$this->save()) {
                throw new Exception('Unable to save data! '.$model->errorMessage);
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $transaction->rollBack();
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
            return false;
        }
        return true;
    }
}