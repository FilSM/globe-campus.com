<?php

namespace common\models\client\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;

use common\models\client\AgreementPayment;

/**
 * AgreementPaymentSearch represents the model behind the search form of `common\models\client\AgreementPayment`.
 */
class AgreementPaymentSearch extends AgreementPayment
{
    public $agreement_number;
    public $from_bank_name;
    public $to_bank_name;
    public $paid_date_range;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'agreement_history_id', 'from_bank_id', 'to_bank_id', 'agreement_id', 
                'valuta_id', 'manual_input'], 'integer'],
            [['summa', 'rate', 'summa_eur'], 'number'],
            [['agreement_number', 'from_bank_name', 'to_bank_name', 'paid_date', 
                'confirmed', 'direction', 'paid_date_range'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function getIdsByClient($params, $clientIds = [])
    {
        $start_date = !empty($_GET['from']) ? date('Y-m-d', strtotime($_GET['from'])) : date('Y-01-01');
        $end_date = isset($_GET['till']) ? (!empty($_GET['till']) ? date("Y-m-d", strtotime($_GET['till'])) : date("Y-m-t")) : date("Y-m-t");
        
        $clientField = ($params['direction'] == 'in' ? 'first_client_id' : 'second_client_id');
        
        $query = (new Query());
        $query
            ->select([
                'id' => 'ap.id',
            ])  
            ->from(['ap' => 'agreement_payment'])
                
            ->leftJoin(['a' => 'agreement'], 'a.id = ap.agreement_id')
            ->leftJoin(['c' => 'client'], 'c.id = a.'.$clientField)
            ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = c.id AND ac.abonent_id = '. $this->userAbonentId)
                
            ->andWhere('ap.summa_eur != 0')
            ->andWhere(['not', ['ac.client_group_id' => null]])
            ->andWhere(['between', 'ap.paid_date', $start_date, $end_date.' 23:59:59'])
            ->andWhere(['ap.direction' => ($params['direction'] == 'in' ? 'debet' : 'credit')]);
        
        if(!empty($clientIds)){
            $query->andWhere(['in', 'c.id', $clientIds]);
        }
        
        if(!empty($params['client_id'])){
            $query->andWhere(['a.'.$clientField => $params['client_id']]);
        }

        /*
        SELECT 
            `ap`.`id` AS `id`
        FROM
            `agreement_payment` `ap`
            LEFT JOIN `agreement` `a` ON a.id = ap.agreement_id
            LEFT JOIN `client` `c` ON c.id = a.first_client_id
            LEFT JOIN `abonent_client` `ac` ON ac.client_id = c.id AND ac.abonent_id = 1
        WHERE
            (ap.summa_eur != 0)
            AND (NOT (`ac`.`client_group_id` IS NULL))
            AND (`ap`.`paid_date` BETWEEN '2019-01-01' AND '2019-04-30 23:59:59')
            AND (`ap`.`direction` = 'debet')
            AND (`a`.`first_client_id` = '18')
         * 
         */
        
        $paymentList = $query->all();
        $ids = [];
        foreach ($paymentList as $payment) {
            $ids[] = $payment['id'];
        }
        return $ids;        
    }
    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
    
        $query = AgreementPayment::find(true);
        $query->select = [
            $baseTableName . '.*',
            'agreement_number' => 'agreement.number',
            'from_bank_name' => 'from_bank.name',
            'to_bank_name' => 'to_bank.name',
        ];
        $query->leftJoin(['agreement' => 'agreement'], 'agreement.id = '.$baseTableName.'.agreement_id');
        $query->leftJoin(['from_bank' => 'bank'], 'from_bank.id = '.$baseTableName.'.from_bank_id');
        $query->leftJoin(['to_bank' => 'bank'], 'to_bank.id = '.$baseTableName.'.to_bank_id');
        $query->leftJoin(['aa' => 'abonent_agreement'], 'aa.agreement_id = agreement.id');
        $query->andWhere(['aa.abonent_id' => $this->userAbonentId]);
        
        if(!isset($params['sort'])){
            $query->addOrderBy('id desc');
        }
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $idsArr = isset($params['agr_ids']) ? $params['agr_ids'] : null;
        unset(
            $params['agr_ids'] 
        );
        
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if(isset($idsArr)){
            $query->andWhere([$baseTableName.'.id' => $idsArr]);
        }
        
        if(!empty($this->paid_date) && strpos($this->paid_date, '-') !== false) { 
            $this->paid_date_range = $this->paid_date;
            list($start_date, $end_date) = explode(' - ', $this->paid_date_range);
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));
            $query->andFilterWhere(['between', $baseTableName . '.paid_date', $start_date, $end_date.' 23:59:59']);
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            $baseTableName.'.id' => $this->id,
            $baseTableName.'.agreement_history_id' => $this->agreement_history_id,
            $baseTableName.'.from_bank_id' => $this->from_bank_id,
            $baseTableName.'.to_bank_id' => $this->to_bank_id,
            $baseTableName.'.agreement_id' => $this->agreement_id,
            $baseTableName.'.summa' => $this->summa,
            $baseTableName.'.valuta_id' => $this->valuta_id,
            $baseTableName.'.rate' => $this->rate,
            $baseTableName.'.summa_eur' => $this->summa_eur,
            $baseTableName.'.paid_date' => !empty($this->paid_date) && empty($this->paid_date_range) ? date('Y-m-d', strtotime($this->paid_date)) : null,
            $baseTableName.'.confirmed' => !empty($this->confirmed) ? date('Y-m-d', strtotime($this->confirmed)) : null,
            $baseTableName.'.manual_input' => $this->manual_input,
        ]);

        $query->andFilterWhere(['like', 'direction', $this->direction])
            ->andFilterWhere(['like', 'agreement.number', $this->agreement_number]);
        
/*
        SELECT 
            `agreement_payment`.*,
            `agreement`.`number` AS `agreement_number`,
            `from_bank`.`name` AS `from_bank_name`,
            `to_bank`.`name` AS `to_bank_name`
        FROM
            `agreement_payment`
                LEFT JOIN
            `agreement` `agreement` ON agreement.id = agreement_payment.agreement_id
                LEFT JOIN
            `bank` `from_bank` ON from_bank.id = agreement_payment.from_bank_id
                LEFT JOIN
            `bank` `to_bank` ON to_bank.id = agreement_payment.to_bank_id
        ORDER BY `id` DESC
 * 
 */        
        //return $query->all();                
        return $dataProvider;
    }
}