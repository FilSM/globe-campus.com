<?php

namespace common\models\client\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\client\ClientHistory;

/**
 * ClientHistorySearch represents the model behind the search form of `common\models\client\ClientHistory`.
 */
class ClientHistorySearch extends ClientHistory
{
    public $user_name;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'abonent_id', 'client_id', 'create_user_id'], 'integer'],
            [['history_date', 'history_text', 'create_time', 'user_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
    
        $query = ClientHistory::find(true);

        $query->select = [
            $baseTableName . '.*',
            'user_name' => 'profile.name',
        ];
        
        $query->leftJoin(['profile' => 'profile'], 'profile.user_id = '.$baseTableName.'.create_user_id');

        if(!isset($params['sort'])){
            $query->addOrderBy('id desc');
        }
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if($this->hasAttribute('version')){
            $this->__unset('version');
        }

        // grid filtering conditions
        $query->andFilterWhere([
            $baseTableName.'.id' => $this->id,
            $baseTableName.'.abonent_id' => $this->abonent_id,
            $baseTableName.'.client_id' => $this->client_id,
            $baseTableName.'.history_date' => $this->history_date,
            $baseTableName.'.create_time' => $this->create_time,
            $baseTableName.'.create_user_id' => $this->create_user_id,
        ]);

        $query->andFilterWhere(['like', $baseTableName.'.history_text', $this->history_text]);

        return $dataProvider;
    }
}