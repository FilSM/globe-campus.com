<?php

namespace common\models\client\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

use common\models\client\Client;
use common\models\user\FSMUser;

/**
 * ClientSearch represents the model behind the search form about `common\models\Client`.
 */
class ClientSearch extends Client {

    public $create_time_range;
    public $parent_name;
    public $manager_name;
    public $manager_user_id;
    public $agent_name;
    public $agent_user_id;
    public $country_name;

    public $parent_id;
    public $it_is;
    public $client_group_id;
    public $manager_id;
    public $agent_id;
    public $language_id;
    public $status;
    public $debit;
    public $debit_valuta_id;
    public $gen_number;
    public $comment;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'parent_id', 'client_group_id', 'legal_country_id', 'office_country_id', 
                'manager_id', 'agent_id', 'language_id', 'debit_valuta_id',
                'vat_payer', 'deleted'/*, 'create_user_id', 'update_user_id'*/], 'integer'],
            [['it_is', 'client_type', 'status', 'name', 'reg_number', 'vat_number', 'gen_number', 
                'parent_name', 'manager_name', 'manager_user_id', 'agent_name', 'agent_user_id', 
                'create_time_range', 'country_name', 'country_id', 'debit', 'comment', 
                /*'create_time', 'update_time'*/ ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
        
        $query = Client::find(true);

        $query->select = [
            $baseTableName . '.*',
            'ac.parent_id',
            'ac.it_is',
            'ac.client_group_id',
            'ac.manager_id',
            'ac.agent_id',
            'ac.language_id',
            'ac.status',
            'ac.debit',
            'ac.debit_valuta_id',
            'ac.gen_number',
            'ac.comment',            
            
            'parent_name' => 'parent.name',
            'manager_name' => 'manager.name',
            'manager_user_id' => 'manager.user_id',
            'agent_name' => 'agent.name',
            'agent_user_id' => 'agent.user_id',
            'country_name' => 'country.name',
        ];
        
        $query->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = '.$baseTableName.'.id AND ac.abonent_id = '. $this->userAbonentId);
        $query->leftJoin(['parent' => 'client'], 'parent.id = ac.parent_id');
        $query->leftJoin(['manager' => 'profile'], 'manager.id = ac.manager_id');
        $query->leftJoin(['agent' => 'profile'], 'agent.id = ac.agent_id');
        $query->leftJoin(['country' => 'location_country'], 'country.id = '.$baseTableName.'.legal_country_id');
        
        if(!isset($params['sort'])){
            $query->addOrderBy('id desc');
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            /*
            'pagination' => [
                'pageSize' => 50,
            ],
             * 
             */
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $withoutTypes = [];
        if(!FSMUser::getIsPortalAdmin()){
            //$withoutTypes[] = Client::CLIENT_IT_IS_OWNER;
        }
        $itIsList = $this->clientItIsList;
        foreach ($withoutTypes as $type) {
            unset($itIsList[$type]);
        }
        
        if(!empty($this->create_time) && strpos($this->create_time, '-') !== false) { 
            $this->create_time_range = $this->create_time;
            list($start_date, $end_date) = explode(' - ', $this->create_time_range); 
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));
            $query->andFilterWhere(['between', $baseTableName . '.create_time', $start_date, $end_date.' 23:59:59']);
        }
        
        $query->andFilterWhere([
            $baseTableName . '.id' => $this->id,
            $baseTableName . '.legal_country_id' => $this->legal_country_id,
            $baseTableName . '.office_country_id' => $this->office_country_id,
            $baseTableName . '.deleted' => $this->deleted,
            $baseTableName . '.vat_payer' => $this->vat_payer,
            
            'ac.parent_id' => $this->parent_id,
            'ac.manager_id' => $this->manager_id,
            'ac.agent_id' => $this->agent_id,
            'ac.language_id' => $this->language_id,
            'ac.it_is' => !empty($this->it_is) ? $this->it_is : array_keys($itIsList),
            $baseTableName . '.create_time' => !empty($this->create_time) && empty($this->create_time_range) ? date('Y-m-d', strtotime($this->create_time)) : null,
        ]);

        $query
            ->andFilterWhere(['like', $baseTableName . '.client_type', $this->client_type])
            ->andFilterWhere(['like', $baseTableName . '.name', $this->name])
            ->andFilterWhere(['like', $baseTableName . '.reg_number', $this->reg_number])
            ->andFilterWhere(['like', 'ac.gen_number', $this->gen_number])
            ->andFilterWhere(['like', 'ac.status', $this->status])
            ->andFilterWhere(['like', 'parent.name', $this->parent_name])
            ->andFilterWhere(['like', 'manager.name', $this->manager_name])
            ->andFilterWhere(['like', 'agent.name', $this->agent_name])
        ;
        
        if(isset($params['our_clients'])){
            if($params['our_clients']){
                $query->andWhere(['not', ['ac.client_group_id' => null]]);
            }else{
                $query->andWhere(['is', 'ac.client_group_id', null]);
            }
        }
        
        /*
        SELECT 
            `client`.*,
            `ac`.`parent_id`,
            `ac`.`it_is`,
            `ac`.`client_group_id`,
            `ac`.`manager_id`,
            `ac`.`agent_id`,
            `ac`.`language_id`,
            `ac`.`status`,
            `ac`.`debit`,
            `ac`.`debit_valuta_id`,
            `ac`.`gen_number`,
            `ac`.`comment`,
            `parent`.`name` AS `parent_name`,
            `manager`.`name` AS `manager_name`,
            `manager`.`user_id` AS `manager_user_id`,
            `agent`.`name` AS `agent_name`,
            `agent`.`user_id` AS `agent_user_id`,
            `country`.`name` AS `country_name`
        FROM
            `client`
                INNER JOIN
            `abonent_client` `abonent_client_scope` ON abonent_client_scope.client_id = client.id
                LEFT JOIN
            `abonent_client` `ac` ON ac.client_id = client.id AND ac.abonent_id = 1
                LEFT JOIN
            `client` `parent` ON parent.id = ac.parent_id
                LEFT JOIN
            `profile` `manager` ON manager.id = ac.manager_id
                LEFT JOIN
            `profile` `agent` ON agent.id = ac.agent_id
                LEFT JOIN
            `location_country` `country` ON country.id = client.legal_country_id
        WHERE
            (`abonent_client_scope`.`abonent_id` = 1)
                AND ((`client`.`deleted` = 0)
                AND (`ac`.`it_is` = 'client'))
                AND (`ac`.`status` LIKE '%active%')
        ORDER BY `name`
         * 
         */

        //return $query->all();
        return $dataProvider;
    }

}
