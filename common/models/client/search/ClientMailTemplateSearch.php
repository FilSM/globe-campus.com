<?php

namespace common\models\client\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\client\ClientMailTemplate;

/**
 * ClientMailTemplateSearch represents the model behind the search form of `common\models\client\ClientMailTemplate`.
 */
class ClientMailTemplateSearch extends ClientMailTemplate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'abonent_id', 'client_id', 'uploaded_file_id', 'create_user_id', 'update_user_id'], 'integer'],
            [['cc', 'subject', 'header', 'body', 'footer', 'create_time', 'update_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
    
        $query = ClientMailTemplate::find(true);

        if(!isset($params['sort'])){
            $query->addOrderBy('id desc');
        }
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            $baseTableName.'.id' => $this->id,
            $baseTableName.'.abonent_id' => $this->abonent_id,
            $baseTableName.'.client_id' => $this->client_id,
            $baseTableName.'.uploaded_file_id' => $this->uploaded_file_id,
            $baseTableName.'.create_time' => $this->create_time,
            $baseTableName.'.create_user_id' => $this->create_user_id,
            $baseTableName.'.update_time' => $this->update_time,
            $baseTableName.'.update_user_id' => $this->update_user_id,
        ]);

        $query->andFilterWhere(['like', $baseTableName.'.cc', $this->cc])
            ->andFilterWhere(['like', $baseTableName.'.subject', $this->subject])
            ->andFilterWhere(['like', $baseTableName.'.header', $this->header])
            ->andFilterWhere(['like', $baseTableName.'.body', $this->body])
            ->andFilterWhere(['like', $baseTableName.'.footer', $this->footer]);

        return $dataProvider;
    }
}