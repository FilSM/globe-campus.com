<?php

namespace common\models\client\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\client\Agreement;

/**
 * AgreementSearch represents the model behind the search form of `common\models\client\Agreement`.
 */
class AgreementSearch extends Agreement
{
    public $project_id;
    public $project_name;
    public $create_time_range;
    public $first_client_name;
    public $second_client_name;
    public $third_client_name;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'deleted', 'first_client_id', 'second_client_id', 
                'third_client_id', 'create_user_id', 'update_user_id'], 'integer'],
            [['number', 'signing_date', 'due_date', 'status', 'transfer_status', 'agreement_type', 
                'conclusion', 'comment', 'create_time', 'update_time', 
                'first_client_name', 'second_client_name', 'third_client_name', 
                'project_id', 'project_name', 'create_time_range'], 'safe'],
            [['summa', 'rate', 'summa_eur', 'rate_rate'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
    
        $query = Agreement::find(true);

        $query->select = [
            $baseTableName . '.*',
            'first_client_name' => 'first_client.name',
            'second_client_name' => 'second_client.name',
            'third_client_name' => 'third_client.name',
        ];
        
        $query->leftJoin(['first_client' => 'client'], 'first_client.id = '.$baseTableName.'.first_client_id');
        $query->leftJoin(['second_client' => 'client'], 'second_client.id = '.$baseTableName.'.second_client_id');
        $query->leftJoin(['third_client' => 'client'], 'third_client.id = '.$baseTableName.'.third_client_id');
        $query->innerJoin(['aa' => 'abonent_agreement'], 'aa.agreement_id = '.$baseTableName.'.id and aa.abonent_id = '.$this->userAbonentId);
        $query->innerJoin(['aap' => 'abonent_agreement_project'], 'aap.abonent_agreement_id = aa.id');
        $query->innerJoin(['project' => 'project'], 'project.id = aap.project_id');
        
        if(!isset($params['sort'])){
            $query->addOrderBy('id desc');
        }
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => isset($_POST['expandRowKey']) ? 0 : 50,
            ],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            $baseTableName.'.id' => $this->id,
            $baseTableName.'.deleted' => $this->deleted,
            $baseTableName.'.parent_id' => $this->parent_id,
            'aap.project_id' => $this->project_id,
            $baseTableName.'.first_client_id' => $this->first_client_id,
            $baseTableName.'.second_client_id' => $this->second_client_id,
            $baseTableName.'.third_client_id' => $this->third_client_id,
            $baseTableName.'.signing_date' => $this->signing_date,
            $baseTableName.'.due_date' => $this->due_date,
            $baseTableName.'.summa' => $this->summa,
            $baseTableName.'.summa_eur' => $this->summa_eur,
            $baseTableName.'.rate' => $this->rate,
            $baseTableName.'.rate_rate' => $this->rate_rate,
            $baseTableName.'.transfer_status' => $this->transfer_status,
            $baseTableName.'.create_time' => $this->create_time,
            $baseTableName.'.create_user_id' => $this->create_user_id,
            $baseTableName.'.update_time' => $this->update_time,
            $baseTableName.'.update_user_id' => $this->update_user_id,
        ]);

        $query->andFilterWhere(['like', $baseTableName.'.number', $this->number])
            ->andFilterWhere(['like', $baseTableName.'.comment', $this->comment])
            ->andFilterWhere(['like', $baseTableName . '.agreement_type', $this->agreement_type])
            ->andFilterWhere(['like', $baseTableName . '.status', $this->status])
            ->andFilterWhere(['like', $baseTableName . '.conclusion', $this->conclusion])
            ->andFilterWhere(['like', 'first_client.name', $this->first_client_name])
            ->andFilterWhere(['like', 'second_client.name', $this->second_client_name])
            ->andFilterWhere(['like', 'third_client.name', $this->third_client_name])
            ->andFilterWhere(['like', 'project.name', $this->project_name]);
        
        if(!empty($params['client_id'])){
            $query->andWhere(['or', 
                [$baseTableName.'.first_client_id' => $params['client_id']],
                [$baseTableName.'.second_client_id' => $params['client_id']],
                [$baseTableName.'.third_client_id' => $params['client_id']],
            ]);            
        }
        
        $query->groupBy($baseTableName.'.id'); 
        
        /*
        SELECT 
            `agreement`.*,
            `first_client`.`name` AS `first_client_name`,
            `second_client`.`name` AS `second_client_name`,
            `third_client`.`name` AS `third_client_name`
        FROM
            `agreement`
            INNER JOIN `abonent_agreement` `abonent_agreement_scope` ON abonent_agreement_scope.agreement_id = agreement.id
            LEFT JOIN `client` `first_client` ON first_client.id = agreement.first_client_id
            LEFT JOIN `client` `second_client` ON second_client.id = agreement.second_client_id
            LEFT JOIN `client` `third_client` ON third_client.id = agreement.third_client_id
            INNER JOIN `abonent_agreement` `aa` ON aa.agreement_id = agreement.id AND aa.abonent_id = 1
            INNER JOIN `abonent_agreement_project` `aap` ON aap.abonent_agreement_id = aa.id
            INNER JOIN `project` `project` ON project.id = aap.project_id
        WHERE
            (`abonent_agreement_scope`.`abonent_id` = 1)
            AND (`agreement`.`deleted` = 0)
        GROUP BY `agreement`.`id`
         * 
         */
        
        //return $query->all();
        return $dataProvider;
    }
    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchLast($params)
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
    
        $query = Agreement::find(true);

        $query->select = [
            $baseTableName . '.*',
            'first_client_name' => 'first_client.name',
            'second_client_name' => 'second_client.name',
            'third_client_name' => 'third_client.name',
        ];
        
        $query->leftJoin(['first_client' => 'client'], 'first_client.id = '.$baseTableName.'.first_client_id');
        $query->leftJoin(['second_client' => 'client'], 'second_client.id = '.$baseTableName.'.second_client_id');
        $query->leftJoin(['third_client' => 'client'], 'third_client.id = '.$baseTableName.'.third_client_id');
        $query->innerJoin(['aa' => 'abonent_agreement'], 'aa.agreement_id = '.$baseTableName.'.id and aa.abonent_id = '.$this->userAbonentId);
        $query->innerJoin(['aap' => 'abonent_agreement_project'], 'aap.abonent_agreement_id = aa.id');
        $query->innerJoin(['project' => 'project'], 'project.id = aap.project_id');
        $query->groupBy($baseTableName.'.id');
        
        if(!isset($params['sort'])){
            //$query->addOrderBy('id desc');
            $query->addOrderBy('signing_date desc, number');
        }
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
                'pageParam' => 'agr-page',
            ],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->andFilterWhere(['>=', $baseTableName . '.create_time', $params['start_date']]);
        
        /*
        SELECT 
            `agreement`.*,
            `first_client`.`name` AS `first_client_name`,
            `second_client`.`name` AS `second_client_name`,
            `third_client`.`name` AS `third_client_name`
        FROM
            `agreement`
            INNER JOIN `abonent_agreement` `abonent_agreement_scope` ON abonent_agreement_scope.agreement_id = agreement.id
            LEFT JOIN `client` `first_client` ON first_client.id = agreement.first_client_id
            LEFT JOIN `client` `second_client` ON second_client.id = agreement.second_client_id
            LEFT JOIN `client` `third_client` ON third_client.id = agreement.third_client_id
            INNER JOIN `abonent_agreement` `aa` ON aa.agreement_id = agreement.id AND aa.abonent_id = 1
            INNER JOIN `abonent_agreement_project` `aap` ON aap.abonent_agreement_id = aa.id
            INNER JOIN `project` `project` ON project.id = aap.project_id
        WHERE
            (`abonent_agreement_scope`.`abonent_id` = 1)
            AND (`agreement`.`deleted` = 0)
        GROUP BY `agreement`.`id`
         * 
         */
        
        //return $query->all();
        return $dataProvider;
    }    
    
    public function searchOverdue()
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
        $query = $this->find();
        $query->andWhere([
            $baseTableName.'.deleted' => 0,
        ]);        
        $query->andWhere(['not', [$baseTableName.'.due_date' => null]])
            ->andWhere(['<', $baseTableName.'.due_date', date('Y-m-d')])
            ->andWhere(['in', $baseTableName.'.status', [
                Agreement::AGREEMENT_STATUS_NEW, 
                Agreement::AGREEMENT_STATUS_POTENCIAL,
                Agreement::AGREEMENT_STATUS_SIGNED,
            ]
        ]);
        
        return $query->all();
    }    
}