<?php

namespace common\models\client;

use Yii;

use common\models\client\Agreement;
use common\models\Files;

/**
 * This is the model class for table "agreement_attachment".
 *
 * @property integer $id
 * @property integer $agreement_id
 * @property integer $uploaded_file_id
 *
 * @property Agreement $agreement
 * @property Files $uploadedFile
 */
class AgreementAttachment extends \common\models\mainclass\FSMBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agreement_attachment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agreement_id', 'uploaded_file_id'], 'required'],
            [['agreement_id', 'uploaded_file_id'], 'integer'],
            [['agreement_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agreement::class, 'targetAttribute' => ['agreement_id' => 'id']],
            [['uploaded_file_id'], 'exist', 'skipOnError' => true, 'targetClass' => Files::class, 'targetAttribute' => ['uploaded_file_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('client', 'Agreement attachment|Agreement attachments', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'agreement_id' => Yii::t('agreement', 'Agreement'),
            'uploaded_file_id' => Yii::t('common', 'Attachment'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreement()
    {
        return $this->hasOne(Agreement::class, ['id' => 'agreement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadedFile()
    {
        return $this->hasOne(Files::class, ['id' => 'uploaded_file_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function beforeDelete()
    {
        $file = $this->uploadedFile;
        if($file->delete()){
            return parent::beforeDelete();
        }
        return false;
    }    
}