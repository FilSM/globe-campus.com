<?php

namespace common\models\client;

use Yii;

use common\models\abonent\Abonent;
use common\models\Bank;
use common\models\FileXML;
use common\models\FilePDF;

/**
 * This is the model class for table "client_bank".
 *
 * @property integer $id
 * @property integer $deleted
 * @property integer $primary
 * @property integer $abonent_id
 * @property integer $client_id
 * @property integer $bank_id
 * @property string $account
 * @property string $name
 * @property string $services_period_till
 * @property integer $uploaded_file_id
 * @property integer $uploaded_pdf_file_id
 * @property string $balance
 * @property string $currency
 *
 * @property Abonent $abonent
 * @property Client $client
 * @property Bank $bank
 * @property ClientBankBalance $balances
 * @property FileXML $uploadedFile
 * @property FilePDF $uploadedPdfFile
 */
class ClientBank extends \common\models\mainclass\FSMBaseModel
{

    protected $_externalFields = [
        'client_name',
        'bank_name',
        'swift',
        'home_page',
        'file_name_xml',
        'file_name_pdf',
        'valuta_id',
        'is_active',
    ];

    public static function find($withScope = false) {
        if ((Yii::$app->id != 'app-console') && $withScope) {
            $scope = new \common\scopes\ClientBankScopeQuery(get_called_class());
            return $scope->forClient()->notDeleted();             
        }
        return parent::find();
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_bank';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[/* 'abonent_id', */'client_id', 'bank_id', 'account'], 'required'],
            [['abonent_id', 'client_id', 'bank_id', 'deleted', 'primary'], 'integer'],
            [['account'], 'string', 'max' => 34],
            [['name'], 'string', 'max' => 64],
            [['currency'], 'string', 'max' => 3],
            [['balance'], 'number'],
            [['services_period_till'], 'safe'],            
            [['abonent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Abonent::class, 'targetAttribute' => ['abonent_id' => 'id']], 
            [['bank_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bank::class, 'targetAttribute' => ['bank_id' => 'id']],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['client_id' => 'id']],
            //[['uploaded_file_id'], 'exist', 'skipOnError' => true, 'targetClass' => FileXML::class, 'targetAttribute' => ['uploaded_file_id' => 'id']],
            //[['uploaded_pdf_file_id'], 'exist', 'skipOnError' => true, 'targetClass' => FilePDF::class, 'targetAttribute' => ['uploaded_pdf_file_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true)
    {
        return parent::label('client', 'Client bank|Client banks', $n, $translate);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'deleted' => Yii::t('common', 'Deleted'),
            'primary' => Yii::t('client', 'Primary'),
            'abonent_id' => Yii::t('client', 'Abonent'),
            'client_id' => Yii::t('client', 'Client'),
            'bank_id' => Yii::t('bill', 'Bank'),
            'account' => Yii::t('client', 'Account'),
            'name' => Yii::t('common', 'Account name'),
            'services_period_till' => Yii::t('client', 'Service period till'),
            'uploaded_file_id' => Yii::t('client', 'XML file'),
            'uploaded_pdf_file_id' => Yii::t('client', 'PDF file'),
            'balance' => Yii::t('client', 'Balance'),
            'currency' => Yii::t('common', 'Currency'),
            
            'client_name' => Yii::t('client', 'Client'),
            'bank_name' => Yii::t('client', 'Bank name'),
            'swift' => Yii::t('client', 'SWIFT code'),
            'home_page' => Yii::t('bank', 'WWW'),
            'file_name_xml' => Yii::t('bill', 'XML file name'),
            'file_name_pdf' => Yii::t('bill', 'PDF file name'),
            'valuta_id' => Yii::t('common', 'Currency'),
            'is_active' => Yii::t('common', 'Is active'),
        ];
    }

    protected function getIgnoredFieldsForDelete() {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, ['abonent_id', 'client_id', 'bank_id']
        );
        return $fields;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonent() {
        return $this->hasOne(Abonent::class, ['id' => 'abonent_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBank()
    {
        return $this->hasOne(Bank::class, ['id' => 'bank_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::class, ['id' => 'client_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBalances()
    {
        return $this->hasMany(ClientBankBalance::class, ['account_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadedFile()
    {
        return $this->hasOne(FileXML::class, ['id' => 'uploaded_file_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadedPdfFile()
    {
        return $this->hasOne(FilePDF::class, ['id' => 'uploaded_pdf_file_id']);
    }
    
        
    public function beforeSave($insert) 
    {
        if(!parent::beforeSave($insert)){
            return;
        }
        if(!empty($this->services_period_till)){
            $this->services_period_till = date('Y-m-d', strtotime($this->services_period_till));
        }
        if($insert){
            $this->abonent_id = $this->userAbonentId;
        }
        return true;
    }    
}
