<?php

namespace common\models\client;

use Yii;
use yii\helpers\ArrayHelper;

use common\models\Bank;
use common\models\Valuta;
use common\models\client\Agreement;
use common\models\client\AgreementHistory;
use common\models\bill\PaymentConfirm;
use common\models\bill\BillConfirm;
use common\components\FSMHelper;
use common\components\FSMAccessHelper;
/**
 * This is the model class for table "agreement_payment".
 *
 * @property integer $id
 * @property integer $agreement_history_id
 * @property integer $from_bank_id
 * @property integer $to_bank_id
 * @property integer $agreement_id
 * @property string $paid_date
 * @property string $summa
 * @property integer $valuta_id
 * @property string $rate
 * @property string $summa_eur
 * @property string $direction
 * @property string $confirmed
 * @property integer $manual_input
 *
 * @property AgreementHistory $agreementHistory
 * @property Agreement $agreement
 * @property Bank $fromBank
 * @property Bank $toBank
 * @property Valuta $valuta
 * @property PaymentConfirm $paymentConfirms
 * @property BillConfirm $billConfirms
 */
class AgreementPayment extends AgreementHistory
{

    const DIRECTION_CREDIT = 'credit';
    const DIRECTION_DEBET = 'debet';
/*
    protected $_externalFields = [
        'from_bank_name',
        'to_bank_name',
    ]; 
 * 
 */

    public static function find($withScope = false) {
        if ((Yii::$app->id != 'app-console') && $withScope) {
            $scope = new \common\scopes\AgreementPaymentScopeQuery(get_called_class());
            return $scope->onlyAuthor();            
        }
        return parent::find();
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agreement_payment';
    }
    
    public function attributes($withExternal = true) { 
        $parentClass = get_parent_class($this);
        $parent = new $parentClass;
        $attributes = $parent->attributes();
        $attributes = ArrayHelper::merge(
            $attributes,
            [
                'agreement_history_id',
                'from_bank_id',
                'to_bank_id',
                'from_bank_name',
                'to_bank_name',
                'summa',
                'valuta_id',
                'rate', 
                'summa_eur',
                'paid_date',
                'direction',
                'confirmed',
                'manual_input'
            ]
        );
        return $attributes; 
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agreement_history_id', 'agreement_id', 'paid_date', 'summa', 'valuta_id', 'rate', 'direction'], 'required'],
            [['agreement_history_id', 'from_bank_id', 'to_bank_id', 'agreement_id', 'valuta_id', 'manual_input'], 'integer'],
            [['direction'], 'string'],
            [['summa', 'rate', 'summa_eur'], 'number'],
            [['paid_date', 'confirmed'], 'safe'],
            [['rate'], 'validateRate'],
            [['summa'], 'validateSummaDirectionCreate', 'on' => 'create'],
            [['summa'], 'validateSummaDirectionUpdate', 'on' => 'update'],
            [['agreement_history_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgreementHistory::class, 'targetAttribute' => ['agreement_history_id' => 'id']],
            [['agreement_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agreement::class, 'targetAttribute' => ['agreement_id' => 'id']],
            [['from_bank_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bank::class, 'targetAttribute' => ['from_bank_id' => 'id']],
            [['to_bank_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bank::class, 'targetAttribute' => ['to_bank_id' => 'id']],
            [['valuta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Valuta::class, 'targetAttribute' => ['valuta_id' => 'id']],
        ];
    }

    public function validateRate($attribute, $params, $validator)
    {
        $rate = (float) $this->rate;
        $valuta_id = (int) $this->valuta_id;
        if (
                (($rate == 1) && ($valuta_id != Valuta::VALUTA_DEFAULT)) ||
                (($rate != 1) && ($valuta_id == Valuta::VALUTA_DEFAULT))
        ) {
            $message = Yii::t('bill', 'The currency rate is not correct.');
            $validator->addError($this, 'summa', $message);
        }
    }
    
    public function validateSummaDirectionCreate($attribute, $params, $validator)
    {
        $direction = $this->direction;
        switch ($direction) {
            case AgreementPayment::DIRECTION_CREDIT:
                $paymentsSumma = $this->agreement->paymentsSummaCredit;
                break;
            case AgreementPayment::DIRECTION_DEBET:
                $paymentsSumma = $this->agreement->paymentsSummaDebet;
                break;
            default:
                break;
        }
        $availabledSumma = $this->agreement->summa - $paymentsSumma;
        if ($availabledSumma < $this->summa) {
            $message = Yii::t('agreement', 'Sum cannot be more than {value}.');
            $params = ['attribute' => 'summa', 'value' => number_format($availabledSumma, 2)];
            $validator->addError($this, $attribute, $message, $params);
        }
    }

    public function validateSummaDirectionUpdate($attribute, $params, $validator)
    {
        $oldDirection = $this->getOldAttribute('direction');
        $direction = $this->direction;
        switch ($direction) {
            case AgreementPayment::DIRECTION_CREDIT:
                $paymentsSumma = $this->agreement->paymentsSummaCredit;
                break;
            case AgreementPayment::DIRECTION_DEBET:
                $paymentsSumma = $this->agreement->paymentsSummaDebet;
                break;
            default:
                break;
        }
        $paymentsSumma -= ($oldDirection == $direction) ? $this->getOldAttribute('summa') : 0;
        
        $availabledSumma = $this->agreement->summa - $paymentsSumma;
        if ($availabledSumma < $this->summa) {
            $message = Yii::t('agreement', 'Sum cannot be more than {value}.');
            $params = ['attribute' => 'summa', 'value' => number_format($availabledSumma, 2)];
            $validator->addError($this, $attribute, $message, $params);
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('agreement', 'Agreement payment|Agreement payments', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels = ArrayHelper::merge(
            $labels, 
            [
                'id' => Yii::t('common', 'ID'),
                'agreement_history_id' => Yii::t('agreement', 'History action'),
                'from_bank_id' => Yii::t('bill', 'Bank from'),
                'to_bank_id' => Yii::t('bill', 'Bank to'),
                'agreement_id' => Yii::t('agreement', 'Agreement number'),
                'paid_date' => Yii::t('bill', 'Paid'),
                'summa' => Yii::t('common', 'Sum'),
                'valuta_id' => Yii::t('common', 'Currenсy'),
                'rate' => Yii::t('common', 'Rate'),
                'summa_eur' => Yii::t('common', 'Sum').' €',
                'direction' => Yii::t('common', 'Direction'),
                'confirmed' => Yii::t('common', 'Confirmed').'?',
            
                'agreement_number' => Yii::t('agreement', 'Agreement number'),
                'from_bank_name' => Yii::t('bill', 'Bank from'),
                'to_bank_name' => Yii::t('bill', 'Bank to'),
            ]
        );
        return $labels;
    }

    protected function getIgnoredFieldsForDelete() {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, ['from_bank_id', 'to_bank_id', 'agreement_id', 'valuta_id']
        );
        return $fields;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementHistory()
    {
        return $this->hasOne(AgreementHistory::class, ['id' => 'agreement_history_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreement()
    {
        return $this->hasOne(Agreement::class, ['id' => 'agreement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromBank()
    {
        return $this->hasOne(Bank::class, ['id' => 'from_bank_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToBank()
    {
        return $this->hasOne(Bank::class, ['id' => 'to_bank_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValuta()
    {
        return $this->hasOne(Valuta::class, ['id' => 'valuta_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillConfirms()
    {
        return $this->hasMany(BillConfirm::class, ['agr_payment_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentConfirms()
    {
        return $this->hasMany(PaymentConfirm::class, ['id' => 'payment_confirm_id'])->viaTable('bill_confirm', ['agr_payment_id' => 'id']);
    }  
    
    static public function getPaymentDirectionList()
    {
        return [
            AgreementPayment::DIRECTION_CREDIT => Yii::t('common', 'Credit'),
            AgreementPayment::DIRECTION_DEBET => Yii::t('common', 'Debet'),
        ];
    }

    public function canUpdate() {
        $agreementModel = $this->agreement;
        if(empty($agreementModel)){
            return false;
        }        
        if(!in_array($agreementModel->status, [
            Agreement::AGREEMENT_STATUS_NEW, 
            Agreement::AGREEMENT_STATUS_POTENCIAL,
            Agreement::AGREEMENT_STATUS_SIGNED,
        ])){
            return false;
        }
        
        $agreementSumma = $agreementModel->summa;
        $paymentsSummaCredit = $agreementModel->paymentsSummaCredit;
        $paymentsSummaDebet = $agreementModel->paymentsSummaDebet;
        $canCredit = ($agreementSumma > $paymentsSummaCredit);
        $canDebet = ($agreementSumma > $paymentsSummaDebet);
        return ($canCredit || $canDebet);
    }

    public function canDelete() {
        return $this->canUpdate();
    }
    
    static function getButtonConfirm($params)
    {
        extract($params);
        if(!$model->canUpdate()){
            return null;
        }
        if($model->confirmed){
            return null;
        }
        
        $data = [
            'title' => Yii::t('bill', 'Confirm payment'),
            'controller' => 'agreement-payment',
            'action' => 'ajax-confirm-payment',
            'icon' => 'ok',
            'modal' => true,
        ];        
        if (!empty($isBtn)) {
            $result = FSMHelper::vButton($model->id, 
                ArrayHelper::merge($data, [
                    'class' => 'success',
                    'size' => 'btn-xs',
                ])
            );
        } else {
            $result = FSMHelper::vDropdown($model->id, 
                ArrayHelper::merge($data, [
                    'label' => Yii::t('bill', 'Confirm payment'),
                ])
            );
        }
        return $result;        
    }
    
    static function getButtonUpdate($params)
    {
        extract($params);
        $agreementModel = $model->agreement;
        if(
            ($agreementModel->agreement_type != Agreement::AGREEMENT_TYPE_LOAN) ||
            !FSMAccessHelper::can('updateAgreementPayment', $model) ||
            !$model->canUpdate()
        ){
            return;
        }        
        $data = [
            'title' => Yii::t('kvgrid', 'Update'),
            'controller' => 'agreement-payment',
            'action' => 'ajax-update',
            'icon' => 'pencil',
            'modal' => true,
        ];
        if (!empty($isBtn)) {
            $result = FSMHelper::vButton($model->id, 
                ArrayHelper::merge($data, [
                    'class' => 'primary',
                    'size' => 'btn-xs',
                ])
            );
        } else {
            $result = FSMHelper::vDropdown($model->id, 
                ArrayHelper::merge($data, [
                    'label' => Yii::t('kvgrid', 'Update'),
                ])
            );
        }
        return $result;        
    }  
    
    public function beforeSave($insert) 
    {
        $this->rate = ($this->valuta_id == Valuta::VALUTA_DEFAULT) ? 1 : $this->rate;
        $this->summa_eur = $this->summa / $this->rate;
        $this->paid_date = date('Y-m-d', strtotime($this->paid_date));
        if(!parent::beforeSave($insert)){
            return;
        }
        return true;
    }   
}