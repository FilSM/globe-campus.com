<?php

namespace common\models\client;

use Yii;

use common\models\client\Agreement;
use common\models\client\ClientContact;
/**
 * This is the model class for table "agreement_person".
 *
 * @property integer $id
 * @property integer $agreement_id
 * @property integer $client_contact_id
 * @property integer $person_order
 *
 * @property Agreement $agreement
 * @property ClientContact $clientContact
 */
class AgreementPerson extends \common\models\mainclass\FSMBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agreement_person';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agreement_id', 'client_contact_id', 'person_order'], 'required'],
            [['agreement_id', 'client_contact_id', 'person_order'], 'integer'],
            [['agreement_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agreement::className(), 'targetAttribute' => ['agreement_id' => 'id']],
            [['client_contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => ClientContact::className(), 'targetAttribute' => ['client_contact_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('agreement', 'AgreementPerson|Agreement People', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'agreement_id' => Yii::t('agreement', 'Agreement ID'),
            'client_contact_id' => Yii::t('agreement', 'Client Contact ID'),
            'person_order' => Yii::t('agreement', 'Person Order'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreement()
    {
        return $this->hasOne(Agreement::className(), ['id' => 'agreement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientContact()
    {
        return $this->hasOne(ClientContact::className(), ['id' => 'client_contact_id']);
    }
}