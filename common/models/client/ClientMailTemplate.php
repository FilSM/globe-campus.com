<?php

namespace common\models\client;

use Yii;

use common\models\user\FSMUser;
use common\models\abonent\Abonent;
use common\models\client\Client;
use common\models\Files;

/**
 * This is the model class for table "client_mail_template".
 *
 * @property integer $id
 * @property integer $abonent_id
 * @property integer $client_id
 * @property string $cc
 * @property string $subject
 * @property string $header
 * @property string $body
 * @property string $footer
 * @property string $contacts
 * @property integer $uploaded_file_id
 * @property string $create_time
 * @property integer $create_user_id
 * @property string $update_time
 * @property integer $update_user_id
 *
 * @property Abonent $abonent
 * @property Client $client
 * @property Files $uploadedFile
 * @property FSMUser $createUser
 * @property FSMUser $updateUser
 */
class ClientMailTemplate extends \common\models\mainclass\FSMCreateUpdateModel
{

    public static function find($withScope = false) {
        if ((Yii::$app->id != 'app-console') && $withScope) {
            return new \common\scopes\AbonentFieldScopeQuery(get_called_class());
        }
        return parent::find();
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_mail_template';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[/* 'abonent_id', */'client_id'], 'required'],
            [['abonent_id', 'client_id', 'uploaded_file_id', 'create_user_id', 'update_user_id'], 'integer'],
            [['header', 'body', 'footer', 'contacts'], 'string'],
            [['create_time', 'update_time'], 'safe'],
            [['cc', 'subject'], 'string', 'max' => 255],
            [['cc'], 'validateCc'],
            [['abonent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Abonent::class, 'targetAttribute' => ['abonent_id' => 'id']], 
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['client_id' => 'id']],
            [['uploaded_file_id'], 'exist', 'skipOnError' => true, 'targetClass' => Files::class, 'targetAttribute' => ['uploaded_file_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::className(), 'targetAttribute' => ['create_user_id' => 'id']],
            [['update_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::className(), 'targetAttribute' => ['update_user_id' => 'id']],
        ];
    }

    public function validateCc($attribute, $params, $validator)
    {
        $ccStr = preg_replace('/\s+/', '', $this->cc);
        $ccArr = explode(';', $ccStr);
        $emailValidator = new \yii\validators\EmailValidator();
        $result = true;
        $notValidEmail = null;
        foreach ($ccArr as $cc) {
            if(!$result = $emailValidator->validate($cc)){
                $notValidEmail = $cc;
                break;
            }
        }
        if(!$result){
            $message = Yii::t('cleint', '"{value}" is not a valid email address.').' '.Yii::t('client', 'Enter CC addresses, separating them with a semicolon');
            $params = ['value' => $notValidEmail];            
            $validator->addError($this, $attribute, $message, $params);
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('client', 'E-mail template|E-mail templates', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'abonent_id' => Yii::t('client', 'Abonent'),
            'client_id' => Yii::t('client', 'Client'),
            'cc' => Yii::t('client', 'CC'),
            'subject' => Yii::t('client', 'Subject'),
            'header' => Yii::t('client', 'Header'),
            'body' => Yii::t('client', 'Body'),
            'footer' => Yii::t('client', 'Footer'),
            'contacts' => Yii::t('client', 'Contacts'),
            'uploaded_file_id' => Yii::t('client', 'Logo'),
            'create_time' => Yii::t('client', 'Create Time'),
            'create_user_id' => Yii::t('client', 'Create User ID'),
            'update_time' => Yii::t('client', 'Update Time'),
            'update_user_id' => Yii::t('client', 'Update User ID'),
        ];
    }

    protected function getIgnoredFieldsForDelete() {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, ['abonent_id']
        );
        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonent() {
        return $this->hasOne(Abonent::class, ['id' => 'abonent_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::class, ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadedFile()
    {
        return $this->hasOne(Files::class, ['id' => 'uploaded_file_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogo() {
        return $this->hasOne(Files::class, ['id' => 'uploaded_file_id']);
    }  

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(FSMUser::className(), ['id' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdateUser()
    {
        return $this->hasOne(FSMUser::className(), ['id' => 'update_user_id']);
    }    
}