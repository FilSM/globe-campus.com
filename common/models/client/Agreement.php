<?php

namespace common\models\client;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use kartik\helpers\Html;

use common\models\abonent\Abonent;
use common\models\abonent\AbonentAgreement;
use common\models\user\FSMUser;
use common\models\Valuta;
use common\models\Files;
use common\models\bill\Bill;
use common\models\bill\BillTemplate;
use common\models\bill\Convention;
use common\models\client\AgreementAttachment;
use common\models\client\AgreementHistory;
use common\models\client\FirstAgreementPerson;
use common\models\client\SecondAgreementPerson;
use common\components\FSMHelper;
use common\components\FSMAccessHelper;

/**
 * This is the model class for table "agreement".
 *
 * @property integer $id
 * @property integer $deleted
 * @property string $agreement_type
 * @property integer $parent_id
 * @property integer $first_client_id
 * @property integer $second_client_id
 * @property integer $third_client_id
 * @property integer $first_client_role_id
 * @property integer $second_client_role_id
 * @property integer $third_client_role_id
 * @property string $number
 * @property string $signing_date
 * @property string $due_date
 * @property integer $deferment_payment
 * @property string $summa
 * @property integer $valuta_id
 * @property string $rate
 * @property string $summa_eur
 * @property string $rate_rate
 * @property string $rate_summa
 * @property string $rate_from_date
 * @property string $rate_till_date
 * @property string $status
 * @property string $transfer_status
 * @property string $conclusion
 * @property string $write_activity
 * @property string $subject_contract
 * @property string $consideration
 * @property string $prepayment_proc
 * @property string $prepayment_sum
 * @property integer $prepayment_valuta_id
 * @property integer $prepayment_term
 * @property string $comment
 * @property string $create_time
 * @property integer $create_user_id
 * @property string $update_time
 * @property integer $update_user_id
 *
 * @property Abonent[] $abonents
 * @property AbonentAgreement[] $abonentAgreements
 * @property Project[] $projects
 * @property Client $firstClient
 * @property Client $secondClient
 * @property Client $thirdClient
 * @property ClientRole $firstClientRole
 * @property ClientRole $secondClientRole
 * @property ClientRole $thirdClientRole
 * @property Valuta $valuta
 * @property AgreementAttachment $attachments
 * @property AgreementPayment[] $agreementPayments
 * @property BillTemplate[] $billTemplates
 * @property FSMUser $createUser
 * @property FSMUser $updateUser
 */
class Agreement extends \common\models\mainclass\FSMCreateUpdateModel
{

    const AGREEMENT_TYPE_COOPERATION = 'cooperation';
    const AGREEMENT_TYPE_LOAN = 'loan';
    const AGREEMENT_TYPE_CESSION = 'cession';
    const AGREEMENT_TYPE_AGENCY = 'agency';
    const AGREEMENT_TYPE_EMPLOYMENT = 'employment';
    
    const AGREEMENT_STATUS_POTENCIAL = 'potencial';
    const AGREEMENT_STATUS_NEW = 'new';
    const AGREEMENT_STATUS_READY = 'ready';
    const AGREEMENT_STATUS_SIGNED = 'signed';
    const AGREEMENT_STATUS_OVERDUE = 'overdue';
    const AGREEMENT_STATUS_ARCHIVED = 'archived';
    const AGREEMENT_STATUS_CANCELED = 'canceled';
    
    const AGREEMENT_CONCLUSION_ORAL = 'oral';
    const AGREEMENT_CONCLUSION_WRITTEN = 'written';

    const AGREEMENT_TRANSFER_STATUS_NONE = '';
    const AGREEMENT_TRANSFER_STATUS_SENT = 'sent';
    const AGREEMENT_TRANSFER_STATUS_RECEIVED = 'received';

    static $nameField = 'number';
    
    protected $_externalFields = [
        'project_id',
        'project_name',
        'first_client_name',
        'second_client_name',
        'third_client_name',
        'number_concat',
        'grid_progress',
    ];
    
    public $project_id = [];

    public function init() {
        parent::init();
        $this->cascadeDeleting = true;
    }

    public static function find($withScope = false) {
        if ((Yii::$app->id != 'app-console') && $withScope) {
            $scope = new \common\scopes\AgreementScopeQuery(get_called_class());
            return $scope->onlyAuthor()->forClient();            
        }
        return parent::find();
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'agreement';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['first_client_id', 'second_client_id',
                'first_client_role_id', 'second_client_role_id', 'number',
                'summa', 'valuta_id', 'deferment_payment'], 'required'],
            [['deleted', 'parent_id', 'first_client_id',
                'second_client_id', 'third_client_id', 'first_client_role_id',
                'second_client_role_id', 'third_client_role_id', 'valuta_id',
                'create_user_id', 'update_user_id', 'deferment_payment', 
                'prepayment_valuta_id', 'prepayment_term'], 'integer'],
            [['signing_date', 'due_date', 'rate_from_date', 'rate_till_date', 'create_time', 'update_time'], 'safe'],
            [['summa', 'rate', 'summa_eur', 'rate_rate', 'rate_summa', 'prepayment_proc', 'prepayment_sum'], 'number'],
            [['status', 'agreement_type', 'transfer_status', 'conclusion', 
                'write_activity', 'subject_contract', 'consideration', 'comment'], 'string'],
            [['number'], 'string', 'max' => 50],
            //[['number'], 'validateUniqueNumber'],
            [['summa'], 'validateRate'],
            [['first_client_id', 'second_client_id', 'third_client_id'], 'validateUniqueClient', 'params' => ['first_client_id', 'second_client_id', 'third_client_id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agreement::class, 'targetAttribute' => ['parent_id' => 'id']],
            [['valuta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Valuta::class, 'targetAttribute' => ['valuta_id' => 'id']],
            [['prepayment_valuta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Valuta::class, 'targetAttribute' => ['prepayment_valuta_id' => 'id']],
            [['first_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['first_client_id' => 'id']],
            [['second_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['second_client_id' => 'id']],
            [['third_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['third_client_id' => 'id']],
            [['first_client_role_id'], 'exist', 'skipOnError' => true, 'targetClass' => ClientRole::class, 'targetAttribute' => ['first_client_role_id' => 'id']],
            [['second_client_role_id'], 'exist', 'skipOnError' => true, 'targetClass' => ClientRole::class, 'targetAttribute' => ['second_client_role_id' => 'id']],
            [['third_client_role_id'], 'exist', 'skipOnError' => true, 'targetClass' => ClientRole::class, 'targetAttribute' => ['third_client_role_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['create_user_id' => 'id']],
            [['update_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['update_user_id' => 'id']],
        ];
    }

    public function validateUniqueClient($attribute, $params, $validator)
    {
        if((!empty($this->first_client_id) && !empty($this->second_client_id) && ($this->first_client_id == $this->second_client_id)) ||
            (!empty($this->first_client_id) && !empty($this->third_client_id) && ($this->first_client_id == $this->third_client_id)) ||
            (!empty($this->second_client_id) && !empty($this->third_client_id) && ($this->second_client_id == $this->third_client_id))){
            $message = Yii::t('client', 'The same clients are selected!');
            $validator->addError($this, $attribute, $message);
        }
    }

    public function validateRate($attribute, $params, $validator)
    {
        $rate = (float) $this->rate;
        $valuta_id = (int) $this->valuta_id;
        if (
                (($rate == 1) && ($valuta_id != Valuta::VALUTA_DEFAULT)) ||
                (($rate != 1) && ($valuta_id == Valuta::VALUTA_DEFAULT))
        ) {
            $message = Yii::t('bill', 'The currency rate is not correct.');
            $validator->addError($this, 'summa', $message);
        }
    }

    public function validateUniqueNumber($attribute, $params, $validator)
    {
        if($this->conclusion != self::AGREEMENT_CONCLUSION_WRITTEN){
            return;
        }
        $baseTableName = $this->tableName();
        $query = self::find(true);
        $query->andWhere([$baseTableName.'.number' => $this->$attribute]);
        if(!empty($this->id)){
            $query->andWhere(['not', [$baseTableName.'.id' => $this->id]]);
        }
        $exists = $query->exists();
        if ($exists) {
            $message = Yii::t('yii', '{attribute} "{value}" has already been taken.');
            $params = ['attribute' => $attribute, 'value' => $this->$attribute];
            $validator->addError($this, $attribute, $message, $params);
        }
    }
    
    public function canUpdate()
    {
        if(!parent::canUpdate()){
            return;
        }
        if(FSMUser::getIsPortalAdmin()){
            return true;
        }
        $abonentModel = $this->currentAbonentAgreement;
        if(isset($abonentModel) && ($abonentModel->primary_abonent === 0) && !empty($abonentModel->project_id)
        ){
            return;
        }
        return true; 
    }
    
    public function canDelete()
    {
        if(!parent::canDelete()){
            return;
        }
        $abonentModel = $this->currentAbonentAgreement;
        if(isset($abonentModel) && ($abonentModel->primary_abonent === 0)){
            return;
        }
        /*
        $billList = $this->getBills()->one();
        if(isset($billList)){
            return;
        }
         * 
         */
        return true; 
    }
    
    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true)
    {
        return parent::label('client', 'Agreement|Agreements', $n, $translate);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'deleted' => Yii::t('common', 'Deleted'),
            'agreement_type' => Yii::t('agreement', 'Agreement type'),
            'parent_id' => Yii::t('common', 'Main agreement'),
            'first_client_id' => Yii::t('agreement', 'First party'),
            'second_client_id' => Yii::t('agreement', 'Second party'),
            'third_client_id' => Yii::t('agreement', 'Third party'),
            'first_client_role_id' => Yii::t('agreement', 'First party role'),
            'second_client_role_id' => Yii::t('agreement', 'Second party role'),
            'third_client_role_id' => Yii::t('agreement', 'Third party role'),
            'number' => Yii::t('bill', 'Doc.number'),
            'signing_date' => Yii::t('client', 'Signing date'),
            'due_date' => Yii::t('client', 'Term'),
            'deferment_payment' => Yii::t('agreement', 'Deferment of payment'),
            'summa' => Yii::t('common', 'Sum'),
            'valuta_id' => Yii::t('common', 'Currency'),
            'rate' => Yii::t('common', 'Rate'),
            'summa_eur' => Yii::t('common', 'Sum').' €',
            'rate_rate' => Yii::t('common', 'Rate').' %',
            'rate_summa' => Yii::t('agreement', 'Rate (sum)'),
            'rate_from_date' => Yii::t('agreement', 'Date from'),
            'rate_till_date' => Yii::t('agreement', 'Date till'),
            'status' => Yii::t('common', 'Status'),
            'transfer_status' => Yii::t('bill', 'Transfer status'),
            'conclusion' => Yii::t('client', 'Conclusion'),
            'write_activity' => Yii::t('agreement', 'Write in the activity'),
            'subject_contract' => Yii::t('agreement', 'Subject of the contract'),
            'consideration' => Yii::t('agreement', 'Considerations'),
            'prepayment_proc' => Yii::t('agreement', 'Prepayment proc.').' %',
            'prepayment_sum' => Yii::t('agreement', 'Prepayment sum').' €',
            'prepayment_valuta_id' => Yii::t('agreement', 'Prepayment currency'),
            'prepayment_term' => Yii::t('agreement', 'Prepayment term'),
            'comment' => Yii::t('common', 'Comment'),
            'create_time' => Yii::t('common', 'Create Time'),
            'create_user_id' => Yii::t('common', 'Create User'),
            'update_time' => Yii::t('common', 'Update Time'),
            'update_user_id' => Yii::t('common', 'Update User'),
            
            'project_id' => Yii::t('agreement', 'Project name'),
            'project_name' => Yii::t('agreement', 'Project name'),
            'first_client_name' => Yii::t('client', 'First party'),
            'second_client_name' => Yii::t('client', 'Second party'),
            'third_client_name' => Yii::t('client', 'Third party'),
        ];
    }
    
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors = ArrayHelper::merge(
            $behaviors,
            [
                \common\behaviors\DeletedModelBehavior::class,
            ]
        );
        return $behaviors;        
    }
    
    protected function getIgnoredFieldsForDelete()
    {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, ['first_client_id', 'second_client_id', 'third_client_id',
            'first_client_role_id', 'second_client_role_id', 'third_client_role_id',
            'valuta_id', 'parent_id']
        );
        return $fields;
    }

    /** @inheritdoc */
    public function beforeValidate() {
        if (!empty($this->first_client_id) && !empty($this->second_client_id) &&
                (
                ($this->first_client_id == $this->second_client_id) ||
                ($this->first_client_id == $this->third_client_id) ||
                ($this->second_client_id == $this->third_client_id)
                )
        ) {
            $this->addError('second_client_id', Yii::t('bill', 'The same clients are selected!'));
            return false;
        } else {
            return parent::beforeValidate();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent() {
        return $this->hasOne(Agreement::class, ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonents() {
        return $this->hasMany(Abonent::class, ['id' => 'abonent_id'])->viaTable('abonent_agreement', ['agreement_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrentAbonent()
    {
        $abonentModel = $this->currentAbonentAgreement;
        return Abonent::findOne($abonentModel->abonent_id);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrentAbonentAgreement()
    {
        $abonentModel = AbonentAgreement::findOne(['abonent_id' => $this->userAbonentId, 'agreement_id' => $this->id]);
        return $abonentModel;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonentAgreements(){
        return $this->hasMany(AbonentAgreement::class, ['agreement_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConventions()
    {
        return $this->hasMany(Convention::class, ['agreement_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser() {
        return $this->hasOne(FSMUser::class, ['id' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstClient() {
        return $this->hasOne(Client::class, ['id' => 'first_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondClient() {
        return $this->hasOne(Client::class, ['id' => 'second_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThirdClient() {
        return $this->hasOne(Client::class, ['id' => 'third_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstClientRole() {
        return $this->hasOne(ClientRole::class, ['id' => 'first_client_role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondClientRole() {
        return $this->hasOne(ClientRole::class, ['id' => 'second_client_role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThirdClientRole() {
        return $this->hasOne(ClientRole::class, ['id' => 'third_client_role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects() {
        $abonentId = $this->userAbonentId;
        return $this->hasMany(Project::class, ['id' => 'project_id'], true)
            ->notDeleted()
            ->viaTable('abonent_agreement_project', ['abonent_agreement_id' => 'id'])
            ->viaTable('abonent_agreement', ['agreement_id' => 'id'], function ($query) use ($abonentId) {
                $query->andWhere(['abonent_id' => $abonentId]);
            });
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdateUser() {
        return $this->hasOne(FSMUser::class, ['id' => 'update_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValuta() {
        return $this->hasOne(Valuta::class, ['id' => 'valuta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrepaymentValuta() {
        return $this->hasOne(Valuta::class, ['id' => 'prepayment_valuta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBills() {
        return $this->hasMany(Bill::class, ['agreement_id' => 'id'])->notDeleted();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementAttachments() {
        return $this->hasMany(AgreementAttachment::class, ['agreement_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments() {
        return $this->getAgreementAttachments();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachmentFiles() {
        return $this->hasMany(Files::class, ['id' => 'uploaded_file_id'])->via('attachments');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementHistories() {
        return $this->hasMany(AgreementHistory::class, ['agreement_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementPayments()
    {
        return $this->hasMany(AgreementPayment::class, ['agreement_id' => 'id'])->orderBy('id desc');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfirmedPayments()
    {
        $result = $this->hasMany(AgreementPayment::class, ['agreement_id' => 'id']);
        $result->andWhere(['not', ['confirmed' => null]]);
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfirmedPaymentsDebet()
    {
        $result = $this->hasMany(AgreementPayment::class, ['agreement_id' => 'id']);
        $result
            ->andWhere(['not', ['confirmed' => null]])
            ->andWhere(['direction' => 'debet']);
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfirmedPaymentsCredit()
    {
        $result = $this->hasMany(AgreementPayment::class, ['agreement_id' => 'id']);
        $result
            ->andWhere(['not', ['confirmed' => null]])
            ->andWhere(['direction' => 'credit']);
        return $result;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotConfirmedPayments()
    {
        $result = $this->hasMany(AgreementPayment::class, ['agreement_id' => 'id']);
        $result->andWhere(['confirmed' => null]);
        return $result;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotConfirmedPaymentsDebet()
    {
        $result = $this->hasMany(AgreementPayment::class, ['agreement_id' => 'id']);
        $result
            ->andWhere(['confirmed' => null])
            ->andWhere(['direction' => 'debet']);
        return $result;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotConfirmedPaymentsCredit()
    {
        $result = $this->hasMany(AgreementPayment::class, ['agreement_id' => 'id']);
        $result
            ->andWhere(['confirmed' => null])
            ->andWhere(['direction' => 'credit']);
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstClientPerson()
    {
        $result = $this->hasMany(FirstAgreementPerson::class, ['agreement_id' => 'id']);
        $result->andWhere(['person_order' => 1]);
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondClientPerson()
    {
        $result = $this->hasMany(SecondAgreementPerson::class, ['agreement_id' => 'id']);
        $result->andWhere(['person_order' => 2]);
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementPeople()
    {
        $result = $this->hasMany(FirstAgreementPerson::class, ['agreement_id' => 'id']);
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementPersons()
    {
        $result = $this->hasMany(FirstAgreementPerson::class, ['agreement_id' => 'id']);
        return $result;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillTemplates()
    {
        return $this->hasMany(BillTemplate::class, ['agreement_id' => 'id'])->orderBy('id desc');
    }
    
    public function getAgreementPaymentsCredit()
    {
        return $this->hasMany(AgreementPayment::class, ['agreement_id' => 'id'])
            ->where(['direction' => 'credit'])
            ->orderBy('id desc');
    }
    
    public function getAgreementPaymentsDebet()
    {
        return $this->hasMany(AgreementPayment::class, ['agreement_id' => 'id'])
            ->where(['direction' => 'debet'])
            ->orderBy('id desc');
    }
    
    public static function getAgreementTypeList() {
        return [
            Agreement::AGREEMENT_TYPE_COOPERATION => Yii::t('agreement', 'Cooperation'),
            Agreement::AGREEMENT_TYPE_LOAN => Yii::t('agreement', 'Loan'),
            Agreement::AGREEMENT_TYPE_CESSION => Yii::t('agreement', 'Cession'),
            Agreement::AGREEMENT_TYPE_AGENCY => Yii::t('agreement', 'Agency'),
            Agreement::AGREEMENT_TYPE_EMPLOYMENT => Yii::t('agreement', 'Employment'),
        ];
    }

    public static function getAgreementStatusList() {
        return [
            Agreement::AGREEMENT_STATUS_POTENCIAL => Yii::t('client', 'Preparation'),
            Agreement::AGREEMENT_STATUS_NEW => Yii::t('client', 'New'),
            Agreement::AGREEMENT_STATUS_READY => Yii::t('client', 'Ready'),
            Agreement::AGREEMENT_STATUS_SIGNED => Yii::t('client', 'Signed'),
            Agreement::AGREEMENT_STATUS_OVERDUE => Yii::t('client', 'Overdue'),
            Agreement::AGREEMENT_STATUS_ARCHIVED => Yii::t('client', 'Arhived'),
            Agreement::AGREEMENT_STATUS_CANCELED => Yii::t('client', 'Canceled'),
        ];
    }

    public static function getAgreementConclusionList() {
        return [
            Agreement::AGREEMENT_CONCLUSION_ORAL => Yii::t('client', 'Oral'),
            Agreement::AGREEMENT_CONCLUSION_WRITTEN => Yii::t('client', 'Written'),
        ];
    }

    static public function getAgreementTransferStatusList()
    {
        return [
            Agreement::AGREEMENT_TRANSFER_STATUS_NONE => Yii::t('common', 'None'),
            Agreement::AGREEMENT_TRANSFER_STATUS_SENT => Yii::t('bill', 'Sent'),
            Agreement::AGREEMENT_TRANSFER_STATUS_RECEIVED => Yii::t('bill', 'Received'),
        ];
    }

    public function getTransferStatusHTML()
    {
        return (!empty($this->transfer_status) ?
            Html::a(
                Html::badge($this->agreementTransferStatusListByAbonent[$this->transfer_status], ['class' => $this->transferBackgroundColor . ' status-badge']), 
                Url::to(['/agreement/index']) . '?AgreementSearch[transfer_status]=' . $this->transfer_status, 
                ['data-pjax' => 0]
            ) : null);
    }
    
    public function getStatusHTML()
    {
        return (!empty($this->status) ?
            Html::a(
                Html::badge($this->agreementStatusList[$this->status], ['class' => $this->statusBackgroundColor . ' status-badge']), Url::to(['/agreement/index']) . '?AgreementSearch[status]=' . $this->status, ['data-pjax' => 0]
            ) : null);
    }
    
    public function getStatusBackgroundColor() {
        return $this->getStaticStatusBackgroundColor($this->status);
    }
    
    public function getTransferBackgroundColor()
    {
        return $this->getStaticTransferBackgroundColor($this->transfer_status);
    }
    
    static public function getStaticStatusBackgroundColor($status = null) {
        $status = !empty($status) ? $status : null;
        switch ($status) {
            case Agreement::AGREEMENT_STATUS_POTENCIAL:
            case Agreement::AGREEMENT_STATUS_CANCELED:
                $class = 'badge-default';
                break;
            case Agreement::AGREEMENT_STATUS_NEW:
                $class = 'badge-info';
                break;
            case Agreement::AGREEMENT_STATUS_READY:
                $class = 'badge-primary';
                break;
            case Agreement::AGREEMENT_STATUS_SIGNED:
                $class = 'badge-success';
                break;
            case Agreement::AGREEMENT_STATUS_OVERDUE:
                $class = 'badge-warning';
                break;
            case Agreement::AGREEMENT_STATUS_ARCHIVED:
                $class = 'badge-danger';
                break;
            default:
                $class = 'badge-default';
                break;
        }
        return $class;
    }
    
    public function getStaticTransferBackgroundColor($status = null)
    {
        $status = !empty($status) ? $status : null;
        $abonentModel = $this->currentAbonent;
        switch ($status) {
            case Bill::BILL_TRANSFER_STATUS_SENT:
                $class = ($abonentModel->primary_abonent == 1 ? 'badge-success' : 'badge-default');
                break;
            case Bill::BILL_TRANSFER_STATUS_RECEIVED:
                $class = ($abonentModel->primary_abonent == 1 ? 'badge-success' : 'badge-default');
                break;
            default:
                $class = 'badge-default';
                break;
        }
        return $class;
    }
    
    static public function getNameArr($where = null, $orderBy = 'number', $idField = 'id', $nameField = 'number', $withScope = true) {
        if (isset($where)) {
            return ArrayHelper::map(self::findByCondition($where, $withScope)->orderBy($orderBy)->asArray()->all(), $idField, $nameField);
        } else {
            return ArrayHelper::map(self::find($withScope)->orderBy($orderBy)->asArray()->all(), $idField, $nameField);
        }
    }

    static function getButtonAttachment(array $params) {
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url

        $disabled = $oral = false;
        $filesModel = $model->attachmentFiles;
        $disabled = empty($filesModel);
        switch ($model->conclusion) {
            case Agreement::AGREEMENT_CONCLUSION_ORAL:
                $oral = true;
                return '';
                break;
            /*
            case Agreement::AGREEMENT_CONCLUSION_WRITTEN:
                $disabled = empty($filesModel);
                break;
             * 
             */
        }
        if (!empty($isBtn)) {
            $url = ($disabled ? '#' : $filesModel[0]->fileurl);
            return Html::a(Html::icon('paperclip'), $url, [
                'class' => 'btn btn-xs ' . ($disabled ? 'btn-danger disabled' : ($oral ? 'btn-default' : 'btn-info')),
                'title' => Yii::t('common', 'Attachment'),
                'target' => !$disabled ? '_blank' : null,
                'data-pjax' => !$disabled ? '0' : null,
                'disabled' => $disabled,
            ]);            
        } else {
            $label = Yii::t('common', 'Attachment');
            $options = [
                'class' => $disabled ? 'disabled' : null,
                'target' => !$disabled ? '_blank' : null,
                'data-pjax' => !$disabled ? 0 : null,
                'disabled' => $disabled,
            ];
            if(count($filesModel) > 1){
                $result = '<li class="dropdown-submenu pull-left">';
                $result .= Html::a(Html::icon('paperclip').'&nbsp;'.$label, '#', [
                    'class' => 'dropdown-toggle',
                    'data-toggle' => 'dropdown',
                    'tabindex' => -1,
                    'data-pjax' => 0,
                ]);                    
                $items = [];
                $index = 1;
                foreach ($filesModel as $file) {
                    $url = $file->fileurl;
                    $items[] = ['label' => $label.'-'.$index, 'url' => $url, 'linkOptions' => $options];
                    $index++;
                }
                $result .= \kartik\dropdown\DropdownX::widget([
                    'items' => $items,
                ]);
                $result .= '</li>';
                return $result;
            }else{
                $url = ($disabled ? '#' : $filesModel[0]->fileurl);
                return '<li>' . Html::a(Html::icon('paperclip').'&nbsp;'.$label, $url, $options) . '</li>' . PHP_EOL;
            }
        }
    }

    static function getButtonPayment(array $params, $btnSize = '', $labeled = false)
    {
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url
        
        if(
            ($model->agreement_type != Agreement::AGREEMENT_TYPE_LOAN) ||
            !FSMAccessHelper::can('createAgreementPayment', $model)
        ){
            return;
        }

        $agreementSumma = $model->summa;
        $paymentsSummaCredit = $model->paymentsSummaCredit;
        $paymentsSummaDebet = $model->paymentsSummaDebet;
        if(($agreementSumma <= $paymentsSummaDebet) && ($agreementSumma <= $paymentsSummaDebet)){
            return '';
        }
            
        $controller = 'agreement-payment';
        $action = 'ajax-create';
        $title = Yii::t('bill', 'Payment');
        $icon = 'eur';
        if (!empty($isBtn)) {
            $result = FSMHelper::vButton($model->id, [
                'label' => empty($isDropdown) ? Yii::t('bill', 'Payment') : null,
                'title' => $title,
                'class' => 'primary',
                'controller' => $controller,
                'action' => $action,
                'icon' => $icon,
                'modal' => true,
            ]);            
        } else {
            $result = FSMHelper::vDropdown($model->id, [
                'label' => Yii::t('bill', 'Payment'),
                'title' => $title,
                'controller' => $controller,
                'action' => $action,
                'icon' => $icon,
                'modal' => true,
            ]);
        }
        return $result;
    }
    
    static function getButtonSendOtherAbonent(array $params)
    {
        if(empty(Yii::$app->params['ENABLE_BILL_TRANSFER'])){
            return '';
        }
        
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url
        if(!FSMAccessHelper::can('sendToAnotherAbonent', $model)){
            return null;
        }            

        if (in_array($model->status, [
                Agreement::AGREEMENT_STATUS_CANCELED,
            ]) || !empty($model->transfer_status)) {
            return '';
        }

        if (!empty($isBtn)) {
            $result = FSMHelper::vButton($model->id, [
                'label' => empty($isDropdown) ? Yii::t('common', 'Send') : null,
                'title' => Yii::t('common', 'Send to another abonent'),
                'controller' => 'abonent-agreement',
                'action' => 'send-other-abonent',
                'icon' => 'log-out',
                'modal' => true,
                //'message' => Yii::t('bill', 'Are you sure you want to send this invoice?'),
            ]);
        } else {
            $result = FSMHelper::vDropdown($model->id, [
                'label' => Yii::t('common', 'Send'),
                'title' => Yii::t('common', 'Send to another abonent'),
                'controller' => 'abonent-agreement',
                'action' => 'send-other-abonent',
                'icon' => 'log-out',
                'modal' => true,
                //'message' => Yii::t('bill', 'Are you sure you want to send this invoice?'),
            ]);
        }
        return $result;
    }
    
    static function getButtonReceiveOtherAbonent(array $params)
    {
        if(empty(Yii::$app->params['ENABLE_BILL_TRANSFER'])){
            return '';
        }
        
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url

        if (in_array($model->status, [
                Agreement::AGREEMENT_STATUS_CANCELED,
            ]) || 
            empty($model->transfer_status) ||
            (!empty($model->transfer_status) && ($model->transfer_status != Agreement::AGREEMENT_TRANSFER_STATUS_SENT))
        ) {
            return '';
        }

        if (!empty($isBtn)) {
            $result = FSMHelper::vButton($model->id, [
                'label' => empty($isDropdown) ? Yii::t('common', 'Receive') : '',
                'title' => Yii::t('common', 'Receive from another abonent'),
                'controller' => 'abonent-agreement',
                'action' => 'receive-other-abonent',
                'icon' => 'log-in',
                'modal' => true,
                //'message' => Yii::t('bill', 'Are you sure you want to send this invoice?'),
            ]);
        } else {
            $result = FSMHelper::vDropdown($model->id, [
                'label' => Yii::t('common', 'Receive'),
                'title' => Yii::t('common', 'Receive from another abonent'),
                'controller' => 'abonent-agreement',
                'action' => 'receive-other-abonent',
                'icon' => 'log-in',
                'modal' => true,
                //'message' => Yii::t('bill', 'Are you sure you want to send this invoice?'),
            ]);
        }
        return $result;
    }
    
    static function getButtonReturn(array $params)
    {
        if(empty(Yii::$app->params['ENABLE_BILL_TRANSFER'])){
            return '';
        }
        
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url

        if (in_array($model->status, [
                Agreement::AGREEMENT_STATUS_CANCELED,
            ]) || 
            empty($model->transfer_status) ||
            (!empty($model->transfer_status) && ($model->transfer_status != Agreement::AGREEMENT_TRANSFER_STATUS_SENT))
        ) {
            return '';
        }

        if (!empty($isBtn)) {
            $result = FSMHelper::aButton($model->id, [
                'label' => empty($isDropdown) ? Yii::t('common', 'Return') : '',
                'title' => Yii::t('common', 'Return agreement'),
                'controller' => 'abonent-agreement',
                'action' => 'return',
                'icon' => 'log-out',
                'message' => Yii::t('agreement', 'Are you sure to return this agreement?'),
            ]);
        } else {
            $result = FSMHelper::aDropdown($model->id, [
                'label' => Yii::t('common', 'Return'),
                'title' => Yii::t('common', 'Receive agreement'),
                'controller' => 'abonent-agreement',
                'action' => 'return',
                'icon' => 'log-out',
                'message' => Yii::t('agreement', 'Are you sure to return this agreement?'),
            ]);
        }
        return $result;
    }
    
    static function getButtonPdf(array $params)
    {
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url

        if (!in_array($model->status, [
                Agreement::AGREEMENT_STATUS_READY,
                Agreement::AGREEMENT_STATUS_SIGNED,
            ])
        ) {
            return '';
        }

        $title = Yii::t('common', 'PDF');
        //$controller = 'abonent-agreement';
        $action = 'print-pdf';
        $icon = 'file';
        if (!empty($isBtn)) {
            $result = FSMHelper::aButton($model->id, [
                'label' => empty($isDropdown) ? $title : '',
                'title' => $title,
                'action' => $action,
                'icon' => $icon,
                'options' => [
                    'target' => '_blank',
                ],
            ]);
        } else {
            $result = FSMHelper::aDropdown($model->id, [
                'label' => $title,
                'title' => $title,
                'action' => $action,
                'icon' => $icon,
                'options' => [
                    'target' => '_blank',
                ],
            ]);
        }
        return $result;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentsSummaCredit()
    {
        static $id;

        if (!array_key_exists($this->id, (array) $id) || ($id[$this->id] == 0)) {
            $paymentList = $this->agreementPaymentsCredit;
            $summa = 0;
            foreach ($paymentList as $payment) {
                $summa += $payment->summa;
            }
            $id[$this->id] = $summa;
        }
        return $id[$this->id];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentsSummaDebet()
    {
        static $id;

        if (!array_key_exists($this->id, (array) $id) || ($id[$this->id] == 0)) {
            $paymentList = $this->agreementPaymentsDebet;
            $summa = 0;
            foreach ($paymentList as $payment) {
                $summa += $payment->summa;
            }
            $id[$this->id] = $summa;
        }
        return $id[$this->id];
    }     
    
    public function getActiveBillList()
    {
        $billList = $this->bills;
        $result = [];
        foreach ($billList as $bill) {
            if(!in_array($bill->status, [
                    Bill::BILL_STATUS_SIGNED,
                    Bill::BILL_STATUS_PREP_PAYMENT,
                    Bill::BILL_STATUS_PAYMENT,
                    Bill::BILL_STATUS_PAID,
                ])){
                continue;
            }
            if(in_array($bill->pay_status, [
                    Bill::BILL_PAY_STATUS_FULL,
                ])){
                continue;
            }   
            $result[] = ['id' => $bill->id, 'doc_number' => $bill->doc_number]; 
        }
        return $result;
    }
    
    public function beforeSave($insert) 
    {
        $this->rate = ($this->valuta_id == Valuta::VALUTA_DEFAULT) ? 1 : $this->rate;
        $this->summa_eur = $this->summa / $this->rate;        
        $this->signing_date = !empty($this->signing_date) ? date('Y-m-d', strtotime($this->signing_date)) : null;
        $this->due_date = !empty($this->due_date) ? date('Y-m-d', strtotime($this->due_date)) : null;
        $this->rate_from_date = !empty($this->rate_from_date) ? date('Y-m-d', strtotime($this->rate_from_date)) : null;
        $this->rate_till_date = !empty($this->rate_till_date) ? date('Y-m-d', strtotime($this->rate_till_date)) : null;
        if(!parent::beforeSave($insert)){
            return;
        }
        return true;
    } 
    
    public function checkOverdue()
    {
        $totalUpdated = 0;
        $searchModel = new search\AgreementSearch();
        
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            $agreementList = $searchModel->searchOverdue();
            foreach ($agreementList as $agreement) {
                $agreement->updateAttributes(['status' => self::AGREEMENT_STATUS_OVERDUE]);
                $totalUpdated++;
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $transaction->rollBack();
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
        } finally {
            return $totalUpdated;
        }         
    }

    public function getProgressButtons($btnSize = null, $labeled = false)
    {
        if(!FSMAccessHelper::can('changeAgreementStatus')){
            return '';
        }
        
        switch ($this->status) {
            case self::AGREEMENT_STATUS_POTENCIAL:
                return
                    FSMHelper::aButton($this->id, [
                        'label' => ($labeled ? Yii::t('agreement', 'To processing') : null),
                        'title' => Yii::t('agreement', 'Send to work'),
                        'controller' => 'agreement-history',
                        'action' => 'agreement-new',
                        'class' => 'info btn-raised waves-effect',
                        'size' => ($btnSize ?? 'xs'),
                        'icon' => 'arrow-up',
                    ]);
                break;
            case self::AGREEMENT_STATUS_NEW:
                return
                    FSMHelper::aButton($this->id, [
                        'label' => ($labeled ? Yii::t('agreement', 'To preparing') : null),
                        'title' => Yii::t('agreement', 'Rollback to preparing'),
                        'controller' => 'agreement-history',
                        'action' => 'agreement-rollback-preparing',
                        'class' => 'default btn-raised waves-effect',
                        'size' => ($btnSize ?? 'xs'),
                        'icon' => 'arrow-down',
                    ]) . '&nbsp;' .
                    FSMHelper::aButton($this->id, [
                        'label' => ($labeled ? Yii::t('agreement', 'To verification') : null),
                        'title' => Yii::t('agreement', 'Send to verification'),
                        'controller' => 'agreement-history',
                        'action' => 'agreement-ready',
                        'class' => 'primary btn-raised waves-effect',
                        'size' => ($btnSize ?? 'xs'),
                        'icon' => 'arrow-up',
                    ]);
                break;
            case self::AGREEMENT_STATUS_READY:
                return
                    FSMHelper::aButton($this->id, [
                        'label' => ($labeled ? Yii::t('agreement', 'To processing') : null),
                        'title' => Yii::t('agreement', 'Rollback to processing'),
                        'controller' => 'agreement-history',
                        'action' => 'agreement-rollback-new',
                        'class' => 'info btn-raised waves-effect',
                        'size' => ($btnSize ?? 'xs'),
                        'icon' => 'arrow-down',
                    ]) . '&nbsp;' .
                    FSMHelper::aButton($this->id, [
                        'label' => ($labeled ? Yii::t('agreement', 'Sign') : null),
                        'title' => Yii::t('agreement', 'Sign'),
                        'controller' => 'agreement-history',
                        'action' => 'agreement-sign',
                        'class' => 'success btn-raised waves-effect',
                        'size' => ($btnSize ?? 'xs'),
                        'icon' => 'arrow-up',
                    ]);
                break;
            case self::AGREEMENT_STATUS_SIGNED:
                return
                    FSMHelper::aButton($this->id, [
                        'label' => ($labeled ? Yii::t('agreement', 'To signature') : null),
                        'title' => Yii::t('agreement', 'Rollback to signing'),
                        'controller' => 'agreement-history',
                        'action' => 'agreement-rollback-ready',
                        'class' => 'primary btn-raised waves-effect',
                        'size' => ($btnSize ?? 'xs'),
                        'icon' => 'arrow-down',
                    ]) . '&nbsp;' .
                    FSMHelper::aButton($this->id, [
                        'label' => ($labeled ? Yii::t('agreement', 'To archive') : null),
                        'title' => Yii::t('agreement', 'Send to archive'),
                        'controller' => 'agreement-history',
                        'action' => 'agreement-archive',
                        'class' => 'danger btn-raised waves-effect',
                        'size' => ($btnSize ?? 'xs'),
                        'icon' => 'arrow-up',
                    ]);
                break;
            case self::AGREEMENT_STATUS_OVERDUE:
                return
                    FSMHelper::aButton($this->id, [
                        'label' => ($labeled ? Yii::t('agreement', 'To archive') : null),
                        'title' => Yii::t('agreement', 'Send to archive'),
                        'controller' => 'agreement-history',
                        'action' => 'agreement-archive',
                        'class' => 'danger btn-raised waves-effect',
                        'size' => ($btnSize ?? 'xs'),
                        'icon' => 'arrow-up',
                    ]);
                break;
            case self::AGREEMENT_STATUS_ARCHIVED:
                return
                    FSMHelper::aButton($this->id, [
                        'label' => ($labeled ? Yii::t('agreement', 'To processing') : null),
                        'title' => Yii::t('agreement', 'Rollback to processing'),
                        'controller' => 'agreement-history',
                        'action' => 'agreement-rollback-sign',
                        'class' => 'success btn-raised waves-effect',
                        'size' => ($btnSize ?? 'xs'),
                        'icon' => 'arrow-down',
                    ]);
                break;
            case self::AGREEMENT_STATUS_CANCELED:
                return
                    FSMHelper::aButton($this->id, [
                        'label' => ($labeled ? Yii::t('agreement', 'To preparing') : null),
                        'title' => Yii::t('agreement', 'Rollback to preparing'),
                        'controller' => 'agreement-history',
                        'action' => 'agreement-rollback-new',
                        'class' => 'default btn-raised waves-effect',
                        'size' => ($btnSize ?? 'xs'),
                        'icon' => 'arrow-down',
                    ]);
                break;
            default:
                return '';
                break;
        }
    }

    public function changeStatus($status, $params = [])
    {
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            $arrForUpdate = ['status' => $status];
            $arrForUpdate = ArrayHelper::merge($arrForUpdate, $params);
            $this->updateAttributes($arrForUpdate);
            $transaction->commit();
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $transaction->rollBack();
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
            return;
        }
        return true;
    }    
}
