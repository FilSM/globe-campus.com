<?php

namespace common\models\client;

use Yii;
use yii\helpers\ArrayHelper;

use common\models\abonent\Abonent;
use common\models\abonent\AbonentAgreement;
use common\models\abonent\AbonentAgreementProject;
use common\models\abonent\AbonentBill;
use common\models\abonent\AbonentBillTemplate;
use common\models\address\Country;
use common\models\user\FSMUser;
use common\models\bill\Bill;
use common\models\bill\BillProductProject;
use common\models\bill\Expense;
use common\models\Files;

/**
 * This is the model class for table "project".
 *
 * @property integer $id
 * @property integer $abonent_id
 * @property integer $deleted
 * @property string $name
 * @property integer $contry_id
 * @property string $address
 * @property integer $vat_taxable
 * @property string $comment
 * @property string $create_time
 * @property integer $create_user_id
 * @property string $update_time
 * @property integer $update_user_id
 *
 * @property Abonent $abonent
 * @property Bill[] $bills
 * @property Expense[] $expense
 * @property AbonentAgreement[] $abonentAgreements
 * @property AbonentBill[] $abonentBills
 * @property AbonentBillTemplate[] $abonentBillTemplates
 * @property FSMUser $createUser
 * @property FSMUser $updateUser
 */
class Project extends \common\models\mainclass\FSMCreateUpdateModel
{

    protected $_externalFields = [
        'country_name',
    ];

    public function init() {
        parent::init();
        $this->cascadeDeleting = true;
    }

    public static function find($withScope = false) {
        if ((Yii::$app->id != 'app-console') && $withScope) {
            $scope = new \common\scopes\ProjectScopeQuery(get_called_class());
            return $scope->onlyAuthor()->forClient();
        }
        return parent::find();
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['abonent_id', 'name'], 'required'],
            [['deleted', 'abonent_id', 'country_id', 'vat_taxable', 'create_user_id', 'update_user_id'], 'integer'],
            [['comment'], 'string'],
            [['create_time', 'update_time'], 'safe'],
            [['name'], 'string', 'max' => 64],
            [['address'], 'string', 'max' => 100],
            [['country_id', 'name'], 'validateUniqueName', 'params' => ['abonent_id', 'country_id', 'name']],
            [['abonent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Abonent::class, 'targetAttribute' => ['abonent_id' => 'id']],   
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::class, 'targetAttribute' => ['country_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['create_user_id' => 'id']],
            [['update_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['update_user_id' => 'id']],
        ];
    }

    public function validateUniqueName($attribute, $params, $validator)
    {
        $baseTableName = $this->tableName();
        $query = self::find(true);
        $query->andWhere([$baseTableName.'.name' => $this->name]);
        $query->andWhere([$baseTableName.'.country_id' => (!empty($this->country_id) ? $this->country_id : '')]);
        $query->andFilterWhere([$baseTableName.'.abonent_id' => $this->userAbonentId]);
        if(!empty($this->id)){
            $query->andFilterWhere(['not', [$baseTableName.'.id' => $this->id]]);
        }
        $exists = $query->exists();
        if ($exists) {
            $message = Yii::t('client', 
                'The combination of {attribute1} and {attribute2} has already been taken.', 
                [
                    'attribute1' => $this->getAttributeLabel('name'),
                    'attribute2' => $this->getAttributeLabel('country_id'),
                ]
            );
            $validator->addError($this, $attribute, $message);
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('client', 'Project|Projects', $n, $translate);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'abonent_id' => Yii::t('client', 'Abonent'),
            'deleted' => Yii::t('common', 'Deleted'),
            'name' => Yii::t('agreement', 'Project name'),
            'country_id' => Yii::t('address', 'Country'),
            'address' => Yii::t('address', 'Address'),
            'vat_taxable' => Yii::t('agreement', 'VAT taxable transaction'),
            'comment' => Yii::t('common', 'Comment'),
            'create_time' => Yii::t('common', 'Create Time'),
            'create_user_id' => Yii::t('common', 'Create User'),
            'update_time' => Yii::t('common', 'Update Time'),
            'update_user_id' => Yii::t('common', 'Update User'),
            'country_name' => Yii::t('address', 'Country'),
        ];
    }
    
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors = ArrayHelper::merge(
            $behaviors,
            [
                \common\behaviors\DeletedModelBehavior::class,
            ]
        );
        return $behaviors;        
    }

    protected function getIgnoredFieldsForDelete() {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, ['abonent_id', 'country_id']
        );
        return $fields;
    }

    static public function getNameArr($where = null, $orderBy = '', $idField = '', $nameField = '', $withScope = true) {
        $user = Yii::$app->user->identity;
        if($user->hasRole([
                FSMUser::USER_ROLE_CONTRAGENT, 
                FSMUser::USER_ROLE_CLIENT,
            ])
        ){
            $projectModel = Project::find()->notDeleted()
                ->andWhere(['abonent_id' => Yii::$app->session->get('user_current_abonent_id')])
                ->andWhere(['name' => 'Undefined'])
                ->one();
            if(empty($projectModel)){
                $projectModel = new Project();
                $projectModel->abonent_id = Yii::$app->session->get('user_current_abonent_id');
                $projectModel->name = 'Undefined';
                $projectModel->save();
            }
            return [$projectModel-> id => $projectModel->name];
        }   
        
        $idField = !empty($idField) ? $idField : self::$keyField;
        $nameField = !empty($nameField) ? $nameField : self::$nameField;
        $orderBy = !empty($orderBy) ? $orderBy : self::$nameField;
        if (isset($where)) {
            $list = self::findByCondition($where, $withScope)->orderBy($orderBy)->asArray()->all();
        } else {
            $list = self::find($withScope)->orderBy($orderBy)->asArray()->all();
        }
        return ArrayHelper::map($list, $idField, $nameField);
    }
    
    public function canUpdate()
    {
        if(!parent::canUpdate()){
            return;
        }
        if(FSMUser::getIsPortalAdmin()){
            return true;
        }
        return true; 
    }
    
    public function canDelete()
    {
        if(!parent::canDelete()){
            return;
        }
        $agreementList = $this->getAgreements()->one();
        if(isset($agreementList)){
            return;
        }
        return true; 
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonent() {
        return $this->hasOne(Abonent::class, ['id' => 'abonent_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonentAgreements()
    {
        return $this->hasMany(AbonentAgreement::class, ['project_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonentAgreementProjects()
    {
        return $this->hasMany(AbonentAgreementProject::class, ['project_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonentBills()
    {
        return $this->hasMany(AbonentBill::class, ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonentBillTemplates()
    {
        return $this->hasMany(AbonentBillTemplate::class, ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry() {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreements()
    {
        $abonentAgreements = $this->hasMany(\common\models\abonent\AbonentAgreementProject::class, ['project_id' => 'id'])->all();
        $ids = ArrayHelper::map($abonentAgreements, 'abonent_agreement_id', 'abonent_agreement_id');
        return Agreement::find(true)
            ->notDeleted()
            ->leftJoin(['aa' => 'abonent_agreement'], 'aa.agreement_id = agreement.id')
            ->andWhere(['aa.id' => $ids]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveAgreements()
    {
        $query = $this->getAgreements;
        $query
            ->andWhere(['`agreement`.`status`' => [
                Agreement::AGREEMENT_STATUS_POTENCIAL,
                Agreement::AGREEMENT_STATUS_NEW,
                Agreement::AGREEMENT_STATUS_READY,
                Agreement::AGREEMENT_STATUS_SIGNED,
                Agreement::AGREEMENT_STATUS_OVERDUE,
            ]])
            ->orderBy('number');

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser() {
        return $this->hasOne(FSMUser::class, ['id' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdateUser() {
        return $this->hasOne(FSMUser::class, ['id' => 'update_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBills() {
        $abonentId = $this->userAbonentId;
        return $this->hasMany(Bill::class, ['id' => 'bill_id'])
            ->viaTable('abonent_bill', ['project_id' => 'id'], function ($query) use ($abonentId) {
                $query->andWhere(['abonent_id' => $abonentId]);
        });
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillProjects() {
        return $this->hasMany(BillProductProject::class, ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenses() {
        $result = $this->hasMany(Expense::class, ['project_id' => 'id']);
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectAttachments() {
        return $this->hasMany(ProjectAttachment::class, ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments() {
        return $this->getProjectAttachments();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachmentFiles() {
        return $this->hasMany(Files::class, ['id' => 'uploaded_file_id'])->via('attachments');
    }

}
