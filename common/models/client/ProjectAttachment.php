<?php

namespace common\models\client;

use Yii;

use common\models\client\AgreementAttachment;
use common\models\Files;

/**
 * This is the model class for table "project_attachment".
 *
 * @property integer $id
 * @property integer $project_id
 * @property integer $uploaded_file_id
 *
 * @property Project $project
 * @property Project $uploadedFile
 */
class ProjectAttachment extends \common\models\mainclass\FSMBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_attachment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'uploaded_file_id'], 'required'],
            [['project_id', 'uploaded_file_id'], 'integer'],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::class, 'targetAttribute' => ['project_id' => 'id']],
            [['uploaded_file_id'], 'exist', 'skipOnError' => true, 'targetClass' => Files::class, 'targetAttribute' => ['uploaded_file_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('project', 'Project attachment|Project attachments', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'project_id' => Yii::t('bill', 'Project'),
            'uploaded_file_id' => Yii::t('common', 'Attachment'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadedFile()
    {
        return $this->hasOne(Files::class, ['id' => 'uploaded_file_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function beforeDelete()
    {
        $file = $this->uploadedFile;
        if($file->delete()){
            return parent::beforeDelete();
        }
        return false;
    }     
}