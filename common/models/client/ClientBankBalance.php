<?php

namespace common\models\client;

use Yii;

use common\models\FileXML;
use common\models\FilePDF;
use common\models\bill\PaymentConfirm;

/**
 * This is the model class for table "client_bank".
 *
 * @property integer $id
 * @property integer $payment_confirm_id
 * @property integer $account_id
 * @property string $start_date
 * @property string $end_date
 * @property integer $uploaded_file_id
 * @property integer $uploaded_pdf_file_id
 * @property string $balance
 * @property string $currency
 *
 * @property PaymentConfirm $paymentConfirm
 * @property ClientBank $account
 * @property FileXML $uploadedFile
 * @property FilePDF $uploadedPdfFile
 */
class ClientBankBalance extends \common\models\mainclass\FSMBaseModel
{
    
    protected $_externalFields = [
        'file_name_xml',
        'file_name_pdf',
        'valuta_id',
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_bank_balance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_id', 'start_date', 'end_date', 'balance'/*, 'currency'*/], 'required'],
            [['account_id'], 'integer'],
            [['currency'], 'string', 'max' => 3],
            [['balance'], 'number'],
            [['payment_confirm_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentConfirm::class, 'targetAttribute' => ['payment_confirm_id' => 'id']],
            [['account_id'], 'exist', 'skipOnError' => true, 'targetClass' => ClientBank::class, 'targetAttribute' => ['account_id' => 'id']],
            [['uploaded_file_id'], 'exist', 'skipOnError' => true, 'targetClass' => FileXML::class, 'targetAttribute' => ['uploaded_file_id' => 'id']],
            [['uploaded_pdf_file_id'], 'exist', 'skipOnError' => true, 'targetClass' => FilePDF::class, 'targetAttribute' => ['uploaded_pdf_file_id' => 'id']],
            [['account_id', 'start_date', 'end_date'], 'validateUniqueDate'],
            //[['account_id', 'start_date', 'end_date'], 'unique', 'targetAttribute' => ['account_id', 'start_date', 'end_date'], 'message' => Yii::t('client', 'The combination of Account, Start date and End date has already been taken.')]
        ];
    }

    public function validateUniqueDate($attribute, $params, $validator)
    {
        $baseTableName = $this->tableName();
        $query = self::find(true);
        $query->andWhere([$baseTableName.'.account_id' => $this->account_id]);
        $query->andWhere([$baseTableName.'.start_date' => date('Y-m-d', strtotime($this->start_date))]);
        $query->andWhere([$baseTableName.'.end_date' => date('Y-m-d', strtotime($this->end_date))]);
        if(!empty($this->id)){
            $query->andFilterWhere(['not', [$baseTableName.'.id' => $this->id]]);
        }
        $exists = $query->exists();
        if ($exists) {
            $message = Yii::t('client', 'The combination of Account, Start date and End date has already been taken.');
            $params = ['attribute' => $attribute, 'value' => $this->$attribute];
            $validator->addError($this, $attribute, $message, $params);
        }
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true)
    {
        return parent::label('client', 'Bank account balance|Bank account balances', $n, $translate);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'payment_confirm_id' => Yii::t('bill', 'Payment confirmation'),
            'account_id' => Yii::t('client', 'Account'),
            'start_date' => Yii::t('bill', 'Start date'),
            'end_date' => Yii::t('bill', 'End date'),
            'uploaded_file_id' => Yii::t('client', 'XML file'),
            'uploaded_pdf_file_id' => Yii::t('client', 'PDF file'),
            'balance' => Yii::t('client', 'Balance'),
            'currency' => Yii::t('common', 'Currency'),
            
            'valuta_id' => Yii::t('common', 'Currency'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentConfirm()
    {
        return $this->hasOne(PaymentConfirm::class, ['id' => 'payment_confirm_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(ClientBank::class, ['id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadedFile()
    {
        return $this->hasOne(FileXML::class, ['id' => 'uploaded_file_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFileXml()
    {
        return $this->getUploadedFile();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadedPdfFile()
    {
        return $this->hasOne(FilePDF::class, ['id' => 'uploaded_pdf_file_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilePdf()
    {
        return $this->getUploadedPdfFile();
    }    
}
