<?php

namespace common\models\client;

use Yii;
use yii\helpers\ArrayHelper;

use common\models\mainclass\FSMBaseModel;
use common\components\FSMVIES;
use common\models\user\FSMUser;
use common\models\user\FSMProfile;
use common\models\Bank;
use common\models\address\Country;
use common\models\address\Address;
use common\models\Language;
use common\models\Valuta;
use common\models\Files;
use common\models\abonent\Abonent;
use common\models\abonent\AbonentClient;
use common\models\client\ClientContact;
use common\models\bill\Bill;
use common\models\bill\BillConfirm;
use common\models\bill\Expense;
use common\models\bill\PaymentOrder;
use common\models\bill\PaymentConfirm;
use common\models\bill\ReservedId;
use common\models\user\ProfileClient;
use common\models\client\Agreement;
use common\models\bill\Convention;
use common\models\bill\TaxPayment;
/**
 * This is the model class for table "client".
 *
 * @property integer $id
 * @property integer $deleted
 * @property string $client_type
 * @property string $name
 * @property integer $legal_country_id
 * @property string $legal_address
 * @property integer $office_country_id
 * @property string $office_address
 * @property string $reg_number
 * @property integer $vat_payer
 * @property string $vat
 * @property integer $tax
 * @property string $email_address 
 * @property string $phone_number 
 * @property string $nace_code 
 * @property string $network 
 * @property integer $uploaded_file_id
 * @property string $create_time
 * @property integer $create_user_id
 * @property string $update_time
 * @property integer $update_user_id
 *
 * @property Abonent[] $abonents
 * @property AbonentClient[] $abonentClients
 * @property Country $legalCountry
 * @property Country $officeCountry
 * @property Bank[] $clientBanks
 * @property ClientContact[] $clientContacts
 * @property ClientRegDoc[] $clientRegDocs
 * @property ClientMailTemplate $clientMailTemplate
 * @property ClientMailTemplate[] $clientMailTemplates
 * @property Project[] $clientProjects
 * @property PaymentOrder $paymentOrders
 * @property FSMProfile[] $profiles
 * @property Files $logo
 * @property FSMUser $createUser
 * @property FSMUser $updateUser
 */
class Client extends \common\models\mainclass\FSMCreateUpdateModel
{

    const CLIENT_TYPE_PHYSICAL = 'physical';
    const CLIENT_TYPE_LEGAL = 'legal';
    
    const CLIENT_STATUS_ACTIVE = 'active';
    const CLIENT_STATUS_POTENTIAL = 'potential';
    const CLIENT_STATUS_ARCHIVED = 'archived';
    const CLIENT_STATUS_DELETED = 'deleted';    
    
    const CLIENT_IT_IS_OWNER = 'owner';
    const CLIENT_IT_IS_ABONENT = 'abonent';
    const CLIENT_IT_IS_CLIENT = 'client';
    
    const CLIENT_VAT_PAYER_YES = 1;
    const CLIENT_VAT_PAYER_NO = 0;
    
    const CLIENT_DEFAULT_VAT_TAX = 21;

    protected $_externalFields = [
        'parent_name',
        'manager_name',
        'manager_user_id',
        'agent_name',
        'agent_user_id',
        'country_name',
        'lastNumber',

        'parent_id',
        'it_is',
        'client_group_id',
        'manager_id',
        'agent_id',
        'language_id',
        'status',
        'debit',
        'debit_valuta_id',
        'gen_number',
        'comment',
    ];

    public function init() {
        parent::init();
        $this->cascadeDeleting = true;
    }

    public static function find($withScope = false) {
        if ((Yii::$app->id != 'app-console') && $withScope) {
            $scope = new \common\scopes\ClientScopeQuery(get_called_class());
            return $scope->onlyAuthor()->forClient();             
        }
        return parent::find();
    }
    
    public function afterFind()
    {
        parent::afterFind();
        
        static $id = null;
        static $currentAbonentId = null;
        static $abonentData = null;
        
        if(!isset($currentAbonentId)){
            $session = Yii::$app->session ?? null;
            $currentAbonentId = !empty($session->id) ? $session->get('user_current_abonent_id') : null; 
        }

        if(isset($currentAbonentId)){
            
            if(!empty($this->id) && ($this->id != $id)){
                $id = $this->id;
                $abonentData = AbonentClient::find()
                    ->andWhere(['abonent_id' => $currentAbonentId])
                    ->andWhere(['client_id' => $this->id])
                    ->one();
            }

            if($abonentData){
                $data = [
                    'parent_id' => $abonentData->parent_id,
                    'it_is' => $abonentData->it_is,
                    'client_group_id' => $abonentData->client_group_id,
                    'manager_id' => $abonentData->manager_id,
                    'agent_id' => $abonentData->agent_id,
                    'language_id' => $abonentData->language_id,
                    'status' => $abonentData->status,
                    'debit' => $abonentData->debit,
                    'debit_valuta_id' => $abonentData->debit_valuta_id,
                    'gen_number' => $abonentData->gen_number,
                    'comment' => $abonentData->comment,
                ];
                $this->setAttributes($data);
                //$this->setOldAttributes($data);
            }
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'client';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['reg_number', 'name', 'legal_address', 'legal_country_id'], 'required'],
            //[['manager_id'], 'required', 'on' => ['abonent']],
            [['reg_number', 'vat_number'], 'validateUniqueNumber', 'on' => ['default']],
            [['vat_payer', 'deleted', 'legal_country_id', 'office_country_id',
                'tax', 'uploaded_file_id', 'create_user_id', 'update_user_id'], 'integer'],
            [['client_type', 'legal_address', 'office_address', 'nace_code', 'network'], 'string'],
            [['create_time', 'update_time'], 'safe'],
            [['reg_number', 'vat_number'], 'string', 'max' => 30],
            [['name'], 'string', 'max' => 100],
            [['email_address'], 'string', 'max' => 50],
            [['phone_number'], 'string', 'max' => 16],
            [['legal_country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::class, 'targetAttribute' => ['legal_country_id' => 'id']],
            [['office_country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::class, 'targetAttribute' => ['office_country_id' => 'id']],
            [['uploaded_file_id'], 'exist', 'skipOnError' => true, 'targetClass' => Files::class, 'targetAttribute' => ['uploaded_file_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['create_user_id' => 'id']],
            [['update_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['update_user_id' => 'id']],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['abonent'] = [
            'id', 
            'client_type', 
            'name', 
            'reg_number', 
            'vat_payer', 
            'vat_number', 
            'tax', 
            'legal_country_id', 
            'legal_address', 
            'office_country_id', 
            'office_address', 
            'uploaded_file_id',
        ];
        return $scenarios;
    }
    
    public function validateUniqueNumber($attribute, $params, $validator)
    {
        $baseTableName = $this->tableName();
        $query = self::find(true)->notDeleted();
        $query->andWhere([$baseTableName.'.'.$attribute => $this->$attribute]);
        if(!empty($this->id)){
            $query->andWhere(['not', [$baseTableName.'.id' => $this->id]]);
        }
        $exists = $query->exists();
        if ($exists) {
            $message = Yii::t('yii', '{attribute} "{value}" has already been taken.');
            $params = ['attribute' => $attribute, 'value' => $this->$attribute];
            $validator->addError($this, $attribute, $message, $params);
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('client', 'Client|Clients', $n, $translate);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'deleted' => Yii::t('common', 'Deleted'),
            'client_type' => Yii::t('client', 'Client type'),
            'name' => Yii::t('client', 'Full name'),
            'reg_number' => Yii::t('client', 'Reg.number'),
            'vat_payer' => Yii::t('client', 'VAT payer'),
            'vat_number' => Yii::t('client', 'VAT number'),
            'tax' => Yii::t('cargo', 'Tax %'),
            'email_address' => Yii::t('client', 'Email address'),
            'phone_number' => Yii::t('client', 'Phone number'),
            'nace_code' => Yii::t('client', 'NACE code'),
            'network' => Yii::t('client', 'Social network profile link'),
            'legal_country_id' => Yii::t('client', 'Country'),
            'legal_address' => Yii::t('client', 'Legal address'),
            'office_country_id' => Yii::t('client', 'Country'),
            'office_address' => Yii::t('client', 'Office address'),
            'uploaded_file_id' => Yii::t('files', 'Logo'),
            'create_time' => Yii::t('common', 'Create Time'),
            'create_user_id' => Yii::t('common', 'Create User'),
            'update_time' => Yii::t('common', 'Update Time'),
            'update_user_id' => Yii::t('common', 'Update User'),
            
            'parent_id' => Yii::t('client', 'Parent company'),
            'it_is' => Yii::t('common', 'It is'),
            'client_group_id' => Yii::t('client', 'Client group'),
            'manager_id' => Yii::t('common', 'Curator'),
            'agent_id' => Yii::t('common', 'Agent'),
            'language_id' => Yii::t('languages', 'Communication language'),
            'status' => Yii::t('common', 'Status'),
            'debit' => Yii::t('client', 'Debit / Credit (+/-)'),
            'debit_valuta_id' => Yii::t('common', 'Currency'),
            'gen_number' => Yii::t('abonent', 'Number gen.template'),
            'comment' => Yii::t('common', 'Comment'),
            
            'parent_name' => Yii::t('client', 'Parent company'),
            'manager_name' => Yii::t('client', 'Curator'),
            'agent_name' => Yii::t('client', 'Agent'),
            'country_id' => Yii::t('address', 'Country'),
            'country_name' => Yii::t('address', 'Country'),
        ];
    }

    protected function getIgnoredFieldsForDelete() {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, 
            ['main_client_id', 'legal_country_id','office_country_id', 
                'client_group_id', 'parent_id', 'language_id', 
                'manager_id', 'agent_id', 'debit_valuta_id',
            ]
        );
        return $fields;
    }
    
    public function beforeMarkAsDeleted() {
        $client = Client::findOne(['reg_number' => $this->reg_number, 'deleted' => true]);
        if (isset($client)) {
            $client->updateAttributes(['reg_number' => $client->id . '-' . $client->reg_number]);
        }
        return parent::beforeMarkAsDeleted();
    }

    static public function getNameArr($where = null, $orderBy = '', $idField = '', $nameField = '', $withScope = true) {
        $idField = !empty($idField) ? $idField : self::$keyField;
        $nameField = !empty($nameField) ? $nameField : self::$nameField;
        $orderBy = !empty($orderBy) ? $orderBy : self::$nameField;
        if (isset($where)) {
            return ArrayHelper::map(self::findByCondition($where, $withScope)->orderBy($orderBy)->asArray()->all(), $idField, $nameField);
        } else {
            return ArrayHelper::map(self::find($withScope)->orderBy($orderBy)->asArray()->all(), $idField, $nameField);
        }
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonents() {
        return $this->hasMany(Abonent::class, ['id' => 'abonent_id'])->viaTable('abonent_client', ['client_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonentClients(){
        return $this->hasMany(AbonentClient::class, ['client_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainForAbonent() {
        return $this->hasOne(Abonent::class, ['main_client_id' => 'id'])->andWhere(['deleted' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientGroup() {
        $abonentId = $this->userAbonentId;
        $result = $this->hasOne(ClientGroup::class, ['id' => 'client_group_id'])
            ->viaTable('abonent_client', ['client_id' => 'id'], function ($query) use ($abonentId) {
                $query->andWhere(['abonent_id' => $abonentId]);
        });
        return $result;         
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent() {
        $abonentId = $this->userAbonentId;
        $result = $this->hasOne(Client::class, ['id' => 'parent_id'])
            ->viaTable('abonent_client', ['client_id' => 'id'], function ($query) use ($abonentId) {
                $query->andWhere(['abonent_id' => $abonentId]);
        });
        return $result;  
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFullLegalAddress() {
        if (empty($this->legal_address)) {
            return '';
        }
        $address = $this->legal_address . (!empty($this->legal_country_id) ? ', ' . $this->legalCountry->name : '');
        return $address;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLegalCountry() {
        return $this->hasOne(Country::class, ['id' => 'legal_country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFullOfficeAddress() {
        if (empty($this->office_address)) {
            return '';
        }
        $address = $this->office_address . (!empty($this->office_country_id) ? ', ' . $this->officeCountry->name : '');
        return $address;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfficeCountry() {
        return $this->hasOne(Country::class, ['id' => 'office_country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientBanks() {
        return $this->hasMany(ClientBank::class, ['client_id' => 'id'], true);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientActiveBanks() {
        $list = ClientBank::find(true)
            ->andWhere(['client_id' => $this->id])
            ->andWhere('services_period_till IS NULL OR (services_period_till >= DATE(NOW()))')
            ->groupBy('bank_id')
            ->all();        
        return $list;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreements() {
        $agreementList = Agreement::find(true)
            ->andWhere(['or', 
                ['first_client_id' => $this->id],
                ['second_client_id' => $this->id],
                ['third_client_id' => $this->id]
            ])
            ->notDeleted();
        return $agreementList;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenses() {
        $agreementList = Expense::find(true)
            ->andWhere(['or', 
                ['first_client_id' => $this->id],
                ['second_client_id' => $this->id],
            ]);
        return $agreementList;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConventions() {
        return Convention::find(true)
            ->andWhere(['or', 
            ['first_client_id' => $this->id],
            ['second_client_id' => $this->id],
            ['third_client_id' => $this->id]
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientProjects() {
        return $this->hasMany(Project::class, ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillConfirms() {
        return $this->hasMany(BillConfirm::class, ['second_client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses() {
        return $this->hasMany(Address::class, ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        $abonentId = $this->userAbonentId;
        $result = $this->hasOne(Language::class, ['id' => 'language_id'])
            ->viaTable('abonent_client', ['client_id' => 'id'], function ($query) use ($abonentId) {
                $query->andWhere(['abonent_id' => $abonentId]);
        });
        return $result;          
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDebitValuta() {
        $abonentId = $this->userAbonentId;
        $result = $this->hasOne(Valuta::class, ['id' => 'debit_valuta_id'])
            ->viaTable('abonent_client', ['client_id' => 'id'], function ($query) use ($abonentId) {
                $query->andWhere(['abonent_id' => $abonentId]);
        });
        return $result;          
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager() {
        $abonentId = $this->userAbonentId;
        $result = $this->hasOne(FSMProfile::class, ['id' => 'manager_id'])
            ->viaTable('abonent_client', ['client_id' => 'id'], function ($query) use ($abonentId) {
                $query->andWhere(['abonent_id' => $abonentId]);
        });
        return $result;          
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgent() {
        $abonentId = $this->userAbonentId;
        $result = $this->hasOne(FSMProfile::class, ['id' => 'agent_id'])
            ->viaTable('abonent_client', ['client_id' => 'id'], function ($query) use ($abonentId) {
                $query->andWhere(['abonent_id' => $abonentId]);
        });
        return $result;          
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile() {
        $profileList = $this->profiles;
        if (count($profileList) > 0) {
            return $profileList[0];
        } else {
            return null;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfiles() {
        return $this->hasMany(FSMProfile::class, ['id' => 'profile_id'])->viaTable('profile_client', ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfileClients() {
        return $this->hasMany(ProfileClient::class, ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientContacts() {
        return $this->hasMany(ClientContact::class, ['client_id' => 'id'], true)->notDeleted();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceClientContact() {
        $clientContact = ClientContact::findByCondition(['client_id' => $this->id, 'receive_invoice' => 1], true)->one();
        return $clientContact;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceClientAllContact() {
        $contactList = ClientContact::findByCondition(['client_id' => $this->id, 'receive_invoice' => 1], true)->all();
        return $contactList;
    }    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogo() {
        return $this->hasOne(Files::class, ['id' => 'uploaded_file_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadedFile() {
        return $this->getLogo();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentOrders(){
        return $this->hasMany(PaymentOrder::class, ['client_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentConfirms() {
        return $this->hasOne(PaymentConfirm::class, ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientRegDocs() {
        return $this->hasMany(ClientRegDoc::class, ['client_id' => 'id'], true);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShareholders() {
        return $this->hasOne(Shareholder::class, ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientHistories() {
        return $this->hasOne(ClientHistory::class, ['client_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientMailTemplate() {
        return $this->hasOne(ClientMailTemplate::class, ['client_id' => 'id'], true);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientMailTemplates() {
        return $this->hasMany(ClientMailTemplate::class, ['client_id' => 'id'], true);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientInvoicePdf() {
        return $this->hasOne(ClientInvoicePdf::class, ['client_id' => 'id'], true);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientInvoicePdfs() {
        return $this->hasMany(ClientInvoicePdf::class, ['client_id' => 'id'], true);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReserveds() {
        return $this->hasMany(ReservedId::class, ['client_id' => 'id'], true);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxPayments() {
        $list = TaxPayment::find(true)->andWhere(['or', 
            ['from_client_id' => $this->id],
            ['to_client_id' => $this->id],
        ])->all();
        return $list;        
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser() {
        return $this->hasOne(FSMUser::class, ['id' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdateUser() {
        return $this->hasOne(FSMUser::class, ['id' => 'update_user_id']);
    }

    public static function getClientItIsList() {
        return [
            Client::CLIENT_IT_IS_OWNER => Yii::t('client', 'Owner'),
            Client::CLIENT_IT_IS_ABONENT => Yii::t('abonent', 'Abonent'),
            Client::CLIENT_IT_IS_CLIENT => Yii::t('client', 'Simple client'),
        ];
    }

    public function getClientTypeList() {
        return [
            Client::CLIENT_TYPE_PHYSICAL => Yii::t('client', 'Physical person'),
            Client::CLIENT_TYPE_LEGAL => Yii::t('client', 'Legal person'),
        ];
    }

    static public function getClientStatusList() {
        return [
            Client::CLIENT_STATUS_ACTIVE => Yii::t('client', 'Active'),
            Client::CLIENT_STATUS_POTENTIAL => Yii::t('client', 'Potential'),
            Client::CLIENT_STATUS_ARCHIVED => Yii::t('client', 'Archived'),
        ];
    }

    public function getVATPayerTypeList() {
        return [
            Client::CLIENT_VAT_PAYER_YES => Yii::t('common', 'Yes'),
            Client::CLIENT_VAT_PAYER_NO => Yii::t('common', 'No'),
        ];
    }

    static public function getClientList($search) {
        $result = ArrayHelper::map(self::find(true)
            ->andWhere(['LIKE', 'client.name', $search])
            ->orderBy('client.name')
            ->asArray()
            ->all(), 'id', 'name');
        return $result;
    }

    static public function getClientListByItIs($itIs = null, $params = []) {
        $result = ArrayHelper::map(self::find(true)
            ->select(['client.id', 'client.name'])
            ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = '.$baseTableName.'.id AND ac.abonent_id = '. self::getUserAbonentId())
            ->andWhere(['client.deleted' => 0])
            ->andWhere((isset($itIs) ? ['ac.it_is' => $itIs] : 'ac.it_is IS NOT NULL'))
            ->andWhere((isset($params['search']) ? 'client.name LIKE "%' . $params['search'] . '%"' : 'client.name IS NOT NULL'))
            ->andWhere((isset($params['id']) ? ['client.id' => $params['id']] : 'client.id IS NOT NULL'))
            ->orderBy('client.name')
            ->asArray()
            ->all(), 'id', 'name');
        return $result;
    }

    public function getLastNumber($reservedId)
    {
        $reservedIdModel = ReservedId::findOne($reservedId);
        $billList = Bill::find(false)
            ->select(['bill.id', 'bill.doc_number'])
            ->leftJoin(['agreement' => 'agreement'], 'agreement.id = bill.agreement_id')
            //->andWhere(['=', 'YEAR(bill.doc_date)', date('Y')])
            ->andWhere(['=', 'agreement.first_client_id', $this->id])
            //->andWhere(['!=', 'doc_type', Bill::BILL_DOC_TYPE_AVANS])
            ->notDeleted()
            ->addOrderBy('doc_number')
            ->all();
        
        /*
        SELECT 
            `bill`.`id`, `bill`.`doc_number`
        FROM
            `bill`
                LEFT JOIN
            `agreement` `agreement` ON agreement.id = bill.agreement_id
        WHERE
            (`agreement`.`first_client_id` = 1)
            AND (`bill`.`deleted` != 1)
        ORDER BY `doc_number`
         * 
         */
        
        $freeBillArr = $billArr = [];
        foreach ($billList as $bill) {
            if(!empty($bill->doc_number) && (strpos($bill->doc_number, $this->gen_number) !== false)){
                $docNumber = str_replace($this->gen_number, '', $bill->doc_number);
                $billArr[] = preg_replace('/\s+/', '', $docNumber);
            }
        }
        sort($billArr);
        if(!empty($billArr)){
            $firstNumber = preg_replace('/[a-zA-Z]/', '', $billArr[0], -1);
            $firstNumberArr = preg_split("/([\-_])+/", $firstNumber, -1, PREG_SPLIT_NO_EMPTY);
            $firstNumber = intval(end($firstNumberArr));
            $nextNumber = $lastNumber = substr($firstNumber, 0, strlen($firstNumber) - 4) . str_pad(1, 4, "0", STR_PAD_LEFT);
            foreach ($billArr as $key => $billNumber) {
                $currentNumber = preg_replace('/[a-zA-Z]/', '', $billNumber, -1);
                $currentNumberArr = preg_split("/([\-_])+/", $currentNumber, -1, PREG_SPLIT_NO_EMPTY);
                $currentNumber = intval(end($currentNumberArr));
                if($currentNumber < $nextNumber){
                    $currentNumber = intval($currentNumberArr[0]);
                }
                if($currentNumber < $nextNumber){
                    continue;
                }                
                if($nextNumber < $currentNumber){
                    do {
                        $freeBillArr[] = $nextNumber;
                        $nextNumber++;
                    } while ($nextNumber < $currentNumber);
                }
                $nextNumber++;
            }
            
            if(!empty($freeBillArr)){
                foreach ($freeBillArr as $key => $billNumber) {
                    $lastNumber = $billNumber;
                    $nextIdModel = ReservedId::find()
                        ->andWhere(['=', 'client_id', $this->id])
                        ->andWhere(['=', 'doc_number', $lastNumber])
                        ->andWhere(['!=', 'id', $reservedId])
                        ->one();
                    if(empty($nextIdModel)){
                        if(empty($reservedIdModel)){
                            $reservedIdModel = new ReservedId();
                            $reservedIdModel->id = $reservedId;
                            $reservedIdModel->client_id = $this->id;
                        }
                        $reservedIdModel->doc_number = $lastNumber;
                        $reservedIdModel->save();
                        break;
                    }else{
                        continue;
                    }
                }
            }else{
                $lastNumber = $nextNumber;
                if(empty($reservedIdModel)){
                    $reservedIdModel = new ReservedId();
                    $reservedIdModel->id = $reservedId;
                    $reservedIdModel->client_id = $this->id;
                }
                $reservedIdModel->doc_number = $lastNumber;
                $reservedIdModel->save();                
            }
        }else{
            $lastNumber = 1;
        }
        return str_pad($lastNumber, 4, "0", STR_PAD_LEFT);
    }
    
    public function checkViesData($params)
    {
        $service = new FSMVIES();
        $data = $service->search($params);
        unset($service);
        if (!empty($data['result'])) {
            $data = (array) $data['answer'];
        } else {
            $data = [];
        }
        return $data;
    }
    
    public function saveMainContact(ClientContact $mainContactModel, $abonentId = null)
    {
        if(!empty($mainContactModel->phone)){
            if(preg_match('/^[(][+][0-9]{1,4}[)]\s[_]*$/', $mainContactModel->phone, $matches)){
                $mainContactModel->phone = null;
            }
        }
        
        if(
            !empty($mainContactModel->first_name) || 
            !empty($mainContactModel->last_name) || 
            !empty($mainContactModel->phone) || 
            !empty($mainContactModel->email)
        ){
            if($this->client_type == Client::CLIENT_TYPE_PHYSICAL){
                $phisicalName = explode(' ', $this->name);
                $mainContactModel->first_name = $phisicalName[0];
                $mainContactModel->last_name = isset($phisicalName[1]) ? trim($phisicalName[1]) : '...';
            }
            if(isset($abonentId)){
                $mainContactModel->abonent_id = $abonentId;
            }
            $mainContactModel->client_id = $this->id;
            $mainContactModel->main = 1;
            if(!empty($mainContactModel->id)){
                $mainContactModel->setIsNewRecord(false);
            }
            if(!$mainContactModel->save()){
                throw new Exception('Unable to save data! ' . $mainContactModel->errorMessage);
            }
        }
    }

    public function delete(FSMBaseModel $owner = null)
    {
        $result = true;
        if($attachment = $this->logo){
            $result = $attachment->delete();
        }

        return $result && parent::delete($owner);
    }    
}
