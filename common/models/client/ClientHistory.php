<?php

namespace common\models\client;

use Yii;

use common\models\abonent\Abonent;
use common\models\client\Client;
use common\models\user\FSMUser;
/**
 * This is the model class for table "client_history".
 *
 * @property integer $id
 * @property integer $abonent_id
 * @property integer $client_id
 * @property string $history_date
 * @property string $history_text
 * @property string $create_time
 * @property integer $create_user_id
 *
 * @property Abonent $abonent
 * @property Client $client
 * @property FSMUser $createFSMUser
 */
class ClientHistory extends \common\models\mainclass\FSMCreateModel
{
    protected $_externalFields = [
        'user_name',
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['abonent_id', 'client_id', 'create_user_id'], 'integer'],
            [['client_id', 'history_date', 'history_text'], 'required'],
            [['history_date', 'create_time'], 'safe'],
            [['history_text'], 'string', 'max' => 255],
            [['abonent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Abonent::class, 'targetAttribute' => ['abonent_id' => 'id']],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['client_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['create_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('client', 'Client history|Client histories', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'abonent_id' => Yii::t('bill', 'Abonent'),
            'client_id' => Yii::t('client', 'Client'),
            'history_date' => Yii::t('client', 'History date'),
            'history_text' => Yii::t('client', 'History text'),
            'create_time' => Yii::t('common', 'Create Time'),
            'create_user_id' => Yii::t('common', 'Create User'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonent()
    {
        return $this->hasOne(Abonent::class, ['id' => 'abonent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::class, ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateFSMUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'create_user_id']);
    }
}