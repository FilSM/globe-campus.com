<?php

namespace common\models\client;

use Yii;

use common\models\user\FSMUser;
use common\models\abonent\Abonent;
use common\models\client\PersonPosition;

/**
 * This is the model class for table "client_contact".
 *
 * @property integer $id
 * @property integer $abonent_id
 * @property integer $client_id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $email
 * @property string $position_id
 * @property integer $can_sign
 * @property string $acting_on_basis
 * @property integer $receive_invoice
 * @property string $term_from
 * @property string $term_till
 * @property string $share
 * @property integer $top_manager
 * @property integer $main
 *
 * @property Abonent $abonent
 * @property Client $client
 * @property PersonPosition $position
 */
class ClientContact extends \common\models\mainclass\FSMBaseModel
{
    const ACTING_TYPE_CHARTER = 'charter';
    const ACTING_TYPE_ATTORNEY = 'attorney';
    
    protected $_externalFields = [
        'position_name',
        'period',        
    ];

    public static function find($withScope = false) {
        if ((Yii::$app->id != 'app-console') && $withScope) {
            return new \common\scopes\AbonentFieldScopeQuery(get_called_class());
        }
        return parent::find();
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_contact';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[/* 'abonent_id', */'client_id'], 'required'],
            [['first_name', 'last_name'], 'required', 'on' => ['default']],
            [['phone'], 'required', 'on' => ['client'], 'when' => function($model) {
                return Yii::$app->params['appSector'] == 'estate';
            }],
            [['abonent_id', 'client_id', 'position_id', 'can_sign', 'receive_invoice', 'top_manager', 'main'], 'integer'],
            [['acting_on_basis'], 'string'],
            [['first_name', 'last_name'], 'string', 'max' => 30],
            [['phone'], 'string', 'max' => 20],
            [['email'], 'string', 'max' => 50],
            [['email'], 'email'],
            [['term_from', 'term_till'], 'safe'],
            [['share'], 'number'],
            [['abonent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Abonent::class, 'targetAttribute' => ['abonent_id' => 'id']], 
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['client_id' => 'id']],
            [['position_id'], 'exist', 'skipOnError' => true, 'targetClass' => PersonPosition::class, 'targetAttribute' => ['position_id' => 'id']],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['client'] = ['id', 'first_name', 'last_name', 'phone', 'email'];
        return $scenarios;
    }
    
    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true)
    {
        return parent::label('client', 'Contact person|Contact persons', $n, $translate);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'abonent_id' => Yii::t('client', 'Abonent'),
            'client_id' => Yii::t('common', 'Client'),
            'first_name' => Yii::t('client', 'First name'),
            'last_name' => Yii::t('client', 'Last name'),
            'phone' => Yii::t('client', 'Phone'),
            'email' => Yii::t('client', 'Email'),
            'position_id' => Yii::t('client', 'Position'),
            'can_sign' => Yii::t('client', 'Can sign'),
            'acting_on_basis' => Yii::t('client', 'Acting on the basis of'),
            'receive_invoice' => Yii::t('client', 'Send/receive invoices'),
            'term_from' => Yii::t('client', 'Term from'),
            'term_till' => Yii::t('client', 'Term till'),
            'share' => Yii::t('client', 'Share'),
            'top_manager' => Yii::t('client', 'Is TOP manager'),
            'main' => Yii::t('client', 'Main contact'),
            
            'position_name' => Yii::t('client', 'Position'),
            'period' => Yii::t('common', 'Period'),
        ];
    }

    protected function getIgnoredFieldsForDelete() {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, ['abonent_id']
        );
        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonent() {
        return $this->hasOne(Abonent::class, ['id' => 'abonent_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::class, ['id' => 'client_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(PersonPosition::class, ['id' => 'position_id']);
    }
    
    public function beforeValidate()
    {
        if(!empty($this->phone)){
            if(preg_match('/^[(][+][0-9]{1,4}[)]\s[_]*$/', $this->phone, $matches)){
                $this->phone = null;
            }
        }
        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if(!parent::beforeSave($insert)){
            return;
        }
        if($insert){
            $this->abonent_id = $this->userAbonentId;
        }
        return true;
    }    

    public function canUpdate() {
        if(!parent::canUpdate()){
            return;
        }
        if(FSMUser::getIsPortalAdmin()){
            return true;
        }        
        $user = Yii::$app->user->identity;
        if(!isset($user) || ($user->profile->id != $this->client->manager_id)){
            return;
        }        
        return true;
    }

    public function canDelete() {
        if(!parent::canDelete()){
            return;
        }
        return $this->canUpdate();
    }

    public static function getActingTypeList() {
        return [
            self::ACTING_TYPE_CHARTER => Yii::t('client', 'Articles of association'),
            self::ACTING_TYPE_ATTORNEY => Yii::t('client', 'Power of attorney'),
        ];
    }    
}
