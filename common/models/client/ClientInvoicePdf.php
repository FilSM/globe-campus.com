<?php

namespace common\models\client;

use Yii;

use common\models\user\FSMUser;
use common\models\abonent\Abonent;
use common\models\client\Client;
use common\models\Files;

/**
 * This is the model class for table "client_invoice_pdf".
 *
 * @property integer $id
 * @property integer $abonent_id
 * @property integer $client_id
 * @property string $template_name
 * @property integer $uploaded_file_id
 * @property string $create_time
 * @property integer $create_user_id
 * @property string $update_time
 * @property integer $update_user_id
 *
 * @property Abonent $abonent
 * @property Client $client
 * @property Files $uploadedFile
 * @property FSMUser $createUser
 * @property FSMUser $updateUser
 */
class ClientInvoicePdf extends \common\models\mainclass\FSMCreateUpdateModel
{
    const TEMPLATE_NAME_LIST = [
        'bill-base',
        'bill-gr1',
        'bill-gr2',
        'bill-gr3',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_invoice_pdf';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['abonent_id', 'client_id', 'template_name'], 'required'],
            [['abonent_id', 'client_id', 'uploaded_file_id', 'create_user_id', 'update_user_id'], 'integer'],
            [['template_name'], 'string', 'max' => 30],
            [['create_time', 'update_time'], 'safe'],
            [['abonent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Abonent::className(), 'targetAttribute' => ['abonent_id' => 'id']],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['uploaded_file_id'], 'exist', 'skipOnError' => true, 'targetClass' => Files::className(), 'targetAttribute' => ['uploaded_file_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::className(), 'targetAttribute' => ['create_user_id' => 'id']],
            [['update_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::className(), 'targetAttribute' => ['update_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('client', 'Invoice PDF settings|Invoice PDF settings', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'abonent_id' => Yii::t('client', 'Abonent'),
            'client_id' => Yii::t('client', 'Client'),
            'template_name' => Yii::t('client', 'PDF template name'),
            'uploaded_file_id' => Yii::t('common', 'Footer image'),
            'create_time' => Yii::t('client', 'Create time'),
            'create_user_id' => Yii::t('client', 'Create User'),
            'update_time' => Yii::t('client', 'Update time'),
            'update_user_id' => Yii::t('client', 'Update User'),
        ];
    }

    protected function getIgnoredFieldsForDelete() {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, ['abonent_id', 'client_id']
        );
        return $fields;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonent()
    {
        return $this->hasOne(Abonent::className(), ['id' => 'abonent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadedFile()
    {
        return $this->hasOne(Files::className(), ['id' => 'uploaded_file_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(FSMUser::className(), ['id' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdateUser()
    {
        return $this->hasOne(FSMUser::className(), ['id' => 'update_user_id']);
    }
    
    public function getTemplateNameList()
    {
        $result = array_combine(
            ClientInvoicePdf::TEMPLATE_NAME_LIST,
            [
                Yii::t('app', 'Default template'),
                Yii::t('app', 'Invoice Group 1 template'),
                Yii::t('app', 'Invoice Group 2 template'),
                Yii::t('app', 'Invoice Group 3 template'),
            ]
        );
        
        return $result;
        
    }
}