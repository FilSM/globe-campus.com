<?php

namespace common\models\client;

use Yii;
use yii\helpers\ArrayHelper;

use common\models\abonent\Abonent;

/**
 * This is the model class for table "client_group".
 *
 * @property integer $id
 * @property integer $abonent_id
 * @property string $name
 * @property integer $enabled
 * @property integer $is_customer
 *
 * @property Abonent $abonent
 * @property Client[] $clients
 */
class ClientGroup extends \common\models\mainclass\FSMBaseModel
{

    public static function find($withScope = false) {
        if ((Yii::$app->id != 'app-console') && $withScope) {
            $scope = new \common\scopes\ClientGroupScopeQuery(get_called_class());
            return $scope;             
        }
        return parent::find();
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[/* 'abonent_id', */'name'], 'required'],
            [['abonent_id', 'enabled', 'is_customer'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['abonent_id', 'name'], 'unique', 'targetAttribute' => ['abonent_id', 'name']],
            [['abonent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Abonent::class, 'targetAttribute' => ['abonent_id' => 'id']],   
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('client', 'Client group|Client groups', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'abonent_id' => Yii::t('client', 'Abonent'),
            'name' => Yii::t('common', 'Name'),
            'enabled' => Yii::t('common', 'Enabled'),
            'is_customer' => Yii::t('client', 'Is customer'),
        ];
    }

    protected function getIgnoredFieldsForDelete() {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, ['abonent_id']
        );
        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonent() {
        return $this->hasOne(Abonent::class, ['id' => 'abonent_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        $abonentId = $this->userAbonentId;
        $result = $this->hasMany(Client::class, ['id' => 'client_id'])
            ->viaTable('abonent_client', ['client_group_id' => 'id'], function ($query) use ($abonentId) {
                $query->andWhere(['abonent_id' => $abonentId]);
        });
        return $result;        
    }
   
}