<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

use abonent\Abonent;
use abonent\AbonentProduct;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $name
 * @property integer $measure_id
 * @property string $description
 * @property integer $prinded
 *
 * @property Abonent[] $abonents
 * @property AbonentProduct[] $abonentProducts
 * @property Measure $measure
 */
class Product extends \common\models\mainclass\FSMBaseModel
{
    public $_externalFields = [
        'measure_name',
    ];

    public static function find($withScope = false) {
        if ((Yii::$app->id != 'app-console') && $withScope) {
            return new \common\scopes\AbonentScopeQuery(get_called_class());
        }
        return parent::find();
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'measure_id'], 'required'],
            [['measure_id', 'prinded'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['description'], 'string', 'max' => 255],
            ['name', 'validateUniqueName'],
            //[['name'], 'unique', 'targetAttribute' => ['name'], 'skipOnError' => false, 'skipOnEmpty' => false,],
            [['measure_id'], 'exist', 'skipOnError' => true, 'targetClass' => Measure::class, 'targetAttribute' => ['measure_id' => 'id']],
        ];
    }

    public function validateUniqueName($attribute, $params, $validator)
    {
        $baseTableName = $this->tableName();
        $query = self::find(true);
        $query->andWhere([$baseTableName.'.name' => $this->$attribute]);
        if(!empty($this->id)){
            $query->andFilterWhere(['not', [$baseTableName.'.id' => $this->id]]);
        }
        $exists = $query->exists();
        if ($exists) {
            $message = Yii::t('yii', '{attribute} "{value}" has already been taken.');
            $params = ['attribute' => $attribute, 'value' => $this->$attribute];
            $validator->addError($this, $attribute, $message, $params);
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('product', 'Product|Products', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Name'),
            'measure_id' => Yii::t('product', 'Measure'),
            'description' => Yii::t('product', 'Description'),
            'prinded' => Yii::t('product', 'Printout'),
            
            'measure_name' => Yii::t('measure', 'Measure'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonents() {
        return $this->hasMany(Abonent::class, ['id' => 'abonent_id'])->viaTable('abonent_product', ['product_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonentProducts(){
        return $this->hasMany(AbonentProduct::class, ['event_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeasure()
    {
        return $this->hasOne(Measure::class, ['id' => 'measure_id']);
    }
    
    static public function getNameArr($where = null, $orderBy = '', $idField = '', $nameField = '', $withScope = true) {
        $idField = !empty($idField) ? $idField : self::$keyField;
        $nameField = !empty($nameField) ? $nameField : self::$nameField;
        $orderBy = !empty($orderBy) ? $orderBy : self::$nameField;
        if (isset($where)) {
            return ArrayHelper::map(self::findByCondition($where, $withScope)->orderBy($orderBy)->asArray()->all(), $idField, $nameField);
        } else {
            return ArrayHelper::map(self::find($withScope)->orderBy($orderBy)->asArray()->all(), $idField, $nameField);
        }
    } 
    
}