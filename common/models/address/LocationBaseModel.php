<?php

namespace common\models\address;

use Yii;
use yii\helpers\ArrayHelper;

class LocationBaseModel extends \common\models\mainclass\FSMBaseModel
{

    // TranslateBehavior
    /*
    public function behaviors()
    {
        $behaviors = [
            [
                'class' => \lajax\translatemanager\behaviors\TranslateBehavior::class,
                'translateAttributes' => ['name'],
                'category' => 'location',
            ],
            // or If the category is the database table name.
            // [
            //     'class' => \lajax\translatemanager\behaviors\TranslateBehavior::class,
            //     'translateAttributes' => ['name', 'description'],
            //     'category' => static::tableName(),
            // ],
        ];
        return $behaviors;
    }
     * 
     */

    public function getIdByGMapData(array $data = [])
    {
        if (empty($data)) {
            return null;
        }

        $one = $this->findOne($data);
        if (!$one) {
            unset($data['subregion_id']);
            $one = $this->findOne($data);
            if (!$one) {
                $this->setAttributes($data);
                if (!$this->save()) {
                    $message = \Yii::t('location', $this->modelTitle() . ' not inserted due to validation error.');
                    Yii::$app->getSession()->setFlash('error', $message);
                    Yii::error($message, __METHOD__);
                    return [
                        'address_state' => 'error',
                        'message' => $message,
                    ];
                }
                return $this->id;
            }
            return $one->id;
        } else {
            return $one->id;
        }
    }

    /**
     * @return string Returning the 'name' attribute on the site's own language.
     */
    public function getName($params = [], $language = null)
    {
        return Language::d($this->name, $params, $language);

        // or If the category is the database table name.
        // return Language::t(static::tableName(), $this->name, $params, $language);
    }

}
