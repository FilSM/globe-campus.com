<?php

namespace common\models\address;

use Yii;

use lajax\translatemanager\helpers\Language;

/**
 * This is the model class for table "country".
 *
 * @property integer $id
 * @property string $name
 * @property string $short_name
 * @property string $currency
 *
 * @property Address[] $addresses
 * @property Region[] $regions
 * @property City[] $cities
 * @property District[] $districts
 * @property Route[] $routes
 */
class Country extends LocationBaseModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'location_country';
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50],
            [['short_name'], 'string', 'max' => 2],
            [['currency'], 'string', 'max' => 30],
            [['name', 'short_name'], 'unique', 'targetAttribute' => ['name', 'short_name']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) 
    {
        return parent::label('location', 'Country|Countries', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Name'),
            'short_name' => Yii::t('location', 'Short Name'),
            'currency' => Yii::t('common', 'Currency'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::class, ['country_id' => 'id'])->orderBy('formated_address');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegions()
    {
        return $this->hasMany(Region::class, ['country_id' => 'id'])->orderBy('name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::class, ['country_id' => 'id'])->orderBy('name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistricts()
    {
        return $this->hasMany(District::class, ['country_id' => 'id'])->orderBy('name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoutes()
    {
        return $this->hasMany(Route::class, ['country_id' => 'id'])->orderBy('name');
    }
}
