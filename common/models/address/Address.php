<?php

namespace common\models\address;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\mainclass\FSMBaseModel;
use common\models\user\FSMProfile;

/**
 * This is the model class for table "address".
 *
 * @property integer $id
 * @property integer $deleted
 * @property string $address_type
 * @property string $company_name
 * @property integer $country_id
 * @property integer $region_id
 * @property integer $subregion_id
 * @property integer $city_id
 * @property integer $district_id
 * @property integer $route_id
 * @property string $contact_person
 * @property string $contact_phone
 * @property string $contact_email
 * @property string $customer_address
 * @property string $apartment_number
 * @property string $street_number
 * @property string $route
 * @property string $district
 * @property string $political
 * @property string $sublocality_level_1
 * @property string $sublocality
 * @property string $locality
 * @property string $administrative_area_level_1
 * @property string $administrative_area_level_2
 * @property string $postal_code
 * @property double $latitude
 * @property double $longitude
 * @property string $formated_address
 * @property string $create_time
 * @property integer $create_user_id
 * @property string $update_time
 * @property integer $update_user_id
 *
 * @property Country $country
 * @property Region $region
 * @property Subregion $subregion
 * @property City $city
 * @property District $district
 * @property Route $route
 * @property FSMUser $createUser
 * @property FSMUser $updateUser
 * @property FSMProfile $userProfile
 * @property FSMProfile[] $profiles
 */
class Address extends \common\models\mainclass\FSMCreateUpdateModel
{
    const ADDRESS_FIELD_POSTAL_CODE = 'postal_code';
    const ADDRESS_FIELD_COUNTRY = 'country';
    const ADDRESS_FIELD_REGION = 'administrative_area_level_1';
    const ADDRESS_FIELD_SUBREGION = 'administrative_area_level_2';
    const ADDRESS_FIELD_CITY = 'locality';
    const ADDRESS_FIELD_DISTRICT = 'district';
    const ADDRESS_FIELD_STREET = 'route';
    const ADDRESS_FIELD_STREET_NUMBER = 'street_number';
    const ADDRESS_FIELD_FLAT_NUMBER = 'apartment_number';
    
    const ADDRESS_TYPE_USER = 'user';
    const ADDRESS_TYPE_COMPANY = 'company';
    const ADDRESS_TYPE_CUSTOM = 'customs';
    const ADDRESS_TYPE_START = 'start';
    const ADDRESS_TYPE_FINISH = 'finish';
    const ADDRESS_TYPE_INTERIM = 'interim';
    const ADDRESS_TYPE_OBJECT = 'object';

    public $gmapLocationFieldSet = '';
    private $needSave = true;

    public function getNeedSave()
    {
        $addresToSave = !empty($this->customer_address) ? $this->customer_address : $this->addressText;
        $this->needSave = !empty($addresToSave);
        return $this->needSave;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['street_number', 'route', 'locality', 'country', 'postal_code'], 'required'],
                //['company_name', 'required', 'on' => ['default']],
            [['country_id', 'region_id', 'subregion_id', 'city_id', 'district_id', 'route_id',
                'deleted', 'create_user_id', 'update_user_id'], 'integer'],
            [['latitude', 'longitude'], 'number'],
            [['formated_address', 'address_type'], 'string'],
            [['create_time', 'update_time'], 'safe'],
            [['customer_address'], 'string', 'max' => 255],
            [['contact_person', 'contact_email', 'locality', 'country'], 'string', 'max' => 50],
            [['contact_phone', 'postal_code'], 'string', 'max' => 20],
            [['route', 'district', 'political', 'company_name', 'route'
                /* , 'sublocality_level_1', 'sublocality' */, 
                'administrative_area_level_1', 'administrative_area_level_2'], 'string', 'max' => 100],
            [['apartment_number', 'street_number'], 'string', 'max' => 10],
            [['contact_email'], 'email'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::class, 'targetAttribute' => ['country_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::class, 'targetAttribute' => ['region_id' => 'id']],
            [['subregion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subregion::class, 'targetAttribute' => ['subregion_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::class, 'targetAttribute' => ['city_id' => 'id']],
            [['district_id'], 'exist', 'skipOnError' => true, 'targetClass' => District::class, 'targetAttribute' => ['district_id' => 'id']],
            [['route_id'], 'exist', 'skipOnError' => true, 'targetClass' => Route::class, 'targetAttribute' => ['route_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true)
    {
        return parent::label('location', 'Address|Addresses', $n, $translate);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'address_type' => Yii::t('location', 'Address type'),
            'country_id' => Yii::t('location', 'Country'),
            'region_id' => Yii::t('location', 'State/Region'),
            'subregion_id' => Yii::t('location', 'Province'),
            'city_id' => Yii::t('location', 'City'),
            'district_id' => Yii::t('location', 'District'),
            'route_id' => Yii::t('location', 'Street'),
            'customer_address' => Yii::t('location', 'Customer Address'),
            'company_name' => Yii::t('location', 'Company name'),
            'contact_person' => Yii::t('location', 'Contact person'),
            'contact_phone' => Yii::t('location', 'Contact phone'),
            'contact_email' => Yii::t('location', 'Contact Email'),
            'apartment_number' => Yii::t('location', 'Apartment number'),
            'street_number' => Yii::t('location', 'Street Number'),
            'route' => Yii::t('location', 'Street'),
            'district' => Yii::t('location', 'District'),
            'political' => Yii::t('location', 'District'),
            'sublocality_level_1' => Yii::t('location', 'District'),
            'sublocality' => Yii::t('location', 'Sub District'),
            'locality' => Yii::t('location', 'City'),
            'administrative_area_level_1' => Yii::t('location', 'State/Region'),
            'administrative_area_level_2' => Yii::t('location', 'Province'),
            'country' => Yii::t('location', 'Country'),
            'postal_code' => Yii::t('location', 'Postal code'),
            'latitude' => Yii::t('location', 'Latitude'),
            'longitude' => Yii::t('location', 'Longitude'),
            'formated_address' => Yii::t('location', 'Formatted Address'),
            'deleted' => Yii::t('common', 'Deleted'),
            'create_time' => Yii::t('common', 'Create Time'),
            'create_user_id' => Yii::t('common', 'Create User'),
            'update_time' => Yii::t('common', 'Update Time'),
            'update_user_id' => Yii::t('common', 'Update User'),
        ];
    }

    /*
      public function scenarios() {
      $scenarios = parent::scenarios();
      $scenarios = ArrayHelper::merge($scenarios, ['estate' =>
      [
      'postal_code',
      'country',
      'administrative_area_level_1',
      'locality',
      'district',
      'route',
      'street_number',
      ]
      ]);
      return $scenarios;
      }
     * 
     */

    protected function getIgnoredFieldsForDelete()
    {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields,
            ['country_id', 'region_id', 'subregion_id', 'city_id', 'district_id', 'route_id']
        );
        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::class, ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubregion()
    {
        return $this->hasOne(Subregion::className(), ['id' => 'region_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(District::class, ['id' => 'district_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'update_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfiles()
    {
        return $this->hasMany(FSMProfile::class, ['address_id' => 'id']);
    }

    public function getAddressText(array $params = [])
    {
        if(empty($params)){
            $params = [
                'apartment_number',
                'street_number',
                'route',
                'district',
                'city',
                'region',
                'postal_code',
                'country',
            ];
        }
        $result = [];

        if (!empty($this->route) && in_array('route', $params)) {
            $route = '';
            if (!empty($this->apartment_number) && in_array('apartment_number', $params)) {
                $route .= $this->apartment_number . ', ';
            }
            $route .= $this->route;
            if (!empty($this->street_number) && in_array('street_number', $params)) {
                $route .= ' - ' . $this->street_number;
            }
            $result[] = $route;
        }
        
        $district = !empty($this->district) ?
            $this->district :
            (!empty($this->political) ?
                $this->political :
                    (!empty($this->sublocality_level_1) ?
                        $this->sublocality_level_1 :
                        $this->sublocality
                )
            );
        if (!empty($district) && in_array('district', $params)) {
            $result[] = $district;
        }
        
        if (!empty($this->locality) && in_array('city', $params)) {
            $result[] = $this->locality;
        }
        
        preg_match('/(Rīga|Jūrmala)/u', $this->locality, $matches);
        if(empty($matches)){
            /*
            if (!empty($this->administrative_area_level_2) && ($this->administrative_area_level_1 != $this->administrative_area_level_2)) {
                $result[] = $this->administrative_area_level_2;
            }
             * 
             */
            if (!empty($this->administrative_area_level_1) && in_array('region', $params)) {
                $result[] = $this->administrative_area_level_1;
            }
        }
        
        if (!empty($this->postal_code) && in_array('postal_code', $params)) {
            $result[] = $this->postal_code;
        }
        
        if (!empty($this->country) && in_array('country', $params)) {
            $result[] = $this->country;
        }
        $result = implode(', ', $result);
        if (!empty($result)) {
            return $result;
        }
        
        if (!empty($this->formated_address)) {
            return $this->formated_address;
        } elseif (!empty($this->customer_address)) {
            return $this->customer_address;
        }
    }

    public function getAddressTypeList()
    {
        return [
            Address::ADDRESS_TYPE_START => Yii::t('location', 'Start'),
            Address::ADDRESS_TYPE_FINISH => Yii::t('location', 'Finish'),
            Address::ADDRESS_TYPE_INTERIM => Yii::t('location', 'Interim'),
            Address::ADDRESS_TYPE_USER => Yii::t('location', 'User'),
            Address::ADDRESS_TYPE_COMPANY => Yii::t('location', 'Company'),
            Address::ADDRESS_TYPE_CUSTOM => Yii::t('location', 'Customs'),
            Address::ADDRESS_TYPE_OBJECT => Yii::t('location', 'Object'),
        ];
    }

    public function beforeValidate()
    {
        /*
          $dataToSave = Yii::$app->request->post();
          if(!empty($dataToSave) && empty($this->company_name)){
          $error = Yii::t('cargo', 'Need to define one of those: weight, volume or LDM. Or describe your shipment in the comments.');
          $this->addError('company_name', $error);
          return false;
          }
         * 
         */

        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $dataToSave = Yii::$app->request->post();

        if (empty($dataToSave)) {
            return true;
        }

        $this->customer_address = !empty($this->customer_address) ? $this->customer_address : $this->addressText;

        if (empty($this->customer_address)) {
            $this->addError('customer_address',
                Yii::t('user',
                    'The address cannot be empty. ' .
                    'Type address into the text field and select postal address from Google list or select location on the map'
                )
            );
            $message = 'Address not updated due to validation error.';
            Yii::error($message, __METHOD__);
            Yii::$app->getSession()->setFlash('error', Yii::t('user', $message));
            return false;
        }

        $gMapLocation = !empty($this->gmapLocationFieldSet) ?
            $dataToSave[$this->gmapLocationFieldSet] :
            (isset($dataToSave['address-customer_address']) ? $dataToSave['address-customer_address'] : null);

        //$gMapLocation['customer_address'] = $this->customer_address;
        //$gMapLocation['contact_person'] = isset($this->contact_person) ? $this->contact_person : null;
        //$gMapLocation['contact_phone'] = isset($this->contact_phone) ? $this->contact_phone : null;
        //$gMapLocation['contact_email'] = isset($this->contact_email) ? $this->contact_email : null;
        //if(empty($gMapLocation['formated_address'])){
        $location = ArrayHelper::merge($gMapLocation, $this->attributes);
        //}else{
        //    $location = ArrayHelper::merge($this->attributes, $gMapLocation);
        //}

        $location['latitude'] = !empty($gMapLocation['latitude']) ? doubleval($gMapLocation['latitude']) :
                (!empty($location['latitude']) ? doubleval($location['latitude']) : null);
        $location['longitude'] = !empty($gMapLocation['longitude']) ? doubleval($gMapLocation['longitude']) :
                (!empty($location['longitude']) ? doubleval($location['longitude']) : null);

        $address = $this->getLocationData($location);
        $result = true;
        if (!empty($address)) {
            if (isset($address['address_state'])) {
                if ($address['address_state'] == 'error') {
                    $this->addError('location', $address['message']);
                    return false;
                } elseif ($address['address_state'] == 'saved') {
                    $this->formated_address = $this->addressText;
                    return true;
                }
            } else {
                foreach ($address as $field => $value) {
                    if ($result = $this->hasAttribute($field)) {
                        $this->$field = $value;
                    } else {
                        $this->addError($field, 'Field "' . $field . '" not exists!');
                        return false;
                    }
                }
                $this->formated_address = $this->addressText;
            }
        }
        return $result;
    }

    public function saveDataFromGMap($data)
    {
        $addressData = $this->getLocationData($data);
        if (!empty($addressData)) {
            if (isset($addressData['address_state']) && ($addressData['address_state'] == 'error')) {
                Yii::$app->getSession()->setFlash('error', $addressData['message']);
                Yii::error($addressData['message'], __METHOD__);
                return [
                    'address_id' => null,
                    'address_state' => 'error',
                    'message' => $addressData['message'],
                ];
            } elseif (isset($addressData['address_state']) && ($addressData['address_state'] == 'saved')) {
                return $addressData;
            }
        }

        $this->setAttributes($addressData);
        if (!$this->save(false)) {
            if ($this->hasErrors()) {
                $message = [];
                foreach ($this->getErrors() as $attribute) {
                    foreach ($attribute as $error) {
                        $message[] = $error;
                    }
                }
                $message = implode(PHP_EOL, $message);
            } else {
                $message = Yii::t('location', $this->modelTitle() . ' not inserted due to validation error.');
            }
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
            return [
                'address_id' => null,
                'address_state' => 'error',
                'message' => $message,
            ];
        } else {
            return [
                'address_id' => $this->id,
                'address_state' => 'saved',
            ];
        }
    }

    public function getLocationData($data)
    {
        $addressData = $this->getAttributes(null, [
            'create_time',
            'create_user_id',
            'update_time',
            'update_user_id'
        ]);
        $addressData['country_short_name'] = isset($data['country_short_name']) ? $data['country_short_name'] : null;

        //$result = array_diff($data, $addressData);
        /*
          $result = [];
          foreach ($data as $key => $value) {
          if(array_key_exists($key, $addressData) && ($addressData[$key]) != $value){
          $result[$key] = $value;
          }
          }
          if (empty($result) && isset($this->id)) {
          return [
          'address_id' => $this->id,
          'address_state' => 'saved',
          ];
          }
         * 
         */

        $addressData = ArrayHelper::merge($addressData, $data);

        //----------------------------------------------------------------------
        $addressObj = new Country();
        if (!empty($data['country_short_name'])) {
            $idCountry = $addressObj->getIdByGMapData(
                [
                    'name' => $data['country'],
                    'short_name' => $data['country_short_name'],
                ]
            );
        } else {
            $idCountry = $addressObj->getIdByGMapData(
                [
                    'name' => $data['country'],
                ]
            );
        }
        if (is_array($idCountry) && !empty($idCountry['message'])) {
            return $idCountry;
        } else {
            $addressData['country_id'] = $idCountry;
        }
        //----------------------------------------------------------------------
        $addressObj = new Region();
        $idRegion = !empty($data['administrative_area_level_1']) ? $addressObj->getIdByGMapData(
                [
                    'country_id' => $idCountry,
                    'name' => $data['administrative_area_level_1'],
                ]
            ) : null;
        if (is_array($idRegion) && !empty($idRegion['message'])) {
            return $idRegion;
        } else {
            $addressData['region_id'] = $idRegion;
        }
        //----------------------------------------------------------------------
        $addressObj = new Subregion();
        $idSubregion = !empty($data['administrative_area_level_2']) ? $addressObj->getIdByGMapData(
                [
                    'country_id' => $idCountry,
                    'region_id' => $idRegion,
                    'name' => $data['administrative_area_level_2'],
                ]
            ) : null;
        if (is_array($idRegion) && !empty($idRegion['message'])) {
            return $idRegion;
        } else {
            $addressData['subregion_id'] = $idSubregion;
        }        
        //----------------------------------------------------------------------
        $addressObj = new City();
        $idCity = !empty($data['locality']) ? $addressObj->getIdByGMapData(
                [
                    'country_id' => $idCountry,
                    'region_id' => $idRegion,
                    'subregion_id' => $idSubregion,
                    'name' => $data['locality'],
                ]
            ) : null;
        if (is_array($idCity) && !empty($idCity['message'])) {
            return $idCity;
        } else {
            $addressData['city_id'] = $idCity;
        }
        //----------------------------------------------------------------------
        $addressObj = new District();
        $district = !empty($data['district']) ?
            $data['district'] :
            (!empty($data['political']) ?
                $data['political'] :
                (!empty($data['sublocality_level_1']) ?
                    $data['sublocality_level_1'] :
                    $data['sublocality']
                )
            );
        $idDistrict = !empty($district) ? $addressObj->getIdByGMapData(
                [
                    'country_id' => $idCountry,
                    'region_id' => $idRegion,
                    'subregion_id' => $idSubregion,
                    'city_id' => $idCity,
                    'name' => $district,
                ]
            ) : null;
        if (is_array($idDistrict) && !empty($idDistrict['message'])) {
            return $idDistrict;
        } else {
            $addressData['district_id'] = $idDistrict;
        }
        //----------------------------------------------------------------------
        $routeObj = new Route();
        $idRoute = !empty($data['route']) ? $routeObj->getIdByGMapData(
                [
                    'country_id' => $idCountry,
                    'region_id' => $idRegion,
                    'subregion_id' => $idSubregion,
                    'city_id' => $idCity,
                    'district_id' => $idDistrict,
                    'name' => $data['route'],
                ]
            ) : null;
        if (is_array($idRoute) && !empty($idRoute['message'])) {
            return $idRoute;
        } else {
            $addressData['route_id'] = $idRoute;
        }
        unset($addressObj, $addressData['country_short_name']);

        return $addressData;
    }

    public function delete(FSMBaseModel $owner = null)
    {
        $result = parent::delete($owner);
        if (!$result) {
            Yii::$app->getSession()->setFlash('error', Yii::t('user', 'Cant`t delete address'));
        }
        return $result;
    }

    static public function getPostalCodeList($search, $limit = null)
    {
        Yii::$app->db->createCommand("SET sql_mode = ''")->execute();
        $query = self::find()
                ->where(['LIKE', 'postal_code', $search])
                ->groupBy('postal_code')
                ->orderBy(['postal_code' => SORT_ASC, 'district' => SORT_DESC]);
        if (!empty($limit)) {
            $query->limit($limit);
        }
        $result = $query->asArray()->all();
        return $result;
    }

}
