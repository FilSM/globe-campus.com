<?php

namespace common\models\address;

use Yii;

/**
 * This is the model class for table "district".
 *
 * @property integer $id
 * @property integer $country_id
 * @property integer $region_id
 * @property integer $subregion_id
 * @property integer $city_id
 * @property string $name
 *
 * @property Country $country
 * @property Region $region
 * @property Subregion $subregion
 * @property City $city
 * @property Address[] $addresses
 * @property Route[] $routes
 */
class District extends LocationBaseModel
{
    public $_externalFields = [
        'country_name',
        'region_name',
        'subregion_name',
        'city_name',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'location_district';
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'city_id', 'name'], 'required'],
            [['country_id', 'region_id', 'subregion_id', 'city_id'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['name', 'region_id', 'country_id', 'city_id'], 'unique', 'targetAttribute' => ['name', 'region_id', 'country_id', 'city_id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::class, 'targetAttribute' => ['country_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::class, 'targetAttribute' => ['region_id' => 'id']],
            [['subregion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subregion::class, 'targetAttribute' => ['subregion_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::class, 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) 
    {
        return parent::label('location', 'District|Districts', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'country_id' => Yii::t('location', 'Country'),
            'region_id' => Yii::t('location', 'State/Region'),
            'subregion_id' => Yii::t('location', 'Province'),
            'city_id' => Yii::t('location', 'City'),
            'name' => Yii::t('common', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::class, ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubregion()
    {
        return $this->hasOne(Subregion::className(), ['id' => 'region_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::class, ['district_id' => 'id'])->orderBy('formated_address');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoutes()
    {
        return $this->hasMany(Route::class, ['district_id' => 'id'])->orderBy('name');
    }
}
