<?php

namespace common\models\address\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\address\Country;

/**
 * CountrySearch represents the model behind the search form about `common\models\address\Country`.
 */
class CountrySearch extends Country
{

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'short_name', 'currency'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();

        $query = Country::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            $baseTableName . '.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', $baseTableName . '.name', $this->name])
                ->andFilterWhere(['like', $baseTableName . '.short_name', $this->short_name])
                ->andFilterWhere(['like', $baseTableName . '.currency', $this->currency]);

        return $dataProvider;
    }

}
