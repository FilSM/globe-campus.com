<?php

namespace common\models\address\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\address\Subregion;

/**
 * SubregionSearch represents the model behind the search form of `common\models\address\Subregion`.
 */
class SubregionSearch extends Subregion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'country_id', 'region_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
    
        $query = Subregion::find();

        $query->joinWith(['country', 'region']);
        $query->select = [
            $baseTableName . '.*',
            'country_name' => 'location_country.name',
            'region_name' => 'location_region.name',
        ];
        
        if(!isset($params['sort'])){
            $query->addOrderBy('id desc');
        }
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if($this->hasAttribute('version')){
            $this->__unset('version');
        }

        // grid filtering conditions
        $query->andFilterWhere([
            $baseTableName.'.id' => $this->id,
            $baseTableName.'.country_id' => $this->country_id,
            $baseTableName.'.region_id' => $this->region_id,
        ]);

        $query->andFilterWhere(['like', $baseTableName.'.name', $this->name]);

        return $dataProvider;
    }
}