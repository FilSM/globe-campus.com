<?php

namespace common\models\address\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

use common\models\address\District;

/**
 * DistrictSearch represents the model behind the search form about `common\models\address\District`.
 */
class DistrictSearch extends District {

    public function rules() {
        return [
            [['id', 'country_id', 'region_id', 'subregion_id', 'city_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params) {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
        
        $query = District::find();

        $query->joinWith(['country', 'region', 'subregion', 'city']);
        $query->select = [
            $baseTableName . '.*',
            'country_name' => 'location_country.name',
            'region_name' => 'location_region.name',
            'subregion_name' => 'location_subregion.name',
            'city_name' => 'location_city.name',
        ];

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            $baseTableName . '.id' => $this->id,
            $baseTableName . '.country_id' => $this->country_id,
            $baseTableName . '.region_id' => $this->region_id,
            $baseTableName.'.subregion_id' => $this->subregion_id,
            $baseTableName . '.city_id' => $this->city_id,
        ]);

        $query->andFilterWhere(['like', $baseTableName . '.name', $this->name]);

        return $dataProvider;
    }

}
