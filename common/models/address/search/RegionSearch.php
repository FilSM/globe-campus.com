<?php

namespace common\models\address\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\address\Region;

/**
 * RegionSearch represents the model behind the search form about `common\models\address\Region`.
 */
class RegionSearch extends Region
{

    public function rules()
    {
        return [
            [['id', 'country_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();

        $query = Region::find();

        $query->joinWith(['country']);
        $query->select = [
            $baseTableName . '.*',
            'country_name' => 'location_country.name',
        ];

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            $baseTableName . '.id' => $this->id,
            $baseTableName . '.country_id' => $this->country_id,
        ]);

        $query->andFilterWhere(['like', $baseTableName . '.name', $this->name]);

        return $dataProvider;
    }

}
