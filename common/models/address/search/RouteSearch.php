<?php

namespace common\models\address\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\address\Route;

/**
 * RouteSearch represents the model behind the search form of `common\models\address\Route`.
 */
class RouteSearch extends Route
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'country_id', 'region_id', 'subregion_id', 'city_id', 'district_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
    
        $query = Route::find();

        $query->joinWith(['country', 'region', 'subregion', 'city', 'district']);
        $query->select = [
            $baseTableName . '.*',
            'country_name' => 'location_country.name',
            'region_name' => 'location_region.name',
            'subregion_name' => 'location_subregion.name',
            'city_name' => 'location_city.name',
            'district_name' => 'location_district.name',
        ];
        
        if(!isset($params['sort'])){
            $query->addOrderBy('id desc');
        }
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if($this->hasAttribute('version')){
            $this->__unset('version');
        }

        // grid filtering conditions
        $query->andFilterWhere([
            $baseTableName.'.id' => $this->id,
            $baseTableName.'.country_id' => $this->country_id,
            $baseTableName.'.region_id' => $this->region_id,
            $baseTableName.'.subregion_id' => $this->subregion_id,
            $baseTableName.'.city_id' => $this->city_id,
            $baseTableName.'.district_id' => $this->district_id,
        ]);

        $query->andFilterWhere(['like', $baseTableName.'.name', $this->name]);

        return $dataProvider;
    }
}