<?php

namespace common\models\address;

use Yii;

/**
 * This is the model class for table "region".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $name
 *
 * @property Country $country
 * @property Address[] $addresses
 * @property City[] $cities
 * @property District[] $districts
 * @property Route[] $routes
 */
class Region extends LocationBaseModel
{
    public $_externalFields = [
        'country_name',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'location_region';
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'name'], 'required'],
            [['country_id'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['name', 'country_id'], 'unique', 'targetAttribute' => ['name', 'country_id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::class, 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) 
    {
        return parent::label('location', 'Region|Regions', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'country_id' => Yii::t('location', 'Country'),
            'name' => Yii::t('common', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::class, ['region_id' => 'id'])->orderBy('formated_address');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::class, ['region_id' => 'id'])->orderBy('name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistricts()
    {
        return $this->hasMany(District::class, ['region_id' => 'id'])->orderBy('name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoutes()
    {
        return $this->hasMany(Route::class, ['region_id' => 'id'])->orderBy('name');
    }
}
