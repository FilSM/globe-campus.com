<?php

namespace common\models\bill;

use Yii;

/**
 * This is the model class for table "account_map".
 *
 * @property integer $id
 * @property integer $name
 * @property string $description
 *
 * @property Bill[] $bills
 * @property Bill[] $bills0
 */
class AccountMap extends \common\models\mainclass\FSMBaseModel
{
    const CODE_2310 = 2310;
    const CODE_5310 = 5310;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'account_map';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'integer'],
            [['name'], 'unique'],
            [['description'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('bill', 'Account|Account map', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('bill', 'Code'),
            'description' => Yii::t('bill', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBills()
    {
        return $this->hasMany(Bill::class, ['id' => 'bill_id'])->viaTable('bill_account', ['first_account_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBills0()
    {
        return $this->hasMany(Bill::class, ['id' => 'bill_id'])->viaTable('bill_account', ['second_account_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDebetBills()
    {
        return $this->hasMany(Bill::class, ['id' => 'bill_id'])->viaTable('bill_account', ['first_account_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKreditBills()
    {
        return $this->hasMany(Bill::class, ['id' => 'bill_id'])->viaTable('bill_account', ['second_account_id' => 'id']);
    }    
}