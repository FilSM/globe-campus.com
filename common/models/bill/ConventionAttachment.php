<?php

namespace common\models\bill;

use Yii;

use common\models\bill\Convention;
use common\models\Files;

/**
 * This is the model class for table "convention_attachment".
 *
 * @property integer $id
 * @property integer $convention_id
 * @property integer $uploaded_file_id
 *
 * @property Convention $convention
 * @property Files $uploadedFile
 */
class ConventionAttachment extends \common\models\mainclass\FSMBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'convention_attachment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['convention_id', 'uploaded_file_id'], 'required'],
            [['convention_id', 'uploaded_file_id'], 'integer'],
            [['convention_id'], 'exist', 'skipOnError' => true, 'targetClass' => Convention::class, 'targetAttribute' => ['convention_id' => 'id']],
            [['uploaded_file_id'], 'exist', 'skipOnError' => true, 'targetClass' => Files::class, 'targetAttribute' => ['uploaded_file_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('bill', 'Convention attachment|Convention attachments', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'convention_id' => Yii::t('bill', 'Convention ID'),
            'uploaded_file_id' => Yii::t('bill', 'Uploaded File ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConvention()
    {
        return $this->hasOne(Convention::class, ['id' => 'convention_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadedFile()
    {
        return $this->hasOne(Files::class, ['id' => 'uploaded_file_id']);
    }
}