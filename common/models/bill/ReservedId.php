<?php

namespace common\models\bill;

use Yii;

use common\models\client\Client;
use common\models\user\FSMUser;

/**
 * This is the model class for table "reserved_id".
 *
 * @property integer $id
 * @property integer $client_id
 * @property integer $doc_number
 * @property string $create_time
 * @property integer $create_user_id
 *
 * @property Client $client
 */
class ReservedId extends \common\models\mainclass\FSMCreateModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reserved_id';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'doc_number'], 'required'],
            [['client_id', 'doc_number', 'create_user_id'], 'integer'],
            [['create_time'], 'safe'],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['client_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['create_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('app', 'ReservedId|Reserved Ids', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'doc_number' => 'Doc Number',
            'create_time' => Yii::t('common', 'Create Time'),
            'create_user_id' => Yii::t('common', 'Create User'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::class, ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'create_user_id']);
    }    
}