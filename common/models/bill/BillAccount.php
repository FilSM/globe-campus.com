<?php

namespace common\models\bill;

use Yii;

use common\models\bill\Bill;
use common\models\bill\AccountMap;

/**
 * This is the model class for table "bill_account".
 *
 * @property integer $id
 * @property integer $bill_id
 * @property integer $first_account_id
 * @property integer $second_account_id
 * @property string $summa
 *
 * @property Bill $bill
 * @property AccountMap $firstAccount
 * @property AccountMap $secondAccount
 */
class BillAccount extends \common\models\mainclass\FSMBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bill_account';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bill_id', 'summa'], 'required'],
            [['first_account_id', 'second_account_id'], 'required', 'message' => Yii::t('bill', 'Both account numbers must be completed.')],
            [['bill_id', 'first_account_id', 'second_account_id'], 'integer'],
            [['summa'], 'number'],
            [['bill_id', 'first_account_id', 'second_account_id'], 'unique', 
                'targetAttribute' => ['bill_id', 'first_account_id', 'second_account_id'], 
                'message' => Yii::t('bill', 'This combination of Invoice, Debet and Credit has already been taken.')],
            [['first_account_id', 'second_account_id'], 'validateUniqueAccounts'],
            [['bill_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bill::class, 'targetAttribute' => ['bill_id' => 'id']],
            [['first_account_id'], 'exist', 'skipOnError' => true, 'targetClass' => AccountMap::class, 'targetAttribute' => ['first_account_id' => 'id']],
            [['second_account_id'], 'exist', 'skipOnError' => true, 'targetClass' => AccountMap::class, 'targetAttribute' => ['second_account_id' => 'id']],
        ];
    }

    public function validateUniqueAccounts($attribute, $params, $validator)
    {
        if(
            (!empty($this->first_account_id) || !empty($this->second_account_id)) && 
            ($this->first_account_id == $this->second_account_id)
        ){
            $message = Yii::t('bill', 'Account numbers must not be the same.');
            $validator->addError($this, $attribute, $message);
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('bill', 'Accounting entry|Accounting entries', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'bill_id' => Yii::t('bill', 'Bill ID'),
            'first_account_id' => Yii::t('bill', 'Debet'), 
            'second_account_id' => Yii::t('bill', 'Credit'),
            'summa' => Yii::t('common', 'Sum'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBill()
    {
        return $this->hasOne(Bill::class, ['id' => 'bill_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstAccount()
    {
        return $this->hasOne(AccountMap::class, ['id' => 'first_account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondAccount()
    {
        return $this->hasOne(AccountMap::class, ['id' => 'second_account_id']);
    }
}