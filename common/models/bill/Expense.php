<?php

namespace common\models\bill;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use kartik\helpers\Html;

use common\components\FSMHelper;
use common\components\FSMAccessHelper;
use common\models\bill\ExpenseType;
use common\models\bill\ExpenseAttachment;
use common\models\abonent\Abonent;
use common\models\client\Project;
use common\models\client\Client;
use common\models\Valuta;
use common\models\Files;
use common\models\user\FSMUser;
use common\models\user\FSMProfile;

/**
 * This is the model class for table "expense".
 *
 * @property integer $id
 * @property integer $abonent_id
 * @property integer $expense_type_id
 * @property integer $project_id
 * @property string $doc_number
 * @property string $doc_date
 * @property string $pay_type
 * @property integer $first_client_id
 * @property integer $second_client_id
 * @property string $summa
 * @property string $vat
 * @property string $total
 * @property integer $valuta_id
 * @property string $rate
 * @property string $summa_eur
 * @property string $vat_eur
 * @property string $total_eur
 * @property string $first_client_pvn_payer
 * @property string $second_client_pvn_payer
 * @property integer $report_plus
 * @property string $comment
 * @property string $confirmed
 * @property integer $manual_input
 * @property string $create_time
 * @property integer $create_user_id
 * @property string $update_time
 * @property integer $update_user_id
 *
 * @property Abonent $abonent
 * @property Project $project
 * @property ExpenseType $expenseType
 * @property Client $firstClient
 * @property Client $secondClient
 * @property Valuta $valuta
 * @property ExpenseAttachment[] $attachments
 * @property FSMUser $createUser
 * @property FSMUser $updateUser
 */
class Expense extends \common\models\mainclass\FSMCreateUpdateModel
{
    const PAY_TYPE_CASH = 'cash';
    const PAY_TYPE_CARD = 'card';
    const PAY_TYPE_BANK = 'bank';
    
    protected $_externalFields = [
        'project_name',
        'first_client_name',
        'second_client_name',
        'expense_type_name',
        'doc_number_concat',
    ];

    public static function find($withScope = false)
    {
        if ((Yii::$app->id != 'app-console') && $withScope) {
            $scope = new \common\scopes\ExpenseScopeQuery(get_called_class());
            return $scope->onlyAuthor()->forClient();
        }
        return parent::find();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expense';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['abonent_id', 'expense_type_id', 'project_id', 'doc_number', 'doc_date',
                'first_client_id', 'second_client_id', 'summa', 'vat', 'total',
                'valuta_id'], 'required'],
            [['abonent_id', 'expense_type_id', 'project_id', 'first_client_id', 'second_client_id', 
                'valuta_id', 'report_plus', 'manual_input', 'create_user_id', 'update_user_id'], 'integer'],
            [['doc_date', 'confirmed', 'create_time', 'update_time'], 'safe'],
            [['summa', 'vat', 'total', 'rate', 'summa_eur', 'vat_eur', 'total_eur', 'first_client_pvn_payer', 'second_client_pvn_payer'], 'number'],
            [['pay_type', 'comment'], 'string'],
            [['doc_number'], 'string', 'max' => 20],
            [['summa'], 'validateRate'],
            [['doc_number'], 'validateUniqueNumber', 'params' => ['abonent_id', 'doc_number']],
            [['abonent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Abonent::class, 'targetAttribute' => ['abonent_id' => 'id']],   
            [['expense_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ExpenseType::class, 'targetAttribute' => ['expense_type_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::class, 'targetAttribute' => ['project_id' => 'id']],
            [['first_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['first_client_id' => 'id']],
            [['second_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['second_client_id' => 'id']],
            [['valuta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Valuta::class, 'targetAttribute' => ['valuta_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['create_user_id' => 'id']],
            [['update_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['update_user_id' => 'id']],
        ];
    }

    public function validateUniqueNumber($attribute, $params, $validator)
    {
        $baseTableName = $this->tableName();
        $query = self::find(true);
        $query->andWhere([$baseTableName . '.doc_number' => $this->$attribute]);
        $query->andFilterWhere([$baseTableName.'.abonent_id' => $this->userAbonentId]);
        if (!empty($this->id)) {
            $query->andFilterWhere(['not', [$baseTableName . '.id' => $this->id]]);
        }
        $exists = $query->exists();
        if ($exists) {
            $message = Yii::t('yii', '{attribute} "{value}" has already been taken.');
            $params = ['attribute' => $attribute, 'value' => $this->$attribute];
            $validator->addError($this, $attribute, $message, $params);
        }
    }

    public function validateRate($attribute, $params, $validator)
    {
        $rate = (float) $this->rate;
        $valuta_id = (int) $this->valuta_id;
        if (
                (($rate == 1) && ($valuta_id != Valuta::VALUTA_DEFAULT)) ||
                (($rate != 1) && ($valuta_id == Valuta::VALUTA_DEFAULT))
        ) {
            $message = Yii::t('bill', 'The currency rate is not correct.');
            $validator->addError($this, 'summa', $message);
        }
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true)
    {
        return parent::label('bill', 'Expense|Expenses', $n, $translate);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'abonent_id' => Yii::t('client', 'Abonent'),
            'expense_type_id' => Yii::t('bill', 'Expense type'),
            'project_id' => Yii::t('bill', 'Project'),
            'doc_number' => Yii::t('bill', 'Doc.number'),
            'doc_date' => Yii::t('bill', 'Doc.date'),
            'confirmed' => Yii::t('bill', 'Paid'),
            'pay_type' => Yii::t('bill', 'Payment type'),
            'first_client_id' => Yii::t('bill', 'First party'),
            'second_client_id' => Yii::t('bill', 'Second party'),
            'summa' => Yii::t('common', 'Sum'),
            'vat' => Yii::t('common', 'VAT'),
            'total' => Yii::t('common', 'Total'),
            'valuta_id' => Yii::t('common', 'Currency'),
            'rate' => Yii::t('common', 'Rate'),
            'summa_eur' => Yii::t('common', 'Sum').' €',
            'vat_eur' => Yii::t('common', 'VAT').' €',
            'total_eur' => Yii::t('common', 'Total').' €',
            'report_plus' => Yii::t('bill', 'Use in the EBITDA-PLUS'),
            'comment' => Yii::t('common', 'Comment'),
            'create_time' => Yii::t('bill', 'Create Time'),
            'create_user_id' => Yii::t('bill', 'Create User'),
            'update_time' => Yii::t('bill', 'Update Time'),
            'update_user_id' => Yii::t('bill', 'Update User'),
        ];
    }
    
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors = ArrayHelper::merge(
            $behaviors,
            [
                \common\behaviors\DeletedModelBehavior::class,
            ]
        );
        return $behaviors;        
    }
    
    protected function getIgnoredFieldsForDelete()
    {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, ['abonent_id', 'project_id', 'first_client_id', 'second_client_id', 'valuta_id']
        );
        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonent() {
        return $this->hasOne(Abonent::class, ['id' => 'abonent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenseType()
    {
        return $this->hasOne(ExpenseType::class, ['id' => 'expense_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstClient()
    {
        return $this->hasOne(Client::class, ['id' => 'first_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondClient()
    {
        return $this->hasOne(Client::class, ['id' => 'second_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValuta()
    {
        return $this->hasOne(Valuta::class, ['id' => 'valuta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'update_user_id']);
    }

    static function getButtonCopy(array $params)
    {
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url

        if (!FSMAccessHelper::can('createExpense')) {
            return '';
        }

        $action = 'copy';
        $label = Yii::t('common', 'Copy');
        $title = Yii::t('bill', 'Copy current expense');
        $icon = 'duplicate';
        if (!empty($isBtn)) {
            return FSMHelper::aButton($model->id, [
                'label' => $label,
                'title' => $title,
                'action' => $action,
                'icon' => $icon,
            ]);
        } else {
            return FSMHelper::aDropdown($model->id, [
                'label' => $label,
                'title' => $title,
                'action' => $action,
                'icon' => $icon,
            ]);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(ExpenseAttachment::class, ['expense_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenseAttachments()
    {
        return $this->hasMany(ExpenseAttachment::class, ['expense_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachmentFiles()
    {
        return $this->hasMany(Files::class, ['id' => 'uploaded_file_id'])->via('attachments');
    }

    static public function getPayTypeList()
    {
        return [
            Expense::PAY_TYPE_CASH => Yii::t('bill', 'Cash'),
            Expense::PAY_TYPE_CARD => Yii::t('bill', 'Card'),
            Expense::PAY_TYPE_BANK => Yii::t('bill', 'Bank'),
        ];        
    } 
    
    static function getButtonAttachment(array $params)
    {
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url

        if (empty($model->attachments)) {
            return '';
        }

        $filesModel = $model->attachmentFiles;
        if (!empty($isBtn)) {
            $url = $filesModel[0]->fileurl;
            return Html::a(Html::icon('paperclip'), $url, [
                'class' => 'btn btn-xs btn-info',
                'title' => Yii::t('common', 'Attachment'),
                'target' => '_blank',
                'data-pjax' => '0',
            ]);
        } else {
            $label = Yii::t('common', 'Attachment');
            $options = [
                'target' => '_blank',
                'data-pjax' => 0,
            ];
            if(count($filesModel) > 1){
                $result = '<li class="dropdown-submenu pull-left">';
                $result .= Html::a(Html::icon('paperclip').'&nbsp;'.$label, '#', [
                    'class' => 'dropdown-toggle',
                    'data-toggle' => 'dropdown',
                    'tabindex' => -1,
                    'data-pjax' => 0,
                ]);                    
                $items = [];
                $index = 1;
                foreach ($filesModel as $file) {
                    $url = $file->fileurl;
                    $items[] = ['label' => $label.'-'.$index, 'url' => $url, 'linkOptions' => $options];
                    $index++;
                }
                $result .= \kartik\dropdown\DropdownX::widget([
                    'items' => $items,
                ]);
                $result .= '</li>';
                return $result;
            }else{
                $url = $filesModel[0]->fileurl;
                return '<li>' . Html::a(Html::icon('paperclip').'&nbsp;'.$label, $url, $options) . '</li>' . PHP_EOL;
            }
        }
    }

    public function beforeSave($insert)
    {
        $this->doc_date = date('Y-m-d', strtotime($this->doc_date));
        $this->rate = ($this->valuta_id == Valuta::VALUTA_DEFAULT) ? 1 : $this->rate;
        $this->summa_eur = $this->summa / $this->rate;
        $this->vat_eur = $this->vat / $this->rate;
        $this->total_eur = $this->total / $this->rate;
        $this->doc_date = date('Y-m-d', strtotime($this->doc_date));
        $this->confirmed = !empty($this->confirmed) ? date('Y-m-d', strtotime($this->confirmed)) : null;

        $client = $this->firstClient;
        $vatPayer = $client->vat_payer;
        if ($vatPayer) {
            $params = ['code' => $client->vat_number];
            $data = $client->checkViesData($params);
            $vatPayer = !empty($data['valid']);
        }
        $this->first_client_pvn_payer = $vatPayer;

        $client = $this->secondClient;
        $vatPayer = $client->vat_payer;
        if ($vatPayer) {
            $params = ['code' => $client->vat_number];
            $data = $client->checkViesData($params);
            $vatPayer = !empty($data['valid']);
        }
        $this->second_client_pvn_payer = $vatPayer;

        if (!parent::beforeSave($insert)) {
            return;
        }
        if($insert){
            $this->abonent_id = $this->userAbonentId;
        }
        return true;
    }

}
