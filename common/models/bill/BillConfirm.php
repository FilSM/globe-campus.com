<?php

namespace common\models\bill;

use Yii;
use yii\helpers\ArrayHelper;

use common\components\FSMAccessHelper;
use common\components\FSMHelper;
use common\models\user\FSMUser;
use common\models\bill\PaymentConfirm;
use common\models\client\Client;
use common\models\client\ClientBank;
use common\models\bill\Bill;
use common\models\bill\BillPayment;
use common\models\bill\TaxPayment;
use common\models\bill\BillHistory;
use common\models\bill\BillConfirmLink;
use common\models\client\Agreement;
use common\models\client\AgreementPayment;
use common\models\client\AgreementHistory;
use common\models\bill\Expense;
use common\models\Valuta;

/**
 * This is the model class for table "bill_confirm".
 *
 * @property integer $id
 * @property integer $payment_confirm_id
 * @property string $first_client_account
 * @property string $second_client_name
 * @property string $second_client_reg_number
 * @property string $second_client_account
 * @property integer $second_client_id
 * @property string $doc_date
 * @property string $doc_number
 * @property string $bank_ref
 * @property string $direction
 * @property string $summa
 * @property string $currency
 * @property integer $valuta_id
 * @property string $rate
 * @property string $comment
 * @property integer $manual_input
 * @property integer $find_part
 *
 * @property Client $firstClient
 * @property PaymentConfirm $paymentConfirm
 * @property Client $secondClient
 * @property BillConfirmLink[] $confirmLinks
 * @property Bill[] $bills
 * @property BillPayment[] $billPayments
 * @property BillHistory[] $billHistorys
 * @property Agreement[] $agreements
 * @property AgreementPayment[] $agreementPayments
 * @property AgreementHistory[] $agreementHistorys
 * @property Expense[] $expenses
 * @property Expense[] $purchases
 */
class BillConfirm extends \common\models\mainclass\FSMBaseModel
{
    const DIRECTION_DEBET = 'D';
    const DIRECTION_CREDIT = 'C';

    protected $_externalFields = [
        'bill_number',
        'agreement_number',
        'expense_number',
        'linked_ids',    
    ]; 

    public function init() {
        parent::init();
        $this->cascadeDeleting = true;
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bill_confirm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_confirm_id', 'first_client_account', 'doc_date', 'bank_ref', 
                'summa', 'currency'], 'required', 'on' => ['default']],
            [['payment_confirm_id', 'doc_date', 'bank_ref', 'summa', 'valuta_id'], 'required', 'on' => ['create']],
            [['id', 'payment_confirm_id', 'second_client_id', 'valuta_id', 'manual_input', 'find_part'], 'integer'],
            [['doc_date'], 'safe'],
            [['direction', 'comment'], 'string'],
            [['summa', 'rate'], 'number'],
            [['second_client_name'], 'string', 'max' => 100],
            [['second_client_reg_number'], 'string', 'max' => 30],
            [['first_client_account', 'second_client_account'], 'string', 'max' => 34],
            [['doc_number', 'bank_ref'], 'string', 'max' => 35],
            [['currency'], 'string', 'max' => 3],
            [['summa'], 'validateRate'],
            [['payment_confirm_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentConfirm::class, 'targetAttribute' => ['payment_confirm_id' => 'id']],
            [['second_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['second_client_id' => 'id']],
            [['valuta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Valuta::class, 'targetAttribute' => ['valuta_id' => 'id']],
        ];
    }
    
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = [
            'payment_confirm_id',
            'first_client_account',
            'second_client_id',
            'second_client_name',
            'second_client_reg_number',
            'second_client_account',
            'doc_date',
            'doc_number', 
            'bank_ref',
            'summa', 
            'rate', 
            'currency',
            'valuta_id', 
            'comment',
        ];
        return $scenarios;
    }
    
    public function validateRate($attribute, $params, $validator)
    {
        $rate = (float)$this->rate;
        $valuta_id = (int)$this->valuta_id;
        if (empty($valuta_id) && !empty($this->currency)){
            $valuta = Valuta::findOne(['name' => $this->currency]);
            $valuta_id = isset($valuta) ? $valuta->id : null;
        }
            
        if (!empty($valuta_id) && (
                (($rate == 1) && ($valuta_id != Valuta::VALUTA_DEFAULT)) ||
                (($rate != 1) && ($valuta_id == Valuta::VALUTA_DEFAULT))
            )) {
            $message = Yii::t('bill', 'The currency rate is not correct.');
            $validator->addError($this, 'summa', $message);
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('bill', 'Bank payment|Bank payments', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'payment_confirm_id' => Yii::t('bill', 'Payment confirmation'),
            'first_client_account' => Yii::t('bill', 'Client account'),
            'second_client_name' => Yii::t('bill', 'Other side name'),
            'second_client_reg_number' => Yii::t('bill', 'Other side reg.number'),
            'second_client_account' => Yii::t('bill', 'Other side account'),
            'second_client_id' => Yii::t('bill', 'Other side client'),
            'doc_date' => Yii::t('bill', 'Doc.date'),
            'doc_number' => Yii::t('bill', 'Doc.number'),
            'bank_ref' => Yii::t('bill', 'Bank reference'),
            'direction' => Yii::t('bill', 'Direction'),
            'summa' => Yii::t('common', 'Sum'),
            'currency' => Yii::t('common', 'Currency'),
            'valuta_id' => Yii::t('common', 'Currenсy'),
            'rate' => Yii::t('common', 'Rate'),            
            'comment' => Yii::t('common', 'Comment'),
            'manual_input' => Yii::t('bill', 'Manual input'),
            'find_part' => Yii::t('bill', 'Partially found'),
            
            'linked_ids' => Yii::t('bill', 'Linked doc.number'),
        ];
    }

    protected function getIgnoredFieldsForDelete()
    {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, ['payment_confirm_id', 'second_client_id', 'valuta_id']
        );
        return $fields;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBills()
    {
        return $this->hasMany(Bill::class, ['id' => 'doc_id'])
            ->viaTable('bill_confirm_link', ['bill_confirm_id' => 'id'], function($query){
                $query->andWhere(['doc_type' => BillConfirmLink::DOC_TYPE_BILL]);
            });
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillPayments()
    {
        return $this->hasMany(BillPayment::class, ['id' => 'payment_id'])
            ->viaTable('bill_confirm_link', ['bill_confirm_id' => 'id'], function($query){
                $query->andWhere(['doc_type' => BillConfirmLink::DOC_TYPE_BILL]);
            });
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxPayments()
    {
        return $this->hasMany(TaxPayment::class, ['id' => 'doc_id'])
            ->viaTable('bill_confirm_link', ['bill_confirm_id' => 'id'], function($query){
                $query->andWhere(['doc_type' => BillConfirmLink::DOC_TYPE_TAX]);
            });
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillHistorys()
    {
        return $this->hasMany(BillHistory::class, ['id' => 'history_id'])
            ->viaTable('bill_confirm_link', ['bill_confirm_id' => 'id'], function($query){
                $query->andWhere(['doc_type' => BillConfirmLink::DOC_TYPE_BILL]);
            });
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreements()
    {
        return $this->hasMany(Agreement::class, ['id' => 'doc_id'])
            ->viaTable('bill_confirm_link', ['bill_confirm_id' => 'id'], function($query){
                $query->andWhere(['doc_type' => BillConfirmLink::DOC_TYPE_AGREEMENT]);
            });
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementPayments()
    {
        return $this->hasMany(AgreementPayment::class, ['id' => 'payment_id'])
            ->viaTable('bill_confirm_link', ['bill_confirm_id' => 'id'], function($query){
                $query->andWhere(['doc_type' => BillConfirmLink::DOC_TYPE_AGREEMENT]);
            });
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementHistorys()
    {
        return $this->hasMany(AgreementHistory::class, ['id' => 'history_id'])
            ->viaTable('bill_confirm_link', ['bill_confirm_id' => 'id'], function($query){
                $query->andWhere(['doc_type' => BillConfirmLink::DOC_TYPE_AGREEMENT]);
            });
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenses()
    {
        return $this->hasMany(Expense::class, ['id' => 'doc_id'])
            ->viaTable('bill_confirm_link', ['bill_confirm_id' => 'id'], function($query){
                $query->andWhere(['doc_type' => BillConfirmLink::DOC_TYPE_EXPENSE]);
            });
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchases()
    {
        return $this->hasMany(Expense::class, ['id' => 'doc_id'])
            ->viaTable('bill_confirm_link', ['bill_confirm_id' => 'id'], function($query){
                $query->andWhere(['doc_type' => BillConfirmLink::DOC_TYPE_DIRECT]);
            });
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLinkedObjects($status)
    {   
        $objectList = [];
        $list = $this->bills;
        if(!empty($list)){
            $objectList[BillConfirmLink::DOC_TYPE_BILL] = $list;
        }
        $list = $this->agreements;
        if(!empty($list)){
            $objectList[BillConfirmLink::DOC_TYPE_AGREEMENT] = $list;
        }
        $list = $this->expenses;
        if(!empty($list)){
            $objectList[BillConfirmLink::DOC_TYPE_EXPENSE] = $list;
        }
        $list = ($status == PaymentConfirm::IMPORT_STATE_COMPLETE ? $this->purchases : null);
        if(!empty($list)){
            $objectList[BillConfirmLink::DOC_TYPE_DIRECT] = $list;
        }else{
            $list = $this->hasMany(BillConfirmLink::class, ['bill_confirm_id' => 'id'])
                ->andWhere(['doc_type' => BillConfirmLink::DOC_TYPE_DIRECT])
                ->all();
            if(!empty($list)){
                $objectList[BillConfirmLink::DOC_TYPE_DIRECT] = $list;
            }
        }
        $list = ($status == PaymentConfirm::IMPORT_STATE_COMPLETE ? $this->taxPayments : null);
        if(!empty($list)){
            $objectList[BillConfirmLink::DOC_TYPE_TAX] = $list;
        }else{
            $list = $this->hasMany(BillConfirmLink::class, ['bill_confirm_id' => 'id'])
                ->andWhere(['doc_type' => BillConfirmLink::DOC_TYPE_TAX])
                ->all();
            if(!empty($list)){
                $objectList[BillConfirmLink::DOC_TYPE_TAX] = $list;
            }
        }
        return $objectList;
    }    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentConfirm()
    {
        return $this->hasOne(PaymentConfirm::class, ['id' => 'payment_confirm_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondClient()
    {
        return $this->hasOne(Client::class, ['id' => 'second_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValuta()
    {
        return $this->hasOne(Valuta::class, ['id' => 'valuta_id']);
    }
    
    public function getConfirmLinks()
    {
        return $this->hasMany(BillConfirmLink::class, ['bill_confirm_id' => 'id']);
    }
    
    public function getBillConfirmLinks()
    {
        return $this->getConfirmLinks();
    }
    
    static public function getDirectionList() {
        return [
            BillConfirm::DIRECTION_DEBET => Yii::t('bill', 'Outgoing'),
            BillConfirm::DIRECTION_CREDIT => Yii::t('bill', 'Ingoing'),
        ];
    }   
    
    public function canUpdate()
    {
        if(!parent::canUpdate()){
            return;
        }
        
        $paymentConfirm = $this->paymentConfirm;
        if($paymentConfirm->status == PaymentConfirm::IMPORT_STATE_COMPLETE){
            return;
        }
        
        return true; 
    }
    
    public function canDelete()
    {
        if(!parent::canDelete()){
            return;
        }
        
        $paymentConfirm = $this->paymentConfirm;
        if($paymentConfirm->status == PaymentConfirm::IMPORT_STATE_COMPLETE){
            return;
        }
        
        return true; 
    }
    
    static public function getButtonUpdate(array $params)
    {
        extract($params); //$url, $model, $key, $isBtn = true
        if(!$model->canUpdate()){
            return;
        }
        return 
            FSMHelper::vButton($model->id, [
            'title' => Yii::t('kvgrid', 'Update'),
            'controller' => 'bill-confirm',
            'action' => 'update',
            'class' => 'primary',
            'size' => 'btn-xs',
            'icon' => 'pencil',
            //'modal' => true,
        ]);        
    }    
    
    public function getAvailableBillList($asArray = true)
    {
        $billList = Bill::find(true)->notDeleted()
            ->select([
                'id' => 'bill.id',
                'doc_number' => 'bill.doc_number',
                'doc_number_concat' => 'CONCAT(bill.doc_number, " | ",  fp.name, " -> ",  sp.name)',
                'total' => 'bill.total',
            ])
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->leftJoin(['fp' => 'client'], 'fp.id = a.first_client_id')
            ->leftJoin(['sp' => 'client'], 'sp.id = a.second_client_id')
            ->andWhere(['bill.pay_status' => 
                [
                    Bill::BILL_PAY_STATUS_NOT,
                    Bill::BILL_PAY_STATUS_PART,
                    Bill::BILL_PAY_STATUS_PART_CLOSED,
                    Bill::BILL_PAY_STATUS_DELAYED,
                ]
            ])
            ;
        
        if(!empty($this->payment_confirm_id) || !empty ($_GET['payment_confirm_id'])){
            $paymentConfirmId = !empty($this->payment_confirm_id) ? $this->payment_confirm_id : $_GET['payment_confirm_id'];
            $paymentConfirmModel = PaymentConfirm::findOne($paymentConfirmId);
        }        
        
        if(isset($this->id)){
            if(isset($paymentConfirmModel)){
                $firstClientId = $paymentConfirmModel->client_id;
            }else{
                $clientBank = ClientBank::findOne(['account' => $this->first_client_account]);
                $firstClientId = !empty($clientBank) ? $clientBank->client_id : null;
            }

            if(isset($firstClientId)){
                if ($this->direction == 'C') {
                    $billList
                        ->andWhere(['a.first_client_id' => $firstClientId])
                        ->andFilterWhere(['or',
                            ['a.second_client_id' => !empty($this->second_client_id) ? $this->second_client_id : null],
                            ['a.third_client_id' => !empty($this->second_client_id) ? $this->second_client_id : null],
                        ]);
                } else {
                    $billList
                        ->andWhere(['or',
                            ['a.second_client_id' => $firstClientId],
                            ['a.third_client_id' => $firstClientId],
                        ])
                        ->andFilterWhere(['a.first_client_id' => !empty($this->second_client_id) ? $this->second_client_id : null]);
                }
            }else{
                $paymentConfirmModel = $this->paymentConfirm;
                if(isset($paymentConfirmModel)){
                    //$billList->andWhere(['a.first_client_id' => $paymentConfirmModel->client_id]);
                    $billList->andWhere(['or',
                        ['a.first_client_id' => $paymentConfirmModel->client_id],
                        ['a.second_client_id' => $paymentConfirmModel->client_id],
                        ['a.third_client_id' => $paymentConfirmModel->client_id],
                    ]);
                }
            }
        }elseif(isset($paymentConfirmModel)){
            //$billList->andWhere(['a.first_client_id' => $paymentConfirmModel->client_id]);
            $billList->andWhere(['or',
                ['a.first_client_id' => $paymentConfirmModel->client_id],
                ['a.second_client_id' => $paymentConfirmModel->client_id],
                ['a.third_client_id' => $paymentConfirmModel->client_id],
            ]);
        }

        $billList = $billList
            ->orderBy('bill.doc_number, fp.name, sp.name')
            ->all();

        if($asArray){
            $resultList = [];
            foreach ($billList as $key => $bill) {
                $resultList[$bill->id] = $bill->doc_number_concat;
            }      

            return $resultList;
        }else{
            return $billList;
        }
    }
    
    public function getAvailableAgreementList($asArray = true)
    {
        $agreementList = Agreement::find(true)->notDeleted()
            ->select([
                'id' => 'agreement.id',
                'number' => 'agreement.number',
                'number_concat' => 'CONCAT(agreement.number, " | ",  fp.name, " -> ",  sp.name)',
            ])
            ->leftJoin(['fp' => 'client'], 'fp.id = agreement.first_client_id')
            ->leftJoin(['sp' => 'client'], 'sp.id = agreement.second_client_id')
            ->andWhere(['agreement.agreement_type' => Agreement::AGREEMENT_TYPE_LOAN])
            ->andWhere(['agreement.status' => 
                [
                    Agreement::AGREEMENT_STATUS_NEW,
                    Agreement::AGREEMENT_STATUS_POTENCIAL,
                    Agreement::AGREEMENT_STATUS_SIGNED,
                    Agreement::AGREEMENT_STATUS_OVERDUE,
                ]
            ]);
        
        if(!empty($this->payment_confirm_id) || !empty ($_GET['payment_confirm_id'])){
            $paymentConfirmId = !empty($this->payment_confirm_id) ? $this->payment_confirm_id : $_GET['payment_confirm_id'];
            $paymentConfirmModel = PaymentConfirm::findOne($paymentConfirmId);
        }
        
        if(isset($this->id)){
            if(isset($paymentConfirmModel)){
                $firstClientId = $paymentConfirmModel->client_id;
            }else{
                $clientBank = ClientBank::findOne(['account' => $this->first_client_account]);
                $firstClientId = !empty($clientBank) ? $clientBank->client_id : null;
            }            

            if(isset($firstClientId)){
                if ($this->direction == 'C') {
                    $agreementList
                        ->andWhere(['agreement.first_client_id' => $firstClientId])
                        ->andFilterWhere(['or',
                            ['agreement.second_client_id' => !empty($this->second_client_id) ? $this->second_client_id : null],
                            ['agreement.third_client_id' => !empty($this->second_client_id) ? $this->second_client_id : null],
                        ]);                    
                } else {
                    $agreementList
                        ->andWhere(['or',
                            ['agreement.second_client_id' => $firstClientId],
                            ['agreement.third_client_id' => $firstClientId],
                        ])                            
                        ->andFilterWhere(['agreement.first_client_id' => !empty($this->second_client_id) ? $this->second_client_id : null]);
                }
            }else{
                $paymentConfirmModel = $this->paymentConfirm;
                if(isset($paymentConfirmModel)){
                    $agreementList->andWhere(['or',
                        ['agreement.first_client_id' => $paymentConfirmModel->client_id],
                        ['agreement.second_client_id' => $paymentConfirmModel->client_id],
                        ['agreement.third_client_id' => $paymentConfirmModel->client_id],
                    ]);
                }
            }
        }elseif(isset($paymentConfirmModel)){
            $agreementList->andWhere(['or',
                ['agreement.first_client_id' => $paymentConfirmModel->client_id],
                ['agreement.second_client_id' => $paymentConfirmModel->client_id],
                ['agreement.third_client_id' => $paymentConfirmModel->client_id],
            ]);
        }

        $agreementList = $agreementList
            ->orderBy('agreement.number, fp.name, sp.name')
            ->all();

        if($asArray){
            $resultList = [];
            foreach ($agreementList as $key => $agreement) {
                $resultList[$agreement->id] = $agreement->number_concat;
            }      

            return $resultList;
        }else{
            return $agreementList;
        }        
    }    
    
    public function getAvailableExpenseList($asArray = true)
    {
        $expenseList = Expense::find(true)
            ->select([
                'id' => 'expense.id',
                'doc_number' => 'expense.doc_number',
                'doc_number_concat' => 'CONCAT(expense.doc_number, " | ",  fp.name, " -> ",  sp.name)',
            ])
            ->leftJoin(['fp' => 'client'], 'fp.id = expense.first_client_id')
            ->leftJoin(['sp' => 'client'], 'sp.id = expense.second_client_id')
            ->andWhere(['expense.pay_type' => [
                Expense::PAY_TYPE_CARD,
                Expense::PAY_TYPE_BANK,
            ]]);

        if(!empty($this->payment_confirm_id) || !empty ($_GET['payment_confirm_id'])){
            $paymentConfirmId = !empty($this->payment_confirm_id) ? $this->payment_confirm_id : $_GET['payment_confirm_id'];
            $paymentConfirmModel = PaymentConfirm::findOne($paymentConfirmId);
        }
        
        if(isset($this->id)){
            if(isset($paymentConfirmModel)){
                $firstClientId = $paymentConfirmModel->client_id;
            }else{
                $clientBank = ClientBank::findOne(['account' => $this->first_client_account]);
                $firstClientId = !empty($clientBank) ? $clientBank->client_id : null;
            }            

            if(isset($firstClientId)){
//                if ($this->direction == 'C') {
                    $expenseList
                        ->andWhere(['expense.second_client_id' => $firstClientId])
                        ->andFilterWhere(['expense.first_client_id' => !empty($this->second_client_id) ? $this->second_client_id : null]);
/*                    
                } else {
                    $expenseList
                        ->andWhere(['expense.first_client_id' => $firstClientId])
                        ->andFilterWhere(['expense.second_client_id' => !empty($this->second_client_id) ? $this->second_client_id : null]);
                }
 * 
 */
            }else{
                $paymentConfirmModel = $this->paymentConfirm;
                if(isset($paymentConfirmModel)){
                    $expenseList->andWhere(['expense.second_client_id' => $paymentConfirmModel->client_id]);
                    /*
                    $expenseList->andWhere(['or',
                        ['expense.first_client_id' => $paymentConfirmModel->client_id],
                        ['expense.second_client_id' => $paymentConfirmModel->client_id]
                    ]);
                     * 
                     */
                }
            }
        }elseif(isset($paymentConfirmModel)){
            $expenseList->andWhere(['expense.second_client_id' => $paymentConfirmModel->client_id]);
            /*
            $expenseList->andWhere(['or',
                ['expense.first_client_id' => $paymentConfirmModel->client_id],
                ['expense.second_client_id' => $paymentConfirmModel->client_id]
            ]);
             * 
             */
        }

        $expenseList = $expenseList
            ->orderBy('expense.doc_number, fp.name, sp.name')
            ->all();

        if($asArray){
            $resultList = [];
            foreach ($expenseList as $key => $expense) {
                $resultList[$expense->id] = $expense->doc_number_concat;
            }      

            return $resultList;
        }else{
            return $expenseList;
        }
    }    
    
    public function getAvailableDirectExpenseList($asArray = true)
    {
        if($asArray){
            return ExpenseType::getNameArr();
        }else{
            return ExpenseType::find()->orderBy('name')->all();
        }
    }    
    
    
    public function beforeSave($insert) 
    {
        $this->doc_date = date('Y-m-d', strtotime($this->doc_date));
        $this->currency = !empty($this->currency) ? $this->currency : (!empty($this->valuta_id) ? $this->valuta->name : '');
        $this->valuta_id = !empty($this->valuta_id) ? $this->valuta_id : (!empty($this->currency) ? Valuta::findOne(['name' => $this->currency])->id : null);
        
        if(!parent::beforeSave($insert)){
            return;
        }
        return true;
    }    
}