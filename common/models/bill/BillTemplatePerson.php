<?php

namespace common\models\bill;

use Yii;

use common\models\bill\BillTemplate;
use common\models\client\ClientContact;

/**
 * This is the model class for table "bill_template_person".
 *
 * @property integer $id
 * @property integer $bill_template_id
 * @property integer $client_person_id
 * @property integer $person_order
 *
 * @property BillTemplate $billTemplate
 * @property ClientContact $clientPerson
 */
class BillTemplatePerson extends \common\models\mainclass\FSMBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bill_template_person';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bill_template_id'], 'required'],
            [['id', 'bill_template_id', 'client_person_id', 'person_order'], 'integer'],
            [['bill_template_id'], 'exist', 'skipOnError' => true, 'targetClass' => BillTemplate::class, 'targetAttribute' => ['bill_template_id' => 'id']],
            [['client_person_id'], 'exist', 'skipOnError' => true, 'targetClass' => ClientContact::class, 'targetAttribute' => ['client_person_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('app', 'Template person|Template persons', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bill_template_id' => 'Invoice template',
            'client_person_id' => 'First party signing person',
            'person_order' => 'Person order',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillTemplate()
    {
        return $this->hasOne(BillTemplate::class, ['id' => 'bill_template_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientPerson()
    {
        return $this->hasOne(ClientContact::class, ['id' => 'client_person_id']);
    }

}