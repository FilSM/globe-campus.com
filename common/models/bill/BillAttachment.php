<?php

namespace common\models\bill;

use Yii;

use common\models\bill\Bill;
use common\models\Files;

/**
 * This is the model class for table "bill_attachment".
 *
 * @property integer $id
 * @property integer $bill_id
 * @property integer $uploaded_file_id
 *
 * @property Bill $bill
 * @property Files $uploadedFile
 */
class BillAttachment extends \common\models\mainclass\FSMBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bill_attachment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bill_id', 'uploaded_file_id'], 'required'],
            [['bill_id', 'uploaded_file_id'], 'integer'],
            [['bill_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bill::class, 'targetAttribute' => ['bill_id' => 'id']],
            [['uploaded_file_id'], 'exist', 'skipOnError' => true, 'targetClass' => Files::class, 'targetAttribute' => ['uploaded_file_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('client', 'Bill attachment|Bill attachments', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'bill_id' => Yii::t('bill', 'Bill'),
            'uploaded_file_id' => Yii::t('common', 'Attachment'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBill()
    {
        return $this->hasOne(Bill::class, ['id' => 'bill_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadedFile()
    {
        return $this->hasOne(Files::class, ['id' => 'uploaded_file_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function beforeDelete()
    {
        $file = $this->uploadedFile;
        if($file->delete()){
            return parent::beforeDelete();
        }
        return false;
    }    
}