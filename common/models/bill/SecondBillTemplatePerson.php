<?php

namespace common\models\bill;

/**
 * This is the model class for table "bill_template_person".
 *
 * @property integer $id
 * @property integer $bill_template_id
 * @property integer $client_person_id
 * @property integer $person_order
 *
 * @property BillTemplate $billTemplate
 * @property ClientContact $clientPerson
 */
class SecondBillTemplatePerson extends BillTemplatePerson
{
    
}