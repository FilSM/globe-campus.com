<?php

namespace common\models\bill;

/**
 * This is the model class for table "bill_person".
 *
 * @property integer $id
 * @property integer $bill_id
 * @property integer $client_person_id
 * @property integer $person_order
 *
 * @property BillTemplate $billTemplate
 * @property ClientContact $clientPerson
 */
class SecondBillPerson extends BillPerson
{
    
}