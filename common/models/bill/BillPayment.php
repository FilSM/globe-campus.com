<?php

namespace common\models\bill;

use Yii;
use yii\helpers\ArrayHelper;

use common\models\Bank;
use common\models\bill\PaymentOrder;
use common\models\bill\BillHistory;
use common\models\Valuta;
use common\models\user\FSMUser;
use common\components\FSMHelper;
use common\components\FSMAccessHelper;

/**
 * This is the model class for table "bill_payment".
 *
 * @property integer $id
 * @property intgere $bill_history_id
 * @property intgere $payment_order_id
 * @property string $paid_date
 * @property intgere $from_bank_id
 * @property intgere $to_bank_id
 * @property integer $bill_id
 * @property string $summa
 * @property integer $valuta_id
 * @property string $rate
 * @property string $summa_eur
 * @property string $summa_origin
 * @property string $confirmed
 * @property integer $manual_input
 * @property integer $combine
 * @property integer $need_agreement
 *
 * @property Bill $bill
 * @property BillHistory $BillHistory
 * @property PaymentOrder $paymentOrder
 * @property Valuta $valuta
 */
class BillPayment extends BillHistory
{
    
    public static function find($withScope = false) {
        if ((Yii::$app->id != 'app-console') && $withScope) {
            $scope = new \common\scopes\BillPaymentScopeQuery(get_called_class());
            return $scope->onlyAuthor();            
        }
        return parent::find();
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bill_payment';
    }
    
    public function attributes($withExternal = true) { 
        $parentClass = get_parent_class($this);
        $parent = new $parentClass;
        $attributes = $parent->attributes();
        $attributes = ArrayHelper::merge(
            $attributes,
            [
                'bill_history_id',
                'payment_order_id',
                'payment_order_number',
                'paid_date',
                'bill_id',
                'from_bank_id',
                'to_bank_id',
                'from_bank_name',
                'to_bank_name',        
                'summa',
                'valuta_id',
                'rate', 
                'summa_eur',
                'summa_origin',
                'confirmed',
                'manual_input',
                'combine',
                'need_agreement',
                
                'first_client_id',
                'first_client_name',
            ]
        );
        return $attributes; 
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules = ArrayHelper::merge(
            $rules, 
            [
                [['bill_history_id', 'bill_id', 'summa', 'valuta_id'], 'required'],
                [['bill_history_id', 'payment_order_id', 'from_bank_id', 'to_bank_id', 
                    'bill_id', 'valuta_id', 'manual_input', 'combine', 'need_agreement'], 'integer'],
                ['payment_order_id', 'required', 'when' => function($model) {
                    return empty($model->paid_date);
                }],
                ['paid_date', 'required', 'when' => function($model) {
                    return empty($model->payment_order_id);
                }],
                /*
                ['from_bank_id', 'required', 'when' => function($model) {
                    return empty($model->payment_order_id);
                }],
                 * 
                 */
                /*
                ['to_bank_id', 'required', 'when' => function($model) {
                    return empty($model->payment_order_id);
                }],
                * 
                */
                [['summa', 'rate', 'summa_eur', 'summa_origin'], 'number'],
                [['summa'], 'validateRate'],
                [['paid_date', 'confirmed'], 'safe'],
                [['bill_history_id'], 'exist', 'skipOnError' => true, 'targetClass' => BillHistory::class, 'targetAttribute' => ['bill_history_id' => 'id']],
                [['payment_order_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentOrder::class, 'targetAttribute' => ['payment_order_id' => 'id']],
                [['from_bank_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bank::class, 'targetAttribute' => ['from_bank_id' => 'id']],
                [['to_bank_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bank::class, 'targetAttribute' => ['to_bank_id' => 'id']],
                [['bill_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bill::class, 'targetAttribute' => ['bill_id' => 'id']],
                [['valuta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Valuta::class, 'targetAttribute' => ['valuta_id' => 'id']],
            ]
        );
        return $rules;
    }
    
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['many'] = $scenarios['default'];
        foreach ($scenarios['many'] as $key => $value) {
            if(in_array($value, [
                'bill_history_id',
                'payment_order_id',
                'paid_date',
            ])){
                unset($scenarios['many'][$key]);       
            }
        }
        return $scenarios;
    }
    
    public function validateRate($attribute, $params, $validator)
    {
        $rate = (float)$this->rate;
        $valuta_id = (int)$this->valuta_id;
        if (!empty($valuta_id) && (
                (($rate == 1) && ($valuta_id != Valuta::VALUTA_DEFAULT)) ||
                (($rate != 1) && ($valuta_id == Valuta::VALUTA_DEFAULT))
            )) {
            $message = Yii::t('bill', 'The currency rate is not correct.');
            $validator->addError($this, 'summa', $message);
        }        
    }
    
    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true)
    {
        return parent::label('bill', 'Invoice payment|Invoice payments', $n, $translate);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels = ArrayHelper::merge(
            $labels, 
            [
                'id' => Yii::t('common', 'ID'),
                'bill_history_id' => Yii::t('bill', 'History action'),
                'payment_order_id' => Yii::t('bill', 'Payment order'),
                'paid_date' => Yii::t('bill', 'Paid'),
                'from_bank_id' => Yii::t('bill', 'Bank from'),
                'to_bank_id' => Yii::t('bill', 'Bank to'),
                'bill_id' => Yii::t('bill', 'Invoice number'),
                'summa' => Yii::t('common', 'Sum'),
                'valuta_id' => Yii::t('common', 'Currenсy'),
                'rate' => Yii::t('common', 'Rate'),
                'summa_eur' => Yii::t('common', 'Sum').' €',
                'summa_origin' => Yii::t('bill', 'Sum by invoice rate'),
                'confirmed' => Yii::t('common', 'Confirmed'),
                'manual_input' => Yii::t('common', 'Manual input'),
                'combine' => Yii::t('bill', 'Combine'),
                'need_agreement' => Yii::t('bill', 'Show agreement'),
            
                'order_number' => Yii::t('bill', 'Payment order number'),
                'bill_number' => Yii::t('bill', 'Invoice number'),
                'from_bank_name' => Yii::t('bill', 'Bank from'),
                'to_bank_name' => Yii::t('bill', 'Bank to'),
                'first_client_id' => Yii::t('bill', 'First party'),
                'first_client_name' => Yii::t('bill', 'First party'),
            ]
        );
        return $labels;
    }

    protected function getIgnoredFieldsForDelete() {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, ['payment_order_id', 'from_bank_id', 'to_bank_id', 
                'bill_id', 'valuta_id']
        );
        return $fields;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBill()
    {
        return $this->hasOne(Bill::class, ['id' => 'bill_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillHistory()
    {
        return $this->hasOne(BillHistory::class, ['id' => 'bill_history_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentOrder()
    {
        return $this->hasOne(PaymentOrder::class, ['id' => 'payment_order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankFrom()
    {
        return $this->hasOne(Bank::class, ['id' => 'from_bank_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankTo()
    {
        return $this->hasOne(Bank::class, ['id' => 'to_bank_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValuta() {
        return $this->hasOne(Valuta::class, ['id' => 'valuta_id']);
    }

    public function canUpdate() {
        if(FSMUser::getIsPortalAdmin() || FSMAccessHelper::can('updateBillPayment')){
            return true;
        }        
        $paymentOrder = $this->paymentOrder;
        if(!empty($paymentOrder) && ($paymentOrder->status == PaymentOrder::EXPORT_STATE_SENT)){
            return false;
        }
        $bill = $this->bill;
        if(!empty($bill) && 
            in_array($bill->status, [Bill::BILL_STATUS_COMPLETE,]) &&
            !empty(Yii::$app->params['COMPLETE_BLOCKED'])
        ){
            return false;
        }
        return true;
        //return empty($this->confirmed);
    }

    public function canDelete() {
        return $this->canUpdate();
    }
    
    static function getButtonConfirm($params)
    {
        extract($params);
        if(!$model->canUpdate()){
            return null;
        }
        if($model->confirmed){
            return null;
        }
        
        $data = [
            'title' => Yii::t('bill', 'Confirm payment'),
            'controller' => 'bill-payment',
            'action' => 'ajax-confirm-payment',
            'icon' => 'ok',
            'modal' => true,
        ];        
        if (!empty($isBtn)) {
            $result = FSMHelper::vButton($model->id, 
                ArrayHelper::merge($data, [
                    'class' => 'success',
                    'size' => 'btn-xs',
                ])
            );
        } else {
            $result = FSMHelper::vDropdown($model->id, 
                ArrayHelper::merge($data, [
                    'label' => Yii::t('bill', 'Confirm payment'),
                ])
            );
        }
        return $result;        
    } 
    
    static function getButtonUpdate($params)
    {
        extract($params);
        if(!$model->canUpdate()){
            return null;
        }
        $data = [
            'title' => Yii::t('kvgrid', 'Update'),
            'controller' => 'bill-payment',
            'action' => 'ajax-update',
            'icon' => 'pencil',
            'modal' => true,
        ];        
        if (!empty($isBtn)) {
            $result = FSMHelper::vButton($model->id, 
                ArrayHelper::merge($data, [
                    'class' => 'primary',
                    'size' => 'btn-xs',
                ])
            );
        } else {
            $result = FSMHelper::vDropdown($model->id, 
                ArrayHelper::merge($data, [
                    'label' => Yii::t('kvgrid', 'Update'),
                ])
            );
        }
        return $result;        
    }    
    
    public function beforeSave($insert) 
    {
        $billModel = $this->bill;
        $this->rate = ($this->valuta_id == Valuta::VALUTA_DEFAULT) ? 1 : $this->rate;
        $this->summa_eur = $this->summa / $this->rate;
        $this->summa_origin = ($this->valuta_id == $billModel->valuta_id) ? $this->summa : $this->summa / $billModel->rate;
        $this->paid_date = (!empty($this->paid_date) ? date('Y-m-d', strtotime($this->paid_date)) : null);
        $this->confirmed = (!empty($this->confirmed) ? date('Y-m-d', strtotime($this->confirmed)) : null);
        if(!parent::beforeSave($insert)){
            return;
        }
        return true;
    }   
    
    public function afterDelete() 
    {
        parent::afterDelete();
        
        $billModel = $this->bill;
        $paymentList = $billModel->billPayments;
        if(empty($paymentList)){
            $billModel->changeStatus(Bill::BILL_STATUS_PREP_PAYMENT, ['paid_date' => null]);
        }else{
            $payment = end($paymentList);
            $billModel->changeStatus(Bill::BILL_STATUS_PAID, ['paid_date' => $payment->paid_date]);
        }
    }
}
