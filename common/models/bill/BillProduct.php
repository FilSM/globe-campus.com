<?php

namespace common\models\bill;

use Yii;

use common\models\bill\Bill;
use common\models\Product;
use common\models\Measure;

/**
 * This is the model class for table "bill_product".
 *
 * @property integer $id
 * @property integer $deleted
 * @property integer $bill_id
 * @property integer $product_id
 * @property string $product_name
 * @property integer $measure_id
 * @property string $amount
 * @property string $price
 * @property string $vat
 * @property integer $revers
 * @property string $summa
 * @property string $summa_vat
 * @property string $total
 * @property string $price_eur
 * @property string $summa_eur
 * @property string $summa_vat_eur
 * @property string $total_eur
 * @property string $comment
 *
 * @property Bill $bill
 * @property Product $product
 * @property Measure $measure
 */
class BillProduct extends \common\models\mainclass\FSMBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bill_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bill_id'], 'required'],
            ['product_id', 'required', 'when' => function($model) {return empty(trim($model->product_name));}],
            ['product_name', 'required', 'when' => function($model) {return empty($model->product_id);}],
            [['id', 'deleted', 'bill_id', 'product_id', 'measure_id', 'revers'], 'integer'],
            [['amount', 'price', 'vat', 'summa', 'summa_vat', 'total', 
                'price_eur', 'summa_eur', 'summa_vat_eur', 'total_eur'], 'number'],
            [['product_name'], 'string', 'max' => 100],
            [['comment'], 'string', 'max' => 255],
            [['bill_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bill::class, 'targetAttribute' => ['bill_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['product_id' => 'id']],
            [['measure_id'], 'exist', 'skipOnError' => true, 'targetClass' => Measure::class, 'targetAttribute' => ['measure_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('bill', 'Invoice product|Invoice products', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'deleted' => Yii::t('common', 'Deleted'),
            'bill_id' => Yii::t('bill', 'Invoice'),
            'product_id' => Yii::t('bill', 'Product'),
            'product_name' => Yii::t('bill', 'Product'),
            'measure_id' => Yii::t('product', 'Measure'),
            'amount' => Yii::t('common', 'Amount'),
            'price' => Yii::t('common', 'Price'),
            'vat' => Yii::t('common', 'VAT').' %',
            'revers' => Yii::t('bill', 'Reverse'),
            'summa' => Yii::t('common', 'Sum'),
            'summa_vat' => Yii::t('common', 'VAT'),
            'total' => Yii::t('common', 'Total'),
            'price_eur' => Yii::t('common', 'Price'),
            'summa_eur' => Yii::t('common', 'Sum').' €',
            'summa_vat_eur' => Yii::t('common', 'VAT').' €',
            'total_eur' => Yii::t('common', 'Total').' €',
            'comment' => Yii::t('common', 'Comment'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBill()
    {
        return $this->hasOne(Bill::class, ['id' => 'bill_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }
    
    public function getProductName()
    {
        return !empty($this->product_name) ? $this->product_name : (isset($this->product) ? (!empty($this->product->prinded) ? $this->product->name : '') : '');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeasure()
    {
        return $this->hasOne(Measure::class, ['id' => 'measure_id']);
    }    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeasureName()
    {
        return !empty($this->measure_id) ? ($this->measure ? $this->measure->name : '') : (isset($this->product, $this->product->measure) ? $this->product->measure->name : '');
    }   
    
    public function beforeSave($insert) 
    {
        $bill = $this->bill;
        if($bill->doc_type == Bill::BILL_DOC_TYPE_CRBILL){
            $this->summa = $this->summa * -1;
            $this->summa_vat = $this->summa_vat * -1;
            $this->total = $this->total * -1;
            $this->summa_eur = $this->summa_eur * -1;
            $this->summa_vat_eur = $this->summa_vat_eur * -1;
            $this->total_eur = $this->total_eur * -1;            
        }        
        
        if(!parent::beforeSave($insert)){
            return;
        }
        return true;
    }    
}