<?php

namespace common\models\bill;

use Yii;
use yii\base\Exception;

use common\models\user\FSMUser;
use common\models\user\FSMProfile;

/**
 * This is the model class for table "bill_history".
 *
 * @property integer $id
 * @property integer $bill_id
 * @property integer $action_id
 * @property string $comment
 * @property string $create_time
 * @property integer $create_user_id
 *
 * @property Bill $bill
 * @property FSMUser $createUser
 */
class BillHistory extends \common\models\mainclass\FSMCreateModel
{
    const BillHistory_ACTIONS = [
        'ACTION_CREATE'                     => 1,
        'ACTION_EDIT'                       => 2,
        'ACTION_DELETE'                     => 3,
        'ACTION_STATUS_UP_NEW'              => 4,
        'ACTION_STATUS_UP_READY'            => 5,
        'ACTION_STATUS_UP_SIGNED'           => 6,
        'ACTION_STATUS_UP_PREP_PAYMENT'     => 7,
        'ACTION_STATUS_UP_PAYMENT'          => 8,
        'ACTION_STATUS_UP_PAID'             => 9,
        'ACTION_STATUS_UP_COMPLETE'         => 10,
        'ACTION_STATUS_UP_CANCELED'         => 11,
        'ACTION_STATUS_DOWN_PREPAR'         => 12,
        'ACTION_STATUS_DOWN_NEW'            => 13,
        'ACTION_STATUS_DOWN_READY'          => 14,
        'ACTION_STATUS_DOWN_SIGNED'         => 15,
        'ACTION_STATUS_DOWN_PREP_PAYMENT'   => 16,
        'ACTION_STATUS_DOWN_PAYMENT'        => 17,
        'ACTION_SENT_EMAIL_CLIENT'          => 18,
        'ACTION_SENT_EMAIL_AGENT'           => 19,
        'ACTION_BLOCK'                      => 20,
        'ACTION_UNBLOCK'                    => 21,
        'ACTION_PAYMENT'                    => 22,
        'ACTION_EDIT_PAYMENT'               => 23,
        'ACTION_DELETE_PAYMENT'             => 24,
    ];
    const BillHistory_ACTIONS_DOWN = [
        'ACTION_STATUS_DOWN_PREPAR'         => 12,
        'ACTION_STATUS_DOWN_NEW'            => 13,
        'ACTION_STATUS_DOWN_READY'          => 14,
        'ACTION_STATUS_DOWN_SIGNED'         => 15,
        'ACTION_STATUS_DOWN_PREP_PAYMENT'   => 16,
        'ACTION_STATUS_DOWN_PAYMENT'        => 17,
    ];
    
    protected $_externalFields = [
        'bill_number',
        'user_name',
    ]; 
    
    public static function getLastDownAction($bill_id)
    {
        $historyList = BillHistory::find()
            ->where(['bill_id' => $bill_id])
            ->andWhere([
                'action_id' => BillHistory::BillHistory_ACTIONS_DOWN,
            ])
            ->addOrderBy('id desc')
            ->limit('1')
            ->all();
        return !empty($historyList[0]) ? $historyList[0] : null;
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bill_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bill_id', 'action_id', 'create_user_id'], 'integer'],
            [['comment'], 'string'],
            [['create_time'], 'safe'],
            [['bill_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bill::class, 'targetAttribute' => ['bill_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['create_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('bill', 'Invoice history|Invoices history', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'bill_id' => Yii::t('bill', 'Invoice number'),
            'action_id' => Yii::t('common', 'Action'),
            'comment' => Yii::t('common', 'Comment'),
            'create_time' => Yii::t('common', 'Action time'),
            'create_user_id' => Yii::t('common', 'Performer'),
            
            'bill_number' => Yii::t('bill', 'Invoice number'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBill()
    {
        return $this->hasOne(Bill::class, ['id' => 'bill_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'create_user_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUserProfile()
    {
        return $this->hasOne(FSMProfile::class, ['user_id' => 'create_user_id']);
    }    
    
    static public function getBillActionList() {
        return [
            BillHistory::BillHistory_ACTIONS['ACTION_CREATE'] => Yii::t('action', 'Creation'),
            BillHistory::BillHistory_ACTIONS['ACTION_EDIT'] => Yii::t('action', 'Editing'),
            BillHistory::BillHistory_ACTIONS['ACTION_DELETE'] => Yii::t('action', 'Deleting'),
            BillHistory::BillHistory_ACTIONS['ACTION_STATUS_UP_NEW'] => Yii::t('action', 'Change status to "New"'),
            BillHistory::BillHistory_ACTIONS['ACTION_STATUS_UP_READY'] => Yii::t('action', 'Change status to "Ready"'),
            BillHistory::BillHistory_ACTIONS['ACTION_STATUS_UP_SIGNED'] => Yii::t('action', 'Change status to "Signed"'),
            BillHistory::BillHistory_ACTIONS['ACTION_STATUS_UP_PREP_PAYMENT'] => Yii::t('action', 'Change status to "Payment preparation"'),
            BillHistory::BillHistory_ACTIONS['ACTION_STATUS_UP_PAYMENT'] => Yii::t('action', 'Change status to "Payment"'),
            BillHistory::BillHistory_ACTIONS['ACTION_STATUS_UP_PAID'] => Yii::t('action', 'Payment confirmation'),
            BillHistory::BillHistory_ACTIONS['ACTION_STATUS_UP_COMPLETE'] => Yii::t('action', 'Change status to "Complete"'),
            BillHistory::BillHistory_ACTIONS['ACTION_STATUS_UP_CANCELED'] => Yii::t('action', 'Change status to "Canceled"'),
            BillHistory::BillHistory_ACTIONS['ACTION_STATUS_DOWN_PREPAR'] => Yii::t('action', 'Rollback status to "Preparing"'),
            BillHistory::BillHistory_ACTIONS['ACTION_STATUS_DOWN_NEW'] => Yii::t('action', 'Rollback status to "New"'),
            BillHistory::BillHistory_ACTIONS['ACTION_STATUS_DOWN_READY'] => Yii::t('action', 'Rollback status to "Ready"'),
            BillHistory::BillHistory_ACTIONS['ACTION_STATUS_DOWN_SIGNED'] => Yii::t('action', 'Rollback status to "Signed"'),
            BillHistory::BillHistory_ACTIONS['ACTION_STATUS_DOWN_PAYMENT'] => Yii::t('action', 'Rollback status to "Payment"'),
            BillHistory::BillHistory_ACTIONS['ACTION_STATUS_DOWN_PREP_PAYMENT'] => Yii::t('action', 'Rollback status to "Payment preparation"'),
            BillHistory::BillHistory_ACTIONS['ACTION_SENT_EMAIL_CLIENT'] => Yii::t('bill', 'Send invoice to client as attachment of email'),
            BillHistory::BillHistory_ACTIONS['ACTION_SENT_EMAIL_AGENT'] => Yii::t('bill', 'Send invoice to agent as attachment of email'),
            BillHistory::BillHistory_ACTIONS['ACTION_BLOCK'] => Yii::t('action', 'Blocked'),
            BillHistory::BillHistory_ACTIONS['ACTION_UNBLOCK'] => Yii::t('action', 'Unblocked'),
            BillHistory::BillHistory_ACTIONS['ACTION_PAYMENT'] => Yii::t('bill', 'Payment'),
            BillHistory::BillHistory_ACTIONS['ACTION_EDIT_PAYMENT'] => Yii::t('bill', 'Edit payment'),
            BillHistory::BillHistory_ACTIONS['ACTION_DELETE_PAYMENT'] => Yii::t('bill', 'Delete payment'),
        ];
    }    
    
    public function saveHistory($bill_id, $action_id, $comment = null)
    {
        $this->bill_id = !empty($bill_id) ? $bill_id : null;
        $this->action_id = $action_id;
        $this->comment = !empty($comment) ? $comment : $this->comment;
        $this->create_time = date('d-m-Y H:i:s');
        $this->create_user_id = Yii::$app->user->id;
        
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            if (!$this->save()) {
                throw new Exception('Unable to save data! '.$model->errorMessage);
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $transaction->rollBack();
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
            return false;
        }
        return true;
    }
}