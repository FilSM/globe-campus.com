<?php

namespace common\models\bill\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\bill\TaxPayment;

/**
 * TaxPaymentSearch represents the model behind the search form of `\common\models\bill\TaxPayment`.
 */
class TaxPaymentSearch extends TaxPayment
{
    public $from_client_name;
    public $to_client_name;
            
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'abonent_id', 'from_client_id', 'to_client_id', 'valuta_id', 'manual_input'], 'integer'],
            [['paid_date', 'confirmed', 'from_client_name', 'to_client_name', 'comment'], 'safe'],
            [['summa', 'rate', 'summa_eur'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
    
        $query = TaxPayment::find(true);
        $query->select = [
            $baseTableName . '.*',
            'from_client_name' => 'from_client.name',
            'to_client_name' => 'to_client.name',
        ];
        $query->leftJoin(['from_client' => 'client'], 'from_client.id = '.$baseTableName.'.from_client_id');
        $query->leftJoin(['to_client' => 'client'], 'to_client.id = '.$baseTableName.'.to_client_id');
        
        if(!isset($params['sort'])){
            $query->addOrderBy('id desc');
        }
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if($this->hasAttribute('version')){
            $this->__unset('version');
        }

        // grid filtering conditions
        $query->andFilterWhere([
            $baseTableName.'.id' => $this->id,
            $baseTableName.'.abonent_id' => $this->abonent_id,
            $baseTableName.'.paid_date' => !empty($this->doc_date) ? date('Y-m-d', strtotime($this->doc_date)) : null,
            $baseTableName.'.from_client_id' => $this->from_client_id,
            $baseTableName.'.to_client_id' => $this->to_client_id,
            $baseTableName.'.summa' => $this->summa,
            $baseTableName.'.valuta_id' => $this->valuta_id,
            $baseTableName.'.rate' => $this->rate,
            $baseTableName.'.summa_eur' => $this->summa_eur,
            $baseTableName.'.confirmed' => !empty($this->confirmed) ? date('Y-m-d', strtotime($this->confirmed)) : null,
            $baseTableName.'.manual_input' => $this->manual_input,
        ]);

        $query->andFilterWhere(['like', $baseTableName.'.comment', $this->comment]);

        return $dataProvider;
    }
}