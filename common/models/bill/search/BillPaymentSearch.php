<?php

namespace common\models\bill\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\bill\BillPayment;

/**
 * BillPaymentSearch represents the model behind the search form of `common\models\bill\BillPayment`.
 */
class BillPaymentSearch extends BillPayment
{
    public $payment_order_number;
    public $bill_number;
    public $from_bank_name;
    public $to_bank_name;
    public $paid_date_range;
    public $confirmed_range;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'bill_history_id', 'payment_order_id', 'from_bank_id', 'to_bank_id', 
                'bill_id', 'valuta_id', 'manual_input', 'combine', 'need_agreement'], 'integer'],
            [['payment_order_number', 'bill_number', 'from_bank_name', 'to_bank_name', 
                'paid_date', 'confirmed', 'paid_date_range', 'confirmed_range'], 'safe'],
            [['summa', 'rate', 'summa_eur', 'summa_origin'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
    
        $query = BillPayment::find(true);
        $query->select = [
            $baseTableName . '.*',
            'payment_order_number' => 'payment_order.number',
            'bill_number' => 'bill.doc_number',
            'from_bank_name' => 'from_bank.name',
            'to_bank_name' => 'to_bank.name',
        ];
        $query->leftJoin(['payment_order' => 'payment_order'], 'payment_order.id = '.$baseTableName.'.payment_order_id');
        $query->leftJoin(['bill' => 'bill'], 'bill.id = '.$baseTableName.'.bill_id');
        $query->leftJoin(['from_bank' => 'bank'], 'from_bank.id = '.$baseTableName.'.from_bank_id');
        $query->leftJoin(['to_bank' => 'bank'], 'to_bank.id = '.$baseTableName.'.to_bank_id');
        $query->leftJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id');
        $query->andWhere(['ab.abonent_id' => $this->userAbonentId]);
        
        if(isset($params['bill_id']) && !isset($params['sort'])){
            $query->addOrderBy($baseTableName.'.paid_date');
        }elseif(!isset($params['sort'])){
            $query->addOrderBy($baseTableName.'.id '. (!in_array('bill_id', $params) && !in_array('payment_order_id', $params) ? 'desc' : ''));
        }
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => empty($params['payment_order_id']) ? 20 : 0,
            ],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if(!empty($this->paid_date) && strpos($this->paid_date, '-') !== false) { 
            $this->paid_date_range = $this->paid_date;
            list($start_date, $end_date) = explode(' - ', $this->paid_date_range);
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));
            $query->andFilterWhere(['between', $baseTableName . '.paid_date', $start_date, $end_date.' 23:59:59']);
        }

        if(!empty($this->confirmed) && strpos($this->confirmed, '-') !== false) { 
            $this->confirmed_range = $this->confirmed;
            list($start_date, $end_date) = explode(' - ', $this->confirmed_range);
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));
            $query->andFilterWhere(['between', $baseTableName . '.confirmed', $start_date, $end_date.' 23:59:59']);
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            $baseTableName.'.id' => $this->id,
            $baseTableName.'.bill_history_id' => $this->bill_history_id,
            $baseTableName.'.payment_order_id' => $this->payment_order_id,
            $baseTableName.'.paid_date' => !empty($this->paid_date) && empty($this->paid_date_range) ? date('Y-m-d', strtotime($this->paid_date)) : null,
            $baseTableName.'.from_bank_id' => $this->from_bank_id,
            $baseTableName.'.to_bank_id' => $this->to_bank_id,
            $baseTableName.'.bill_id' => $this->bill_id,
            $baseTableName.'.valuta_id' => $this->valuta_id,
            $baseTableName.'.summa' => $this->summa,
            $baseTableName.'.summa_eur' => $this->summa_eur,
            $baseTableName.'.summa_origin' => $this->summa_origin,
            $baseTableName.'.confirmed' => !empty($this->confirmed) && empty($this->confirmed_range) ? date('Y-m-d', strtotime($this->confirmed)) : null,
            $baseTableName.'.manual_input' => $this->manual_input,
            $baseTableName.'.combine' => $this->combine,
            $baseTableName.'.need_agreement' => $this->need_agreement,
        ]);
        
        $query->andFilterWhere(['like', 'bill.doc_number', $this->bill_number])
            ->andFilterWhere(['like', 'payment_order.number', $this->payment_order_number]);
                
        return $dataProvider;
    }
}