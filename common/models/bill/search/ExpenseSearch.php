<?php

namespace common\models\bill\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\bill\Expense;
use yii\db\Query;

/**
 * ExpenseSearch represents the model behind the search form of `common\models\bill\Expense`.
 */
class ExpenseSearch extends Expense
{
    public $project_name;
    public $first_client_name;
    public $second_client_name;
    public $expense_type_name;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'abonent_id', 'expense_type_id', 'project_id', 'first_client_id', 'second_client_id', 
                'report_plus', 'manual_input', 'create_user_id', 'update_user_id'], 'integer'],
            [['doc_number', 'doc_date', 'confirmed', 'pay_type', 'comment', 
                'create_time', 'update_time',
                'project_name', 'first_client_name', 'second_client_name',
                'expense_type_name', 'valuta_id'], 'safe'],
            [['summa', 'vat', 'total'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    public function getIdsByProject($params)
    {
        $start_date = !empty($_GET['from']) ? date('Y-m-d', strtotime($_GET['from'])) : date('Y-01-01');
        $end_date = !empty($_GET['till']) ? date('Y-m-d', strtotime($_GET['till'])) : '2500-01-01';
        
        $query = (new Query());
        $query
            ->select([
                'id' => 'id',
            ])  
            ->from('expense')
            ->andWhere(['abonent_id' => $this->userAbonentId])
            ->andWhere(['!=', 'total', 0])
            ->andWhere(['between', 'doc_date', $start_date, $end_date.' 23:59:59']);
        
        if(!empty($params['project_id'])){
            $query->andWhere(['project_id' => $params['project_id']]);
        }

        /*
        SELECT 
            id AS `id` 
        FROM 
            `expense` 
        WHERE 
            (abonent_id = 1) AND
            (total != 0) AND
            (`doc_date` BETWEEN '2018-12-01' AND '2019-01-31 23:59:59') AND 
            (`project_id`= 57)
         * 
         */
        
        $billList = $query->all();
        $ids = [];
        foreach ($billList as $bill) {
            $ids[] = $bill['id'];
        }
        return $ids;        
    }

    public function getIdsByClient($params, $clientIds = [])
    {
        $start_date = !empty($_GET['from']) ? date('Y-m-d', strtotime($_GET['from'])) : date('Y-01-01');
        $end_date = !empty($_GET['till']) ? date('Y-m-d', strtotime($_GET['till'])) : '2500-01-01';
        
        $query = (new Query())
            ->select([
                'id' => 'expense.id',
            ]) 
            ->from('expense')
            ->andWhere(['expense.abonent_id' => $this->userAbonentId])
            ->leftJoin(['c' => 'client'], 'c.id = expense.second_client_id')
            ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = c.id AND ac.abonent_id = '.$this->userAbonentId)
            ->andWhere(['!=', 'expense.total', 0])
            ->andWhere(['not', ['ac.client_group_id' => null]])
            ->andWhere(['between', 'doc_date', $start_date, $end_date.' 23:59:59']);

        if(empty($params['plus'])){
            $query->andWhere(['expense.report_plus' => 0]);
        }
        
        if(!empty($clientIds)){
            $query->where(['or', 
                ['expense.first_client_id' => $clientIds], 
                ['expense.second_client_id' => $clientIds],
            ]);             
        }        
        
        if(!empty($params['client_id'])){
            $query->andWhere(['c.id' => $params['client_id']]);
        }
        
        /*
        SELECT 
            `expense`.`id` AS `id` 
        FROM 
            `expense` 
            LEFT JOIN `client` `c` ON c.id = expense.second_client_id 
            LEFT JOIN `abonent_client` `ac` ON ac.client_id = c.id AND ac.abonent_id = 1 
        WHERE 
            (`expense`.abonent_id = 1) AND
            (`expense`.`total` != 0) AND 
            (NOT (`ac`.`client_group_id` IS NULL)) AND 
            (`doc_date` BETWEEN '2018-01-01' AND '2019-01-31 23:59:59') AND 
            (`c`.`id`= 3)
         * 
         */
        
        $expenseList = $query->all();
        $ids = [];
        foreach ($expenseList as $expense) {
            $ids[] = $expense['id'];
        }
        return $ids;        
    }
    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
    
        $query = Expense::find(true);

        $query->select = [
            $baseTableName . '.*',
            'project_name' => 'project.name',
            'first_client_name' => 'first_client.name',
            'second_client_name' => 'second_client.name',
            'expense_type_name' => 'expense_type.name',
        ];
        
        $query->leftJoin(['project' => 'project'], 'project.id = '.$baseTableName.'.project_id');
        $query->leftJoin(['first_client' => 'client'], 'first_client.id = '.$baseTableName.'.first_client_id');
        $query->leftJoin(['second_client' => 'client'], 'second_client.id = '.$baseTableName.'.second_client_id');
        $query->leftJoin(['expense_type' => 'expense_type'], 'expense_type.id = '.$baseTableName.'.expense_type_id');
        
        if(!isset($params['sort'])){
            $query->addOrderBy('id desc');
        }

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => isset($_POST['expandRowKey']) || isset($params['expense_ids']) ? 0 : 50,
            ],
        ]);
        
        $idsArr = isset($params['expense_ids']) ? $params['expense_ids'] : null;
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
                
        if(!empty($this->doc_date) && strpos($this->doc_date, '-') !== false) { 
            $doc_date_range = $this->doc_date;
            list($start_date, $end_date) = explode(' - ', $doc_date_range);
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));
            $query->andFilterWhere(['between', $baseTableName . '.doc_date', $start_date, $end_date.' 23:59:59']);
        }
        
        // grid filtering conditions
        if(isset($idsArr)){
            $query->andWhere([$baseTableName.'.id' => $idsArr]);
        }        
        $query->andFilterWhere([
            $baseTableName.'.id' => $this->id,
            $baseTableName.'.abonent_id' => $this->abonent_id,
            $baseTableName.'.expense_type_id' => $this->expense_type_id,
            $baseTableName.'.project_id' => $this->project_id,
            $baseTableName.'.doc_date' => !empty($this->doc_date) && empty($doc_date_range) ? date('Y-m-d', strtotime($this->doc_date)) : null,
            $baseTableName.'.confirmed' => !empty($this->confirmed) ? date('Y-m-d', strtotime($this->confirmed)) : null,
            $baseTableName.'.pay_type' => $this->pay_type,
            $baseTableName.'.first_client_id' => $this->first_client_id,
            $baseTableName.'.second_client_id' => $this->second_client_id,
            $baseTableName.'.summa' => $this->summa,
            $baseTableName.'.vat' => $this->vat,
            $baseTableName.'.total' => $this->total,
            $baseTableName.'.valuta_id' => $this->valuta_id,
            $baseTableName.'.report_plus' => $this->report_plus,
            $baseTableName.'.manual_input' => $this->manual_input,
            $baseTableName.'.create_time' => $this->create_time,
            $baseTableName.'.create_user_id' => $this->create_user_id,
            $baseTableName.'.update_time' => $this->update_time,
            $baseTableName.'.update_user_id' => $this->update_user_id,
        ]);

        $query->andFilterWhere(['like', $baseTableName.'.doc_number', $this->doc_number])
            ->andFilterWhere(['like', $baseTableName.'.comment', $this->comment])
                
            ->andFilterWhere(['like', 'project.name', $this->project_name]);
              
        /*
        SELECT 
            `expense`.*,
            `project`.`name` AS `project_name`,
            `first_client`.`name` AS `first_client_name`,
            `second_client`.`name` AS `second_client_name`,
            `expense_type`.`name` AS `expense_type_name`
        FROM
            `expense`
                LEFT JOIN
            `project` `project` ON project.id = expense.project_id
                LEFT JOIN
            `client` `first_client` ON first_client.id = expense.first_client_id
                LEFT JOIN
            `client` `second_client` ON second_client.id = expense.second_client_id
                LEFT JOIN
            `expense_type` `expense_type` ON expense_type.id = expense.expense_type_id
        WHERE
            (`expense`.`abonent_id` = 1)
                AND (`expense`.`first_client_id` IN (1 , 11, 69))
        ORDER BY `doc_date` DESC , `doc_number` DESC
         * 
         */
        
        //return $query->all();
        return $dataProvider;
    }
    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchLast($params)
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
    
        $query = Expense::find(true);

        $query->select = [
            $baseTableName . '.*',
            'project_name' => 'project.name',
            'first_client_name' => 'first_client.name',
            'second_client_name' => 'second_client.name',
            'expense_type_name' => 'expense_type.name',
        ];
        
        $query->leftJoin(['project' => 'project'], 'project.id = '.$baseTableName.'.project_id');
        $query->leftJoin(['first_client' => 'client'], 'first_client.id = '.$baseTableName.'.first_client_id');
        $query->leftJoin(['second_client' => 'client'], 'second_client.id = '.$baseTableName.'.second_client_id');
        $query->leftJoin(['expense_type' => 'expense_type'], 'expense_type.id = '.$baseTableName.'.expense_type_id');
        
        if(!isset($params['sort'])){
            //$query->addOrderBy('id desc');
            $query->addOrderBy('doc_date desc, doc_number desc');
        }

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
                'pageParam' => 'exp-page',
            ],
        ]);
        
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->andFilterWhere(['>=', $baseTableName . '.create_time', $params['start_date']]);
        /*
        SELECT 
            `expense`.*,
            `project`.`name` AS `project_name`,
            `first_client`.`name` AS `first_client_name`,
            `second_client`.`name` AS `second_client_name`,
            `expense_type`.`name` AS `expense_type_name`
        FROM
            `expense`
                LEFT JOIN
            `project` `project` ON project.id = expense.project_id
                LEFT JOIN
            `client` `first_client` ON first_client.id = expense.first_client_id
                LEFT JOIN
            `client` `second_client` ON second_client.id = expense.second_client_id
                LEFT JOIN
            `expense_type` `expense_type` ON expense_type.id = expense.expense_type_id
        WHERE
            (`expense`.`abonent_id` = 1)
                AND (`expense`.`create_time` >= '2019-01-01')
        ORDER BY `doc_date` DESC , `doc_number` DESC
         * 
         */
        
        //return $query->all();
        return $dataProvider;
    }
    
}