<?php

namespace common\models\bill\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\bill\Convention as ConventionModel;

/**
 * Convention represents the model behind the search form of `common\models\bill\Convention`.
 */
class ConventionSearch extends ConventionModel
{
    public $first_client_name;
    public $first_client_role_name;
    public $second_client_name;
    public $second_client_role_name;
    public $third_client_name;
    public $third_client_role_name;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'deleted', 'abonent_id', 'first_client_id', 'second_client_id', 'third_client_id', 
                'create_user_id', 'update_user_id'], 'integer'],
            [['doc_type', 'doc_number', 'doc_date', 'comment', 
                'first_client_name', 'first_client_role_name',
                'second_client_name', 'second_client_role_name',
                'third_client_name', 'third_client_role_name',
                'create_time', 'update_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
    
        $query = ConventionModel::find(true);

        $query->select = [
            $baseTableName . '.*',
            'first_client_name' => 'first_client.name',
            'second_client_name' => 'second_client.name',
            'third_client_name' => 'third_client.name',

        ];
        $query->leftJoin(['first_client' => 'client'], 'first_client.id = '.$baseTableName.'.first_client_id');
        $query->leftJoin(['second_client' => 'client'], 'second_client.id = '.$baseTableName.'.second_client_id');
        $query->leftJoin(['third_client' => 'client'], 'third_client.id = '.$baseTableName.'.third_client_id');
        
        if(!isset($params['sort'])){
            $query->addOrderBy('id desc');
        }
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            $baseTableName.'.id' => $this->id,
            $baseTableName.'.abonent_id' => $this->abonent_id,
            $baseTableName.'.deleted' => $this->deleted,
            $baseTableName.'.first_client_id' => $this->first_client_id,
            $baseTableName.'.second_client_id' => $this->second_client_id,
            $baseTableName.'.third_client_id' => $this->third_client_id,
            $baseTableName.'.doc_date' => !empty($this->doc_date) ? date('Y-m-d', strtotime($this->doc_date)) : null,
            $baseTableName.'.create_time' => $this->create_time,
            $baseTableName.'.create_user_id' => $this->create_user_id,
            $baseTableName.'.update_time' => $this->update_time,
            $baseTableName.'.update_user_id' => $this->update_user_id,
        ]);

        $query->andFilterWhere(['like', $baseTableName.'.doc_type', $this->doc_type])
            ->andFilterWhere(['like', $baseTableName.'.doc_number', $this->doc_number])
            ->andFilterWhere(['like', $baseTableName.'.comment', $this->comment])
                
            ->andFilterWhere(['like', 'first_client.name', $this->first_client_name])
            ->andFilterWhere(['like', 'second_client.name', $this->second_client_name])
            ->andFilterWhere(['like', 'third_client.name', $this->third_client_name]);                

        /*
        SELECT 
            `convention`.*,
            `first_client`.`name` AS `first_client_name`,
            `second_client`.`name` AS `second_client_name`,
            `third_client`.`name` AS `third_client_name`
        FROM
            `convention`
                LEFT JOIN
            `client` `first_client` ON first_client.id = convention.first_client_id
                LEFT JOIN
            `client` `second_client` ON second_client.id = convention.second_client_id
                LEFT JOIN
            `client` `third_client` ON third_client.id = convention.third_client_id
        WHERE
            (`convention`.`abonent_id` = 1)
            AND (
                (`convention`.`first_client_id` = 831)
                OR (`convention`.`second_client_id` = 831)
                OR (`convention`.`third_client_id` = 831)
            )
            AND (`convention`.`deleted` = 0)
        ORDER BY `id` DESC
         * 
         */
        //return $query->all();
        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchLast($params)
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
    
        $query = ConventionModel::find(true);

        $query->select = [
            $baseTableName . '.*',
            'first_client_name' => 'first_client.name',
            'second_client_name' => 'second_client.name',
            'third_client_name' => 'third_client.name',
        ];
        $query->leftJoin(['first_client' => 'client'], 'first_client.id = '.$baseTableName.'.first_client_id');
        $query->leftJoin(['second_client' => 'client'], 'second_client.id = '.$baseTableName.'.second_client_id');
        $query->leftJoin(['third_client' => 'client'], 'third_client.id = '.$baseTableName.'.third_client_id');
        
        if(!isset($params['sort'])){
            $query->addOrderBy('id desc');
        }
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
                'pageParam' => 'con-page',
            ],            
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->andFilterWhere(['>=', $baseTableName . '.create_time', $params['start_date']]);
        /*
        SELECT 
            `convention`.*,
            `first_client`.`name` AS `first_client_name`,
            `second_client`.`name` AS `second_client_name`,
            `third_client`.`name` AS `third_client_name`
        FROM
            `convention`
                LEFT JOIN
            `client` `first_client` ON first_client.id = convention.first_client_id
                LEFT JOIN
            `client` `second_client` ON second_client.id = convention.second_client_id
                LEFT JOIN
            `client` `third_client` ON third_client.id = convention.third_client_id
        WHERE
            (`convention`.`abonent_id` = 1)
            AND (`convention`.`create_time` >= '2019-01-01')
        ORDER BY `id` DESC
         * 
         */
        //return $query->all();
        return $dataProvider;
    }    
}