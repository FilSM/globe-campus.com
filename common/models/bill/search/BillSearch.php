<?php

namespace common\models\bill\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\db\ActiveQuery;

use common\models\bill\Bill;

/**
 * BillSearch represents the model behind the search form of `common\models\bill\Bill`.
 */
class BillSearch extends Bill
{
    public $create_time_range;
    public $doc_date_range;
    public $pay_date_range;
    public $paid_date_range;
    public $project_id;
    public $project_name;
    public $agreement_number;
    public $first_client_id;
    public $first_client_name;
    public $first_client_role_name;
    public $second_client_id;
    public $second_client_name;
    public $second_client_role_name;
    public $third_client_id;
    public $third_client_name;
    public $third_client_role_name;
    public $manager_name;
    public $manager_user_id;
    
    public $project_sales;
    public $project_purchases;
    public $project_profit;

    public $client_id;
    public $client_name;
    public $client_sales;
    public $client_vat_plus;
    public $client_purchases;
    public $client_vat_minus;
    public $client_vat_result;
    public $client_profit;
    
    public $debtors_summa;
    public $creditors_summa;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'deleted', 'returned', 'blocked', 'agreement_id', 
                'parent_id', 'delayed', 'has_act', 'first_client_bank_id', 'second_client_bank_id', 
                /*'valuta_id', */'manager_id', 'create_user_id', 'update_user_id'], 'integer'],
            [['doc_type', 'doc_number', 'doc_date', 'pay_date', 'paid_date', 'complete_date', 
                'status', 'pay_status', 'mail_status', 'transfer_status', 'comment', 
                'create_time', 'update_time', 
                'doc_date_range', 'pay_date_range', 'paid_date_range', 'create_time_range', 
                'project_name', 'agreement_number', 
                'first_client_name', 'second_client_name', 'third_client_name', 'manager_name', 
                'manager_user_id', 'first_client_id', 'second_client_id', 'third_client_id', 
                'first_client_role_name', 'second_client_role_name', 'third_client_role_name', 
                'project_id', 'client_id', 'valuta_id'], 'safe'],
            [['summa', 'vat', 'total', 'rate', 'summa_eur', 'vat_eur', 'total_eur', 
                'debtors_summa', 'creditors_summa'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function getIdsByProject($params)
    {
        $start_date = !empty($_GET['from']) ? date('Y-m-d', strtotime($_GET['from'])) : date('Y-01-01');
        $end_date = isset($_GET['till']) ? (!empty($_GET['till']) ? date("Y-m-d", strtotime($_GET['till'])) : date("Y-m-t")) : date("Y-m-t");
        
        $query = (new Query());
        $query
            ->select([
                'id' => 'bill.id',
            ])  
            ->from('bill_project')
                
            ->innerJoin(['bill' => 'bill'], 'bill.id = bill_project.bill_id')
            ->innerJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id and ab.abonent_id = '.$this->userAbonentId)
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->leftJoin(['c' => 'client'], 'c.id = a.'.($params['direction'] == 'in' ? 'first_client_id' : 'second_client_id'))
            ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = c.id')
                
            ->andWhere('bill.summa_eur != 0')
            ->andWhere(['in', 'bill.status', ['signed', 'prepar_payment', 'payment', 'paid', 'complete']])
            ->andWhere(['not', ['ac.client_group_id' => null]])
            ->andWhere(['!=', 'bill.doc_type', Bill::BILL_DOC_TYPE_AVANS])                
            ->andWhere(['between', 'IF(bill.services_period_till IS NOT NULL, bill.services_period_till, bill.doc_date)', $start_date, $end_date.' 23:59:59'])
            ->andWhere(['!=', 'bill.deleted', 1])
            ->andWhere(['bill_project.abonent_id' => $this->userAbonentId]);
       
        if(!empty($params['direction']) && ($params['direction'] == 'out')){
            $query->andWhere(['!=', 'bill.not_expenses', 1]);
        }
        
        if(!empty($params['project_id'])){
            $query->andWhere(['ab.project_id' => $params['project_id']]);
        }
        
        $query->groupBy('bill.id');

        /*
        SELECT 
            bill.id AS `id` 
        FROM 
            `bill` 
            INNER JOIN `abonent_bill` `ab` ON ab.bill_id = bill.id AND ab.abonent_id = 1
            LEFT JOIN `agreement` `a` ON a.id = bill.agreement_id 
            LEFT JOIN `client` `c` ON c.id = a.first_client_id 
            LEFT JOIN `abonent_client` `ac` ON ac.client_id = c.id 
            LEFT JOIN `project` `p` ON p.id = ab.project_id 
        WHERE 
            (bill.summa_eur != 0) AND 
            (`bill`.`status` IN ('signed', 'prepar_payment', 'payment', 'paid', 'complete')) AND 
            (NOT (`ac`.`client_group_id` IS NULL)) AND 
            (`bill`.`doc_type` != 'avans') AND 
            (IF(bill.services_period_till IS NOT NULL, bill.services_period_till, bill.doc_date) BETWEEN '2018-12-01' AND '2019-01-31 23:59:59') AND 
            (`bill`.`deleted` != 1) AND 
            -- (`bill`.`not_expenses` != 1) AND 
            (`ab`.`project_id`= 57)
         * 
         */
        
        $billList = $query->all();
        $ids = [];
        foreach ($billList as $bill) {
            $ids[] = $bill['id'];
        }        
        return $ids;        
    }

    public function getIdsByClient($params, $clientIds = [])
    {
        $start_date = !empty($_GET['from']) ? date('Y-m-d', strtotime($_GET['from'])) : date('Y-01-01');
        $end_date = isset($_GET['till']) ? (!empty($_GET['till']) ? date("Y-m-d", strtotime($_GET['till'])) : date("Y-m-t")) : date("Y-m-t");
        
        $clientField = ($params['direction'] == 'in' ? 'first_client_id' : 'second_client_id');
        
        $query = (new Query());
        $query
            ->select([
                'id' => 'bill.id',
            ])  
            ->from('bill')
                
            ->innerJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id and ab.abonent_id = '.$this->userAbonentId)
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->leftJoin(['c' => 'client'], 'c.id = a.'.$clientField)
            ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = c.id')
                
            ->andWhere('bill.summa_eur != 0')
            ->andWhere(['in', 'bill.status', ['signed', 'prepar_payment', 'payment', 'paid', 'complete']])
            ->andWhere(['not', ['ac.client_group_id' => null]])
            ->andWhere(['!=', 'bill.deleted', 1]);
        
        if($start_date != '1900-01-01'){
            $query->andWhere(['between', 'IF(bill.services_period_till IS NOT NULL, bill.services_period_till, bill.doc_date)', $start_date, $end_date.' 23:59:59']);
        }else{
            $query->andWhere(['between', 'bill.doc_date', $start_date, $end_date.' 23:59:59']);
        }
        
        if(!empty($params['doc_type'])){
            $query->andWhere(['in', 'bill.doc_type', $params['doc_type']]);
        }else{
            $query->andWhere(['!=', 'bill.doc_type', Bill::BILL_DOC_TYPE_AVANS]);
        }
        
        if(!empty($params['pay_status'])){
            $query->andWhere(['or',
                ['in', 'bill.pay_status', $params['pay_status']],
                ['>', 'bill.paid_date', $end_date]
            ]);
        }
        
        if(!empty($params['direction']) && ($params['direction'] == 'out')){
            $query->andWhere(['!=', 'bill.not_expenses', 1]);
        }
                
        if(!empty($clientIds)){
            $query->andWhere(['in', 'c.id', $clientIds]);
        }
        
        if(!empty($params['client_id'])){
            $query->andWhere(['a.'.$clientField => $params['client_id']]);
        }

        /*
        SELECT 
            `bill`.`id` AS `id`
        FROM
            `bill`
                LEFT JOIN
            `agreement` `a` ON a.id = bill.agreement_id
                INNER JOIN
            `abonent_bill` `ab` ON ab.bill_id = bill.id AND ab.abonent_id = 1
                LEFT JOIN
            `client` `c` ON c.id = a.first_client_id
                LEFT JOIN
            `abonent_client` `ac` ON ac.client_id = c.id
        WHERE
            (`bill`.`status` IN ('signed' , 'prepar_payment', 'payment', 'paid', 'complete'))
            AND (`bill`.`pay_status` IN ('not' , 'part'))
            AND (`bill`.`doc_type` IN ('bill' , 'invoice'))
            AND (`bill`.`doc_date` BETWEEN '1900-01-01' AND '2019-07-31 23:59:59')
            AND (`bill`.`summa_eur` != 0)
            AND (`bill`.`deleted` != 1)
            -- AND (`bill`.`not_expenses` != 1)
            AND (NOT (`ac`.`client_group_id` IS NULL))
            AND (`a`.`first_client_id` = 3)
        order by `bill`.`id
         * 
         */
        
        $billList = $query->all();
        $ids = [];
        foreach ($billList as $bill) {
            $ids[] = $bill['id'];
        }
        return $ids;        
    }

    public function getIdsForVatReport($params)
    {
        $ids = [];
        $start_date = !empty($_GET['from']) ? date('Y-m-d', strtotime($_GET['from'])) : date('Y-01-01');
        $end_date = isset($_GET['till']) ? (!empty($_GET['till']) ? date("Y-m-d", strtotime($_GET['till'])) : date("Y-m-t")) : date("Y-m-t");
        
        $clientField = ($params['direction'] == 'in' ? 'first_client_id' : 'second_client_id');
        
        $query = (new Query());
        $query
            ->select([
                'id' => 'bill.id',
            ])  
            ->from('bill')
                
            ->innerJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id and ab.abonent_id = '.$this->userAbonentId)
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->leftJoin(['c' => 'client'], 'c.id = a.'.$clientField)
            ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = c.id')
                
            ->andWhere(['in', 'bill.status', ['signed', 'prepar_payment', 'payment', 'paid', 'complete']])
            ->andWhere(['!=', 'bill.doc_type', Bill::BILL_DOC_TYPE_AVANS])
            ->andWhere(['between', 'bill.doc_date', $start_date, $end_date.' 23:59:59'])
            ->andWhere(['!=', 'bill.deleted', 1])
            ->andWhere(['not', ['ac.client_group_id' => null]]);
        
        if(!empty($params['client_id'])){
            $query->andWhere(['a.'.$clientField => $params['client_id']]);
        }

        /*
        SELECT 
            `bill`.`id` AS `id`
        FROM 
            `bill` 
                INNER JOIN
            `abonent_bill` `ab` ON ab.bill_id = bill.id AND ab.abonent_id = 1 
                LEFT JOIN
            `agreement` `a` ON a.id = bill.agreement_id 
                LEFT JOIN
            `client` `c` ON c.id = a.first_client_id
                LEFT JOIN
            `abonent_client` `ac` ON ac.client_id = c.id
        WHERE 
            (`bill`.`status` IN ('signed', 'prepar_payment', 'payment', 'paid', 'complete')) AND 
            (`bill`.`doc_type` != 'avans') AND 
            (`bill`.`doc_date` BETWEEN '2019-01-01' AND '2019-08-31') AND 
            (`bill`.`deleted` != 1) AND 
            (`ac`.`client_group_id` IS NOT NULL) AND 
            (`a`.`first_client_id` = 3)
        ORDER BYby `bill`.`id
         * 
         */

        $billList = $query->all();
        foreach ($billList as $bill) {
            $ids[] = $bill['id'];
        }
        
        $query = (new Query());
        $query
            ->select([
                'id' => 'bill.id',
            ])  
            ->from('bill')
                
            ->innerJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id and ab.abonent_id = '.$this->userAbonentId)
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->leftJoin(['c' => 'client'], 'c.id = a.'.$clientField)
            ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = c.id')
                
            ->andWhere(['in', 'bill.status', ['complete']])
            ->andWhere(['bill.doc_type' => Bill::BILL_DOC_TYPE_AVANS])
            ->andWhere(['between', 'bill.paid_date', $start_date, $end_date.' 23:59:59'])
            ->andWhere(['!=', 'bill.deleted', 1])
            ->andWhere(['bill.child_id' => null])
            ->andWhere(['not', ['ac.client_group_id' => null]]);
        
        if(!empty($params['client_id'])){
            $query->andWhere(['a.'.$clientField => $params['client_id']]);
        }

        /*
        SELECT 
            `bill`.`id` AS `id`
        FROM 
            `bill` 
                INNER JOIN
            `abonent_bill` `ab` ON ab.bill_id = bill.id AND ab.abonent_id = 1 
                LEFT JOIN
            `agreement` `a` ON a.id = bill.agreement_id 
                LEFT JOIN
            `client` `c` ON c.id = a.first_client_id
                LEFT JOIN
            `abonent_client` `ac` ON ac.client_id = c.id
        WHERE 
            (`bill`.`status` IN ('complete')) AND 
            (`bill`.`doc_type` = 'avans') AND 
            (`bill`.`doc_date` BETWEEN '2019-01-01' AND '2019-08-31') AND 
            (`bill`.`deleted` != 1) AND 
            (`bill`.`child_id` IS NULL) AND
            (`ac`.`client_group_id` IS NOT NULL) AND 
            (`a`.`first_client_id` = 3)
        order by `bill`.`id
         * 
         */

        $billList = $query->all();
        foreach ($billList as $bill) {
            $ids[] = $bill['id'];
        }
                
        return $ids;        
    }
    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
    
        $query = Bill::find(true);

        $query->select = [
            $baseTableName . '.*',
            'agreement_number' => 'agreement.number',
            'first_client_id' => 'If('.$baseTableName.'.doc_type != "cession", agreement.first_client_id, IF('.$baseTableName.'.cession_direction = "D", agreement.first_client_id, agreement.third_client_id))',
            'first_client_name' => 'If('.$baseTableName.'.doc_type != "cession", first_client.name, IF('.$baseTableName.'.cession_direction = "D", first_client.name, third_client.name))',
            'first_client_role_name' => 'If('.$baseTableName.'.doc_type != "cession", first_client_role.name, IF('.$baseTableName.'.cession_direction = "D", first_client_role.name, third_client_role.name))',
            'first_client_regno' => 'If('.$baseTableName.'.doc_type != "cession", first_client.reg_number, IF('.$baseTableName.'.cession_direction = "D", first_client.reg_number, third_client.reg_number))',
            'second_client_id' => 'If('.$baseTableName.'.doc_type != "cession", agreement.second_client_id, IF('.$baseTableName.'.cession_direction = "D", agreement.third_client_id, agreement.second_client_id))',
            'second_client_name' => 'If('.$baseTableName.'.doc_type != "cession", second_client.name, IF('.$baseTableName.'.cession_direction = "D", third_client.name, second_client.name))',
            'second_client_role_name' => 'If('.$baseTableName.'.doc_type != "cession", second_client_role.name, IF('.$baseTableName.'.cession_direction = "D", third_client_role.name, second_client_role.name))',
            'second_client_regno' => 'If('.$baseTableName.'.doc_type != "cession", second_client.reg_number, IF('.$baseTableName.'.cession_direction = "D", third_client.reg_number, second_client.reg_number))',
            'third_client_id' => 'agreement.third_client_id',
            'third_client_name' => 'third_client.name',
            'third_client_role_name' => 'third_client_role.name',
            'manager_name' => 'manager.name',
            'manager_user_id' => 'manager.user_id',
        ];
        
        if(isset($params['project_id']) || isset($params['BillSearch']['project_id'])){
            $query->select += [
                'project_id' => 'bp.project_id',
                'project_name' => 'project.name',
            ];
        }
        
        $query->innerJoin(['ab' => 'abonent_bill'], 'ab.bill_id = '.$baseTableName.'.id and ab.abonent_id = '.$this->userAbonentId);
        $query->leftJoin(['agreement' => 'agreement'], 'agreement.id = '.$baseTableName.'.agreement_id');
        $query->leftJoin(['first_client' => 'client'], 'first_client.id = agreement.first_client_id');
        $query->leftJoin(['second_client' => 'client'], 'second_client.id = agreement.second_client_id');
        $query->leftJoin(['third_client' => 'client'], 'third_client.id = agreement.third_client_id');
        $query->leftJoin(['first_client_role' => 'client_role'], 'first_client_role.id = agreement.first_client_role_id');
        $query->leftJoin(['second_client_role' => 'client_role'], 'second_client_role.id = agreement.second_client_role_id');
        $query->leftJoin(['third_client_role' => 'client_role'], 'third_client_role.id = agreement.third_client_role_id');
        $query->leftJoin(['manager' => 'profile'], 'manager.id = '.$baseTableName.'.manager_id');
        
        if(isset($params['project_id']) || isset($params['BillSearch']['project_id'])){
            $query->leftJoin(['bp' => 'bill_project'], 'bp.bill_id = '.$baseTableName.'.id and bp.abonent_id = '.$this->userAbonentId);
            $query->leftJoin(['project' => 'project'], 'project.id = bp.project_id');
            if(isset($params['project_id']) && isset($_POST['export_type'])){
                $query->select['index_by'] = 'CONCAT('.$baseTableName . '.id, " | ", bp.project_id)';
                $query->indexBy('index_by');
            }else{
                $query->groupBy($baseTableName . '.id');
            }
        }
        
        if(!isset($params['sort'])){
            $query->addOrderBy('id desc');
            //$query->addOrderBy('doc_date desc, doc_number desc');
        }
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => isset($_POST['expandRowKey']) || isset($params['ids']) ? 0 : 50,
            ],
        ]);

        $idsArr = isset($params['ids']) ? $params['ids'] : null;
        $statusArr = !empty($params['status']) ? $params['status'] : null;
        $payStatusArr = !empty($params['pay_status']) ? $params['pay_status'] : null;
        $docTypeArr = !empty($params['doc_type']) ? $params['doc_type'] : null;
        $projectIds = !empty($params['BillSearch']['project_id']) ? $params['BillSearch']['project_id'] : null;
        $agreementIds = !empty($params['BillSearch']['agreement_id']) ? $params['BillSearch']['agreement_id'] : null;
        $firstClientIds = !empty($params['BillSearch']['first_client_id']) ? $params['BillSearch']['first_client_id'] : null;
        unset(
            $params['ids'], 
            $params['status'], 
            $params['pay_status'], 
            $params['doc_type'], 
            $params['BillSearch']['project_id'], 
            $params['BillSearch']['agreement_id'], 
            $params['BillSearch']['first_client_id']
        );
        
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $this->project_id = $projectIds ?? ($params['project_id'] ?? ($params['BillSearch']['project_id'] ?? null));
        $this->agreement_id = $agreementIds ?? ($params['agreement_id'] ?? null);
        $this->first_client_id = !empty($firstClientIds) ? $firstClientIds : null;
                
        // grid filtering conditions
        if(isset($idsArr)){
            $query->andWhere([$baseTableName.'.id' => $idsArr]);
        }
                
        if(!empty($this->doc_date) && strpos($this->doc_date, '-') !== false) { 
            $this->doc_date_range = $this->doc_date;
            list($start_date, $end_date) = explode(' - ', $this->doc_date_range);
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));
            $query->andFilterWhere(['between', $baseTableName . '.doc_date', $start_date, $end_date.' 23:59:59']);
        }
                
        if(!empty($this->pay_date) && strpos($this->pay_date, '-') !== false) { 
            $this->pay_date_range = $this->pay_date;
            list($start_date, $end_date) = explode(' - ', $this->pay_date_range);
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));
            $query->andFilterWhere(['between', $baseTableName . '.pay_date', $start_date, $end_date.' 23:59:59']);
        }
                
        if(!empty($this->paid_date) && strpos($this->paid_date, '-') !== false) { 
            $this->paid_date_range = $this->paid_date;
            list($start_date, $end_date) = explode(' - ', $this->paid_date_range);
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));
            $query->andFilterWhere(['between', $baseTableName . '.paid_date', $start_date, $end_date.' 23:59:59']);
        }
        
        $query->andFilterWhere([
            $baseTableName.'.id' => $this->id,
            $baseTableName.'.deleted' => $this->deleted,
            $baseTableName.'.returned' => $this->returned,
            $baseTableName.'.blocked' => $this->blocked,
            $baseTableName.'.has_act' => $this->has_act,
            //$baseTableName.'.status' => $this->status,
            $baseTableName.'.transfer_status' => $this->transfer_status,
            $baseTableName.'.mail_status' => $this->mail_status,
            $baseTableName.'.delayed' => $this->delayed,
            'ab.project_id' => $this->project_id,
            $baseTableName.'.agreement_id' => $this->agreement_id,
            $baseTableName.'.parent_id' => $this->parent_id,
            $baseTableName.'.doc_date' => !empty($this->doc_date) && empty($this->doc_date_range) ? date('Y-m-d', strtotime($this->doc_date)) : null,
            $baseTableName.'.pay_date' => !empty($this->pay_date) && empty($this->pay_date_range) ? date('Y-m-d', strtotime($this->pay_date)) : null,
            $baseTableName.'.paid_date' => !empty($this->paid_date) && empty($this->paid_date_range) ? date('Y-m-d', strtotime($this->paid_date)) : null,
            $baseTableName.'.complete_date' => !empty($this->complete_date) ? date('Y-m-d', strtotime($this->complete_date)) : null,
            $baseTableName.'.first_client_bank_id' => $this->first_client_bank_id,
            $baseTableName.'.second_client_bank_id' => $this->second_client_bank_id,
            $baseTableName.'.summa' => $this->summa,
            $baseTableName.'.vat' => $this->vat,
            $baseTableName.'.total' => $this->total,
            $baseTableName.'.summa_eur' => $this->summa_eur,
            $baseTableName.'.vat_eur' => $this->vat_eur,
            $baseTableName.'.total_eur' => $this->total_eur,
            $baseTableName.'.valuta_id' => $this->valuta_id,
            $baseTableName.'.manager_id' => $this->manager_id,
            $baseTableName.'.create_time' => $this->create_time,
            $baseTableName.'.create_user_id' => $this->create_user_id,
            $baseTableName.'.update_time' => $this->update_time,
            $baseTableName.'.update_user_id' => $this->update_user_id,
            
            'agreement.first_client_id' => $this->first_client_id,
            'agreement.second_client_id' => $this->second_client_id,
        ]);
        if(isset($this->project_id)){
            $query->andFilterWhere(['bp.project_id' => $this->project_id]);
        }

        $query->andFilterWhere(['like', $baseTableName.'.doc_number', $this->doc_number])
            ->andFilterWhere(['like', $baseTableName.'.comment', $this->comment])
                
            ->andFilterWhere(['like', 'project.name', $this->project_name])
            ->andFilterWhere(['like', 'agreement.number', $this->agreement_number])
            ->andFilterWhere(['like', 'If('.$baseTableName.'.doc_type != "cession", first_client.name, IF('.$baseTableName.'.cession_direction = "D", first_client.name, third_client.name))', $this->first_client_name])
            ->andFilterWhere(['like', 'If('.$baseTableName.'.doc_type != "cession", second_client.name, IF('.$baseTableName.'.cession_direction = "D", third_client.name, second_client.name))', $this->second_client_name])
            ->andFilterWhere(['like', 'manager.name', $this->manager_name]);

        if(!empty($this->status)){
            $query->andFilterWhere(['in', $baseTableName.'.status', $this->status]);
        }elseif(!empty($statusArr)){
            $query->andFilterWhere(['in', $baseTableName.'.status', $statusArr]);
        }
        
        if(!empty($this->doc_type)){
            $query->andFilterWhere(['in', $baseTableName.'.doc_type', $this->doc_type]);
        }elseif(!empty($docTypeArr)){
            $query->andFilterWhere(['in', $baseTableName.'.doc_type', $docTypeArr]);
        }
        
        if(isset($this->pay_status)){
            if($this->pay_status != 'delayed'){
                if(is_array($this->pay_status)){
                    $query->andFilterWhere(['in', $baseTableName.'.pay_status', $this->pay_status]);
                }else{
                    $query->andFilterWhere([$baseTableName.'.pay_status' => $this->pay_status]);
                }
            }else{
                $query->andFilterWhere([$baseTableName.'.delayed' => 1]);
            }
        }elseif(!empty($payStatusArr)){
            $query->andFilterWhere(['in', $baseTableName.'.pay_status', $payStatusArr]);
        }
        
        if(!empty($this->create_time_range) && strpos($this->create_time_range, '-') !== false) { 
            list($start_date, $end_date) = explode(' - ', $this->create_time_range); 
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));
            $query->andFilterWhere(['between', $baseTableName . '.create_time', $start_date, $end_date.' 23:59:59']);
        }
        /*
        SELECT 
            `bill`.*,
            `project`.`id` AS `project_id`,
            `project`.`name` AS `project_name`,
            `agreement`.`number` AS `agreement_number`,
            IF(bill.doc_type != 'cession',
                agreement.first_client_id,
                IF(bill.cession_direction = 'D',
                    agreement.first_client_id,
                    agreement.third_client_id)) AS `first_client_id`,
            IF(bill.doc_type != 'cession',
                first_client.name,
                IF(bill.cession_direction = 'D',
                    first_client.name,
                    third_client.name)) AS `first_client_name`,
            IF(bill.doc_type != 'cession',
                first_client_role.name,
                IF(bill.cession_direction = 'D',
                    first_client_role.name,
                    third_client_role.name)) AS `first_client_role_name`,
            IF(bill.doc_type != 'cession',
                agreement.second_client_id,
                IF(bill.cession_direction = 'D',
                    agreement.third_client_id,
                    agreement.second_client_id)) AS `second_client_id`,
            IF(bill.doc_type != 'cession',
                second_client.name,
                IF(bill.cession_direction = 'D',
                    third_client.name,
                    second_client.name)) AS `second_client_name`,
            IF(bill.doc_type != 'cession',
                second_client_role.name,
                IF(bill.cession_direction = 'D',
                    third_client_role.name,
                    second_client_role.name)) AS `second_client_role_name`,
            `agreement`.`third_client_id` AS `third_client_id`,
            `third_client`.`name` AS `third_client_name`,
            `third_client_role`.`name` AS `third_client_role_name`,
            `manager`.`name` AS `manager_name`,
            `manager`.`user_id` AS `manager_user_id`
        FROM
            `bill`
                INNER JOIN
            `abonent_bill` `abonent_bill_scope` ON abonent_bill_scope.bill_id = bill.id
                INNER JOIN
            `abonent_bill` `ab` ON ab.bill_id = bill.id AND ab.abonent_id = 1
                LEFT JOIN
            `project` `project` ON project.id = ab.project_id
                LEFT JOIN
            `agreement` `agreement` ON agreement.id = bill.agreement_id
                LEFT JOIN
            `client` `first_client` ON first_client.id = agreement.first_client_id
                LEFT JOIN
            `client` `second_client` ON second_client.id = agreement.second_client_id
                LEFT JOIN
            `client` `third_client` ON third_client.id = agreement.third_client_id
                LEFT JOIN
            `client_role` `first_client_role` ON first_client_role.id = agreement.first_client_role_id
                LEFT JOIN
            `client_role` `second_client_role` ON second_client_role.id = agreement.second_client_role_id
                LEFT JOIN
            `client_role` `third_client_role` ON third_client_role.id = agreement.third_client_role_id
                LEFT JOIN
            `profile` `manager` ON manager.id = bill.manager_id
                LEFT JOIN
            `agreement` `scope_agreement` ON scope_agreement.id = bill.agreement_id
        WHERE
            (`abonent_bill_scope`.`abonent_id` = 1)
                AND (`bill`.`agreement_id` = 839)
                AND (`bill`.`deleted` = 0)
         * 
         */
        
        //return $query->all();
        return $dataProvider;
    }
    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchLast($params)
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
    
        $query = Bill::find(true);

        $query->select = [
            $baseTableName . '.*',
            'project_id' => 'project.id',
            'project_name' => 'project.name',
            'agreement_number' => 'agreement.number',
            'first_client_id' => 'If('.$baseTableName.'.doc_type != "cession", agreement.first_client_id, IF('.$baseTableName.'.cession_direction = "D", agreement.first_client_id, agreement.third_client_id))',
            'first_client_name' => 'If('.$baseTableName.'.doc_type != "cession", first_client.name, IF('.$baseTableName.'.cession_direction = "D", first_client.name, third_client.name))',
            'first_client_role_name' => 'If('.$baseTableName.'.doc_type != "cession", first_client_role.name, IF('.$baseTableName.'.cession_direction = "D", first_client_role.name, third_client_role.name))',
            'second_client_id' => 'If('.$baseTableName.'.doc_type != "cession", agreement.second_client_id, IF('.$baseTableName.'.cession_direction = "D", agreement.third_client_id, agreement.second_client_id))',
            'second_client_name' => 'If('.$baseTableName.'.doc_type != "cession", second_client.name, IF('.$baseTableName.'.cession_direction = "D", third_client.name, second_client.name))',
            'second_client_role_name' => 'If('.$baseTableName.'.doc_type != "cession", second_client_role.name, IF('.$baseTableName.'.cession_direction = "D", third_client_role.name, second_client_role.name))',
            'third_client_id' => 'agreement.third_client_id',
            'third_client_name' => 'third_client.name',
            'third_client_role_name' => 'third_client_role.name',
            'manager_name' => 'manager.name',
            'manager_user_id' => 'manager.user_id',
        ];
        
        $query->innerJoin(['ab' => 'abonent_bill'], 'ab.bill_id = '.$baseTableName.'.id and ab.abonent_id = '.$this->userAbonentId);
        $query->leftJoin(['project' => 'project'], 'project.id = ab.project_id');
        $query->leftJoin(['agreement' => 'agreement'], 'agreement.id = '.$baseTableName.'.agreement_id');
        $query->leftJoin(['first_client' => 'client'], 'first_client.id = agreement.first_client_id');
        $query->leftJoin(['second_client' => 'client'], 'second_client.id = agreement.second_client_id');
        $query->leftJoin(['third_client' => 'client'], 'third_client.id = agreement.third_client_id');
        $query->leftJoin(['first_client_role' => 'client_role'], 'first_client_role.id = agreement.first_client_role_id');
        $query->leftJoin(['second_client_role' => 'client_role'], 'second_client_role.id = agreement.second_client_role_id');
        $query->leftJoin(['third_client_role' => 'client_role'], 'third_client_role.id = agreement.third_client_role_id');
        $query->leftJoin(['manager' => 'profile'], 'manager.id = '.$baseTableName.'.manager_id');
        
        if(!isset($params['sort'])){
            $query->addOrderBy('id desc');
            //$query->addOrderBy('doc_date desc, doc_number desc');
        }
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
                'pageParam' => 'bill-page',
            ],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->andFilterWhere(['>=', $baseTableName . '.create_time', $params['start_date']]);
        /*
        SELECT 
            `bill`.*,
            `project`.`id` AS `project_id`,
            `project`.`name` AS `project_name`,
            `agreement`.`number` AS `agreement_number`,
            agreement.first_client_id,
            first_client.name,
            first_client_role.name,
            agreement.second_client_id,
            second_client.name,
            second_client_role.name,
            `agreement`.`third_client_id` AS `third_client_id`,
            `third_client`.`name` AS `third_client_name`,
            `third_client_role`.`name` AS `third_client_role_name`,
            `manager`.`name` AS `manager_name`,
            `manager`.`user_id` AS `manager_user_id`
        FROM
            `bill`
                INNER JOIN
            `abonent_bill` `abonent_bill_scope` ON abonent_bill_scope.bill_id = bill.id
                INNER JOIN
            `abonent_bill` `ab` ON ab.bill_id = bill.id AND ab.abonent_id = 1
                LEFT JOIN
            `project` `project` ON project.id = ab.project_id
                LEFT JOIN
            `agreement` `agreement` ON agreement.id = bill.agreement_id
                LEFT JOIN
            `client` `first_client` ON first_client.id = agreement.first_client_id
                LEFT JOIN
            `client` `second_client` ON second_client.id = agreement.second_client_id
                LEFT JOIN
            `client` `third_client` ON third_client.id = agreement.third_client_id
                LEFT JOIN
            `client_role` `first_client_role` ON first_client_role.id = agreement.first_client_role_id
                LEFT JOIN
            `client_role` `second_client_role` ON second_client_role.id = agreement.second_client_role_id
                LEFT JOIN
            `client_role` `third_client_role` ON third_client_role.id = agreement.third_client_role_id
                LEFT JOIN
            `profile` `manager` ON manager.id = bill.manager_id
                LEFT JOIN
            `agreement` `scope_agreement` ON scope_agreement.id = bill.agreement_id
        WHERE
            (`abonent_bill_scope`.`abonent_id` = 1)
                AND (`bill`.`deleted` = 0)
                AND (`bill`.`create_time` >= '2019-01-01')
         * 
         */
        
        //return $query->all();
        return $dataProvider;
    }
    
    public function searchDelayed()
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
        $query = $this->find();
        $query->andWhere([
            $baseTableName.'.deleted' => 0,
        ]);        
        $query->andWhere(['<', $baseTableName.'.pay_date', date('Y-m-d')])
            ->andWhere(['in', $baseTableName.'.pay_status', [Bill::BILL_PAY_STATUS_NOT, Bill::BILL_PAY_STATUS_PART]]);
        
        return $query->all();
    }
    
    public function searchEbitdaReport($params, $clientIds)
    {
        $this->clearDefaultValues();
        
        $start_date = !empty($_GET['from']) ? date('Y-m-d', strtotime($_GET['from'])) : date('Y-01-01');
        $end_date = isset($_GET['till']) ? (!empty($_GET['till']) ? date("Y-m-d", strtotime($_GET['till'])) : date("Y-m-t")) : date("Y-m-t");
        
        $query = new ActiveQuery(Bill::class);
        $query->select([
                'project_id' => 'project.id',
                'project_name' => 'project.name',
                'project_sales' => 'debet.total',
                'project_purchases' => '(IF(credit.total, credit.total, 0) + IF(expense.total, expense.total, 0))',
                'project_profit' => '(IF(debet.total, debet.total, 0) - IF(credit.total, credit.total, 0) - IF(expense.total, expense.total, 0))',
            ])
            ->from('project');
        
        $subBillQuery = (new Query())
            ->select([
                'project_id' => 'bprj.project_id',
                'total' => 'SUM(bprod.summa_eur)',
            ])  
            ->from(['bprj' => 'bill_project'])
            ->innerJoin(['bprod' => 'bill_product'], 'bprod.id = bprj.bill_product_id')
            ->innerJoin(['bill' => 'bill'], 'bill.id = bprod.bill_id')
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->leftJoin(['c' => 'client'], 'c.id = a.first_client_id')
            ->leftJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id')
            ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = c.id')
            ->andWhere(['in', 'bill.status', ['signed', 'prepar_payment', 'payment', 'paid', 'complete']])
            ->andWhere(['not', ['ac.client_group_id' => null]])
            ->andWhere(['!=', 'bill.doc_type', Bill::BILL_DOC_TYPE_AVANS])                
            ->andWhere(['between', 'IF(bill.services_period_till IS NOT NULL, bill.services_period_till, bill.doc_date)', $start_date, $end_date.' 23:59:59'])
            ->andWhere(['!=', 'bill.deleted', 1])
            ->andWhere(['ab.abonent_id' => $this->userAbonentId])
            ->andWhere(['ac.abonent_id' => $this->userAbonentId])
            ->andWhere(['bprj.abonent_id' => $this->userAbonentId])
            ->groupBy('bprj.project_id');
        
        if(!empty($clientIds)){
            $subBillQuery->andWhere(['in', 'a.first_client_id', $clientIds]);
        }
        
        $query->leftJoin(['debet' => $subBillQuery], 'debet.project_id = project.id');

        $subBillQuery = (new Query())
            ->select([
                'project_id' => 'bprj.project_id',
                'total' => 'SUM(IF(bill.second_client_pvn_payer, bprod.summa_eur, bprod.total_eur))',
            ])
            ->from(['bprj' => 'bill_project'])
            ->innerJoin(['bprod' => 'bill_product'], 'bprod.id = bprj.bill_product_id')
            ->innerJoin(['bill' => 'bill'], 'bill.id = bprod.bill_id')
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->leftJoin(['c' => 'client'], 'c.id = a.second_client_id')
            ->leftJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id')
            ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = c.id')
            ->andWhere(['in', 'bill.status', ['signed', 'prepar_payment', 'payment', 'paid', 'complete']])
            ->andWhere(['not', ['ac.client_group_id' => null]])
            ->andWhere(['!=', 'bill.doc_type', Bill::BILL_DOC_TYPE_AVANS])                
            ->andWhere(['between', 'IF(bill.services_period_till IS NOT NULL, bill.services_period_till, bill.doc_date)', $start_date, $end_date.' 23:59:59'])
            ->andWhere(['!=', 'bill.deleted', 1])
            ->andWhere(['!=', 'bill.not_expenses', 1])
            ->andWhere(['ab.abonent_id' => $this->userAbonentId])
            ->andWhere(['ac.abonent_id' => $this->userAbonentId])
            ->andWhere(['bprj.abonent_id' => $this->userAbonentId])
            ->groupBy('bprj.project_id');
        
        if(!empty($clientIds)){
            $subBillQuery->andWhere(['in', 'a.second_client_id', $clientIds]);
        }
                
        $query->leftJoin(['credit' => $subBillQuery], 'credit.project_id = project.id');
        
        $subExpenseQuery = (new Query())
            ->select([
                'project_id' => 'project_id',
                'total' => 'SUM(IF(expense.second_client_pvn_payer, expense.summa_eur, expense.total_eur))',
            ]) 
            ->from('expense')
            ->andWhere(['between', 'doc_date', $start_date, $end_date.' 23:59:59']) 
            ->andWhere(['expense.abonent_id' => $this->userAbonentId])
            ->groupBy('project_id');
        
        if(!empty($clientIds)){
            $subExpenseQuery->andWhere(['in', 'expense.first_client_id', $clientIds]);
        }
                        
        $query->leftJoin(['expense' => $subExpenseQuery], 'expense.project_id = project.id');
        
        $query
            ->andWhere(['!=', 'project.deleted', 1])
            ->andWhere('(debet.total != 0) OR (credit.total != 0) OR (expense.total != 0)')
            ->andWhere(['project.abonent_id' => $this->userAbonentId]);
        
        if(!isset($params['sort'])){
            $query->addOrderBy('project.name');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 0,
            ],
        ]);
        
        $projectIds = $params['project_id'] ?? null;
        unset($params['project_id']);
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $this->project_id = $projectIds;
        
        $query->andFilterWhere([
            'project.id' => $this->project_id,
        ]);
        
        /*
        SELECT 
            `project`.`id` AS `project_id`,
            `project`.`name` AS `project_name`,
            `debet`.`total` AS `project_sales`,
            (IF(credit.total, credit.total, 0) + IF(expense.total, expense.total, 0)) AS `project_purchases`,
            (IF(debet.total, debet.total, 0) - IF(credit.total, credit.total, 0) - IF(expense.total, expense.total, 0)) AS `project_profit`
        FROM
            `project`

                LEFT JOIN
            (SELECT 
                `ab`.`project_id` AS `project_id`, SUM(bill.summa_eur) AS `total`
            FROM
                `bill`
            LEFT JOIN `agreement` `a` ON a.id = bill.agreement_id
            LEFT JOIN `client` `c` ON c.id = a.first_client_id
            LEFT JOIN `abonent_bill` `ab` ON ab.bill_id = bill.id
            LEFT JOIN `abonent_client` `ac` ON ac.client_id = c.id
            WHERE
                (`bill`.`status` IN ('signed' , 'prepar_payment', 'payment', 'paid', 'complete'))
                AND (NOT (`ac`.`client_group_id` IS NULL))
                AND (`bill`.`doc_type` != 'avans')
                AND (IF(bill.services_period_till IS NOT NULL, bill.services_period_till, bill.doc_date) BETWEEN '2019-01-01' AND '2019-07-31 23:59:59')
                AND (`bill`.`deleted` != 1)
                AND (`ab`.`abonent_id` = 2)
                AND (`ac`.`abonent_id` = 2)
            GROUP BY `ab`.`project_id`) `debet` ON debet.project_id = project.id

                LEFT JOIN
            (SELECT 
                `ab`.`project_id` AS `project_id`, SUM(IF(bill.second_client_pvn_payer, bill.summa_eur, bill.total_eur)) AS `total`
            FROM
                `bill`
            LEFT JOIN `agreement` `a` ON a.id = bill.agreement_id
            LEFT JOIN `client` `c` ON c.id = a.second_client_id
            LEFT JOIN `abonent_bill` `ab` ON ab.bill_id = bill.id
            LEFT JOIN `abonent_client` `ac` ON ac.client_id = c.id
            WHERE
                (`bill`.`status` IN ('signed' , 'prepar_payment', 'payment', 'paid', 'complete'))
                AND (NOT (`ac`.`client_group_id` IS NULL))
                AND (`bill`.`doc_type` != 'avans')
                AND (IF(bill.services_period_till IS NOT NULL, bill.services_period_till, bill.doc_date) BETWEEN '2019-01-01' AND '2019-07-31 23:59:59')
                AND (`bill`.`deleted` != 1)
                AND (`bill`.`not_expenses` != 1)
                AND (`ab`.`abonent_id` = 2)
                AND (`ac`.`abonent_id` = 2)
            GROUP BY `ab`.`project_id`) `credit` ON credit.project_id = project.id

                LEFT JOIN
            (SELECT 
                `project_id`,
                    SUM(IF(expense.second_client_pvn_payer, expense.summa_eur, expense.total_eur)) AS `total`
            FROM
                `expense`
            WHERE
                (`doc_date` BETWEEN '2019-01-01' AND '2019-07-31 23:59:59')
                AND (`expense`.`abonent_id` = 2)
            GROUP BY `project_id`) `expense` ON expense.project_id = project.id

        WHERE
            (`project`.`deleted` != 1)
            AND 
            (
                (debet.total != 0)
                OR (credit.total != 0)
                OR (expense.total != 0)
            )
            AND (`project`.`abonent_id` = '2')
        ORDER BY `name`
         * 
         */
        
        //return $query->all();
        return $dataProvider;
    }
    
    public function searchEbitdaClientPlusReport($params, $clientIds = [])
    {
        $this->clearDefaultValues();
        
        $start_date = !empty($_GET['from']) ? date('Y-m-d', strtotime($_GET['from'])) : date('Y-01-01');
        $end_date = isset($_GET['till']) ? (!empty($_GET['till']) ? date("Y-m-d", strtotime($_GET['till'])) : date("Y-m-t")) : date("Y-m-t");
        
        $query = new ActiveQuery(Bill::class);
        $query->select([
                'client_id' => 'client.id',
                'client_name' => 'client.name',
                'client_sales' => 'debet.total',
                'client_purchases' => '(IF(credit.total, credit.total, 0) + '
                    . 'IF(expense_base.total, expense_base.total, 0) + '
                    . 'IF(expense_plus.total, expense_plus.total, 0))',
                'client_profit' => '(IF(debet.total, debet.total, 0) - '
                    . 'IF(credit.total, credit.total, 0) - '
                    . 'IF(expense_base.total, expense_base.total, 0) - '
                    . 'IF(expense_plus.total, expense_plus.total, 0))',
            ])
            ->from('client')
            ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = client.id');
        
        $subBillQuery = (new Query())
            ->select([
                'client_id' => 'a.first_client_id',
                'total' => 'SUM(bill.summa_eur)',
            ])  
            ->from('bill')
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->leftJoin(['c' => 'client'], 'c.id = a.first_client_id')
            ->leftJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id')
            ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = c.id')
            ->andWhere(['in', 'bill.status', ['signed', 'prepar_payment', 'payment', 'paid', 'complete']])
            ->andWhere(['not', ['ac.client_group_id' => null]])
            ->andWhere(['!=', 'bill.doc_type', Bill::BILL_DOC_TYPE_AVANS])                
            ->andWhere(['between', 'IF(bill.services_period_till IS NOT NULL, bill.services_period_till, bill.doc_date)', $start_date, $end_date.' 23:59:59'])
            ->andWhere(['!=', 'bill.deleted', 1])
            ->andWhere(['ab.abonent_id' => $this->userAbonentId])
            ->groupBy('a.first_client_id');
        
        $query->leftJoin(['debet' => $subBillQuery], 'debet.client_id = client.id');

        $subBillQuery = (new Query())
            ->select([
                'client_id' => 'a.second_client_id',
                'total' => 'SUM(IF(bill.second_client_pvn_payer, bill.summa_eur, bill.total_eur))',
            ])  
            ->from('bill')
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->leftJoin(['c' => 'client'], 'c.id = a.second_client_id')
            ->leftJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id')
            ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = c.id')
            ->andWhere(['in', 'bill.status', ['signed', 'prepar_payment', 'payment', 'paid', 'complete']])
            ->andWhere(['not', ['ac.client_group_id' => null]])
            ->andWhere(['!=', 'bill.doc_type', Bill::BILL_DOC_TYPE_AVANS])                
            ->andWhere(['between', 'IF(bill.services_period_till IS NOT NULL, bill.services_period_till, bill.doc_date)', $start_date, $end_date.' 23:59:59'])
            ->andWhere(['!=', 'bill.deleted', 1])
            ->andWhere(['!=', 'bill.not_expenses', 1])
            ->andWhere(['ab.abonent_id' => $this->userAbonentId])
            ->groupBy('a.second_client_id');
        
        $query->leftJoin(['credit' => $subBillQuery], 'credit.client_id = client.id');
        
        $subExpenseQuery = (new Query())
            ->select([
                'client_id' => 'expense.second_client_id',
                'total' => 'SUM(IF(expense.second_client_pvn_payer, expense.summa_eur, expense.total_eur))',
            ]) 
            ->from('expense')
            ->andWhere(['report_plus' => 0]) 
            ->andWhere(['between', 'doc_date', $start_date, $end_date.' 23:59:59']) 
            ->andWhere(['expense.abonent_id' => $this->userAbonentId])
            ->groupBy('second_client_id');
        
        $query->leftJoin(['expense_base' => $subExpenseQuery], 'expense_base.client_id = client.id');
        
        $subExpenseQuery = (new Query())
            ->select([
                'client_id' => 'expense.second_client_id',
                'total' => 'SUM(total_eur)'
            ]) 
            ->from('expense')
            ->andWhere(['report_plus' => 1]) 
            ->andWhere(['between', 'doc_date', $start_date, $end_date.' 23:59:59']) 
            ->andWhere(['expense.abonent_id' => $this->userAbonentId])
            ->groupBy('second_client_id');
        
        $query->leftJoin(['expense_plus' => $subExpenseQuery], 'expense_plus.client_id = client.id');
        
        $query
            ->andWhere(['!=', 'client.deleted', 1])
            ->andWhere('(debet.total != 0) OR (credit.total != 0) OR (expense_base.total != 0) OR (expense_plus.total != 0)')
            ->andWhere(['ac.abonent_id' => $this->userAbonentId])
            ->andWhere(['not', ['ac.client_group_id' => null]]);

        if(!empty($clientIds)){
            $query->andWhere(['in', 'client.id', $clientIds]);
        }
        
        if(!isset($params['sort'])){
            $query->addOrderBy('name');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 0,
            ],
        ]);
        
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'client.id' => $this->client_id,
        ]);
        
        /*
        SELECT 
            `client`.`id` AS `client_id`, 
            `client`.`name` AS `client_name`, 
            `debet`.`total` AS `client_sales`, 
            (IF(credit.total, credit.total, 0) + 
                IF(expense_base.total, expense_base.total, 0) + 
                IF(expense_plus.total, expense_plus.total, 0)
            ) AS `client_purchases`, 
            (IF(debet.total, debet.total, 0) - 
                IF(credit.total, credit.total, 0) - 
                IF(expense_base.total, expense_base.total, 0) - 
                IF(expense_plus.total, expense_plus.total, 0)
            ) AS `client_profit`
        FROM 
            `client`
            LEFT JOIN `abonent_client` `ac` ON ac.client_id = client.id 
            LEFT JOIN (
                SELECT 
                    `a`.`first_client_id` AS `client_id`, 
                    SUM(bill.summa_eur) AS `total` 
                FROM 
                    `bill` 
                    LEFT JOIN `agreement` `a` ON a.id = bill.agreement_id 
                    LEFT JOIN `client` `c` ON c.id = a.first_client_id 
                    LEFT JOIN `abonent_bill` `ab` ON ab.bill_id = bill.id 
                    LEFT JOIN `abonent_client` `ac` ON ac.client_id = c.id 
                WHERE 
                    (`bill`.`status` IN (:qp0, :qp1, :qp2, :qp3, :qp4)) AND 
                    (NOT (`ac`.`client_group_id` IS NULL)) AND 
                    (`bill`.`doc_type` != :qp5) AND 
                    (IF(bill.services_period_till IS NOT NULL, bill.services_period_till, bill.doc_date) BETWEEN :qp6 AND :qp7) AND 
                    (`bill`.`deleted` != :qp8) AND 
                    (`ab`.`abonent_id`=:qp9) 
                GROUP BY `a`.`first_client_id`
            ) `debet` ON debet.client_id = client.id 

            LEFT JOIN (
                SELECT 
                    `a`.`second_client_id` AS `client_id`, 
                    SUM(IF(bill.second_client_pvn_payer, bill.summa_eur, bill.total_eur)) AS `total` 
                FROM 
                    `bill` 
                    LEFT JOIN `agreement` `a` ON a.id = bill.agreement_id 
                    LEFT JOIN `client` `c` ON c.id = a.second_client_id 
                    LEFT JOIN `abonent_bill` `ab` ON ab.bill_id = bill.id 
                    LEFT JOIN `abonent_client` `ac` ON ac.client_id = c.id 
                WHERE 
                    (`bill`.`status` IN (:qp10, :qp11, :qp12, :qp13, :qp14)) AND 
                    (NOT (`ac`.`client_group_id` IS NULL)) AND 
                    (`bill`.`doc_type` != :qp15) AND 
                    (IF(bill.services_period_till IS NOT NULL, bill.services_period_till, bill.doc_date) BETWEEN :qp16 AND :qp17) AND 
                    (`bill`.`deleted` != :qp18) AND 
                    (`ab`.`abonent_id`=:qp19) 
                GROUP BY `a`.`second_client_id`
            ) `credit` ON credit.client_id = client.id 

            LEFT JOIN (
                SELECT 
                    `expense`.`second_client_id` AS `client_id`, 
                    SUM(IF(expense.second_client_pvn_payer, expense.summa_eur, expense.total_eur)) AS `total`
                FROM 
                    `expense` 
                WHERE   
                    (`report_plus` = 0) AND
                    (`doc_date` BETWEEN :qp20 AND :qp21) AND 
                    (`expense`.`abonent_id`=:qp22) 
                GROUP BY `second_client_id`
            ) expense_base ON expense_base.client_id = client.id

            LEFT JOIN (
                SELECT 
                    `expense`.`second_client_id` AS `client_id`, 
                    SUM(total_eur) AS `total` 
                FROM 
                    `expense` 
                WHERE   
                    (`report_plus` = 1) AND
                    (`doc_date` BETWEEN :qp20 AND :qp21) AND 
                    (`expense`.`abonent_id`=:qp22) 
                GROUP BY `second_client_id`
            ) expense_plus ON expense_plus.client_id = client.id

        WHERE 
            (`client`.`deleted` != :qp23) AND 
            ((debet.total != 0) OR (credit.total != 0) OR (expense_base.total != 0) OR (expense_plus.total != 0)) AND 
            (`ac`.`abonent_id`=:qp24)
 
        ORDER BY `name
         * 
         */
        
        //return $query->all();
        return $dataProvider;
    } 
    
    public function searchEbitdaClientBaseReport($params, $clientIds = [])
    {
        $this->clearDefaultValues();
        
        $start_date = !empty($_GET['from']) ? date('Y-m-d', strtotime($_GET['from'])) : date('Y-01-01');
        $end_date = isset($_GET['till']) ? (!empty($_GET['till']) ? date("Y-m-d", strtotime($_GET['till'])) : date("Y-m-t")) : date("Y-m-t");
        
        $query = new ActiveQuery(Bill::class);
        $query->select([
                'client_id' => 'client.id',
                'client_name' => 'client.name',
                'client_sales' => 'debet.total',
                'client_purchases' => '(IF(credit.total, credit.total, 0) + IF(expense.total, expense.total, 0))',
                'client_profit' => '(IF(debet.total, debet.total, 0) - IF(credit.total, credit.total, 0) - IF(expense.total, expense.total, 0))',
            ])
            ->from('client')
            ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = client.id');
        
        $subBillQuery = (new Query())
            ->select([
                'client_id' => 'a.first_client_id',
                'total' => 'SUM(bill.summa_eur)',
            ])  
            ->from('bill')
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->leftJoin(['c' => 'client'], 'c.id = a.first_client_id')
            ->leftJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id')
            ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = c.id')
            ->andWhere(['in', 'bill.status', ['signed', 'prepar_payment', 'payment', 'paid', 'complete']])
            ->andWhere(['not', ['ac.client_group_id' => null]])
            ->andWhere(['!=', 'bill.doc_type', Bill::BILL_DOC_TYPE_AVANS])                
            ->andWhere(['between', 'IF(bill.services_period_till IS NOT NULL, bill.services_period_till, bill.doc_date)', $start_date, $end_date.' 23:59:59'])
            ->andWhere(['!=', 'bill.deleted', 1])
            ->andWhere(['ab.abonent_id' => $this->userAbonentId])
            ->groupBy('a.first_client_id');
        
        $query->leftJoin(['debet' => $subBillQuery], 'debet.client_id = client.id');

        $subBillQuery = (new Query())
            ->select([
                'client_id' => 'a.second_client_id',
                'total' => 'SUM(IF(bill.second_client_pvn_payer, bill.summa_eur, bill.total_eur))',
            ])  
            ->from('bill')
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->leftJoin(['c' => 'client'], 'c.id = a.second_client_id')
            ->leftJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id')
            ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = c.id')
            ->andWhere(['in', 'bill.status', ['signed', 'prepar_payment', 'payment', 'paid', 'complete']])
            ->andWhere(['not', ['ac.client_group_id' => null]])
            ->andWhere(['!=', 'bill.doc_type', Bill::BILL_DOC_TYPE_AVANS])                
            ->andWhere(['between', 'IF(bill.services_period_till IS NOT NULL, bill.services_period_till, bill.doc_date)', $start_date, $end_date.' 23:59:59'])
            ->andWhere(['!=', 'bill.deleted', 1])
            ->andWhere(['!=', 'bill.not_expenses', 1])
            ->andWhere(['ab.abonent_id' => $this->userAbonentId])
            ->groupBy('a.second_client_id');
        
        $query->leftJoin(['credit' => $subBillQuery], 'credit.client_id = client.id');
        
        $subExpenseQuery = (new Query())
            ->select([
                'client_id' => 'expense.second_client_id',
                'total' => 'SUM(IF(expense.second_client_pvn_payer, expense.summa_eur, expense.total_eur))',
            ]) 
            ->from('expense')
            ->andWhere(['report_plus' => 0]) 
            ->andWhere(['between', 'doc_date', $start_date, $end_date.' 23:59:59']) 
            ->andWhere(['expense.abonent_id' => $this->userAbonentId])
            ->groupBy('second_client_id');
        
        $query->leftJoin(['expense' => $subExpenseQuery], 'expense.client_id = client.id');
        
        $query
            ->andWhere(['!=', 'client.deleted', 1])
            ->andWhere('(debet.total != 0) OR (credit.total != 0) OR (expense.total != 0)')
            ->andWhere(['ac.abonent_id' => $this->userAbonentId])
            ->andWhere(['not', ['ac.client_group_id' => null]]);

        if(!empty($clientIds)){
            $query->andWhere(['in', 'client.id', $clientIds]);
        }
        
        if(!isset($params['sort'])){
            $query->addOrderBy('name');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 0,
            ],
        ]);
        
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'client.id' => $this->client_id,
        ]);
        
        /*
        SELECT 
            `client`.`id` AS `client_id`, 
            `client`.`name` AS `client_name`, 
            `debet`.`total` AS `client_sales`, 
            (IF(credit.total, credit.total, 0) + IF(expense.total, expense.total, 0)) AS `client_purchases`, 
            (IF(debet.total, debet.total, 0) - IF(credit.total, credit.total, 0) - IF(expense.total, expense.total, 0)) AS `client_profit`,
        FROM 
            `client`
            LEFT JOIN `abonent_client` `ac` ON ac.client_id = client.id 
            LEFT JOIN (
                SELECT 
                    `a`.`first_client_id` AS `client_id`, 
                    SUM(bill.summa_eur) AS `total` 
                FROM 
                    `bill` 
                    LEFT JOIN `agreement` `a` ON a.id = bill.agreement_id 
                    LEFT JOIN `client` `c` ON c.id = a.first_client_id 
                    LEFT JOIN `abonent_bill` `ab` ON ab.bill_id = bill.id 
                    LEFT JOIN `abonent_client` `ac` ON ac.client_id = c.id 
                WHERE 
                    (`bill`.`status` IN (:qp0, :qp1, :qp2, :qp3, :qp4)) AND 
                    (NOT (`ac`.`client_group_id` IS NULL)) AND 
                    (`bill`.`doc_type` != :qp5) AND 
                    (`bill`.`doc_date` BETWEEN :qp6 AND :qp7) AND 
                    (`bill`.`deleted` != :qp8) AND 
                    (`ab`.`abonent_id`=:qp9) 
                GROUP BY `a`.`first_client_id`
            ) `debet` ON debet.client_id = client.id 

            LEFT JOIN (
                SELECT 
                    `a`.`second_client_id` AS `client_id`, 
                    SUM(IF(bill.second_client_pvn_payer, bill.summa_eur, bill.total_eur)) AS `total`
                FROM 
                    `bill` 
                    LEFT JOIN `agreement` `a` ON a.id = bill.agreement_id 
                    LEFT JOIN `client` `c` ON c.id = a.second_client_id 
                    LEFT JOIN `abonent_bill` `ab` ON ab.bill_id = bill.id 
                    LEFT JOIN `abonent_client` `ac` ON ac.client_id = c.id 
                WHERE 
                    (`bill`.`status` IN (:qp10, :qp11, :qp12, :qp13, :qp14)) AND 
                    (NOT (`ac`.`client_group_id` IS NULL)) AND 
                    (`bill`.`doc_type` != :qp15) AND 
                    (`bill`.`doc_date` BETWEEN :qp16 AND :qp17) AND 
                    (`bill`.`deleted` != :qp18) AND
                    (`bill`.`not_expenses` != 1) AND 
                    (`ab`.`abonent_id`=:qp19) 
                GROUP BY `a`.`second_client_id`
            ) `credit` ON credit.client_id = client.id 

            LEFT JOIN (
                SELECT 
                    `expense`.`second_client_id` AS `client_id`, 
                    SUM(IF(expense.second_client_pvn_payer, expense.summa_eur, expense.total_eur)) AS `total`
                FROM 
                    `expense` 
                WHERE 
                    (`expense`.`report_plus` = 0) AND
                    (`doc_date` BETWEEN :qp20 AND :qp21) AND 
                    (`expense`.`abonent_id`=:qp22) 
                GROUP BY `second_client_id`
            ) `expense` ON expense.client_id = client.id

        WHERE 
            (`client`.`deleted` != :qp23) AND 
            ((debet.total != 0) OR (credit.total != 0) OR (expense.total != 0)) AND 
            (`ac`.`abonent_id`=:qp24)
 
        ORDER BY `name
         * 
         */
        
        //return $query->all();
        return $dataProvider;
    }    
        
    public function searchVatReport($params, $clientIds = [])
    {
        $this->clearDefaultValues();
        
        $start_date = !empty($_GET['from']) ? date('Y-m-d', strtotime($_GET['from'])) : date('Y-01-01');
        $end_date = isset($_GET['till']) ? (!empty($_GET['till']) ? date("Y-m-d", strtotime($_GET['till'])) : date("Y-m-t")) : date("Y-m-t");
        
        $query = new ActiveQuery(Bill::class);
        $query->select([
            'client_id' => 'client.id',
            'client_name' => 'client.name',
            
            'client_sales' => 
                '(debet.summa - '.
                'IF(debet_avans_paid.summa, debet_avans_paid.summa, 0)) + '.
                'IF(debet_avans.summa, debet_avans.summa, 0)',
            
            'client_vat_plus' => 
                '(debet.vat - '.
                'IF(debet_avans_paid.vat, debet_avans_paid.vat, 0)) + '.
                'IF(debet_avans.vat, debet_avans.vat, 0)',
            
            'client_purchases' => 
                '(credit.summa - '.
                'IF(credit_avans_paid.summa, credit_avans_paid.summa, 0)) + '.
                'IF(credit_avans.summa, credit_avans.summa, 0)',
            
            'client_vat_minus' => 
                '(credit.vat - '.
                'IF(credit_avans_paid.vat, credit_avans_paid.vat, 0)) + '.
                'IF(credit_avans.vat, credit_avans.vat, 0) + '.
                'IF(expense.vat, expense.vat, 0)',
            
            'client_vat_result' => 
                '(IF(debet.vat, 
                    debet.vat - IF(debet_avans_paid.vat, debet_avans_paid.vat, 0) + IF(debet_avans.vat, debet_avans.vat, 0), 
                    IF(debet_avans.vat, debet_avans.vat, 0)
                ) - 
                IF(credit.vat, 
                    credit.vat - IF(credit_avans_paid.vat, credit_avans_paid.vat, 0) + IF(credit_avans.vat, credit_avans.vat, 0) + IF(expense.vat, expense.vat, 0), 
                    IF(credit_avans.vat, credit_avans.vat, 0) + IF(expense.vat, expense.vat, 0))
                )',
            
            'expense_ids' => 'IF(expense.ids, expense.ids, "")',
        ])
        ->from('client')
        ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = client.id');
        
        $subQuery = (new Query())
            ->select([
                'client_id' => 'first_client_id',
                'summa' => 'SUM(bill.summa_eur)',
                'vat' => 'SUM(vat_eur)',
            ])  
            ->from('bill')
            ->innerJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id and ab.abonent_id = '.$this->userAbonentId)
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->andWhere(['in', 'bill.status', ['signed', 'prepar_payment', 'payment', 'paid', 'complete']])
            ->andWhere(['!=', 'bill.doc_type', Bill::BILL_DOC_TYPE_AVANS])                
            ->andWhere(['between', 'bill.doc_date', $start_date, $end_date.' 23:59:59'])
            ->andWhere(['!=', 'bill.deleted', 1]) 
            ->groupBy('first_client_id');
        $query->leftJoin(['debet' => $subQuery], 'debet.client_id = client.id');
        
        $subQuery = (new Query())
            ->select([
                'client_id' => 'first_client_id',
                'summa' => 'SUM(bill.summa_eur)',
                'vat' => 'SUM(vat_eur)',
            ])  
            ->from('bill')
            ->innerJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id and ab.abonent_id = '.$this->userAbonentId)
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->andWhere(['in', 'bill.status', ['complete']])
            ->andWhere(['bill.doc_type' => Bill::BILL_DOC_TYPE_AVANS])                 
            ->andWhere(['between', 'bill.paid_date', $start_date, $end_date.' 23:59:59'])
            ->andWhere(['!=', 'bill.deleted', 1])                 
            ->andWhere(['not', ['bill.child_id' => null]])                 
            ->groupBy('first_client_id');
        $query->leftJoin(['debet_avans_paid' => $subQuery], 'debet_avans_paid.client_id = client.id');
        
        $subQuery = (new Query())
            ->select([
                'client_id' => 'first_client_id',
                'summa' => 'SUM(bill.summa_eur)',
                'vat' => 'SUM(vat_eur)',
            ])  
            ->from('bill')
            ->innerJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id and ab.abonent_id = '.$this->userAbonentId)
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->andWhere(['in', 'bill.status', ['complete']])
            ->andWhere(['bill.doc_type' => Bill::BILL_DOC_TYPE_AVANS])                
            ->andWhere(['between', 'bill.paid_date', $start_date, $end_date.' 23:59:59'])
            ->andWhere(['!=', 'bill.deleted', 1])                 
            ->andWhere(['bill.child_id' => null])                 
            ->groupBy('first_client_id');
        $query->leftJoin(['debet_avans' => $subQuery], 'debet_avans.client_id = client.id');
        
        $subQuery = (new Query())
            ->select([
                'client_id' => 'second_client_id',
                'summa' => 'SUM(bill.summa_eur)',
                'vat' => 'SUM(vat_eur)',
            ])  
            ->from('bill')
            ->innerJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id and ab.abonent_id = '.$this->userAbonentId)
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->leftJoin(['cl' => 'client'], 'cl.id = a.second_client_id')
            ->andWhere(['cl.vat_payer' => 1])
            ->andWhere(['in', 'bill.status', ['signed', 'prepar_payment', 'payment', 'paid', 'complete']])
            ->andWhere(['!=', 'bill.doc_type', Bill::BILL_DOC_TYPE_AVANS])                
            ->andWhere(['between', 'bill.doc_date', $start_date, $end_date.' 23:59:59'])
            ->andWhere(['!=', 'bill.deleted', 1])                   
            ->groupBy('second_client_id');
        $query->leftJoin(['credit' => $subQuery], 'credit.client_id = client.id');
        
        $subQuery = (new Query())
            ->select([
                'client_id' => 'second_client_id',
                'summa' => 'SUM(bill.summa_eur)',
                'vat' => 'SUM(vat_eur)',
            ])  
            ->from('bill')
            ->innerJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id and ab.abonent_id = '.$this->userAbonentId)
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->leftJoin(['cl' => 'client'], 'cl.id = a.second_client_id')
            ->andWhere(['cl.vat_payer' => 1])
            ->andWhere(['in', 'bill.status', ['complete']])
            ->andWhere(['bill.doc_type' => Bill::BILL_DOC_TYPE_AVANS])                 
            ->andWhere(['between', 'bill.paid_date', $start_date, $end_date.' 23:59:59'])
            ->andWhere(['!=', 'bill.deleted', 1])                   
            ->andWhere(['not', ['bill.child_id' => null]])
            ->groupBy('second_client_id');
        $query->leftJoin(['credit_avans_paid' => $subQuery], 'credit_avans_paid.client_id = client.id');
        
        $subQuery = (new Query())
            ->select([
                'client_id' => 'second_client_id',
                'summa' => 'SUM(bill.summa_eur)',
                'vat' => 'SUM(vat_eur)',
            ])  
            ->from('bill')
            ->innerJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id and ab.abonent_id = '.$this->userAbonentId)
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->leftJoin(['cl' => 'client'], 'cl.id = a.second_client_id')
            ->andWhere(['cl.vat_payer' => 1])
            ->andWhere(['in', 'bill.status', ['complete']])
            ->andWhere(['bill.doc_type' => Bill::BILL_DOC_TYPE_AVANS])                 
            ->andWhere(['between', 'bill.paid_date', $start_date, $end_date.' 23:59:59'])
            ->andWhere(['!=', 'bill.deleted', 1])                   
            ->andWhere(['bill.child_id' => null])                 
            ->groupBy('second_client_id');
        $query->leftJoin(['credit_avans' => $subQuery], 'credit_avans.client_id = client.id');
       
        $subQuery = (new Query())
            ->select([
                'client_id' => 'expense.second_client_id',
                'vat' => 'SUM(vat_eur)',
                'ids' => 'GROUP_CONCAT(DISTINCT expense.id ORDER BY expense.id DESC SEPARATOR ",")',
            ]) 
            ->from('expense')
            ->andWhere(['report_plus' => 0]) 
            ->andWhere(['between', 'doc_date', $start_date, $end_date.' 23:59:59']) 
            ->andWhere(['expense.abonent_id' => $this->userAbonentId])
            ->groupBy('second_client_id');
        
        $query->leftJoin(['expense' => $subQuery], 'expense.client_id = client.id');
        
        $query
            ->andWhere(['client.vat_payer' => 1])
            ->andWhere('(debet.vat IS NOT NULL OR credit.vat IS NOT NULL OR debet_avans.vat IS NOT NULL OR credit_avans.vat IS NOT NULL)')
            ->andWhere(['not', ['ac.client_group_id' => null]])
            ->andWhere(['ac.abonent_id' => $this->userAbonentId]);
        
        if(!empty($clientIds)){
            $query->andWhere(['in', 'client.id', $clientIds]);
        }
        
        if(!isset($params['sort'])){
            $query->addOrderBy('name');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 0,
            ],
        ]);
        
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->andFilterWhere([
            'client.id' => $this->client_id,
        ]);
        
        /*
        SELECT 
            `client`.`id` AS `client_id`, 
            `client`.`name` AS `client_name`, 
         
            (debet.summa -
                IF(debet_avans_paid.summa, debet_avans_paid.summa, 0) + 
                IF(debet_avans.summa, debet_avans.summa, 0)
            ) AS `client_sales`, 
        
            (debet.vat - 
                IF(debet_avans_paid.vat, debet_avans_paid.vat, 0) + 
                IF(debet_avans.vat, debet_avans.vat, 0)
            ) AS `client_vat_plus`,
          
            (credit.summa - 
                IF(credit_avans_paid.summa, credit_avans_paid.summa, 0) + 
                IF(credit_avans.summa, credit_avans.summa, 0)
            ) AS `client_purchases`, 
         
            (credit.vat - 
                IF(credit_avans_paid.vat, credit_avans_paid.vat, 0) + 
                IF(credit_avans.vat, credit_avans.vat, 0) + 
                IF(expense.vat, expense.vat, 0)
            ) AS `client_vat_minus`, 
         
            (
                IF(debet.vat, 
                    debet.vat - 
                        IF(debet_avans_paid.vat, debet_avans_paid.vat, 0) + 
                        IF(debet_avans.vat, debet_avans.vat, 0), 
                    IF(debet_avans.vat, debet_avans.vat, 0)
                ) - 
                IF(credit.vat, 
                    credit.vat - 
                        IF(credit_avans_paid.vat, credit_avans_paid.vat, 0) + 
                        IF(credit_avans.vat, credit_avans.vat, 0) + 
                        IF(expense.vat, expense.vat, 0), 
                    IF(credit_avans.vat, credit_avans.vat, 0) + IF(expense.vat, expense.vat, 0)
                )
            ) AS `client_vat_result`, 
         
            CONCAT(IF(expense.ids, expense.ids, "")) AS `expense_ids`
        FROM 
            `client`
            LEFT JOIN `abonent_client` `ac` ON ac.client_id = client.id
            LEFT JOIN (
                SELECT 
                    `first_client_id` AS `client_id`, 
                    SUM(bill.summa_eur) AS `summa`, 
                    SUM(vat_eur) AS `vat`
                FROM 
                    `bill` 
                    INNER JOIN `abonent_bill` `ab` ON ab.bill_id = bill.id AND `ab`.`abonent_id`=:qp9
                    LEFT JOIN `agreement` `a` ON a.id = bill.agreement_id 
                WHERE 
                    (`bill`.`status` IN (:qp0, :qp1, :qp2, :qp3, :qp4)) AND 
                    (`bill`.`doc_type` != :qp5) AND 
                    (`bill`.`doc_date` BETWEEN :qp6 AND :qp7) AND 
                    (`bill`.`deleted` != :qp8)
                GROUP BY `first_client_id`
            ) `debet` ON debet.client_id = client.id

            LEFT JOIN (
                SELECT 
                    `first_client_id` AS `client_id`, 
                    SUM(bill.summa_eur) AS `summa`,
                    SUM(bill.vat_eur) AS `vat` 
		FROM 
                    `bill` 
                    INNER JOIN `abonent_bill` `ab` ON ab.bill_id = bill.id AND `ab`.`abonent_id`=1
                    LEFT JOIN `agreement` `a` ON a.id = bill.agreement_id
		WHERE 
                    (`bill`.`status`='complete') AND 
                    (`bill`.`doc_type` = "avans") AND
                    (`bill`.`paid_date` BETWEEN "2019-01-01" AND "2019-08-31 23:59:59") AND 
                    (`bill`.`deleted` != 1) AND 
                    (`bill`.`child_id` IS NOT NULL)
                GROUP BY `first_client_id`
            ) `debet_avans_paid` ON debet_avans_paid.client_id = client.id

            LEFT JOIN (
                SELECT 
                    `first_client_id` AS `client_id`, 
                    SUM(bill.summa_eur) AS `summa`,
                    SUM(bill.vat_eur) AS `vat` 
		FROM 
                    `bill` 
                    INNER JOIN `abonent_bill` `ab` ON ab.bill_id = bill.id AND `ab`.`abonent_id`=1
                    LEFT JOIN `agreement` `a` ON a.id = bill.agreement_id
		WHERE 
                    (`bill`.`status`='complete') AND 
                    (`bill`.`doc_type` = "avans") AND
                    (`bill`.`paid_date` BETWEEN "2019-01-01" AND "2019-08-31 23:59:59") AND 
                    (`bill`.`deleted` != 1) AND 
                    (`bill`.`child_id` IS NULL)
                GROUP BY `first_client_id`
            ) `debet_avans` ON debet_avans.client_id = client.id

            LEFT JOIN (
                SELECT 
                    `second_client_id` AS `client_id`, 
                    SUM(bill.summa_eur) AS `summa`, 
                    SUM(vat_eur) AS `vat`, 
                    GROUP_CONCAT(DISTINCT bill.id ORDER BY bill.id DESC SEPARATOR ",") AS `ids` 
                FROM 
                    `bill` 
                    INNER JOIN `abonent_bill` `ab` ON ab.bill_id = bill.id AND `ab`.`abonent_id`=:qp9
                    LEFT JOIN `agreement` `a` ON a.id = bill.agreement_id 
                WHERE 
                    (`bill`.`status` IN (:qp16, :qp17, :qp18, :qp19, :qp20)) AND 
                    (`bill`.`doc_type` != :qp21) AND 
                    (`bill`.`doc_date` BETWEEN :qp22 AND :qp23) AND 
                    (`bill`.`deleted` != :qp24)
                GROUP BY `second_client_id`
            ) `credit` ON credit.client_id = client.id

            LEFT JOIN (
                SELECT 
                    `second_client_id` AS `client_id`, 
                    SUM(bill.summa_eur) AS `summa`,
                    SUM(bill.vat_eur) AS `vat` 
		FROM 
                    `bill` 
                    INNER JOIN `abonent_bill` `ab` ON ab.bill_id = bill.id AND `ab`.`abonent_id`=1
                    LEFT JOIN `agreement` `a` ON a.id = bill.agreement_id
		WHERE 
                    (`bill`.`status`='complete') AND 
                    (`bill`.`doc_type` = "avans") AND
                    (`bill`.`paid_date` BETWEEN "2019-01-01" AND "2019-08-31 23:59:59") AND 
                    (`bill`.`deleted` != 1) AND 
                    (`bill`.`child_id` IS NOT NULL)
                GROUP BY `second_client_id`
            ) `credit_avans_paid` ON credit_avans_paid.client_id = client.id

            LEFT JOIN (
                SELECT 
                    `second_client_id` AS `client_id`, 
                    SUM(bill.summa_eur) AS `summa`,
                    SUM(bill.vat_eur) AS `vat` 
		FROM 
                    `bill` 
                    INNER JOIN `abonent_bill` `ab` ON ab.bill_id = bill.id AND `ab`.`abonent_id`=1
                    LEFT JOIN `agreement` `a` ON a.id = bill.agreement_id
		WHERE 
                    (`bill`.`status`='complete') AND 
                    (`bill`.`doc_type` = "avans") AND
                    (`bill`.`paid_date` BETWEEN "2019-01-01" AND "2019-08-31 23:59:59") AND 
                    (`bill`.`deleted` != 1) AND 
                    (`bill`.`child_id` IS NULL)
                GROUP BY `second_client_id`
            ) `credit_avans` ON credit_avans.client_id = client.id

            LEFT JOIN (
                SELECT 
                    `second_client_id` AS `client_id`, 
                    SUM(vat_eur) AS `vat`, 
                    GROUP_CONCAT(DISTINCT expense.id ORDER BY expense.id DESC SEPARATOR ",") AS `ids` 
                FROM 
                    `expense` 
                WHERE 
                    (`doc_date` BETWEEN :qp20 AND :qp21) AND 
                    (`expense`.`abonent_id`=:qp22) 
                GROUP BY `second_client_id`
            ) `expense` ON expense.client_id = client.id
        WHERE 
            (`client`.`vat_payer` = 1) AND 
            (debet.vat IS NOT NULL OR credit.vat IS NOT NULL OR debet_avans.vat IS NOT NULL OR credit_avans.vat IS NOT NULL) AND 
            (`ac`.`client_group_id` IS NOT NULL) AND 
            (`ac`.`abonent_id`=:qp32)
         
        ORDER BY `name`
         * 
         */
        
        //return $query->all();
        return $dataProvider;
    }    
    
    public function searchDebitorCreditorReport($params, $clientIds = [])
    {
        $this->clearDefaultValues();
        
        //$start_date = !empty($_GET['from']) ? date('Y-m-d', strtotime($_GET['from'])) : date('Y-01-01');
        $start_date = '1900-01-01';
        $end_date = isset($_GET['till']) ? (!empty($_GET['till']) ? date("Y-m-d", strtotime($_GET['till'])) : date("Y-m-t")) : date("Y-m-t");
        
        $query = new ActiveQuery(Bill::class);
        $query->select([
            'client_id' => 'client.id',
            'client_name' => 'client.name',
            'debtors_summa' => 'IF(debet_agr.summa, debet.total + debet_agr.summa, debet.total) '
            . ' - IF(debet_payments.total, debet_payments.total, 0) '
            . ' - IF(debet_cr.total, debet_cr.total, 0)',
            'creditors_summa' => 'IF(credit_agr.summa, credit.total + credit_agr.summa, credit.total) '
            . ' - IF(credit_payments.total, credit_payments.total, 0) '
            . ' - IF(credit_cr.total, credit_cr.total, 0)',
            ])
            ->from('client')
            ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = client.id');
        
        $subQuery = (new Query())
            ->select([
                'client_id' => 'first_client_id',
                'total' => 'SUM(bill.total_eur)'
            ])  
            ->from('bill')
            ->leftJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id')
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->andWhere(['in', 'bill.status', ['signed', 'prepar_payment', 'payment', 'paid', 'complete']])
            ->andWhere(['or',
                ['in', 'bill.pay_status', ['not', 'part']],
                ['>', 'bill.paid_date', $end_date]
            ])
            ->andWhere(['in', 'bill.doc_type', 
                [
                    Bill::BILL_DOC_TYPE_BILL, 
                    Bill::BILL_DOC_TYPE_INVOICE,
                ]
            ])
            ->andWhere(['between', 'bill.doc_date', $start_date, $end_date.' 23:59:59'])
            ->andWhere(['!=', 'bill.deleted', 1])
            ->andWhere(['ab.abonent_id' => $this->userAbonentId])
            ->groupBy('first_client_id');
        $query->leftJoin(['debet' => $subQuery], 'debet.client_id = client.id');
        
        $subQuery = (new Query())
            ->select([
                'client_id' => 'first_client_id',
                'total' => 'ABS(SUM(bill.total_eur))'
            ])  
            ->from('bill')
            ->leftJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id')
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->andWhere(['in', 'bill.status', ['signed', 'prepar_payment', 'payment', 'paid', 'complete']])
            ->andWhere(['or',
                ['in', 'bill.pay_status', ['not', 'part']],
                ['>', 'bill.paid_date', $end_date]
            ])
            ->andWhere(['bill.doc_type' => Bill::BILL_DOC_TYPE_CRBILL])
            ->andWhere(['between', 'bill.doc_date', $start_date, $end_date.' 23:59:59'])
            ->andWhere(['!=', 'bill.deleted', 1])
            ->andWhere(['ab.abonent_id' => $this->userAbonentId])
            ->groupBy('first_client_id');
        $query->leftJoin(['debet_cr' => $subQuery], 'debet_cr.client_id = client.id');
        
        $subQuery = (new Query())
            ->select([
                'client_id' => 'first_client_id',
                'total' => 'SUM(bp.summa_eur)',
            ])  
            ->from('bill')
            ->leftJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id')
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->leftJoin(['bp' => 'bill_payment'], 'bp.bill_id = bill.id AND (bp.paid_date <= "'.$end_date.'")')
            ->andWhere(['in', 'bill.status', ['signed', 'prepar_payment', 'payment', 'paid', 'complete']])
            ->andWhere(['or',
                ['in', 'bill.pay_status', ['not', 'part']],
                ['>', 'bill.paid_date', $end_date]
            ])
            ->andWhere(['in', 'bill.doc_type', 
                [
                    Bill::BILL_DOC_TYPE_BILL, 
                    Bill::BILL_DOC_TYPE_INVOICE,
                ]
            ])
            ->andWhere(['between', 'bill.doc_date', $start_date, $end_date.' 23:59:59'])
            ->andWhere(['!=', 'bill.deleted', 1])
            ->andWhere(['ab.abonent_id' => $this->userAbonentId])
            ->groupBy('first_client_id');
        $query->leftJoin(['debet_payments' => $subQuery], 'debet_payments.client_id = client.id');
        
        $subQuery = (new Query())
            ->select([
                'client_id' => 'second_client_id',
                'total' => 'SUM(bill.total_eur)',
            ])  
            ->from('bill')
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->leftJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id')
            ->andWhere(['in', 'bill.status', ['signed', 'prepar_payment', 'payment', 'paid', 'complete']])
            ->andWhere(['or',
                ['in', 'bill.pay_status', ['not', 'part']],
                ['>', 'bill.paid_date', $end_date]
            ])
            ->andWhere(['in', 'bill.doc_type', 
                [
                    Bill::BILL_DOC_TYPE_BILL, 
                    Bill::BILL_DOC_TYPE_INVOICE,
                ]
            ])
            ->andWhere(['between', 'bill.doc_date', $start_date, $end_date.' 23:59:59'])                
            ->andWhere(['!=', 'bill.deleted', 1])
            ->andWhere(['ab.abonent_id' => $this->userAbonentId])
            ->groupBy('second_client_id');
        $query->leftJoin(['credit' => $subQuery], 'credit.client_id = client.id');
        
        $subQuery = (new Query())
            ->select([
                'client_id' => 'second_client_id',
                'total' => 'ABS(SUM(bill.total_eur))'
            ])  
            ->from('bill')
            ->leftJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id')
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->andWhere(['in', 'bill.status', ['signed', 'prepar_payment', 'payment', 'paid', 'complete']])
            ->andWhere(['or',
                ['in', 'bill.pay_status', ['not', 'part']],
                ['>', 'bill.paid_date', $end_date]
            ])
            ->andWhere(['bill.doc_type' => Bill::BILL_DOC_TYPE_CRBILL])
            ->andWhere(['between', 'bill.doc_date', $start_date, $end_date.' 23:59:59'])
            ->andWhere(['!=', 'bill.deleted', 1])
            ->andWhere(['ab.abonent_id' => $this->userAbonentId])
            ->groupBy('second_client_id');
        $query->leftJoin(['credit_cr' => $subQuery], 'credit_cr.client_id = client.id');
        
        $subQuery = (new Query())
            ->select([
                'client_id' => 'second_client_id',
                'total' => 'SUM(bp.summa_eur)',
            ])  
            ->from('bill')
            ->leftJoin(['a' => 'agreement'], 'a.id = bill.agreement_id')
            ->leftJoin(['bp' => 'bill_payment'], 'bp.bill_id = bill.id AND (bp.paid_date <= "'.$end_date.'")')
            ->leftJoin(['ab' => 'abonent_bill'], 'ab.bill_id = bill.id')
            ->andWhere(['in', 'bill.status', ['signed', 'prepar_payment', 'payment', 'paid', 'complete']])
            ->andWhere(['or',
                ['in', 'bill.pay_status', ['not', 'part']],
                ['>', 'bill.paid_date', $end_date]
            ])
            ->andWhere(['in', 'bill.doc_type', 
                [
                    Bill::BILL_DOC_TYPE_BILL, 
                    Bill::BILL_DOC_TYPE_INVOICE,
                ]
            ])
            ->andWhere(['between', 'bill.doc_date', $start_date, $end_date.' 23:59:59'])                
            ->andWhere(['!=', 'bill.deleted', 1])
            ->andWhere(['ab.abonent_id' => $this->userAbonentId])
            ->groupBy('second_client_id');
        $query->leftJoin(['credit_payments' => $subQuery], 'credit_payments.client_id = client.id');
        
        $subQuery = (new Query())
            ->select([
                'client_id' => 'first_client_id',
                'summa' => 'SUM(ap.summa_eur)',
            ])  
            ->from(['ap' => 'agreement_payment'])
            ->leftJoin(['a' => 'agreement'], 'a.id = ap.agreement_id')
            ->leftJoin(['c' => 'client'], 'c.id = a.first_client_id')
            ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = c.id')
                
            ->andWhere('ap.summa_eur != 0')
            ->andWhere(['not', ['ac.client_group_id' => null]])
            ->andWhere(['between', 'ap.paid_date', $start_date, $end_date.' 23:59:59'])
            ->andWhere(['ap.direction' => 'debet'])
            ->andWhere(['ac.abonent_id' => $this->userAbonentId])
            ->groupBy('first_client_id');
        $query->leftJoin(['debet_agr' => $subQuery], 'debet_agr.client_id = client.id');
        
        $subQuery = (new Query())
            ->select([
                'client_id' => 'second_client_id',
                'summa' => 'SUM(ap.summa_eur)',
            ])  
            ->from(['ap' => 'agreement_payment'])
            ->leftJoin(['a' => 'agreement'], 'a.id = ap.agreement_id')
            ->leftJoin(['c' => 'client'], 'c.id = a.second_client_id')
            ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = c.id')
                
            ->andWhere('ap.summa_eur != 0')
            ->andWhere(['not', ['ac.client_group_id' => null]])
            ->andWhere(['between', 'ap.paid_date', $start_date, $end_date.' 23:59:59'])
            ->andWhere(['ap.direction' => 'credit'])
            ->andWhere(['ac.abonent_id' => $this->userAbonentId])
            ->groupBy('second_client_id');
        $query->leftJoin(['credit_agr' => $subQuery], 'credit_agr.client_id = client.id');

        $query->andWhere('debet.total IS NOT NULL OR credit.total IS NOT NULL OR debet_agr.summa IS NOT NULL OR credit_agr.summa IS NOT NULL')
            ->andWhere(['!=', 'client.deleted', 1])
            ->andWhere(['not', ['ac.client_group_id' => null]])
            ->andWhere(['ac.abonent_id' => $this->userAbonentId]);
        
        if(!empty($clientIds)){
            $query->andWhere(['in', 'client.id', $clientIds]);
        }
        
        if(!isset($params['sort'])){
            $query->addOrderBy('name');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 0,
            ],
        ]);
        
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->andFilterWhere([
            'client.id' => $this->client_id,
        ]);
        
        /*
        SELECT 
            `client`.`id` AS `client_id`, 
            `client`.`name` AS `client_name`, 
            IF(debet_agr.summa, debet.total + debet_agr.summa, debet.total) - 
                IF(debet_payments.total, debet_payments.total, 0) - 
                IF(debet_cr.total, debet_cr.total, 0) 
            AS `debtors_summa`, 
            IF(credit_agr.summa, credit.total + credit_agr.summa, credit.total) -
                IF(credit_payments.total, credit_payments.total, 0) -
                IF(credit_cr.total, credit_cr.total, 0) 
            AS `creditors_summa`
        FROM 
            `client` 
            LEFT JOIN `abonent_client` `ac` ON ac.client_id = client.id 
        
            LEFT JOIN (
                SELECT 
                    `first_client_id` AS `client_id`, 
                    SUM(bill.total_eur) AS `total` 
                FROM 
                    `bill` 
                    LEFT JOIN `agreement` `a` ON a.id = bill.agreement_id 
                    LEFT JOIN `abonent_bill` `ab` ON ab.bill_id = bill.id 
                WHERE 
                    (`bill`.`status` IN ('signed' , 'prepar_payment', 'payment', 'paid', 'complete')) AND 
                    (`bill`.`pay_status` IN ('not' , 'part')) AND 
                    (`bill`.`doc_type` IN ('bill', 'invoice')) AND 
                    (`bill`.`doc_date` BETWEEN '1900-01-01' AND '2019-07-31') AND 
                    (`bill`.`deleted` != 1) AND 
                    (`ab`.`abonent_id`= 1) 
                GROUP BY `first_client_id`
            ) `debet` ON debet.client_id = client.id 
        
            LEFT JOIN (
                SELECT 
                    `first_client_id` AS `client_id`, 
                    ABS(SUM(bill.total_eur)) AS `total` 
                FROM 
                    `bill` 
                    LEFT JOIN `agreement` `a` ON a.id = bill.agreement_id 
                    LEFT JOIN `abonent_bill` `ab` ON ab.bill_id = bill.id 
                WHERE 
                    (`bill`.`status` IN ('signed' , 'prepar_payment', 'payment', 'paid', 'complete')) AND 
                    (`bill`.`pay_status` IN ('not' , 'part')) AND 
                    (`bill`.`doc_type` = 'cr_bill') AND 
                    (`bill`.`doc_date` BETWEEN '1900-01-01' AND '2019-07-31') AND 
                    (`bill`.`deleted` != 1) AND 
                    (`ab`.`abonent_id`= 1) 
                GROUP BY `first_client_id`
            ) `debet_cr` ON debet_cr.client_id = client.id 
        
            LEFT JOIN (
                SELECT 
                    `first_client_id` AS `client_id`, 
                    SUM(bp.summa_eur) AS `total` 
                FROM 
                    `bill` 
                    LEFT JOIN `agreement` `a` ON a.id = bill.agreement_id 
                    LEFT JOIN `abonent_bill` `ab` ON ab.bill_id = bill.id 
                    LEFT JOIN `bill_payment` `bp` ON bp.bill_id = bill.id AND (`bp`.`paid_date` <= '2019-07-25')                
                WHERE 
                    (`bill`.`status` IN ('signed' , 'prepar_payment', 'payment', 'paid', 'complete')) AND 
                    (`bill`.`pay_status` IN ('not' , 'part')) AND 
                    (`bill`.`doc_type` IN ('bill', 'invoice')) AND 
                    (`bill`.`doc_date` BETWEEN '1900-01-01' AND '2019-07-31') AND 
                    (`bill`.`deleted` != 1) AND 
                    (`ab`.`abonent_id`= 1) 
                GROUP BY `first_client_id`
            ) `debet_payments` ON debet_payments.client_id = client.id 

            LEFT JOIN (
                SELECT 
                    `second_client_id` AS `client_id`, 
                    SUM(bill.total_eur) AS `total` 
                FROM 
                    `bill` 
                    LEFT JOIN `agreement` `a` ON a.id = bill.agreement_id 
                    LEFT JOIN `abonent_bill` `ab` ON ab.bill_id = bill.id 
                WHERE 
                    (`bill`.`status` IN ('signed' , 'prepar_payment', 'payment', 'paid', 'complete')) AND 
                    (`bill`.`pay_status` IN ('not' , 'part')) AND 
                    (`bill`.`doc_type` IN ('bill', 'invoice')) AND  
                    (`bill`.`doc_date` BETWEEN '1900-01-01' AND '2019-07-31') AND 
                    (`bill`.`deleted` != 1) AND 
                    (`ab`.`abonent_id`= 1) 
                GROUP BY `second_client_id`
            ) `credit` ON credit.client_id = client.id 
        
            LEFT JOIN (
                SELECT 
                    `second_client_id` AS `client_id`, 
                    ABS(SUM(bill.total_eur)) AS `total` 
                FROM 
                    `bill` 
                    LEFT JOIN `agreement` `a` ON a.id = bill.agreement_id 
                    LEFT JOIN `abonent_bill` `ab` ON ab.bill_id = bill.id 
                WHERE 
                    (`bill`.`status` IN ('signed' , 'prepar_payment', 'payment', 'paid', 'complete')) AND 
                    (`bill`.`pay_status` IN ('not' , 'part')) AND 
                    (`bill`.`doc_type` = 'cr_bill') AND 
                    (`bill`.`doc_date` BETWEEN '1900-01-01' AND '2019-07-31') AND 
                    (`bill`.`deleted` != 1) AND 
                    (`ab`.`abonent_id`= 1) 
                GROUP BY `second_client_id`
            ) `credit_cr` ON credit_cr.client_id = client.id 
        
            LEFT JOIN (
                SELECT 
                    `second_client_id` AS `client_id`, 
                    SUM(bp.summa_eur) AS `total` 
                FROM 
                    `bill` 
                    LEFT JOIN `agreement` `a` ON a.id = bill.agreement_id 
                    LEFT JOIN `abonent_bill` `ab` ON ab.bill_id = bill.id 
                    LEFT JOIN `bill_payment` `bp` ON bp.bill_id = bill.id AND (`bp`.`paid_date` <= '2019-07-25')                
                WHERE 
                    (`bill`.`status` IN ('signed' , 'prepar_payment', 'payment', 'paid', 'complete')) AND 
                    (`bill`.`pay_status` IN ('not' , 'part')) AND 
                    (`bill`.`doc_type` IN ('bill', 'invoice')) AND 
                    (`bill`.`doc_date` BETWEEN '1900-01-01' AND '2019-07-31') AND 
                    (`bill`.`deleted` != 1) AND 
                    (`ab`.`abonent_id`= 1) 
                GROUP BY `second_client_id`
            ) `credit_payments` ON credit_payments.client_id = client.id 

            LEFT JOIN (
                SELECT 
                    `first_client_id` AS `client_id`, 
                    SUM(ap.summa_eur) AS `summa` 
                FROM 
                    `agreement_payment` ap
                    LEFT JOIN `agreement` `a` ON a.id = ap.agreement_id 
                    LEFT JOIN `client` `c` ON c.id = a.first_client_id 
                    LEFT JOIN `abonent_client` `ac` ON ac.client_id = c.id 
                WHERE 
                    (ap.summa_eur != 0) AND 
                    (NOT ac.client_group_id IS NULL) AND 
                    (ap.paid_date BETWEEN '1900-01-01' AND '2019-07-31') AND 
                    (ap.direction = 'debet') AND 
                    (ac.abonent_id = 1) 
                GROUP BY `first_client_id`
            ) `debet_agr` ON debet_agr.client_id = client.id 

            LEFT JOIN (
                SELECT 
                    `second_client_id` AS `client_id`, 
                    SUM(ap.summa_eur) AS `summa` 
                FROM 
                    `agreement_payment` ap
                    LEFT JOIN `agreement` `a` ON a.id = ap.agreement_id 
                    LEFT JOIN `client` `c` ON c.id = a.second_client_id 
                    LEFT JOIN `abonent_client` `ac` ON ac.client_id = c.id 
                WHERE 
                    (ap.summa_eur != 0) AND 
                    (NOT ac.client_group_id IS NULL) AND 
                    (ap.paid_date BETWEEN '1900-01-01' AND '2019-07-31') AND 
                    (ap.direction = 'credit') AND 
                    (ac.abonent_id = 1) 
                GROUP BY `second_client_id`
            ) `credit_agr` ON credit_agr.client_id = client.id 
        
        WHERE 
            (debet.total IS NOT NULL OR credit.total IS NOT NULL OR debet_agr.summa IS NOT NULL OR credit_agr.summa IS NOT NULL) AND 
            (`client`.`deleted` != 1) AND 
            (NOT (`ac`.`client_group_id` IS NULL)) AND 
            (`ac`.`abonent_id` = 1) 
        ORDER BY `name`
         * 
         */
        
        //return $query->all();
        return $dataProvider;
    }
    
    public function searchPaidReport($params)
    {
        $start_date = !empty($_GET['from']) ? date('Y-m-d', strtotime($_GET['from'])) : date('Y-m-01');
        $end_date = !empty($_GET['till']) ? date("Y-m-d", strtotime($_GET['till'])) : date("Y-m-t");
        
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
    
        $query = Bill::find(true)->notDeleted();

        $query->select = [
            $baseTableName . '.*',
            'agreement_number' => 'agreement.number',
            'first_client_id' => 'If('.$baseTableName.'.doc_type != "cession", agreement.first_client_id, IF('.$baseTableName.'.cession_direction = "D", agreement.first_client_id, agreement.third_client_id))',
            'first_client_name' => 'If('.$baseTableName.'.doc_type != "cession", first_client.name, IF('.$baseTableName.'.cession_direction = "D", first_client.name, third_client.name))',
            'first_client_role_name' => 'If('.$baseTableName.'.doc_type != "cession", first_client_role.name, IF('.$baseTableName.'.cession_direction = "D", first_client_role.name, third_client_role.name))',
            'first_client_regno' => 'If('.$baseTableName.'.doc_type != "cession", first_client.reg_number, IF('.$baseTableName.'.cession_direction = "D", first_client.reg_number, third_client.reg_number))',
            'second_client_id' => 'If('.$baseTableName.'.doc_type != "cession", agreement.second_client_id, IF('.$baseTableName.'.cession_direction = "D", agreement.third_client_id, agreement.second_client_id))',
            'second_client_name' => 'If('.$baseTableName.'.doc_type != "cession", second_client.name, IF('.$baseTableName.'.cession_direction = "D", third_client.name, second_client.name))',
            'second_client_role_name' => 'If('.$baseTableName.'.doc_type != "cession", second_client_role.name, IF('.$baseTableName.'.cession_direction = "D", third_client_role.name, second_client_role.name))',
            'second_client_regno' => 'If('.$baseTableName.'.doc_type != "cession", second_client.reg_number, IF('.$baseTableName.'.cession_direction = "D", third_client.reg_number, second_client.reg_number))',
            'third_client_id' => 'agreement.third_client_id',
            'third_client_name' => 'third_client.name',
            'third_client_role_name' => 'third_client_role.name',
            'manager_name' => 'manager.name',
            'manager_user_id' => 'manager.user_id',
        ];
        
        $query->innerJoin(['ab' => 'abonent_bill'], 'ab.bill_id = '.$baseTableName.'.id and ab.abonent_id = '.$this->userAbonentId);
        $query->leftJoin(['agreement' => 'agreement'], 'agreement.id = '.$baseTableName.'.agreement_id');
        $query->leftJoin(['first_client' => 'client'], 'first_client.id = agreement.first_client_id');
        $query->leftJoin(['second_client' => 'client'], 'second_client.id = agreement.second_client_id');
        $query->leftJoin(['third_client' => 'client'], 'third_client.id = agreement.third_client_id');
        $query->leftJoin(['first_client_role' => 'client_role'], 'first_client_role.id = agreement.first_client_role_id');
        $query->leftJoin(['second_client_role' => 'client_role'], 'second_client_role.id = agreement.second_client_role_id');
        $query->leftJoin(['third_client_role' => 'client_role'], 'third_client_role.id = agreement.third_client_role_id');
        $query->leftJoin(['manager' => 'profile'], 'manager.id = '.$baseTableName.'.manager_id');
        $query->leftJoin(['acf' => 'abonent_client'], 'acf.client_id = first_client.id');
        $query->leftJoin(['acs' => 'abonent_client'], 'acs.client_id = second_client.id');
        $query->leftJoin(['acfg' => 'client_group'], 'acfg.id = acf.client_group_id');
        $query->leftJoin(['acsg' => 'client_group'], 'acsg.id = acs.client_group_id');
        
        if(!isset($params['sort']) && !isset($params['dp-1-sort'])){
            $query->addOrderBy('id desc');
        }
        
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 15,
            ],            
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
       
        $query
            ->andWhere(['>', $baseTableName.'.total', 0])
            ->andWhere(['not', ['acf.client_group_id' => null]])
            ->andWhere(['acfg.is_customer' => 0])
            ->andWhere(['OR', 
                ['acs.client_group_id' => null],
                ['AND', 
                    ['not', ['acs.client_group_id' => null]],
                    ['acsg.is_customer' => 1]
                ]
            ])
            ->andWhere(['in', $baseTableName.'.pay_status', [
                Bill::BILL_PAY_STATUS_PART,
                Bill::BILL_PAY_STATUS_FULL,
            ]])
            ->andWhere(['between', $baseTableName.'.paid_date', $start_date, $end_date.' 23:59:59']);
        
        //return $query->all();
        return $dataProvider;
    }
    
    public function searchNotPaidReport($params)
    {
        //$start_date = !empty($_GET['from']) ? date('Y-m-d', strtotime($_GET['from'])) : date('Y-m-01');
        $start_date = '1900-01-01';
        $end_date = !empty($_GET['till']) ? date("Y-m-d", strtotime($_GET['till'])) : date("Y-m-t");
        
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
    
        $query = Bill::find(true)->notDeleted();

        $query->select = [
            $baseTableName . '.*',
            'agreement_number' => 'agreement.number',
            'first_client_id' => 'If('.$baseTableName.'.doc_type != "cession", agreement.first_client_id, IF('.$baseTableName.'.cession_direction = "D", agreement.first_client_id, agreement.third_client_id))',
            'first_client_name' => 'If('.$baseTableName.'.doc_type != "cession", first_client.name, IF('.$baseTableName.'.cession_direction = "D", first_client.name, third_client.name))',
            'first_client_role_name' => 'If('.$baseTableName.'.doc_type != "cession", first_client_role.name, IF('.$baseTableName.'.cession_direction = "D", first_client_role.name, third_client_role.name))',
            'first_client_regno' => 'If('.$baseTableName.'.doc_type != "cession", first_client.reg_number, IF('.$baseTableName.'.cession_direction = "D", first_client.reg_number, third_client.reg_number))',
            'second_client_id' => 'If('.$baseTableName.'.doc_type != "cession", agreement.second_client_id, IF('.$baseTableName.'.cession_direction = "D", agreement.third_client_id, agreement.second_client_id))',
            'second_client_name' => 'If('.$baseTableName.'.doc_type != "cession", second_client.name, IF('.$baseTableName.'.cession_direction = "D", third_client.name, second_client.name))',
            'second_client_role_name' => 'If('.$baseTableName.'.doc_type != "cession", second_client_role.name, IF('.$baseTableName.'.cession_direction = "D", third_client_role.name, second_client_role.name))',
            'second_client_regno' => 'If('.$baseTableName.'.doc_type != "cession", second_client.reg_number, IF('.$baseTableName.'.cession_direction = "D", third_client.reg_number, second_client.reg_number))',
            'third_client_id' => 'agreement.third_client_id',
            'third_client_name' => 'third_client.name',
            'third_client_role_name' => 'third_client_role.name',
            'manager_name' => 'manager.name',
            'manager_user_id' => 'manager.user_id',
        ];
        
        $query->innerJoin(['ab' => 'abonent_bill'], 'ab.bill_id = '.$baseTableName.'.id and ab.abonent_id = '.$this->userAbonentId);
        $query->leftJoin(['agreement' => 'agreement'], 'agreement.id = '.$baseTableName.'.agreement_id');
        $query->leftJoin(['first_client' => 'client'], 'first_client.id = agreement.first_client_id');
        $query->leftJoin(['second_client' => 'client'], 'second_client.id = agreement.second_client_id');
        $query->leftJoin(['third_client' => 'client'], 'third_client.id = agreement.third_client_id');
        $query->leftJoin(['first_client_role' => 'client_role'], 'first_client_role.id = agreement.first_client_role_id');
        $query->leftJoin(['second_client_role' => 'client_role'], 'second_client_role.id = agreement.second_client_role_id');
        $query->leftJoin(['third_client_role' => 'client_role'], 'third_client_role.id = agreement.third_client_role_id');
        $query->leftJoin(['manager' => 'profile'], 'manager.id = '.$baseTableName.'.manager_id');
        $query->leftJoin(['acf' => 'abonent_client'], 'acf.client_id = first_client.id');
        $query->leftJoin(['acs' => 'abonent_client'], 'acs.client_id = second_client.id');
        $query->leftJoin(['acfg' => 'client_group'], 'acfg.id = acf.client_group_id');
        $query->leftJoin(['acsg' => 'client_group'], 'acsg.id = acs.client_group_id');
        
        if(!isset($params['sort']) && !isset($params['dp-1-sort'])){
            $query->addOrderBy('id desc');
        }
        
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 15,
            ],            
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
       
        $query
            ->andWhere(['>', $baseTableName.'.total', 0])
            ->andWhere(['not', ['acf.client_group_id' => null]])
            ->andWhere(['acfg.is_customer' => 0])
            ->andWhere(['OR', 
                ['acs.client_group_id' => null],
                ['AND',
                    ['not', ['acs.client_group_id' => null]],
                    ['acsg.is_customer' => 1]
                ]
            ])
            ->andWhere(['not', [$baseTableName.'.status' => [
                Bill::BILL_STATUS_COMPLETE,
                Bill::BILL_STATUS_CANCELED,
            ]]])
            ->andWhere(['in', $baseTableName.'.pay_status', [
                Bill::BILL_PAY_STATUS_NOT,
                Bill::BILL_PAY_STATUS_PART,
                Bill::BILL_PAY_STATUS_DELAYED,
            ]])
            ->andWhere(['between', $baseTableName.'.doc_date', $start_date, $end_date.' 23:59:59']);
        
        //return $query->all();
        return $dataProvider;
    }
    
}