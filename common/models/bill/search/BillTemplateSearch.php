<?php

namespace common\models\bill\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

use common\models\bill\BillTemplate;

/**
 * BillTemplateSearch represents the model behind the search form of `common\models\bill\BillTemplate`.
 */
class BillTemplateSearch extends BillTemplate
{
    public $project_id;
    public $project_name;
    public $agreement_number;
    public $first_client_id;
    public $first_client_name;
    public $first_client_role_name;
    public $second_client_id;
    public $second_client_name;
    public $second_client_role_name;
    public $third_client_id;
    public $third_client_name;
    public $third_client_role_name;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'agreement_id', 'first_client_bank_id', 'second_client_bank_id', 'according_contract', 
                'valuta_id', 'language_id', 'periodical_day', 'create_user_id', 'update_user_id'], 'integer'],
            [['summa', 'vat', 'total', 'rate', 'summa_eur', 'vat_eur', 'total_eur'], 'number'],
            [['justification', 'place_service', 'comment_special', 'comment', 
                'periodicity', 'create_time', 'update_time', 
                'project_id', 'project_name', 'agreement_number', 
                'first_client_id', 'first_client_name', 'first_client_role_name', 
                'second_client_id', 'second_client_name', 'second_client_role_name',
                'third_client_id', 'third_client_name', 'third_client_role_name',
                'periodical_next_date', 'periodical_finish_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
    
        $query = BillTemplate::find(true);

        $query->select = [
            $baseTableName . '.*',
            'project_id' => 'project.id',
            'project_name' => 'project.name',
            'agreement_number' => 'agreement.number',
            'first_client_id' => 'agreement.first_client_id',
            'first_client_name' => 'first_client.name',
            'first_client_role_name' => 'first_client_role.name',
            'second_client_id' => 'agreement.second_client_id',
            'second_client_name' => 'second_client.name',
            'second_client_role_name' => 'second_client_role.name',
            'third_client_id' => 'agreement.third_client_id',
            'third_client_name' => 'third_client.name',
            'third_client_role_name' => 'third_client_role.name',
        ];
        $query->innerJoin(['ab' => 'abonent_bill_template'], 'ab.bill_template_id = '.$baseTableName.'.id and ab.abonent_id = '.$this->userAbonentId);
        $query->leftJoin(['project' => 'project'], 'project.id = ab.project_id');
        $query->leftJoin(['agreement' => 'agreement'], 'agreement.id = '.$baseTableName.'.agreement_id');
        $query->leftJoin(['first_client' => 'client'], 'first_client.id = agreement.first_client_id');
        $query->leftJoin(['second_client' => 'client'], 'second_client.id = agreement.second_client_id');
        $query->leftJoin(['third_client' => 'client'], 'third_client.id = agreement.third_client_id');
        $query->leftJoin(['first_client_role' => 'client_role'], 'first_client_role.id = agreement.first_client_role_id');
        $query->leftJoin(['second_client_role' => 'client_role'], 'second_client_role.id = agreement.second_client_role_id');
        $query->leftJoin(['third_client_role' => 'client_role'], 'third_client_role.id = agreement.third_client_role_id');
        
        if(!isset($params['sort'])){
            $query->addOrderBy('id desc');
        }
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            $baseTableName.'.id' => $this->id,
            'ab.project_id' => $this->project_id,
            $baseTableName.'.agreement_id' => $this->agreement_id,
            $baseTableName.'.first_client_bank_id' => $this->first_client_bank_id,
            $baseTableName.'.second_client_bank_id' => $this->second_client_bank_id,
            $baseTableName.'.summa' => $this->summa,
            $baseTableName.'.vat' => $this->vat,
            $baseTableName.'.total' => $this->total,
            $baseTableName.'.valuta_id' => $this->valuta_id,
            $baseTableName.'.language_id' => $this->language_id,
            $baseTableName.'.rate' => $this->rate,
            $baseTableName.'.summa_eur' => $this->summa_eur,
            $baseTableName.'.vat_eur' => $this->vat_eur,
            $baseTableName.'.total_eur' => $this->total_eur,
            $baseTableName.'.periodical_day' => $this->periodical_day,
            $baseTableName.'.periodical_next_date' => !empty($this->periodical_next_date) ? date('Y-m-d', strtotime($this->periodical_next_date)) : null,
            $baseTableName.'.periodical_finish_date' => !empty($this->periodical_finish_date) ? date('Y-m-d', strtotime($this->periodical_finish_date)) : null,
            $baseTableName.'.create_time' => $this->create_time,
            $baseTableName.'.create_user_id' => $this->create_user_id,
            $baseTableName.'.update_time' => $this->update_time,
            $baseTableName.'.update_user_id' => $this->update_user_id,
            
            'agreement.first_client_id' => $this->first_client_id,
            'agreement.second_client_id' => $this->second_client_id,
            
        ]);

        $query->andFilterWhere(['like', $baseTableName.'.according_contract', $this->according_contract])
            ->andFilterWhere(['like', $baseTableName.'.justification', $this->justification])
            ->andFilterWhere(['like', $baseTableName.'.place_service', $this->place_service])
            ->andFilterWhere(['like', $baseTableName.'.comment_special', $this->comment_special])
            ->andFilterWhere(['like', $baseTableName.'.comment', $this->comment])
            ->andFilterWhere(['like', $baseTableName.'.periodicity', $this->periodicity])
                
            ->andFilterWhere(['like', 'project.name', $this->project_name])
            ->andFilterWhere(['like', 'agreement.number', $this->agreement_number]);

        return $dataProvider;
    }
    
    public function searchPeriodical($execDate = null)
    {
        $execDate = $execDate ?? date('Y-m-d');
        $baseTableName = $this->tableName();
        $query = $this->find()
            ->andWhere(['<=', $baseTableName.'.periodical_next_date', $execDate])
            ->andWhere(['or',
                ['>=', $baseTableName.'.periodical_finish_date', $execDate],
                [$baseTableName.'.periodical_finish_date' => null]
            ]);
        return $query->all();
    }
        
}