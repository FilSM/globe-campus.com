<?php

namespace common\models\bill;

use Yii;
use yii\helpers\ArrayHelper;

use common\components\FSMHelper;
use common\models\user\FSMUser;
use common\models\abonent\Abonent;
use common\models\client\Client;
use common\models\Valuta;

/**
 * This is the model class for table "tax_payment".
 *
 * @property integer $id
 * @property integer $abonent_id
 * @property string $paid_date
 * @property integer $from_client_id
 * @property integer $to_client_id
 * @property string $summa
 * @property integer $valuta_id
 * @property string $rate
 * @property string $summa_eur
 * @property string $confirmed
 * @property string $comment
 * @property integer $manual_input
 *
 * @property Client $fromClient
 * @property Client $toClient
 * @property Valuta $valuta
 */
class TaxPayment extends \common\models\mainclass\FSMBaseModel
{
    public static function find($withScope = false) {
        if ((Yii::$app->id != 'app-console') && $withScope) {
            return new \common\scopes\AbonentFieldScopeQuery(get_called_class());
        }
        return parent::find();
    }
    
    protected $_externalFields = [
        'from_client_name',
        'to_client_name',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax_payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['abonent_id', 'paid_date', 'from_client_id', 'to_client_id', 'summa', 'valuta_id'], 'required'],
            [['from_client_id', 'to_client_id', 'valuta_id', 'manual_input'], 'integer'],
            [['summa', 'rate', 'summa_eur'], 'number'],
            [['paid_date', 'confirmed'], 'safe'],
            [['comment'], 'string'],
            [['summa'], 'validateRate'],
            [['abonent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Abonent::class, 'targetAttribute' => ['abonent_id' => 'id']],
            [['from_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['from_client_id' => 'id']],
            [['to_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['to_client_id' => 'id']],
            [['valuta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Valuta::class, 'targetAttribute' => ['valuta_id' => 'id']],
        ];
    }

    public function validateRate($attribute, $params, $validator)
    {
        $rate = (float)$this->rate;
        $valuta_id = (int)$this->valuta_id;
        if (!empty($valuta_id) && (
                (($rate == 1) && ($valuta_id != Valuta::VALUTA_DEFAULT)) ||
                (($rate != 1) && ($valuta_id == Valuta::VALUTA_DEFAULT))
            )) {
            $message = Yii::t('bill', 'The currency rate is not correct.');
            $validator->addError($this, 'summa', $message);
        }        
    }
    
    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('bill', 'Tax payment|Tax payments', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'abonent_id' => Yii::t('client', 'Abonent'),
            'paid_date' => Yii::t('bill', 'Paid'),
            'from_client_id' => Yii::t('bill', 'Client from'),
            'to_client_id' => Yii::t('bill', 'Client to'),
            'summa' => Yii::t('common', 'Sum'),
            'valuta_id' => Yii::t('common', 'Currenсy'),
            'rate' => Yii::t('common', 'Rate'),
            'summa_eur' => Yii::t('common', 'Sum').' €',
            'confirmed' => Yii::t('bill', 'Confirmed'),
            'comment' => Yii::t('common', 'Comment'),
        ];
    }

    protected function getIgnoredFieldsForDelete() {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, ['from_client_id', 'to_client_id', 'valuta_id']
        );
        return $fields;
    }
    
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getAbonent()
    {
        return $this->hasOne(Abonent::class, ['id' => 'abonent_id']);
    }
   
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromClient()
    {
        return $this->hasOne(Client::class, ['id' => 'from_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToClient()
    {
        return $this->hasOne(Client::class, ['id' => 'to_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValuta()
    {
        return $this->hasOne(Valuta::class, ['id' => 'valuta_id']);
    }

    public function canUpdate() {
        if(FSMUser::getIsPortalAdmin()){
            return true;
        }        
        return !empty($this->manual_input);
        return true;
    }

    public function canDelete() {
        return $this->canUpdate();
    }    
    
    static function getButtonConfirm($params)
    {
        extract($params);
        if(!$model->canUpdate()){
            return null;
        }
        if($model->confirmed){
            return null;
        }
        
        $data = [
            'title' => Yii::t('bill', 'Confirm payment'),
            'controller' => 'tax-payment',
            'action' => 'ajax-confirm-payment',
            'icon' => 'ok',
            'modal' => true,
        ];        
        if (!empty($isBtn)) {
            $result = FSMHelper::vButton($model->id, 
                ArrayHelper::merge($data, [
                    'class' => 'success',
                    'size' => 'btn-xs',
                ])
            );
        } else {
            $result = FSMHelper::vDropdown($model->id, 
                ArrayHelper::merge($data, [
                    'label' => Yii::t('bill', 'Confirm payment'),
                ])
            );
        }
        return $result;        
    }     
    
    static function getButtonUpdate($params)
    {
        extract($params);
        if(!$model->canUpdate()){
            return null;
        }
        $data = [
            'title' => Yii::t('kvgrid', 'Update'),
            'controller' => 'tax-payment',
            'action' => 'ajax-update',
            'icon' => 'pencil',
            'modal' => true,
        ];        
        if (!empty($isBtn)) {
            $result = FSMHelper::vButton($model->id, 
                ArrayHelper::merge($data, [
                    'class' => 'primary',
                    'size' => 'btn-xs',
                ])
            );
        } else {
            $result = FSMHelper::vDropdown($model->id, 
                ArrayHelper::merge($data, [
                    'label' => Yii::t('kvgrid', 'Update'),
                ])
            );
        }
        return $result;        
    }    
    
    public function beforeSave($insert) 
    {
        $this->rate = ($this->valuta_id == Valuta::VALUTA_DEFAULT) ? 1 : $this->rate;
        $this->summa_eur = $this->summa / $this->rate;
        $this->paid_date = date('Y-m-d', strtotime($this->paid_date));
        $this->confirmed = isset($model->confirmed) ? date('Y-m-d', strtotime($this->confirmed)) : null;
        if(!parent::beforeSave($insert)){
            return;
        }
        return true;
    }     
}