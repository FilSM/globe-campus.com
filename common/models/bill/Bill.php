<?php

namespace common\models\bill;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use kartik\helpers\Html;
use kartik\popover\PopoverX;

use common\components\FSMToken;
use common\components\FSMAccessHelper;
use common\models\abonent\Abonent;
use common\models\abonent\AbonentBill;
use common\models\bill\BillAttachment;
use common\models\bill\BillProductProject;
use common\models\client\Client;
use common\models\client\Project;
use common\models\client\Agreement;
use common\models\client\ClientBank;
use common\models\client\ClientRole;
use common\models\user\FSMUser;
use common\models\user\FSMProfile;
use common\models\Valuta;
use common\models\Language;
use common\models\Files;
use common\printDocs\PrintModule;
use common\components\FSMHelper;

/**
 * This is the model class for table "bill".
 *
 * @property integer $id
 * @property integer $deleted
 * @property integer $agreement_id
 * @property integer $parent_id
 * @property integer $child_id
 * @property string $doc_type
 * @property string $doc_number
 * @property string $doc_date
 * @property string $pay_date
 * @property string $paid_date
 * @property string $complete_date
 * @property string $status
 * @property string $pay_status
 * @property string $transfer_status
 * @property string $mail_status
 * @property integer $delayed
 * @property integer $first_client_bank_id
 * @property integer $second_client_bank_id
 * @property integer $according_contract
 * @property string $summa
 * @property string $vat
 * @property string $total
 * @property integer $valuta_id
 * @property string $rate
 * @property string $summa_eur
 * @property string $vat_eur
 * @property string $total_eur
 * @property string $first_client_pvn_payer
 * @property string $second_client_pvn_payer
 * @property integer $manager_id
 * @property integer $language_id
 * @property string $services_period_from
 * @property string $services_period_till
 * @property string $services_period
 * @property string $doc_key
 * @property string $loading_address
 * @property string $unloading_address
 * @property string $carrier
 * @property string $transport
 * @property integer $e_signing
 * @property integer $has_act
 * @property string $justification
 * @property string $place_service
 * @property string $comment_special
 * @property string $comment
 * @property integer $returned
 * @property integer $blocked
 * @property integer $not_expenses
 * @property integer $bill_template_id
 * @property string $create_time
 * @property integer $create_user_id
 * @property string $update_time
 * @property integer $update_user_id
 *
 * @property Abonent[] $abonents
 * @property AbonentBill[] $abonentBills
 * @property Project $project
 * @property Agreement $agreement
 * @property Client $client
 * @property Client $secondClient
 * @property Bill $parent
 * @property Bill[] $parents
 * @property Bill[] $child
 * @property FSMProfile $manager
 * @property ClientBank $firstClientBank
 * @property ClientBank $secondClientBank
 * @property FirstBillPerson $firstClientPerson
 * @property SecondBillPerson $secondClientPerson
 * @property Valuta $valuta
 * @property BillTemplate $billTemplate
 * @property FSMUser $createUser
 * @property FSMUser $updateUser
 * @property BillPayment[] $billPayments
 * @property BillConfirm[] $billConfirms
 * @property BillHistory[] $billHistories
 * @property BillAccount[] $billAccounts
 * @property BillAttachment[] $attachments
 * @property AccountMap $firstAccount
 * @property AccountMap $secondAccount
 * @property BillProductProject[] $billProductProjects
 */
class Bill extends \common\models\mainclass\FSMCreateUpdateModel
{
    const BILL_DEFAULT_PAYMENT_DAYS = 3;
    
    const BILL_DOC_TYPE_BILL = 'bill';
    const BILL_DOC_TYPE_AVANS = 'avans';
    const BILL_DOC_TYPE_CRBILL = 'cr_bill';
    const BILL_DOC_TYPE_INVOICE = 'invoice';
    const BILL_DOC_TYPE_DEBT = 'debt';
    const BILL_DOC_TYPE_CESSION = 'cession';
    
    const BILL_STATUS_PREPAR = 'prepar';
    const BILL_STATUS_NEW = 'new';
    const BILL_STATUS_READY = 'ready';
    const BILL_STATUS_SIGNED = 'signed';
    const BILL_STATUS_PREP_PAYMENT = 'prepar_payment';
    const BILL_STATUS_PAYMENT = 'payment';
    const BILL_STATUS_PAID = 'paid';
    const BILL_STATUS_COMPLETE = 'complete';
    const BILL_STATUS_CANCELED = 'canceled';
    
    const BILL_PAY_STATUS_NOT = 'not';
    const BILL_PAY_STATUS_PART = 'part';
    const BILL_PAY_STATUS_FULL = 'full';
    const BILL_PAY_STATUS_OVER = 'over';
    const BILL_PAY_STATUS_DELAYED = 'delayed';
    const BILL_PAY_STATUS_SETOFF = 'setoff';
    const BILL_PAY_STATUS_PART_CLOSED = 'part_closed';

    const BILL_TRANSFER_STATUS_NONE = '';
    const BILL_TRANSFER_STATUS_SENT = 'sent';
    const BILL_TRANSFER_STATUS_RECEIVED = 'received';

    const BILL_MAIL_STATUS_NONE = '';
    const BILL_MAIL_STATUS_SEND = 'sent';
    const BILL_MAIL_STATUS_RECEIVED = 'received';
    
    const TOKEN_TYPE_NEW_BILL = 20;
    const TOKEN_TYPE_UPDATED_BILL = 21;

    protected $_externalFields = [
        'project_id',
        'project_name',
        'agreement_number',
        'first_client_id',
        'first_client_name',
        'first_client_regno',
        'first_client_role_name',
        'second_client_id',
        'second_client_name',
        'second_client_regno',
        'second_client_role_name',
        'third_client_id',
        'third_client_name',
        'third_client_role_name',
        'manager_name',
        'manager_user_id',
        'project_sales',
        'project_purchases',
        'project_profit',
        'client_id',
        'client_name',
        'client_sales',
        'client_vat_plus',
        'client_purchases',
        'client_vat_minus',
        'client_vat_result',
        'client_profit',
        'debtors_summa',
        'creditors_summa',
        'expense_ids',
        'reserved_id',
        'notPaidSumma',
        'doc_number_concat',
    ];

    public function init()
    {
        parent::init();
        $this->cascadeDeleting = true;
    }

    public static function find($withScope = false)
    {
        if ((Yii::$app->id != 'app-console') && $withScope) {
            $scope = new \common\scopes\BillScopeQuery(get_called_class());
            return $scope->onlyAuthor()->forClient();
        }
        return parent::find();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bill';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agreement_id', 'doc_number', 'doc_date',
                'pay_date', 'first_client_bank_id',
                'summa', 'vat', 'total', 'valuta_id'], 'required'],
            [['id', 'deleted', 'agreement_id', 'bill_template_id',
                'parent_id', 'child_id', 'delayed', 'first_client_bank_id', 'second_client_bank_id',
                'according_contract', 'valuta_id', 'manager_id', 'language_id', 'e_signing',
                'returned', 'blocked', 'not_expenses', 'has_act', 'create_user_id', 'update_user_id'], 'integer'],
            [['doc_type', 'status', 'pay_status', 'transfer_status', 'mail_status', 'doc_key',
                'loading_address', 'unloading_address', 'carrier', 'transport',
                'services_period', 'comment_special', 'comment', 'cession_direction'], 'string'],
            [['doc_date', 'pay_date', 'paid_date', 'complete_date', 'services_period_from', 'services_period_till', 
                'create_time', 'update_time'], 'safe'],
            [['summa', 'vat', 'total', 'rate', 'summa_eur', 'vat_eur', 'total_eur', 'first_client_pvn_payer', 'second_client_pvn_payer'], 'number'],
            [['doc_number'], 'string', 'max' => 40],
            [['transport'], 'string', 'max' => 20],
            [['carrier', 'services_period'], 'string', 'max' => 50],
            [['loading_address', 'unloading_address', 'justification'], 'string', 'max' => 100],
            [['place_service'], 'string', 'max' => 255],
            [['total'], 'validateRate'],
            //[['doc_number', 'deleted'], 'validateUniqueNumber'],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bill::class, 'targetAttribute' => ['parent_id' => 'id']],
            [['agreement_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agreement::class, 'targetAttribute' => ['agreement_id' => 'id']],
            [['first_client_bank_id'], 'exist', 'skipOnError' => true, 'targetClass' => ClientBank::class, 'targetAttribute' => ['first_client_bank_id' => 'id']],
            [['second_client_bank_id'], 'exist', 'skipOnError' => true, 'targetClass' => ClientBank::class, 'targetAttribute' => ['second_client_bank_id' => 'id']],
            [['manager_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMProfile::class, 'targetAttribute' => ['manager_id' => 'id']],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::class, 'targetAttribute' => ['language_id' => 'id']],
            [['valuta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Valuta::class, 'targetAttribute' => ['valuta_id' => 'id']],
            [['bill_template_id'], 'exist', 'skipOnError' => true, 'targetClass' => BillTemplate::class, 'targetAttribute' => ['bill_template_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['create_user_id' => 'id']],
            [['update_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['update_user_id' => 'id']],
        ];
    }

    public function validateUniqueNumber($attribute, $params, $validator)
    {
        if($attribute == 'deleted'){
            return;
        }
        $baseTableName = $this->tableName();
        $query = self::find(true);
        $query->andWhere([$baseTableName.'.doc_number' => $this->$attribute]);
        //$query->andWhere(['=', 'YEAR('.$baseTableName.'.doc_date)', date('Y')]);
        $query->andWhere([$baseTableName.'.deleted' => 0]);
        if(!empty($this->id)){
            $query->andWhere(['not', [$baseTableName.'.id' => $this->id]]);
        }
        $exists = $query->exists();
        if ($exists) {
            $message = Yii::t('yii', '{attribute} "{value}" has already been taken.');
            $params = ['attribute' => $attribute, 'value' => $this->$attribute];
            $validator->addError($this, $attribute, $message, $params);
        }
    }

    public function validateRate($attribute, $params, $validator)
    {
        $rate = (float)$this->rate;
        $valuta_id = (int)$this->valuta_id;
        if (
                (($rate == 1) && ($valuta_id != Valuta::VALUTA_DEFAULT)) ||
                (($rate != 1) && ($valuta_id == Valuta::VALUTA_DEFAULT))
            ) {
            $message = Yii::t('bill', 'The currency rate is not correct.');
            $validator->addError($this, 'total', $message);
        }
    }
    
    public function canUpdate()
    {
        if(!parent::canUpdate()){
            return;
        }
        if(!empty($this->blocked)){
            return;
        }
        if(FSMUser::getIsPortalAdmin()){
            return true;
        }
        if(in_array($this->status, [BILL::BILL_STATUS_COMPLETE,]) &&
            !empty(Yii::$app->params['COMPLETE_BLOCKED'])
        ){
            return;
        }
        $abonentModel = $this->currentAbonent;
        if(isset($abonentModel) && ($abonentModel->primary_abonent === 0)){
            return;
        }
        return true; 
    }
    
    public function canDelete()
    {
        if(!parent::canDelete()){
            return;
        }
        if(!empty($this->blocked)){
            return;
        }
        if(in_array($this->status, [
            BILL::BILL_STATUS_COMPLETE,
        ])){
            return;
        }        
        $abonentModel = $this->currentAbonent;
        if(isset($abonentModel) && ($abonentModel->primary_abonent === 0)){
            return;
        }
        return true; 
    }
    
    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true)
    {
        return parent::label('bill', 'Invoice|Invoices', $n, $translate);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'deleted' => Yii::t('common', 'Deleted'),
            'agreement_id' => Yii::t('bill', 'Agreement'),
            'parent_id' => Yii::t('bill', 'Parent invoice'),
            'child_id' => Yii::t('bill', 'Child invoice'),
            'doc_type' => Yii::t('bill', 'Doc.type'),
            'doc_number' => Yii::t('bill', 'Doc.number'),
            'doc_date' => Yii::t('bill', 'Doc.date'),
            'pay_date' => Yii::t('bill', 'Due date'),
            'paid_date' => Yii::t('bill', 'Paid'),
            'complete_date' => Yii::t('bill', 'Final payment date'),
            'first_client_bank_id' => Yii::t('bill', 'First party bank account'),
            'second_client_bank_id' => Yii::t('bill', 'Second party bank account'),
            'status' => Yii::t('common', 'Status'),
            'pay_status' => Yii::t('bill', 'Payment status'),
            'transfer_status' => Yii::t('bill', 'Transfer status'),
            'mail_status' => Yii::t('bill', 'Mail status'),
            'delayed' => Yii::t('bill', 'Delayed'),
            'according_contract' => Yii::t('bill', 'According contract'),
            'summa' => Yii::t('common', 'Sum'),
            'vat' => Yii::t('common', 'VAT'),
            'total' => Yii::t('common', 'Total'),
            'valuta_id' => Yii::t('common', 'Currenсy'),
            'rate' => Yii::t('common', 'Rate'),
            'summa_eur' => Yii::t('common', 'Sum').' €',
            'vat_eur' => Yii::t('common', 'VAT').' €',
            'total_eur' => Yii::t('common', 'Total').' €',
            'manager_id' => Yii::t('common', 'Curator'),
            'language_id' => Yii::t('common', 'Print language'),
            'services_period_from' => Yii::t('bill', 'Service period (from)'),
            'services_period_till' => Yii::t('bill', 'Service period (till)'),
            'services_period' => Yii::t('bill', 'Service period'),
            'loading_address' => Yii::t('bill', 'Loading address'),
            'unloading_address' => Yii::t('bill', 'Unloading address'),
            'carrier' => Yii::t('bill', 'Carrier'),
            'transport' => Yii::t('bill', 'Transport'),
            'e_signing' => Yii::t('bill', 'Without signing'),
            'has_act' => Yii::t('bill', 'Act'),
            'justification' => Yii::t('bill', 'Justification'),
            'place_service' => Yii::t('bill', 'Place of service'),
            'comment_special' => Yii::t('bill', 'Special notes'),
            'comment' => Yii::t('common', 'Comment'),
            'returned' => Yii::t('bill', 'Returned'),
            'blocked' => Yii::t('bill', 'Blocked'),
            'not_expenses' => Yii::t('bill', 'The invoice does not need to be charged to expenses'),
            'create_time' => Yii::t('common', 'Create Time'),
            'create_user_id' => Yii::t('common', 'Create User'),
            'update_time' => Yii::t('common', 'Update Time'),
            'update_user_id' => Yii::t('common', 'Update User'),
            
            'project_id' => Yii::t('bill', 'Project'),
            'project_name' => Yii::t('bill', 'Project'),
            'agreement_number' => Yii::t('bill', 'Agreement'),
            'first_client_id' => Yii::t('bill', 'First party'),
            'first_client_name' => Yii::t('bill', 'First party'),
            'first_client_regno' => Yii::t('client', 'First party reg.number'),
            'second_client_id' => Yii::t('bill', 'Second party'),
            'second_client_name' => Yii::t('bill', 'Second party'),
            'second_client_regno' => Yii::t('client', 'Second party reg.number'),
            'manager_name' => Yii::t('client', 'Curator'),
            'project_sales' => Yii::t('report', 'Sales'),
            'project_purchases' => Yii::t('report', 'Purchases'),
            'project_profit' => Yii::t('report', 'Profit or Loss'),
            'client_id' => Yii::t('client', 'Client'),
            'client_name' => Yii::t('client', 'Client'),
            'client_sales' => Yii::t('report', 'Sales'),
            'client_vat_plus' => Yii::t('report', 'VAT+'),
            'client_purchases' => Yii::t('report', 'Purchases'),
            'client_vat_minus' => Yii::t('report', 'VAT-'),
            'client_vat_result' => Yii::t('report', 'VAT result'),
            'client_profit' => Yii::t('report', 'Profit or Loss'),
            'debtors_summa' => Yii::t('report', 'Debtors'),
            'creditors_summa' => Yii::t('report', 'Creditors'),
        ];
    }
    
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors = ArrayHelper::merge(
            $behaviors,
            [
                \common\behaviors\DeletedModelBehavior::class,
            ]
        );
        return $behaviors;        
    }

    protected function getIgnoredFieldsForDelete()
    {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, ['bill_id', 'agreement_id', 'manager_id', 'language_id',
            'first_client_bank_id', 'second_client_bank_id',
            'parent_id', 'valuta_id', 'bill_template_id']
        );
        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonents()
    {
        return $this->hasMany(Abonent::class, ['id' => 'abonent_id'])->viaTable('abonent_bill', ['bill_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrentAbonent()
    { 
        $abonentModel = AbonentBill::findOne(['abonent_id' => $this->userAbonentId, 'bill_id' => $this->id]);
        return $abonentModel;
    }    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonentBills(){
        return $this->hasMany(AbonentBill::class, ['bill_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreement()
    {
        return $this->hasOne(Agreement::class, ['id' => 'agreement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(FSMProfile::class, ['id' => 'manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::class, ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstClientBank()
    {
        return $this->hasOne(ClientBank::class, ['id' => 'first_client_bank_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondClientBank()
    {
        return $this->hasOne(ClientBank::class, ['id' => 'second_client_bank_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstClient()
    {
        return $this->hasOne(Client::class, ['id' => 'first_client_id'])->viaTable('agreement', ['id' => 'agreement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondClient()
    {
        return $this->hasOne(Client::class, ['id' => 'second_client_id'])->viaTable('agreement', ['id' => 'agreement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstClientRole()
    {
        return $this->hasOne(ClientRole::class, ['id' => 'first_client_role_id'])->viaTable('agreement', ['id' => 'agreement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondClientRole()
    {
        return $this->hasOne(ClientRole::class, ['id' => 'second_client_role_id'])->viaTable('agreement', ['id' => 'agreement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Bill::class, ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParents()
    {
        $result = $this->hasMany(Bill::class, ['child_id' => 'id'])->notDeleted();
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChild()
    {
        return $this->hasOne(Bill::class, ['id' => 'child_id'])->notDeleted();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChilds()
    {
        $result = $this->hasMany(Bill::class, ['parent_id' => 'id'])->notDeleted();
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillProducts()
    {
        $result = $this->hasMany(BillProduct::class, ['bill_id' => 'id']);
        $result->andWhere(['bill_product.deleted' => 0]);
        return $result;
    }    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillProjects(){
        return $this->getBillProductProjects();
    }    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillProductProjects(){
        return $this->hasMany(BillProductProject::class, ['bill_id' => 'id']);
    }    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillProductProjectsByAbonent($abonentId = null)
    {
        $abonentId = $abonentId ?? $this->userAbonentId;
        return $this->hasMany(BillProductProject::class, ['bill_id' => 'id'])->andWhere(['abonent_id' => $abonentId]);
    }    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillProductProjectsByProject($projectId)
    {
        static $abonentId;
        
        if(!isset($abonentId)){
            $abonentId = $abonentId ?? $this->userAbonentId;
        }
        
        return $this->hasMany(BillProductProject::class, ['bill_id' => 'id'])
            ->andWhere(['abonent_id' => $abonentId])
            ->andWhere(['project_id' => $projectId]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillPeople()
    {
        $result = $this->hasMany(FirstBillPerson::class, ['bill_id' => 'id']);
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillPersons()
    {
        $result = $this->hasMany(FirstBillPerson::class, ['bill_id' => 'id']);
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstClientPerson()
    {
        $result = $this->hasMany(FirstBillPerson::class, ['bill_id' => 'id']);
        $result->andWhere(['person_order' => 1]);
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondClientPerson()
    {
        $result = $this->hasMany(SecondBillPerson::class, ['bill_id' => 'id']);
        $result->andWhere(['person_order' => 2]);
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillConfirms()
    {
        return $this->hasMany(BillConfirm::class, ['bill_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        $abonentId = $this->userAbonentId;
        return $this->hasOne(Project::class, ['id' => 'project_id'])
            ->viaTable('abonent_bill', ['bill_id' => 'id'], function ($query) use ($abonentId) {
                $query->andWhere(['abonent_id' => $abonentId]);
        });        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'update_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValuta()
    {
        return $this->hasOne(Valuta::class, ['id' => 'valuta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillAttachments() {
        return $this->hasMany(BillAttachment::class, ['bill_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments() {
        return $this->getBillAttachments();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachmentFiles() {
        return $this->hasMany(Files::class, ['id' => 'uploaded_file_id'])->via('attachments');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillTemplate()
    {
        return $this->hasOne(BillTemplate::class, ['id' => 'bill_template_id']);
    }
    
    public function beforeSave($insert) 
    {
        $this->doc_number = preg_replace('/\s+/', '', $this->doc_number);
        $this->doc_date = date('Y-m-d', strtotime($this->doc_date));
        $this->pay_date = !empty($this->pay_date) ? date('Y-m-d', strtotime($this->pay_date)) : null;
        if(!empty($this->services_period_from) && (date('Y-m-d', strtotime($this->services_period_from)) == '1970-01-01')){
            $this->services_period_from = null;
        }
        if(!empty($this->services_period_till) && (date('Y-m-d', strtotime($this->services_period_till)) == '1970-01-01')){
            $this->services_period_till = null;
        }
        if(!empty($this->services_period_from) && !empty($this->services_period_till)){
            $this->services_period_from = date('Y-m-d', strtotime($this->services_period_from));
            $this->services_period_till = date('Y-m-d', strtotime($this->services_period_till));
            $this->services_period = $this->services_period_from.' - '.$this->services_period_till;
        }else{
            $this->services_period = '';
        }
        
        $this->delayed = (int)(($this->pay_date < date('Y-m-d')) && $this->paymentsSummaIsLess);

        $this->rate = ($this->valuta_id == Valuta::VALUTA_DEFAULT) ? 1 : $this->rate;
        $this->summa_eur = $this->summa / $this->rate;
        $this->vat_eur = $this->vat / $this->rate;
        $this->total_eur = $this->total / $this->rate;
        
        if($this->doc_type == Bill::BILL_DOC_TYPE_CRBILL){
            $this->summa = $this->summa * -1;
            $this->vat = $this->vat * -1;
            $this->total = $this->total * -1;
            $this->summa_eur = $this->summa_eur * -1;
            $this->vat_eur = $this->vat_eur * -1;
            $this->total_eur = $this->total_eur * -1;
        }
        
        $client = $this->firstClient;

        $vatPayer = $client->vat_payer;
        if($vatPayer){
            $params = ['code' => $client->vat_number];
            $data = $client->checkViesData($params);
            $vatPayer = empty($data) ? $vatPayer : !empty($data['valid']);
        }
        $this->first_client_pvn_payer = $vatPayer;
        
        $client = $this->secondClient;
        $vatPayer = $client->vat_payer;
        if($vatPayer){
            $params = ['code' => $client->vat_number];
            $data = $client->checkViesData($params);
            $vatPayer = empty($data) ? $vatPayer : !empty($data['valid']);
        }
        $this->second_client_pvn_payer = $vatPayer;
        
        if(!parent::beforeSave($insert)){
            return;
        }
        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        $abonentModel = $this->currentAbonent;
        if(!$insert && isset($abonentModel) && ($abonentModel->primary_abonent === 1)){
            $ignoredFieldArr = [
                'mail_status',
                'transfer_status',
                'update_time',
                'update_user_id',
            ];
            $needSendMail = false;
            $changedAttributes = $this->getDirtyAttributes();
            foreach ($changedAttributes as $key => $attribute) {
                if(!in_array($key, $ignoredFieldArr) && ($this->oldAttributes[$key] != $this->$key)){
                    $needSendMail = true;
                    break;
                }
            }
            if($needSendMail && ($this->mail_status == Bill::BILL_MAIL_STATUS_RECEIVED)){
                $this->sendMailClient(true);
                $this->sendMailAgent(true);
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillPayments()
    {
        return $this->hasMany(BillPayment::class, ['bill_id' => 'id'])->orderBy('id desc');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfirmedPayments()
    {
        $result = $this->hasMany(BillPayment::class, ['bill_id' => 'id']);
        $result->andWhere(['not', ['confirmed' => null]]);
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfirmedPaymentsToDate(array $dateArr, $force = false)
    {
        $result = BillPayment::find(true)
            ->andWhere(['bill_id' => $this->id])
            ->andWhere(['not', ['confirmed' => null]])
            ->andWhere(['between', 'paid_date', $dateArr['from'], $dateArr['till'].' 23:59:59'])
            ->all();
        return $result;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotConfirmedPayments()
    {
        $result = $this->hasMany(BillPayment::class, ['bill_id' => 'id']);
        $result->andWhere(['confirmed' => null]);
        return $result;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillLastPayment()
    {
        $paymentList = $this->notConfirmedPayments;
        return !empty($paymentList) ? $paymentList[0] : null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillHistories()
    {
        return $this->hasMany(BillHistory::class, ['bill_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillAccounts()
    {
        return $this->hasMany(BillAccount::class, ['bill_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentPaymentsSumma()
    {
        $summa = 0;
        $parentList = $this->parents;
        foreach ($parentList as $parent) {
            $summa += $parent->paymentsSumma;
        }
        return $summa;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentPaymentsSummaToDate(array $dateArr, $force = false)
    {
        $summa = 0;
        $parentList = $this->parents;
        foreach ($parentList as $parent) {
            $summa += $parent->getPaymentsSummaToDate($dateArr, $force);
        }
        return $summa;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildsPaidSumma($force = false)
    {
        static $id;

        if (!array_key_exists($this->id, (array) $id) || ($id[$this->id] == 0) || $force) {
            $summa = 0;
            $childList = $this->childs;
            foreach ($childList as $child) {
                if(($child->status == BILL::BILL_STATUS_COMPLETE) && 
                    (
                        ($this->status != BILL::BILL_STATUS_COMPLETE) ||
                        ($this->complete_date <= $child->complete_date)
                    )
                ){
                    $summa += abs($child->total);
                }
            }        
            $id[$this->id] = $summa;
        }
        return $id[$this->id];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentsSumma($force = false)
    {
        static $id;

        if (!array_key_exists($this->id, (array) $id) || ($id[$this->id] == 0) || $force) {
            $summa = $this->parentPaymentsSumma;
            if ($this->compareFloatNumbers($summa, $this->total, '>')) {
                $summa = $this->total;
            }
            $paymentList = $this->confirmedPayments;
            foreach ($paymentList as $payment) {
                $summa += $payment->summa_origin;
            }
            $id[$this->id] = $summa;
        }
        return $id[$this->id];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentsSummaToDate(array $dateArr, $force = false)
    {
        static $id;

        if (!array_key_exists($this->id, (array) $id) || ($id[$this->id] == 0) || $force) {
            $summa = $this->getParentPaymentsSummaToDate($dateArr, $force);
            if ($this->compareFloatNumbers($summa, $this->total, '>')) {
                $summa = $this->total;
            }
            $paymentList = $this->getConfirmedPaymentsToDate($dateArr, $force);
            foreach ($paymentList as $payment) {
                $summa += $payment->summa_origin;
            }
            $id[$this->id] = $summa;
        }
        return $id[$this->id];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentsSummaGrate()
    {
        return $this->billPaidSumma - $this->total;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentsSummaLess()
    {
        return $this->total - $this->billPaidSumma;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentsSummaIsGrate()
    {
        return $this->compareFloatNumbers($this->total, $this->billPaidSumma, '<');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentsSummaIsLess()
    {
        return $this->compareFloatNumbers($this->total, $this->billPaidSumma, '>');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentsSummaIsEqual()
    {
        return $this->compareFloatNumbers($this->total, $this->billPaidSumma, '=');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillPaidSumma($force = false)
    {
        $result = $this->getPaymentsSumma($force);
        $result += $this->getChildsPaidSumma($force);
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillPaidSummaToDate(array $dateArr, $force = false)
    {
        $result = $this->getPaymentsSummaToDate($dateArr, $force);
        $result += $this->getChildsPaidSumma($force);
        return $result;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillNotPaidSumma()
    {
        $result = $this->total - $this->paymentsSumma;
        $result -= $this->childsPaidSumma;
        $result = $this->compareFloatNumbers(abs($result), 0.01, '>=') ? $result : 0;
        return  $result;
    }
    
    static public function getBillDocTypeList()
    {
        /*
          return [
          Bill::BILL_DOC_TYPE_AVANS => Yii::t('bill', 'Prepayment'),
          Bill::BILL_DOC_TYPE_BILL => Yii::t('bill', 'Invoice'),
          Bill::BILL_DOC_TYPE_CRBILL => Yii::t('bill', 'Credit invoice'),
          Bill::BILL_DOC_TYPE_INVOICE => Yii::t('bill', 'Waybill'),
          Bill::BILL_DOC_TYPE_DEBT => Yii::t('bill', 'Debt relief'),
          Bill::BILL_DOC_TYPE_CESSION => Yii::t('bill', 'Cession'),
          ];
         * 
         */
        return \lajax\translatemanager\helpers\Language::a([
            Bill::BILL_DOC_TYPE_AVANS => 'Prepayment invoice',
            Bill::BILL_DOC_TYPE_BILL => 'Invoice',
            Bill::BILL_DOC_TYPE_CRBILL => 'Credit invoice',
            Bill::BILL_DOC_TYPE_INVOICE => 'Waybill',
            Bill::BILL_DOC_TYPE_DEBT => 'Debt relief',
            Bill::BILL_DOC_TYPE_CESSION => 'Cession',
        ]);
    }
    
    static public function getBillDocTypeListByIds($ids = [])
    {
        $docTypeList = self::getBillDocTypeList();
        if(empty($ids)){
            return $docTypeList;
        }else{
            $result = [];
            foreach ($ids as $id) {
                $result[$id] = $docTypeList[$id];
            }
        }
        return $result;
    }
    
    static public function getBillStatusList()
    {
        return [
            Bill::BILL_STATUS_PREPAR => Yii::t('bill', 'Preparation'),
            Bill::BILL_STATUS_NEW => Yii::t('bill', 'New'),
            Bill::BILL_STATUS_READY => Yii::t('bill', 'Ready'),
            Bill::BILL_STATUS_SIGNED => Yii::t('bill', 'Signed'),
            Bill::BILL_STATUS_PREP_PAYMENT => Yii::t('bill', 'Payment prep.'),
            Bill::BILL_STATUS_PAYMENT => Yii::t('bill', 'i-Bank'),
            Bill::BILL_STATUS_PAID => Yii::t('bill', 'Paid'),
            Bill::BILL_STATUS_COMPLETE => Yii::t('bill', 'Complete'),
            Bill::BILL_STATUS_CANCELED => Yii::t('bill', 'Canceled'),
        ];
    }

    static public function getBillPayStatusList()
    {
        return [
            Bill::BILL_PAY_STATUS_NOT => Yii::t('bill', 'Not paid'),
            Bill::BILL_PAY_STATUS_PART => Yii::t('bill', 'Partially paid'),
            Bill::BILL_PAY_STATUS_FULL => Yii::t('bill', 'Full paid'),
            Bill::BILL_PAY_STATUS_OVER => Yii::t('bill', 'Overpaid'),
            Bill::BILL_PAY_STATUS_SETOFF => Yii::t('bill', 'Set-off'),
            Bill::BILL_PAY_STATUS_PART_CLOSED => Yii::t('bill', 'Partially closed'),
            Bill::BILL_PAY_STATUS_DELAYED => Yii::t('bill', 'Delayed'),
        ];
    }

    static public function getBillTransferStatusList()
    {
        return [
            Bill::BILL_TRANSFER_STATUS_NONE => Yii::t('common', 'None'),
            Bill::BILL_TRANSFER_STATUS_SENT => Yii::t('bill', 'Sent'),
            Bill::BILL_TRANSFER_STATUS_RECEIVED => Yii::t('bill', 'Received'),
        ];
    }

    static public function getBillMailStatusList()
    {
        return [
            Bill::BILL_MAIL_STATUS_NONE => Yii::t('common', 'None'),
            Bill::BILL_MAIL_STATUS_SEND => Yii::t('bill', 'Sent'),
            Bill::BILL_MAIL_STATUS_RECEIVED => Yii::t('bill', 'Received'),
        ];
    }
    
    public function getStatusBackgroundColor()
    {
        return $this->getStaticStatusBackgroundColor($this->status);
    }
    
    public function getTransferBackgroundColor()
    {
        return $this->getStaticTransferBackgroundColor($this->transfer_status);
    }
    
    public function getStatusHTML()
    {
        $lastAction = BillHistory::getLastDownAction($this->id);
        $content = !empty($lastAction) ? $lastAction->comment : '';
        $returned = !empty($this->returned) ?
            (!empty($content) ?
                '&nbsp;' .
                PopoverX::widget([
                    'header' => Yii::t('bill', 'Reason for return'),
                    'type' => PopoverX::TYPE_INFO,
                    'placement' => PopoverX::ALIGN_RIGHT_TOP,
                    'size' => PopoverX::SIZE_MEDIUM,
                    'content' => $content,
                    'toggleButton' => [
                        'tag' => 'span',
                        'label' => '', 
                        'class' => 'glyphicon glyphicon-arrow-down',
                        'title' => Yii::t('bill', 'Returned. Click to see the reason.'),
                        'style' => 'font-size: 20px; vertical-align: middle; color: #777777;'
                    ],
                ]) : ''
            ) : '';
        return (!empty($this->status) ?
            Html::a(
                Html::badge($this->billStatusList[$this->status], ['class' => $this->statusBackgroundColor . ' status-badge']), Url::to(['/bill/index']) . '?BillSearch[status]=' . $this->status, [/* 'target' => '_blank', */'data-pjax' => 0]
            ) : null) . $returned;
    }

    public function getPayStatusHTML()
    {
        $class = $this->delayed ? 'badge-danger' : $this->payStatusBackgroundColor;
        $result = '&nbsp;';
        if (isset($this->pay_status) && isset($this->status)) {
            
            if(in_array($this->status, [
                Bill::BILL_STATUS_SIGNED,
                Bill::BILL_STATUS_PREP_PAYMENT,
                Bill::BILL_STATUS_PAYMENT,
                Bill::BILL_STATUS_PAID,
                Bill::BILL_STATUS_COMPLETE])
            ){
                $result = '';
                if (($this->status != Bill::BILL_STATUS_COMPLETE) ||
                        (($this->status == Bill::BILL_STATUS_COMPLETE) && in_array($this->pay_status, [Bill::BILL_PAY_STATUS_PART_CLOSED]))) {
                    $result = Html::a(
                        Html::badge($this->billPayStatusList[$this->pay_status], ['class' => $class . ' pay-status-badge']), 
                        Url::to(['/bill/index', 'BillSearch[pay_status]' => $this->pay_status]), 
                        [/* 'target' => '_blank', */'data-pjax' => 0]
                    );
                }
                if ($this->pay_status == Bill::BILL_PAY_STATUS_FULL){
                    return $result;
                }elseif (($this->pay_status == Bill::BILL_PAY_STATUS_PART) ||
                    (
                        !empty($this->parents) && 
                        ($this->status != Bill::BILL_STATUS_COMPLETE)
                    )
                ){
                    if(!empty($result)){
                        $result .= '<br/>';
                    }
                    $result .= 
                        '<span style="padding-top: 5px;">' . 
                            Yii::t('bill', 'Paid') . ': ' . number_format($this->billPaidSumma, 2, '.', ' ') . 
                        '</span>'.
                        '<br/>'.
                        '<span style="padding-top: 5px; font-weight: bold;">' . 
                            Yii::t('bill', 'Debt') . ': ' . number_format($this->billNotPaidSumma, 2, '.', ' ') . 
                        '</span>';
                } elseif ($this->pay_status == Bill::BILL_PAY_STATUS_PART_CLOSED) {
                    if(!empty($result)){
                        $result .= '<br/>';
                    }
                    $result .= 
                        '<span style="padding-top: 5px;">' . 
                            Yii::t('bill', 'Paid') . ': ' . number_format($this->billPaidSumma, 2, '.', ' ') . 
                        '</span>' .
                        '<br/>'.
                        '<span style="padding-top: 5px; font-weight: bold;">' . 
                            Yii::t('bill', 'Not closed') . ': ' . number_format($this->billNotPaidSumma, 2, '.', ' ') . 
                        '</span>';
                } elseif ($this->pay_status == Bill::BILL_PAY_STATUS_OVER) {
                    if(!empty($result)){
                        $result .= '<br/>';
                    }
                    $result .= 
                        '<span style="padding-top: 5px;">' . 
                            Yii::t('bill', 'Overpayment') . ': ' . number_format($this->paymentsSummaGrate, 2, '.', ' ') . 
                        '</span>';
                    /*
                      }elseif(($this->doc_type == Bill::BILL_DOC_TYPE_AVANS) || (!empty($this->child) && ($this->status != Bill::BILL_STATUS_COMPLETE))){
                     * 
                     */
                }
            }elseif(in_array($this->status, [
                Bill::BILL_STATUS_PREPAR,
                Bill::BILL_STATUS_NEW,
                Bill::BILL_STATUS_READY,]) && !empty($this->parents)
            ){
                $result = Html::a(
                    Html::badge($this->billPayStatusList[$this->pay_status], ['class' => $class . ' pay-status-badge']), 
                    Url::to(['/bill/index', 'BillSearch[pay_status]' => $this->pay_status]), 
                    [/* 'target' => '_blank', */'data-pjax' => 0]
                );                
                if ($this->pay_status == Bill::BILL_PAY_STATUS_PART) {
                    if(!empty($result)){
                        $result .= '<br/>';
                    }
                    $result .= 
                        '<span style="padding-top: 5px;">' . 
                            Yii::t('bill', 'Paid') . ': ' . number_format($this->billPaidSumma, 2, '.', ' ') . 
                        '</span>'.
                        '<br/>'.
                        '<span style="padding-top: 5px; font-weight: bold;">' . 
                            Yii::t('bill', 'Debt') . ': ' . number_format($this->billNotPaidSumma, 2, '.', ' ') . 
                        '</span>';
                }
            }
        }
        return $result;
    }

    public function getMailStatusHTML()
    {
        return (!empty($this->mail_status) ?
            Html::a(
                Html::badge($this->billMailStatusList[$this->mail_status]), Url::to(['/bill/index']) . '?BillSearch[mail_status]=' . $this->mail_status, [/* 'target' => '_blank', */'data-pjax' => 0]
            ) : null);
    }

    public function getTransferStatusHTML()
    {
        return (!empty($this->transfer_status) ?
            Html::a(
                Html::badge($this->billTransferStatusList[$this->transfer_status], ['class' => $this->transferBackgroundColor . ' status-badge']), Url::to(['/bill/index']) . '?BillSearch[transfer_status]=' . $this->transfer_status, [/* 'target' => '_blank', */'data-pjax' => 0]
            ) : null);
    }
    
    public function getStaticStatusBackgroundColor($status = null)
    {
        $status = !empty($status) ? $status : null;
        switch ($status) {
            case Bill::BILL_STATUS_PREPAR:
            case Bill::BILL_STATUS_CANCELED:
                $class = 'badge-default';
                break;
            case Bill::BILL_STATUS_NEW:
                $class = 'badge-info';
                break;
            case Bill::BILL_STATUS_READY:
                $class = 'badge-primary';
                break;
            case Bill::BILL_STATUS_SIGNED:
                $class = 'badge-danger';
                break;
            case Bill::BILL_STATUS_PREP_PAYMENT:
                $class = 'badge-warning';
                break;
            case Bill::BILL_STATUS_PAYMENT:
                $class = 'badge-warning';
                break;
            case Bill::BILL_STATUS_PAID:
            case Bill::BILL_STATUS_COMPLETE:
                $class = 'badge-success';
                break;
            default:
                $class = 'badge-default';
                break;
        }
        return $class;
    }
    
    public function getStaticTransferBackgroundColor($status = null)
    {
        $status = !empty($status) ? $status : null;
        $abonentModel = $this->currentAbonent;
        switch ($status) {
            case Bill::BILL_TRANSFER_STATUS_SENT:
                $class = ($abonentModel->primary_abonent == 1 ? 'badge-success' : 'badge-default');
                break;
            case Bill::BILL_TRANSFER_STATUS_RECEIVED:
                $class = ($abonentModel->primary_abonent == 1 ? 'badge-success' : 'badge-default');
                break;
            default:
                $class = 'badge-default';
                break;
        }
        return $class;
    }
    
    public function getPayStatusBackgroundColor()
    {
        return $this->getStaticPayStatusBackgroundColor($this->pay_status);
    }

    static public function getStaticPayStatusBackgroundColor($status = null)
    {
        $status = !empty($status) ? $status : null;
        switch ($status) {
            case Bill::BILL_PAY_STATUS_NOT:
                $class = 'badge-default';
                break;
            case Bill::BILL_PAY_STATUS_PART:
                $class = 'badge-warning';
                break;
            case Bill::BILL_PAY_STATUS_FULL:
                $class = 'badge-success';
                break;
            case Bill::BILL_PAY_STATUS_OVER:
                $class = 'badge-info';
                break;
            default:
                $class = 'badge-default';
                break;
        }
        return $class;
    }

    public function checkPDF()
    {
        if(!$this->doc_key){
            $this->createPdf();
        //}elseif(!YII_ENV_PROD){
        }elseif(!$this->blocked && $this->canUpdate()){
            $filename = "invoice-{$this->printDocNumber}-{$this->doc_key}.pdf";
            $storagePath = Yii::getAlias(PrintModule::INVOICES_DIR.'/'.$this->agreement->first_client_id);
            $filepath = "$storagePath/$filename";
            if (file_exists($filepath)) {
                unlink($filepath);
            }            
            $this->createPdf();
        }            
    }
    
    public function createPdf()
    {
        $docKey = Yii::$app->security->generateRandomString();
        $agreement = $this->agreement;
        if (!$agreement) {
            return false;
        }

        $firstClient = ($this->doc_type != Bill::BILL_DOC_TYPE_CESSION) ? $agreement->firstClient : ($this->cession_direction == 'D' ? $agreement->firstClient : $agreement->thirdClient);
        $secondClient = ($this->doc_type != Bill::BILL_DOC_TYPE_CESSION) ? $agreement->secondClient : ($this->cession_direction == 'D' ? $agreement->thirdClient : $agreement->secondClient);

        $avansNumberList = '';
        $avansSumma = $paidSumma = 0;

        $parent = $this->parent;
        if (!empty($parent)) {
            //$avansSumma = $parent->total;
            $avansNumberList = '# ' . $parent->doc_number;
        }

        $parentList = $this->parents;
        if (!empty($parentList)) {
            $avansSumma = $this->parentPaymentsSumma;
            if($parentList[0]->valuta_id != $this->valuta_id){
                $avansSumma = $avansSumma * $this->rate;
            }
            $avansNumberList = [];
            foreach ($parentList as $bill) {
                $avansNumberList[] = $bill->doc_number;
            }
            $avansNumberList = ((count($avansNumberList) > 1) ? '## ' : '# ') . implode(', ', $avansNumberList);
        }
        
        $childList = $this->childs;
        if (!empty($childList)) {
            $paidSumma = $this->childsPaidSumma;
            if($childList[0]->valuta_id != $this->valuta_id){
                $paidSumma = $paidSumma * $this->rate;
            }
        }
        
        $oldLng = Yii::$app->language;
        $language = empty($this->language_id) ? Yii::$app->params['billDefaultPrintLanguage'] : $this->language->language;
        if ($language == 'lv') {
            Yii::$app->language = 'lv-LV';
        }
        $data = [
            'doc-type' => $this->doc_type,
            'template-dir' => 'invoice',
            'clientId' => $firstClient->id,
            //'mode' => 'FI',
            'mode' => 'F',
            'doc-key' => $docKey,
            'doc_number' => $this->printDocNumber,
            'agreement' => $agreement,
            'invoice' => $this,
            'billProducts' => $this->billProducts,
            'firstClient' => $firstClient,
            'secondClient' => $secondClient,
            'firstClientBank' => $this->firstClientBank,
            'firstClientAllBanks' => $firstClient->clientBanks,
            'secondClientBank' => $this->secondClientBank,
            'billPersons' => $this->billPersons,
            'firstClientAddress' => $firstClient->fullLegalAddress,
            'secondClientAddress' => $secondClient->fullLegalAddress,
            'avansNumberList' => $avansNumberList,
            'avansSumma' => $avansSumma,
            'paidSumma' => $paidSumma,
            'pdfSettings' => $this->firstClient->clientInvoicePdf,
        ];
        Yii::$app->language = $oldLng;

        $printModule = new PrintModule($data);
        $pdfFilename = $printModule->printDoc();
        unset($printModule);

        $result = $pdfFilename && (bool) $this->updateAttributes([
            'doc_key' => $docKey
        ]);

        if (in_array($data['mode'], ['I', 'FI'])) {
            exit;
        }

        return $result;
    }

    public function changeStatus($status, $params = [])
    {
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            $arrForUpdate = ['status' => $status];
            $arrForUpdate = ArrayHelper::merge($arrForUpdate, $params);
            $paymentsSumma = $this->getBillPaidSumma(true);
            $childIsCI = false;
            $childs = $this->childs;
            foreach ($childs as $child) {
                $childIsCI = $childIsCI || ($child->doc_type == Bill::BILL_DOC_TYPE_CRBILL);
            }
            if(empty($arrForUpdate['pay_status'])){
                if (($paymentsSumma == 0) && !$childIsCI){
                    $arrForUpdate['pay_status'] = Bill::BILL_PAY_STATUS_NOT;
                } elseif($this->compareFloatNumbers($paymentsSumma, $this->total, '=')) {
                    $arrForUpdate['pay_status'] = Bill::BILL_PAY_STATUS_FULL;
                } elseif($this->compareFloatNumbers($paymentsSumma, $this->total, '>')) {
                    $arrForUpdate['pay_status'] = Bill::BILL_PAY_STATUS_OVER;
                } elseif($this->compareFloatNumbers($paymentsSumma, $this->total, '<')) {
                    $arrForUpdate['pay_status'] = Bill::BILL_PAY_STATUS_PART;
                }
            }
            switch ($status) {
                case Bill::BILL_STATUS_PREPAR:
                case Bill::BILL_STATUS_NEW:
                case Bill::BILL_STATUS_READY:
                case Bill::BILL_STATUS_CANCELED:
                    $arrForUpdate['pay_status'] = !empty($arrForUpdate['pay_status']) ? $arrForUpdate['pay_status'] : ($childIsCI ? Bill::BILL_PAY_STATUS_PART : null);
                    break;
                case Bill::BILL_STATUS_SIGNED:
                    $arrForUpdate['pay_status'] = !empty($arrForUpdate['pay_status']) ? $arrForUpdate['pay_status'] : ($childIsCI ? Bill::BILL_PAY_STATUS_PART : Bill::BILL_PAY_STATUS_NOT);
                    break;
                case Bill::BILL_STATUS_PREP_PAYMENT:
                    break;
                case Bill::BILL_STATUS_PAYMENT:
                    break;
                case Bill::BILL_STATUS_PAID:
                    $arrForUpdate['pay_status'] = ($this->paymentsSummaIsLess ? Bill::BILL_PAY_STATUS_PART : ($this->paymentsSummaIsGrate ? Bill::BILL_PAY_STATUS_OVER : Bill::BILL_PAY_STATUS_FULL));
                    $arrForUpdate['delayed'] = (int) (($this->pay_date < date('Y-m-d')) && $this->paymentsSummaIsLess);
                    if (in_array($arrForUpdate['pay_status'], [Bill::BILL_PAY_STATUS_FULL, Bill::BILL_PAY_STATUS_OVER])) {
                        $arrForUpdate['status'] = Bill::BILL_STATUS_COMPLETE;
                        $arrForUpdate['complete_date'] = date('Y-m-d');
                        $arrForUpdate['returned'] = 0;
                        $historyModule = new BillHistory();
                        $historyModule->saveHistory($this->id, BillHistory::BillHistory_ACTIONS['ACTION_STATUS_UP_COMPLETE']);
                        unset($historyModule);
                    }
                    if (empty($arrForUpdate['paid_date']) && empty($this->paid_date)) {
                        $arrForUpdate['paid_date'] = date('Y-m-d');
                    }
                    $parentList = $this->parents;
                    foreach ($parentList as $parent) {
                        if ($this->total < $parent->total) {
                            $parent->updateAttributes(['pay_status' => BILL::BILL_PAY_STATUS_PART_CLOSED]);
                        } else {
                            //}elseif($this->total >= $parent->total){
                            $parent->updateAttributes(['pay_status' => BILL::BILL_PAY_STATUS_FULL]);
                        }
                    }
                    break;
                case Bill::BILL_STATUS_COMPLETE:
                    $arrForUpdate['pay_status'] = ($this->doc_type != Bill::BILL_DOC_TYPE_AVANS ? Bill::BILL_PAY_STATUS_FULL :
                            ($this->pay_status != Bill::BILL_PAY_STATUS_PART_CLOSED ? Bill::BILL_PAY_STATUS_FULL : Bill::BILL_PAY_STATUS_PART_CLOSED));
                    $arrForUpdate['delayed'] = 0;
                    $arrForUpdate['returned'] = 0;
                    $arrForUpdate['complete_date'] = date('Y-m-d');
                    if (empty($arrForUpdate['paid_date']) && empty($this->paid_date)) {
                        $arrForUpdate['paid_date'] = date('Y-m-d');
                    }
                    /*
                      $parentList = $this->parents;
                      foreach ($parentList as $parent) {
                      if($parent->pay_status == Bill::BILL_PAY_STATUS_PART_CLOSED){
                      //$parent->paymentsSumma - $parent->childsPaidSumma
                      $childList = $parent->child;

                      }
                      }
                     * 
                     */
                    break;
                default:
                    break;
            }

            $this->updateAttributes($arrForUpdate);
            $transaction->commit();
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $transaction->rollBack();
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
        }
        return true;
    }

    public function getProgressButtons($btnSize = '', $labeled = false)
    {
        if(!FSMAccessHelper::can('changeBillStatus')){
            return '';
        }
        
        static $user;
        if(!isset($user)){
            $user = Yii::$app->user->identity;
        }
                
        $abonentSimpleWorkflow = Yii::$app->session->get('user_current_abonent_workflow');
        
        if (!empty($btnSize)) {
            $btnSize = 'btn-' . $btnSize;
        }

        if($abonentSimpleWorkflow){
            switch ($this->status) {
                case Bill::BILL_STATUS_PREPAR:
                    return
                        FSMHelper::aButton($this->id, [
                            'label' => ($labeled ? Yii::t('bill', 'Sign') : null),
                            'title' => Yii::t('bill', 'Sign'),
                            'controller' => 'bill-history',
                            'action' => 'bill-sign',
                            'class' => 'danger',
                            'size' => $btnSize,
                            'icon' => 'arrow-up',
                                //'modal' => true,
                        ]);
                    break;
                case Bill::BILL_STATUS_SIGNED:
                    return
                        FSMHelper::vButton($this->id, [
                            'label' => ($labeled ? Yii::t('bill', 'To preparing') : null),
                            'title' => Yii::t('bill', 'Rollback to preparing'),
                            'controller' => 'bill-history',
                            'action' => 'bill-rollback-prepar',
                            'class' => 'default',
                            'size' => $btnSize,
                            'icon' => 'arrow-down',
                            'modal' => true,
                        ]) . '&nbsp;' .
                        (!$user->hasRole([
                            FSMUser::USER_ROLE_CONTRAGENT, 
                        ]) ? 
                            FSMHelper::aButton($this->id, [
                                'label' => ($labeled ? Yii::t('bill', 'Prepare payment') : null),
                                'title' => Yii::t('bill', 'Prepare payment'),
                                'controller' => 'bill-history',
                                'action' => 'bill-prep-payment',
                                'class' => 'warning',
                                'size' => $btnSize,
                                'icon' => 'arrow-up',
                                'modal' => true,
                            ])
                        : '');
                    break;
                case Bill::BILL_STATUS_PREP_PAYMENT:
                    return
                        FSMHelper::vButton($this->id, [
                            'label' => ($labeled ? Yii::t('bill', 'To verification') : null),
                            'title' => Yii::t('bill', 'Rollback to verification'),
                            'controller' => 'bill-history',
                            'action' => 'bill-rollback-signed',
                            'class' => 'danger ',
                            'size' => $btnSize,
                            'icon' => 'arrow-down',
                            'modal' => true,
                        ]) . '&nbsp;' .
                        (!$user->hasRole([
                            FSMUser::USER_ROLE_CONTRAGENT, 
                        ]) ? 
                            FSMHelper::vButton($this->id, [
                                'label' => ($labeled ? Yii::t('bill', 'Payment') : null),
                                'title' => Yii::t('bill', 'Payment'),
                                'controller' => 'bill-payment',
                                'action' => 'ajax-create',
                                'class' => 'warning',
                                'size' => $btnSize,
                                'icon' => 'arrow-up',
                                'modal' => true,
                            ])
                        : '');
                    break;
                case Bill::BILL_STATUS_PAYMENT:
                    return
                        FSMHelper::vButton($this->id, [
                            'label' => ($labeled ? Yii::t('bill', 'To payment preparation') : null),
                            'title' => Yii::t('bill', 'Rollback to payment preparation'),
                            'controller' => 'bill-history',
                            'action' => 'bill-rollback-prep-payment',
                            'class' => 'warning',
                            'size' => $btnSize,
                            'icon' => 'arrow-down',
                            'modal' => true,
                        ]) . '&nbsp;' .
                        (!$user->hasRole([
                            FSMUser::USER_ROLE_CONTRAGENT, 
                        ]) ? 
                            FSMHelper::vButton($this->id, [
                                'label' => ($labeled ? Yii::t('bill', 'Confirm payment') : null),
                                'title' => Yii::t('bill', 'Confirm payment'),
                                'controller' => 'bill',
                                'action' => 'ajax-bill-pay',
                                'class' => 'success',
                                'size' => $btnSize,
                                'icon' => 'arrow-up',
                                'modal' => true,
                            ])
                        : '');
                    break;
                case Bill::BILL_STATUS_PAID:
                    if (!in_array($this->pay_status, [Bill::BILL_PAY_STATUS_FULL, Bill::BILL_PAY_STATUS_OVER])) {
                        return
                            FSMHelper::vButton($this->id, [
                                'label' => ($labeled ? Yii::t('bill', 'To payment') : null),
                                'title' => Yii::t('bill', 'Rollback to payment'),
                                'controller' => 'bill-history',
                                'action' => 'bill-rollback-payment',
                                'class' => 'warning',
                                'size' => $btnSize,
                                'icon' => 'arrow-down',
                                'modal' => true,
                            ]) . '&nbsp;' .
                            (!$user->hasRole([
                                FSMUser::USER_ROLE_CONTRAGENT, 
                            ]) ? 
                                FSMHelper::vButton($this->id, [
                                    'label' => ($labeled ? Yii::t('bill', 'Confirm payment') : null),
                                    'title' => Yii::t('bill', 'Confirm payment'),
                                    'controller' => 'bill',
                                    'action' => 'ajax-bill-pay',
                                    'class' => 'success',
                                    'size' => $btnSize,
                                    'icon' => 'arrow-up',
                                    'modal' => true,
                                ])
                            : '');
                    } else {
                        return (!$user->hasRole([
                                FSMUser::USER_ROLE_CONTRAGENT, 
                            ]) ? 
                                FSMHelper::aButton($this->id, [
                                    'label' => ($labeled ? Yii::t('bill', 'Complete') : null),
                                    'title' => Yii::t('bill', 'Complete invoice'),
                                    'controller' => 'bill-history',
                                    'action' => 'bill-complete',
                                    'class' => 'success',
                                    'size' => $btnSize,
                                    'icon' => 'arrow-up',
                                        //'modal' => true,
                                ])
                            : '');
                    }
                    break;
                default:
                    return '';
                    break;
            }
        }else{
            switch ($this->status) {
                case Bill::BILL_STATUS_PREPAR:
                    return
                        FSMHelper::aButton($this->id, [
                            'label' => ($labeled ? Yii::t('bill', 'Register') : null),
                            'title' => Yii::t('bill', 'Register'),
                            'controller' => 'bill-history',
                            'action' => 'bill-register',
                            'class' => 'info',
                            'size' => $btnSize,
                            'icon' => 'arrow-up',
                                //'modal' => true,
                        ]);
                    break;
                case Bill::BILL_STATUS_NEW:
                    return
                        FSMHelper::vButton($this->id, [
                            'label' => ($labeled ? Yii::t('bill', 'To preparing') : null),
                            'title' => Yii::t('bill', 'Rollback to preparing'),
                            'controller' => 'bill-history',
                            'action' => 'bill-rollback-prepar',
                            'class' => 'default',
                            'size' => $btnSize,
                            'icon' => 'arrow-down',
                            'modal' => true,
                        ]) . '&nbsp;' .
                        FSMHelper::aButton($this->id, [
                            'label' => ($labeled ? Yii::t('bill', 'To signature') : null),
                            'title' => Yii::t('bill', 'Send for signature'),
                            'controller' => 'bill-history',
                            'action' => 'bill-send-signature',
                            'class' => 'primary',
                            'size' => $btnSize,
                            'icon' => 'arrow-up',
                                //'modal' => true,
                        ]);
                    break;
                case Bill::BILL_STATUS_READY:
                    return
                        FSMHelper::vButton($this->id, [
                            'label' => ($labeled ? Yii::t('bill', 'To verification') : null),
                            'title' => Yii::t('bill', 'Rollback to verification'),
                            'controller' => 'bill-history',
                            'action' => 'bill-rollback-new',
                            'class' => 'info',
                            'size' => $btnSize,
                            'icon' => 'arrow-down',
                            'modal' => true,
                        ]) . '&nbsp;' .
                        FSMHelper::aButton($this->id, [
                            'label' => ($labeled ? Yii::t('bill', 'Sign') : null),
                            'title' => Yii::t('bill', 'Sign'),
                            'controller' => 'bill-history',
                            'action' => 'bill-sign',
                            'class' => 'danger',
                            'size' => $btnSize,
                            'icon' => 'arrow-up',
                                //'modal' => true,
                        ]);
                    break;
                case Bill::BILL_STATUS_SIGNED:
                    return
                        FSMHelper::vButton($this->id, [
                            'label' => ($labeled ? Yii::t('bill', 'To signing') : null),
                            'title' => Yii::t('bill', 'Rollback to signing'),
                            'controller' => 'bill-history',
                            'action' => 'bill-rollback-ready',
                            'class' => 'primary',
                            'size' => $btnSize,
                            'icon' => 'arrow-down',
                            'modal' => true,
                        ]) . '&nbsp;' .
                        (!$user->hasRole([
                            FSMUser::USER_ROLE_CONTRAGENT, 
                        ]) ? 
                            FSMHelper::aButton($this->id, [
                                'label' => ($labeled ? Yii::t('bill', 'Prepare payment') : null),
                                'title' => Yii::t('bill', 'Prepare payment'),
                                'controller' => 'bill-history',
                                'action' => 'bill-prep-payment',
                                'class' => 'warning',
                                'size' => $btnSize,
                                'icon' => 'arrow-up',
                                'modal' => true,
                            ])
                        : '');
                    break;
                case Bill::BILL_STATUS_PREP_PAYMENT:
                    return
                        FSMHelper::vButton($this->id, [
                            'label' => ($labeled ? Yii::t('bill', 'To verification') : null),
                            'title' => Yii::t('bill', 'Rollback to verification'),
                            'controller' => 'bill-history',
                            'action' => 'bill-rollback-signed',
                            'class' => 'danger ',
                            'size' => $btnSize,
                            'icon' => 'arrow-down',
                            'modal' => true,
                        ]) . '&nbsp;' .
                        (!$user->hasRole([
                            FSMUser::USER_ROLE_CONTRAGENT, 
                        ]) ? 
                            FSMHelper::vButton($this->id, [
                                'label' => ($labeled ? Yii::t('bill', 'Payment') : null),
                                'title' => Yii::t('bill', 'Payment'),
                                'controller' => 'bill-payment',
                                'action' => 'ajax-create',
                                'class' => 'warning',
                                'size' => $btnSize,
                                'icon' => 'arrow-up',
                                'modal' => true,
                            ])
                        : '');
                    break;
                case Bill::BILL_STATUS_PAYMENT:
                    return
                        FSMHelper::vButton($this->id, [
                            'label' => ($labeled ? Yii::t('bill', 'To payment preparation') : null),
                            'title' => Yii::t('bill', 'Rollback to payment preparation'),
                            'controller' => 'bill-history',
                            'action' => 'bill-rollback-prep-payment',
                            'class' => 'warning',
                            'size' => $btnSize,
                            'icon' => 'arrow-down',
                            'modal' => true,
                        ]) . '&nbsp;' .
                        (!$user->hasRole([
                            FSMUser::USER_ROLE_CONTRAGENT, 
                        ]) ? 
                            FSMHelper::vButton($this->id, [
                                'label' => ($labeled ? Yii::t('bill', 'Confirm payment') : null),
                                'title' => Yii::t('bill', 'Confirm payment'),
                                'controller' => 'bill',
                                'action' => 'ajax-bill-pay',
                                'class' => 'success',
                                'size' => $btnSize,
                                'icon' => 'arrow-up',
                                'modal' => true,
                            ])
                        : '');
                    break;
                case Bill::BILL_STATUS_PAID:
                    if (!in_array($this->pay_status, [Bill::BILL_PAY_STATUS_FULL, Bill::BILL_PAY_STATUS_OVER])) {
                        return
                            FSMHelper::vButton($this->id, [
                                'label' => ($labeled ? Yii::t('bill', 'To payment') : null),
                                'title' => Yii::t('bill', 'Rollback to payment'),
                                'controller' => 'bill-history',
                                'action' => 'bill-rollback-payment',
                                'class' => 'warning',
                                'size' => $btnSize,
                                'icon' => 'arrow-down',
                                'modal' => true,
                            ]) . '&nbsp;' .
                            (!$user->hasRole([
                                FSMUser::USER_ROLE_CONTRAGENT, 
                            ]) ? 
                                FSMHelper::vButton($this->id, [
                                    'label' => ($labeled ? Yii::t('bill', 'Confirm payment') : null),
                                    'title' => Yii::t('bill', 'Confirm payment'),
                                    'controller' => 'bill',
                                    'action' => 'ajax-bill-pay',
                                    'class' => 'success',
                                    'size' => $btnSize,
                                    'icon' => 'arrow-up',
                                    'modal' => true,
                                ])
                            : '');
                    } else {
                        return 
                            (!$user->hasRole([
                                FSMUser::USER_ROLE_CONTRAGENT, 
                            ]) ? 
                                FSMHelper::aButton($this->id, [
                                    'label' => ($labeled ? Yii::t('bill', 'Complete') : null),
                                    'title' => Yii::t('bill', 'Complete invoice'),
                                    'controller' => 'bill-history',
                                    'action' => 'bill-complete',
                                    'class' => 'success',
                                    'size' => $btnSize,
                                    'icon' => 'arrow-up',
                                        //'modal' => true,
                                ])
                            : '');
                    }
                    break;
                default:
                    return '';
                    break;
            }
        }
    }

    static function getButtonPrint(array $params)
    {
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url
        if(!FSMAccessHelper::can('viewBillPDF', $model)){
            return null;
        }
        if (!in_array($model->doc_type, [
                Bill::BILL_DOC_TYPE_AVANS,
                Bill::BILL_DOC_TYPE_BILL,
                Bill::BILL_DOC_TYPE_INVOICE,
                Bill::BILL_DOC_TYPE_CRBILL,
                Bill::BILL_DOC_TYPE_DEBT,
                Bill::BILL_DOC_TYPE_CESSION,
            ])
        ) {
            return '';
        }

        $url = (!empty($url) ? $url : ['view-pdf', 'id' => $model->id]);
        if (!empty($isBtn)) {
            $class = !$model->e_signing ? 'success' : 'warning';
            return Html::a(Html::icon('print'), $url, [
                'class' => 'btn btn-xs btn-'.$class,
                'title' => Yii::t('common', 'Print'),
                'target' => '_blank',
                'data-pjax' => 0,
            ]);
        } else {
            $label = Html::icon('print') . '&nbsp;' . Yii::t('common', 'Print');
            $options = [
                'target' => '_blank',
                'data-pjax' => 0,
            ];
            return '<li>' . Html::a($label, $url, $options) . '</li>' . PHP_EOL;
        }
    }

    static function getButtonActPrint(array $params)
    {
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url
        if(!FSMAccessHelper::can('viewBillActPDF', $model)){
            return null;
        }
        if (empty($model->has_act)) {
            return '';
        }
        if (!in_array($model->doc_type, [
                Bill::BILL_DOC_TYPE_AVANS,
                Bill::BILL_DOC_TYPE_BILL,
                Bill::BILL_DOC_TYPE_INVOICE,
                Bill::BILL_DOC_TYPE_CRBILL,
                Bill::BILL_DOC_TYPE_DEBT,
                Bill::BILL_DOC_TYPE_CESSION,
            ])
        ) {
            return '';
        }

        $url = (!empty($url) ? $url : ['view-act-pdf', 'id' => $model->id]);
        if (!empty($isBtn)) {
            return Html::a(Html::icon('print'), $url, [
                'class' => 'btn btn-sm btn-success',
                'title' => Yii::t('bill', 'Print act'),
                'target' => '_blank',
                'data-pjax' => 0,
            ]);
        } else {
            $label = Html::icon('print') . '&nbsp;' . Yii::t('bill', 'Print act');
            $options = [
                'target' => '_blank',
                'data-pjax' => 0,
            ];
            return '<li>' . Html::a($label, $url, $options) . '</li>' . PHP_EOL;
        }
    }

    static function getButtonAddAttachment(array $params)
    {
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url
        
        if(!FSMAccessHelper::can('updateBill')){
            return '';
        }

        $label = $title = Yii::t('bill', 'Add attachment');
        $controller = 'bill';
        $action = 'ajax-add-attachment';
        $icon = 'plus-sign';
        
        if (!empty($isBtn)) {
            return FSMHelper::vButton($model->id, [
                'title' => (!empty($labeled) ? null : $title),
                'label' => $label,
                'controller' => $controller,
                'action' => $action,
                'url' => $url,
                'class' => 'info',
                'size' => (!empty($btnSize) ? $btnSize : null),
                'icon' => $icon,
                'modal' => true,
            ]);
        } else {
            return FSMHelper::vDropdown($model->id, [
                'label' => $label,
                'title' => $title,
                'controller' => $controller,
                'action' => $action,
                'url' => $url,
                'icon' => $icon,
                'modal' => true,
            ]);
        }
    }
    
    static function getButtonViewAttachment(array $params)
    {
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url

        if (empty($model->attachments)) {
            return '';
        }

        $filesModel = $model->attachmentFiles;
        if (!empty($isBtn)) {
            $url = $filesModel[0]->fileurl;
            return Html::a(Html::icon('paperclip'), $url, [
                'class' => 'btn btn-xs btn-info',
                'title' => Yii::t('common', 'View attachment'),
                'target' => '_blank',
                'data-pjax' => '0',
            ]);
        } else {
            $label = Yii::t('common', 'View attachment');
            $options = [
                'target' => '_blank',
                'data-pjax' => 0,
            ];
            if(count($filesModel) > 1){
                $result = '<li class="dropdown-submenu pull-left">';
                $result .= Html::a(Html::icon('paperclip').'&nbsp;'.$label, '#', [
                    'class' => 'dropdown-toggle',
                    'data-toggle' => 'dropdown',
                    'tabindex' => -1,
                    'data-pjax' => 0,
                ]);                    
                $items = [];
                $index = 1;
                foreach ($filesModel as $file) {
                    $url = $file->fileurl;
                    $items[] = ['label' => $label.'-'.$index, 'url' => $url, 'linkOptions' => $options];
                    $index++;
                }
                $result .= \kartik\dropdown\DropdownX::widget([
                    'items' => $items,
                ]);
                $result .= '</li>';
                return $result;
            }else{
                $url = $filesModel[0]->fileurl;
                return '<li>' . Html::a(Html::icon('paperclip').'&nbsp;'.$label, $url, $options) . '</li>' . PHP_EOL;
            }
        }
    }
    
    static function getButtonSendOtherAbonent(array $params)
    {
        if(empty(Yii::$app->params['ENABLE_BILL_TRANSFER'])){
            return '';
        }
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url
        if(!FSMAccessHelper::can('sendToAnotherAbonent', $model)){
            return null;
        }            

        if (!in_array($model->status, [
                Bill::BILL_STATUS_SIGNED,
                Bill::BILL_STATUS_PREP_PAYMENT,
                Bill::BILL_STATUS_PAYMENT,
                Bill::BILL_STATUS_PAID,
                Bill::BILL_STATUS_COMPLETE,
            ]) || !empty($model->transfer_status)
        ){
            return '';
        }

        $agreement = $model->agreement;
        if (empty($agreement->transfer_status) ||
            !in_array($agreement->transfer_status, [
                    Agreement::AGREEMENT_TRANSFER_STATUS_RECEIVED,
            ])
        ){
            return '';
        }
        
        if (!empty($isBtn)) {
            $result = FSMHelper::aButton($model->id, [
                'label' => Yii::t('common', 'Send'),
                'title' => Yii::t('common', 'Send to another abonent'),
                'controller' => 'abonent-bill',
                'action' => 'send-other-abonent',
                'icon' => 'log-out',
            ]);
        } else {
            $result = FSMHelper::aDropdown($model->id, [
                'label' => Yii::t('common', 'Send'),
                'title' => Yii::t('common', 'Send to another abonent'),
                'controller' => 'abonent-bill',
                'action' => 'send-other-abonent',
                'icon' => 'log-out',
            ]);
        }
        return $result;
    }
    
    static function getButtonReceiveOtherAbonent(array $params)
    {
        if(empty(Yii::$app->params['ENABLE_BILL_TRANSFER'])){
            return '';
        }

        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url

        if (empty($model->transfer_status) || 
            ($model->transfer_status != Bill::BILL_TRANSFER_STATUS_SENT)
        ) {
            return '';
        }

        if (!empty($isBtn)) {
            $result = FSMHelper::aButton($model->id, [
                'label' => Yii::t('common', 'Receive'),
                'title' => Yii::t('common', 'Receive from another abonent'),
                'controller' => 'abonent-bill',
                'action' => 'receive-other-abonent',
                'icon' => 'log-in',
            ]);
        } else {
            $result = FSMHelper::aDropdown($model->id, [
                'label' => Yii::t('common', 'Receive'),
                'title' => Yii::t('common', 'Receive from another abonent'),
                'controller' => 'abonent-bill',
                'action' => 'receive-other-abonent',
                'icon' => 'log-in',
            ]);
        }
        return $result;
    }
    
    static function getButtonSendMailClient(array $params)
    {
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url
        if(!FSMAccessHelper::can('sendBillToClient', $model)){
            return null;
        }
        
        $user = Yii::$app->user->identity;
        if(!isset($user) ||
            $user->hasRole([
                FSMUser::USER_ROLE_AGENT,
            ]
        )){
            return '';
        }
        
        if (!in_array($model->doc_type, [
                Bill::BILL_DOC_TYPE_AVANS,
                Bill::BILL_DOC_TYPE_BILL,
                Bill::BILL_DOC_TYPE_INVOICE,
                Bill::BILL_DOC_TYPE_CRBILL,
                Bill::BILL_DOC_TYPE_DEBT,
                Bill::BILL_DOC_TYPE_CESSION,
            ])
            /*||
            !in_array($model->status, [
                Bill::BILL_STATUS_SIGNED,
                Bill::BILL_STATUS_PREP_PAYMENT,
                Bill::BILL_STATUS_PAYMENT,
                Bill::BILL_STATUS_PAID,
            ])*/
        ){
            return '';
        }

        $label = Yii::t('bill', 'Send to client');
        $title = Yii::t('bill', 'Send invoice to client as attachment of email');
        $message = Yii::t('bill', 'Are you sure you want to send this invoice?');
        $icon = 'envelope';
        if (!empty($isBtn)) {
            $result = FSMHelper::aButton($model->id, [
                'label' => $label,
                'title' => $title,
                'class' => 'success',
                'url' => $url,
                'icon' => $icon,
                'message' => $message,
            ]);            
        } else {
            $result = FSMHelper::aDropdown($model->id, [
                'label' => $label,
                'title' => $title,
                'url' => $url,
                'icon' => $icon,
                'message' => $message,
            ]);
        }
        return $result;
    }
    
    static function getButtonSendMailAgent(array $params)
    {
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url
        if(!FSMAccessHelper::can('sendBillToAgent', $model)){
            return null;
        }
        
        $user = Yii::$app->user->identity;
        if(!isset($user) ||
            !$user->hasRole([
                FSMUser::USER_ROLE_BOOKER,
                FSMUser::USER_ROLE_OPERATOR,
            ]
        )){
            return '';
        }

        $label = Yii::t('common', 'Send to agent');
        $title = Yii::t('common', 'Send invoice to agent as attachment of email');
        $message = Yii::t('bill', 'Are you sure you want to send this invoice?');
        $icon = 'envelope';
        if (!empty($isBtn)) {
            $result = FSMHelper::aButton($model->id, [
                'label' => $label,
                'title' => $title,
                'class' => 'success',
                'url' => $url,
                'icon' => $icon,
                'message' => $message,
            ]);            
        } else {
            $result = FSMHelper::aDropdown($model->id, [
                'label' => $label,
                'title' => $title,
                'url' => $url,
                'icon' => $icon,
                'message' => $message,
            ]);
        }
        return $result;
    }
    
    static function getButtonCopy(array $params)
    {
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url
        
        if(!FSMAccessHelper::can('createBill')){
            return '';
        }

        if (!in_array($model->doc_type, [
            Bill::BILL_DOC_TYPE_BILL,
            Bill::BILL_DOC_TYPE_INVOICE,
        ])) {
            return '';
        }

        $action = 'copy';
        $label = Yii::t('common', 'Copy');
        $title = Yii::t('bill', 'Copy current invoice');
        $icon = 'duplicate';
        if (!empty($isBtn)) {
            return FSMHelper::aButton($model->id, [
                'label' => $label,
                'title' => $title,
                'action' => $action,
                'data-pjax' => 0,
                'icon' => $icon,
            ]);            
        } else {
            return FSMHelper::aDropdown($model->id, [
                'label' => $label,
                'title' => $title,
                'action' => $action,
                'data-pjax' => 0,
                'icon' => $icon,
            ]);
        }
    }

    static function getButtonBlock(array $params)
    {
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url
        
        if(!FSMAccessHelper::can('blockBill')){
            return '';
        }
        
        $action = empty($model->blocked) ? 'block' : 'unblock';
        $label = empty($model->blocked) ? Yii::t('common', 'Block') : Yii::t('common', 'Unblock');
        $title = empty($model->blocked) ? Yii::t('common', 'Block current invoice') : Yii::t('common', 'Unblock current invoice');
        $message = empty($model->blocked) ? Yii::t('bill', 'Are you sure you want to block this invoice?') : Yii::t('bill', 'Are you sure you want to unblock this invoice?');
        $icon = empty($model->blocked) ? 'ban-circle' : 'ok-circle';
        if (!empty($isBtn)) {
            $result = FSMHelper::aButton($model->id, [
                'label' => $label,
                'title' => $title,
                'class' => empty($model->blocked) ? 'danger' : 'success',
                'action' => $action,
                'icon' => $icon,
                'message' => $message,
            ]);            
        } else {
            $result = FSMHelper::aDropdown($model->id, [
                'label' => $label,
                'title' => $title,
                'action' => $action,
                'icon' => $icon,
                'message' => $message,
            ]);
        }
        return $result;
    }
    
    public function getCellButtonBlock()
    {
        if(!FSMAccessHelper::can('blockBill')){
            return '';
        }
        
        $action = empty($this->blocked) ? 'block' : 'unblock';
        //$label = empty($this->blocked) ? Yii::t('common', 'Block') : Yii::t('common', 'Unblock');
        $title = empty($this->blocked) ? Yii::t('common', 'Block current invoice') : Yii::t('common', 'Unblock current invoice');
        $message = empty($this->blocked) ? Yii::t('bill', 'Are you sure you want to block this invoice?') : Yii::t('bill', 'Are you sure you want to unblock this invoice?');
        $icon = empty($this->blocked) ? 'ban-circle' : 'ok-circle';
        $result = FSMHelper::aButton($this->id, [
            //'label' => $label,
            'title' => $title,
            'class' => (empty($this->blocked) ? 'danger' : 'success'),
            'action' => $action,
            'size' => 'btn-sm',
            'icon' => $icon,
            'message' => $message,
        ]);
        return $result;
    }
    
    static function getButtonWriteOnBasis(array $params)
    {
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url
        
        if(!FSMAccessHelper::can('createBill')){
            return '';
        }
        
        $result = '';
        if (($model->doc_type == Bill::BILL_DOC_TYPE_AVANS) &&
            (empty($model->child_id) || ($model->pay_status == Bill::BILL_PAY_STATUS_PART_CLOSED))
        ) {
            if (!empty($isBtn)) {
                $result = FSMHelper::aButton($model->id, [
                    'title' => Yii::t('bill', 'Write out on this basis'),
                    'icon' => 'share',
                    'action' => 'bill-write-on-basis',
                    'size' => $btnSize,
                ]);
            } else {
                $result = FSMHelper::aDropdown($model->id, [
                    'label' => Yii::t('bill', 'Write out on this basis'),
                    'title' => Yii::t('bill', 'Write out on this basis'),
                    'action' => 'bill-write-on-basis',
                    'icon' => 'share',
                ]);
            }
        }
        return $result;
    }

    static function getButtonPayment(array $params)
    {
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url
        
        if(!FSMAccessHelper::can('updateBill')){
            return '';
        }
        
        $result = '';
        $childs = $model->childs;
        $childIsCI = false;
        foreach ($childs as $child) {
            $childIsCI = $childIsCI || ($child->doc_type == Bill::BILL_DOC_TYPE_CRBILL);
        }
        if (in_array($model->doc_type, [
                    Bill::BILL_DOC_TYPE_AVANS,
                    Bill::BILL_DOC_TYPE_BILL,
                    Bill::BILL_DOC_TYPE_CESSION,
                ]) &&
                in_array($model->status, [
                    Bill::BILL_STATUS_PREP_PAYMENT,
                    Bill::BILL_STATUS_PAYMENT,
                    Bill::BILL_STATUS_PAID,
                ]) &&
                (!$childIsCI || (
                    $childIsCI && $model->paymentsSummaIsLess)
                ) &&
                $model->paymentsSummaIsLess
        ) {

            if (!empty($isBtn)) {
                $result = FSMHelper::vButton($model->id, [
                    'label' => ($labeled ? Yii::t('bill', 'Payment') : null),                    
                    'title' => ($labeled ? null : Yii::t('bill', 'Payment')),
                    'controller' => 'bill-payment',
                    'action' => 'ajax-create',
                    'class' => 'primary',
                    'size' => $btnSize,
                    'icon' => 'eur',
                    'modal' => true,
                ]);
            } else {
                $result = FSMHelper::vDropdown($model->id, [
                    'label' => Yii::t('bill', 'Payment'),
                    'title' => Yii::t('bill', 'Payment'),
                    'controller' => 'bill-payment',
                    'action' => 'ajax-create',
                    'icon' => 'eur',
                    'modal' => true,
                ]);
            }
        }
        return $result;
    }

    static function getButtonCreditInvoice(array $params)
    {
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url
        
        if(!FSMAccessHelper::can('createBill')){
            return '';
        }
        
        $result = '';
        $childs = $model->childs;
        $childIsCI = false;
        foreach ($childs as $child) {
            $childIsCI = $childIsCI || ($child->doc_type == Bill::BILL_DOC_TYPE_CRBILL);
        }
        if (in_array($model->doc_type, [
                Bill::BILL_DOC_TYPE_BILL,
                Bill::BILL_DOC_TYPE_CESSION,
                Bill::BILL_DOC_TYPE_INVOICE,
            ]) &&
            in_array($model->status, [
                Bill::BILL_STATUS_SIGNED,
                Bill::BILL_STATUS_PREP_PAYMENT,
                Bill::BILL_STATUS_PAYMENT,
                Bill::BILL_STATUS_PAID,
                Bill::BILL_STATUS_COMPLETE,
            ]) &&
            !$childIsCI
        ){
            if (!empty($isBtn)) {
                $result = FSMHelper::aButton($model->id, [
                    'label' => ($labeled ? Yii::t('bill', 'Credit invoice') : null),                    
                    'title' => ($labeled ? null : Yii::t('bill', 'Credit invoice')),
                    'icon' => 'duplicate',
                    'action' => 'bill-credit-invoice',
                    'size' => $btnSize,
                ]);
            } else {
                $result = FSMHelper::aDropdown($model->id, [
                    'label' => Yii::t('bill', 'Credit invoice'),
                    'title' => Yii::t('bill', 'Credit invoice'),
                    'action' => 'bill-credit-invoice',
                    'icon' => 'duplicate',
                ]);
            }
        }
        return $result;
    }

    static function getButtonMutualSettlement(array $params)
    {
        return null;
        
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url
        
        if(!FSMAccessHelper::can('createBill')){
            return '';
        }
        
        $result = '';
        $childs = $model->childs;
        $childIsCI = false;
        foreach ($childs as $child) {
            $childIsCI = $childIsCI || ($child->doc_type == Bill::BILL_DOC_TYPE_CRBILL);
        }
        if (in_array($model->doc_type, [
                    Bill::BILL_DOC_TYPE_BILL,
                    Bill::BILL_DOC_TYPE_CESSION,
                ]) &&
                in_array($model->status, [
                    Bill::BILL_STATUS_SIGNED,
                    Bill::BILL_STATUS_PREP_PAYMENT,
                    Bill::BILL_STATUS_PAYMENT,
                    Bill::BILL_STATUS_PAID,
                ]) &&
                !$childIsCI &&
                $model->paymentsSummaIsLess
        ){
            if (!empty($isBtn)) {
                $result = FSMHelper::aButton($model->id, [
                    'title' => Yii::t('bill', 'Set-off'),
                    'icon' => 'transfer',
                    'action' => '#',
                    //'action' => 'bill-mutual-settlement',                                        
                    'size' => $btnSize,
                ]);
            } else {
                $result = FSMHelper::aDropdown($model->id, [
                    'label' => Yii::t('bill', 'Set-off'),
                    'title' => Yii::t('bill', 'Set-off'),
                    'action' => '#',
                    //'action' => 'bill-mutual-settlement',                                        
                    'icon' => 'transfer',
                ]);
            }
        }
        return $result;
    }

    static function getButtonCession(array $params)
    {
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url
        
        if(!FSMAccessHelper::can('createBill')){
            return '';
        }
        
        $result = '';
        $childs = $model->childs;
        $childIsCI = false;
        foreach ($childs as $child) {
            $childIsCI = $childIsCI || ($child->doc_type == Bill::BILL_DOC_TYPE_CRBILL);
        }
        if (in_array($model->doc_type, [
                Bill::BILL_DOC_TYPE_BILL,
                Bill::BILL_DOC_TYPE_CESSION,
            ]) &&
            in_array($model->status, [
                Bill::BILL_STATUS_SIGNED,
                Bill::BILL_STATUS_PREP_PAYMENT,
                Bill::BILL_STATUS_PAYMENT,
                Bill::BILL_STATUS_PAID,
            ]) &&
            !$childIsCI &&
            $model->paymentsSummaIsLess
        ){

            $agreementList = Agreement::findAll([
                'agreement.first_client_id' => $model->firstClient->id,
                'agreement.second_client_id' => $model->secondClient->id,
                'agreement.agreement_type' => Agreement::AGREEMENT_TYPE_CESSION,
                'agreement.status' => Agreement::AGREEMENT_STATUS_SIGNED,
                'agreement.deleted' => 0]);
            if (empty($agreementList)) {
                return '';
            }
            if (!empty($isBtn)) {
                $result = FSMHelper::aButton($model->id, [
                    'title' => Yii::t('bill', 'Cession'),
                    'action' => 'bill-cession',
                    'icon' => 'random',
                    'size' => $btnSize,
                ]);
            } else {
                $result = FSMHelper::aDropdown($model->id, [
                    'label' => Yii::t('bill', 'Cession'),
                    'title' => Yii::t('bill', 'Cession'),
                    'action' => 'bill-cession',
                    'icon' => 'random',
                ]);
            }
        }
        return $result;
    }

    static function getButtonDebtRelief(array $params)
    {
        return null;
        
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url
        
        if(!FSMAccessHelper::can('createBill')){
            return '';
        }
        
        $result = '';
        $childs = $model->childs;
        $childIsCI = false;
        foreach ($childs as $child) {
            $childIsCI = $childIsCI || ($child->doc_type == Bill::BILL_DOC_TYPE_CRBILL);
        }
        if (in_array($model->doc_type, [
                Bill::BILL_DOC_TYPE_BILL,
                Bill::BILL_DOC_TYPE_INVOICE,
                Bill::BILL_DOC_TYPE_CESSION,
            ]) &&
            in_array($model->status, [
                Bill::BILL_STATUS_SIGNED,
                Bill::BILL_STATUS_PREP_PAYMENT,
                Bill::BILL_STATUS_PAYMENT,
                Bill::BILL_STATUS_PAID,
            ]) &&
            !$childIsCI &&
            $model->paymentsSummaIsLess
        ){
            if (!empty($isBtn)) {
                $result = FSMHelper::aButton($model->id, [
                    'title' => Yii::t('bill', 'Debt relief'),
                    'icon' => 'remove-circle',
                    'action' => '#',
                    //'action' => 'bill-debt-relief',  
                    'class' => 'danger',
                    'size' => $btnSize,
                ]);
            } else {
                $result = FSMHelper::aDropdown($model->id, [
                    'label' => Yii::t('bill', 'Debt relief'),
                    'title' => Yii::t('bill', 'Debt relief'),
                    'action' => '#',
                    //'action' => 'bill-debt-relief',  
                    'icon' => 'remove-circle',
                ]);
            }
        }
        return $result;
    }

    static function getButtonCancel(array $params)
    {
        extract($params); //$isBtn, $isDropdown, $key, $linkedObjParam, $model, $url
        
        if(!FSMAccessHelper::can('updateBill')){
            return '';
        }
        
        $result = '';
        switch ($model->status) {
            case Bill::BILL_STATUS_NEW:
                //case Bill::BILL_STATUS_READY:
                //case Bill::BILL_STATUS_SIGNED:
                //case Bill::BILL_STATUS_PAYMENT:
                //case Bill::BILL_STATUS_PAID:
                if ($model->paymentsSumma == 0) {
                    if (!empty($isBtn)) {
                        $result = FSMHelper::vButton($model->id, [
                            'label' => ($labeled ? Yii::t('common', 'Cancel') : null),
                            'title' => ($labeled ? null : Yii::t('common', 'Cancel')),
                            'controller' => 'bill-history',
                            'action' => 'bill-cancel',
                            'class' => 'default',
                            'size' => $btnSize,
                            'icon' => 'ban-circle',
                            'modal' => true,
                        ]);
                    } else {
                        $result = FSMHelper::vDropdown($model->id, [
                            'label' => Yii::t('common', 'Cancel'),
                            'title' => Yii::t('common', 'Cancel'),
                            'controller' => 'bill-history',
                            'action' => 'bill-cancel',
                            'icon' => 'ban-circle',
                            'modal' => true,
                        ]);
                    }
                }
                break;
            default:
                break;
        }
        return $result;
    }

    public function getOptionsButtons($btnSize = '', $labeled = false)
    {
        if (!empty($btnSize)) {
            $btnSize = 'btn-' . $btnSize;
        }

        if (in_array($this->status, [
                Bill::BILL_STATUS_PREPAR,
                Bill::BILL_STATUS_CANCELED,
            ])
        ) {
            return '';
        }

        $params = [
            'model' => $this, 
            'btnSize' => $btnSize, 
            'labeled' => $labeled, 
            'isBtn' => true,
        ];        
        $result = [];
        $result[] = Bill::getButtonPayment($params);
        $result[] = Bill::getButtonWriteOnBasis($params);
        $result[] = Bill::getButtonCreditInvoice($params);
        $result[] = Bill::getButtonMutualSettlement($params);
        $result[] = Bill::getButtonCession($params);
        $result[] = Bill::getButtonDebtRelief($params);
        $result[] = Bill::getButtonCancel($params);

        foreach ($result as $key => $btn) {
            if (empty($btn)) {
                unset($result[$key]);
            }
        }

        $result = implode('&nbsp;', $result);
        return $result;
    }

    public function getOptionsActionButtons($btnSize = '', $labeled = false)
    {
        if (!empty($btnSize)) {
            $btnSize = 'btn-' . $btnSize;
        }

        $result = [];

        $result['pay'] = function (array $params) use ($btnSize, $labeled) {
            $params = ArrayHelper::merge($params, [
                'btnSize' => $btnSize,
                'labeled' => $labeled,
            ]);
            return Bill::getButtonPayment($params);
        };
        $result['write-on-basis'] = function (array $params) use ($btnSize, $labeled) {
            $params = ArrayHelper::merge($params, [
                'btnSize' => $btnSize,
                'labeled' => $labeled,
            ]);
            return Bill::getButtonWriteOnBasis($params);
        };
        $result['credit-invoice'] = function (array $params) use ($btnSize, $labeled) {
            $params = ArrayHelper::merge($params, [
                'btnSize' => $btnSize,
                'labeled' => $labeled,
            ]);
            return Bill::getButtonCreditInvoice($params);
        };
        $result['mutual-settlement'] = function (array $params) use ($btnSize, $labeled) {
            $params = ArrayHelper::merge($params, [
                'btnSize' => $btnSize,
                'labeled' => $labeled,
            ]);
            return Bill::getButtonMutualSettlement($params);
        };
        $result['cession'] = function (array $params) use ($btnSize, $labeled) {
            $params = ArrayHelper::merge($params, [
                'btnSize' => $btnSize,
                'labeled' => $labeled,
            ]);
            return Bill::getButtonCession($params);
        };
        $result['debt-relief'] = function (array $params) use ($btnSize, $labeled) {
            $params = ArrayHelper::merge($params, [
                'btnSize' => $btnSize,
                'labeled' => $labeled,
            ]);
            return Bill::getButtonDebtRelief($params);
        };
        $result['cancel'] = function (array $params) use ($btnSize, $labeled) {
            $params = ArrayHelper::merge($params, [
                'btnSize' => $btnSize,
                'labeled' => $labeled,
            ]);
            return Bill::getButtonCancel($params);
        };
        return $result;
    }

    public function getPrintDocNumber(){
        $docNumber = preg_replace('/[^a-zA-Z0-9 -]/', '-', $this->doc_number);
        return $docNumber;
    }
    
    public function checkDelayed()
    {
        $totalUpdated = 0;
        $searchModel = new search\BillSearch();
        
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            $billList = $searchModel->searchDelayed();
            foreach ($billList as $bill) {
                $bill->updateAttributes(['delayed' => true]);
                $totalUpdated++;
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $transaction->rollBack();
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
        } finally {
            return $totalUpdated;
        }         
    }

    public function sendMailClient($wasChanged = null)
    {
        $firstClient = $this->agreement->firstClient;
        $clientMailTemplate = $firstClient->clientMailTemplate;
        $fromContact = $firstClient->invoiceClientContact;
        if(!$fromContact){
            return;
        }
        
        $secondClient = $this->agreement->secondClient;
        $toContactList = $secondClient->invoiceClientAllContact;
        foreach ($toContactList as $key => $toContact) {
            if(empty($toContact->email)){
                unset($toContactList[$key]);
            }
        }
        if(!$toContactList){
            return;
        }
        
        $filename = "invoice-{$this->printDocNumber}-{$this->doc_key}.pdf";
        $storagePath = Yii::getAlias(PrintModule::INVOICES_DIR.'/'.$firstClient->id);
        $filepath = "$storagePath/$filename";
        if (!file_exists($filepath)) {
            return;
        } 
        
        $attachmentArr = [
            'invoice-'.$this->doc_number.'.pdf' => $filepath,
        ];
        $abonentList = $this->abonents;
        foreach ($abonentList as $abonent) {
            if(!empty($abonent->send_all_attachment)){
                $attachmentList = $this->attachments;
                $key = 0;
                foreach ($attachmentList as $attachment) {
                    $file = $attachment->uploadedFile;
                    $filepath = $file->filepath;
                    if (file_exists($filepath)) {
                        $key++;
                        $attachmentArr['attachment-'.$key.'.pdf'] = $file->filepath;
                    }                 
                }
                break;
            }
        }

        $logoImg = null;
        if(!empty(Yii::$app->params['EMAILS_PRINT_LOGO'])){
            $logoModel = isset($clientMailTemplate) ? $clientMailTemplate->logo : null;
            if(empty($logoModel)){
                $logoModel = $firstClient->logo;
            }
            if(!empty($logoModel)){
                $logoPath = $logoModel->uploadedFilePath;
                if (!empty($logoPath) && file_exists($logoPath)) {
                    $logoImg = $logoPath;
                }  
            }                
        }
        
        if(empty($wasChanged)){
            $layout = 'layouts/bill-send-email-client';
        }else{
            $layout = 'layouts/bill-changed-send-email-client';
        }
        $layout .= (empty(Yii::$app->params['EMAILS_WITH_NAME']) ? '' : '-with-name');

        $language = empty($secondClient->language_id) ? Yii::$app->params['billDefaultPrintLanguage'] : $secondClient->language->language;
        $param = [
            'fromContact' => $fromContact,
            'toContact' => $toContactList,
            'firstClient' => $firstClient,
            'attachmentArr' => $attachmentArr,
            'logoImg' => $logoImg,
            'layout' => $layout,
            'language' => $language,
            'clientMailTemplate' => $clientMailTemplate,
        ];
        $result = $this->sendMail($param);
        if($result){
            $this->updateAttributes(['mail_status' => BILL::BILL_MAIL_STATUS_SEND]);
        }
        return $result;
    }

    public function sendMailAgent($wasChanged = null)
    {
        $result = false;
        
        $firstClient = $this->agreement->firstClient;
        $filename = "invoice-{$this->printDocNumber}-{$this->doc_key}.pdf";
        $storagePath = Yii::getAlias(PrintModule::INVOICES_DIR.'/'.$firstClient->id);
        $filepath = "$storagePath/$filename";
        if (!file_exists($filepath)) {
            return;
        } 
        $attachmentArr = [
            'invoice-'.$this->doc_number.'.pdf' => $filepath,
        ];        
        
        $clientAgent = $firstClient->agent;
        if($clientAgent){
            $user = $clientAgent->user;
            $toContact = new \stdClass();
            $toContact->email = $user->email;
        
            $language = empty($firstClient->language_id) ? Yii::$app->params['billDefaultPrintLanguage'] : $firstClient->language->language;
            $param = [
                'toContact' => $toContact,
                'firstClient' => $firstClient,
                'attachmentArr' => $attachmentArr,
                'logoImg' => null,
                'layout' => (empty($wasChanged) ? 'layouts/bill-send-email-agent' : 'layouts/bill-changed-send-email-agent'),
                'language' => $language,
            ];
            $result = $this->sendMail($param);
        }

        $secondClient = $this->agreement->secondClient;
        $clientAgent = $secondClient->agent;
        if($clientAgent){
            $user = $clientAgent->user;
            $toContact = new \stdClass();
            $toContact->email = $user->email;
        
            $language = empty($secondClient->language_id) ? Yii::$app->params['billDefaultPrintLanguage'] : $secondClient->language->language;
            $param = [
                'toContact' => $toContact,
                'firstClient' => $secondClient,
                'attachmentArr' => $attachmentArr,
                'filepath' => $filepath,
                'logoImg' => null,
                'layout' => (empty($wasChanged) ? 'layouts/bill-send-email-agent' : 'layouts/bill-changed-send-email-agent'),
                'language' => $language,
            ];
            $result = $this->sendMail($param);
        }
        
        if($result){
            $this->updateAttributes(['mail_status' => BILL::BILL_MAIL_STATUS_SEND]);
        }
        return $result;
    }
    
    private function sendMail($params)
    {
        extract($params); //$fromContact, $toContact, $firstClient, $attachmentArr, $logoImg, $layout, $language
        
        if(is_array($toContact)){
            $toContactList = $toContact;
            $toContact = $toContact[0];
        }
        
        if(isset($clientMailTemplate) && !empty($clientMailTemplate->contacts)){
            $contacts = $clientMailTemplate->contacts;
        }elseif(isset($fromContact) && (!empty($fromContact->email) || !empty($fromContact->phone))){
            $contacts = "
                <div>
                    <strong>".$firstClient->name."</strong> <br/>
                    <table style=\"font-family: 'Tahoma'; font-size: 12px; color: #434343\">
                        ".(!empty($fromContact->phone) ?
                            "<tr>
                                <td>Phone:</td>
                                <td>".$fromContact->phone."</td>
                            </tr>" : ''
                        ).
                        (!empty($fromContact->email) ?
                            "<tr>
                                <td>E-mail:</td>
                                <td><a href='mailto:".$fromContact->email."' style='font-weight: bold; color: #434343'>".$fromContact->email."</a></td>
                            </tr>" : ''
                        ).
                    "</table>
                </div>
                ";
        }else{
            $contacts = Yii::$app->params['EMAILS_DEFAULT_CONTACT'];
        }
        
        $user = Yii::$app->user;
        $token = new FSMToken();
        $token->user_id = $user->id;
        $token->type = Bill::TOKEN_TYPE_NEW_BILL;
        if (!$token->save(false)) {
            throw new Exception('Can not save the Token data!');
        }                
        
        $mailParams = [
            'bill' => $this,
            'toContact' => $toContact,
            'content' => [
                'client_name' => $firstClient->name,
                'doc_number' => $this->doc_number,
            ],
            'contacts' => $contacts,
            'clientMailTemplate' => isset($clientMailTemplate) ? $clientMailTemplate : null,
            'logoImageFileName' => $logoImg,
            'viewPDFUrl' => Url::to(['/bill/view-mail-pdf', 'bill_id' => $this->id, 'user_id' => $user->id, 'code' => $token->code], true),
            'receiveMailUrl' => Url::to(['/bill/receive-mail-gif', 'bill_id' => $this->id, 'user_id' => $user->id, 'code' => $token->code], true),
        ];
        
        $oldLng = Yii::$app->language;
        if ($language == 'lv') {
            Yii::$app->language = 'lv-LV';
        }
        try{
            $mail = Yii::$app->mailer->compose($layout, $mailParams)
                ->setFrom(Yii::$app->params['EMAILS_DEFAULT_SENDER'])
                //->setFrom(!empty($fromContact->email) ? $fromContact->email : Yii::$app->params['EMAILS_DEFAULT_SENDER'])
                ->setTo($toContact->email)
                ->setSubject((isset($clientMailTemplate) && !empty($clientMailTemplate->subject) ? 
                    $clientMailTemplate->subject : 
                    $firstClient->name).' | '.Yii::t('bill', 'Invoice #').$this->doc_number);
            foreach ($attachmentArr as $key => $attachment) {
                $mail->attach($attachment, ['fileName' => $key]);
            }
            if((isset($clientMailTemplate) && !empty($clientMailTemplate->cc)) || isset($toContactList)){
                $emailArr = [];
                if(isset($clientMailTemplate) && !empty($clientMailTemplate->cc)){
                    $ccArr = explode(';', $clientMailTemplate->cc);
                    foreach ($ccArr as $cc) {
                        $emailArr[] = preg_replace('/\s+/', '', $cc);
                    }
                }
                if(isset($toContactList)){
                    unset($toContactList[0]);
                    if(count($toContactList) > 0){
                        foreach ($toContactList as $contact) {
                            $emailArr[] = $contact->email;
                        }
                    }
                }
                if(!empty($emailArr)){
                    $mail->setCc($emailArr);
                }
            }
            $result = Yii::$app->mailer->send($mail);
        } catch (Exception $ex) {
            $message = $e->getMessage();
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
        } finally {
            Yii::$app->language = $oldLng;
        }
        return $result;
    }

}
