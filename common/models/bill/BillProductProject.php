<?php

namespace common\models\bill;

use Yii;

use common\models\abonent\Abonent;
use common\models\bill\Bill;
use common\models\bill\BillProduct;
use common\models\client\Project;
use common\models\user\FSMUser;

/**
 * This is the model class for table "bill_project".
 *
 * @property integer $id
 * @property integer $abonent_id
 * @property integer $bill_id
 * @property integer $bill_product_id
 * @property integer $project_id
 * @property string $created_at
 * @property integer $create_user_id
 * @property string $updated_at
 * @property integer $updat_user_id
 *
 * @property Abonent $abonent
 * @property Bill $bill
 * @property BillProduct $billProduct
 * @property Project $project
 * @property FSMUser $createUser
 * @property FSMUser $updateUser
 */
class BillProductProject extends \common\models\mainclass\FSMCreateUpdateModel
{
    public $needValidateProject = false;
    public $needValidateObject = false;
    public $product_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bill_project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['abonent_id', 'bill_id', 'bill_product_id'], 'required'],
            ['project_id', 'required', 'when' => function($model) {
                return !empty($model->needValidateProject);
            }],
            [['abonent_id', 'bill_id', 'bill_product_id', 'project_id', 'create_user_id', 'update_user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['abonent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Abonent::className(), 'targetAttribute' => ['abonent_id' => 'id']],
            [['bill_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bill::className(), 'targetAttribute' => ['bill_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::class, 'targetAttribute' => ['project_id' => 'id']],
            [['bill_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => BillProduct::className(), 'targetAttribute' => ['bill_product_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['create_user_id' => 'id']],
            [['update_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['update_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('bill', 'Invoice project|Invoice projects', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'abonent_id' => Yii::t('bill', 'Abonent'),
            'bill_id' => Yii::t('bill', 'Invoice'),
            'bill_product_id' => Yii::t('bill', 'Invoice product ID'),
            'project_id' => Yii::t('bill', 'Project'),
            'create_time' => Yii::t('common', 'Created at'), 
            'create_user_id' => Yii::t('common', 'Author'), 
            'update_time' => Yii::t('common', 'Updated at'), 
            'update_user_id' => Yii::t('common', 'Editor'), 
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonent()
    {
        return $this->hasOne(Abonent::className(), ['id' => 'abonent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBill()
    {
        return $this->hasOne(Bill::className(), ['id' => 'bill_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillProduct()
    {
        return $this->hasOne(BillProduct::className(), ['id' => 'bill_product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'update_user_id']);
    } 
    
    public function beforeSave($insert) 
    {
        $this->abonent_id = $this->abonent_id ?? $this->userAbonentId;
        
        if(!parent::beforeSave($insert)){
            return;
        }
        return true;
    } 
}