<?php

namespace common\models\bill;

use Yii;
use yii\helpers\ArrayHelper;

use common\models\user\FSMUser;
use common\models\abonent\Abonent;
use common\models\client\Client;
use common\models\bill\ConventionAttachment;
use common\models\Valuta;
use common\models\Files;

/**
 * This is the model class for table "convention".
 *
 * @property integer $id
 * @property integer $deleted
 * @property integer $abonent_id
 * @property string $doc_type
 * @property integer $first_client_id
 * @property integer $second_client_id
 * @property integer $third_client_id
 * @property string $doc_number
 * @property string $doc_date
 * @property string $comment
 * @property string $create_time
 * @property integer $create_user_id
 * @property string $update_time
 * @property integer $update_user_id
 *
 * @property Abonent $abonent
 * @property Client $firstClient
 * @property Client $secondClient
 * @property Client $thirdClient
 * @property FSMUser $createUser
 * @property FSMUser $updateUser
 * @property Valuta $valuta
 * @property ConventionAttachment[] $conventionAttachments
 */
class Convention extends \common\models\mainclass\FSMCreateUpdateModel
{
    const CONVENTION_TYPE_SET_OFF = 'set_off';
    const CONVENTION_TYPE_ASSIGNMENT = 'assignment';
    const CONVENTION_TYPE_MEETING = 'meeting';
    const CONVENTION_TYPE_SETTLEMENT = 'settlement';
    const CONVENTION_NON_DISCLOSURE = 'non_disclosure';
    const CONVENTION_TYPE_OTHER = 'other';
    
    protected $_externalFields = [
        'first_client_name',
        'second_client_name',
        'third_client_name',
    ];    

    public static function find($withScope = false) {
        if ((Yii::$app->id != 'app-console') && $withScope) {
            $scope = new \common\scopes\ConventionScopeQuery(get_called_class());
            return $scope->onlyAuthor()->forClient();            
        }
        return parent::find();
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'convention';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['abonent_id', 'first_client_id', 'doc_number', 'doc_date'], 'required'],
            [['deleted', 'abonent_id', 'first_client_id', 'second_client_id', 'third_client_id', 
                'create_user_id', 'update_user_id'], 'integer'],
            [['doc_type', 'comment'], 'string'],
            [['doc_date', 'create_time', 'update_time'], 'safe'],
            [['doc_number'], 'string', 'max' => 40],
            [['first_client_id', 'second_client_id', 'third_client_id'], 'validateUniqueClient', 'params' => ['first_client_id', 'second_client_id', 'third_client_id']],
            [['abonent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Abonent::class, 'targetAttribute' => ['abonent_id' => 'id']],   
            [['first_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['first_client_id' => 'id']],
            [['second_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['second_client_id' => 'id']],
            [['third_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['third_client_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['create_user_id' => 'id']],
            [['update_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['update_user_id' => 'id']],
        ];
    }

    public function validateUniqueClient($attribute, $params, $validator)
    {
        if((!empty($this->first_client_id) && !empty($this->second_client_id) && ($this->first_client_id == $this->second_client_id)) ||
            (!empty($this->first_client_id) && !empty($this->third_client_id) && ($this->first_client_id == $this->third_client_id)) ||
            (!empty($this->second_client_id) && !empty($this->third_client_id) && ($this->second_client_id == $this->third_client_id))){
            $message = Yii::t('client', 'The same clients are selected!');
            $validator->addError($this, $attribute, $message);
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('bill', 'Convention|Conventions', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'deleted' => Yii::t('common', 'Deleted'),
            'abonent_id' => Yii::t('client', 'Abonent'),
            'doc_type' => Yii::t('bill', 'Doc.type'),
            'first_client_id' => Yii::t('agreement', 'First party'),
            'second_client_id' => Yii::t('agreement', 'Second party'),
            'third_client_id' => Yii::t('agreement', 'Third party'),
            'doc_number' => Yii::t('bill', 'Doc.number'),
            'doc_date' => Yii::t('bill', 'Doc.date'),
            'comment' => Yii::t('common', 'Comment'),
            'create_time' => Yii::t('bill', 'Create Time'),
            'create_user_id' => Yii::t('bill', 'Create User'),
            'update_time' => Yii::t('bill', 'Update Time'),
            'update_user_id' => Yii::t('bill', 'Update User'),
            
            'first_client_name' => Yii::t('agreement', 'First party'),
            'second_client_name' => Yii::t('agreement', 'Second party'),
            'third_client_name' => Yii::t('agreement', 'Third party'),
        ];
    }
    
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors = ArrayHelper::merge(
            $behaviors,
            [
                \common\behaviors\DeletedModelBehavior::class,
            ]
        );
        return $behaviors;        
    }

    protected function getIgnoredFieldsForDelete() {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, ['abonent_id', 'first_client_id', 'second_client_id', 'third_client_id']
        );
        return $fields;
    }

    /** @inheritdoc */
    public function beforeValidate() {
        if (!empty($this->first_client_id) && !empty($this->second_client_id) &&
                (
                ($this->first_client_id == $this->second_client_id) ||
                ($this->first_client_id == $this->third_client_id) ||
                ($this->second_client_id == $this->third_client_id)
                )
        ) {
            $this->addError('second_client_id', Yii::t('bill', 'The same clients are selected!'));
            return false;
        } else {
            return parent::beforeValidate();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonent() {
        return $this->hasOne(Abonent::class, ['id' => 'abonent_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstClient() {
        return $this->hasOne(Client::class, ['id' => 'first_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondClient() {
        return $this->hasOne(Client::class, ['id' => 'second_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThirdClient() {
        return $this->hasOne(Client::class, ['id' => 'third_client_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'update_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments() {
        return $this->hasMany(ConventionAttachment::class, ['convention_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConventionAttachments()
    {
        return $this->hasMany(ConventionAttachment::class, ['convention_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachmentFiles() {
        return $this->hasMany(Files::class, ['id' => 'uploaded_file_id'])->via('attachments');
    }   

    static public function getConventionTypeList()
    {
        return \lajax\translatemanager\helpers\Language::a([
            self::CONVENTION_TYPE_SET_OFF => 'Set-Off',
            self::CONVENTION_TYPE_ASSIGNMENT => 'Assignment',
            self::CONVENTION_TYPE_MEETING => 'Meeting Minutes',
            self::CONVENTION_TYPE_SETTLEMENT => 'Settlement',
            self::CONVENTION_NON_DISCLOSURE => 'Non-disclosure',
            self::CONVENTION_TYPE_OTHER => 'Other',
        ]);
    }    
    
    public function beforeSave($insert) 
    {
        $this->doc_date = date('Y-m-d', strtotime($this->doc_date));
        if(!parent::beforeSave($insert)){
            return;
        }
        return true;
    }    

}