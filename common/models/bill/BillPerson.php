<?php

namespace common\models\bill;

use Yii;

use common\models\bill\Bill;
use common\models\client\ClientContact;

/**
 * This is the model class for table "bill_person".
 *
 * @property integer $id
 * @property integer $bill_id
 * @property integer $client_person_id
 * @property integer $person_order
 *
 * @property Bill $bill
 * @property ClientContact $clientPerson
 */
class BillPerson extends \common\models\mainclass\FSMBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bill_person';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bill_id'], 'required'],
            [['id', 'bill_id', 'client_person_id', 'person_order'], 'integer'],
            [['bill_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bill::class, 'targetAttribute' => ['bill_id' => 'id']],
            [['client_person_id'], 'exist', 'skipOnError' => true, 'targetClass' => ClientContact::class, 'targetAttribute' => ['client_person_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('app', 'Bill person|Bill persons', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bill_id' => 'Bill',
            'client_person_id' => 'First party signing person',
            'person_order' => 'Person order',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBill()
    {
        return $this->hasOne(Bill::class, ['id' => 'bill_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientPerson()
    {
        return $this->hasOne(ClientContact::class, ['id' => 'client_person_id']);
    }

}