<?php

namespace common\models\bill;

use Yii;

use common\models\bill\Expense;
use common\models\Files;

/**
 * This is the model class for table "expense_attachment".
 *
 * @property integer $id
 * @property integer $expense_id
 * @property integer $uploaded_file_id
 *
 * @property Expense $expense
 * @property Files $uploadedFile
 */
class ExpenseAttachment extends \common\models\mainclass\FSMBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expense_attachment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['expense_id', 'uploaded_file_id'], 'required'],
            [['expense_id', 'uploaded_file_id'], 'integer'],
            [['expense_id'], 'exist', 'skipOnError' => true, 'targetClass' => Expense::class, 'targetAttribute' => ['expense_id' => 'id']],
            [['uploaded_file_id'], 'exist', 'skipOnError' => true, 'targetClass' => Files::class, 'targetAttribute' => ['uploaded_file_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('client', 'Expense attachment|Expense attachments', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'expense_id' => Yii::t('bill', 'Expense'),
            'uploaded_file_id' => Yii::t('common', 'Attachment'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpense()
    {
        return $this->hasOne(Expense::class, ['id' => 'expense_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadedFile()
    {
        return $this->hasOne(Files::class, ['id' => 'uploaded_file_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function beforeDelete()
    {
        $file = $this->uploadedFile;
        if($file->delete()){
            return parent::beforeDelete();
        }
        return false;
    }    
}