<?php

namespace common\models\bill;

use Yii;
use yii\helpers\ArrayHelper;
use yii\base\Exception;

use kartik\helpers\Html;

use common\components\FSMAccessHelper;
use common\models\abonent\Abonent;
use common\models\abonent\AbonentBillTemplate;
use common\models\abonent\AbonentBill;
use common\models\abonent\AbonentClient;
use common\models\bill\BillProductProject;
use common\models\client\Client;
use common\models\client\Project;
use common\models\client\Agreement;
use common\models\client\ClientBank;
use common\models\client\ClientRole;
use common\models\user\FSMUser;
use common\models\Valuta;
use common\components\FSMHelper;
use common\models\Language;

/**
 * This is the model class for table "bill_template".
 *
 * @property integer $id
 * @property integer $agreement_id
 * @property integer $first_client_bank_id
 * @property integer $second_client_bank_id
 * @property integer $according_contract
 * @property string $summa
 * @property string $vat
 * @property string $total
 * @property integer $valuta_id
 * @property string $rate
 * @property string $summa_eur
 * @property string $vat_eur
 * @property string $total_eur
 * @property integer $language_id
 * @property integer $e_signing
 * @property string $justification
 * @property string $place_service
 * @property string $comment_special
 * @property string $comment
 * @property integer $periodical_day
 * @property string $periodicity
 * @property string $periodical_last_date
 * @property string $periodical_next_date
 * @property string $periodical_finish_date
 * @property string $create_time
 * @property integer $create_user_id
 * @property string $update_time
 * @property integer $update_user_id
 *
 * @property AbonentBillTemplate[] $abonentBillTemplates
 * @property Bill[] $bills
 * @property Agreement $agreement
 * @property User $createUser
 * @property ClientBank $firstClientBank
 * @property ClientBank $secondClientBank
 * @property FirstBillTemplatePerson $firstClientPerson
 * @property SecondBillTemplatePerson $secondClientPerson
 * @property User $updateUser
 * @property Valuta $valuta
 * @property BillTemplateProduct[] $billTemplateProducts
 */
class BillTemplate extends \common\models\mainclass\FSMCreateUpdateModel
{
    const BILL_PERIODICITY_MONTH = 'month';
    const BILL_PERIODICITY_QUARTER = 'quarter';
    const BILL_PERIODICITY_YEAR = 'year';
    
    protected $_externalFields = [
        'project_id',
        'project_name',
        'agreement_number',
        'first_client_id',
        'first_client_name',
        'first_client_role_name',
        'second_client_id',
        'second_client_name',
        'second_client_role_name',
        'third_client_id',
        'third_client_name',
        'third_client_role_name',
        'client_id',
        'client_name',
    ];

    public function init()
    {
        parent::init();
        $this->cascadeDeleting = true;
    }
    
    public static function find($withScope = false)
    {
        if ((Yii::$app->id != 'app-console') && $withScope) {
            $scope = new \common\scopes\BillTemplatelScopeQuery(get_called_class());
            return $scope->onlyAuthor()->forClient();
        }
        return parent::find();
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bill_template';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agreement_id', 'first_client_bank_id', 'summa', 'vat', 'total', 'valuta_id'], 'required'],
            [['agreement_id', 'first_client_bank_id', 'second_client_bank_id', 'according_contract', 
                'valuta_id', 'language_id', 'periodical_day', 'e_signing', 'create_user_id', 'update_user_id'], 'integer'],
            [['summa', 'vat', 'total', 'rate', 'summa_eur', 'vat_eur', 'total_eur'], 'number'],
            [['comment_special', 'comment', 'periodicity'], 'string'],
            [['periodical_last_date', 'periodical_next_date', 'periodical_finish_date', 
                'create_time', 'update_time'], 'safe'],
            [['justification'], 'string', 'max' => 100],
            [['place_service'], 'string', 'max' => 255],
            [['periodical_day', 'periodicity'], 'validatePeriodical'],
            [['total'], 'validateRate'],
            [['agreement_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agreement::class, 'targetAttribute' => ['agreement_id' => 'id']],
            [['first_client_bank_id'], 'exist', 'skipOnError' => true, 'targetClass' => ClientBank::class, 'targetAttribute' => ['first_client_bank_id' => 'id']],
            [['second_client_bank_id'], 'exist', 'skipOnError' => true, 'targetClass' => ClientBank::class, 'targetAttribute' => ['second_client_bank_id' => 'id']],
            [['valuta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Valuta::class, 'targetAttribute' => ['valuta_id' => 'id']],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::class, 'targetAttribute' => ['language_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['create_user_id' => 'id']],
            [['update_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['update_user_id' => 'id']],
        ];
    }

    public function validatePeriodical($attribute, $params, $validator)
    {
        if (empty($this->periodical_day) || empty($this->periodicity)) {
            $message = Yii::t('yii', '{attribute} cannot be blank.');
        }
        if (empty($this->periodical_day)) {
            $attribute = 'periodical_day';
        }elseif (empty($this->periodicity)) {
            $attribute = 'periodicity';
        }

        if(isset($message)){
            $validator->addError($this, $attribute, $message);
        }
    }

    public function validateRate($attribute, $params, $validator)
    {
        $rate = (float)$this->rate;
        $valuta_id = (int)$this->valuta_id;
        if (
                (($rate == 1) && ($valuta_id != Valuta::VALUTA_DEFAULT)) ||
                (($rate != 1) && ($valuta_id == Valuta::VALUTA_DEFAULT))
            ) {
            $message = Yii::t('bill', 'The currency rate is not correct.');
            $validator->addError($this, 'total', $message);
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('bill', 'Invoice template|Invoice templates', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'agreement_id' => Yii::t('bill', 'Agreement'),
            'first_client_bank_id' => Yii::t('bill', 'First party bank account'),
            'second_client_bank_id' => Yii::t('bill', 'Second party bank account'),
            'according_contract' => Yii::t('bill', 'According contract'),
            'summa' => Yii::t('common', 'Sum'),
            'vat' => Yii::t('common', 'VAT'),
            'total' => Yii::t('common', 'Total'),
            'valuta_id' => Yii::t('common', 'Currenсy'),
            'rate' => Yii::t('common', 'Rate'),
            'summa_eur' => Yii::t('common', 'Sum').' €',
            'vat_eur' => Yii::t('common', 'VAT').' €',
            'total_eur' => Yii::t('common', 'Total').' €',
            'language_id' => Yii::t('common', 'Print language'),
            'e_signing' => Yii::t('bill', 'Without signing'),
            'justification' => Yii::t('bill', 'Justification'),
            'place_service' => Yii::t('bill', 'Place of service'),
            'comment_special' => Yii::t('bill', 'Special notes'),
            'comment' => Yii::t('common', 'Comment'),
            'periodical_day' => Yii::t('bill', 'Generation day'),
            'periodicity' => Yii::t('bill', 'Periodicity'),
            'periodical_last_date' => Yii::t('bill', 'Last invoice generation date'),
            'periodical_next_date' => Yii::t('bill', 'Next invoice generation date'),
            'periodical_finish_date' => Yii::t('bill', 'Invoice generation finish date'),
            'create_time' => Yii::t('common', 'Create Time'),
            'create_user_id' => Yii::t('common', 'Create User'),
            'update_time' => Yii::t('common', 'Update Time'),
            'update_user_id' => Yii::t('common', 'Update User'),
            
            'project_id' => Yii::t('bill', 'Project'),
            'project_name' => Yii::t('bill', 'Project'),
            'agreement_number' => Yii::t('bill', 'Agreement'),
            'first_client_id' => Yii::t('bill', 'First party'),
            'first_client_name' => Yii::t('bill', 'First party'),
            'second_client_id' => Yii::t('bill', 'Second party'),
            'second_client_name' => Yii::t('bill', 'Second party'),
            'third_client_id' => Yii::t('bill', 'Third party'),
            'third_client_name' => Yii::t('bill', 'Third party'),
        ];
    }

    protected function getIgnoredFieldsForDelete()
    {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, [
                'agreement_id', 
                'first_client_bank_id', 
                'second_client_bank_id', 
                'valuta_id', 
                'bill_template_id', 
                'language_id',
            ]
        );
        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonents()
    {
        return $this->hasMany(Abonent::class, ['id' => 'abonent_id'])->viaTable('abonent_bill_template', ['bill_template_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonentBillTemplates()
    {
        return $this->hasMany(AbonentBillTemplate::class, ['bill_template_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBills()
    {
        return $this->hasMany(Bill::class, ['bill_template_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreement()
    {
        return $this->hasOne(Agreement::class, ['id' => 'agreement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstClientBank()
    {
        return $this->hasOne(ClientBank::class, ['id' => 'first_client_bank_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondClientBank()
    {
        return $this->hasOne(ClientBank::class, ['id' => 'second_client_bank_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'update_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValuta()
    {
        return $this->hasOne(Valuta::class, ['id' => 'valuta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::class, ['id' => 'language_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillTemplateProducts()
    {
        return $this->hasMany(BillTemplateProduct::class, ['bill_template_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        $abonentId = $this->userAbonentId;
        return $this->hasOne(Project::class, ['id' => 'project_id'])
            ->viaTable('abonent_bill_template', ['bill_template_id' => 'id'], function ($query) use ($abonentId) {
                $query->andWhere(['abonent_id' => $abonentId]);
        });        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstClient()
    {
        return $this->hasOne(Client::class, ['id' => 'first_client_id'])->viaTable('agreement', ['id' => 'agreement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondClient()
    {
        return $this->hasOne(Client::class, ['id' => 'second_client_id'])->viaTable('agreement', ['id' => 'agreement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillProducts()
    {
        return $this->hasMany(BillTemplateProduct::class, ['bill_template_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstClientRole()
    {
        return $this->hasOne(ClientRole::class, ['id' => 'first_client_role_id'])->viaTable('agreement', ['id' => 'agreement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondClientRole()
    {
        return $this->hasOne(ClientRole::class, ['id' => 'second_client_role_id'])->viaTable('agreement', ['id' => 'agreement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillTemplatePeople()
    {
        $result = $this->hasMany(FirstBillPerson::class, ['bill_id' => 'id']);
        return $result;
    }    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillPersons()
    {
        $result = $this->hasMany(FirstBillTemplatePerson::class, ['bill_template_id' => 'id']);
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstClientPerson()
    {
        $result = $this->hasMany(FirstBillTemplatePerson::class, ['bill_template_id' => 'id']);
        $result->andWhere(['person_order' => 1]);
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondClientPerson()
    {
        $result = $this->hasMany(SecondBillTemplatePerson::class, ['bill_template_id' => 'id']);
        $result->andWhere(['person_order' => 2]);
        return $result;
    }
    
    static public function getBillPeriodicityList()
    {
        return [
            BillTemplate::BILL_PERIODICITY_MONTH => Yii::t('common', '1 time per month'),
            BillTemplate::BILL_PERIODICITY_QUARTER => Yii::t('common', '1 time per quarter'),
            BillTemplate::BILL_PERIODICITY_YEAR => Yii::t('common', '1 time per year'),
        ];
    }    

    static function getButtonCreateBill(array $params, $btnSize = '', $labeled = false)
    {
        extract($params); //$url, $model, $key, $isBtn = true
        
        if(!FSMAccessHelper::can('createBill')){
            return '';
        }
        
        $url = ['bill/create', 'bill_template_id' => $model->id];
        if (!empty($isBtn)) {
            return FSMHelper::aButton($model->id, [
                'idName' => 'bill_template_id',
                'label' => ($labeled ? Yii::t('bill', 'Create invoice') : null),
                'title' => Yii::t('bill', 'Create invoice'),
                'controller' => 'bill',
                'action' => 'create',
                'class' => 'success',
                'size' => $btnSize,
                'icon' => 'duplicate',
            ]);
            /*
            return Html::a(Html::icon('duplicate'), $url, [
                'class' => 'btn btn-xs btn-success',
                'title' => Yii::t('bill', 'Create invoice'),
            ]);
             * 
             */
        } else {
            $label = Html::icon('duplicate') . '&nbsp;' . Yii::t('common', 'Create invoice');
            return '<li>' . Html::a($label, $url) . '</li>' . PHP_EOL;
        }
    }
    
    public function beforeSave($insert) 
    {
        $this->rate = ($this->valuta_id == Valuta::VALUTA_DEFAULT) ? 1 : $this->rate;
        $this->summa_eur = $this->summa / $this->rate;
        $this->vat_eur = $this->vat / $this->rate;
        $this->total_eur = $this->total / $this->rate;
        
        if(!empty($this->periodical_finish_date) && ($this->periodical_finish_date == date('Y-m-d'))){
            $this->periodical_last_date = null;
            $this->periodical_next_date = null;
            $this->periodical_finish_date = null;
        }else{
            if(empty($this->periodical_last_date)){
                $lastDate = date('Y-m-'.str_pad($this->periodical_day, 2, "0", STR_PAD_LEFT));
                if($lastDate > date('Y-m-d')){
                    $strPeriodicity = '+0 month';
                }else{
                    $strPeriodicity = '+1 month';
                }
            }else{
                $lastDate = $this->periodical_last_date;
                $strPeriodicity = ($this->periodicity == BillTemplate::BILL_PERIODICITY_QUARTER) ? '+3 month' : '+1 '.$this->periodicity;
            }
            $nextDate = new \DateTime($lastDate);
            $nextDate->modify($strPeriodicity);
            $nextDate = $nextDate->format('Y-m-'.$this->periodical_day);
            $this->periodical_next_date = $nextDate;
        }
            
        $this->periodical_last_date = !empty($this->periodical_last_date) ? date('Y-m-d', strtotime($this->periodical_last_date)) : null;
        $this->periodical_next_date = !empty($this->periodical_next_date) ? date('Y-m-d', strtotime($this->periodical_next_date)) : null;
        $this->periodical_finish_date = !empty($this->periodical_finish_date) ? date('Y-m-d', strtotime($this->periodical_finish_date)) : null;
        
        if(!parent::beforeSave($insert)){
            return;
        }
        return true;
    }
    
    public function generateBill($execDate = null)
    {
        $fromCron = (Yii::$app->id == 'app-console');
        
        $searchModel = new search\BillTemplateSearch();
        $billTemplateList = $searchModel->searchPeriodical($execDate);
        if(count($billTemplateList) == 0){
            return 0;
        }
        
        $totalGenerated = 0;
        foreach ($billTemplateList as $billTemplate) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $periodicalBillId = $billTemplate->id;

                $firstTime = true;
                $abonentList = $billTemplate->abonents;
                foreach ($abonentList as $abonent) {
                    $abonentBillTemplateModel = AbonentBillTemplate::findOne([
                        'abonent_id' => $abonent->id, 
                        'bill_template_id' => $periodicalBillId,
                    ]);
                    
                    if($firstTime){
                        $reservedId = Yii::$app->security->generateRandomString();
                        $client = $billTemplate->firstClient;
                        $abonentData = AbonentClient::find()
                            ->andWhere(['abonent_id' => $abonent->id])
                            ->andWhere(['client_id' => $client->id])
                            ->one();
                        $client->gen_number = $abonentData->gen_number;
                        if(!empty($client->gen_number)){
                            $lastNumber = $client->getLastNumber($reservedId);
                            $lastNumber = $client->gen_number.$lastNumber;
                        }else{
                            $lastNumber = $client->id.'/'.date('Y-m-d').'-'.$periodicalBillId;
                        }
                        
                        $agreement = $billTemplate->agreement;
                                
                        $bill = new Bill();
                        $bill->status = Bill::BILL_STATUS_PREPAR;
                        $bill->pay_status = Bill::BILL_PAY_STATUS_NOT;
                        $bill->doc_number = $lastNumber;
                        $bill->doc_date = date('Y-m-d');
                        $bill->pay_date = date('Y-m-d', strtotime("+".(!empty($agreement->deferment_payment) ? $agreement->deferment_payment : Bill::BILL_DEFAULT_PAYMENT_DAYS)." days", time()));
                        $bill->bill_template_id = $periodicalBillId;
                        $bill->agreement_id = $billTemplate->agreement_id;
                        $bill->first_client_bank_id = $billTemplate->first_client_bank_id;
                        $bill->second_client_bank_id = $billTemplate->second_client_bank_id;
                        $bill->according_contract = $billTemplate->according_contract;
                        $bill->justification = $billTemplate->justification;
                        $bill->place_service = $billTemplate->place_service;
                        $bill->comment_special = $billTemplate->comment_special;
                        $bill->comment = $billTemplate->comment;
                        $bill->e_signing = $billTemplate->e_signing;
                        $bill->summa = $billTemplate->summa;
                        $bill->vat = $billTemplate->vat;
                        $bill->total = $billTemplate->total;
                        $bill->valuta_id = $billTemplate->valuta_id;
                        $bill->language_id = $billTemplate->language_id;
                        $bill->summa_eur = $billTemplate->summa_eur;
                        $bill->vat_eur = $billTemplate->vat_eur;
                        $bill->total_eur = $billTemplate->total_eur;
                        $bill->services_period_from = date('Y-m-01');
                        $bill->services_period_till = date('Y-m-t');
            
                        if($rateModel = \common\models\ValutaRate::getByDate($bill->valuta->name, date('Y-m-d'))){
                            $bill->rate = number_format($rateModel->rate, 4, '.', '');
                        }else{
                            $bill->rate = $billTemplate->rate;
                        }
                        
                        if(!$bill->save(false)){
                            $message = Yii::t('common', 'Unable to save data!').' '.$bill->errorMessage;
                            if($fromCron){
                                Yii::error($message, __METHOD__);
                            }else{
                                throw new Exception($message);
                            }
                        }
                        $bill->updateAttributes(['create_user_id' => $billTemplate->create_user_id]);

                        foreach ($billTemplate->billProducts as $index => $product) {
                            $billProduct = new BillProduct();
                            $billProduct->bill_id = $bill->id;
                            $billProduct->product_id = $product->product_id;
                            $billProduct->product_name = $product->product_name;
                            $billProduct->measure_id = $product->measure_id;
                            $billProduct->amount = $product->amount;
                            $billProduct->price = $product->price;
                            $billProduct->vat = $product->vat;
                            $billProduct->revers = $product->revers;
                            $billProduct->summa = $product->summa;
                            $billProduct->summa_vat = $product->summa_vat;
                            $billProduct->total = $product->total;
                            $billProduct->price_eur = $product->price_eur;
                            $billProduct->summa_eur = $product->summa_eur;
                            $billProduct->summa_vat_eur = $product->summa_vat_eur;
                            $billProduct->total_eur = $product->total_eur;
                            $billProduct->comment = $product->comment;
                            if(!$billProduct->save(false)){
                                $message = Yii::t('common', 'Unable to save data!').' '.$billProduct->errorMessage;
                                if($fromCron){
                                    Yii::error($message, __METHOD__);
                                }else{
                                    throw new Exception($message);
                                }
                            }
                            
                            if($abonentBillTemplateModel){
                                $billProductProjectModel = new BillProductProject();
                                $billProductProjectModel->abonent_id = $abonent->id;
                                $billProductProjectModel->bill_id = $bill->id;
                                $billProductProjectModel->bill_product_id = $billProduct->id;
                                $billProductProjectModel->project_id = $abonentBillTemplateModel->project_id;
                                if (!$billProductProjectModel->save(false)) {
                                    $message = Yii::t('common', 'Unable to save data!').' '.$billProductProjectModel->errorMessage;
                                    if($fromCron){
                                        Yii::error($message, __METHOD__);
                                    }else{
                                        throw new Exception($message);
                                    }
                                }
                            }
                        }
                        
                        if(empty($billTemplate->e_signing)){
                            foreach ($billTemplate->billPersons as $index => $person) {
                                $billPerson = new BillPerson();
                                $billPerson->bill_id = $bill->id;
                                $billPerson->client_person_id = $person->client_person_id;
                                $billPerson->person_order = $person->person_order;
                                if(!$billPerson->save(false)){
                                    $message = Yii::t('common', 'Unable to save data!').' '.$billPerson->errorMessage;
                                    if($fromCron){
                                        Yii::error($message, __METHOD__);
                                    }else{
                                        throw new Exception($message);
                                    }
                                }
                            }
                        }
                        
                        $firstTime = false;
                    }

                    $abonentModel = new AbonentBill();
                    $abonentModel->abonent_id = $abonent->id;
                    $abonentModel->project_id = $abonentBillTemplateModel->project_id ?? null;
                    $abonentModel->bill_id = $bill->id;
                    if(!$abonentModel->save()){
                        $message = Yii::t('common', 'Unable to save data!').' '.$abonentModel->errorMessage;
                        if($fromCron){
                            Yii::error($message, __METHOD__);
                        }else{
                            throw new Exception($message);
                        }
                    }                    
                }
                
                $billTemplate->periodical_last_date = date('Y-m-d');
                $billTemplate->save(false);
                
                $totalGenerated++;
            
                $transaction->commit();
            } catch (Exception $e) {
                $transaction->rollBack();
                $message = $e->getMessage();
                Yii::error($message, __METHOD__);
                if($fromCron) {
                    echo PHP_EOL.date('Y-m-d H:i:s').': "generateBill" | '.$message;
                }else{
                    Yii::$app->getSession()->setFlash('error', $message);
                }
            }
        }
        
        return $totalGenerated;
    }
    
}