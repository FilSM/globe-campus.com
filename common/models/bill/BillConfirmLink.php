<?php

namespace common\models\bill;

use Yii;

use common\models\mainclass\FSMBaseModel;
use common\models\bill\PaymentConfirm;
use common\models\bill\BillConfirm;
use common\models\bill\Bill;
use common\models\bill\BillPayment;
use common\models\bill\TaxPayment;
use common\models\bill\BillHistory;
use common\models\client\Agreement;
use common\models\client\AgreementPayment;
use common\models\client\AgreementHistory;
use common\models\bill\Expense;

/**
 * This is the model class for table "bill_confirm_link".
 *
 * @property integer $id
 * @property integer $bill_confirm_id
 * @property string $doc_type
 * @property string $doc_subtype
 * @property integer $history_id
 * @property integer $payment_id
 * @property integer $doc_id
 * @property string $summa
 * @property string $vat
 *
 * @property BillConfirm $billConfirm
 * @property Bill $bill
 * @property BillPayment $billPayment
 * @property BillHistory $billHistory
 * @property Agreement $agreement
 * @property AgreementPayment $agreementPayment
 * @property AgreementHistory $agreementHistory
 * @property Expense $expense
 */
class BillConfirmLink extends \common\models\mainclass\FSMBaseModel
{
    const DOC_TYPE_BILL = 'bill';
    const DOC_TYPE_AGREEMENT = 'agreement';
    const DOC_TYPE_EXPENSE = 'expense';
    const DOC_TYPE_DIRECT = 'direct';
    const DOC_TYPE_TAX = 'tax';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bill_confirm_link';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bill_confirm_id'], 'required'],
            [['bill_confirm_id', 'history_id', 'payment_id', 'doc_id', 'doc_subtype'], 'integer'],
            [['doc_type'], 'string'],
            [['summa', 'vat'], 'number'],
            [['bill_confirm_id'], 'exist', 'skipOnError' => true, 'targetClass' => BillConfirm::class, 'targetAttribute' => ['bill_confirm_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('bill', 'BillConfirmLink|Bill Confirm Links', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'bill_confirm_id' => Yii::t('bill', 'Bill confirm'),
            'doc_type' => Yii::t('bill', 'Payment type'),
            'doc_subtype' => Yii::t('bill', 'Subtype'),
            'history_id' => Yii::t('bill', 'History'),
            'payment_id' => Yii::t('bill', 'Payment'),
            'doc_id' => Yii::t('bill', 'Document'),
            'summa' => Yii::t('common', 'Sum'),
            'vat' => Yii::t('common', 'VAT'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillConfirm()
    {
        return $this->hasOne(BillConfirm::class, ['id' => 'bill_confirm_id']);
    }
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBill()
    {
        return $this->hasOne(Bill::class, ['id' => 'doc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillPayment()
    {
        return $this->hasOne(BillPayment::class, ['id' => 'payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxPayment()
    {
        return $this->hasOne(TaxPayment::class, ['id' => 'doc_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillHistory()
    {
        return $this->hasOne(BillHistory::class, ['id' => 'history_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreement()
    {
        return $this->hasOne(Agreement::class, ['id' => 'doc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementPayment()
    {
        return $this->hasOne(AgreementPayment::class, ['id' => 'payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementHistory()
    {
        return $this->hasOne(AgreementHistory::class, ['id' => 'history_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpense()
    {
        return $this->hasOne(Expense::class, ['id' => 'doc_id']);
    }
    
    static public function getDocTypeList() {
        return [
            BillConfirmLink::DOC_TYPE_BILL => Yii::t('bill', 'Invoice payment'),
            BillConfirmLink::DOC_TYPE_AGREEMENT => Yii::t('agreement', 'Agreement payment'),
            BillConfirmLink::DOC_TYPE_EXPENSE => Yii::t('bill', 'Expense payment'),
            BillConfirmLink::DOC_TYPE_DIRECT => Yii::t('bill', 'Direct expense'),
            BillConfirmLink::DOC_TYPE_TAX => Yii::t('bill', 'Tax payment'),
        ];
    }    

    public function delete(FSMBaseModel $owner = null)
    {
        $result = true;
        switch ($this->doc_type) {
            case BillConfirmLink::DOC_TYPE_BILL:
                $doc = $this->billPayment;
                break;
            case BillConfirmLink::DOC_TYPE_AGREEMENT:
                $doc = $this->agreementPayment;
                break;
            case BillConfirmLink::DOC_TYPE_EXPENSE:
                $doc = $this->expense;
                break;
            case BillConfirmLink::DOC_TYPE_DIRECT:
                $paymentConfirmModel = $this->billConfirm->paymentConfirm;
                $doc = ($paymentConfirmModel->status == PaymentConfirm::IMPORT_STATE_COMPLETE ? $this->agreementPayment : null);
                break;
            case BillConfirmLink::DOC_TYPE_TAX:
                $doc = $this->taxPayment;
                break;
        }
        if(isset($doc) && !$doc->manual_input){
            $result = $doc->delete();
        }

        return $result && parent::delete($owner);
    }     
}