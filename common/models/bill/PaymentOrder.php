<?php

namespace common\models\bill;

use Yii;
use yii\helpers\ArrayHelper;
use kartik\helpers\Html;

use common\models\bill\Bill;
use common\models\bill\BillPayment;
use common\models\user\FSMUser;
use common\models\user\FSMProfile;
use common\models\Files;
use common\models\Bank;
use common\models\abonent\Abonent;
use common\components\FSMHelper;
use common\components\FSMArray2XML;

/**
 * This is the model class for table "payment_order".
 *
 * @property integer $id
 * @property integer $abonent_id
 * @property integer $bank_id
 * @property string $number
 * @property string $pay_date
 * @property string $status
 * @property integer $file_id
 * @property string $comment
 * @property string $action_time
 * @property integer $action_user_id
 * @property string $create_time
 * @property integer $create_user_id
 * @property string $update_time
 * @property integer $update_user_id
 *
 * @property Abonent $abonent
 * @property BillPayment[] $billPayments
 * @property Bank $bank
 * @property Bill[] $bills
 * @property FSMUser $actionUser
 * @property FSMUser $createUser
 * @property Files $file
 * @property FSMUser $updateUser
 * @property FSMProfile $actionUserProfile
 */
class PaymentOrder extends \common\models\mainclass\FSMCreateUpdateModel
{

    const EXPORT_STATE_PREPARE = 'prepare';
    const EXPORT_STATE_SENT = 'sent';

    static $nameField = 'number';
    protected $_externalFields = [
        'file_name',
        'user_name',
        'bank_name',
    ];

    public function init() {
        parent::init();
        $this->cascadeDeleting = true;
    }

    public static function find($withScope = false) {
        if ((Yii::$app->id != 'app-console') && $withScope) {
            $scope = new \common\scopes\PaymentOrderScopeQuery(get_called_class());
            return $scope->onlyAuthor();
        }
        return parent::find();
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'payment_order';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['abonent_id', 'bank_id', 'pay_date', 'number'], 'required'],
            [['status', 'comment'], 'string'],
            [['abonent_id', 'bank_id', 'file_id', 'action_user_id', 'create_user_id', 'update_user_id'], 'integer'],
            [['pay_date', 'action_time', 'create_time', 'update_time'], 'safe'],
            [['number'], 'string', 'max' => 50],
            [['abonent_id', 'number'], 'unique', 'targetAttribute' => ['abonent_id', 'number']],
            [['abonent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Abonent::class, 'targetAttribute' => ['abonent_id' => 'id']],   
            [['bank_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bank::class, 'targetAttribute' => ['bank_id' => 'id']],
            [['file_id'], 'exist', 'skipOnError' => true, 'targetClass' => Files::class, 'targetAttribute' => ['file_id' => 'id']],
            [['action_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['action_user_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['create_user_id' => 'id']],
            [['update_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['update_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('bill', 'Payment order|Payment orders', $n, $translate);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'abonent_id' => Yii::t('client', 'Abonent'),
            'bank_id' => Yii::t('bill', 'Bank'),
            'number' => Yii::t('bill', 'Number'),
            'pay_date' => Yii::t('bill', 'Payment date'),
            'status' => Yii::t('bill', 'State'),
            'file_id' => Yii::t('bill', 'File name'),
            'comment' => Yii::t('common', 'Comment'),
            'action_time' => Yii::t('bill', 'Sent time'),
            'action_user_id' => Yii::t('bill', 'Performer'),
            'create_time' => Yii::t('bill', 'Create Time'),
            'create_user_id' => Yii::t('bill', 'Create User ID'),
            'update_time' => Yii::t('bill', 'Update Time'),
            'update_user_id' => Yii::t('bill', 'Update User ID'),
            'file_name' => Yii::t('bill', 'XML file name'),
            'user_name' => Yii::t('bill', 'Performer'),
            'bank_name' => Yii::t('bill', 'Bank'),
        ];
    }

    protected function getIgnoredFieldsForDelete() {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, ['abonent_id', 'bank_id', 'action_user_id']
        );
        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonent() {
        return $this->hasOne(Abonent::class, ['id' => 'abonent_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBank() {
        return $this->hasOne(Bank::class, ['id' => 'bank_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillPayments() {
        return $this->hasMany(BillPayment::class, ['payment_order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBills() {
        return $this->hasMany(Bill::class, ['id' => 'bill_id'])
            ->viaTable('bill_payment', ['payment_order_id' => 'id'])
            ->notDeleted();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionUser() {
        return $this->hasOne(FSMUser::class, ['id' => 'action_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionUserProfile() {
        return $this->hasOne(FSMProfile::class, ['user_id' => 'action_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser() {
        return $this->hasOne(FSMUser::class, ['id' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile() {
        return $this->hasOne(Files::class, ['id' => 'file_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdateUser() {
        return $this->hasOne(FSMUser::class, ['id' => 'update_user_id']);
    }

    static public function getExportStateList() {
        return [
            PaymentOrder::EXPORT_STATE_PREPARE => Yii::t('bill', 'Preparation'),
            PaymentOrder::EXPORT_STATE_SENT => Yii::t('bill', 'Sent'),
        ];
    }
    
    public function canUpdate()
    {
        if(!parent::canUpdate()){
            return;
        }
        
        return $this->status != PaymentOrder::EXPORT_STATE_SENT; 
    }
    
    public function canDelete()
    {
        if(!parent::canDelete()){
            return;
        }
        return true; 
    }
    
    public function getOptionsButtons($btnSize = '', $labeled = false) {
        if (!empty($btnSize)) {
            $btnSize = 'btn-' . $btnSize;
        }

        $params = [
            'model' => $this, 
            'btnSize' => $btnSize, 
            'labeled' => $labeled, 
            'isBtn' => true,
        ];
        $result = [];
        $result[] = PaymentOrder::getButtonSend($params);
        $result[] = PaymentOrder::getButtonCreateXml($params);
        $result[] = PaymentOrder::getButtonDownload($params);
        foreach ($result as $key => $btn) {
            if (empty($btn)) {
                unset($result[$key]);
            }
        }

        $result = implode('&nbsp;', $result);
        return $result;
    }

    static function getButtonSend(array $params) {
        extract($params); //'model', 'btnSize', 'labeled', 'isBtn'
        $result = '';
        if (in_array($model->status, [
                PaymentOrder::EXPORT_STATE_PREPARE,
            ])
        ){
            $label = Yii::t('common', 'Export');
            $title = Yii::t('common', 'Export');
            if (!empty($isBtn)) {
                $result = FSMHelper::aButton($model->id, [
                    'label' => (!empty($labeled) ? $label : null),
                    'title' => (!empty($labeled) ? null : $title),
                    'controller' => 'payment-order',
                    'action' => 'send',
                    //'class' => 'primary',
                    'size' => !empty($btnSize) ? $btnSize : null,
                    'icon' => 'export',
                    'modal' => true,
                ]);
            } else {
                $result = FSMHelper::aDropdown($model->id, [
                    'label' => $label,
                    'title' => $title,
                    'controller' => 'payment-order',
                    'action' => 'send',
                    'icon' => 'export',
                    'modal' => true,
                ]);
            }
        }
        return $result;
    }

    static function getButtonDownload(array $params) {
        extract($params); //'model', 'btnSize', 'labeled', 'isBtn'
        if (empty($model->file_id)){
            return '';
        }
        
        $result = '';
        $label = Yii::t('common', 'Download XML');
        $title = Yii::t('common', 'Download XML');
        if (!empty($isBtn)) {
            $result = FSMHelper::aButton($model->id, [
                'label' => (!empty($labeled) ? $label : null),
                'title' => (!empty($labeled) ? null : $title),
                'action' => 'download-xml',
                'class' => 'primary',
                'size' => !empty($btnSize) ? $btnSize : null,
                'icon' => 'download-alt',
                'modal' => true,
            ]);
        } else {
            $result = FSMHelper::aDropdown($model->id, [
                'label' => $label,
                'title' => $title,
                'action' => 'download-xml',
                'icon' => 'download-alt',
                'modal' => true,
            ]);
        }
        return $result;
    }

    static function getButtonCreateXml(array $params) {
        extract($params); //'model', 'btnSize', 'labeled', 'isBtn'
        /*
        if (in_array($model->status, [
                PaymentOrder::EXPORT_STATE_SENT,
            ])
        ){
            return '';
        }
         * 
         */

        $result = '';
        $label = Yii::t('common', 'Update XML');
        $title = Yii::t('common', 'Update XML');
        if (!empty($isBtn)) {
            $result = FSMHelper::aButton($model->id, [
                'label' => (!empty($labeled) ? $label : null),
                'title' => (!empty($labeled) ? null : $title),
                'action' => 'create-xml',
                'class' => 'primary',
                'size' => !empty($btnSize) ? $btnSize : null,
                'icon' => 'paperclip',
                'modal' => true,
            ]);
        } else {
            $result = FSMHelper::aDropdown($model->id, [
                'label' => $label,
                'title' => $title,
                'action' => 'create-xml',
                'icon' => 'paperclip',
                'modal' => true,
            ]);
        }
        return $result;
    }
    
    static public function getNameArr($where = null, $orderBy = 'number', $idField = 'id', $nameField = 'number', $withScope = true) {
        if (isset($where)) {
            return ArrayHelper::map(self::findByCondition($where, $withScope)->orderBy($orderBy)->asArray()->all(), $idField, $nameField);
        } else {
            return ArrayHelper::map(self::find($withScope)->orderBy($orderBy)->asArray()->all(), $idField, $nameField);
        }
    }    
    
    public function getLastNumber()
    {
        $date = date('Y-m-d');
        $orderList = $this
            ->find(true)
            ->andWhere(['>=', 'create_time', $date])
            ->orderBy('id desc')
            ->all();
        $number = count($orderList);
        $number++;
        $result = $date.'-'.str_pad($number, 2, '0', STR_PAD_LEFT);
        return $result;
    }
    
    public function beforeSave($insert) {
        if(!parent::beforeSave($insert)){
            return;
        }
        $this->pay_date = date('Y-m-d', strtotime($this->pay_date));
        
        return true;
    }    
    
    public function createXML()
    {
        $billPaymentModel = new \common\models\bill\search\BillPaymentSearch;
        $params = Yii::$app->request->getQueryParams();
        unset($params['id']);
        $params['payment_order_id'] = $this->id;
        $billPaymentDataProvider = $billPaymentModel->search($params);
        $data = $billPaymentDataProvider->query->all();
        
        if(empty($data)){
            Yii::$app->getSession()->setFlash('error', Yii::t('bill', 'Cannot to create XML file without invoices'));
            return $this->redirect(FSMBaseModel::getBackUrl());
        }
        
        $combinedArr = [];
        foreach ($data as $payment) {
            if(empty($payment->combine)){
                continue;
            }
            $bill = $payment->bill;
            $agreement = $bill->agreement;
            $firstClient = $agreement->firstClient;
            if(!array_key_exists($firstClient->id, $combinedArr)){
                $combinedArr[$firstClient->id]['items'] = [];
                $combinedArr[$firstClient->id]['total'] = 0;
                $combinedArr[$firstClient->id]['number'] = [];
            }
            if(!in_array($payment->id, $combinedArr[$firstClient->id])){
                $combinedArr[$firstClient->id]['items'][] = $payment->id;
                $combinedArr[$firstClient->id]['total'] += $payment->summa;
                $combinedArr[$firstClient->id]['number'][] = $bill->doc_number;
            }
        }
        foreach ($combinedArr as $key => $combinedItem) {
            if(count($combinedArr[$key]['items']) == 1){
                unset($combinedArr[$key]);
                continue;
            }
            $combinedArr[$key]['number'] = implode(', ', $combinedItem['number']);
        }        
            
        $transaction = Yii::$app->getDb()->beginTransaction();
        $arrToDelete = [];
        try {
            $paymentList = [];
            foreach ($data as $key => $payment) {
                if(in_array($payment->id, $arrToDelete)){
                    continue;
                }
                
                if($key == 0){
                    $paymentOrder = $payment->paymentOrder;
                }
                
                $bill = $payment->bill;
                $agreement = $bill->agreement;
                //$project = $agreement->project;
                $currency = $bill->valuta;
                $firstClient = $agreement->firstClient;
                $secondClient = $agreement->secondClient;
                $firstClientBank = $bill->firstClientBank;
                $secondClientBank = $bill->secondClientBank;
                
                $id = $payment->id;
                $docNumber = $bill->doc_number;
                $summa = number_format($payment->summa, 2, '.', '');
                if(array_key_exists($firstClient->id, $combinedArr) && in_array($id, $combinedArr[$firstClient->id]['items'])){
                    $id = $combinedArr[$firstClient->id]['items'][0];
                    $docNumber = $combinedArr[$firstClient->id]['number'];
                    for ($index = 0; $index < count($combinedArr[$firstClient->id]['items']); $index++) {
                        if(!in_array($combinedArr[$firstClient->id]['items'][$index], $arrToDelete)){
                            $arrToDelete[] = $combinedArr[$firstClient->id]['items'][$index];
                        }                        
                    }
                    $summa = number_format($combinedArr[$firstClient->id]['total'], 2, '.', '');
                }
                
                $pmtInfo = $docNumber.(!empty($payment->need_agreement) ? ' | Agreement #'.$payment->bill->agreement->number : '');
                
                $paymentArr = [
                    'ExtId' => $id,
                    'DocNo' => $id,
                    'RegDate' => date('Y-m-d'),
                    //'TaxPmtFlg' => ($project->vat_taxable ? 'Y' : 'N'),
                    'TaxPmtFlg' => 'N',
                    'Ccy' => $currency->name,
                    'PmtInfo' => $pmtInfo,
                    'PayLegalId' => $secondClient->reg_number,
                    'PayAccNo' => (isset($secondClientBank) ? $secondClientBank->account : ''),
                    'DebitCcy' => $currency->name,
                    'BenSet' => [
                        'BenExtId' => $firstClient->id,
                        'Priority' => 'N',
                        'Comm' => 'SHA',
                        'Amt' => $summa,
                        'BenAccNo' => (isset($firstClientBank) ? $firstClientBank->account : ''),
                        'BenName' => $firstClient->name,
                        'BenLegalId' => $firstClient->reg_number,
                        'BenAddress' => $firstClient->legal_address,
                        'BenCountry' => $firstClient->legalCountry->short_name,
                        'BBName' => (isset($firstClientBank) ? $firstClientBank->bank->name : ''),
                        'BBAddress' => (isset($firstClientBank) ? $firstClientBank->bank->address : ''),
                        'BBSwift' => (isset($firstClientBank) ? $firstClientBank->bank->swift : ''),
                    ]
                ];
                array_push($paymentList, $paymentArr);
            }
            
            $fidavistaArr = [
                '@attributes' => [
                    'xmlns' => 'http://bankasoc.lv/fidavista/fidavista_1-2.xsd',
                ],
                'Header' => [
                    'Timestamp' => date('YmdHis').'000',
                    'From' => Yii::$app->params['brandLabel'].' v.'.Yii::$app->params['brandVersion'],
                ],
                'Payment' => $paymentList,
            ];
            
            $xmlDOM = FSMArray2XML::createXML('FIDAVISTA', $fidavistaArr);
            $xmlStr = $xmlDOM->saveXML();

            if(!empty($this->file_id)){
                $filesModel = $this->file;
                $filesModel->deleteFile();
            }else{
                $filesModel = new Files();
            }
            $filename = date('Ymd-H-i').'-'.Yii::$app->security->generateRandomString().".xml";
        
            $result = $filesModel->saveDataToFile($filename, $xmlStr, 'payment/payment-order');
            if($result){
                $this->updateAttributes([
                    'file_id' => $filesModel->id,
                ]);
                $transaction->commit();
            }
        } catch (\Exception $e) {
            $result = false;
            $filesModel->delete();
            $transaction->rollBack();
        } finally {
            return $result;
        }         
    }
}
