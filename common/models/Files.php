<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\web\UploadedFile;

use common\models\mainclass\FSMBaseModel;
use common\models\user\FSMUser;

/**
 * This is the model class for table "files".
 *
 * @property integer $id
 * @property string $filename
 * @property string $filepath
 * @property string $fileurl
 * @property string $filemime
 * @property integer $filesize
 * @property string $create_time
 * @property integer $create_user_id
 * 
 * @property FSMUser $createUser
 */
class Files extends \common\models\mainclass\FSMCreateModel
{

    public $file;
    public $uploadedFile;
    public $oldFileName;

    private $uploadUrl;
    private $baseUrl;

    public function init()
    {
        $baseUrl = Url::base(true);
        $this->baseUrl = str_replace('/backend', '', $baseUrl);
        $this->uploadUrl = $this->baseUrl . '/uploads/';
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['filename', 'filepath', 'fileurl', 'filemime', 'filesize'], 'required'],
            [['filesize', 'create_user_id'], 'integer'],
            [['filename', 'file', 'create_time'], 'safe'],
            [['file'], 'file', 'extensions' => 'pdf, png, jpg, jpeg, doc, docx, xls, xlsx, pptx, zip', 'on' => 'update', 'skipOnEmpty' => true, ],
            [['fileurl', 'filepath', 'filemime'], 'string', 'max' => 255],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['create_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true)
    {
        return parent::label('files', 'File|Files', $n);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'filename' => Yii::t('files', 'File name'),
            'filepath' => Yii::t('files', 'File path'),
            'fileurl' => Yii::t('files', 'File Url'),
            'filemime' => Yii::t('files', 'File Mime'),
            'filesize' => Yii::t('files', 'File size'),
            'create_time' => Yii::t('common', 'Create Time'),
            'create_user_id' => Yii::t('common', 'Create User'),
        ];
    }

    /**
     * fetch stored uploadedFile name with complete path 
     * @return string
     */
    public function getUploadedFilePath()
    {
        return !empty($this->filepath) ? $this->filepath : 
            (!empty($this->filename) ? (Yii::$app->params['uploadPath'] . $this->filename) : null);
    }

    /**
     * fetch stored uploadedFile url
     * @return string
     */
    public function getUploadedFileUrl()
    {
        return !empty($this->fileurl) ? $this->fileurl : 
            (!empty($this->filename) ? $this->uploadUrl . $this->filename : null);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser() {
        return $this->hasOne(FSMUser::class, ['id' => 'create_user_id']);
    }
    
    /**
     * Process upload of file
     *
     * @return mixed the uploaded file instance
     */
    public function uploadFile($destinPath = '', $attribute = 'file')
    {
        if (!empty($destinPath)) {
            if (!in_array(substr("$destinPath", -1), ['/', '\\'])) {
                $destinPath .= '/';
            }
        }
        // get the uploaded file instance. for multiple file uploads
        // the following data will return an array (you may need to use
        // getInstances method)
        $this->uploadedFile = $file = UploadedFile::getInstance($this, $attribute);

        // if no file was uploaded abort the upload
        if (empty($file)) {
            return false;
        }   

        $filename = $file->name;
        $array = explode('.', $filename);
        $ext = end($array);
        // generate a unique file name
        $newFilename = Yii::$app->security->generateRandomString() . ".{$ext}";
        // the path to save file, you can set an uploadPath
        // in Yii::$app->params (as used in example below)
        $this->filename = $filename;
        $this->filepath = Yii::$app->params['uploadPath'] . $destinPath . $newFilename;
        $this->fileurl = $this->uploadUrl . $destinPath . $newFilename;
        //$this->filemime = $file->type;
        $this->filemime = !empty($file->tempName) ? mime_content_type($file->tempName) : 'none';
        $this->filesize = $file->size;

        // the uploaded file instance
        return $file;
    }

    /**
     * Process upload of multiply files
     *
     * @return mixed the uploaded files instances
     */
    public function uploadFileMultiply(UploadedFile $file, $destinPath = '', $attribute = 'file')
    {
        // if no file was uploaded abort the upload
        if (empty($file)) {
            return false;
        }   
        
        if (!empty($destinPath)) {
            if (!in_array(substr("$destinPath", -1), ['/', '\\'])) {
                $destinPath .= '/';
            }
        }

        $this->uploadedFile = $file;
        
        $filename = $file->name;
        $array = explode('.', $filename);
        $ext = end($array);
        // generate a unique file name
        $newFilename = Yii::$app->security->generateRandomString() . ".{$ext}";
        // the path to save file, you can set an uploadPath
        // in Yii::$app->params (as used in example below)
        $this->filename = $filename;
        $this->filepath = Yii::$app->params['uploadPath'] . $destinPath . $newFilename;
        $this->fileurl = $this->uploadUrl . $destinPath . $newFilename;
        //$this->filemime = $file->type;
        $this->filemime = !empty($file->tempName) ? mime_content_type($file->tempName) : 'none';
        $this->filesize = $file->size;

        // the uploaded file instance
        return $file;
    }

    public function save($runValidation = true, $attributeNames = NULL)
    {
        if(parent::save()){
            $result = true;
            if(!empty($this->oldFileName)){
                $this->deleteOldFile();
            }

            if (!empty($this->filepath)) {
                $dir = dirname($this->filepath);
                if(!is_dir($dir)){
                    mkdir($dir, 0777, true);
                }

                $result = $this->uploadedFile->saveAs($this->filepath);
            }
            return $result;
        }else{
            return;
        }
    }

    public function saveDataToFile($filename, $data, $destinPath = '')
    {
        if (!empty($destinPath)) {
            if (!in_array(substr("$destinPath", -1), ['/', '\\'])) {
                $destinPath .= '/';
            }
        }

        $array = explode('.', $filename);
        $ext = end($array);

        $this->filename = $filename;
        $this->filepath = str_replace('\\', '/', Yii::$app->params['uploadPath'] . $destinPath . $filename);
        $this->fileurl = $this->uploadUrl . $destinPath . $filename;
        
        $dir = dirname($this->filepath);
        if(!is_dir($dir)){
            mkdir($dir, 0777, true);
        }
        if(!file_put_contents($this->filepath, $data)){
            return;
        }
        
        $this->filemime = mime_content_type($this->filepath);
        $this->filesize = filesize($this->filepath);
        
        return parent::save();
    }
    
    /**
     * Process deletion of file
     *
     * @return boolean the status of deletion
     */
    public function deleteFile($file = null)
    {
        $file = isset($file) ? $file : $this->uploadedFilePath;

        // check if file exists on server
        if (!empty($file) && file_exists($file)) {
            // check if uploaded file can be deleted on server
            if (!unlink($file)) {
                return false;
            }
        }

        $dir = dirname($file);
        if (is_dir($dir) && (count(glob("$dir/*")) === 0) && !rmdir($dir)) {
            return false;
        }

        // if deletion successful, reset your file attributes
        $this->filename = $this->filepath = $this->fileurl = $this->filemime = $this->filesize = null;

        return true;
    }
    
    public function deleteOldFile($file = null)
    {
        $file = isset($file) ? $file : $this->oldFileName;

        // check if file exists on server
        if (!empty($file) && file_exists($file)) {
            // check if uploaded file can be deleted on server
            if (!unlink($file)) {
                return false;
            }
        }

        $dir = dirname($file);
        if (is_dir($dir) && (count(glob("$dir/*")) === 0) && !rmdir($dir)) {
            return false;
        }

        return true;        
    }

    public function delete(FSMBaseModel $owner = null)
    {
        if ($this->deleteFile()) {
            return parent::delete($owner);
        } else {
            return;
        }
    }

}
