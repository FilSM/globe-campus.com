<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "assets".
 *
 * @property integer $id
 * @property string $class_name
 * @property string $asset_version
 * @property string $updated_at
 */
class Assets extends \common\models\mainclass\FSMBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'assets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class_name', 'updated_at'], 'required'],
            [['updated_at'], 'safe'],
            [['asset_version'], 'string', 'max' => 50],
            [['class_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('app', 'Assets|Assets', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'class_name' => 'Class Name',
            'asset_version' => 'Asset version',
            'updated_at' => 'Updated At',
        ];
    }
}