<?php

namespace common\models\user;

use Yii;

use common\models\user\FSMProfile;
use common\models\client\Client;
use common\models\abonent\Abonent;
/**
 * This is the model class for table "profile_client".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property integer $client_id
 * @property integer $abonent_id
 *
 * @property Client $client
 * @property Profile $profile
 * @property Abonent $abonent
 */
class ProfileClient extends \common\models\mainclass\FSMBaseModel
{

    public static function find($withScope = true) {
        if ((Yii::$app->id != 'app-console') && $withScope) {
            return new \common\scopes\AbonentFieldScopeQuery(get_called_class());
        }
        return parent::find();
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile_client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['abonent_id', 'profile_id', 'client_id'], 'required'],
            [['abonent_id', 'profile_id', 'client_id'], 'integer'],
            [['profile_id', 'client_id', 'abonent_id'], 'unique', 'targetAttribute' => ['profile_id', 'client_id', 'abonent_id']],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['client_id' => 'id']],
            [['profile_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMProfile::class, 'targetAttribute' => ['profile_id' => 'id']],
            [['abonent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Abonent::class, 'targetAttribute' => ['abonent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('user', 'User client|User clients', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'profile_id' => Yii::t('user', 'Profile'),
            'client_id' => Yii::t('common', 'Client'),
            'abonent_id' => Yii::t('common', 'Abonent'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonent()
    {
        return $this->hasOne(Abonent::class, ['id' => 'abonent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::class, ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(FSMProfile::class, ['id' => 'profile_id']);
    }
}