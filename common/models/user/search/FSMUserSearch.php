<?php

namespace common\models\user\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\models\user\FSMUser;

/**
 * UserSearch represents the model behind the search form about User.
 */
class FSMUserSearch extends \dektrium\user\models\UserSearch
{

    /** @var string */
    public $client;
    public $client_id;
    public $client_name;
    public $role;
    public $fullName;
    public $phone;
    public $clientItIs;
    public $sms_auth;

    /** @inheritdoc */
    public function rules()
    {
        $rules = parent::rules();
        $rules = ArrayHelper::merge(
            $rules, [
                [['fullName', 'client', 'client_id', 'client_name', 'phone', 'role', 'clientItIs', 'sms_auth'], 'safe'],
            ]
        );
        return $rules;
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if(FSMUser::getIsPortalAdmin()){
            $query = $this->finder->getUserQuery();
        }else{
            $query = FSMUser::find(true);
        }

        $query->select = [
            '{{%user}}.*',
            'fullName' => 'profile.name',
            'client_name' => 'client.name',
            'sms_auth' => 'profile.sms_auth',
        ];
        $query->leftJoin(['profile' => 'profile'], 'profile.user_id = {{%user}}.id');
        $query->leftJoin(['profile_client' => 'profile_client'], 'profile_client.profile_id = profile.id');
        $query->leftJoin(['client' => 'client'], 'client.id = profile_client.client_id');
        $query->leftJoin(['auth_assignment' => 'auth_assignment'], 'auth_assignment.user_id = {{%user}}.id');

        $query->groupBy('{{%user}}.id');

        if (!isset($params['sort'])) {
            //$query->addOrderBy('id desc');
            $query->addOrderBy('fullName');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'client.id' => $this->client_id,
            'auth_assignment.item_name' => $this->role,
            'registration_ip' => $this->registration_ip,
            'sms_auth' => $this->sms_auth,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username]);
        $query->andFilterWhere(['like', 'email', $this->email]);
        $query->andFilterWhere(['like', 'profile.name', $this->fullName]);
        $query->andFilterWhere(['like', 'profile.phone', $this->phone]);
        $query->andFilterWhere(['like', 'client.name', $this->client_name]);
        $query->andFilterWhere(['client.it_is' => $this->clientItIs]);

        if (!empty($this->created_at) && strpos($this->created_at, '-') !== false) {
            list($start_date, $end_date) = explode(' - ', $this->created_at);
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));
            $query->andFilterWhere(['between', '{{%user}}.created_at', strtotime($start_date), strtotime($end_date . ' 23:59:59')]);
        }

        if (!empty($this->last_login_at) && strpos($this->last_login_at, '-') !== false) {
            list($start_date, $end_date) = explode(' - ', $this->last_login_at);
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));
            $query->andFilterWhere(['between', '{{%user}}.last_login_at', strtotime($start_date), strtotime($end_date . ' 23:59:59')]);
        }
/*
        SELECT 
            user.*,
            `profile`.`name` AS `fullName`,
            `client`.`name` AS `client_name`,
            `profile`.`sms_auth` AS `sms_auth`
        FROM
            user
                LEFT JOIN
            `profile` `scope_profile` ON scope_profile.user_id = user.id
                LEFT JOIN
            `profile_client` `scope_profile_client` ON scope_profile_client.profile_id = scope_profile.id
                LEFT JOIN
            `abonent_client` `scope_abonent_client` ON scope_abonent_client.client_id = scope_profile_client.client_id
                LEFT JOIN
            `profile` ON profile.user_id = user.id
                LEFT JOIN
            `profile_client` ON profile_client.profile_id = profile.id
                LEFT JOIN
            `client` ON client.id = profile_client.client_id
                LEFT JOIN
            `auth_assignment` ON auth_assignment.user_id = user.id
        WHERE
            `scope_abonent_client`.`abonent_id` = 1
        GROUP BY user.`id`
        ORDER BY `fullName`
 * 
 */
        
        //return $query->all();
        return $dataProvider;
    }

}
