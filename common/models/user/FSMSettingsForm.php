<?php

namespace common\models\user;

use Yii;
use yii\helpers\ArrayHelper;
use yii\base\Exception;

use dektrium\user\Mailer;

use common\components\FSMStrengthValidator;
use common\models\user\FSMUser;
use common\models\user\PasswordHistory;

class FSMSettingsForm extends \dektrium\user\models\SettingsForm {

    /** @var string */
    public $role;
    public $sms_auth;
    public $sms_code;
    public $sms_auth_exp;

    private $_user;
    
    /** @return User */
    public function getUser() {
        if ($this->_user == null) {
            $this->_user = \Yii::$app->user->identity;
        }

        return $this->_user;
    }

    /** @inheritdoc */
    public function __construct(Mailer $mailer, $config = []) {
        if (isset($config['user'])) {
            $this->user = $config['user'];
        }
        parent::__construct($mailer, $config);
        $this->setAttributes(['role' => $this->user->role,], false);
    }

    /** @inheritdoc */
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios = ArrayHelper::merge($scenarios, ['admin_update' => ['role', 'username', 'email', 'password', 'new_password', 'sms_auth'],]);
        return $scenarios;        
    }

    public function setUser($user) {
        $this->_user = $user;
    }

    /** @inheritdoc */
    public function rules() {
        $rules = parent::rules();
        $scenario = $this->getScenario();
        if($scenario == 'admin_update'){
            $rules = ArrayHelper::merge(
                $rules, 
                [
                    [['role'], 'required'],
                ]        
            );
        }else{
            $rules = ArrayHelper::merge(
                $rules,
                [
                    [['new_password'], FSMStrengthValidator::class, 
                        FSMStrengthValidator::RULE_MIN => 9, 
                        FSMStrengthValidator::RULE_UP => 1, 
                        FSMStrengthValidator::RULE_LOW => 1, 
                        FSMStrengthValidator::RULE_NUM => 1, 
                        FSMStrengthValidator::RULE_SPL => 1, 
                        'userAttribute' => 'username',
                    ], 
                ]
            );        
        }
        /*
        $rules = ArrayHelper::merge(
            $rules,
            [
                [['new_password'], FSMStrengthValidator::class, 
                    FSMStrengthValidator::RULE_MIN => 9, 
                    FSMStrengthValidator::RULE_UP => 1, 
                    FSMStrengthValidator::RULE_LOW => 1, 
                    FSMStrengthValidator::RULE_NUM => 1, 
                    FSMStrengthValidator::RULE_SPL => 1, 
                    'userAttribute' => 'username',
                ],                
            ]
        );
         * 
         */        

        return $rules;
    }

    /** @inheritdoc */
    public function attributeLabels() {
        $labels = parent::attributeLabels();
        $labels = ArrayHelper::merge(
            $labels, 
            [
                'role' => Yii::t('fsmuser', 'Role'),
                'sms_auth' => Yii::t('fsmuser', 'Need SMS authentication'),
                'sms_code' => Yii::t('fsmuser', 'SMS code'),
                'sms_auth_exp' => Yii::t('fsmuser', 'SMS authentication expiration time'),
            ]
        );
        return $labels;
    }

    /**
     * Saves new account settings.
     *
     * @return bool
     */
    public function save() {
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {        
            $this->user->role = $this->role;
            $profile = $this->user->profile;
            $profile->sms_auth = $this->sms_auth;
            if(!$profile->save(false)){
                throw new Exception('Can not save Profile! ' . $profile->errorMessage);
            }

            if(!parent::save()){
                throw new Exception('Can not save user data! ' . $this->user->errorMessage);
            }
            
            if (!empty(Yii::$app->params['STRONG_PASSWORD_CONTROL'])) {
                if(!$this->user->passwordControl()){
                    $lastPassword = $this->user->lastPasswordHistory;
                    if(!empty($lastPassword)){
                        if(password_verify($this->new_password, $lastPassword->password_hash)){
                            $transaction->rollBack();
                            $message = Yii::t('user', 'A new password cannot be the same as the old one.');
                            Yii::$app->getSession()->setFlash('error', $message);
                            return;
                        }
                    }

                    $passwordHistory = new PasswordHistory();
                    $passwordHistory->user_id = $this->user->id;
                    $passwordHistory->password_hash = $this->user->password_hash;
                    $passwordHistory->create_date = date('Y-m-d');
                    if(!$passwordHistory->save()){
                        throw new Exception('Can not save password history! ' . $passwordHistory->errorMessage);
                    }
                }
            }
            
            $transaction->commit();            
        } catch (Exception $e) {
            $message = $e->getMessage();
            $transaction->rollBack();
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
            return;
        }
        
        return true;
    }

}
