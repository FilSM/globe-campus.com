<?php

namespace common\models\user;

use Yii;
use yii\base\Exception;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

use common\components\FSMStrengthValidator;
use common\models\client\Client;
use common\models\user\PasswordHistory;
use common\components\user\FSMUserMailer;

/**
 * User ActiveRecord model.
 *
 * Database fields:
 * @property integer $id
 * @property string  $username
 * @property string  $email
 * @property string  $unconfirmed_email
 * @property string  $password_hash
 * @property string  $auth_key
 * @property integer $superuser
 * @property integer $registration_ip
 * @property integer $confirmed_at
 * @property integer $blocked_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $flags
 *
 * Defined relations:
 * @property Account[] $accounts
 * @property Profile $profile
 * @property PasswordHistory[] $passwordHistories
 */
class FSMUser extends \dektrium\user\models\User {

    const USER_ROLE_SUPERUSER   = 'superuser';
    const USER_ROLE_PORTALADMIN = 'portal_admin';
    const USER_ROLE_ADMIN       = 'admin';
    const USER_ROLE_BOSS        = 'boss';
    const USER_ROLE_MANAGER     = 'manager';
    const USER_ROLE_BOOKER      = 'booker';
    const USER_ROLE_OPERATOR    = 'operator';
    const USER_ROLE_AUTHOR      = 'author';
    const USER_ROLE_COORDINATOR = 'coordinator';
    const USER_ROLE_LEGAL_TAX   = 'legal_tax';
    const USER_ROLE_TEHNICAL    = 'tehnical';
    const USER_ROLE_USER        = 'user';
    const USER_ROLE_CLIENT      = 'client';
    const USER_ROLE_AGENT       = 'agent';
    const USER_ROLE_CONTRAGENT  = 'contragent';
    const USER_ROLE_LIMIT_USER  = 'limit_user';
    const USER_ROLE_SUPPLIER    = 'supplier';
    const USER_ROLE_REPORTING   = 'reporting_person';
    
    public $role;
    public $client_id;
    public $client_name;
    public $clientItIs;
    public $sms_auth;

    public static function find($withScope = false)
    {
        if ((Yii::$app->id != 'app-console') && $withScope) {
            $scope = new \common\scopes\FSMUserScopeQuery(get_called_class());
            return $scope->forClient();
        }
        return parent::find();
    }
    
    /** @inheritdoc */
    public function scenarios() {
        $scenarios = parent::scenarios();
        return ArrayHelper::merge($scenarios, [
            'from-client' => ['username', 'email', 'password'],
            'create-from' => ['role', 'username', 'email', 'password'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        $rules = parent::rules();
        $rules = ArrayHelper::merge(
            $rules,
            [
                [['superuser'], 'integer'],
                [['password'], FSMStrengthValidator::class, 
                    FSMStrengthValidator::RULE_MIN => 9, 
                    FSMStrengthValidator::RULE_UP => 1, 
                    FSMStrengthValidator::RULE_LOW => 1, 
                    FSMStrengthValidator::RULE_NUM => 1, 
                    FSMStrengthValidator::RULE_SPL => 1, 
                    'userAttribute' => 'username',
                    'except' => ['create-from'],
                ],                
                //[['password'], StrengthValidator::class, 'preset' => 'normal', 'userAttribute' => 'username'],
                [['email'], 'required', 'on' => ['from-client']],
                [['role', 'username', 'email'], 'required', 'on' => ['create-from']],
            ]
        );
        return $rules;          
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        $labels = parent::attributeLabels();
        $labels = ArrayHelper::merge(
            $labels, 
            [
                'client_id' => Yii::t('user', 'User company'),
                'superuser' => Yii::t('user', 'Superuser'),
                'role' => Yii::t('fsmuser', 'Role'),
                'name' => Yii::t('client', 'Full name'),
                'phone' => Yii::t('user', 'Phone'),
                'clientItIs' => Yii::t('user', 'Profile type'),
                'client_name' => Yii::t('common', 'Client'),
            ]
        );
        return $labels;
    }

    public function behaviors() {
        return [
            'modelBehavior' => array(
                'class' => \common\behaviors\UserModelBehavior::class,
            ),
        ];
    }

    public static function modelTitle($n = 1, $translate = true) {
        return self::label('user', 'User|Users', $n, $translate);
    }

    public static function label($category, $message, $n = 1, $translate = true) {
        if (strpos($message, '|') !== false) {
            $chunks = explode('|', $message);
            $message = $chunks[$n - 1];
        }
        return $translate ? Yii::t($category, $message) : $message;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPasswordHistories() {
        return $this->hasMany(PasswordHistory::class, ['user_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastPasswordHistory() {
        $passwordHistory = PasswordHistory::findByCondition(['user_id' => $this->id])
            ->orderBy(['id' => SORT_DESC])
            ->one();        
        return $passwordHistory;
    }
    
    /**
     * Returns a value indicating whether the user is a guest (not authenticated).
     * @return bool whether the current user is a guest.
     * @see getIdentity()
     */
    public function getIsGuest()
    {
        if(!empty($this->id)){
            if(empty(Yii::$app->params['SMS_AUTH'])) {
                return false;
            }else{
                $smsAuthComplete = Yii::$app->session->get('sms-auth-complete');
                return isset($smsAuthComplete) && empty($smsAuthComplete);
            }            
        }else{
            return true;
        }
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients() {
        $profile = $this->profile;
        return !empty($profile) ? $profile->hasMany(Client::class, ['id' => 'client_id'])
            ->viaTable('profile_client', ['profile_id' => 'id'])
            ->notDeleted()
            : null;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientsWithoutScope() {
        $clientArr = [];
        if($profile = $this->profile){
            $profileClientList = $profile->hasMany(ProfileClient::class, ['profile_id' => 'id'])->all();
            foreach ($profileClientList as $profileClient) {
                $clientArr[] = Client::find()->where(['id' => $profileClient->client_id])->notDeleted()->one();
            }
        }
        return $clientArr;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstClient() {
        $profile = $this->profile;
        return !empty($profile) ? $profile->firstClient : null;
    }
    
    /**
     * @inheritdoc
     */
    public function register() {
        $result = parent::register();
        if (!$result) {
            $message = Yii::t('user', $this->modelTitle() . ' not inserted due to validation error.');
            $message = $this->getErrorMessage($message);
            Yii::$app->getSession()->setFlash('error', Yii::t('user', $message));
            return false;
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert) {
        if(parent::beforeSave($insert)) {
            if (empty($this->role)) {
                $roleList = $this->getUserRoleArray($this->id);
                $this->role = !empty($roleList) ? $roleList : [FSMUser::USER_ROLE_USER];
            }
            if($insert){
                $this->setAttribute('created_at', time());
            }
            $this->setAttribute('updated_at', time());
            return true;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes) {
        if ($insert) {
            if($this->id == 1){
                $this->updateAttributes([
                    'superuser' => 1,
                    //'role' => 'superuser',
                ]);
                $auth = Yii::$app->authManager;
                $role = $auth->getRole(FSMUser::USER_ROLE_SUPERUSER);
                if(isset($role)){
                    $auth->assign($role, $this->id);
                }else{
                    throw new Exception('User role '.FSMUser::USER_ROLE_SUPERUSER.' not exist!');
                }
            }
            
            $userId = Yii::$app->user->getId();
            $postData = Yii::$app->request->post();
            $profile = Yii::createObject([
                'class' => FSMProfile::class,
                'user_id' => $this->id,
                'name' => !empty($postData['FSMProfile']['name']) ? $postData['FSMProfile']['name'] : $this->username,
                'phone' => !empty($postData['FSMProfile']['phone']) ? $postData['FSMProfile']['phone'] : '',
                'timezone' => '',
                'bio' => !empty($postData['FSMProfile']['bio']) ? $postData['FSMProfile']['bio'] : '',
                'deleted' => false,
                'create_user_id' => $userId,
                'create_time' => new Expression('NOW()'),
                'update_user_id' => $userId,
                'update_time' => new Expression('NOW()')
            ]);

            if (!$profile->save(false)) {
                $message = Yii::t('user', $profile->modelTitle() . ' not inserted due to non-specific error.');
                $message = $this->getErrorMessage($message);
                Yii::$app->getSession()->setFlash('error', Yii::t('user', $message));
            }
        }
        Yii\db\ActiveRecord::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete(){
        $result = true;
        if ($profile = $this->profile) {
            $result = $profile->delete();
        }
        return $result;
    }
    
    public function getErrorMessage($defaultMessage = 'Undefined error!') {
        if ($this->hasErrors()) {
            $message = [];
            foreach ($this->getErrors() as $key => $attribute) {
                foreach ($attribute as $error) {
                    if(!in_array($error, $message)){
                        $message[] = $error;
                    }
                }
            }
            $message = implode(PHP_EOL, $message);
        } else {
            $message = $defaultMessage;
        }
        return $message;
    }

    public function getMyRoleList() {
        static $result = null;
        static $oldUserId = null;
        
        $userId = $this->id;
        if(!isset($result) || ($oldUserId != $userId)){
            $oldUserId = $userId;
            $result = Yii::$app->authManager->getArrRolesByUser($userId);
        }
        return $result;
    }
    
    static public function getUserRoleArray($userId = null) {
        static $result = null;
        static $oldUserId = null;
        
        if(!isset($result) || ($oldUserId != $userId)){
            if(!$userId){
                $currentUser = Yii::$app->user->identity;
                $userId = $currentUser ? $currentUser->id : null;
            }
            $oldUserId = $userId;
            $roleList = Yii::$app->authManager->getRolesByUser($userId);
            $result = [];
            foreach ($roleList as $role) {
                $result[] = $role->name;
            }
        }
        return $result;
    }
    
    static public function getIsPortalAdmin($userId = null) {
        static $result = null;
        static $oldUserId = null;
        
        if(!isset($result) || ($oldUserId != $userId)){
            if(!$userId){
                $currentUser = Yii::$app->user->identity;
                $userId = $currentUser ? $currentUser->id : null;
            }else{
                $currentUser = self::findOne($userId);
            }
            $oldUserId = $userId;
            if(!$currentUser){
                $result = false;
                return $result;
            }
            if($currentUser->superuser){
                $result = true;
                return $result;
            }
            $userRoles = Yii::$app->authManager->getRolesByUser($currentUser->id);
            foreach ($userRoles as $role) {
                if(in_array($role->name, [
                        FSMUser::USER_ROLE_SUPERUSER, 
                        FSMUser::USER_ROLE_PORTALADMIN,
                    ])){
                    $result = true;
                    return $result;
                }
            }
            $result = false;
            return $result;
        }else{
            return $result;
        }
    }

    static public function getIsSystemAdmin($userId = null) {
        static $result = null;
        static $oldUserId = null;
        
        if(FSMUser::getIsPortalAdmin($userId)){
            $result = true;
            return $result;
        }
        
        if(!isset($result) || ($oldUserId != $userId)){
            if(!$userId){
                $currentUser = Yii::$app->user->identity;
                $userId = $currentUser ? $currentUser->id : null;
            }else{
                $currentUser = self::findOne($userId);
            }
            $oldUserId = $userId;
            if(!$currentUser){
                $result = false;
                return $result;
            }
            $userRoles = Yii::$app->authManager->getRolesByUser($currentUser->id);
            foreach ($userRoles as $role) {
                if(in_array($role->name, [
                        FSMUser::USER_ROLE_ADMIN,
                    ])){
                    $result = true;
                    return $result;
                }
            }
            $result = false;
            return $result;
        }else{
            return $result;
        }
    }

    static public function getMyClientType($userId = null) {
        static $result = null;
        static $oldUserId = null;
        
        if(!isset($result) || ($oldUserId != $userId)){
            if(!$userId){
                $currentUser = Yii::$app->user->identity;
                $userId = $currentUser ? $currentUser->id : null;
            }else{
                $currentUser = self::findOne($userId);
            }
            $oldUserId = $userId;
            if(!$currentUser){
                $result = false;
                return $result;
            }
            $client = isset($currentUser->firstClient) ? $currentUser->firstClient : null;
            $result = $client ? $client->it_is : false;
        }
        return $result;
    }
    
    static public function getIamOwner($userId = null) {
        static $result = null;
        static $oldUserId = null;
        
        if(!isset($result) || ($oldUserId != $userId)){
            if(!$userId){
                $currentUser = Yii::$app->user->identity;
                $userId = $currentUser ? $currentUser->id : null;
            }else{
                $currentUser = self::findOne($userId);
            }
            $oldUserId = $userId;
            if(!$currentUser){
                $result = false;
                return $result;
            }
            $client = !empty($currentUser->firstClient) ? $currentUser->firstClient : null;
            $result = $client && ($client->it_is == Client::CLIENT_IT_IS_OWNER);
            return $result;
        }else{
            return $result;
        }
    }
    
    public function hasRole($roleList = null) {
        static $result = null;
        static $oldUserId = null;
        static $oldRoleList = null;
        
        if(!isset($result) || ($oldUserId != $this->id) || ($oldRoleList != $roleList)){
            $oldUserId = $this->id;
            $oldRoleList = $roleList;
            $userRoles = Yii::$app->authManager->getUserRoleList($this->id);
            $result = false;
            foreach ($userRoles as $role) {
                if($result = in_array($role->name, (array)$roleList)){
                    break;
                }
            }
        }
        return $result;
    }

    public function passwordControl()
    {
        $passwordHistory = $this->lastPasswordHistory;
        if(empty($passwordHistory)){
            return false;
        }
        $date1 = new \DateTime("now");
        $date2 = new \DateTime($passwordHistory->create_date);
        $interval = $date1->diff($date2);
        return ($interval->days < 90);
    }

    /**
     * @return Mailer
     * @throws \yii\base\InvalidConfigException
     */
    protected function getMailer()
    {
        return \Yii::$container->get(FSMUserMailer::class);
    }    
}
