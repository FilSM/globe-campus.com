<?php

namespace common\models\user;

use Yii;
use yii\base\Model;

use common\components\FSMSMSCode;

/**
 * LoginForm get user's login and password, validates them and logs the user in. If user has been blocked, it adds
 * an error to login form.
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class SMSAuthForm extends Model
{
    public $sms_code;

    /** @var \common\models\user\FSMUser */
    protected $user;
    /** @var \common\models\user\FSMProfile */
    protected $profile;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sms_code'], 'required'],
            [['sms_code'], 'validateSMSCode'],
        ];
    }

    /**
     * Validates if the hash of the given password is identical to the saved hash in the database.
     * It will always succeed if the module is in DEBUG mode.
     *
     * @return void
     */
    public function validateSMSCode($attribute, $params)
    {
        if ($this->profile === null || ($this->sms_code != $this->profile->sms_code)){
            $this->addError($attribute, Yii::t('fsmuser', 'Invalid SMS code'));
        }
    }
    
    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'sms_code' => Yii::t('fsmuser', 'SMS code'),
        ];
    }

    /**
     * Validates form and logs the user in.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate() && $this->profile) {
            $isLogged = Yii::$app->user->login($this->user);

            if ($isLogged) {
                $this->user->updateAttributes(['last_login_at' => time()]);
                $this->profile->updateAttributes([
                    'sms_code' => null,
                    'sms_auth_exp' => null,
                ]);
            }

            return $isLogged;
        }

        return false;
    }


    /** @inheritdoc */
    public function formName()
    {
        return 'sms-auth-form';
    }

    /** @inheritdoc */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->user = Yii::$app->user->identity;
            $this->profile = $this->user->profile;
            return true;
        } else {
            return false;
        }
    }
    
}
