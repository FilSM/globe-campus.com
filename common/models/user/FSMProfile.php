<?php

namespace common\models\user;

use Yii;
use yii\base\Exception;
use yii\base\ModelEvent;
use yii\db\ActiveRecord;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

use common\models\mainclass\FSMBaseModel;
use common\models\client\Client;
use common\models\user\ProfileClient;
use common\components\FSMSMSCode;

class FSMProfile extends \dektrium\user\models\Profile
{
    public $clientItIs;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules = ArrayHelper::merge(
            [
                [['name', 'phone'], 'required'],
                [['user_id', 'language_id', 'deleted', 'sms_auth'], 'integer'],
                [['short_name'], 'string', 'max' => 5],
                [['sms_code'], 'string', 'max' => 4],
                [['phone'], 'string', 'max' => 20],
                [['gcalendar_id'], 'string', 'max' => 100],
                [['timezone'], 'string', 'max' => 50],
                [['sms_auth_exp'], 'safe'],                
            ], $rules
        );
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels = ArrayHelper::merge(
            $labels, [
                'id' => Yii::t('common', 'ID'),
                'user_id' => Yii::t('user', 'User ID'),
                'name' => Yii::t('client', 'Full name'),
                'short_name' => Yii::t('client', 'Short name'),
                'language_id' => Yii::t('languages', 'Communication language'),
                'phone' => Yii::t('fsmuser', 'Phone'),
                //'public_email'   => Yii::t('user', 'Public email'),
                //'location' => Yii::t('user', 'Postal address'),
                'bio' => Yii::t('common', 'Comment'),
                'timezone' => Yii::t('user', 'Timezone'),
                'deleted' => Yii::t('common', 'Deleted'),
                'sms_auth' => Yii::t('fsmuser', 'Need SMS authentication'),
                'sms_code' => Yii::t('fsmuser', 'SMS code'),
                'sms_auth_exp' => Yii::t('fsmuser', 'SMS authentication expiration time'),
                'gcalendar_id' => Yii::t('fsmuser', 'Google calendar Id'),
        ]);
        return $labels;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors = ArrayHelper::merge(
            $behaviors, 
            [
                'modelBehavior' => array(
                    'class' => \common\behaviors\FSMProfileModelBehavior::class,
                ),
                'attributeStamp' => [
                    'class' => AttributeBehavior::class,
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => 'create_user_id',
                        ActiveRecord::EVENT_BEFORE_UPDATE => 'update_user_id',
                    ],
                    'value' => function ($event) {
                        return Yii::$app->user->getId();
                    },
                ],
                'timestamp' => [
                    'class' => TimestampBehavior::class,
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => 'create_time',
                        ActiveRecord::EVENT_BEFORE_UPDATE => 'update_time',
                    ],
                    'value' => new Expression('NOW()'),
                ],
            ]   
        );
        return $behaviors;
    }

    public static function modelTitle($n = 1, $translate = true)
    {
        return self::label('user', 'Profile|Profiles', $n, $translate);
    }

    public static function label($category, $message, $n = 1, $translate = true)
    {
        if (strpos($message, '|') !== false) {
            $chunks = explode('|', $message);
            $message = $chunks[$n - 1];
        }
        return $translate ? Yii::t($category, $message) : $message;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(\common\models\Language::class, ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfileClients()
    {
        return $this->hasMany(ProfileClient::class, ['profile_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Client::class, ['id' => 'client_id'])
            ->viaTable('profile_client', ['profile_id' => 'id'])
            ->notDeleted();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstClient() {
        $clientList = $this->clients;
        return isset($clientList) ? (is_array($clientList) ? $clientList[0] : $clientList) : null;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'update_user_id']);
    }

    public function getNonVersionFields()
    {
        return [];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $dataToSave = Yii::$app->request->post();

        if (empty($dataToSave)) {
            return true;
        }

        // will created new profile from user creating form
        if (!empty($dataToSave['Client'])) {
            $this->name = $dataToSave['Client']['name'];
            $this->phone = $dataToSave['Client']['contact_phone'];
            $this->language_id = $dataToSave['Client']['language_id'];
        } elseif (!empty($dataToSave['FSMUser'])) {
            $this->name = !empty($this->name) ? $this->name : '';
            $this->phone = !empty($this->phone) ? $this->phone : '';
            $this->timezone = !empty($this->timezone) ? $this->timezone : '';
            $this->deleted = false;
            $this->create_user_id = Yii::$app->user->getId();
            $this->create_time = new Expression('NOW()');
            $this->update_user_id = Yii::$app->user->getId();
            $this->update_time = new Expression('NOW()');
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $result = true;
        /*
        if($insert){
            $currentUser = Yii::$app->user->identity;
            $currentProfile = $currentUser->profile;
            $currentProfileClients = $currentProfile->profileClients;
            foreach ($currentProfileClients as $client) {
                $userClient = new ProfileClient();  
                $userClient->profile_id = $this->id;
                $userClient->client_id = $client->client_id;
                $result = $result && $userClient->save();
            }
        }
         * 
         */
        return $result;
    }

    public function beforeMarkAsDeleted()
    {
        $event = new ModelEvent;
        $this->trigger(FSMBaseModel::EVENT_BEFORE_MARK_AS_DELETED, $event);

        return $event->isValid;
    }

    private function markAsDeleted()
    {
        $result = false;
        if ((!$this->deleted) && ($result = $this->beforeMarkAsDeleted())) {
            $result = $this->updateAttributes([
                'user_id' => null,
                'phone' => (!empty($this->phone) ? $this->phone : '-'),
                'deleted' => true,
            ]);
        }
        return $result;
    }

    public function delete()
    {
        if (!$this->hasAttribute('deleted') || empty($this->name)) {
            $result = parent::delete();
        } else {
            $result = $this->markAsDeleted();
        }
        if (!$result || $this->hasErrors()) {
            $message = [];
            foreach ($this->getErrors() as $attribute) {
                foreach ($attribute as $error) {
                    $message[] = $error;
                }
            }
            $message[] = Yii::t('user', 'Can`t delete User profile');
            $message = implode(PHP_EOL, $message);
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
        }

        return $result;
    }

    static public function getManagerList()
    {
        $result = ArrayHelper::map(self::find()
            ->leftJoin('user', 'user.id = profile.user_id')
            ->leftJoin('profile_client', 'profile_client.profile_id = profile.id')
            ->leftJoin('client', 'client.id = profile_client.client_id')
            ->andWhere(['client.it_is' => 'broker'])
            ->andWhere(['like', 'user.role', 'manager'])
            ->andWhere(['profile.deleted' => 0])
            ->orderBy('name')
            ->asArray()
            ->all(), 'id', 'name');

        return $result;
    }

    static public function getUserList()
    {
        $result = ArrayHelper::map(self::find()
            ->leftJoin('user', 'user.id = profile.user_id')
            ->leftJoin('profile_client', 'profile_client.profile_id = profile.id')
            ->leftJoin('client', 'client.id = profile_client.client_id')
            ->andWhere(['client.it_is' => 'broker'])
            ->andWhere(['like', 'user.role', 'user'])
            ->andWhere(['profile.deleted' => 0])
            ->orderBy('name')
            ->asArray()
            ->all(), 'id', 'name');

        return $result;
    }

    static public function getProfileListByRole($role, $client_id = null, $itIs = null)
    {
        $role = (array) $role;
        $userIDList = Yii::$app->authManager->getUserIdsByRole($role);

        $query = self::find()
            ->innerJoin('user', 'profile.user_id = user.id')
            ->andWhere(['profile.deleted' => 0])
            ->andWhere(['user_id' => $userIDList])
            ->andWhere(['user.blocked_at' => null])
            ->orderBy('name');
        if (!empty($client_id) || !empty($itIs)) {
            $query
                ->leftJoin('profile_client', 'profile_client.profile_id = profile.id')
                ->leftJoin('client', 'client.id = profile_client.client_id');
        }
        if (!empty($client_id)) {
            $query->andWhere(['client.id' => $client_id]);
        }
        if (!empty($itIs)) {
            $query->andWhere(['client.it_is' => $itIs]);
        }
        $result = ArrayHelper::map($query->asArray()->all(), 'id', 'name');
        return $result;
    }

    static public function getProfileList($params = [])
    {
        $result = ArrayHelper::map(self::find()
            ->innerJoin('user', 'profile.user_id = user.id')
            ->andWhere(['user.blocked_at' => null])
            ->andWhere(['deleted' => 0])
            ->andWhere((isset($params['search']) ? 'name LIKE "%' . $params['search'] . '%"' : 'name IS NOT NULL'))
            ->andWhere((isset($params['id']) ? ['profile.id' => $params['id']] : 'profile.id IS NOT NULL'))
            ->orderBy('name')
            ->asArray()
            ->all(), 'id', 'name');
        return $result;
    }

    static public function getNameArr($where = null, $orderBy = 'name', $idField = 'id', $nameField = 'name', $withScope = true)
    {
        if (isset($where)) {
            return ArrayHelper::map(self::findByCondition($where, $withScope)->orderBy($orderBy)->asArray()->all(), $idField, $nameField);
        } else {
            return ArrayHelper::map(self::find($withScope)->orderBy($orderBy)->asArray()->all(), $idField, $nameField);
        }
    }
    
    public function sendSMSCode()
    {
        $result = true;
        $phone = $this->phone;
        if(empty($phone)){
            return;
        }
        $phone = preg_replace('/[+()\s]/', '', $phone, -1);
        if(empty($this->sms_code) || (!empty($this->sms_code) && ($this->sms_auth_exp < date('Y-m-d H:i:s')))){
            $code = FSMSMSCode::generate(4);
            if($result = FSMSMSCode::send($code, $phone)){
                $this->sms_code = $code;
                $this->sms_auth_exp = date('Y-m-d H:i:s', time()+3600); // +1hour
            }else{
                $this->sms_code = null;
                $this->sms_auth_exp = null;
            }
            $this->save();
        }
        return $result;
    }

}
