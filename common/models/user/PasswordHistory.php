<?php

namespace common\models\user;

use Yii;

use common\models\user\FSMUser;

/**
 * This is the model class for table "password_history".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $password_hash
 * @property string $create_date
 *
 * @property FSMUser $user
 */
class PasswordHistory extends \common\models\mainclass\FSMBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'password_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'password_hash', 'create_date'], 'required'],
            [['user_id'], 'integer'],
            [['create_date'], 'safe'],
            [['password_hash'], 'string', 'max' => 60],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('app', 'Password history|Password histories', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User Id',
            'password_hash' => 'Password hash',
            'create_date' => 'Create date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'user_id']);
    }
}