<?php

namespace common\models\event\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;

use common\models\event\Event;

/**
 * EventSearch represents the model behind the search form of `common\models\event\Event`.
 */
class EventSearch extends Event
{
    public $create_time_range;
    public $user_name;    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'abonent_id', 'parent_event_id', 'client_id', 'manager_id', 'send_email', 
                'deleted', 'remind_day', 'create_user_id', 'update_user_id', 'periodical_day'], 'integer'],
            [['external_id', 'event_type', 'status', 'address', 
                'event_date', 'event_time', 'event_datetime',
                'event_date_finish', 'event_time_finish', 'event_datetime_finish',
                'next_event_type', 'next_date', 'next_time', 'next_datetime', 
                'remind_hours', 'comment', 'result_comment', 
                'name', 'create_time', 'update_time',
            	'manager_name', 'create_time_range', 'user_name',
                'periodicity', 'periodical_last_date', 'periodical_next_date', 'periodical_finish_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
    
        $query = Event::find(true);

        $query->select = [
            $this->tableName() . '.*',
            'manager_name' => 'profile'.'.name',
            'user_name' => 'user_profile.name',
        ];
        
        $query->leftJoin(['profile' => 'profile'], 'profile.id = '.$this->tableName().'.manager_id');
        $query->leftJoin(['user_profile' => 'profile'], 'user_profile.user_id = '.$baseTableName.'.create_user_id');
        
        if (!isset($params['sort'])) {
            $query->addOrderBy('id desc');
        }
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if(!empty($this->create_time) && strpos($this->create_time, '-') !== false) { 
            $this->create_time_range = $this->create_time;
            list($start_date, $end_date) = explode(' - ', $this->create_time_range); 
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));
            $query->andFilterWhere(['between', $baseTableName . '.create_time', $start_date, $end_date.' 23:59:59']);
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            $baseTableName.'.id' => $this->id,
	    $baseTableName.'.abonent_id' => $this->abonent_id,
            $baseTableName.'.name' => $this->name,
            $baseTableName.'.address' => $this->address,
            $baseTableName.'.parent_event_id' => $this->parent_event_id,
            $baseTableName.'.client_id' => $this->client_id,
            $baseTableName.'.manager_id' => $this->manager_id,
            $baseTableName.'.event_date' => !empty($this->event_date) ? date('Y-m-d', strtotime($this->event_date)) : null,
            $baseTableName.'.event_time' => $this->event_time,
            $baseTableName.'.event_datetime' => $this->event_datetime,
            $baseTableName.'.event_date_finish' => !empty($this->event_date_finish) ? date('Y-m-d', strtotime($this->event_date_finish)) : null,
            $baseTableName.'.event_time_finish' => $this->event_time_finish,
            $baseTableName.'.event_datetime_finish' => $this->event_datetime_finish,
            $baseTableName.'.next_date' => !empty($this->next_date) ? date('Y-m-d', strtotime($this->next_date)) : null,
            $baseTableName.'.next_time' => $this->next_time,
            $baseTableName.'.next_datetime' => $this->next_datetime,
            $baseTableName.'.remind_day' => $this->remind_day,
            $baseTableName.'.remind_hours' => $this->remind_hours,
            $baseTableName.'.send_email' => $this->send_email,
            $baseTableName.'.periodical_day' => $this->periodical_day,
            $baseTableName.'.periodical_last_date' => !empty($this->periodical_last_date) ? date('Y-m-d', strtotime($this->periodical_last_date)) : null,
            $baseTableName.'.periodical_next_date' => !empty($this->periodical_next_date) ? date('Y-m-d', strtotime($this->periodical_next_date)) : null,
            $baseTableName.'.periodical_finish_date' => !empty($this->periodical_finish_date) ? date('Y-m-d', strtotime($this->periodical_finish_date)) : null,
            $baseTableName.'.deleted' => $this->deleted,
            $baseTableName.'.create_time' => !empty($this->create_time) && empty($this->create_time_range) ? date('Y-m-d', strtotime($this->create_time)) : null,
            $baseTableName.'.create_user_id' => $this->create_user_id,
            $baseTableName.'.update_time' => $this->update_time,
            $baseTableName.'.update_user_id' => $this->update_user_id,
        ]);

        $query->andFilterWhere(['like', $baseTableName.'.external_id', $this->external_id])
            ->andFilterWhere(['like', $baseTableName.'.event_type', $this->event_type])
            ->andFilterWhere(['like', $baseTableName.'.status', $this->status])
            ->andFilterWhere(['like', $baseTableName.'.next_event_type', $this->next_event_type])
            ->andFilterWhere(['like', $baseTableName.'.comment', $this->comment])
            ->andFilterWhere(['like', $baseTableName.'.result_comment', $this->result_comment])
            ->andFilterWhere(['like', $baseTableName.'.periodicity', $this->periodicity]);

        /*
        SELECT 
            `event`.*, profile.`name` AS `manager_name`
        FROM
            `event`
                LEFT JOIN
            `profile` profile ON profile.id = event.manager_id
        WHERE
            (`event`.`abonent_id` = 1)
            AND ((`event`.`client_id` = 3)
            AND (`event`.`deleted` = 0))
        ORDER BY `id` DESC
         * 
         */
        //return $query->all();
        return $dataProvider;
    }
    
    public function searchPeriodical($execDate = null)
    {
        $execDate = ($execDate ?? date('Y-m-d'));
        $baseTableName = $this->tableName();
        $query = $this->find()
            ->andWhere(['<=', $baseTableName.'.periodical_next_date', $execDate])
            ->andWhere(['or',
                ['>=', $baseTableName.'.periodical_finish_date', $execDate],
                [$baseTableName.'.periodical_finish_date' => null]
            ]);
        return $query->all();
    }
}