<?php

namespace common\models\event;

use Yii;

use common\models\client\ClientContact;

/**
 * This is the model class for table "event_contact".
 *
 * @property integer $id
 * @property integer $event_id
 * @property integer $contact_id
 * @property string $name
 * @property string $short_name
 * @property string $accepted_at
 * @property integer $refused_at
 * @property string $comment
 *
 * @property Event $event
 * @property ClientContact $clientContact
 */
class EventContact extends \common\models\mainclass\FSMBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_contact';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id'], 'required'],
            [['event_id', 'contact_id'], 'integer'],
            [['accepted_at', 'refused_at'], 'safe'],
            [['comment'], 'string'],
            [['name'], 'string', 'max' => 100],
            [['short_name'], 'string', 'max' => 5],
            [['event_id', 'name'], 'unique', 'targetAttribute' => ['event_id', 'name']],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::class, 'targetAttribute' => ['event_id' => 'id']],
            [['contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => ClientContact::class, 'targetAttribute' => ['contact_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('event', 'External participant|External participants', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'event_id' => Yii::t('event', 'Event'),
            'contact_id' => Yii::t('event', 'Client contact'),
            'name' => Yii::t('common', 'Name'),
            'short_name' => Yii::t('event', 'Short name'),
            'accepted_at' => Yii::t('event', 'Accepted at'),
            'refused_at' => Yii::t('event', 'Refused at'),
            'comment' => Yii::t('common', 'Comment'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::class, ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientContact()
    {
        return $this->hasOne(ClientContact::class, ['id' => 'contact_id']);
    }    
    
    public function getClientContactName()
    {
        return !empty($this->name) ? 
            $this->name : 
            (
                isset($this->clientContact) ? ($this->clientContact->first_name.' '.$this->clientContact->last_name) : ''
            );
    }  
    
    public function getClientContactNamePosition()
    {
        return !empty($this->name) ? 
            $this->name : 
            (isset($this->clientContact) ? 
                (
                    $this->clientContact->first_name.' '.$this->clientContact->last_name.
                    (!empty($this->clientContact->position_id) ? ' ( ' . $this->clientContact->position->name . ' )' : '')
                ) : ''
            );
    }    
}