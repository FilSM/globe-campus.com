<?php

namespace common\models\event;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;
use yii\helpers\Url;
use yii\base\Exception;

use common\components\FSMGoogleCalendarApi;
use common\components\FSMToken;
use common\models\abonent\Abonent;
use common\models\user\FSMUser;
use common\models\user\FSMProfile;
use common\models\client\Client;

/**
 * This is the model class for table "event".
 *
 * @property integer $id
 * @property integer $abonent_id 
 * @property string $external_id
 * @property string $name
 * @property integer $parent_event_id
 * @property integer $client_id
 * @property integer $manager_id
 * @property string $event_type
 * @property string $event_type_other
 * @property string $status
 * @property string $address
 * @property string $event_date
 * @property string $event_time
 * @property string $event_datetime
 * @property string $event_date_finish
 * @property string $event_time_finish
 * @property string $event_datetime_finish
 * @property string $next_event_type
 * @property string $next_date
 * @property string $next_time
 * @property string $next_datetime
 * @property integer $remind_day
 * @property string $remind_hours
 * @property string $remind_datetime
 * @property string $sent_remind_at
 * @property integer $send_email
 * @property string $comment
 * @property string $result_comment
 * @property string $periodicity
 * @property integer $periodical_day
 * @property string $periodical_last_date
 * @property string $periodical_next_date
 * @property string $periodical_finish_date
 * @property integer $deleted
 * @property string $create_time
 * @property integer $create_user_id
 * @property string $update_time
 * @property integer $update_user_id
 *
 * @property Abonent $abonent
 * @property Client $client
 * @property User $createUser
 * @property Profile $manager
 * @property Event $parentEvent
 * @property Event[] $events
 * @property FSMUser $updateUser
 * @property EventProfile[] $eventProfiles
 */
class Event extends \common\models\mainclass\FSMCreateUpdateModel
{
    const EVENT_RESULT_NEGATIVE = 'result-negative';
    const EVENT_RESULT_LAST = 'result-last';
    
    const EVENT_TYPE_MEETING = 'meeting';
    const EVENT_TYPE_CALL = 'call';
    const EVENT_TYPE_EMAIL = 'email';
    const EVENT_TYPE_SMS = 'sms';
    const EVENT_TYPE_TASK = 'task';
    const EVENT_TYPE_BUS_TRIP = 'bus_trip';
    const EVENT_TYPE_VACATION = 'vacation';
    const EVENT_TYPE_OTHER = 'other';
    
    const EVENT_STATUS_PLANNED = 'planned';
    const EVENT_STATUS_POSITIVE = 'positive';
    const EVENT_STATUS_NEGATIVE = 'negative';
    const EVENT_STATUS_LAST = 'last';
    
    const TOKEN_TYPE_NEW_EVENT = 10;
    
    const PERIODICITY_NONE = 'none';
    const PERIODICITY_DAY = 'day';
    const PERIODICITY_WEEK = 'week';
    const PERIODICITY_MONTH = 'month';
    const PERIODICITY_YEAR = 'year';
    
    protected $_externalFields = [
        'manager_name',
        'user_name', 
    ];
    
    public function init() {
        parent::init();
        $this->cascadeDeleting = true;
    }

    public static function find($withScope = false) {
        if ((Yii::$app->id != 'app-console') && $withScope) {
            $scope = new \common\scopes\AbonentFieldScopeQuery(get_called_class());
            return $scope->onlyAuthor();            
        }
        return parent::find();
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['abonent_id', 'manager_id', 'event_datetime'], 'required'],
            ['periodical_day', 'required', 'when' => function($model) {
                return !in_array($model->periodicity, [
                    self::PERIODICITY_NONE,
                    self::PERIODICITY_DAY,
                ]);
            }],
            //['periodical_day', 'compare', 'compareValue' => 8, 'operator' => '<', 'message' => Yii::t('event', 'The ordinal day of the week cannot be greater than 7')],
            ['periodical_day', 'compare', 'compareValue' => 8, 'operator' => '<', 'when' => function($model) {
                    return ($model->periodicity == self::PERIODICITY_WEEK);
                }, 
                'message' => Yii::t('event', 'The ordinal day of the week cannot be greater than 7')
            ],
            [['abonent_id', 'parent_event_id', 'client_id', 'manager_id', 'remind_day', 'periodical_day', 
                'send_email', 'deleted', 'create_user_id', 'update_user_id'], 'integer'],
            [['name', 'address'], 'string', 'max' => 255],
            [['event_type_other'], 'string', 'max' => 50],
            [['external_id'], 'string', 'max' => 100],
            [['event_type', 'status', 'next_event_type', 'comment', 'result_comment', 'periodicity'], 'string'],
            [['event_date', 'event_time', 'event_datetime', 
                'event_date_finish', 'event_time_finish', 'event_datetime_finish', 
                'next_date', 'next_time', 'next_datetime', 
                'remind_hours', 'remind_datetime', 'sent_remind_at',
                'periodical_last_date', 'periodical_next_date', 'periodical_finish_date', 'create_time', 'update_time'], 'safe'],
            [['manager_id'], 'validateGoogleCalendarId'],
            [['abonent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Abonent::class, 'targetAttribute' => ['abonent_id' => 'id']],   
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['client_id' => 'id']],
            [['manager_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMProfile::class, 'targetAttribute' => ['manager_id' => 'id']],
            [['parent_event_id'], 'exist', 'skipOnError' => true, 'targetClass' => self::class, 'targetAttribute' => ['parent_event_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['create_user_id' => 'id']],
            [['update_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['update_user_id' => 'id']],
        ];
    }

    public function validateGoogleCalendarId($attribute, $params, $validator)
    {
        if(!empty(Yii::$app->params['ENABLE_GOOGLE_CALENDAR'])){
            Url::remember(Url::current(['manager_id' => $this->$attribute]), get_class(Yii::$app->controller).'_google-api');
            $redirectUrl = Url::to(['/google-api/auth'], true);
            $calendarData = self::getGoogleCalendarData($this->$attribute);
            if(!empty($calendarData['calendarId'])){
                $googleApi = new FSMGoogleCalendarApi($calendarData['username'], $calendarData['calendarId'], $redirectUrl);
                if (!$googleApi->checkIfCredentialFileExists()) {
                    return Yii::$app->response->redirect(['/event/google-auth', 'profile_id' => $this->$attribute]);
                }
            }
        }else{
            
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('event', 'Event|Events', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'abonent_id' => Yii::t('client', 'Abonent'),
            'external_id' => Yii::t('event', 'External Id'),
            'name' => Yii::t('event', 'Description'),
            'parent_event_id' => Yii::t('event', 'Previous event'),
            'client_id' => Yii::t('common', 'Client'),
            'manager_id' => Yii::t('common', 'Performer'),
            'event_type' => Yii::t('event', 'Event type'),
            'event_type_other' => Yii::t('event', 'Other event type'),
            'status' => Yii::t('common', 'Status'),
            'address' => Yii::t('event', 'Address'),
            'event_date' => Yii::t('event', 'Event Date (start)'),
            'event_time' => Yii::t('event', 'Event Time (start)'),
            'event_datetime' => Yii::t('event', 'Event date & time (start)'),
            'event_date_finish' => Yii::t('event', 'Event Date (finish)'),
            'event_time_finish' => Yii::t('event', 'Event Time (finish)'),
            'event_datetime_finish' => Yii::t('event', 'Event date & time (finish)'),
            'next_event_type' => Yii::t('event', 'Next event type'),
            'next_date' => Yii::t('event', 'Next date'),
            'next_time' => Yii::t('event', 'Next time'),
            'next_datetime' => Yii::t('event', 'Next date & time'),
            'remind_day' => Yii::t('event', 'Remind (days)'),
            'remind_hours' => Yii::t('event', 'Remind (hours)'),
            'remind_datetime' => Yii::t('event', 'Remind date & time'),
            'sent_remind_at' => Yii::t('event', 'Remind sent at'),
            'send_email' => Yii::t('event', 'Send notification'),
            'comment' => Yii::t('common', 'Comment'),
            'periodicity' => Yii::t('event', 'Periodicity'),
            'periodical_day' => Yii::t('event', 'Generation day'),
            'periodical_last_date' => Yii::t('event', 'Last event generation date'),
            'periodical_next_date' => Yii::t('event', 'Next event generation date'),
            'periodical_finish_date' => Yii::t('event', 'Event generation finish date'),
            'result_comment' => Yii::t('event', 'Result comment'),
            'deleted' => Yii::t('common', 'Deleted'),
            'create_time' => Yii::t('common', 'Created at'), 
            'create_user_id' => Yii::t('common', 'Author'), 
            'update_time' => Yii::t('event', 'Update Time'),
            'update_user_id' => Yii::t('event', 'Update User ID'),
            
            'manager_name' => Yii::t('event', 'Participant name'),
        ];
    }

    protected function getIgnoredFieldsForDelete() {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields,
            ['abonent_id', 'manager_id']
        );
        return $fields;
    } 
    
    /**
     * @return array of relations to other model
     */
    protected function getRelations() {
        $relations = parent::getRelations();
        if(isset($relations['ParentEvent'])){
            $relations['NextEvent'] = $relations['ParentEvent'];
            unset($relations['ParentEvent']);
        }
        return $relations;        
    }   
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonent() {
        return $this->hasOne(Abonent::class, ['id' => 'abonent_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::class, ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(FSMProfile::class, ['id' => 'manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentEvent()
    {
        return $this->hasOne(self::class, ['id' => 'parent_event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(self::class, ['parent_event_id' => 'id'])->notDeleted();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreviousEvent() {
        return $this->hasOne(self::class, ['id' => 'parent_event_id'], true)->notDeleted();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNextEvent() {
        return $this->hasOne(self::class, ['parent_event_id' => 'id'], true)->notDeleted();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventProfiles()
    {
        return $this->hasMany(EventProfile::class, ['event_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventContacts()
    {
        return $this->hasMany(EventContact::class, ['event_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventResurs()
    {
        return $this->hasMany(EventEventResurs::class, ['event_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventEventResurs()
    {
        return $this->getEventResurs();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'update_user_id']);
    }
    
    static public function getEventTypeList() {
        return [
            self::EVENT_TYPE_MEETING => Yii::t('client', 'Meeting'),
            self::EVENT_TYPE_CALL => Yii::t('client', 'Call'),
            self::EVENT_TYPE_EMAIL => Yii::t('client', 'Email'),
            self::EVENT_TYPE_SMS => Yii::t('client', 'SMS'),
            self::EVENT_TYPE_TASK => Yii::t('client', 'Task'),
            self::EVENT_TYPE_BUS_TRIP => Yii::t('client', 'Business trip'),
            self::EVENT_TYPE_VACATION => Yii::t('client', 'Vacation'),
            self::EVENT_TYPE_OTHER => Yii::t('client', 'Other'),
        ];
    }
        
    static public function getEventStatusList() {
        return [
            self::EVENT_STATUS_PLANNED => Yii::t('client', 'Planned'),
            self::EVENT_STATUS_POSITIVE => Yii::t('client', 'Positive'),
            self::EVENT_STATUS_NEGATIVE => Yii::t('client', 'Negative'),
            self::EVENT_STATUS_LAST => Yii::t('client', 'Last event'),
        ];
    }
    
    static public  function getPeriodicityList()
    {
        return [
            self::PERIODICITY_NONE => Yii::t('common', 'None'),
            self::PERIODICITY_DAY => Yii::t('common', 'Daily'),
            self::PERIODICITY_WEEK => Yii::t('common', 'Weekly'),
            self::PERIODICITY_MONTH => Yii::t('common', 'Monthly'),
            self::PERIODICITY_YEAR => Yii::t('common', 'Annually'),
        ];
    } 

    public function canUpdate() {
        if(!parent::canUpdate()){
            return;
        }
        if(FSMUser::getIsPortalAdmin()){
            return true;
        }        
        $user = Yii::$app->user->identity;
        if(!isset($user) || ($user->profile->id != $this->manager_id)){
            return;
        }        
        return true;
        //return in_array($this->status, [self::EVENT_STATUS_PLANNED]);
    }

    public function canDelete() {
        if(!parent::canDelete()){
            return;
        }
        return $this->canUpdate();
    }
    
    public function beforeSave($insert) {
        if(!parent::beforeSave($insert)){
            return;
        }
        
        $dirtyAttributes = $this->getDirtyAttributes();
        if(isset($dirtyAttributes['periodicity'])){
            $this->periodical_last_date = null;
        }
        
        if(($this->periodicity == self::PERIODICITY_NONE) || 
            (!empty($this->periodical_finish_date) && ($this->periodical_finish_date == date('Y-m-d')))
        ){
            $this->periodical_day = null;
            $this->periodical_last_date = null;
            $this->periodical_next_date = null;
            $this->periodical_finish_date = null;
        }elseif($this->periodicity != self::PERIODICITY_NONE){
            switch ($this->periodicity) {
                case self::PERIODICITY_DAY:
                case self::PERIODICITY_WEEK:
                case self::PERIODICITY_MONTH:
                case self::PERIODICITY_YEAR:
                    $strPeriodicity = '+1 '.$this->periodicity;
                    break;
                default:
                    $strPeriodicity = null;
                    break;
            }
            
            $lastDate = $this->periodical_last_date;
            
            if(empty($lastDate)){
                $lastDate = date('Y-m-'.str_pad($this->periodical_day, 2, "0", STR_PAD_LEFT));
                switch ($this->periodicity) {
                    case self::PERIODICITY_DAY:
                        $lastDate = date('Y-m-d', strtotime($this->event_datetime));
                        break;
                    case self::PERIODICITY_WEEK:
                        // Получаем номер текущего дня недели (1 - понедельник, 7 - воскресенье)
                        $dayOfWeek = date('N');
                        // Вычисляем разницу между текущим днем недели и указанным днем
                        $daysDiff = $dayOfWeek - $this->periodical_day;
                        $daysDiff = ($daysDiff > 0) ? "-$daysDiff days" : abs($daysDiff)." days";
                        // Получаем дату указанного дня недели
                        $lastDate = date('Y-m-d', strtotime($daysDiff));                        
                        break;
                    case self::PERIODICITY_MONTH:
                    case self::PERIODICITY_YEAR:
                        break;
                }
                if($lastDate >= date('Y-m-d')){
                    $strPeriodicity = '+0 day';
                }
            }
            
            $nextDate = new \DateTime($lastDate);
            $nextDate->modify($strPeriodicity);
            switch ($this->periodicity) {
                case self::PERIODICITY_DAY:
                case self::PERIODICITY_WEEK:
                    $nextDate = $nextDate->format('Y-m-d');
                    break;
                case self::PERIODICITY_MONTH:
                case self::PERIODICITY_YEAR:
                    $nextDate = $nextDate->format('Y-m-'.$this->periodical_day);
                    break;
            }
            
            $this->periodical_last_date = $lastDate;
            $this->periodical_next_date = $nextDate;
        }
            
        $this->periodical_last_date = !empty($this->periodical_last_date) ? date('Y-m-d', strtotime($this->periodical_last_date)) : null;
        $this->periodical_next_date = !empty($this->periodical_next_date) ? date('Y-m-d', strtotime($this->periodical_next_date)) : null;
        $this->periodical_finish_date = !empty($this->periodical_finish_date) ? date('Y-m-d', strtotime($this->periodical_finish_date)) : null;
        
        $dataToSave = Yii::$app->request->post();
        
        if(empty($dataToSave)){
            return true;
        }
        
        $this->event_date = date('Y-m-d', strtotime($this->event_datetime));
        $this->event_time = date('H:i', strtotime($this->event_datetime));
        $this->event_datetime = date('Y-m-d H:i', strtotime($this->event_datetime));
        $this->event_date_finish = !empty($this->event_datetime_finish) ? date('Y-m-d', strtotime($this->event_datetime_finish)) : null;
        $this->event_time_finish = !empty($this->event_datetime_finish) ? date('H:i', strtotime($this->event_datetime_finish)) : null;
        $this->event_datetime_finish = !empty($this->event_datetime_finish) ? date('Y-m-d H:i', strtotime($this->event_datetime_finish)) : null;
        
        if($this->status == self::EVENT_STATUS_POSITIVE){
            $this->next_date = !empty($this->next_datetime) ? date('Y-m-d', strtotime($this->next_datetime)) : null;
            $this->next_time = !empty($this->next_datetime) ? date('H:i', strtotime($this->next_datetime)) : null;
            $this->next_datetime = !empty($this->next_datetime) ? date('Y-m-d H:i', strtotime($this->next_datetime)) : null;
        } else {
            $this->next_event_type = null;
            $this->next_datetime = null;
            $this->next_date = null;
            $this->next_time = null;
        }
        
        if(!empty($this->remind_day) || !empty($this->remind_hours)){
            $seconds = !empty($this->remind_day) ? $this->remind_day * 86400 : 0;
            $parsed = date_parse($this->remind_hours);
            $hourSeconds = $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];
            $seconds += $hourSeconds;
            $eventDateTime = new \DateTime($this->event_datetime);
            $eventDateTime->sub(new \DateInterval("PT{$seconds}S"));
            $this->remind_datetime = $eventDateTime->format('Y-m-d H:i:s');
        }else{
            $this->remind_datetime = null;
        }
        
        return true;
    }
    
    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes) {
        if(($this->status == self::EVENT_STATUS_POSITIVE) && 
            (
                $insert || 
                (
                    !$insert &&
                    isset($changedAttributes['status']) && 
                    ($changedAttributes['status'] != self::EVENT_STATUS_POSITIVE)
                )
            )
        ){
            $userId = Yii::$app->user->getId();
            $event = Yii::createObject([
                'class' => self::class,
                'abonent_id' => $this->userAbonentId,
                'parent_event_id' => $this->id,
                'client_id' => $this->client_id,
                'manager_id' => $this->manager_id,
                'event_type' => $this->next_event_type,
                'status' => 'planned',
                'event_date' => date('Y-m-d', strtotime($this->next_datetime)),
                'event_time' => date('H:i:s', strtotime($this->next_datetime)),
                'event_datetime' => $this->next_datetime,
                'next_event_type' => null,
                'next_date' => null,
                'next_time' => null,
                'next_datetime' => null,
                'comment' => $this->result_comment,
                'result_comment' => null,
                'deleted' => false,
                'create_user_id' => $userId,
                'create_time' => new Expression('NOW()'),
                'update_user_id' => $userId,
                'update_time' => new Expression('NOW()'),
            ]);

            if (!$event->save(false)) {
                $message = Yii::t('user', $event->modelTitle() . ' not inserted due to non-specific error.');
                $message = $this->getErrorMessage($message);
                Yii::$app->getSession()->setFlash('error', Yii::t('user', $message));
            }
        }
        Yii\db\ActiveRecord::afterSave($insert, $changedAttributes);
    }
    
    public function sendEmailRemind($time_start = null)
    {
        $time_start = !empty($time_start) ? $time_start : time();
        $eventDatetime = date('Y-m-d H:i:s', $time_start);
        $eventList = self::find()->notDeleted()
            ->andWhere(['send_email' => 1])
            ->andWhere(['sent_remind_at' => null])
            ->andWhere(['<=', 'remind_datetime', $eventDatetime])
            ->all();
        $countSent = 0;
        foreach ($eventList as $event) {
            if($event->sendRemind()){
                $event->updateAttributes(['sent_remind_at' => date('Y-m-d H:i:s', $time_start)]);
                $countSent++;
            }
        }
        return $countSent;
    }
    
    public function sendRemind()
    {
        $contacts = Yii::$app->params['EMAILS_DEFAULT_CONTACT'];
        $logoPath = (!empty(Yii::$app->params['EMAILS_NOTIF_PRINT_LOGO']) ? Yii::getAlias('@common/assets/images/logo-print.png') : null);
        $logoImg = null;
        if (!empty($logoPath) && file_exists($logoPath)) {
            $logoImg = $logoPath;
        }         

        $eventType = $this->eventTypeList[$this->event_type];
        $mailContent = $eventType.' '.date('d-m-Y, H:i', strtotime($this->event_datetime)).(!empty($this->name) ? '<br/>'.$this->name : '');

        $eventProfileModel = $this->eventProfiles;
        foreach ($eventProfileModel as $eventProfile) {
            $user = $eventProfile->profile->user;
            if (!empty($user->email)) {
                $token = new FSMToken();
                $token->user_id = $user->id;
                $token->type = self::TOKEN_TYPE_NEW_EVENT;
                if (!$token->save(false)) {
                    throw new Exception('Can not save the Token data!');
                }

                $mailParams = [
                    'eventType' => $eventType,
                    'content' => $mailContent,
                    'contacts' => $contacts,
                    'logoImageFileName' => $logoImg,
                    'tokenUrl' => Url::to(['/event/accept', 'event_id' => $this->id, 'user_id' => $user->id, 'code' => $token->code], true),
                ];
                $mail = Yii::$app->mailer->compose('layouts/again-event', $mailParams)
                    ->setFrom(Yii::$app->params['EMAILS_DEFAULT_SENDER'])
                    ->setTo($user->email)
                    ->setSubject(Yii::$app->params['brandLabel'] . ' v.' . Yii::$app->params['brandVersion'] .
                        ' | New event: ' . $this->eventTypeList[$this->event_type] . ' ' . date('d-m-Y, H:i', strtotime($this->event_datetime)));
                Yii::$app->mailer->send($mail);
            }
        }

        return true;
    }

    public function sendNotification($recipientArr)
    {
        $contacts = Yii::$app->params['EMAILS_DEFAULT_CONTACT'];
        $logoPath = (!empty(Yii::$app->params['EMAILS_NOTIF_PRINT_LOGO']) ? Yii::getAlias('@common/assets/images/logo-print.png') : null);
        $logoImg = null;
        if (!empty($logoPath) && file_exists($logoPath)) {
            $logoImg = $logoPath;
        }
        
        $eventType = $this->eventTypeList[$this->event_type];
        $mailContent = $eventType.' '.date('d-m-Y, H:i', strtotime($this->event_datetime)).(!empty($this->name) ? '<br/>'.$this->name : '');        

        $mailDescription = [];
        if(!empty($this->address)){
            $mailDescription[] = 'Address: '.$this->address;
        }
        $eventProfileModel = $this->eventProfiles;
        if(!empty($eventProfileModel)){
            $participantList = [];
            foreach ($eventProfileModel as $eventProfile) {
                $short_name = $eventProfile->profile->short_name;
                $short_name = !empty($short_name) ? $short_name : $eventProfile->profile->name;
                if(!empty($short_name)){
                    $participantList[] = $short_name;
                }
            }                    
            if(!empty($participantList)){
                $mailDescription[] = 'Participants: '.implode(', ', $participantList);
            }
        }
        if(!empty($this->client_id)){
            $mailDescription[] = 'Client: '.$this->client->name;
        }
        if(!empty($addedIds)){
            $mailDescription[] = 'External participants: '. implode(', ', $addedIds);
        }
        if(!empty($this->comment)){
            $mailDescription[] = 'Comment: '. $this->comment;
        }                      
        $mailDescription = implode('<br/>', $mailDescription);
        
        try {
            foreach ($recipientArr as $recipient) {
                $user = FSMUser::findOne($recipient);
                if(!isset($user) || empty($user->email)){
                    continue;
                }
                $token = new FSMToken();
                $token->user_id = $user->id;
                $token->type = self::TOKEN_TYPE_NEW_EVENT;                                        
                if (!$token->save(false)) {
                    throw new Exception('Unable to save data!');
                }

                $mailParams = [
                    'eventType' => $eventType,
                    'content' => $mailContent,
                    'description' => $mailDescription,
                    'contacts' => $contacts,
                    'logoImageFileName' => $logoImg,
                    'tokenUrl' => Url::to(['/event/accept', 'event_id' => $this->id, 'user_id' => $user->id, 'code' => $token->code], true),
                ];
                $subject = Yii::$app->params['brandLabel'].' v.'.Yii::$app->params['brandVersion'].
                    ' | New event: '.$this->eventTypeList[$this->event_type] . ' ' . date('d-m-Y, H:i', strtotime($this->event_datetime));
                $mail = Yii::$app->mailer->compose('layouts/new-event', $mailParams)
                    ->setFrom(Yii::$app->params['EMAILS_DEFAULT_SENDER'])
                    ->setTo($user->email)
                    ->setSubject($subject);
                Yii::$app->mailer->send($mail);                            

            }
            $result = true;
        } catch (Exception $exc) {
            $result = false;
            echo $exc->getTraceAsString();
        } finally {
            return !empty($result);
        }
    }
    
    static public function getGoogleCalendarId($profile_id = null)
    {
        if(!empty($profile_id)){
            $profile = FSMProfile::findOne($profile_id);
        }else{
            $user = Yii::$app->user->identity;
            if(!isset($user)){
                return null;
            }
            $profile = $user->profile;
        }
        $calendarId = $profile->gcalendar_id;
        return !empty($calendarId) ? $calendarId : (!empty(Yii::$app->params['USE_COMMON_GOOGLE_CALENDAR']) ? Yii::$app->params['commonGoogleCalendarId'] : null);
    } 
    
    static public function getGoogleCalendarData($profile_id = null)
    {
        if(!empty($profile_id)){
            $profile = FSMProfile::findOne($profile_id);
        }else{
            $user = Yii::$app->user->identity;
            if(!isset($user)){
                return null;
            }
            $profile = $user->profile;
        }
        $calendarId = $profile->gcalendar_id;
        $data['calendarId'] = !empty($calendarId) ? $calendarId : (!empty(Yii::$app->params['USE_COMMON_GOOGLE_CALENDAR']) ? Yii::$app->params['commonGoogleCalendarId'] : null);
        $data['username'] = $profile->user->username;
        return $data;
    }
    
    public function buildGoogleCalendarEvent($googleApi, $eventProfileModel, $eventContactModel)
    {
        $event = [
            'summary' => $this->eventTypeList[$this->event_type],
            'start' => [
                'dateTime' => date('Y-m-d\TH:i:s', strtotime($this->event_datetime)),
                'timeZone' => Yii::$app->params['timezone'],
            ],
            'recurrence' => [],
        ];
        $event['location'] = !empty($this->address) ? $this->address : '';
        $event['description'] = 
            (!empty($this->name) ? $this->name : '').
            (!empty($this->comment) ? (!empty($this->name) ? '<br/>' : '').$this->comment : '');
        if(!empty($this->event_datetime_finish)){
            $event['end'] = [
                'dateTime' => date('Y-m-d\TH:i:s', strtotime($this->event_datetime_finish)),
                'timeZone' => Yii::$app->params['timezone'],
            ];
        }else{
            $event['end'] = [
                'dateTime' => date('Y-m-d\TH:i:s', strtotime($this->event_datetime)),
                'timeZone' => Yii::$app->params['timezone'],
            ];
        }
        $attendees = [];
        if (!empty($eventProfileModel)) {
            foreach ($eventProfileModel as $eventProfile) {
                $user = $eventProfile->profile->user;
                $attendees[] = [
                    'id' => $eventProfile->profile->id,
                    'displayName' => $eventProfile->profile->name,
                    'email' => $user->email,
                ];
            }
        }         
        if(!empty($eventContactModel)){
            foreach ($eventContactModel as $eventContact) {
                if(!empty($eventContact->contact_id)){
                    $clientContact = $eventContact->clientContact;
                    if(!empty($clientContact->email)){
                        $attendees[] = [
                            'displayName' => $eventContact->clientContactNamePosition,
                            'email' => $clientContact->email,
                        ];
                    }
                }
            }
        }                                
        $event['attendees'] = $attendees;
        if(!empty($this->remind_day) || !empty($this->remind_hours)){
            if(!empty($this->remind_day)){
                $event['reminders'] = [
                    'useDefault' => false,
                    'overrides' => [
                        [
                            'method' => 'email', 
                            'minutes' => ($this->remind_day * 24 * 60),
                        ],
                        [
                            'method' => 'popup', 
                            'minutes' => ($this->remind_day * 24 * 60),
                        ],
                    ],
                ];
            }else{
                sscanf($this->remind_hours, "%d:%d:%d", $hours, $minutes, $seconds);
                $event['reminders'] = [
                    'useDefault' => false,
                    'overrides' => [
                        [
                            'method' => 'email', 
                            'minutes' => ($hours * 60) + $minutes,
                        ],
                        [
                            'method' => 'popup', 
                            'minutes' => ($hours * 60) + $minutes,
                        ],
                    ]
                ];
            }
        }else{
            $event['reminders'] = [
                'useDefault' => true,
            ];
        }                                
        
        return $event;
    }
    
    public function generateEvent($execDate = null)
    {
        $execDate = $execDate ?? date('Y-m-d');
        
        $searchModel = new search\EventSearch();
        $eventList = $searchModel->searchPeriodical($execDate);
        if(count($eventList) == 0){
            return 0;
        }
        
        $totalGenerated = 0;
        foreach ($eventList as $event) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $datetime1 = new \DateTime($event->event_datetime);
                $datetime2 = new \DateTime($event->event_datetime_finish);
                $timestamp1 = $datetime1->getTimestamp();
                $timestamp2 = $datetime2->getTimestamp();
                $diffInSeconds = abs($timestamp2 - $timestamp1);
                
                $startDatetime = $execDate.' '.date('H:i', strtotime($event->event_datetime));
                $finishDatetime = new \DateTime($startDatetime);
                $finishDatetime->modify("+$diffInSeconds seconds");                

                $newEvent = $event->cloneSelfOnce([
                    'event_datetime' => $startDatetime,
                    'event_datetime_finish' => $finishDatetime->format('Y-m-d H:i:s'),
                    'periodicity' => self::PERIODICITY_NONE,
                    'create_time' => date('Y-m-d H:i'),
                ], true);
                
                $event->periodical_last_date = $execDate;
                $event->save(false);
                
                $totalGenerated++;
            
                $transaction->commit();
            } catch (Exception | \yii\base\ErrorException | \Throwable $e) {
                $transaction->rollBack();
                $message = $e->getMessage();
                Yii::error($message, __METHOD__);
                if(Yii::$app->id == 'app-console') {
                    echo PHP_EOL.date('Y-m-d H:i:s').': "generateEvent" | '.$message;
                }else{
                    Yii::$app->getSession()->setFlash('error', $message);
                }
            }
        }
        
        return $totalGenerated;
    }
}