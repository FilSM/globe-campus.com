<?php

namespace common\models\event;

use Yii;

use common\models\user\FSMProfile;

/**
 * This is the model class for table "event_profile".
 *
 * @property integer $id
 * @property integer $event_id
 * @property integer $profile_id
 * @property integer $accepted_at
 * @property integer $refused_at
 * @property string $comment
 * @property Event $event
 * @property Profile $profile
 */
class EventProfile extends \common\models\mainclass\FSMBaseModel
{
    
    protected $_externalFields = [
        'manager_name', 
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'profile_id'], 'required'],
            [['event_id', 'profile_id'], 'integer'],
            [['comment'], 'string'],
            [['accepted_at', 'refused_at'], 'safe'],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::class, 'targetAttribute' => ['event_id' => 'id']],
            [['profile_id'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\user\FSMProfile::class, 'targetAttribute' => ['profile_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('event', 'Participant|Participants', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => Yii::t('event', 'Event'),
            'profile_id' => Yii::t('event', 'Participant'),
            'accepted_at' => Yii::t('event', 'Accepted at'),
            'refused_at' => Yii::t('event', 'Refused at'),
            'comment' => Yii::t('common', 'Comment'),
            
            'manager_name' => Yii::t('event', 'Participant name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::class, ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(FSMProfile::class, ['id' => 'profile_id']);
    }
}