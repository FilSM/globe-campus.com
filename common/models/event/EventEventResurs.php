<?php

namespace common\models\event;

use Yii;

/**
 * This is the model class for table "event_event_resurs".
 *
 * @property integer $id
 * @property integer $event_id
 * @property integer $event_resurs_id
 *
 * @property Event $event
 * @property EventResurs $eventResurs
 */
class EventEventResurs extends \common\models\mainclass\FSMBaseModel
{
    public $resurs_name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_event_resurs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'event_resurs_id'], 'required'],
            [['event_id', 'event_resurs_id'], 'integer'],
            [['event_id', 'event_resurs_id'], 'unique', 'targetAttribute' => ['event_id', 'event_resurs_id']],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::class, 'targetAttribute' => ['event_id' => 'id']],
            [['event_resurs_id'], 'exist', 'skipOnError' => true, 'targetClass' => EventResurs::class, 'targetAttribute' => ['event_resurs_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('event', 'Event resurs|Event resurs', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'event_id' => Yii::t('event', 'Event ID'),
            'event_resurs_id' => Yii::t('event', 'Event resurs'),
            'resurs_name'=> Yii::t('event', 'Resurs name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::class, ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventResurs()
    {
        return $this->hasOne(EventResurs::class, ['id' => 'event_resurs_id']);
    }
}