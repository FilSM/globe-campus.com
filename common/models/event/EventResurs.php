<?php

namespace common\models\event;

use Yii;

/**
 * This is the model class for table "event_resurs".
 *
 * @property integer $id
 * @property string $name
 * @property string $color_code
 *
 * @property EventEventResurs[] $eventEventResurs
 * @property Event[] $events
 */
class EventResurs extends \common\models\mainclass\FSMBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_resurs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 64],
            [['color_code'], 'string', 'max' => 7],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('event', 'Event resurs|Event resurs', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Name'),
            'color_code' => Yii::t('common', 'Color'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventEventResurs()
    {
        return $this->hasMany(EventEventResurs::class, ['event_resurs_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::class, ['id' => 'event_id'])
            ->viaTable('event_event_resurs', ['event_resurs_id' => 'id'])
            ->notDeleted();
    }
}