<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Valuta;

/**
 * ValutaSearch represents the model behind the search form about `common\models\Valuta`.
 */
class ValutaSearch extends Valuta
{
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'int_0', 'int_1', 'int_2', 'floor_0', 'floor_1', 'floor_2'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Valuta::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if(!isset($params['sort'])){
            $query->addOrderBy('id desc');
        }
        
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', $this->tableName().'.name', $this->name])
            ->andFilterWhere(['like', $this->tableName().'.int_0', $this->int_0])
            ->andFilterWhere(['like', $this->tableName().'.int_1', $this->int_1])
            ->andFilterWhere(['like', $this->tableName().'.int_2', $this->int_2])
            ->andFilterWhere(['like', $this->tableName().'.floor_0', $this->floor_0])
            ->andFilterWhere(['like', $this->tableName().'.floor_1', $this->floor_1])
            ->andFilterWhere(['like', $this->tableName().'.floor_2', $this->floor_2]);        

        return $dataProvider;
    }
}