<?php

namespace common\models\mainclass;

use Yii;
use yii\helpers\Url;
use yii\base\Exception;
use yii\base\ModelEvent;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;

use kartik\helpers\Html;

use common\components\FSMHelper;
use common\models\user\FSMUser;

class FSMBaseModel extends \yii\db\ActiveRecord
{

    const EVENT_BEFORE_MARK_AS_DELETED = 'beforeMarkAsDeleted';

    private $_cascadeDeleting = false;
    protected $_externalFields = [];
    public $_initDefaultValues = true;
    public $db = 'db';
    public $clone = false;
    public static $keyField = 'id';
    public static $nameField = 'name';
    public static $translationsSourceLanguage;
    public static $goBackUrl;

    public function init()
    {
        if ($this->_initDefaultValues) {
            $tableName = static::getDb()->getSchema()->getRawTableName($this->tableName());
            if (!in_array($tableName, [
                'fsmbase_model',
                'fsmcreate_model',
                'fsmcreate_update_model',
                'fsmcrudmodel',
                'fsmcr_udmodel',
                'fsmversion_model',
                'fsmtranslated_model',
                'location_base_model',
                'lang_model',
            ])) {
                $this->loadDefaultValues();
            }
        }
        
        if(!isset(self::$translationsSourceLanguage)){
            self::$translationsSourceLanguage = Yii::$app->i18n->getMessageSource('*')->sourceLanguage;
        }
        parent::init();
    }

    public static function modelTitle($n = 1, $translate = true)
    {
        $class = get_called_class();
        $method = __FUNCTION__;
        throw new Exception("Method \"{$class}::{$method}()\" is not implemented");
    }

    public static function label($category, $message, $n = 1, $translate = true)
    {
        if (strpos($message, '|') !== false) {
            $chunks = explode('|', $message);
            $message = $chunks[$n - 1];
        }
        return $translate ? Yii::t($category, $message) : $message;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors = ArrayHelper::merge(
            $behaviors, [
                'baseModelBehavior' => [
                    'class' => \common\behaviors\FSMBaseModelBehavior::class,
                ],
            ]
        );
        return $behaviors;
    }

    public function canCreate()
    {
        return !Yii::$app->user->isGuest;
    }

    public function canView()
    {
        return !Yii::$app->user->isGuest;
    }

    public function canUpdate()
    {
        return !Yii::$app->user->isGuest;
    }

    public function canDelete()
    {
        return !Yii::$app->user->isGuest;
    }

    public function getErrorMessage($defaultMessage = 'Undefined error!')
    {
        if ($this->hasErrors()) {
            $message = [];
            foreach ($this->getErrors() as $attribute) {
                foreach ($attribute as $error) {
                    $message[] = $error;
                }
            }
            $message = implode(PHP_EOL, $message);
        } else {
            $message = $defaultMessage;
        }
        return $message;
    }

    public function clearDefaultValues()
    {
        foreach ($this->getAttributes() as $attribute => $value) {
            if (isset($value)) {
                $this->__unset($attribute);
            }
        }
    }

    public function clearModel(array $without = [])
    {
        try {
            $attributeList = $this->getAttributes();
            foreach ($attributeList as $attribute => $value) {
                if(in_array($attribute, $without) || 
                    in_array($attribute, $this->externalFields)
                ){
                    continue;
                }
                
                $field = $this->getTableSchema()->columns[$attribute];
                if($field->autoIncrement ||
                    $field->isPrimaryKey
                ){
                    continue;
                }
                
                if (isset($value)) {
                    if($field->allowNull){
                        $this->updateAttributes([$attribute => null]);
                    }else{
                        $typeArr = explode('(', $field->dbType);
                        $type = strtoupper($typeArr[0]);
                        switch ($type) {
                            case 'DATE':
                            case 'TIMESTAMP':
                                $newValue = '1970-01-01';
                                break;
                            case 'INT':
                            case 'INTEGER':
                            case 'NUMBER':
                            case 'FLOAT':
                            case 'DOUBLE':
                                $newValue = 0;
                                break;
                            case 'ENUM':
                                break;
                            default:
                                $newValue = 'xxxxx';
                                break;
                        }
                        $this->updateAttributes([$attribute => $newValue]);
                    }
                }
            }
        } catch (Exception | \yii\base\ErrorException | \Throwable $e) {
            $message = $e->getMessage();
            Yii::error($message, __METHOD__);
            if ((Yii::$app->id != 'app-console')) {
                Yii::$app->getSession()->addFlash('error', $message);
            }
        }
    }

    public function clearAttributes(array $attributesToClear = [])
    {
        foreach ($attributesToClear as $attribute => $value) {
            if(in_array($attribute, $this->externalFields)){
                continue;
            }

            $field = $this->getTableSchema()->columns[$attribute];
            if($field->autoIncrement ||
                $field->isPrimaryKey
            ){
                continue;
            }

            if (isset($value)) {
                if($field->allowNull){
                    $this->__unset($attribute);
                }else{
                    $typeArr = explode('(', $field->dbType);
                    $type = strtoupper($typeArr[0]);
                    switch ($type) {
                        case 'DATE':
                        case 'TIMESTAMP':
                            $newValue = '1970-01-01';
                            break;
                        case 'INT':
                        case 'INTEGER':
                        case 'NUMBER':
                        case 'FLOAT':
                        case 'DOUBLE':
                            $newValue = 0;
                            break;
                        case 'ENUM':
                            break;
                        default:
                            $newValue = 'xxxxx';
                            break;
                    }
                    $this->$attribute = $newValue;
                }
            }
        }
    }

    public function attributes($withExternal = true)
    {
        // add related fields to searchable attributes 
        if ($withExternal) {
            return ArrayHelper::merge(parent::attributes(), $this->getExternalFields());
        } else {
            return parent::attributes();
        }
    }

    public function getDirtyAttributes($names = null)
    {
        $attributes = parent::getDirtyAttributes($names);
        if (!empty($attributes)) {
            $table = $this->getTableSchema();

            foreach ($attributes as $name => $value) {
                if (!isset($table->columns[$name])) {
                    unset($attributes[$name]);
                    continue;
                }
                if ($table->columns[$name]->allowNull && (is_null($value) OR ( $value === ''))) {
                    if (empty($this->oldAttributes[$name])) {
                        unset($attributes[$name]);
                    } else {
                        $attributes[$name] = null;
                    }
                } elseif (in_array($table->columns[$name]->type, ['tinyint']) && is_bool($value)) {
                    if (isset($this->oldAttributes[$name]) && ($value == $this->oldAttributes[$name])) {
                        unset($attributes[$name]);
                        continue;
                    }
                } elseif (in_array($table->columns[$name]->type, ['tinyint', 'smallint', 'integer']) && is_integer($value)) {
                    if (isset($this->oldAttributes[$name]) && ($value == $this->oldAttributes[$name])) {
                        unset($attributes[$name]);
                        continue;
                    }
                } elseif (in_array($table->columns[$name]->type, ['tinyint', 'smallint', 'integer', 'decimal', 'number', 'double', 'float', 'time', 'datetime']) && is_string($value)) {
                    if (isset($this->oldAttributes[$name]) && ($value == $this->oldAttributes[$name])) {
                        unset($attributes[$name]);
                        continue;
                    }
                    switch ($table->columns[$name]->type) {
                        case 'tinyint':
                        case 'smallint':
                        case 'integer':
                            $this->$name = intval($value);
                            break;
                        case 'decimal':
                        case 'number':
                            $this->$name = floatval(number_format(floatval($value), $table->columns[$name]->scale, '.', ''));
                            break;
                        case 'double':
                            $this->$name = doubleval($value);
                            break;
                        case 'float':
                            $this->$name = floatval($value);
                            break;
                        case 'time':
                            if ((strlen($value) == 5) &&
                                (
                                !isset($this->oldAttributes[$name]) ||
                                (isset($this->oldAttributes[$name]) && ($value == substr($this->oldAttributes[$name], 0, 5)))
                                )
                            ) {
                                $this->$name = $value . ':00';
                            }
                            break;
                        case 'datetime':
                            if ((strlen($value) == 16) &&
                                (
                                !isset($this->oldAttributes[$name]) ||
                                (isset($this->oldAttributes[$name]) && ($value == substr($this->oldAttributes[$name], 0, 16)))
                                )
                            ) {
                                $this->$name = $value . ':00';
                            }
                            break;
                        default:
                            break;
                    }
                } elseif (in_array($table->columns[$name]->type, ['decimal'])) {
                    $oldValue = isset($this->oldAttributes[$name]) ? number_format($this->oldAttributes[$name], $table->columns[$name]->scale, '.', '') : null;
                    $newValue = number_format($value, $table->columns[$name]->scale, '.', '');
                    if ($newValue == $oldValue) {
                        unset($attributes[$name]);
                    }
                } elseif (in_array($table->columns[$name]->type, ['double', 'float'])) {
                    $oldValue = isset($this->oldAttributes[$name]) ? strval($this->oldAttributes[$name]) : null;
                    if (strval($value) == $oldValue) {
                        unset($attributes[$name]);
                    }
                }
            }
        }
        return $attributes;
    }

    static public function getUserAbonentId()
    {
        $session = Yii::$app->session ?? null;
        $userAbonentId = !empty($session->id) ? $session->get('user_current_abonent_id') : null;
        if (empty($userAbonentId)) {
            $user = Yii::$app->user->identity ?? null;
            if (!($user instanceof FSMUser) || empty($user->id)) {
                return -1;
            }
            $clientList = method_exists('FSMUser', 'getClientsWithoutScope') ? $user->clientsWithoutScope : [];
            if (!empty($clientList)) {
                $abonentList = $clientList[0]->abonents;
                foreach ($abonentList as $abonent) {
                    $userAbonentId = $abonent->id;
                    Yii::$app->session->set('user_current_abonent_id', $userAbonentId);
                    break;
                }
            }
        }
        return $userAbonentId;
    }
    
    public function cloneAttributesTo(&$model, array $attributesToSet = [], array $attributesToClear = [], $forNew = true)
    {
        $model->setAttributes($this->attributes);
        foreach ($attributesToSet as $attribute => $value) {
            $model->$attribute = $value;
        }
        
        $model->clearAttributes($attributesToClear);
        
        if($forNew){
            foreach ($model->getPrimaryKey(true) as $field => $value) {
                $this->__unset($attribute);
            }
            $model->setIsNewRecord(true);
        }
        return $model;
    }
    
    public function cloneAttributesFrom($model, array $attributesToSet = [], array $attributesToClear = [], $forNew = true)
    {
        $this->setAttributes($model->attributes);
        foreach ($attributesToSet as $attribute => $value) {
            $this->$attribute = $value;
        }
        
        $this->clearAttributes($attributesToClear);
        
        if($forNew){
            foreach ($this->getPrimaryKey(true) as $attribute => $value) {
                $this->__unset($attribute);
            }
            $this->setIsNewRecord(true);
        }
        return true;
    }
    
    public function cloneModel($model, $count = 1, $withValidation = false)
    {
        if ($count == 0) {
            return;
        }

        $cloneArr = [];
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            
            for ($index = 1; $index <= $count; $index++) {
                $className = get_class($model);
                $clone = new $className;
                //$clone->scenario = $model->scenario;
                $clone->attributes = $model->attributes;
                foreach ($model->getPrimaryKey(true) as $field => $value) {
                    unset($clone->{$field});
                }                
                $clone->clone = true;
                if (!$clone->save($withValidation)) {
                    throw new Exception(Yii::t('common', 'Unable to save data!').' '.$clone->errorMessage);
                }
                $cloneArr[] = $clone;                
            }
                
            $transaction->commit();
        } catch (Exception | \yii\base\ErrorException | \Throwable $e) {
            $message = $e->getMessage();
            Yii::error($message, __METHOD__);
            $transaction->rollBack();
            if ((Yii::$app->id != 'app-console')) {
                Yii::$app->getSession()->addFlash('error', $message);
            }
            return;
        }
        
        return $cloneArr;
    }

    public function cloneSelf($count = 1, $withValidation = false)
    {
        if ($count == 0) {
            return;
        }
        
        $cloneArr = [];
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            
            for ($index = 1; $index <= $count; $index++) {
                $className = get_class($this);
                $clone = new $className;
                //$clone->scenario = $this->scenario;
                $clone->attributes = $this->attributes;
                foreach ($this->getPrimaryKey(true) as $field => $value) {
                    unset($clone->{$field});
                }                
                $clone->clone = true;
                if (!$clone->save($withValidation)) {
                    throw new Exception(Yii::t('common', 'Unable to save data!').' '.$clone->errorMessage);
                }
                $cloneArr[] = $clone;
            }
                
            $transaction->commit();
        } catch (Exception | \yii\base\ErrorException | \Throwable $e) {
            $message = $e->getMessage();
            Yii::error($message, __METHOD__);
            $transaction->rollBack();            
            if ((Yii::$app->id != 'app-console')) {
                Yii::$app->getSession()->addFlash('warning', $message);
            }
            return;
        }
        
        return $cloneArr;
    }

    public function cloneSelfOnce(array $attributes = [], $withValidation = false)
    {
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            $className = get_class($this);
            $clone = new $className;
            //$clone->scenario = $this->scenario;
            $clone->attributes = $this->attributes;
            foreach ($this->getPrimaryKey(true) as $field => $value) {
                unset($clone->{$field});
            }                
            foreach ($attributes as $key => $value) {
                $clone->$key = $value;
            }
            $clone->clone = true;
            if (!$clone->save($withValidation)) {
                throw new Exception(Yii::t('common', 'Unable to save data!').' '.$clone->errorMessage);
            }
            $transaction->commit();
        } catch (Exception | \yii\base\ErrorException | \Throwable $e) {
            $message = $e->getMessage();
            Yii::error($message, __METHOD__);
            $transaction->rollBack();          
            if ((Yii::$app->id != 'app-console')) {
                Yii::$app->getSession()->addFlash('warning', $message);
            }
            return;
        }
        
        return $clone;
    }

    /**
     * Declares a `has-one` relation.
     * The declaration is returned in terms of a relational [[ActiveQuery]] instance
     * through which the related record can be queried and retrieved back.
     *
     * A `has-one` relation means that there is at most one related record matching
     * the criteria set by this relation, e.g., a customer has one country.
     *
     * For example, to declare the `country` relation for `Customer` class, we can write
     * the following code in the `Customer` class:
     *
     * ```php
     * public function getCountry()
     * {
     *     return $this->hasOne(Country::class, ['id' => 'country_id']);
     * }
     * ```
     *
     * Note that in the above, the 'id' key in the `$link` parameter refers to an attribute name
     * in the related class `Country`, while the 'country_id' value refers to an attribute name
     * in the current AR class.
     *
     * Call methods declared in [[ActiveQuery]] to further customize the relation.
     *
     * @param string $class the class name of the related record
     * @param array $link the primary-foreign key constraint. The keys of the array refer to
     * the attributes of the record associated with the `$class` model, while the values of the
     * array refer to the corresponding attributes in **this** AR class.
     * @return ActiveQueryInterface the relational query object.
     */
    public function hasOne($class, $link, $withScope = false)
    {
        return $this->createRelationQuery($class, $link, false, $withScope);
    }

    /**
     * Declares a `has-many` relation.
     * The declaration is returned in terms of a relational [[ActiveQuery]] instance
     * through which the related record can be queried and retrieved back.
     *
     * A `has-many` relation means that there are multiple related records matching
     * the criteria set by this relation, e.g., a customer has many orders.
     *
     * For example, to declare the `orders` relation for `Customer` class, we can write
     * the following code in the `Customer` class:
     *
     * ```php
     * public function getOrders()
     * {
     *     return $this->hasMany(Order::class, ['customer_id' => 'id']);
     * }
     * ```
     *
     * Note that in the above, the 'customer_id' key in the `$link` parameter refers to
     * an attribute name in the related class `Order`, while the 'id' value refers to
     * an attribute name in the current AR class.
     *
     * Call methods declared in [[ActiveQuery]] to further customize the relation.
     *
     * @param string $class the class name of the related record
     * @param array $link the primary-foreign key constraint. The keys of the array refer to
     * the attributes of the record associated with the `$class` model, while the values of the
     * array refer to the corresponding attributes in **this** AR class.
     * @return ActiveQueryInterface the relational query object.
     */
    public function hasMany($class, $link, $withScope = false)
    {
        return $this->createRelationQuery($class, $link, true, $withScope);
    }

    /**
     * Creates a query instance for `has-one` or `has-many` relation.
     * @param string $class the class name of the related record.
     * @param array $link the primary-foreign key constraint.
     * @param bool $multiple whether this query represents a relation to more than one record.
     * @return ActiveQueryInterface the relational query object.
     * @since 2.0.12
     * @see hasOne()
     * @see hasMany()
     */
    protected function createRelationQuery($class, $link, $multiple, $withScope = false)
    {
        /* @var $class ActiveRecordInterface */
        /* @var $query ActiveQuery */
        $query = $class::find($withScope);
        $query->primaryModel = $this;
        $query->link = $link;
        $query->multiple = $multiple;
        return $query;
    }

    /**
     * Creates and populates a set of models.
     *
     * @param string $modelClass
     * @param array $multipleModels
     * @return array
     */
    public static function createMultiple($modelClass, $multipleModels = [])
    {
        $model = new $modelClass;
        $formName = $model->formName();
        $post = Yii::$app->request->post($formName);
        $models = [];

        if (!empty($multipleModels)) {
            $keys = array_keys(ArrayHelper::map($multipleModels, self::$keyField, self::$keyField));
            $multipleModels = array_combine($keys, $multipleModels);
        }

        if ($post && is_array($post)) {
            foreach ($post as $i => $item) {
                if (isset($item[self::$keyField]) && !empty($item[self::$keyField]) && isset($multipleModels[$item[self::$keyField]])) {
                    $models[] = $multipleModels[$item[self::$keyField]];
                } else {
                    $models[] = new $modelClass;
                }
            }
        }

        unset($model, $formName, $post);

        return $models;
    }

    public function load($data, $formName = null)
    {
        if (!$formName) {
            $formName = $this->formName();
        }
        $scope = null;
        if (!empty($data) && !empty($formName)) {
            if (!isset($data[$formName])) {
                $scope = '';
            } else {
                $scope = $formName;
                $addArr = [];
                foreach ($data as $key => $item) {
                    if (!is_array($data[$key])) {
                        $addArr[$key] = $item;
                        unset($data[$key]);
                    }
                }
                if (!empty($addArr)) {
                    $data[$formName] = ArrayHelper::merge($data[$formName], $addArr);
                }
            }
        }

        return parent::load($data, $scope);
    }

    public static function loadMultiple($models, $data, $formName = null)
    {
        if(empty($data)){
            return false;
        }
        
        if (!isset($formName)) {
            /* @var $first Model */
            $first = reset($models);
            if ($first === false) {
                return false;
            }
            $formName = $first->formName();
        }

        $success = false;
        foreach ($models as $i => $model) {
            if (is_array($model)) {
                $success = FSMBaseModel::loadMultiple($model, $data);
                continue;
            }
            if ($formName == '') {
                if (!empty($data[$i])) {
                    $model->load($data[$i], '');
                    $success = true;
                    continue;
                }

                $modelClass = get_class($model);
                if (strpos($modelClass, '\\') !== false) {
                    $arr = explode('\\', $modelClass);
                    $modelClass = array_pop($arr);
                }
                if (!empty($data[$modelClass])) {
                    $model->load($data[$modelClass], '');
                    $success = true;
                    continue;
                }
            } elseif (!empty($data[$formName][$i])) {
                $model->load($data[$formName][$i], '');
                $success = true;
            }
        }

        return $success;
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        $result = parent::validate($attributeNames, $clearErrors);
        if (!$result && !Yii::$app->request->isAjax) {
            $message = $this->modelTitle() . Yii::t('common', ' not updated due to validation error.');
            $message = $this->getErrorMessage($message);
            Yii::error($message, __METHOD__);
            if ((Yii::$app->id != 'app-console')) {
                Yii::$app->getSession()->addFlash('error', $message);
            }
        }
        return $result;
    }

    public static function find($withScope = false)
    {
        return new \common\scopes\BaseScopeQuery(get_called_class());
    }

    public static function findOne($condition, $withScope = false)
    {
        return static::findByCondition($condition, $withScope)->one();
    }

    public static function findByCondition($condition, $withScope = false)
    {
        $query = static::find($withScope);

        if (!ArrayHelper::isAssociative($condition) && !$condition instanceof \yii\db\ExpressionInterface) {
            // query by primary key
            $primaryKey = static::primaryKey();
            if (isset($primaryKey[0])) {
                $pk = $primaryKey[0];
                if (!empty($query->join) || !empty($query->joinWith)) {
                    $pk = static::tableName() . '.' . $pk;
                }
                $condition = [$pk => $condition];
            } else {
                throw new InvalidConfigException('"' . get_called_class() . '" must have a primary key.');
            }
        }

        return $query->andWhere($condition);
    }

    static public function getNameArr($where = null, $orderBy = '', $idField = '', $nameField = '', $withScope = true)
    {
        $idField = !empty($idField) ? $idField : self::$keyField;
        $nameField = !empty($nameField) ? $nameField : self::$nameField;
        $orderBy = !empty($orderBy) ? $orderBy : self::$nameField;
        if (isset($where)) {
            return ArrayHelper::map(self::findByCondition($where, $withScope)->orderBy($orderBy)->asArray()->all(), $idField, $nameField);
        } else {
            return ArrayHelper::map(self::find($withScope)->orderBy($orderBy)->asArray()->all(), $idField, $nameField);
        }
    }

    static public function getNameList($search, array $args = null)
    {
        $query = self::find()->andWhere(['LIKE', self::$nameField, $search]);
        if (!empty($args)) {
            $query->andWhere($args);
        }
        $data = $query->orderBy(self::$nameField)->asArray()->all();
        $result = ArrayHelper::map($data, self::$keyField, self::$nameField);
        return $result;
    }

    static public function getBackURL($defaultUrl = null)
    {
        $request = Yii::$app->request;
        $http = $request->getIsSecureConnection() ? 'https://' : 'http://';
        $serverName = $request->getServerName();
        $baseUrl = $request->getBaseUrl();
        $referrerPath = $request->getReferrer();
        //$referrerPath = str_replace($http, '', $referrerPath);
        //$referrerPath = str_replace($serverName, '', $referrerPath);
        //$referrerPath = [str_replace($baseUrl, '', $referrerPath)];

        return !empty($referrerPath) ? $referrerPath : (!empty($defaultUrl) ? $defaultUrl : Yii::$app->urlManager->homeUrl);
    }

    static public function getCreateButton()
    {
        return Html::submitButton(Yii::t('common', 'Create'), ['class' => 'btn btn-lg btn-success']);
    }

    static public function getCancelButton()
    {
        $referrerPath = self::getBackURL();
        return Html::a(Html::icon('remove', ['class' => 'bootstrap-dialog-button-icon']) . Yii::t('common', 'Cancel'), $referrerPath, [
            'class' => 'btn btn-lg btn-default btn-cancel',
            'data-dismiss' => 'modal',
        ]);
    }

    static public function getResetButton()
    {
        return Html::resetButton(Yii::t('common', 'Cancel'), ['class' => 'btn btn-default']);
    }

    static public function getSaveButton()
    {
        return Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-lg btn-success']);
    }

    static public function getOkButton()
    {
        return Html::submitButton(Yii::t('common', 'Ok'), ['class' => 'btn btn-lg btn-success']);
    }

    public function getSubmitButton($value = 'create')
    {
        $class = 'btn btn-lg';
        switch ($value) {
            case 'create':
            default:
                $label = ($this->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'));
                $class .= ($this->isNewRecord ? ' btn-success btn-submit' : ' btn-primary btn-submit');
                break;
            case 'save':
                $label = Yii::t('common', 'Save');
                $class .= ' btn-success';
                break;
            case 'send':
                $label = Yii::t('common', 'Send');
                $class .= ' btn-success';
                break;
            case 'ok':
                $label = Yii::t('common', 'Ok');
                $class .= ' btn-success';
                break;
        }
        return Html::submitButton(Html::icon('ok', ['class' => 'bootstrap-dialog-button-icon']) . ' ' . $label, [
            'class' => $class,
        ]);
    }

    static public function getBackButton($btnSize = '', $backPath = null)
    {
        $btnSize = !empty($btnSize) ? 'btn-' . $btnSize : '';
        $backPath = !empty($backPath) ? $backPath : self::getBackURL();
        return Html::a(Html::icon('arrow-left', ['class' => 'bootstrap-dialog-button-icon']) . '&nbsp;' . Yii::t('common', 'Back'), $backPath, [
            'class' => "btn {$btnSize} btn-default btn-back",
        ]);
    }

    static public function getModalButton($btnOptions = [])
    {
        $btnOptions = ArrayHelper::merge([
                'formId' => '',
                'prefix' => '',
                'controller' => '',
                'action' => 'create',
                'params' => [],
                'withRefresh' => false,
                'addNewBtnTitle' => Yii::t('common', 'Add new'),
                'refreshBtnTitle' => Yii::t('common', 'Refresh list'),
                'options' => [],
            ], $btnOptions);
        extract($btnOptions);

        $addBtnId = isset($options['id']) ? $options['id'] : null;
        $refreshBtnId = isset($options['id']) ? $options['id'] . '-refresh' : null;
        unset($options['id']);

        $urlArr = ArrayHelper::merge(
            (isset($parent) ? ["/{$controller}/{$action}", $parent['field_name'] => $parent['id']] : ["/{$controller}/{$action}"]),
            $params
        );
        return
            Html::button(Html::icon('plus'),
                ArrayHelper::merge(
                    [
                        'id' => $addBtnId,
                        'class' => 'btn btn-primary show-modal-button',
                        'modal-tag' => "btn-add-{$prefix}{$controller}-{$formId}",
                        'value' => Url::to($urlArr),
                        'title' => $btnOptions['addNewBtnTitle'],
                    ], $options
                )
            ).
            ($withRefresh ?
                Html::button(Html::icon('refresh'),
                    ArrayHelper::merge(
                        [
                            'id' => $refreshBtnId,
                            'class' => 'btn btn-success refresh-list-button',
                            'modal-tag' => "btn-refresh-{$prefix}{$controller}-{$formId}",
                            'value' => Url::to(ArrayHelper::merge(
                                ["/{$controller}/ajax-modal-name-list"],
                                $params
                            )),
                            'title' => $btnOptions['refreshBtnTitle'],
                        //'style' => 'display: none;',
                        ], $options
                    )
                ) : ''
            );
    }

    static function getModalButtonContent($btnOptions = [])
    {
        return [
            //'content' => $this->getModalButton($btnOptions),
            'content' => static::getModalButton($btnOptions),
            'asButton' => true,
        ];
    }

    protected function getCascadeDeleting()
    {
        return $this->_cascadeDeleting;
    }

    protected function setCascadeDeleting($value = 1)
    {
        $this->_cascadeDeleting = $value;
    }

    private function markAsDeleted()
    {
        $result = true;
        if (empty($this->deleted) && ($result = $this->beforeMarkAsDeleted())) {
            $result = $this->updateAttributes(['deleted' => true]);
        }
        return $result;
    }

    public function beforeMarkAsDeleted()
    {
        $event = new ModelEvent;
        $this->trigger(self::EVENT_BEFORE_MARK_AS_DELETED, $event);

        return $event->isValid;
    }

    public static function deleteByIDs(array $ids)
    {
        $result = true;
        foreach ($ids as $id) {
            $model = static::findOne($id);
            $result = $result && $model->delete();
        }
        return $result !== false;
    }

    public function delete(FSMBaseModel $owner = null)
    {
        if ($this->hasAttribute('deleted')) {
            $result = $this->markAsDeleted();
        } else {
            $result = parent::delete();
        }
        return $result !== false;
    }

    protected function getIgnoredFieldsForDelete()
    {
        return [];
    }

    public function getExternalFields()
    {
        return $this->_externalFields;
    }

    public function setExternalFields(array $fields)
    {
        $fields = ArrayHelper::merge(
            $fields, (array) $this->_externalFields
        );
        $this->_externalFields = $fields;
    }

    /**
     * @return array of relations to other model
     */
    protected function getRelations()
    {
        $db = Yii::$app->get($this->db, false);
        $baseTableName = $this->tableName();
        $baseClassName = $this->generateClassName($baseTableName);

        if (($pos = strpos($baseTableName, '.')) !== false) {
            $schemaName = substr($baseTableName, 0, $pos);
        } else {
            $schemaName = '';
        }

        $relations = [];
        foreach ($db->getSchema()->getTableSchemas($schemaName) as $table) {
            $tableName = $table->name;
            $className = $this->generateClassName($tableName);
            foreach ($table->foreignKeys as $refs) {
                $refTable = $refs[0];
                unset($refs[0]);
                $fks = array_keys($refs);
                $refClassName = $this->generateClassName($refTable);

                // Add relation for this table
                $relationName = $this->generateRelationName($relations, $className, $table, $fks[0], false);
                $relations[$className][$relationName] = [
                    'class' => $refClassName,
                    'field' => $fks[0],
                    'hasMany' => false,
                ];

                // Add relation for the referenced table
                $hasMany = false;
                if (count($table->primaryKey) > count($fks)) {
                    $hasMany = true;
                } else {
                    foreach ($fks as $key) {
                        if (!in_array($key, $table->primaryKey, true)) {
                            $hasMany = true;
                            break;
                        }
                    }
                }
                $relationName = $this->generateRelationName($relations, $refClassName, $refTable, $className, $hasMany);
                $relations[$refClassName][$relationName] = [
                    'class' => $className,
                    'field' => $fks[0],
                    'hasMany' => $hasMany,
                ];
            }

            if (($fks = $this->checkPivotTable($table)) === false) {
                continue;
            }
            $table0 = $fks[$table->primaryKey[0]][0];
            $table1 = $fks[$table->primaryKey[1]][0];
            $className0 = $this->generateClassName($table0);
            $className1 = $this->generateClassName($table1);

            $relationName = $this->generateRelationName($relations, $className0, $db->getTableSchema($table0), $table->primaryKey[1], true);
            $relations[$className0][$relationName] = [
                'class' => $className1,
                'field' => $table->primaryKey[1],
                'hasMany' => true,
            ];

            $relationName = $this->generateRelationName($relations, $className1, $db->getTableSchema($table1), $table->primaryKey[0], true);
            $relations[$className1][$relationName] = [
                'class' => $className0,
                'field' => $table->primaryKey[0],
                'hasMany' => true,
            ];
        }

        return isset($relations[$baseClassName]) ? $relations[$baseClassName] : [];
    }

    /**
     * Generates a class name from the specified table name.
     * @param string $tableName the table name (which may contain schema prefix)
     * @return string the generated class name
     */
    protected function generateClassName($tableName)
    {
        static $_classNames = [];

        if (isset($_classNames[$tableName])) {
            return $_classNames[$tableName];
        }

        if (($pos = strrpos($tableName, '.')) !== false) {
            $tableName = substr($tableName, $pos + 1);
        }

        $db = Yii::$app->get($this->db, false);
        $patterns = [];
        $patterns[] = "/^{$db->tablePrefix}(.*?)$/";
        $patterns[] = "/^(.*?){$db->tablePrefix}$/";
        $baseTableName = $this->tableName();
        if (strpos($baseTableName, '*') !== false) {
            $pattern = $baseTableName;
            if (($pos = strrpos($pattern, '.')) !== false) {
                $pattern = substr($pattern, $pos + 1);
            }
            $patterns[] = '/^' . str_replace('*', '(\w+)', $pattern) . '$/';
        }
        $className = $tableName;
        foreach ($patterns as $pattern) {
            if (preg_match($pattern, $tableName, $matches)) {
                $className = $matches[1];
                break;
            }
        }

        return $_classNames[$tableName] = Inflector::id2camel($className, '_');
    }

    /**
     * Generate a relation name for the specified table and a base name.
     * @param array $relations the relations being generated currently.
     * @param string $className the class name that will contain the relation declarations
     * @param \yii\db\TableSchema $table the table schema
     * @param string $key a base name that the relation name may be generated from
     * @param boolean $multiple whether this is a has-many relation
     * @return string the relation name
     */
    protected function generateRelationName($relations, $className, $table, $key, $multiple)
    {
        if (strcasecmp(substr($key, -2), self::$keyField) === 0 && strcasecmp($key, self::$keyField)) {
            $key = rtrim(substr($key, 0, -2), '_');
        }
        if ($multiple) {
            $key = Inflector::pluralize($key);
        }
        $name = $rawName = Inflector::id2camel($key, '_');
        $i = 0;
        while (isset($table->columns[lcfirst($name)])) {
            $name = $rawName . ($i++);
        }
        while (isset($relations[$className][lcfirst($name)])) {
            $name = $rawName . ($i++);
        }

        return $name;
    }

    /**
     * Checks if the given table is a pivot table.
     * For simplicity, this method only deals with the case where the pivot contains two PK columns,
     * each referencing a column in a different table.
     * @param \yii\db\TableSchema the table being checked
     * @return array|boolean the relevant foreign key constraint information if the table is a pivot table,
     * or false if the table is not a pivot table.
     */
    protected function checkPivotTable($table)
    {
        $pk = $table->primaryKey;
        if (count($pk) !== 2) {
            return false;
        }
        $fks = [];
        foreach ($table->foreignKeys as $refs) {
            if (count($refs) === 2) {
                if (isset($refs[$pk[0]])) {
                    $fks[$pk[0]] = [$refs[0], $refs[$pk[0]]];
                } elseif (isset($refs[$pk[1]])) {
                    $fks[$pk[1]] = [$refs[0], $refs[$pk[1]]];
                }
            }
        }
        if (count($fks) === 2 && $fks[$pk[0]][0] !== $fks[$pk[1]][0]) {
            return $fks;
        } else {
            return false;
        }
    }

    public function fieldToFloat($field, $decimals = null)
    {
        if (!isset($this->$field)) {
            return null;
        }
        return FSMHelper::toFloat($this->$field, $decimals);
    }

    public static function facetedSearch(array $param)
    {
        return null;
    }

    // a function for comparing two float numbers  
    // float 1 - The first number  
    // float 2 - The number to compare against the first  
    // operator - The operator. Valid options are =, <=, <, >=, >, <>, eq, lt, lte, gt, gte, ne  
    public function compareFloatNumbers($float1, $float2, $operator = '=')
    {
        // Check numbers to 5 digits of precision  
        $epsilon = 0.00001;

        $float1 = (float) $float1;
        $float2 = (float) $float2;

        switch ($operator) {
            // equal  
            case "=":
            case "eq": {
                if (abs($float1 - $float2) < $epsilon) {
                    return true;
                }
                break;
            }
            // less than  
            case "<":
            case "lt": {
                if (abs($float1 - $float2) < $epsilon) {
                    return false;
                } else {
                    if ($float1 < $float2) {
                        return true;
                    }
                }
                break;
            }
            // less than or equal  
            case "<=":
            case "lte": {
                if ($this->compareFloatNumbers($float1, $float2, '<') || $this->compareFloatNumbers($float1, $float2, '=')) {
                    return true;
                }
                break;
            }
            // greater than  
            case ">":
            case "gt": {
                if (abs($float1 - $float2) < $epsilon) {
                    return false;
                } else {
                    if ($float1 > $float2) {
                        return true;
                    }
                }
                break;
            }
            // greater than or equal  
            case ">=":
            case "gte": {
                if ($this->compareFloatNumbers($float1, $float2, '>') || $this->compareFloatNumbers($float1, $float2, '=')) {
                    return true;
                }
                break;
            }
            case "<>":
            case "!=":
            case "ne": {
                if (abs($float1 - $float2) > $epsilon) {
                    return true;
                }
                break;
            }
            default: {
                die("Unknown operator '" . $operator . "' in compareFloatNumbers()");
            }
        }

        return false;
    }

}
