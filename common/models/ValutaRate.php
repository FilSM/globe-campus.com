<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "valuta_rate".
 *
 * @property integer $id
 * @property string $rate_date
 * @property string $currency
 * @property string $rate
 */
class ValutaRate extends \common\models\mainclass\FSMBaseModel
{
    const URL = 'https://www.bank.lv/vk/ecb.xml';

    private $date;

    public function __construct($date = null)
    {
        $this->date = isset($date) ? date('Ymd', strtotime($date)) : date('Ymd');
        parent::__construct();
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'valuta_rate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rate_date'], 'safe'],
            [['currency', 'rate'], 'required'],
            [['rate'], 'number'],
            [['currency'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('currency', 'ValutaRate|Valuta Rates', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'rate_date' => Yii::t('currency', 'Rate Date'),
            'currency' => Yii::t('currency', 'Currency'),
            'rate' => Yii::t('currency', 'Rate'),
        ];
    }
    
    public function loadFromLB()
    {
        $xml = new \DOMDocument();
        $url = self::URL.'?date=' . $this->date;

        if (@$xml->load($url)) {
            $root = $xml->documentElement;
            $items = $root->getElementsByTagName('Currency');

            $date = date('Y-m-d', strtotime($this->date));
            
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {            
                $totalInserted = 0;
                foreach ($items as $item) {
                    $code = $item->getElementsByTagName('ID')->item(0)->nodeValue;
                    $rate = $item->getElementsByTagName('Rate')->item(0)->nodeValue;
                    $rate = str_replace(',', '.', $rate);

                    if(!$model = self::findOne([
                        'rate_date' => $date,
                        'currency' => $code,
                    ])){
                        $model = new ValutaRate();
                        $model->rate_date = $date;
                        $model->currency = $code;
                        $totalInserted++;
                    }
                    $model->rate = $rate;
                    $model->save();
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                return false;
            } 
            return $totalInserted;
        } else {
            return false;
        }
    }
    
    public static function getByDate($code, $date)
    {
        if(!$model = ValutaRate::findOne([
            'currency' => $code,
            'rate_date' => $date, 
        ])){
            $model = ValutaRate::getLastRate($code);
        }
        return $model;
    }
    
    public static function getLastRate($code)
    {
        $model = ValutaRate::findByCondition(['currency' => $code])
            ->orderBy('rate_date desc')
            ->one();
        return $model;
    }
}