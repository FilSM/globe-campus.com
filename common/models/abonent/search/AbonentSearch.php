<?php

namespace common\models\abonent\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\abonent\Abonent;

/**
 * AbonentSearch represents the model behind the search form of `common\models\abonent\Abonent`.
 */
class AbonentSearch extends Abonent
{
    public $create_time_range;
    public $client_name;
    public $manager_name;
    public $manager_user_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'main_client_id', 'manager_id', 'simple_workflow', 'send_all_attachment', 'deleted'], 'integer'],
            [['name', 'subscription_end_date', 'subscription_type',
                'client_name', 'manager_name', 'manager_user_id', 'create_time_range'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
    
        $query = Abonent::find(true);

        $query->select = [
            $baseTableName . '.*',
            'client_name' => 'client.name',
            'manager_name' => 'manager.name',
            'manager_user_id' => 'manager.user_id',
        ];
        
        $query->leftJoin(['client' => 'client'], 'client.id = '.$baseTableName.'.main_client_id');
        $query->leftJoin(['manager' => 'profile'], 'manager.id = '.$baseTableName.'.manager_id');

        if(!isset($params['sort'])){
            $query->addOrderBy('id desc');
        }
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if(!empty($this->create_time) && strpos($this->create_time, '-') !== false) { 
            $this->create_time_range =$this->create_time;
            list($start_date, $end_date) = explode(' - ', $this->create_time_range); 
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));
            $query->andFilterWhere(['between', $baseTableName . '.create_time', $start_date, $end_date.' 23:59:59']);
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            $baseTableName.'.id' => $this->id,
            $baseTableName.'.main_client_id' => $this->main_client_id,
            $baseTableName.'.subscription_end_date' => $this->subscription_end_date,
            $baseTableName.'.manager_id' => $this->manager_id,
            $baseTableName.'.simple_workflow' => $this->simple_workflow,
            $baseTableName.'.send_all_attachment' => $this->send_all_attachment,
            $baseTableName.'.deleted' => $this->deleted,
            $baseTableName.'.create_time' => !empty($this->create_time) && empty($this->create_time_range) ? date('Y-m-d', strtotime($this->create_time)) : null,
            
        ]);

        $query->andFilterWhere(['like', $baseTableName.'.name', $this->name])
            ->andFilterWhere(['like', $baseTableName.'.subscription_type', $this->subscription_type])
            ->andFilterWhere(['like', 'client.name', $this->client_name])
            ->andFilterWhere(['like', 'manager.name', $this->manager_name]);
        
        return $dataProvider;
    }
}