<?php

namespace common\models\abonent\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\abonent\AbonentAgreement;

/**
 * AbonentBillSearch represents the model behind the search form of `common\models\abonent\AbonentBill`.
 */
class AbonentAgreementSearch extends AbonentAgreement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'abonent_id', 'agreement_id', 'project_id', 'primary_abonent', 'create_user_id'], 'integer'],
            [['create_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $baseTableName = $this->tableName();
        $this->clearDefaultValues();
    
        $query = AbonentAgreement::find(true);

        if(!isset($params['sort'])){
            $query->addOrderBy('id desc');
        }
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            $baseTableName.'.id' => $this->id,
            $baseTableName.'.abonent_id' => $this->abonent_id,
            $baseTableName.'.agreement_id' => $this->agreement_id,
            $baseTableName.'.project_id' => $this->project_id,
            $baseTableName.'.primary_abonent' => $this->primary_abonent,
            $baseTableName.'.create_time' => $this->create_time,
            $baseTableName.'.create_user_id' => $this->create_user_id,
        ]);

        return $dataProvider;
    }
}