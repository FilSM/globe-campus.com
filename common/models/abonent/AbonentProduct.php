<?php

namespace common\models\abonent;

use Yii;

use common\models\abonent\Abonent;
use common\models\user\FSMUser;
use common\models\Product;

/**
 * This is the model class for table "abonent_product".
 *
 * @property integer $id
 * @property integer $abonent_id
 * @property integer $product_id
 * @property string $create_time
 * @property integer $create_user_id
 *
 * @property Abonent $abonent
 * @property FSMUser $createUser
 * @property Product $product
 */
class AbonentProduct extends \common\models\mainclass\FSMCreateModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'abonent_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['abonent_id', 'product_id'], 'required'],
            [['abonent_id', 'product_id', 'create_user_id'], 'integer'],
            [['create_time'], 'safe'],
            [['abonent_id', 'product_id'], 'unique', 'targetAttribute' => ['abonent_id', 'product_id']],
            [['abonent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Abonent::class, 'targetAttribute' => ['abonent_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['create_user_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('user', 'AbonentProduct|Abonent Products', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'abonent_id' => 'Abonent ID',
            'product_id' => 'Product ID',
            'create_time' => 'Create Time',
            'create_user_id' => 'Create User ID',
        ];
    }

    /**
     * @inheritdoc
     */
    protected function getIgnoredFieldsForDelete() {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, ['abonent_id']
        );
        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonent()
    {
        return $this->hasOne(Abonent::class, ['id' => 'abonent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }
}