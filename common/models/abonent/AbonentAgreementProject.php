<?php

namespace common\models\abonent;

use common\models\abonent\AbonentAgreement;
use common\models\client\Project;

/**
 * This is the model class for table "abonent_agreement_project".
 *
 * @property integer $id
 * @property integer $abonent_agreement_id
 * @property integer $project_id
 *
 * @property AbonentAgreement $abonentAgreement
 * @property Project $project
 */
class AbonentAgreementProject extends \common\models\mainclass\FSMBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'abonent_agreement_project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['abonent_agreement_id', 'project_id'], 'required'],
            [['abonent_agreement_id', 'project_id'], 'integer'],
            [['abonent_agreement_id'], 'exist', 'skipOnError' => true, 'targetClass' => AbonentAgreement::class, 'targetAttribute' => ['abonent_agreement_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::class, 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('app', 'AbonentAgreementProject|Abonent Agreement Projects', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'abonent_agreement_id' => 'Abonent Agreement ID',
            'project_id' => 'Project ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonentAgreement()
    {
        return $this->hasOne(AbonentAgreement::class, ['id' => 'abonent_agreement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectName()
    {
        return Project::findOne($this->project_id)->name;
    }
}