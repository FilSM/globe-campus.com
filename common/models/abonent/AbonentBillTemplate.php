<?php

namespace common\models\abonent;

use Yii;

use common\models\abonent\Abonent;
use common\models\user\FSMUser;
use common\models\bill\BillTemplate;
use common\models\client\Project;

/**
 * This is the model class for table "abonent_bill_template".
 *
 * @property integer $id
 * @property integer $abonent_id
 * @property integer $bill_template_id
 * @property integer $project_id
 * @property string $create_time
 * @property integer $create_user_id
 *
 * @property Abonent $abonent
 * @property BillTemplate $billTemplate
 * @property User $createUser
 * @property Project $project
 */
class AbonentBillTemplate extends \common\models\mainclass\FSMCreateModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'abonent_bill_template';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[/*'abonent_id', 'bill_template_id'*/], 'required'],
            [['abonent_id', 'bill_template_id', 'project_id', 'create_user_id'], 'integer'],
            [['create_time'], 'safe'],
            [['abonent_id', 'bill_template_id', 'project_id'], 'unique', 'targetAttribute' => ['abonent_id', 'bill_template_id', 'project_id']],
            [['abonent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Abonent::class, 'targetAttribute' => ['abonent_id' => 'id']],
            [['bill_template_id'], 'exist', 'skipOnError' => true, 'targetClass' => BillTemplate::class, 'targetAttribute' => ['bill_template_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['create_user_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::class, 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('bill', 'AbonentBillTemplate|Abonent Bill Templates', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'abonent_id' => 'Abonent ID',
            'bill_template_id' => 'Bill Template ID',
            'project_id' => 'Project ID',
            'create_time' => 'Create Time',
            'create_user_id' => 'Create User ID',
        ];
    }

    /**
     * @inheritdoc
     */
    protected function getIgnoredFieldsForDelete() {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, ['abonent_id', 'project_id']
        );
        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonent()
    {
        return $this->hasOne(Abonent::class, ['id' => 'abonent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillTemplate()
    {
        return $this->hasOne(BillTemplate::class, ['id' => 'bill_template_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }
}