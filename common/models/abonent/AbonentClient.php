<?php

namespace common\models\abonent;

use Yii;
use yii\helpers\ArrayHelper;

use common\models\abonent\Abonent;
use common\models\user\FSMUser;
use common\models\client\Client;
use common\models\client\ClientGroup;
use common\models\user\FSMProfile;
use common\models\Language;
use common\models\Valuta;

/**
 * This is the model class for table "abonent_client".
 *
 * @property integer $id
 * @property integer $abonent_id
 * @property integer $client_id
 * @property integer $parent_id
 * @property integer $client_group_id
 * @property string $it_is
 * @property integer $manager_id
 * @property integer $agent_id
 * @property integer $language_id
 * @property string $status
 * @property string $debit
 * @property integer $debit_valuta_id
 * @property string $gen_number
 * @property string $comment

 * @property string $create_time
 * @property integer $create_user_id
 *
 * @property Abonent $abonent
 * @property Client $client
 * @property Client $parent
 * @property ClientGroup $clientGroup
 * @property Language $language
 * @property Valuta $debitValuta
 * @property FSMProfile $manager
 * @property FSMProfile $agent
 * @property FSMUser $createUser
 */
class AbonentClient extends \common\models\mainclass\FSMCreateModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'abonent_client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['abonent_id', 'client_id'], 'required'],
            //[['manager_id'], 'required', 'on' => ['abonent']],
            [['abonent_id', 'client_id', 'parent_id', 'client_group_id', 'manager_id', 
                'agent_id', 'language_id', 'debit_valuta_id', 'create_user_id'], 'integer'],
            [['it_is', 'status', 'comment'], 'string'],
            [['debit'], 'number'],
            [['gen_number'], 'string', 'max' => 10],
            [['create_time'], 'safe'],
            [['abonent_id', 'client_id'], 'unique', 'targetAttribute' => ['abonent_id', 'client_id']],
            [['abonent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Abonent::class, 'targetAttribute' => ['abonent_id' => 'id']], 
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['client_id' => 'id']],            
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['parent_id' => 'id']],            
            [['client_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => ClientGroup::class, 'targetAttribute' => ['client_group_id' => 'id']],
            [['debit_valuta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Valuta::class, 'targetAttribute' => ['debit_valuta_id' => 'id']],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::class, 'targetAttribute' => ['language_id' => 'id']],
            [['manager_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMProfile::class, 'targetAttribute' => ['manager_id' => 'id']],
            [['agent_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMProfile::class, 'targetAttribute' => ['agent_id' => 'id']],            
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['create_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('user', 'AbonentClient|Abonent Clients', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'abonent_id' => Yii::t('client', 'Abonent'),
            'client_id' => Yii::t('client', 'Client'),
            'parent_id' => Yii::t('client', 'Parent company'),
            'client_group_id' => Yii::t('client', 'Client group'),
            'it_is' => Yii::t('common', 'It is'),
            'manager_id' => Yii::t('common', 'Curator'),
            'agent_id' => Yii::t('common', 'Agent'),
            'status' => Yii::t('common', 'Status'),
            'language_id' => Yii::t('languages', 'Communication language'),
            'debit' => Yii::t('client', 'Debit / Credit (+/-)'),
            'debit_valuta_id' => Yii::t('common', 'Currency'),
            'gen_number' => Yii::t('abonent', 'Number gen.template'),
            'comment' => Yii::t('common', 'Comment'),
            'create_user_id' => Yii::t('common', 'Create User'),
        ];
    }

    /**
     * @inheritdoc
     */
    protected function getIgnoredFieldsForDelete() {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, 
            ['abonent_id', 'client_id', 'client_group_id', 'parent_id', 'language_id', 
                'manager_id', 'agent_id', 'debit_valuta_id',
            ]
        );
        return $fields;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonent()
    {
        return $this->hasOne(Abonent::class, ['id' => 'abonent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::class, ['id' => 'client_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientGroup() {
        return $this->hasOne(ClientGroup::class, ['id' => 'client_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent() {
        return $this->hasOne(Client::class, ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::class, ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDebitValuta() {
        return $this->hasOne(Valuta::class, ['id' => 'debit_valuta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager() {
        return $this->hasOne(FSMProfile::class, ['id' => 'manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgent() {
        return $this->hasOne(FSMProfile::class, ['id' => 'agent_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'create_user_id']);
    }
    
    public function beforeValidate()
    {
        if(empty($this->abonent_id)){
            $this->abonent_id = $this->userAbonentId;
        }
        return parent::beforeValidate();
    }        
}