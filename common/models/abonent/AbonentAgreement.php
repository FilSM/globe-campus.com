<?php

namespace common\models\abonent;

use Yii;

use common\models\abonent\Abonent;
use common\models\abonent\AbonentAgreementProject;
use common\models\user\FSMUser;
use common\models\client\Agreement;

/**
 * This is the model class for table "abonent_agreement".
 *
 * @property integer $id
 * @property integer $abonent_id
 * @property integer $agreement_id
 * @property integer $primary_abonent
 * @property string $create_time
 * @property integer $create_user_id
 *
 * @property Abonent $abonent
 * @property Agreement $agreement
 * @property FSMUser $createUser
 * @property AbonentAgreementProject[] $abonentAgreementProjects
 */
class AbonentAgreement extends \common\models\mainclass\FSMCreateModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'abonent_agreement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['abonent_id', 'agreement_id',], 'required'],
            [['abonent_id', 'agreement_id', 'primary_abonent', 'create_user_id'], 'integer'],
            [['create_time'], 'safe'],
            [['abonent_id', 'agreement_id'], 'unique', 'targetAttribute' => ['abonent_id', 'agreement_id']],
            [['abonent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Abonent::class, 'targetAttribute' => ['abonent_id' => 'id']],
            [['agreement_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agreement::class, 'targetAttribute' => ['agreement_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['create_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('user', 'AbonentAgreement|Abonent Agreements', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'abonent_id' => 'Abonent',
            'agreement_id' => 'Agreement',
            'primary_abonent' => 'Primary abonent',
            'create_time' => 'Create Time',
            'create_user_id' => 'Create User ID',
        ];
    }

    /**
     * @inheritdoc
     */
    protected function getIgnoredFieldsForDelete() {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, ['abonent_id', 'agreement_id']
        );
        return $fields;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonent()
    {
        return $this->hasOne(Abonent::class, ['id' => 'abonent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreement()
    {
        return $this->hasOne(Agreement::class, ['id' => 'agreement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'create_user_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonentAgreementProjects(){
        return $this->hasMany(AbonentAgreementProject::class, ['abonent_agreement_id' => 'id']);
    }
}