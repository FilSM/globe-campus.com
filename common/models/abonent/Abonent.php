<?php

namespace common\models\abonent;

use Yii;
use yii\helpers\ArrayHelper;

use common\models\Product;
use common\models\client\Project;
use common\models\client\Client;
use common\models\client\Agreement;
use common\models\bill\Bill;
use common\models\bill\Expense;
use common\models\bill\Convention;
use common\models\bill\PaymentOrder;
use common\models\bill\PaymentConfirm;
use common\models\user\FSMUser;
use common\models\user\FSMProfile;

/**
 * This is the model class for table "abonent".
 *
 * @property integer $id
 * @property integer $deleted
 * @property string $name
 * @property integer $main_client_id
 * @property string $subscription_end_date
 * @property string $subscription_type
 * @property integer $manager_id
 * @property integer $simple_workflow
 * @property integer $send_all_attachment
 * @property string $comment
 * 
 * @property Client $mainClient
 * @property FSMProfile $manager
 * @property Project[] $projects
 * @property Agreement[] $agreements
 * @property Client[] $clients
 * @property Bill[] $bills
 * @property Expense[] $expenses
 * @property FSMUser $createUser
 * @property FSMUser $updateUser
 */
class Abonent extends \common\models\mainclass\FSMCreateUpdateModel
{
    const ABONENT_TYPE_SILVER = 'silver';
    const ABONENT_TYPE_GOLD = 'gold';
    const ABONENT_TYPE_PLATINUM = 'platinum';

    protected $_externalFields = [
        'client_name',
        'manager_name',
        'manager_user_id',
    ];    

    public function init() {
        parent::init();
        $this->cascadeDeleting = true;
    }
/*
    public static function find($withScope = false) {
        if ((Yii::$app->id != 'app-console') && $withScope) {
            $scope = new \common\scopes\BaseScopeQuery(get_called_class());
            return $scope->onlyAuthor();
        }
        return parent::find();
    }
*/    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'abonent';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'main_client_id', 'subscription_end_date'], 'required'],
            [['main_client_id', 'manager_id', 'deleted', 'simple_workflow', 'send_all_attachment', 
                'create_user_id', 'update_user_id'], 'integer'],
            [['subscription_end_date'], 'safe'],
            [['subscription_type', 'comment'], 'string'],
            [['name'], 'string', 'max' => 64],
            [['main_client_id', 'name'], 'unique', 'targetAttribute' => ['main_client_id', 'name']],
            [['manager_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMProfile::class, 'targetAttribute' => ['manager_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['create_user_id' => 'id']],
            [['update_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['update_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('abonent', 'Abonent|Abonents', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'deleted' => Yii::t('common', 'Deleted'),
            'name' => Yii::t('common', 'Abonent name'),
            'main_client_id' => Yii::t('abonent', 'Main client'),
            'subscription_end_date' => Yii::t('abonent', 'Subscription end date'),
            'subscription_type' => Yii::t('abonent', 'Subscription type'),
            'manager_id' => Yii::t('common', 'Curator'),
            'simple_workflow' => Yii::t('abonent', 'Simple workflow'),
            'send_all_attachment' => Yii::t('abonent', 'Send all attachments to the client'),
            'comment' => Yii::t('abonent', 'Abonent comment'),
            'create_time' => Yii::t('common', 'Create Time'),
            'create_user_id' => Yii::t('common', 'Create User'),
            'update_time' => Yii::t('common', 'Update Time'),
            'update_user_id' => Yii::t('common', 'Update User'),
            
            'client_name' => Yii::t('client', 'Main Client'),
            'manager_name' => Yii::t('client', 'Curator'),
        ];
    }
    
    protected function getIgnoredFieldsForDelete() {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, ['manager_id']
        );
        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainClient()
    {
        $client = Client::find()->where(['id' => $this->main_client_id])->one();
        return $client;
        //return $this->hasOne(Client::class, ['id' => 'main_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(FSMProfile::class, ['id' => 'manager_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::class, ['abonent_id' => 'id'])->notDeleted();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::class, ['id' => 'product_id'])->viaTable('abonent_product', ['abonent_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Client::class, ['id' => 'client_id'])->viaTable('abonent_client', ['abonent_id' => 'id'])->notDeleted();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBills()
    {
        return $this->hasMany(Bill::class, ['id' => 'bill_id'])->viaTable('abonent_bill', ['abonent_id' => 'id'])->notDeleted();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConventions()
    {
        return $this->hasMany(Convention::class, ['abonent_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenses()
    {
        return $this->hasMany(Expense::class, ['abonent_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreements()
    {
        return $this->hasMany(Agreement::class, ['id' => 'agreement_id'])->viaTable('abonent_agreement', ['abonent_id' => 'id'])->notDeleted();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentOrders()
    {
        return $this->hasMany(PaymentOrder::class, ['abonent_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentConfirms()
    {
        return $this->hasMany(PaymentConfirm::class, ['abonent_id' => 'id']);
    }
    
    static public function getAbonentTypeList() {
        return [
            Abonent::ABONENT_TYPE_SILVER => Yii::t('abonent', 'Silver'),
            Abonent::ABONENT_TYPE_GOLD => Yii::t('abonent', 'Gold'),
            Abonent::ABONENT_TYPE_PLATINUM => Yii::t('abonent', 'Platinum'),
        ];
    }
   
}