<?php

namespace common\models\abonent;

use Yii;

use common\models\abonent\Abonent;
use common\models\user\FSMUser;
use common\models\bill\Bill;
use common\models\client\Project;

/**
 * This is the model class for table "abonent_bill".
 *
 * @property integer $id
 * @property integer $abonent_id
 * @property integer $bill_id
 * @property integer $project_id
 * @property integer $primary_abonent
 * @property string $create_time
 * @property integer $create_user_id
 *
 * @property Abonent $abonent
 * @property Bill $bill
 * @property Project $project
 * @property FSMUser $createUser
 */
class AbonentBill extends \common\models\mainclass\FSMCreateModel
{
    public $needValidateProject = false;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'abonent_bill';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[/*'abonent_id', 'bill_id'*/], 'required'],
            ['project_id', 'required', 'when' => function($model) {
                return !empty($model->needValidateProject);
            }],
            [['abonent_id', 'bill_id', 'project_id', 'primary_abonent', 'create_user_id'], 'integer'],
            [['create_time'], 'safe'],
            [['abonent_id', 'bill_id', 'project_id'], 'unique', 'targetAttribute' => ['abonent_id', 'bill_id', 'project_id']],
            [['abonent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Abonent::class, 'targetAttribute' => ['abonent_id' => 'id']],
            [['bill_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bill::class, 'targetAttribute' => ['bill_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::class, 'targetAttribute' => ['project_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => FSMUser::class, 'targetAttribute' => ['create_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function modelTitle($n = 1, $translate = true) {
        return parent::label('user', 'AbonentBill|Abonent Bills', $n, $translate);
    }    
        
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'abonent_id' => 'Abonent',
            'bill_id' => 'Bill',
            'project_id' => 'Project',
            'primary_abonent' => 'Primary abonent',
            'create_time' => 'Create Time',
            'create_user_id' => 'Create User ID',
        ];
    }

    /**
     * @inheritdoc
     */
    protected function getIgnoredFieldsForDelete() {
        $fields = parent::getIgnoredFieldsForDelete();
        $fields = ArrayHelper::merge(
            $fields, ['abonent_id', 'project_id']
        );
        return $fields;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonent()
    {
        return $this->hasOne(Abonent::class, ['id' => 'abonent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBill()
    {
        return $this->hasOne(Bill::class, ['id' => 'bill_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(FSMUser::class, ['id' => 'create_user_id']);
    }
}