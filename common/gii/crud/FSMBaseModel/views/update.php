<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();

echo "<?php\n";
?>

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = Yii::t($model->tableName(), 'Update a '.$model->modelTitle(1, false)) . ': #' . $model-><?= $generator->getNameAttribute() ?>;
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model-><?= $generator->getNameAttribute() ?>, 'url' => ['view', <?= $urlParams ?>]];
$this->params['breadcrumbs'][] = <?= $generator->generateAppString('Edit') ?>;

$isModal = !empty($isModal);
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-update">

    <?= "<?php " ?>if(!$isModal): ?> 
    <?= "<?= " ?>Html::pageHeader(Html::encode($this->title)); ?>
    <?= "<?php " ?>endif; ?>    

    <?= "<?= " ?>$this->render('_form', [
        'model' => $model,
<?php if($generator->hasLanguageIdField) : ?>
        'languageList' => $languageList,
<?php endif; ?>
    ]) ?>

</div>