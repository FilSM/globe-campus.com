<?php

namespace common\assets;

class PHPJSAsset extends \common\components\FSMAssetBundle {

    public function init() {
        $this->setSourcePath('@common/assets/php_js');
        /*
        $this->setupAssets('js', [
            'datetime/date',
            'datetime/strtotime',
            'math/round',
            //'strings/implode',
            'strings/explode',
            'strings/str_pad',
            'var/empty',
        ]);
         * 
         */
        $this->js = [
            'array/in_array.js',
            'datetime/date.js',
            'datetime/strtotime.js',
            'math/round.js',
            'strings/implode.js',
            'strings/explode.js',
            'strings/str_pad.js',
            'strings/number_format.js',
            'var/empty.js',
        ];

        $this->checkNeedReloadAssets();
        
        parent::init();
    }

}
