<?php

use yii\web\View;
use yii\helpers\Url;

$appUrl = Url::base();
$user = Yii::$app->user->identity;
$enableLogoutTimer = (integer)(YII_ENV_PROD && isset($user) && Yii::$app->params['ENABLE_LOGOUT_TIMER']);
$get = json_encode($_GET);
$script = <<< JS
    var appUrl = '{$appUrl}';
    var enableLogoutTimer = {$enableLogoutTimer};   
    var \$_GET = {$get};
JS;
Yii::$app->getView()->registerJs($script, View::POS_BEGIN);

if(!empty($enableLogoutTimer)){
    $loginUrl = Url::to(['/user/security/login']);
    $logoutUrl = Url::to(['/user/security/logout']);
    $inactivitySeconds = !empty($user) ? Yii::$app->user->authTimeout : 0;
    $showMinutes = Yii::$app->params['dialogTimer.showMinutes'];
    $inactivitySeconds = $inactivitySeconds - (($showMinutes + 1) * 60);
$script = <<< JS
    var loginUrl = '{$loginUrl}';    
    var logoutUrl = '{$logoutUrl}';    
    var inactivitySeconds = '{$inactivitySeconds}';    
    var globalShowMinutes = '{$showMinutes}';    
JS;
    Yii::$app->getView()->registerJs($script, View::POS_BEGIN);
}