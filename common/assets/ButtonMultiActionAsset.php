<?php

namespace common\assets;

use common\components\FSMAssetBundle;

class ButtonMultiActionAsset extends FSMAssetBundle {

    public function init() {
        $this->setSourcePath('@common/assets');
        $this->setupAssets('js', ['js/fsmMultiAction']);
        //$this->setupAssets('css', ['css/fsm-delete-action']);
        
        $this->checkNeedReloadAssets();
        
        parent::init();
    }

}
