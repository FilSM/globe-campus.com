<?php

namespace common\assets;

use common\components\FSMAssetBundle;

class UIAsset extends FSMAssetBundle {

    public function init() {
        $this->setSourcePath('@common/assets');
        $this->setupAssets('js', [
            'js/filJSCommon',
        ]);
        
        /*
        $this->setupAssets('css', [
            'css/gmap.control',
        ]);
         * 
         */
        $this->depends = [
            'common\assets\PHPJSAsset',
        ];
        
        $this->checkNeedReloadAssets();

        parent::init();
    }

}
