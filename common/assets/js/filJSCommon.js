(function ($) {
    
    $('body').off('click.modal').on('click.modal', '.show-modal-button', showModalButtonClick);
    
    function showModalButtonClick(e, options){
        options = options || {};
        var $this = $(this);
        var $select2 = $this.closest('.input-group').find('select');
        var classList = $this.attr('class').split(/\s+/);
        var isMultiSelectBtn = false;
        $.each(classList, function(index, item) {
            if (item.indexOf('btn-multi-select') >= 0){
                isMultiSelectBtn = true;
            }
        });
        if (isMultiSelectBtn && !options.proceed) {
            return false;
        }
        
        var currentUrl = window.location.toString();
        
        var modalWindow = $('#modal-source-window').clone();
        if (modalWindow.length === 0) {
            return false;
        }
        modalWindow.data('select2', $select2);
        modalWindow.data('currentUrl', currentUrl);

        modalWindow.on('shown.bs.modal', function (event) {
            var $this = $(this);

            // keep track of the number of open modals
            if (typeof ($('body').data('fv_open_modals')) == 'undefined'){
                $('body').data('fv_open_modals', 0);
            }

            // if the z-index of this modal has been set, ignore.
            if ($this.hasClass('fv-modal-stack')){
                return;
            }

            var fvOpenModals = $('body').data('fv_open_modals');
            $this.attr('id', 'modal-window-' + fvOpenModals);

            $this.addClass('fv-modal-stack');
            $('body').data('fv_open_modals', fvOpenModals + 1);

            $this.css('z-index', 1030 + (10 * fvOpenModals));
            $('.modal-backdrop').not('.fv-modal-stack')
                .addClass('fv-modal-stack')
                .css('z-index', 1029 + (10 * fvOpenModals));
        
            setTimeout(function (){
                $this.find("input[tabindex='1']").first().focus();
            }, 500);        
        });  

        modalWindow.on('hidden.bs.modal', function (e) {
            var $this = $(this);
            var $body = $(document.body);
            $this.remove();
            var fvOpenModals = $('body').data('fv_open_modals') - 1;
            $('body').data('fv_open_modals', fvOpenModals);
            if((fvOpenModals > 0) && !$body.hasClass('modal-open')){
                $body.addClass('modal-open');
            }
        })
        
        var title = $this.attr('title');
        if(empty(title)){
            title = $this.attr('header-title');
        }
        modalWindow.find('#modalHeaderTitle').html('<h4>' + title + '</h4>');

        modalWindow
            .prependTo('#modal-window-container')
            .draggable({
                handle: "#modalHeader"
            })
            .find('#modalContent')
            .load($this.attr('value'), function(){
                modalWindow.modal({"show": true, "backdrop": "static", "keyboard": false});
            });
    }
    
    $(document).on('click', '.refresh-list-button', function () {
        var $this = $(this);
        var url = $this.attr('value');
        var $select2 = $this.closest('.input-group').find('select');
        $.post(
            url, 
            null, 
            function (result) {
                var data = result.data;
                var id = $($select2).attr('id');
                var $s2Options = $($select2).attr('data-s2-options');
                var configSelect2 = eval($($select2).attr('data-krajee-select2'));
                configSelect2.data = data;
                
                $('#'+id).find('option:not(:first)').remove();
                $.when($select2.select2(configSelect2)).done(initS2Loading(id, $s2Options));
            }, 
            'json'
        );
    });
    
    $(document).on('pjax:start', '#dynagrid-bill-pjax', function (contents, options) {
        var popoverList = $('.kv-popover-active');
        if(popoverList.length > 0){
            popoverList.popoverX('hide');
        }
    })
    
    $(document).on('pjax:end', '#dynagrid-bill-pjax', function (contents, options) {
        var $btns = $("[data-toggle='popover-x']");
        if ($btns.length > 0) {
            $btns.popoverButton();
        }
    })    
    
    $(document).on('pjax:start', '.modal-pjax', function (contents, options) {
        var $this = $(this);
        var modalWindow = $this.closest('.modal');
        modalWindow.hide();
    })
    
    $(document).on('submit', '.modal form[data-pjax]', function(event) {
        //return false;
    })     

    $(document).on('pjax:error', '.modal-pjax', function (event, xhr, textStatus, options) {
        var $this = $(this);
        //alert('Failed to load the page');
        event.preventDefault();
        if (textStatus == 'abort') {
            var modalWindow = $this.closest('.modal');
            window.location = modalWindow.data('currentUrl');
        } 
    })

    $(document).on('pjax:success', '.modal-pjax', function (event, xhr, textStatus, options) {
        var $this = $(this);
        var modalWindow = $this.closest('.modal');
        var $select2 = modalWindow.data('select2');
        modalWindow.modal('hide');

        var result = JSON.parse(xhr);
        if(result && (result.data != undefined)){
            var data = result.data;
            var selectedId = result.selected;
            var $s2Options = $($select2).attr('data-s2-options');
            var depdropOptions = $($select2).attr('data-krajee-depdrop');

            var configSelect2 = eval($($select2).attr('data-krajee-select2'));
            configSelect2.data = data;
            var id = $($select2).attr('id');
            $('#'+id).find('option:not(:first)').remove();
            if(depdropOptions != undefined){
                $($select2).on('depdrop:afterChange', function(){
                    $(this).val(selectedId).trigger("change");
                });
                depdropOptions = eval($($select2).attr('data-krajee-depdrop'));
                var depends = depdropOptions.depends;
                if(!empty(depends)){
                    var lastElement = depends[depends.length - 1];
                    $('#' + lastElement).trigger('depdrop:change');
                }
            }else{
                $.when($select2.select2(configSelect2)).done(initS2Loading(id, $s2Options));
                $($select2).val(selectedId).trigger("change");
            }
        }
        if(result == 'reload'){
            var timeoutTimer;
            timeoutTimer = window.setTimeout( function() {
                window.clearTimeout( timeoutTimer )
                location.reload(true);
            }, 500 );
        }
    });
    
    $(document).ready(function () {
        $('form div.required label').attr('title', 'Required field');
            
        var resetButton = $('form button[type=reset]');
        resetButton.click(function(){
            var href = document.location.toString().split('?', 1)[0];
            document.location = href;
            return false;
        });
    });
    
    $(document).on('change', '.rate-changer-date', function () {
        var $this = $(this);
        var currencyInputId = $this.data('currency_input_id');
        var rateInputId = $this.data('rate_input_id');
        if((currencyInputId.length == 0) || (rateInputId.length == 0)){
            return;
        }
        
        var $form = $this.closest('form');
        var currencyInput = $form.find('#' + currencyInputId);
        var rateInput = $form.find('#' + rateInputId);
        if((currencyInput.length == 0) || (rateInput.length == 0)){
            return;
        }
        
        var dateValue = $this.val();
        var currencyValue = currencyInput.find('option:selected').html();
        if((dateValue.length == 0) || (currencyValue.length == 0)){
            return;
        }
        
        changeCurrenciRate(rateInput, currencyValue, dateValue);
    });
    
    $(document).on('change', '.rate-changer-currency', function () {
        var $this = $(this);
        var dateInputId = $this.data('date_input_id');
        var rateInputId = $this.data('rate_input_id');
        if(rateInputId.length == 0){
            return;
        }
        
        var $form = $this.closest('form');
        var dateInput = (dateInputId != undefined) ? $form.find('#' + dateInputId) : null;
        var rateInput = $form.find('#' + rateInputId);
        if(rateInput.length == 0){
            return;
        }
        
        var dateValue = (dateInput != null) ? dateInput.val() : null;
        var currencyValue = $this.find('option:selected').html();
        if(currencyValue.length == 0){
            return;
        }
        
        changeCurrenciRate(rateInput, currencyValue, dateValue);
    });
    
    function changeCurrenciRate(rateInput, code, date){
        var url = appUrl + '/valuta/ajax-get-currency-rate';
        $.get(
            url,
            {code: code, date: date}, 
            function (data) {
                rateInput.val(data).change();
            }
        );
    }
    
})(jQuery);

function checkRequiredInputs(form){
    /*
    var timeoutTimer;
    timeoutTimer = window.setTimeout( function() {
        var requiredInputs = form.find('.form-group.required select, [aria-required="true"]');
        $.each(requiredInputs, function (index, input) {
            var id = $(input).attr('id');
            form.yiiActiveForm('updateAttribute', id);
        });
        window.clearTimeout( timeoutTimer )
    }, 500 );
    */
    var requiredInputs = form.find('.form-group.required input, .form-group.required select, [aria-required="true"]');
    $.each(requiredInputs, function (index, input) {
        var id = $(input).attr('id');
        form.yiiActiveForm('updateAttribute', id);
    });
}


function filRound(value, decimals) {
    var result = number_format(value, decimals, '.', '');
    /*
    var result = round(value, decimals);
    var parts = explode('.', result);
    if (parts.length == 1) {
        result = result + '.' + str_pad('', decimals, '0');
    } else {
        result = parts[0] + '.' + str_pad(parts[1], decimals, '0');
    }
     * 
     */
    return result;
}