<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body style="font-family: 'Tahoma'; font-size: 12px; color: #434343">
        <?php $this->beginBody() ?>
        <br/>
        
        <p style="line-height: 1.6; margin: 0 0 10px; padding: 0;">
            <?= (!empty($clientMailTemplate->header) ? nl2br($clientMailTemplate->header) : Yii::t('common', 'Welcome').', '.($toContact->first_name.' '.$toContact->last_name).'!'); ?>
        </p>
        <p style="line-height: 1.6; margin: 0 0 10px; padding: 0;">
            <?php 
            if(!empty($clientMailTemplate->body)): 
                echo nl2br($clientMailTemplate->body); 
            else : 
                echo Yii::t('bill', '{0} sent to You {2} #{1}', [
                    $content['client_name'], 
                    $content['doc_number'], 
                    ($bill->mail_status == \common\models\bill\Bill::BILL_MAIL_STATUS_NONE ? Yii::t('bill', 'new invoice') : Yii::t('bill', 'repeated invoice')),
                ]).'.'; 
            endif 
            ?>
            <br/>
            <?= Yii::t('bill', 'Please click the link below to view this invoice') ?>.
        </p>
        <p style="line-height: 1.6; margin: 0 0 10px; padding: 0;">
            <?= Html::a(Yii::t('bill', 'View invoice'), $viewPDFUrl); ?>
        </p>
        
        <br/>
        <?php 
        if(!empty($clientMailTemplate->footer)): 
            echo nl2br($clientMailTemplate->footer); 
        else : 
            echo '<strong>'.Yii::t('common', 'Best regards').',</strong>'; 
        endif 
        ?>
        <br/>
        <br/>
        <?= $contacts; ?>
        <?php if(!empty($logoImageFileName)) : ?>
        <p>
            <img src="<?= $message->embed($logoImageFileName); ?>" width="160"/>
        </p>
        <?php endif; ?>
        <img alt="" src="<?= $receiveMailUrl; ?>" width="1" height="1" border="0" />
        <?php $this->endBody() ?>
    </body>
</html>