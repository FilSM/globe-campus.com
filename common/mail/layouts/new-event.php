<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */

?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body style="font-family: 'Tahoma'; font-size: 12px; color: #434343">
        <?php $this->beginBody() ?>
        <br/>
        
        <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
            <?= Yii::t('common', 'Welcome to {0}!', Yii::$app->params['brandOwner']) ?>,
        </p>
        <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
            <?= Yii::t('event', 'For You in the {0} was registered new event:', Yii::$app->params['brandOwner']);?>
            <br/>
            <b><?= $content; ?></b>
            <br/>
            <?php if(!empty($description)) : ?>
            <?= $description; ?>.
            <br/>
            <?php endif; ?>
            <br/>
            <?= Yii::t('event', 'Please click the link below to accept this event') ?>.
        </p>
        <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
            <?= Html::a(Html::encode($tokenUrl), $tokenUrl); ?>
        </p>
        <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
            <?= Yii::t('user', 'If you cannot click the link, please try pasting the text into your browser') ?>.
        </p>
        
        <br/>
        <br/>
        <strong>Best regards,</strong><br/><br/>
        <?= $contacts; ?>
        <?php if(!empty($logoImageFileName)) : ?>
        <img src="<?= $message->embed($logoImageFileName); ?>" width="160"/>
        <?php endif; ?>
        <?php $this->endBody() ?>
    </body>
</html>