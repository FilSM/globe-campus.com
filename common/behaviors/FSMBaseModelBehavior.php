<?php

namespace common\behaviors;

use yii;
use yii\base\Exception;
use yii\base\Behavior;
use yii\db\ActiveRecord;

use common\models\mainclass\FSMBaseModel;

/**
 * 
 */
class FSMBaseModelBehavior extends Behavior {

    public function events() {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
            FSMBaseModel::EVENT_BEFORE_MARK_AS_DELETED => 'beforeMarkAsDeleted',
        ];
    }

    public function beforeValidate() {
        
    }

    public function beforeSave($event) {
        if ($this->owner->hasAttribute('deleted')) {
            $this->owner->deleted = empty($this->owner->deleted) ? 0 : 1;
        }
    }

    public function beforeDelete($event) {
        $deletedModel = $this->owner;
        if(!empty($deletedModel->cascadeDeleting)){
            // need to delete all related rows from other models
            $relatedModels = $deletedModel->relations;
            $ignoredFieldList = $deletedModel->ignoredFieldsForDelete;
            $result = true;
            try {
                foreach ($relatedModels as $key => $rm) {
                    if(empty($rm) || in_array($rm['field'], $ignoredFieldList)){
                        continue;
                    }

                    $method = new \ReflectionMethod($deletedModel, 'get' . $key);
                    $key = lcfirst(substr($method->getName(), 3));                
                    if(!$rm['hasMany']){
                        if($model = $deletedModel->{$key}){
                            if(!is_array($model)){
                                if(!$model->hasAttribute('deleted')){
                                    $event->isValid = $deletedModel->updateAttributes([$rm['field'] => null]);
                                }
                                $result = $model->delete();
                                $event->isValid = $event->isValid && ($result !== false);
                            }else{
                                foreach ($model as $child) {
                                    $result = $child->delete();
                                    $event->isValid = $event->isValid && ($result !== false);
                                }
                            }                            
                        }
                    }else{
                        $models = $deletedModel->{$key};
                        if(!is_array($models)){
                            if(!empty($models)){
                                $result = $models->delete();
                                $event->isValid = $event->isValid && ($result !== false);
                            }
                        }else{
                            foreach ($models as $model) {
                                $result = $model->delete();
                                $event->isValid = $event->isValid && ($result !== false);
                            }
                        }
                    }
                    if(!$event->isValid){
                        break;
                    }
                }                
            } catch (Exception $e) {
                //echo $exc->getTraceAsString();
                $message = $e->getMessage();
                Yii::$app->getSession()->setFlash('error', $message);
                $event->isValid = false;
            } finally {
                
            }
        }
    }    

    public function beforeMarkAsDeleted($event) {
        if (!$this->owner->hasAttribute('deleted')) {
            throw new Exception('The model does not have the field "Deleted".');
        }
        $this->beforeDelete($event);
    }
    
}
