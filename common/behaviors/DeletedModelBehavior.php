<?php

namespace common\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * 
 */
class DeletedModelBehavior extends Behavior
{

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
        ];
    }

    public function beforeDelete($event)
    {
        $result = true;
        $attchmentList = $this->owner->attachmentFiles;
        foreach ($attchmentList as $attchment) {
            $result = $result && $attchment->delete();
        }
        $event->isValid = $result;
        $event->handled = !$result;
    }

}
