<?php

namespace common\behaviors;

use Yii;
use yii\base\Behavior;

use dektrium\user\controllers\SecurityController;

use common\components\FSMAccessHelper;
use common\models\abonent\Abonent;
use common\models\user\FSMUser;
use common\models\user\ProfileClient;

/**
 * 
 */
class FSMSecurityBehavior extends Behavior
{

    public function events()
    {
        return [
            SecurityController::EVENT_AFTER_LOGIN => 'afterLogin',
        ];
    }

    public static function afterLogin($event)
    {
        $user = Yii::$app->user->identity;
        if (!isset($user)) {
            return;
        }

        $profile = $user->profile;

        $session = Yii::$app->session;
        if (!empty(Yii::$app->params['SMS_AUTH'])) {
            if ($profile && $profile->sms_auth) {
                $session->set('sms-auth-complete', true);
            } else {
                Yii::$app->getSession()->remove('sms-auth-complete');
            }
        }

        $session->set('user-after-login', true);

        if (empty(Yii::$app->params['ABONENT_ENABLED'])) {
            return;
        }
        
        $session->set('user_client_id', null);
        $session->set('user_client_it_is', null);
        $session->set('user_abonent_id', null);
        $session->set('user_abonent_name', null);
        $session->set('user_current_abonent_id', null);

        $user_client_id = $user_client_it_is = $user_abonent_id = $user_abonent_name = [];
        $abonentList = Abonent::find()->all();
        if (FSMUser::getIsPortalAdmin() || FSMAccessHelper::can('administrateUser')) {
            foreach ($abonentList as $key => $abonent) {
                if ($key == 0) {
                    $session->set('user_current_abonent_id',$abonent->id);
                    $session->set('user_current_abonent_workflow', $abonent->simple_workflow);
                }
                $user_abonent_id[] = $abonent->id;
                $user_abonent_name[] = $abonent->name;
            }
            $session->set('user_abonent_id', $user_abonent_id);
            $session->set('user_abonent_name', $user_abonent_name);
        }

        $clientList = $user->clientsWithoutScope;
        if (!empty($clientList)) {
            $mainClientList = [];
            foreach ($abonentList as $abonent) {
                $mainClientList[$abonent->id] = $abonent->main_client_id;
            }

            foreach ($clientList as $key => $client) {
                $user_client_id[] = $client->id;
                $user_client_it_is[] = $client->it_is;

                if($user->hasRole([
                    FSMUser::USER_ROLE_CLIENT, 
                    FSMUser::USER_ROLE_CONTRAGENT, 
                    ])
                ){
                    $profileClient = ProfileClient::findOne([
                        'profile_id' => $profile->id,
                        'client_id' => $client->id,
                    ]);
                    if (!empty($profileClient) && !empty($profileClient->abonent_id)) {
                        $abonent = Abonent::findOne($profileClient->abonent_id);
                        if ($key == 0) {
                            $session->set('user_current_abonent_id', $abonent->id);
                            $session->set('user_current_abonent_workflow', $abonent->simple_workflow);
                        }
                        if (!in_array($abonent->id, $user_abonent_id)) {
                            $user_abonent_id[] = $abonent->id;
                            $user_abonent_name[] = $abonent->name;
                        }
                    }
                }elseif ($id = array_search($client->id, $mainClientList)) {
                    if ($abonent = Abonent::findOne($id)) {
                        if ($key == 0) {
                            $session->set('user_current_abonent_id', $abonent->id);
                            $session->set('user_current_abonent_workflow', $abonent->simple_workflow);
                        }
                        if (!in_array($abonent->id, $user_abonent_id)) {
                            $user_abonent_id[] = $abonent->id;
                            $user_abonent_name[] = $abonent->name;
                        }
                    }
                } else {
                    $abonentClientList = $client->abonentClients;
                    foreach ($abonentClientList as $aKey => $abonentClient) {
                        if(!empty($abonentClient->client_group_id)){
                            if ($abonent = Abonent::findOne($abonentClient->abonent_id)) {
                                if(!$currentAbonentId = $session->get('user_current_abonent_id')){
                                    $session->set('user_current_abonent_id', $abonent->id);
                                    $session->set('user_current_abonent_workflow', $abonent->simple_workflow);
				}
                                if (!in_array($abonent->id, $user_abonent_id)) {
                                    $user_abonent_id[] = $abonent->id;
                                    $user_abonent_name[] = $abonent->name;
                                }
                            }
                        }
                    }
                }
            }

            $session->set('user_client_id', $user_client_id);
            $session->set('user_client_it_is', $user_client_it_is);
            if (empty($session->get('user_abonent_id'))) {
                $session->set('user_abonent_id', $user_abonent_id);
                $session->set('user_abonent_name', $user_abonent_name);
            }
        }
    }

}
