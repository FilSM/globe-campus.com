<?php

namespace common\components;

use kartik\nav\NavX;
use kartik\nav\NavXBs4;
use kartik\nav\NavXBs3;

use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class FSMNavX extends NavX
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        $opts = [
            'items' => $this->items,
            'encodeLabels' => $this->encodeLabels,
            'activateItems' => $this->activateItems,
            'activateParents' => $this->activateParents,
            'dropdownOptions' => $this->dropdownOptions,
            'options' => $this->options,
            'clientOptions' => $this->pluginOptions,
        ];
        $props = ['route', 'params', 'dropdownClass'];
        foreach ($props as $prop) {
            if (isset($this->$prop)) {
                $opts[$prop] = $this->$prop;
            }
        }
        echo $this->isBs4() ? NavXBs4::widget($opts) : FSMNavXBs3::widget($opts);
    }
}

class FSMNavXBs3 extends NavXBs3
{
    /**
     * Renders a widget's item.
     * @param string|array $item the item to render.
     * @return string the rendering result.
     * @throws InvalidConfigException
     */
    public function renderItem($item)
    {
        if (is_string($item)) {
            return $item;
        }
        if (!isset($item['label'])) {
            throw new InvalidConfigException("The 'label' option is required.");
        }
        $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
        $label = $encodeLabel ? Html::encode($item['label']) : $item['label'];
        $options = ArrayHelper::getValue($item, 'options', []);
        $items = ArrayHelper::getValue($item, 'items');
        $url = ArrayHelper::getValue($item, 'url', null);
        $linkOptions = ArrayHelper::getValue($item, 'linkOptions', []);

        if (isset($item['active'])) {
            $active = ArrayHelper::remove($item, 'active', false);
        } else {
            $active = $this->isItemActive($item);
        }

        if (empty($items)) {
            $items = '';
        } else {
            //$linkOptions['data-toggle'] = 'dropdown';
            Html::addCssClass($options, ['widget' => 'dropdown']);
            Html::addCssClass($linkOptions, ['widget' => 'dropdown-toggle']);
            if ($this->dropDownCaret !== '') {
                $label .= ' ' . $this->dropDownCaret;
            }
            if (is_array($items)) {
                $items = $this->isChildActive($items, $active);
                $items = $this->renderDropdown($items, $item);
            }
        }

        if ($active) {
            Html::addCssClass($options, 'active');
        }

        return Html::tag('li', Html::a($label, $url, $linkOptions) . $items, $options);
    }    
    
}
