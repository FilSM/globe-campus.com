<?php
namespace common\components\fidavista;

use omj\financetools\statementstandart\iAccountStatementDbController;

class FSMFidavistaDbController implements iAccountStatementDbController
{

    public $transactData;

    public function executeDataImport() {
        
    }

    public function executeDataImportIncoming() {
        
    }

    public function setData($data) {
        $this->transactData = $data;
    }

}
