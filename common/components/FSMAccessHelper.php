<?php

namespace common\components;

use Yii;

use common\models\user\FSMUser;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class FSMAccessHelper extends \mdm\admin\components\Helper
{

    public static function checkSMSAuth(){
        if(!empty(Yii::$app->session->get('original_user'))){
            return true;
        }
        if (!empty(Yii::$app->params['SMS_AUTH']) && !\Yii::$app->user->isGuest) {
            $user = Yii::$app->user->identity;
            if(isset($user) && ($profile = $user->profile) && $profile->sms_auth){
                return empty($profile->sms_code);
            }
        }        
        return true;
    }

    /**
     * Check access route for user.
     * @param string|array $route
     * @param integer|User $user
     * @return boolean
     */
    public static function checkRoute($route, $params = [], $user = null)
    {
        if(FSMUser::getIsPortalAdmin()){
            return true;
        }
        if(!FSMAccessHelper::checkSMSAuth()){
            return false;
        }
        return parent::checkRoute($route, $params, $user);
    }    
    
    /**
     * Checks if the user can perform the operation as specified by the given permission.
     *
     * @return bool whether the user can perform the operation as specified by the given permission.
     */
    public static function can($permissionName, $model = null, $params = [], $allowCaching = true)
    {
        if(FSMUser::getIsPortalAdmin()){
            return true;
        }
        if(!FSMAccessHelper::checkSMSAuth()){
            return false;
        }
        if(self::canAny($permissionName, $params, $allowCaching)){
            return true;
        }        
        if(isset($model) && self::canSelf($permissionName, $model, $params, $allowCaching)){
            return true;
        }
        return Yii::$app->user->can($permissionName, $params, $allowCaching);
    }    
    
    /**
     * Checks if the user can perform the operation as specified by the given permission.
     *
     * @return bool whether the user can perform the operation as specified by the given permission.
     */
    public static function canAny($permissionName, $params = [], $allowCaching = true)
    {
        return Yii::$app->user->can($permissionName.'Any', $params, $allowCaching);
    }
    
    /**
     * Checks if the user can perform the operation as specified by the given permission.
     *
     * @return bool whether the user can perform the operation as specified by the given permission.
     */
    public static function canSelf($permissionName, $model = null, $params = [], $allowCaching = true)
    {
        $result = Yii::$app->user->can($permissionName.'Self', $params, $allowCaching);
        if($result && isset($model) && !empty($model->create_user_id) && ($model->create_user_id == Yii::$app->user->getId())){
            return true;
        }
        return false;
    }     
}
