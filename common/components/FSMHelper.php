<?php

namespace common\components;

use Yii;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\helpers\Html;

class FSMHelper extends BaseObject
{
    static private $say = [
        'nulle', 'viens', 'divi', 'trīs', 'četri', 'pieci', 'seši', 'septiņi', 'astoņi', 'deviņi', 'desmit',
        'vienpadsmit', 'divpadsmit', 'trīspadsmit', 'četrpadsmit', 'piecpadsmit', 'sešpadsmit',
        'septiņpadsmit', 'astoņpadsmimt', 'deviņpadsmit',
        'divdesmit', 30 => 'trīsdemit', 40 => 'četrdesmit', 50 => 'piecdesmit', 60 => 'sešdesmit',
        70 => 'septiņdesmit', 80 => 'astoņdesmit', 90 => 'deviņdesmit',
        '100' => 'simts', '100x' => 'simti', '1000' => 'tūkstotis', '1000x' => 'tūkstoši',
        '1000000' => 'miljons', '1000000x' => 'miljoni', '1000000000' => 'miljards', '1000000000x' => 'miljardi',
        'ls' => 'eiro', 'lsx' => 'eiro', 'snt' => 'cents', 'sntx' => 'centi', 'snt0' => 'centu'
    ];
    
    static private function say($summa) {
        if (($floorPart = floor($summa / 1000000000)) >= 1)
            return self::say($floorPart) . ' ' . self::$say['1000000000' . ($floorPart > 1 ? 'x' : '')] . ' ' . self::say($summa - $floorPart * 1000000000);
        if (($floorPart = floor($summa / 1000000)) >= 1)
            return self::say($floorPart) . ' ' . self::$say['1000000' . ($floorPart > 1 ? 'x' : '')] . ' ' . self::say($summa - $floorPart * 1000000);
        if (($floorPart = floor($summa / 1000)) >= 1)
            return self::say($floorPart) . ' ' . self::$say['1000' . ($floorPart > 1 ? 'x' : '')] . ' ' . self::say($summa - $floorPart * 1000);
        if (($floorPart = floor($summa / 100)) >= 1)
            return self::say($floorPart) . ' ' . self::$say['100' . ($floorPart > 1 ? 'x' : '')] . ' ' . self::say($summa - $floorPart * 100);
        if (($floorPart = floor($summa / 10)) >= 2)
            return self::$say[$floorPart * 10] . ' ' . self::say($summa - $floorPart * 10);
        if ($summa > 0)
            return self::$say[$summa];
    }
    
    static public function getSummaToLVWords($summa = null){
        return FSMHelper::my_ucfirst(
            self::say($floorPart = floor($summa)) . ' ' . //[(n)]n
            self::$say['ls' . (floor($summa - floor($summa / 10) * 10) > 1 ? 'x' : (($summa % 100 == 0) ? 'x' : ''))] . ', ' . //ls
            str_pad($floorPart = round($summa - $floorPart, 2) * 100, 2, 0, STR_PAD_LEFT) . ' ' . //nn
            self::$say['snt' . (($floorPart = ($floorPart % 10)) > 1 ? 'x' : ($floorPart == 0 ? '0' : ''))] //snt
        );
    }

    static public  function getSummaToWords($sum = null, $locale = '', $currency = 'EUR')
    {
        if (!$sum) {
            return '';
        }
        if (empty($locale)) {
            $locale = Yii::$app->language;
        }
        list($sumInt, $sumFloor) = preg_split("/[.,]/", (string) $sum);
        $oldLng = Yii::$app->language;
        Yii::$app->language = $locale;
        $sumWords = Yii::$app->formatter->asSpellout($sumInt);
        $sumWords = FSMHelper::my_ucfirst($sumWords);

        if (!$currencyModule = \common\models\Valuta::findOne(['name' => $currency])) {
            $currencyModule = \common\models\Valuta::findOne(['name' => 'EUR']);
        }

        $intPart = '{count, plural, =0{ ' . $currencyModule->int_0 . '} =1{ ' . $currencyModule->int_1 . '} other{ ' . $currencyModule->int_2 . '}}';
        $floorPart = '{count, plural, =0{ ' . $currencyModule->floor_0 . '} =1{ ' . $currencyModule->floor_1 . '} other{ ' . $currencyModule->floor_2 . '}}';
        $sumWords = $sumWords .
            Yii::t('common', $intPart, ['count' => $sumInt]) . ', ' . (!empty($sumFloor) ? $sumFloor : '00') .
            Yii::t('common', $floorPart, ['count' => $sumFloor]);
        Yii::$app->language = $oldLng;
        return $sumWords;
    }
    
    static public function toFloat($num, $decimals = null)
    {
        $dotPos = strrpos($num, '.');
        $commaPos = strrpos($num, ',');
        $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
                ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);

        if (!$sep) {
            return floatval(preg_replace("/[^0-9]/", "", $num));
        }

        $result = floatval(
                preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
                preg_replace("/[^0-9]/", "", substr($num, $sep + 1, strlen($num)))
        );

        return !$decimals ? $result : floatval(number_format($result, $decimals, '.', ' '));
    }
    
    static public function my_ucfirst($string, $e ='utf-8') { 
        if (function_exists('mb_strtoupper') && function_exists('mb_substr') && !empty($string)) { 
            $string = mb_strtolower($string, $e); 
            $upper = mb_strtoupper($string, $e); 
                preg_match('#(.)#us', $upper, $matches); 
                $string = $matches[1] . mb_substr($string, 1, mb_strlen($string, $e), $e); 
        } 
        else { 
            $string = ucfirst($string); 
        } 
        return $string; 
    } 

    static public function sentence_case($string) { 
        $sentences = preg_split('/([.?!]+)/', $string, -1, PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE); 
        $new_string = ''; 
        foreach ($sentences as $key => $sentence) { 
            $new_string .= ($key & 1) == 0? 
                my_ucfirst(strtolower(trim($sentence))) : 
                $sentence.' ';  
        } 
        return trim($new_string); 
    }

    static public function aButton($id, $options = [])
    {
        $options = ArrayHelper::merge(
            [
                'label' => '',
                'title' => '',
                'icon' => '',
                'controller' => '',
                'action' => '',
                'url' => '',
                'query' => '',
                'class' => 'success',
                'size' => '',
                'message' => '',
                'idName' => 'id',
                'options' => [
                    'data-pjax' => 0,
                ],
            ], $options
        );
        /*
          if(empty($options['message'])){
          $options['message'] = Yii::t('common', 'Are you sure you want to do this operation?');
          }
         * 
         */

        $url = !empty($options['url']) ? 
            $options['url'] : 
            (!empty($options['action']) && ($options['action'] != '#') ?
                [(!empty($options['controller']) ? '/' . $options['controller'] . '/' : '') . $options['action'], $options['idName'] => $id] :
                '#'
            );
        
        if(!empty($options['data'])){
            if(is_array($url)){
                $url = $url + (array)$options['data'];
            }else{
                $query = [];
                foreach ($options['data'] as $key => $value) {
                    $query[] = $key.'='.$value;
                }
                $url .= implode('&', $query);
            }
        }
        if(!empty($options['query'])){
            if(is_array($url)){
                $url = Url::to($url);
            }
            $url = $url.$options['query'];
        }
        
        $class = empty($options['dropdown']) ?
            'btn btn-' . $options['class'] . (!empty($options['size']) ? ' ' . $options['size'] : '') :
            (!empty($options['class']) ? $options['class'] : '');
        $btnOptions = [
            'title' => !empty($options['title']) ? $options['title'] : null,
            'class' => $class,
            'data-method' => !empty($options['message']) ? 'post' : null,
            'data-confirm' => !empty($options['message']) ? $options['message'] : null,
        ];
        $btnOptions = empty($options['options']) ? $btnOptions : ArrayHelper::merge($btnOptions, $options['options']);

        return Html::a(Html::icon($options['icon']) . (!empty($options['label']) ? ' ' . $options['label'] : ''), $url, $btnOptions);
    }

    static public function vButton($id, $options = [])
    {
        $options = ArrayHelper::merge(
            [
                'label' => '',
                'title' => '',
                'icon' => '',
                'controller' => '',
                'action' => '',
                'url' => '',
                'query' => '',
                'class' => 'success',
                'modal' => false,
                'size' => '',
                'message' => '',
                'idName' => 'id',
                'options' => [
                    'type' => 'button',
                    'data-pjax' => 0,
                ],
            ], $options
        );

        $url = !empty($options['url']) ? $options['url'] :
            (
                !empty($options['action']) && ($options['action'] != '#') ?
                [(!empty($options['controller']) ? '/' . $options['controller'] . '/' : '') . $options['action'], $options['idName'] => $id] :
                '#'
            );
        
        if(!empty($options['data'])){
            if(is_array($url)){
                $url = $url + (array)$options['data'];
            }else{
                $query = [];
                foreach ($options['data'] as $key => $value) {
                    $query[] = $key.'='.$value;
                }
                $url .= implode('&', $query);
            }
        }
        if(!empty($options['query'])){
            if(is_array($url)){
                $url = Url::to($url);
            }
            $url = $url.$options['query'];
        }
        
        $class = empty($options['dropdown']) ?
            'btn btn-' . $options['class'] . (!empty($options['size']) ? ' ' . $options['size'] : '') :
            (!empty($options['class']) ? $options['class'] : '');
        $btnOptions = [
            'title' => $options['title'],
            'class' => $class . (!empty($options['modal']) ? ' show-modal-button' : ''),
            'data-method' => !empty($options['message']) ? 'post' : null,
            'data-confirm' => !empty($options['message']) && empty($options['modal']) ? $options['message'] : null,
            'value' => !empty($options['modal']) ? Url::to($url) : null,
        ];
        $btnOptions = empty($options['options']) ? $btnOptions : ArrayHelper::merge($btnOptions, $options['options']);

        return Html::button(Html::icon($options['icon']) . (!empty($options['label']) ? ' ' . $options['label'] : ''), $btnOptions);
    }

    static public function aDropdown($id, $options = [])
    {
        $options['dropdown'] = true;
        return '<li>' . FSMHelper::aButton($id, $options) . '</li>' . PHP_EOL;
    }

    static public function vDropdown($id, $options = [])
    {
        $options = ArrayHelper::merge(
            [
                'label' => '',
                'title' => '',
                'icon' => '',
                'controller' => '',
                'action' => '',
                'url' => '',
                'query' => '',
                'class' => 'a-button',
                'message' => '',
                'idName' => 'id',
                'options' => [
                    'data-pjax' => 0,
                ],
            ], $options
        );
        /*
          if(empty($options['message'])){
          $options['message'] = Yii::t('common', 'Are you sure you want to do this operation?');
          }
         * 
         */

        $url = !empty($options['url']) ? $options['url'] :
            (
                !empty($options['action']) && ($options['action'] != '#') ?
                [(!empty($options['controller']) ? '/' . $options['controller'] . '/' : '') . $options['action'], $options['idName'] => $id] :
                '#'
            );
        
        if(!empty($options['data'])){
            if(is_array($url)){
                $url = $url + (array)$options['data'];
            }else{
                $query = [];
                foreach ($options['data'] as $key => $value) {
                    $query[] = $key.'='.$value;
                }
                $url .= implode('&', $query);
            }
        }
        if(!empty($options['query'])){
            if(is_array($url)){
                $url = Url::to($url);
            }
            $url = $url.$options['query'];
        }
        
        $class = (!empty($options['class']) ? $options['class'] : '');
        $btnOptions = [
            'title' => $options['title'],
            'class' => $class . (!empty($options['modal']) ? ' show-modal-button' : ''),
            'data-method' => !empty($options['message']) ? 'post' : null,
            'data-confirm' => !empty($options['message']) && empty($options['modal']) ? $options['message'] : null,
            'value' => !empty($options['modal']) ? Url::to($url) : null,
        ];
        $btnOptions = empty($options['options']) ? $btnOptions : ArrayHelper::merge($btnOptions, $options['options']);

        return Html::tag('li', Html::icon($options['icon']) . (!empty($options['label']) ? ' ' . $options['label'] : ''), $btnOptions) . PHP_EOL;
    }

    static function arrayRemove($arr, $item)
    {
        ArrayHelper::remove($arr, $item);
        return $arr;
    }
}
