<?php

namespace common\components;

use yii\base\BaseObject;

class FSMViewHelper extends BaseObject
{

    public static function getImagesUploadHandler()
    {
        return "
            function (blobInfo, success, failure) {
                var xhr, formData;

                xhr = new XMLHttpRequest();
                xhr.withCredentials = false;
                xhr.open('POST', 'upload-image');

                xhr.onload = function() {
                    var json;
                    if (xhr.status != 200) {
                        failure('HTTP Error: ' + xhr.status);
                        return;
                    }
                    json = JSON.parse(xhr.responseText);

                    if (!json || typeof json.location != 'string') {
                        failure('Invalid JSON: ' + xhr.responseText);
                        return;
                    }
                    success(json.location);

                };

                formData = new FormData();
                formData.append('file', blobInfo.blob(), blobInfo.filename());
                xhr.send(formData);
            }
        ";
    }
    
    public static function getTinyMceMenu()
    {
        return [
            'file' => [
                'title' => 'File',
                'items' => 'preview',
            ],
        ];
    }
    
    public static function getSelect2PluginOptions(array $params = [])
    {
        return [
            'allowClear' => (isset($params['model']) && isset($params['attr'])) ? !$params['model']->isAttributeRequired($params['attr']) : true,
            'width' => '100%',
        ];
    }

}
