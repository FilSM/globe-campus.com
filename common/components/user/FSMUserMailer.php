<?php

namespace common\components\user;

use Yii;

class FSMUserMailer extends \dektrium\user\Mailer
{

    /**
     * @param string $to
     * @param string $subject
     * @param string $view
     * @param array  $params
     *
     * @return bool
     */
    protected function sendMessage($to, $subject, $view, $params = [])
    {
        $mailer = $this->mailerComponent === null ? Yii::$app->mailer : Yii::$app->get($this->mailerComponent);

        if ($this->sender === null) {
            $this->sender = isset(Yii::$app->params['supportEmail']) ? Yii::$app->params['supportEmail'] : 'no-reply@example.com';
        }

        $htmlView = $this->viewPath.'/'.$view;
        $txtView = $this->viewPath.'/text/'.$view;
        return $mailer->compose(['html' => $htmlView, 'text' => $txtView], $params)
            ->setTo($to)
            ->setFrom($this->sender)
            ->setSubject($subject)
            ->send();
    }

}
