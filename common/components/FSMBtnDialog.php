<?php

namespace common\components;

use Yii;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;

class FSMBtnDialog extends BaseObject {
    
    static public function button($label, $url, $options) {
        $model = isset($options['model']) ? $options['model'] : null;
        $grid = isset($options['grid']) ? $options['grid'] : null;
        $confirm = isset($options['confirm']) ? $options['confirm'] : null;
        $dialogSettings = isset($options['DialogSettings']) ? $options['DialogSettings'] : null;
        ArrayHelper::remove($options, 'model');
        ArrayHelper::remove($options, 'grid');
        ArrayHelper::remove($options, 'confirm');
        ArrayHelper::remove($options, 'DialogSettings');
        
        $confirm = !empty($confirm) ? $confirm : 
            (isset($model) ? 
                Yii::t('common', 'Are you sure to delete this ').$model->modelTitle().'?' : 
                Yii::t('common', 'Are you sure to delete this item?')
            );
        $defaults = [
            'title' => Yii::t('common', 'Delete'), 
            'data-pjax' => 'false',
            'data-confirm' => $confirm,
            'data-method' => 'post',
        ];
        $css = $grid ? $grid . '-btn-dialog-selected' : 'btn-dialog-selected';
        $options = ArrayHelper::merge($defaults, $options);
        Html::addCssClass($options, $css);

        /*
        $view = Yii::$app->getView();
        $dialogOpts = Json::encode([
            'grid' => $grid,
            'css' => $css,
            'lib' => ArrayHelper::getValue($dialogSettings, 'libName', 'krajeeDialog'),
            'msg' => $confirm,
        ]);
        $js = "fsmDialogAction({$dialogOpts});";
        $view->registerJs($js);   
         * 
         */   
        
        // javascript for triggering the dialogs
        /*
        $js = 
            '$("#'.$css.'").on("click", function() {
                var href = $(this).attr("value");
                krajeeDialog.confirm("'.$confirm.'",
                    function (result) {
                        if (result) {
                            window.location = href;
                            //alert("Great! You accepted!");
                        } else {
                            //alert("Oops! You declined!");
                        }
                    }
                );
            });';
        $view->registerJs($js);        
         * 
         */
        
        return Html::a($label, $url, $options)."<span class='img-loader' style='display: none;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
    }

}
