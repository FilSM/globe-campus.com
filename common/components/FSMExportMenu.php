<?php

namespace common\components;

use Closure;
use Yii;
use yii\helpers\ArrayHelper;

use kartik\export\ExportMenu;

class FSMExportMenu extends ExportMenu
{
    /**
     * Sets visible columns for export
     */
    public function setVisibleColumns()
    {
        $columns = [];
        foreach ($this->columns as $key => $column) {
            $isActionColumn = $column instanceof ActionColumn;
            $isNoExport = in_array($key, $this->noExportColumns);
            //$isNoExport = in_array($key, $this->noExportColumns) || !in_array($key, $this->selectedColumns);
            if ($isActionColumn && !$isNoExport) {
                $this->noExportColumns[] = $key;
            }
            if (!empty($column->hiddenFromExport) || $isActionColumn || $isNoExport) {
                continue;
            }
            $columns[] = $column;
        }
        $this->_visibleColumns = $columns;
    }
    
    /**
     * Generates an output data row with the given data model and key.
     *
     * @param mixed   $model the data model to be rendered
     * @param mixed   $key the key associated with the data model
     * @param integer $index the zero-based index of the data model among the model array returned by [[dataProvider]].
     */
    public function generateRow($model, $key, $index)
    {
        $this->_endCol = 0;
        foreach ($this->getVisibleColumns() as $column) {
            $format = $this->enableFormatter && isset($column->format) ? $column->format : 'raw';
            $value = null;
            if ($column instanceof SerialColumn) {
                $value = $index + 1;
                $pagination = $column->grid->dataProvider->getPagination();
                if ($pagination !== false) {
                    $value += $pagination->getOffset();
                }
            } elseif (isset($column->content)) {
                $value = call_user_func($column->content, $model, $key, $index, $column);
            } elseif (method_exists($column, 'getDataCellValue')) {
                $value = $column->getDataCellValue($model, $key, $index);
            } elseif (isset($column->attribute)) {
                $value = ArrayHelper::getValue($model, $column->attribute, '');
            }
            $this->_endCol++;
            if (isset($value) && $value !== '' && isset($format)) {
                if(empty($column->xlFormat)){
                    $value = $this->formatter->format($value, $format);
                }
            } else {
                $value = '';
            }
            $cell = $this->setOutCellValue(
                $this->_objWorksheet,
                self::columnName($this->_endCol) . ($index + $this->_beginRow + 1),
                $value
            );
            if ($this->enableAutoFormat) {
                $this->autoFormat($model, $key, $index, $column, $cell);
            }
            $this->raiseEvent('onRenderDataCell', [$cell, $value, $model, $key, $index, $this]);
        } 
    }
  
}
