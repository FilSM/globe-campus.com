<?php

namespace common\components;

use kartik\grid\DataColumn;
use kartik\grid\BooleanColumn;

class FSMBooleanColumn extends BooleanColumn
{
    /**
     * @inheritdoc
     */
    public function getDataCellValue($model, $key, $index)
    {
        $value = DataColumn::getDataCellValue($model, $key, $index);
        if ($value !== null) {
            if(empty($value) || ($value === 1)){
                return $value ? $this->trueIcon : $this->falseIcon;
            }else{
                return $value;
            }
        }
        return $this->showNullAsFalse ? $this->falseIcon : $value;
    }
}
