<?php

namespace common\components;

use Yii;
use yii\httpclient\Client;

/**
 * Password helper.
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class FSMSMSCode
{
    /**
     * Wrapper for yii security helper method.
     *
     * @param $code
     * @param $hash
     *
     * @return bool
     */
    public static function validate($code, $hash)
    {
        return Yii::$app->security->validatePassword($code, $hash);
    }

    /**
     * Generates user-friendly random code containing at least one lower case letter, one uppercase letter and one
     * digit. The remaining characters in the code are chosen at random from those three sets.
     *
     * @param $length
     * @return string
     */
    public static function generate($length)
    {
        $sets = [
            'abcdefghjkmnpqrstuvwxyz',
            'ABCDEFGHJKMNPQRSTUVWXYZ',
            '23456789',
        ];
        $all = '';
        $code = '';
        foreach ($sets as $set) {
            $code .= $set[array_rand(str_split($set))];
            $all .= $set;
        }

        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++) {
            $code .= $all[array_rand($all)];
        }

        $code = str_shuffle($code);

        return $code;
    }
    
    public static function send($code, $phone)
    {
        if (YII_ENV_PROD) {
            $time = time();
            $client = new Client();
            $response = $client->createRequest()
                ->setMethod('GET')
                ->setUrl('https://api.teltel.lv/sms/send/text')
                ->setData([
                    'from' => 'GlobeCampus', 
                    'to' => $phone,
                    'message' => $code,
                    'message_id' => $time,
                    'X-API-KEY' => 'b68cfd5fc0e06971932bb823ef190705f92e95e0',
                ])
                ->send();
            return ($response->isOk && ($time == $response->data['sms_id']));
        }else{
            return true;
        }
    }
}
