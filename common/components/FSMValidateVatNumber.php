<?php

namespace common\components;

use SoapClient;
use SoapFault;
use DrawMyAttention\ValidateVatNumber\ValidateVatNumber;

class FSMValidateVatNumber extends ValidateVatNumber
{
    /**
     * @var SoapClient
     */
    protected $client;

    /**
     * @var null
     */
    public $response = null;    
    
    /**
     * ValidateVatNumber constructor.
     */
    public function __construct()
    {
        $this->client = new SoapClient($this->serviceUrl);
    }
    
    /**
     * @param $vatNumber
     * @return bool
     * @throws SoapFault
     */
    public function validate($vatNumber)
    {
        $vatNumber = $this->cleanVatNumber($vatNumber);

        $countryCode = substr($vatNumber, 0, 2);
        $vatNumber = substr($vatNumber, 2);

        try {
            $this->response = $result = $this->client->checkVat([
                'countryCode' => $countryCode,
                'vatNumber'   => $vatNumber,
            ]);

            return $this;

        } catch (SoapFault $e) {
            return $e->getMessage();
            //throw $e;
        }
    }

    /**
     * Is the VAT Number provided valid?
     *
     * @return bool
     */
    public function isValid()
    {
        return $this->response->valid;
    }    
}
