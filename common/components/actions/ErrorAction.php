<?php
namespace common\components\actions;

use Yii;
use yii\web\ErrorAction as BaseErrorAction;
use yii\helpers\ArrayHelper;

class ErrorAction extends BaseErrorAction
{

    /**
     * Renders a view that represents the exception.
     * @return string
     * @since 2.0.11
     */
    protected function renderHtmlResponse()
    {
        $exception = $this->exception;
        $code = $exception->statusCode ?? $exception->getCode();
        if ($exception !== null) {
            switch ($code) {
                case 400:
                case 403:
                case 404:
                case 503:
                    $this->view = 'error'.$code;
                    break;

                default:
                    break;
            }
        } 
        $this->controller->layout = ArrayHelper::getValue(Yii::$app->params, 'errorViewLayout', null); //'@app/views/layouts/error';
        return $this->controller->render($this->view ?: $this->id, $this->getViewRenderParams());
    }
}
