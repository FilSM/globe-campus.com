<?php

namespace common\components;

use kartik\helpers\Html;
use kartik\base\Widget;

use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

class FSMHtml extends Html
{
    /**
     * Generates a panel for boxing content.
     *
     * Example:
     *
     * ~~~
     * echo Html::panel([
     *    'heading' => 'Panel Title',
     *    'body' => 'Panel Content',
     *    'footer' => 'Panel Footer',
     * ], Html::TYPE_PRIMARY);
     * ~~~
     *
     * @see http://getbootstrap.com/components/#panels
     *
     * @param array $content the panel content configuration. The following properties can be setup:
     * - `preHeading`: _string_, raw content that will be placed before `heading` (optional).
     * - `heading`: _string_, the panel box heading (optional).
     * - `preBody`: _string_, raw content that will be placed before $body (optional).
     * - `body`: _string_, the panel body content - this will be wrapped in a "panel-body" container (optional).
     * - `postBody`: _string_, raw content that will be placed after $body (optional).
     * - `footer`: _string_, the panel box footer (optional).
     * - `postFooter`: _string_, raw content that will be placed after $footer (optional).
     * - `headingTitle`: _boolean_, whether to pre-style heading content with a '.panel-title' class. Defaults to `false`.
     * - `footerTitle`: _boolean_, whether to pre-style footer content with a '.panel-title' class. Defaults to `false`.
     * @param string $type the panel type which can be one of the bootstrap color modifier constants. Defaults to
     * [[TYPE_DEFAULT]].
     * - [[TYPE_DEFAULT]] or `default`
     * - [[TYPE_PRIMARY]] or `primary`
     * - [[TYPE_SUCCESS]] or `success`
     * - [[TYPE_INFO]] or `info`
     * - [[TYPE_WARNING]] or `warning`
     * - [[TYPE_DANGER]] or `danger`
     * @param array $options HTML attributes / options for the panel container
     * @param string $prefix the CSS prefix for panel type. Defaults to `panel panel-`.
     *
     * @return string
     * @throws InvalidConfigException
     */
    public static function panel($content = [], $type = 'default', $options = [], $prefix = null)
    {
        if (!is_array($content)) {
            return '';
        } else {
            $widget = new Widget();
            if (isset($prefix)) {
                static::addCssClass($options, $prefix . $type);
            } else {
                $widget->addCssClass($options, Widget::BS_PANEL);
                if($type == 'default'){
                    static::addCssClass($options, "panel-default");
                }else{
                    $widget->addCssClass($options, "panel-{$type}");
                }
            }
            $panel = static::getPanelContent($content, 'preHeading') .
                static::getPanelTitle($content, 'heading', $widget) .
                static::getPanelContent($content, 'preBody') .
                static::getPanelContent($content, 'body') .
                static::getPanelContent($content, 'postBody') .
                static::getPanelTitle($content, 'footer', $widget) .
                static::getPanelContent($content, 'postFooter');
            return static::tag('div', $panel, $options);
        }
    }   
}
