<?php

namespace common\components;

use Yii;

use common\models\Assets;

class FSMAssetBundle extends \kartik\base\AssetBundle
{
    protected $needReloadAsset = false;
    protected $assetModel;

    protected function checkNeedReloadAssets()
    {
        $className = (new \ReflectionClass($this))->getShortName();
        if(!$this->assetModel = Assets::findOne(['class_name' => $className])){
            $assetModel = new Assets();
            $assetModel->class_name = $className;
            $assetModel->updated_at = date('Y-m-d H:i:s');
            if($assetModel->save()){
                $this->assetModel = $assetModel;
            }
        }
        
        $assetVersion = Yii::$app->params['cssVersion'];
        if(!empty($this->assetModel) && ($this->assetModel->asset_version != $assetVersion)){
            $this->needReloadAsset = true;
            $this->assetModel->updateAttributes([
                'asset_version' => $assetVersion,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
        
        if (YII_DEBUG || $this->needReloadAsset) {
            $this->publishOptions['forceCopy'] = true;
        }        
    }
    
    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();    
    }
    
    /**
     * Adds a language JS locale file
     *
     * @param string $lang the ISO language code
     * @param string $prefix the language locale file name prefix
     * @param string $dir the language file directory relative to source path
     * @param bool $min whether to auto use minified version
     *
     * @return AssetBundle instance
     */
    public function addLanguage($lang = '', $prefix = '', $dir = null, $min = false)
    {
        if (empty($lang) || substr($lang, 0, 2) == 'en') {
            return $this;
        }
        $ext = $min ? (YII_ENV_PROD ? ".min.js" : ".js") : ".js";
        $file = "{$prefix}{$lang}{$ext}";
        if ($dir === null) {
            $dir = 'js';
        } elseif ($dir === "/") {
            $dir = '';
        }
        $path = $this->sourcePath . '/' . $dir;
        if (!Config::fileExists("{$path}/{$file}")) {
            $lang = Config::getLang($lang);
            $file = "{$prefix}{$lang}{$ext}";
        }
        if (Config::fileExists("{$path}/{$file}")) {
            $this->js[] = empty($dir) ? $file : "{$dir}/{$file}";
        }
        return $this;
    }

    /**
     * Set up CSS and JS asset arrays based on the base-file names
     *
     * @param string $type whether 'css' or 'js'
     * @param array $files the list of 'css' or 'js' basefile names
     */
    protected function setupAssets($type, $files = [])
    {
        if ($this->$type === self::KRAJEE_ASSET) {
            $srcFiles = [];
            $minFiles = [];
            foreach ($files as $file) {
                $srcFiles[] = "{$file}.{$type}".'?v='.Yii::$app->params['cssVersion'];
                $minFiles[] = "{$file}.min.{$type}".'?v='.Yii::$app->params['cssVersion'];
            }
            $this->$type = YII_ENV_PROD ? $minFiles : $srcFiles;
        } elseif ($this->$type === self::EMPTY_ASSET) {
            $this->$type = [];
        }
    }

}
