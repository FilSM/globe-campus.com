<?php

namespace common\components;

use Yii;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;

use kartik\helpers\Html;

class FSMBtnMultiAction extends BaseObject {
    
    static public function aButton($label, $url, $attributes, $options) {
        $model = isset($attributes['model']) ? $attributes['model'] : null;
        $grid = isset($attributes['grid']) ? $attributes['grid'] : null;
        $confirm = isset($attributes['confirm']) ? $attributes['confirm'] : Yii::t('common', 'Are you sure?');
        $dialogSettings = isset($attributes['DialogSettings']) ? $attributes['DialogSettings'] : null;
        $icon = !empty($attributes['icon']) ? Html::icon($attributes['icon']).'&nbsp;' : '';
        
        $options = ArrayHelper::merge(
            [
                'label' => '',
                'title' => Yii::t('common', 'Action'),
                'class' => 'success',
                'data-method' => 'post',
                'data-pjax' => 'false',
                'options' => [
                    'data-pjax' => 0,
                ],
            ], $options
        );    
        
        $css = $grid ? $grid . '-btn-multi-select' : 'btn-multi-select';
        Html::addCssClass($options, $css);
        $view = Yii::$app->getView();
        $actionOpts = Json::encode([
            'grid' => $grid,
            'css' => $css,
            'lib' => ArrayHelper::getValue($dialogSettings, 'libName', 'krajeeDialog'),
            'msg' => !empty($confirm) ? $confirm : null,
            'aButton' => true,
        ]);
        $js = "fsmMultiAction({$actionOpts});";
        $view->registerJs($js);
        
        return Html::a($icon.$label."<span class='img-loader' style='display: none;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>", 
            $url, 
            $options
        );
    }

    static public function vButton($label, $url, $attributes, $options) {
        $model = isset($attributes['model']) ? $attributes['model'] : null;
        $grid = isset($attributes['grid']) ? $attributes['grid'] : null;
        $confirm = isset($attributes['confirm']) ? $attributes['confirm'] : Yii::t('common', 'Are you sure?');
	$dialogSettings = isset($attributes['DialogSettings']) ? $attributes['DialogSettings'] : null;;
        $icon = !empty($attributes['icon']) ? Html::icon($attributes['icon']).'&nbsp;' : '';
        
        $defaults = [
            'title' => Yii::t('common', 'Action'), 
            'data-pjax' => 'false',
            //'data-confirm' => $confirm,
            'data-method' => 'post',
            'value' => !empty($model) ? Url::to($url) : null,
            'title' => (!empty($options['title']) ? $options['title'] : $label),
        ];        
        $options = ArrayHelper::merge($defaults, $options);
        
        $css = $grid ? $grid . '-btn-multi-select' : 'btn-multi-select';
        Html::addCssClass($options, $css);
        $view = Yii::$app->getView();
        $actionOpts = Json::encode([
            'grid' => $grid,
            'css' => $css,
            'lib' => ArrayHelper::getValue($dialogSettings, 'libName', 'krajeeDialog'),
            'msg' => !empty($confirm) ? $confirm : null,
            'vButton' => true,
        ]);
        $js = "fsmMultiAction({$actionOpts});";
        $view->registerJs($js);
        
        return Html::button($icon.$label."<span class='img-loader' style='display: none;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>", 
            $options
        );
    }

}
