<?php
namespace common\components;

use kartik\password\StrengthValidator;

class FSMStrengthValidator extends StrengthValidator
{
    /**
     * @inheritdoc
     * @throws \ReflectionException
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::$_rules[self::RULE_SPL] = ['match' => '![^A-Za-z0-9 ]!', 'int' => true];
        parent::init();
    }    
}