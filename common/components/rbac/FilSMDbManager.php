<?php

namespace common\components\rbac;

use Yii;
use yii\db\Query;
use yii\rbac\DbManager;
use yii\rbac\Item;

use common\components\rbac\FilSMPermission;
use common\components\rbac\FilSMRole;

class FilSMDbManager extends DbManager
{
    
    public function getRoleList(array $without = []) {
        static $result = null;
        
        if(!isset($result)){
            $result = [];
            foreach ($this->getRoles() as $name => $role) {
                if(in_array($name, $without)){
                    continue;
                }
                $result[$name] = Yii::t('user', $role->description);
            }      
        }
        asort($result);
        return $result;
    }

    /**
     * @inheritdoc
     */
    protected function getItems($type)
    {
        $query = (new Query)
                ->from($this->itemTable)
                ->where(['type' => $type])
                ->orderBy('name');

        $items = [];
        foreach ($query->all($this->db) as $row) {
            $items[$row['name']] = $this->populateItem($row);
        }

        return $items;
    }

    /**
     * Populates an auth item with the data fetched from database
     * @param array $row the data from the auth item table
     * @return Item the populated auth item instance (either Role or Permission)
     */
    protected function populateItem($row)
    {
        $class = $row['type'] == Item::TYPE_PERMISSION ? FilSMPermission::class : FilSMRole::class;

        if (!isset($row['data']) || ($data = @unserialize($row['data'])) === false) {
            $data = null;
        }

        return new $class([
            'id' => !empty($row['id']) ? $row['id'] : null,
            'name' => $row['name'],
            'type' => $row['type'],
            'description' => $row['description'],
            'ruleName' => $row['rule_name'],
            'data' => $data,
            'createdAt' => $row['created_at'],
            'updatedAt' => $row['updated_at'],
        ]);
    }

    public function getArrRolesByUser($userId)
    {
        $roles = [];
        foreach (parent::getRolesByUser($userId) as $name => $role) {
            $roles[] = $name;
        }
        return $roles;
    }

    public function hasPermission($userId, $permissionName)
    {
        $permissionList = parent::getPermissionsByUser($userId);
        return isset($permissionList[$permissionName]);
    }

    public function hasRole($userId, $roleName)
    {
        static $result = null;
        static $oldUserId = null;
        
        if(!isset($result) || ($oldUserId != $userId)){
            $oldUserId = $userId;
            $result = parent::getAssignment($roleName, $userId);
        }
        return $result;
    }
    
    public function getUserRoleList($userId = null) {
        static $result = null;
        static $oldUserId = null;
        
        if(!isset($result) || ($oldUserId != $userId)){
            if(!$userId){
                $user = Yii::$app->user->identity;
                $userId = isset($user) ? $user->id : null;
            }
            $oldUserId = $userId;
            $result = $this->getRolesByUser($userId);
        }
        return $result;
    }    
}
