<?php

namespace common\components\FSMPdf;

use Yii;
use yii\helpers\ArrayHelper;

use setasign\Fpdi\Tcpdf\Fpdi;

use common\models\Valuta;
use common\components\FSMHelper;

class FSMPdf extends Fpdi
{

    private $last_page_flag = false;
    protected $originalMargins;
    protected $footerWithContacts = false;
    protected $headerWithLogo = false;
    protected $headerFontColorArr = [153, 153, 153];
    protected $printData = [];

    private function checkPageFooter($y)
    {
        return $this->checkPageBreak(0, $y);
    }

    private function _footerWithContacts()
    {
        if ($this->last_page_flag) {
            /*
             * 
             * 
             */
        }
        $this->SetFontSize(6);

        $this->Cell(60, 0, 'Eventus Corporate Finance, SIA', 'T', 0, 'L');
        $this->Cell(60, 0, 'Reg.Nr. 40103625012', 0, 0, 'L');
        $this->Cell(60, 0, 'VAT Nr. LV40103625012', 0, 0, 'L');

        $this->Cell(60, 0, 'MEINL BANK', 'T', 1, 'R');
        $this->Cell(60, 0, 'SWIFT: MEINATWW', 0, 1, 'R');
        $this->Cell(60, 0, 'IBAN: AT421924000000578294', 0, 1, 'R');

        $this->Cell(60, 0, 'Legal address: 202, Georgiou Gennadiou - 10, Limassol, 3041, Cyprus', 0, 0, 'L');
        $this->Cell(60, 0, '', 0, 1, 'R');

        $this->Cell(60, 0, 'Actual address: 58a Bauskas str., 5th floor, LV-1004, Riga, Latvia', 0, 0, 'L');
        $this->Cell(60, 0, '', 0, 1, 'R');

        $this->Cell(70, 0, 'Tel. +371 671 033 31', 'T', 0, 'C');
        $this->Cell(70, 0, 'info@globe-campus.com', 0, 0, 'C');
        $this->Cell(70, 0, 'www.globe-campus.com', 0, 0, 'C');
        $this->Cell(70, 0, '', 0, 0, 'C');
        $this->Cell(70, 0, '', 0, 0, 'C');
    }

    private function _header()
    {
        //Logo
        $image_file = K_PATH_IMAGES . 'logo-print.png';
        $this->Image($image_file, 154);
    }

    protected function getSummaToWords($sum = null, $locale = '', $currency = 'EUR')
    {
        return FSMHelper::getSummaToWords($sum, $locale, $currency);
    }

    public function __construct($data)
    {
        parent::__construct(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->printData = $data;
    }

    public function Close()
    {
        $this->last_page_flag = true;
        parent::Close();
    }

    public function Header()
    {
        parent::Header();
        if ($this->headerWithLogo) {
            $this->_header();
        }
    }

    public function Footer()
    {
        if ($this->footerWithContacts) {
            $this->_footerWithContacts();
        }

        $this->SetFontSize(10);
        parent::Footer();
        //$this->Ln(1);
        //$this->Cell(0, 0, 'Lapa Nr.'.$this->getPage(), 0, 1, 'C');
    }

    public function buildOutput(array $margins = [])
    {
        $this->originalMargins = ArrayHelper::merge([
            'top' => 10,
            'left' => 20,
            'footer' => 10,
            'right' => 20
        ], $margins);

        // set default header data
        $this->SetHeaderData('', 0, '', '', $this->headerFontColorArr, $this->headerFontColorArr);
        $this->setFooterData($this->headerFontColorArr, $this->headerFontColorArr);

        // set header and footer fonts
        $this->setHeaderFont(Array('freesans', 'b', PDF_FONT_SIZE_MAIN));
        $this->setFooterFont(Array('freesans', '', PDF_FONT_SIZE_DATA));

        $this->SetFontSize(10);

        // set default monospaced font
        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $this->SetMargins(
            $this->originalMargins['left'],
            $this->originalMargins['top'],
            $this->originalMargins['right'],
            true
        );
        $this->SetHeaderMargin(PDF_MARGIN_HEADER);
        $this->SetFooterMargin($this->originalMargins['footer']);

        // set auto page breaks
        $this->SetAutoPageBreak(true, $this->originalMargins['footer']);

        // set image scale factor
        $this->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set cell padding
        $this->setCellPaddings(0, 1, 0, 1);
        $this->setCellHeightRatio(1);
    }

}
