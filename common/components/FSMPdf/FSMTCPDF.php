<?php

namespace common\components\FSMPdf;

use TCPDF as BaseTCPDF;

/* *
 *  Component to load TCPDF Libraries
 */
class FSMTCPDF extends BaseTCPDF
{

    public function __construct()
    {
        parent::__construct();
    }

}
