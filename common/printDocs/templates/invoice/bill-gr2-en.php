<?php

namespace common\printDocs\template\invoice;

use Yii;

use NumberFormatter;

use common\models\bill\Bill;
use common\components\FSMPdf\FSMPdf;

class BillPDF extends FSMPdf {
    
    private $agreement;
    private $invoice;
    private $firstClient;
    private $secondClient;
    private $firstClientBank;
    private $firstClientAllBanks;
    private $secondClientBank;
    private $billPersons;
    private $firstClientAddress;
    private $secondClientAddress;
    private $billProducts;
    private $pdfSettings;

    public function Header() {
        if($this->numpages > 1){
            parent::Header();
            switch ($this->invoice->doc_type){
                default :
                case 'avans':
                    $title = 'Prepayment invoice';
                    break;
                case 'bill':
                    $title = 'Invoice';
                    break;
                case 'cr_bill':
                    $title = 'Credit invoice';
                    break;
                case 'invoice':
                    $title = 'Waybill';
                    break;
            }
            //$this->SetFontSize(11);
            $this->SetTextColorArray($this->headerFontColorArr);
            $this->Cell(0, 0, $title.' No. '.$this->invoice->doc_number, 0, 1, 'R');
            $this->Ln();
        }
    }
    
    public function Footer() {
        $margins =$this->getMargins();

        if($this->numpages == $this->page){
            $this->SetFont('freesans', 'B', 11);
            $this->Cell(60, 0, 'Payment date:  ', 0, 0, 'R');
            $this->Cell(60, 0,(!empty($this->invoice->pay_date) ? 'due '.date('d.m.Y', strtotime($this->invoice->pay_date)) : null), 0, 1, 'L');

            $y = $this->GetY();
            $this->SetFont('freesans', 'I', 11);
            $this->MultiCell(60, 0, 'Please refer to the invoice number when making your payment', 0, 'L');

            if(isset($this->firstClientBank) && $this->firstClientBank->bank->enabled){
                $x = $margins['left'] + 60;
                $this->SetXY($x, $y);

                $this->SetFont('freesans', '', 11);
                $clientBank = $this->firstClientBank;
                $this->MultiCell(50, 0, $clientBank->bank->name, 0, 'R', false, 1);
                $yIBAN = $this->GetY();
                $this->SetXY($x + 50, $y);
                $this->Cell(70, 0, '  BIC '.$clientBank->bank->swift, 0, 1);

                $this->SetXY($x, $yIBAN);
                $this->SetFont('freesans', 'B', 11);
                $this->Cell(50, 0, 'IBAN:', 0, 0, 'R');
                $this->Cell(70, 0, '  '.$clientBank->account, 0, 1);
                $this->Ln();            
            }     
        }

        parent::Footer();
        
        $this->SetX($margins['left']);
        $this->SetFontSize(8);
        $docKey = $this->invoice->printDocNumber.'-'.(isset($this->invoice->doc_key) ? $this->invoice->doc_key : $this->printData['doc-key']);
        $this->Cell(0, 0, "invoice-{$docKey}.pdf");
    }

    public function buildOutput(array $margins = []) {
        $this->agreement = $this->printData['agreement'];
        $this->invoice = $this->printData['invoice'];
        $this->firstClient = $this->printData['firstClient'];
        $this->secondClient = $this->printData['secondClient'];
        $this->firstClientBank = $this->printData['firstClientBank'];
        $this->firstClientAllBanks = $this->printData['firstClientAllBanks'];
        $this->secondClientBank = $this->printData['secondClientBank'];
        $this->billPersons = $this->printData['billPersons'];
        $this->firstClientAddress = $this->printData['firstClientAddress'];
        $this->secondClientAddress = $this->printData['secondClientAddress'];
        $this->billProducts = $this->printData['billProducts'];
        $this->pdfSettings = $this->printData['pdfSettings'];
        
        $footer = 30;
        if(!empty($this->pdfSettings) && ($fileModel = $this->pdfSettings->uploadedFile) ){
            $filePath = $fileModel->uploadedFilePath;
            if (!empty($filePath) && file_exists($filePath)) {
                $footer = 58;
            }                 
        }
        $margins['footer'] = $footer;

        parent::buildOutput($margins);
        
        switch (count($this->billPersons)) {
            case 0:
                break;
            case 1:
                $signingHeight = 23.5 + (6*3);
                break;
            default:
                $signingHeight = 11.7 + (17.5 * count($this->billPersons));
                break;
        }
        
        Yii::$app->formatter->locale = 'en-GB';

        $this->AddPage();
        $margins = $this->getMargins();
        $rowWidth = $this->w - $margins['right'];
        $rowWidth = $rowWidth - $margins['left'];

        $pTop = $yClient = $this->GetY();
        $logoModel = $this->firstClient->logo;
        if(!empty($logoModel)){
            $logoPath = $logoModel->uploadedFilePath;
            if (!empty($logoPath) && file_exists($logoPath)) {
                $this->SetLeftMargin(15);
                $this->Image($logoPath, 0, $margins['top'], 70, 20, '', '', '', false, 300, 'L', false, false, 0, true);
                $this->SetLeftMargin($this->originalMargins['left']);
                $yClient = 30;
            }  
        }
        
        $this->SetY($pTop);
        $this->SetFont('freesans', 'B', 11);
        $this->Cell($rowWidth, 0, (!empty($this->invoice->doc_date) ? date('F jS, Y', strtotime($this->invoice->doc_date)) : null), 0, 1, 'R');

        // ---------------------------------------------------------------------
        
        $this->SetY($yClient);
        $this->Ln(2);
        $this->SetFont('freesans', '', 11);
        $this->Cell(60, 0, 'Company:  ', 0, 0, 'R');
        $this->SetFont('freesans', 'B', 11);
        $this->Cell(60, 0, $this->firstClient->name, 0, 1);
        
        $this->SetFont('freesans', '', 11);
        $this->Cell(60, 0, 'Registration number:  ', 0, 0, 'R');
        $this->Cell(60, 0, $this->firstClient->reg_number, 0, 1);

        $this->Cell(60, 0, 'VAT No.:  ', 0, 0, 'R');
        $this->Cell(60, 0, $this->firstClient->vat_number, 0, 1);
        
        $this->Cell(60, 0, 'Address:  ', 0, 0, 'R');
        $this->MultiCell(0, 0, $this->firstClientAddress, 0, 'L', false, 1, 80);
        $this->Ln(2);            

        // ---------------------------------------------------------------------
        
        $this->SetFont('freesans', '', 11);
        $this->Cell(60, 0, 'Company:  ', 0, 0, 'R');
        $this->SetFont('freesans', 'B', 11);
        $this->Cell(60, 0, $this->secondClient->name, 0, 1);
        
        $this->SetFont('freesans', '', 11);
        $this->Cell(60, 0, 'Registration number:  ', 0, 0, 'R');
        $this->Cell(60, 0, $this->secondClient->reg_number, 0, 1);

        $this->Cell(60, 0, 'VAT No.:  ', 0, 0, 'R');
        $this->Cell(60, 0, $this->secondClient->vat_number, 0, 1);
        
        $this->Cell(60, 0, 'Address:  ', 0, 0, 'R');
        $this->MultiCell(0, 0, $this->secondClientAddress, 0, 'L', false, 1, 80);
        $this->Ln();         

        switch ($this->invoice->doc_type){
            default :
            case 'avans':
                $title = 'Prepayment invoice';
                break;
            case 'bill':
                $title = 'Invoice';
                break;
            case 'cr_bill':
                $title = 'Credit invoice';
                break;
            case 'invoice':
                $title = 'Waybill';
                break;
        }

        $this->Line($margins['left'], $this->GetY(), $margins['left'] + $rowWidth, $this->GetY(), ['width' => 0.2]);        
        $this->SetFont('freesans', 'B', 14);
        $this->Cell(0, 0, $title.' No. '.$this->invoice->doc_number, 0, 1, 'C');
        $this->Line($margins['left'], $this->GetY(), $margins['left'] + $rowWidth, $this->GetY(), ['width' => 0.2]);     
        $this->Ln();
        $this->SetFont('freesans', '', 11);
        
        if(!empty($this->invoice->services_period_from) && !empty($this->invoice->services_period_till)){
            $this->Cell(60, 0, 'Service period:  ', 0, 0, 'R');
            $this->Cell(60, 0, date('d.m.Y', strtotime($this->invoice->services_period_from)).' - '.date('d.m.Y', strtotime($this->invoice->services_period_till)), 0, 1);            
        }
        
        if(!empty($this->invoice->place_service)){
            $this->SetFont('freesans', '', 11);
            $this->writeHTMLCell(170, 0, $this->GetX(), $this->GetY(), '<b>Place of service:</b> '.$this->invoice->place_service, 0, 1);
        }
        
        if(!empty($this->invoice->justification || !empty($this->invoice->comment))){
            $this->SetFont('freesans', '', 11);
            $comment = $this->invoice->justification;
            $comment .= !empty($this->invoice->comment) ? ' '.$this->invoice->comment : '';
            $this->writeHTMLCell(170, 0, $this->GetX(), $this->GetY(), '<b>Justification:</b> '.$comment, 0, 1);
        }
                
        //---------------------------------------------------------------------------------------
        $this->SetFont('freesans', '', 11);

        $invoiceSumma = number_format((float)$this->invoice->summa, 2, '.', ' ');
        $invoiceVat = number_format((float)$this->invoice->vat, 2, '.', ' ');
        $invoiceTotal = number_format((float)$this->invoice->total, 2, '.', ' ');
        $invoiceTotalDec = number_format(abs((float)$this->invoice->total), 2, '.', '');
        $avansText = '';
        $currency = strtoupper($this->invoice->valuta->name);
        if(empty($this->printData['avansSumma'])){
            $sumWords = $this->getSummaToWords($invoiceTotalDec, '', $currency);
            if($invoiceTotal < 0){
                $avansText = 
                    sprintf(
                        'The credit invoice was issued on the basis of Invoice %1$s.<br/>',
                        $this->printData['avansNumberList']
                    );
            }
        }else{
            if(!empty($this->printData['paidSumma'])){
                $paidSumma = (float)$this->printData['paidSumma'];
                $invoiceTotalDec = number_format(abs((float)$this->invoice->total + $paidSumma), 2, '.', '');
            }
            $total = (float)($invoiceTotalDec - $this->printData['avansSumma']);
            $invoiceTotalDec = number_format(abs($total), 2, '.', '');
            $sumWords = $this->getSummaToWords(number_format($invoiceTotalDec, 2, '.', ''), '', $currency);
            $avansText = 
                sprintf(
                    'The invoice was issued on the basis of Prepayment invoice %1$s.<br/>'.
                    'Was received prepayment %2$s '.$currency.'.<br/>'.
                    (($total >= 0) ? '<span style="color: red;">Amount to be paid %3$s '.$currency.'.</span>' : 'Your prepayment is %3$s'.' '.$currency.'.').'<br/>', 
                    $this->printData['avansNumberList'],
                    number_format($this->printData['avansSumma'], 2, '.', ' '),
                    number_format(abs($total), 2, '.', ' ')
                );
        }
        
        $productRows = '';
        $reversTotal = 0;
        $hasRevers = false;
        if(!empty($this->billProducts)){
            foreach ($this->billProducts as $product) {
                $productName = $product->productName;
                if($this->invoice->according_contract && (count($this->billProducts) == 1)){
                    $productName .= '<br/>No. '.$this->agreement->number;
                }
                $productName = (!empty($product->comment) ? $productName.' '.$product->comment : $productName);
                
                $productAmount = (float)$product->amount;
                $productAmountArr = explode('.', $productAmount);
                $floatPart = !empty($productAmountArr[1]) ? '.'.str_pad($productAmountArr[1], 3, '0') : '';
                $productAmount = $productAmountArr[0].$floatPart;
                        
                $productPrice = (float)$product->price;
                $productPriceArr = explode('.', $productPrice);
                $floatPart = str_pad((!empty($productPriceArr[1])? $productPriceArr[1] : ''), 2, '0');
                $productPrice = number_format($product->price, strlen($floatPart), '.', ' ');
                
                $productRows .= '<tr>';
                $productRows .= '<td style="border: 1px solid black;">'.$productName.'</td>';
                $productRows .= '<td style="border: 1px solid black; text-align: center;">'.(empty($product->measure_id) ? (isset($product->product) ? $product->product->measure->name : '') : $product->measure->name).'</td>';
                $productRows .= '<td style="border: 1px solid black; text-align: center;">'.$productAmount.'</td>';
                $productRows .= '<td style="border: 1px solid black; text-align: right;">'.$productPrice.'</td>';
                $productRows .= '<td style="border: 1px solid black; text-align: right;">'.($product->revers ? Yii::t('bill', 'reverse') : ($this->firstClient->vat_payer ? $product->vat : '---')).'</td>';
                $productRows .= '<td style="border: 1px solid black; text-align: right;">'.number_format($product->summa, 2, '.', ' ').'</td>';
                $productRows .= '</tr>';
                
                $hasRevers = $hasRevers || $product->revers;
                $reversTotal += ($product->revers ? $product->summa_vat : 0);
            }
            if($this->invoice->according_contract && (count($this->billProducts) > 1)){
                $productRows .= '<tr>';
                $productRows .= '<td colspan="5" style="border: 1px solid black; text-align: right;">No. '.$this->agreement->number.'</td>';
                $productRows .= '<td style="border: 1px solid black;"></td>';
                $productRows .= '</tr>';
            }            
        }
        
        $reversRow = ($hasRevers || !empty($reversTotal) ? 
            '<tr>
                <td colspan="3" style="border: none;"></td>
                <td colspan="3" style="border: none; font-style: italic;">*'.
                    Yii::t('bill', 'VAT reverse charge intra community supply').(!empty($reversTotal) ? ': '. number_format($reversTotal, 2, '.', ' '): '').
                '</td>
            </tr>' : ''
        );
        
        $originalSummaRow = ($this->invoice->rate != 1 ? 
            '<tr>
                <td colspan="5" style="border: 1px solid white; font-style: italic;">*'.
                    'Sum EUR: '.number_format((float)$this->invoice->total_eur, 2, '.', ' '). ' (rate '.
                    (number_format((float)$this->invoice->total / (float)$this->invoice->total_eur, 3, '.', ' ')).')'.
                '</td>
            </tr>' : ''
        );
            
        $tableHTML = 
        '<table border="0" cellspacing="0" cellpadding="4">
            <tr style="text-align: center; ">
                <th style="width: 35%; border: 1px solid black;">Service</th>
                <th style="width: 16%; border: 1px solid black;">Measure</th>
                <th style="width: 10%; border: 1px solid black;">Amount</th>
                <th style="width: 14%; border: 1px solid black;">Price</th>
                <th style="width: 10%; border: 1px solid black;">VAT %</th>
                <th style="width: 15%; border: 1px solid black;">Sum, '.$currency.'</th>
            </tr>'.
            $productRows.
            '<tr style="text-align: right;">
                <td colspan="3" style="border-left: none; border-right: 1px solid black; font-style: italic; text-align: left;">'.
                    $avansText.
                '</td>
                <td colspan="2" style="border: 1px solid black; border-right: 1px solid black;">Total net amount:</td>
                <td style="border: 1px solid black;">'.$invoiceSumma.'</td>
            </tr>
            <tr style="text-align: right;">
                <td colspan="3" style="border-left: none; border-right: 1px solid black; font-style: italic; text-align: left;">Amount in words:</td>
                <td colspan="2" style="border: 1px solid black; border-right: 1px solid black;">VAT:</td>
                <td style="border: 1px solid black;">'.(!empty((float)$invoiceVat) ? $invoiceVat : '---').'</td>
            </tr>'.
            '<tr style="text-align: right;">
                <td colspan="3" style="border-left: none; border-right: 1px solid black; font-style: italic; text-align: left;">'.$sumWords.'</td>
                <td colspan="2" style="border: 1px solid black; border-right: 1px solid black; font-weight: bold;">Total:</td>
                <td style="border: 1px solid black; font-weight: bold;">'.$invoiceTotal.'</td>
            </tr>'.
            $originalSummaRow.
            $reversRow.
        '</table>';
        $this->writeHTML($tableHTML, true, false, true);

        if(!empty($this->invoice->comment_special)){
            $this->SetFont('freesans', 'B', 11);
            $this->Cell(85, 0, Yii::t('bill', 'Special notes').':', 0, 1);
            $this->SetFont('freesans', 'I', 11);
            $this->MultiCell(170, 0, $this->invoice->comment_special, 0, 'L');
        }
        
        $secondClientX = $margins['left'] + 90;
        if($this->invoice->doc_type == 'invoice'){
            if(empty($this->invoice->e_signing)){
                $this->checkPageBreak($signingHeight);
            }
            
            $this->SetFont('freesans', '', 11);
            $this->writeHTMLCell(85, 0, $this->GetX(), $this->GetY(), '<b>'.$this->invoice->getAttributeLabel('carrier').':</b> '.$this->invoice->carrier);
            $this->writeHTMLCell(85, 0, $secondClientX, $this->GetY(), '<b>'.$this->invoice->getAttributeLabel('transport').':</b> '.$this->invoice->transport, 0, 1);
            
            $this->SetFont('freesans', 'B', 11);
            $this->Cell(85, 0, $this->invoice->getAttributeLabel('loading_address'));
            $this->SetX($secondClientX);
            $this->Cell(85, 0, $this->invoice->getAttributeLabel('unloading_address'), 0, 1);
            
            $yRow = $this->GetY();
            $this->SetFont('freesans', '', 11);
            $this->MultiCell(85, 0, $this->invoice->loading_address, 0, 'L', false, 0);
            $this->Ln();
            $yCol1 = $this->GetY();
            $this->SetY($yRow);
            $this->MultiCell(85, 0, $this->invoice->unloading_address, 0, 'L', false, 1, $secondClientX);
            $yCol2 = $this->GetY();
            if($yCol1 > $yCol2){
                $this->SetY($yCol1);
            }
            
            $this->Ln();
            
            $this->SetFont('freesans', 'I', 11);
            $this->Cell(90, 0, 'Loading date:');
            $this->Cell(85, 0, 'Unloading date:', 0, 1);
            
            $this->Line($margins['left'], $this->GetY(), 80 + $margins['left'], $this->GetY(), ['width' => 0.2]);
            $this->Line($secondClientX, $this->GetY(), $rowWidth, $this->GetY(), ['width' => 0.2]);
            
            $this->Ln();
        }
        
        if(empty($this->invoice->e_signing)){
            $this->Ln();
            $this->checkPageBreak($signingHeight);

            $billPersonsArr = [
                1 => 0, 
                2 => 0
            ];
            foreach ($this->billPersons as $billPerson) {
                if(empty($billPerson->clientPerson)){
                    continue;
                }
                $billPersonsArr[$billPerson->person_order]++;
            }
            for ($index = 1; $index < 3; $index++) {
                $this->SetFont('freesans', '', 11);
                $this->Cell(85, 0, ($index == 1 ? 'Company:' : 'Client:'), 0, 1, 'R');
                $personPrinted = false;
                $pi = 1;
                foreach ($this->billPersons as $billPerson) {
                    if(($billPerson->person_order == $index) && !empty($billPerson->clientPerson)){
                        $this->Line(105, $this->GetY(), $rowWidth, $this->GetY(), ['width' => 0.2]);
                        $this->SetFont('freesans', 'B', 11);   
                        $this->Cell(85, 0, '');
                        $this->Cell($rowWidth - 110, 0, $billPerson->clientPerson->first_name . ' ' . $billPerson->clientPerson->last_name, 0, 1, 'C');            
                        if(!empty($billPerson->clientPerson->position_id)){
                            $this->SetFont('freesans', 'I', 11);
                            $this->Cell(85, 0, '');
                            $this->Cell($rowWidth - 110, 0, $billPerson->clientPerson->position->name, 0, 1, 'C');            
                        }
                        $personPrinted = true;
                        
                        if($pi < $billPersonsArr[$billPerson->person_order]){
                            $this->Ln(15);
                        }
                        $pi++;
                    }
                }

                if(!$personPrinted){
                    $this->Ln();
                    $this->Line(80, $this->GetY(), $rowWidth, $this->GetY(), ['width' => 0.2]);           
                    $this->Ln();
                }            
                $this->Ln();
            }

        } else {
            $this->SetFont('freesans', 'I', 8);
            $this->Cell(85, 0, 'The invoice  has been prepared electronically and is valid without a signature');
        }

            /*
        $template = dirname(__FILE__) . '/invoice.pdf';
        $pageCount = $this->setSourceFile($template);
        $tplIdx = $this->importPage(1, '/MediaBox');
        $this->useTemplate($tplIdx);
         * 
         */

    }

}