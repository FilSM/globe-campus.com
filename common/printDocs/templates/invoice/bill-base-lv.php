<?php

namespace common\printDocs\template\invoice;

use Yii;

use NumberFormatter;

use common\models\bill\Bill;
use common\components\FSMPdf\FSMPdf;
use common\components\FSMHelper;

class BillPDF extends FSMPdf {
    
    private $agreement;
    private $invoice;
    private $firstClient;
    private $secondClient;
    private $firstClientBank;
    private $secondClientBank;
    private $billPersons;
    private $firstClientAddress;
    private $secondClientAddress;
    private $billProducts;

    public function Header() {
        if($this->numpages > 1){
            parent::Header();
            switch ($this->invoice->doc_type){
                default :
                case 'avans':
                    $title = 'Avansa rēķins';
                    break;
                case 'bill':
                    $title = 'Rēķins';
                    break;
                case 'cr_bill':
                    $title = 'Kredītrēķins';
                    break;
                case 'invoice':
                    $title = 'Pavadzīme';
                    break;
            }
            $this->SetTextColorArray($this->headerFontColorArr);
            $this->Cell(0, 0, $title.' No. '.$this->invoice->doc_number, 0, 1, 'R');
            $this->Ln();
        }
    }
    
    public function Footer() {
        //if($this->numpages > 1){
            parent::Footer();
            $margins =$this->getMargins();
            $this->SetX($margins['left']);
            $this->SetFontSize(8);
            $docKey = $this->invoice->printDocNumber.'-'.(isset($this->invoice->doc_key) ? $this->invoice->doc_key : $this->printData['doc-key']);
            $this->Cell(0, 0, "invoice-{$docKey}.pdf");
        //}
    }

    public function buildOutput(array $margins = []) {
        $this->agreement = $this->printData['agreement'];
        $this->invoice = $this->printData['invoice'];
        $this->firstClient = $this->printData['firstClient'];
        $this->secondClient = $this->printData['secondClient'];
        $this->firstClientBank = $this->printData['firstClientBank'];
        $this->secondClientBank = $this->printData['secondClientBank'];
        $this->billPersons = $this->printData['billPersons'];
        $this->firstClientAddress = $this->printData['firstClientAddress'];
        $this->secondClientAddress = $this->printData['secondClientAddress'];
        $this->billProducts = $this->printData['billProducts'];

        parent::buildOutput($margins);

        $signingHeight = (count($this->billPersons) == 1 ? 10 : 30);
        
        Yii::$app->formatter->locale = 'lv_LV';
        $dataFormatter = Yii::$app->formatter;

        $this->AddPage();
        $margins = $this->getMargins();
        $pWidth = $this->w - $margins['right'];

        switch ($this->invoice->doc_type){
            default :
            case 'avans':
                $title = 'Avansa rēķins';
                break;
            case 'bill':
                $title = 'Rēķins';
                break;
            case 'cr_bill':
                $title = 'Kredītrēķins';
                break;
            case 'invoice':
                $title = 'Pavadzīme';
                break;
        }
        $this->SetFont('freesans', 'B', 14);
        $this->Cell(0, 0, $title.' Nr. '.$this->invoice->doc_number, 0, 1, 'L');
        $this->SetFont('freesans', '', 12);
        $this->Cell(0, 0, (!empty($this->invoice->doc_date) ? 
            date('Y', strtotime($this->invoice->doc_date)).'.gada '.
            date('j', strtotime($this->invoice->doc_date)).'. '.
            $dataFormatter->asDate($this->invoice->doc_date, 'php:F') : null), 0, 1
        );

        $this->SetFont('freesans', 'I', 10);
        $this->Cell(35, 0, 'Maksājuma datums:');
        $this->Cell(85, 0, (!empty($this->invoice->doc_date) ? 'līdz '.date('d.m.Y', strtotime($this->invoice->pay_date)) : null), 0, 1);
        $spTop = $this->GetY();
        
        $pTop = $this->GetY();
        $logoModel = $this->firstClient->logo;
        if(!empty($logoModel)){
            $logoPath = $logoModel->uploadedFilePath;
            if (!empty($logoPath) && file_exists($logoPath)) {
                $this->Image($logoPath, $pWidth - 70, $margins['top'], 0, 20, '', '', '', false, 300, 'R', false, false, 0, true);
                $this->SetY(42);
                $pTop = 42;
            }  
        }
        
        if(!empty($this->invoice->services_period_from) && !empty($this->invoice->services_period_till)){
            $this->SetY($spTop);
            $this->Cell(35, 0, 'Pakalpojuma periods:');
            $this->Cell(85, 0, date('d.m.Y', strtotime($this->invoice->services_period_from)).' - '.date('d.m.Y', strtotime($this->invoice->services_period_till)), 0, 1);
            $pTop = $this->GetY();
        }
        $this->Line($margins['left'], $pTop, $pWidth, $pTop, ['width' => 0.2]);
        $this->Line($margins['left'], $pTop + 1, $pWidth, $pTop + 1, ['width' => 0.5]);

        $this->Ln(5);
        $yClient = $this->GetY();
        
        $this->SetFont('freesans', 'U', 10);
        $this->Cell(85, 0, (isset($this->agreement->firstClientRole) ? $this->agreement->firstClientRole->name.':' : ''), 0, 1, 'L');
        
        $this->SetFont('freesans', 'B', 10);
        $this->Cell(85, 0, $this->firstClient->name, 0, 1, 'L');
        
        $this->SetFont('freesans', '', 10);
        $this->Cell(85, 0, 'Reģistrācijas numurs: '.$this->firstClient->reg_number, 0, 1, 'L');
        $this->Cell(85, 0, 'Adrese:', 0, 1, 'L');
        $this->MultiCell(85, 0, $this->firstClientAddress, 0, 'L', false, 1);
        if(!empty($this->firstClient->vat_number)){
            $this->Cell(85, 0, 'PVN Nr.: '.$this->firstClient->vat_number, 0, 1, 'L');
        }
        
        $yClientEnd_1 = $this->GetY();
        
        //---------------------------------------------------------------------------------------
                
        $secondClientX = $margins['left'] + 90;
        $this->SetXY($secondClientX, $yClient);
        
        $this->SetFont('freesans', 'U', 10);
        $this->Cell(85, 0, (isset($this->agreement->secondClientRole) ? $this->agreement->secondClientRole->name.':' : ''), 0, 1, 'L');
        
        $this->SetFont('freesans', 'B', 10);
        $this->SetX($secondClientX);
        $this->Cell(85, 0, $this->secondClient->name, 0, 1, 'L');
        
        $this->SetFont('freesans', '', 10);
        $this->SetX($secondClientX);
        $this->Cell(85, 0, 'Reģistrācijas numurs: '.$this->secondClient->reg_number, 0, 1, 'L');
        $this->SetX($secondClientX);
        $this->Cell(85, 0, 'Adrese:', 0, 1, 'L');
        $this->MultiCell(85, 0, $this->secondClientAddress, 0, 'L', false, 1, $secondClientX);
        if(!empty($this->secondClient->vat_number)){
            $this->SetX($secondClientX);
            $this->Cell(85, 0, 'PVN Nr.: '.$this->secondClient->vat_number, 0, 1, 'L');
        }
        
        $yClientEnd_2 = $this->GetY();
        
        if($yClientEnd_1 > $yClientEnd_2){
            $this->SetY($yClientEnd_1);
        }
        
        $yClient = $this->GetY();
        if(isset($this->firstClientBank) && $this->firstClientBank->bank->enabled){
            $this->SetFont('freesans', 'B', 10);
            $this->Cell(85, 0, $this->firstClientBank->bank->name, 0, 1, 'L');
            $this->Cell(85, 0, 'BIC '.$this->firstClientBank->bank->swift, 0, 1, 'L');
            $this->Cell(85, 0, 'IBAN: '.$this->firstClientBank->account, 0, 1, 'L');
        }else{
            $this->Cell(85, 0, '', 0, 1, 'L');
            $this->Cell(85, 0, '', 0, 1, 'L');
            $this->Cell(85, 0, '', 0, 1, 'L');
        }
        
        $this->SetY($yClient); 
        if(isset($this->secondClientBank) && $this->secondClientBank->bank->enabled){
            $this->SetFont('freesans', 'B', 10);
            $this->SetX($secondClientX);
            $this->Cell(85, 0, $this->secondClientBank->bank->name, 0, 1, 'L');
            $this->SetX($secondClientX);
            $this->Cell(85, 0, 'BIC '.$this->secondClientBank->bank->swift, 0, 1, 'L');
            $this->SetX($secondClientX);
            $this->Cell(85, 0, 'IBAN: '.$this->secondClientBank->account, 0, 1, 'L');
        }else{
            $this->Cell(85, 0, '', 0, 1, 'L');
            $this->Cell(85, 0, '', 0, 1, 'L');
            $this->Cell(85, 0, '', 0, 1, 'L');
        }
        
        if(!empty($this->invoice->place_service)){
            $this->SetFont('freesans', '', 10);
            $this->writeHTMLCell(170, 0, $this->GetX(), $this->GetY(), '<b>Pakalpojuma sniegšanas vieta:</b> '.$this->invoice->place_service, 0, 1, false, true, 'L');
        }
        
        if(!empty($this->invoice->justification || !empty($this->invoice->comment))){
            $this->SetFont('freesans', '', 10);
            $comment = $this->invoice->justification;
            $comment .= !empty($this->invoice->comment) ? ' '.$this->invoice->comment : '';
            $this->writeHTMLCell(170, 0, $this->GetX(), $this->GetY(), '<b>Pamatojums:</b><br/>'.$comment, 0, 1, false, true, 'L');
        }
                
        //---------------------------------------------------------------------------------------
        $this->SetFont('freesans', '', 10);

        $invoiceSumma = number_format((float)$this->invoice->summa, 2, '.', ' ');
        $invoiceVat = number_format((float)$this->invoice->vat, 2, '.', ' ');
        $invoiceTotal = number_format((float)$this->invoice->total, 2, '.', ' ');
        $invoiceTotalDec = number_format(abs((float)$this->invoice->total), 2, '.', '');
        $avansText = '';
        if(empty($this->printData['avansSumma'])){
            $sumWords = FSMHelper::getSummaToLVWords($invoiceTotalDec);
            if($invoiceTotal < 0){
                $avansText = 
                    sprintf(
                        'Kredītrēķins ir izrakstīts uz rēķina %1$s pamatā.<br/>',
                        $this->printData['avansNumberList']
                    );
            }
        }else{
            if(!empty($this->printData['paidSumma'])){
                $paidSumma = (float)$this->printData['paidSumma'];
                $invoiceTotalDec = number_format(abs((float)$this->invoice->total + $paidSumma), 2, '.', '');
            }
            $total = (float)($invoiceTotalDec - $this->printData['avansSumma']);
            $invoiceTotalDec = number_format(abs($total), 2, '.', '');
            $sumWords = FSMHelper::getSummaToLVWords(number_format($invoiceTotalDec, 2, '.', ''));
            $avansText = 
                sprintf(
                    'Rēķins ir izrakstīts uz Avansa %1$s pamatā.<br/>'.
                    'Saņemta priekšapmaksa %2$s '.strtoupper($this->invoice->valuta->name).'.<br/>'.
                    (($total >= 0) ? '<span style="color: red;">Summa, kas jāmaksā %3$s '.strtoupper($this->invoice->valuta->name).'.</span>' : 'Jūsu priekšapmaksa ir %3$s'.' '.strtoupper($this->invoice->valuta->name).'.').'<br/>',
                    $this->printData['avansNumberList'],
                    number_format($this->printData['avansSumma'], 2, '.', ' '),
                    number_format(abs($total), 2, '.', ' ')
                );
        }
        
        $productRows = '';
        $reversTotal = 0;
        $hasRevers = false;
        if(!empty($this->billProducts)){
            foreach ($this->billProducts as $product) {
                $productName = $product->productName;
                if($this->invoice->according_contract && (count($this->billProducts) == 1)){
                    $productName .= '<br/>Nr. '.$this->agreement->number;
                }
                $productName = (!empty($product->comment) ? $productName.' '.$product->comment : $productName);
                
                $productAmount = (float)$product->amount;
                $productAmountArr = explode('.', $productAmount);
                $floatPart = !empty($productAmountArr[1]) ? '.'.str_pad($productAmountArr[1], 3, '0') : '';
                $productAmount = $productAmountArr[0].$floatPart;
                        
                $productPrice = (float)$product->price;
                $productPriceArr = explode('.', $productPrice);
                $floatPart = str_pad((!empty($productPriceArr[1])? $productPriceArr[1] : ''), 2, '0');
                $productPrice = number_format($product->price, strlen($floatPart), '.', ' ');
                
                $productRows .= '<tr>';
                $productRows .= '<td style="border: 1px solid black;">'.$productName.'</td>';
                $productRows .= '<td style="border: 1px solid black; text-align: center;">'.(empty($product->measure_id) ? (isset($product->product) ? $product->product->measure->name : '') : $product->measure->name).'</td>';
                $productRows .= '<td style="border: 1px solid black; text-align: center;">'.$productAmount.'</td>';
                $productRows .= '<td style="border: 1px solid black; text-align: right;">'.$productPrice.'</td>';
                $productRows .= '<td style="border: 1px solid black; text-align: right;">'.($product->revers ? 'reverss' : ($this->firstClient->vat_payer ? $product->vat : '---')).'</td>';
                $productRows .= '<td style="border: 1px solid black; text-align: right;">'.number_format($product->summa, 2, '.', ' ').'</td>';
                $productRows .= '</tr>';
                
                $hasRevers = $hasRevers || $product->revers;
                $reversTotal += ($product->revers ? $product->summa_vat : 0);
            }
            if($this->invoice->according_contract && (count($this->billProducts) > 1)){
                $productRows .= '<tr>';
                $productRows .= '<td colspan="5" style="border: 1px solid black; text-align: right;">Nr. '.$this->agreement->number.'</td>';
                $productRows .= '<td style="border: 1px solid black;"></td>';
                $productRows .= '</tr>';
            }            
        }
        
        $reversRow = ($hasRevers || !empty($reversTotal) ? 
            '<tr>
                <td colspan="5" style="border: 1px solid white; font-style: italic;">*'.
			'Nodokļa apgrieztā maksāšana'.(!empty($reversTotal) ? ': '. number_format($reversTotal, 2, '.', ' '): '').
		'</td>
            </tr>' : ''
        );
            
        $tableHTML = 
        '<table border="0" cellspacing="0" cellpadding="4">
            <tr style="font-weight: bold; text-align: center; ">
                <th style="width: 45%; border: 1px solid black;">'.($this->invoice->doc_type == BILL::BILL_DOC_TYPE_INVOICE ? 'Prece' : 'Pakalpojums').'</th>
                <th style="width: 10%; border: 1px solid black;">Mērv.</th>
                <th style="width: 10%; border: 1px solid black;">Daudz.</th>
                <th style="width: 12%; border: 1px solid black;">Cena</th>
                <th style="width: 8%; border: 1px solid black;">PVN %</th>
                <th style="width: 15%; border: 1px solid black;">Summa, '.strtoupper($this->invoice->valuta->name).'</th>
            </tr>'.
            $productRows.
            '<tr style="font-weight: bold; text-align: right;">
                <td colspan="5" style="border-left: 1px solid black; border-right: 1px solid black;">Summa:</td>
                <td style="border: 1px solid black;">'.$invoiceSumma.'</td>
            </tr>
            <tr style="font-weight: bold; text-align: right;">
                <td colspan="5" style="border-left: 1px solid black; border-right: 1px solid black;">PVN:</td>
                <td style="border: 1px solid black;">'.(!empty((float)$invoiceVat) ? $invoiceVat : '---').'</td>
            </tr>'.
            '<tr style="font-weight: bold; text-align: right;">
                <td colspan="5" style="border-left: 1px solid black; border-right: 1px solid black;">Kopā:</td>
                <td style="border: 1px solid black;">'.$invoiceTotal.'</td>
            </tr>
            <tr>
                <td colspan="6" style="border: 1px solid black;">'.
                    '<span style="font-weight: bold;">'.
                        $avansText.
                        'Summa vārdos:'.
                    '</span>'.
                    '<br/>'.
                    $sumWords.
                '</td>
            </tr>'.
            $reversRow.
        '</table>';
        $this->writeHTML($tableHTML, true, false, true, false, '');

        if(!empty($this->invoice->comment_special)){
            $this->SetFont('freesans', 'B', 10);
            $this->Cell(85, 0, 'Speciālas atzīmes: ', 0, 1, 'L');
            $this->SetFont('freesans', 'I', 10);
            $this->MultiCell(170, 0, $this->invoice->comment_special, 0, 'L');
        }
                        
        if($this->invoice->doc_type == 'invoice'){
            if(empty($this->invoice->e_signing)){
                $this->checkPageBreak($signingHeight);
            }
            
            $this->SetFont('freesans', '', 10);
            $this->writeHTMLCell(85, 0, $this->GetX(), $this->GetY(), '<b>Pārvadātājs:</b> '.$this->invoice->carrier, 0, 0, false, true, 'L');
            $this->writeHTMLCell(85, 0, $secondClientX, $this->GetY(), '<b>Transports:</b> '.$this->invoice->transport, 0, 1, false, true, 'L');
            
            $this->SetFont('freesans', 'B', 10);
            $this->Cell(85, 0, 'Iekraušanas adrese', 0, 0, 'L');
            $this->SetX($secondClientX);
            $this->Cell(85, 0, 'Izkraušanas adrese', 0, 1, 'L');
            
            $yRow = $this->GetY();
            $this->SetFont('freesans', '', 10);
            $this->MultiCell(85, 0, $this->invoice->loading_address, 0, 'L', false, 0);
            $this->Ln();
            $yCol1 = $this->GetY();
            $this->SetY($yRow);
            $this->MultiCell(85, 0, $this->invoice->unloading_address, 0, 'L', false, 1, $secondClientX);
            $yCol2 = $this->GetY();
            if($yCol1 > $yCol2){
                $this->SetY($yCol1);
            }
            
            $this->Ln();
            
            $this->SetFont('freesans', 'I', 10);
            $this->Cell(90, 0, 'Izsniegšanas datums:', 0, 0, 'L');
            $this->Cell(85, 0, 'Saņēmšanas datums:', 0, 1, 'L');
            
            $this->Line($margins['left'], $this->GetY(), 80 + $margins['left'], $this->GetY(), ['width' => 0.2]);
            $this->Line($secondClientX, $this->GetY(), $pWidth, $this->GetY(), ['width' => 0.2]);
            
            $this->Ln();
        }
        
        if(empty($this->invoice->e_signing)){
            $this->Ln();
            $this->SetFont('freesans', 'I', 10);
            $this->checkPageBreak($signingHeight);

            $this->Cell(85, 0, (isset($this->agreement->firstClientRole) ? $this->agreement->firstClientRole->name.':' : ''), 0, 0, 'L');
            $this->SetX($secondClientX);
            if(!empty($this->agreement->secondClientRole)){
                $this->Cell(85, 0, (isset($this->agreement->secondClientRole) ? $this->agreement->secondClientRole->name.':' : ''), 0, 1, 'L');
            }else{
                $this->Cell(85, 0, '', 0, 1);
            }

            $yClient = $this->GetY();
            $firstPersonPrinted = false;
            foreach ($this->billPersons as $billPerson) {
                if(($billPerson->person_order == 1) && !empty($billPerson->clientPerson)){
                    $firstPersonPrinted = true;
                    $this->Ln();
                    $this->Line($margins['left'], $this->GetY(), 80 + $margins['left'], $this->GetY(), ['width' => 0.2]);
                    
                    $this->SetFont('freesans', 'B', 10);
                    $this->Cell(85, 0, ($billPerson->clientPerson->first_name . ' ' . $billPerson->clientPerson->last_name), 0, 1, 'C');
                    $this->SetFont('freesans', 'I', 10);
                    $this->Cell(85, 0, (!empty($billPerson->clientPerson->position_id) ? $billPerson->clientPerson->position->name : ''), 0, 1, 'C');
                }
            }
            if(!$firstPersonPrinted){
                $this->Ln();
                $this->Line($margins['left'], $this->GetY(), 80 + $margins['left'], $this->GetY(), ['width' => 0.2]);
            }
            
            $this->SetXY($secondClientX, $yClient);
            $secondPersonPrinted = false;
            foreach ($this->billPersons as $billPerson) {
                if(($billPerson->person_order == 2) && !empty($billPerson->clientPerson)){
                    $secondPersonPrinted = true;
                    $this->Ln();
                    $this->SetX($secondClientX);
                    $this->Line($secondClientX, $this->GetY(), $pWidth, $this->GetY(), ['width' => 0.2]);
                    
                    $this->SetFont('freesans', 'B', 10);
                    $this->Cell(85, 0, ($billPerson->clientPerson->first_name . ' ' . $billPerson->clientPerson->last_name), 0, 1, 'C');
                    $this->SetX($secondClientX);
                    $this->SetFont('freesans', 'I', 10);
                    $this->Cell(85, 0, (!empty($billPerson->clientPerson->position_id) ? $billPerson->clientPerson->position->name : ''),  0, 1, 'C');
                }
            }

            if(!$secondPersonPrinted){
                $this->Ln();
                $this->SetX($secondClientX);
                $this->Line($secondClientX, $this->GetY(), $pWidth, $this->GetY(), ['width' => 0.2]);
            }

        } else {
            $this->SetFont('freesans', '', 10);
            $this->Cell(85, 0, 'Šis rēķins ir sagatavots elektroniski un ir derīgs bez paraksta');
        }

            /*
        $template = dirname(__FILE__) . '/invoice.pdf';
        $pageCount = $this->setSourceFile($template);
        $tplIdx = $this->importPage(1, '/MediaBox');
        $this->useTemplate($tplIdx);
         * 
         */

    }

}