<?php
return [
    'appSector' => 'finance',
    'appID' => 'rekini',
    'brandLabel' => 'E-invoices',
    'brandVersion' => '2.4.0',
    'brandOwner' => 'Globe Campus',
    'adminEmail' => 'filsm@inbox.lv',
    'supportEmail' => 'noreplay@globe-campus.com',
    'timezone' => 'Europe/Riga',
    'bsVersion' => '3.x',
    'icon-framework' => 'fa',

    'defaultPhoneCode' => '(+371)',
    'showTranslationErrors' => false,   
    
    'lursoftEnabled' => false,
    'LURSOFT_USER_ID' => 'ecf_xml',
    'LURSOFT_PASSWORD' => 'spring_time',
    
    'billDefaultPrintLanguage' => 'en',
    
    'user.passwordResetTokenExpire' => 3600,
    'dialogTimer.showMinutes' => 3,
    
    'googleMapsApiKey' => 'AIzaSyBo7DbxQhLDjD2MiJeruS73zhXAZdK0EpA',
    'googleMapsLibraries' => 'places',
    'googleMapsLanguage' => 'LV',
    
    'googleCalendarApiKey' => 'AIzaSyCrik1i8qKhZ4lJk3Hw9UaOJ4-LB5GSZ5Q',
    'commonGoogleCalendarId' => 'nnmg4v1r2ujnhn1kivpjtocivc@group.calendar.google.com',
    
    'uploadPath' => dirname(dirname(__DIR__)) . '/uploads/',
    
    'DatePickerPluginOptions' => [
        'format' => 'dd-mm-yyyy',
        'todayBtn' => true,
        'todayHighlight' => true,
        'autoclose' => true,
        'keepEmptyValues' => true,
        'weekStart' => 1,
        'zIndexOffset' => 1010,
    ],
    
    'PjaxModalOptions' => [
        'enablePushState' => false,
        'enableReplaceState' => false,
        'clientOptions' => [
            'skipOuterContainers' => true,
        ],         
        'options' => [
            'class' => 'modal-pjax',
        ],
    ],
    
    'EMAILS_DEFAULT_SENDER' => "noreplay@globe-campus.com",
    'EMAILS_DEFAULT_RECIPIENT' => "filsm@inbox.lv",
    'EMAILS_WITH_NAME' => true,
    'EMAILS_DEFAULT_CONTACT' => 
        "
        <div>
            <strong>Globe Campus</strong> <br/>
            <table style=\"font-family: 'Tahoma'; font-size: 12px; color: #434343\">
                <tr>
                    <td>E-mail: </td>
                    <td><a href='mailto:info@globe-campus.com'><strong style='color: #434343;'>info@globe-campus.com</strong></a></td>
                </tr>
            </table>
        </div>
        ",
    
    'STRONG_PASSWORD_CONTROL' => true,
    'ABONENT_ENABLED' => true,
    'EMAILS_PRINT_LOGO' => true,
    'EMAILS_NOTIF_PRINT_LOGO' => false,
    'SHOW_COPYRIGHT' => false,
    'SMS_AUTH' => true,
    'ENABLE_BILL_TRANSFER' => false,
    'ENABLE_LOGOUT_TIMER' => true,
    'ENABLE_GOOGLE_CALENDAR' => false,
    'USE_COMMON_GOOGLE_CALENDAR' => false,
    'COMPLETE_BLOCKED' => false,
    
];
