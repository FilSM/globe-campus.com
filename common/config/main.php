<?php
use kartik\mpdf\Pdf;

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    //'language' => 'ru-RU',
    'language' => 'en-GB',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        'errorHandler' => [
            'class' => 'common\components\FSMErrorHandler',
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'class' => 'common\components\FSMUrlManager',
            //'languages' => ['en-GB', 'en', 'lv', 'ru'],
            //'languages' => ['en' => 'en-GB', 'lv' => 'lv-LV', 'ru' => 'ru-RU'],
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableLanguagePersistence' => false,
            'enableLanguageDetection' => false,
            'ignoreLanguageUrlPatterns' => [
                '#^backend#' => '#^backend#',
                '#ajax#' => '#ajax#',
            ],
        ],
	
        'authManager' => [
            'class' => /*'yii\rbac\PhpManager', //*/ /*'yii\rbac\DbManager'//*/ 'common\components\rbac\FilSMDbManager',
            //'defaultRoles' => ['user'],
        ],
        
        'i18n' => [
            'translations' => [
                '*' => [
                    //'class' => 'yii\i18n\DbMessageSource',
                    'class' => 'common\components\FSMDbMessageSource',
                    'db' => 'db',
                    'sourceLanguage' => 'en-US',
                    //'basePath' => '@common/messages/bank',
                    'sourceMessageTable' => '{{%language_source}}',
                    'messageTable' => '{{%language_translate}}',
                    'cachingDuration' => 86400,
                    //'enableCaching' => true,  
                    'ignoredCategories' => ['language', 'javascript', 'model'], // these categories won’t be included in the language database.                    
                    'on missingTranslation' => ['common\components\FSMTranslationEventHandler', 'handleMissingTranslation']
                ],
            ]
        ],

        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@backend/views/user'
                ],
            ],
        ],

        'queue' => [
            'class' => \yii\queue\file\Queue::class,
            'path' => '@console/runtime/queue',
            'as log' => \yii\queue\LogBehavior::class,
        ],
	
        'pdf' => [
            'class' => Pdf::class,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            // refer settings section for all configuration options
        ],        
	        
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            //'enableUnconfirmedLogin ' => true,
            'enableFlashMessages' => true,
            'enableRegistration' => false,
            //'enableAccountDelete' => true,
            'confirmWithin' => 21600,
            'cost' => 12,
            //'admins' => ['FilSM'],
            'adminPermission' => 'administrateUser',
            'modelMap' => [
                'User' => 'common\models\user\FSMUser',
                'Profile' => 'common\models\user\FSMProfile',
                'LoginForm' => 'common\models\user\FSMLoginForm',
                'SettingsForm' => 'common\models\user\FSMSettingsForm',
                'RegistrationForm' => 'common\models\user\FSMRegistrationForm',
                'ResendForm' => 'common\models\user\FSMResendForm',
            ],
            'controllerMap' => [
                'admin' => 'backend\controllers\user\AdminController',
                'settings' => 'backend\controllers\user\SettingsController',
                'security' => 'backend\controllers\user\SecurityController',
                'profile' => 'backend\controllers\user\ProfileController',
                //'register' => 'backend\controllers\user\RegistrationController',
            ],
        ],
        'dynagrid'=> [
            'class'=>'\kartik\dynagrid\Module',
            // other module settings
        ],        
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
        'translatemanager' => [
            'class' => 'lajax\translatemanager\Module',
            /*
            'root' => '@app',               // The root directory of the project scan.
            'layout' => 'language',         // Name of the used layout. If using own layout use 'null'.
             * 
             */ 
            'allowedIPs' => ['*'],  // IP addresses from which the translation interface is accessible.
            //'allowedIPs' => ['127.0.0.1', '::1', '185.8.62.116'],  // IP addresses from which the translation interface is accessible.
            /*
            'roles' => ['@'],               // For setting access levels to the translating interface.
            'tmpDir' => '@runtime',         // Writable directory for the client-side temporary language files. 
                                            // IMPORTANT: must be identical for all applications (the AssetsManager serves the JavaScript files containing language elements from this directory).
            'phpTranslators' => ['::t'],    // list of the php function for translating messages.
            'jsTranslators' => ['lajax.t'], // list of the js function for translating messages.
            'patterns' => ['*.js', '*.php'],// list of file extensions that contain language elements.
             * 
             */ 
            'ignoredCategories' => ['yii', 'user', 
                'language', 'array', 'javascript', 'model',
                'kvgrid', 'kvdrp', 'kvdetail', 'kvdialog', 'kvenum', 'kvbase', 
                'kvpwdstrength', 'kvcolor', 'kvdate', 'kvdtime', 'kvselect', 
                'fileinput', 'rbac-admin'
            ], // these categories won’t be included in the language database.
            'ignoredItems' => ['config'],                   // these files will not be processed.
            'languageTable' => '{{%language}}',
            'tables' => [                                   // Properties of individual tables
                [
                    'connection' => 'db',                   // connection identifier
                    'table' => 'location_country',          // table name
                    'columns' => ['name'],                  //names of multilingual fields
		    'category' => 'database-table-name',    // the category is the database table name
                ],               
            ],
        ],
    ],
    /*
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            '*', // add or remove allowed actions to this list
            //'admin/*', // add or remove allowed actions to this list
            //'site/*', // add or remove allowed actions to this list
        ]
    ],
     * 
     */
    'controllerMap' => [
        'abonent' => [
            'class' => 'backend\controllers\abonent\AbonentController',
            'viewPath' => '@backend/views/abonent/abonent',
        ],           
        'abonent-agreement' => [
            'class' => 'backend\controllers\abonent\AbonentAgreementController',
            'viewPath' => '@backend/views/abonent/abonent-agreement',
        ],         
        'abonent-bill' => [
            'class' => 'backend\controllers\abonent\AbonentBillController',
            'viewPath' => '@backend/views/abonent/abonent-bill',
        ],         
        'daemon' => [
            'class' => 'console\controllers\DaemonController',
        ],
        'account-map' => [
            'class' => 'backend\controllers\AccountMapController',
            'viewPath' => '@backend/views/account-map',
        ],
        'sms-auth' => [
            'class' => 'backend\controllers\user\SMSAuthController',
            'viewPath' => '@backend/views/user/security',
        ],
        'address' => [
            'class' => 'backend\controllers\address\AddressController',
            'viewPath' => '@backend/views/address/address',
        ],
        'country' => [
            'class' => 'backend\controllers\address\CountryController',
            'viewPath' => '@backend/views/address/country',
        ],
        'region' => [
            'class' => 'backend\controllers\address\RegionController',
            'viewPath' => '@backend/views/address/region',
        ],
        'subregion' => [
            'class' => 'backend\controllers\address\SubregionController',
            'viewPath' => '@backend/views/address/subregion',
        ],
        'city' => [
            'class' => 'backend\controllers\address\CityController',
            'viewPath' => '@backend/views/address/city',
        ],
        'district' => [
            'class' => 'backend\controllers\address\DistrictController',
            'viewPath' => '@backend/views/address/district',
        ],
        'bank' => [
            'class' => 'backend\controllers\BankController',
            'viewPath' => '@backend/views/bank',
        ],        
        'valuta' => [
            'class' => 'backend\controllers\ValutaController',
            'viewPath' => '@backend/views/valuta',
        ],          
        'client-role' => [
            'class' => 'backend\controllers\ClientRoleController',
            'viewPath' => '@backend/views/client-role',
        ],      
        'person-position' => [
            'class' => 'backend\controllers\PersonPositionController',
            'viewPath' => '@backend/views/person-position',
        ],      
        'client' => [
            'class' => 'frontend\controllers\client\ClientController',
            'viewPath' => '@frontend/views/client/client',
        ],      
        'client-group' => [
            'class' => 'backend\controllers\ClientGroupController',
            'viewPath' => '@backend/views/client-group',
        ],      
        'client-contact' => [
            'class' => 'frontend\controllers\client\ClientContactController',
            'viewPath' => '@frontend/views/client/client-contact',
        ],      
        'client-bank' => [
            'class' => 'frontend\controllers\client\ClientBankController',
            'viewPath' => '@frontend/views/client/client-bank',
        ],      
        'client-bank-balance' => [
            'class' => 'frontend\controllers\client\ClientBankBalanceController',
            'viewPath' => '@frontend/views/client/client-bank-balance',
        ],      
        'share' => [
            'class' => 'frontend\controllers\client\ShareController',
            'viewPath' => '@frontend/views/client/share',
        ],      
        'shareholder' => [
            'class' => 'frontend\controllers\client\ShareholderController',
            'viewPath' => '@frontend/views/client/shareholder',
        ],      
        'mail-template' => [
            'class' => 'frontend\controllers\client\ClientMailTemplateController',
            'viewPath' => '@frontend/views/client/mail-template',
        ],      
        'agreement' => [
            'class' => 'frontend\controllers\client\AgreementController',
            'viewPath' => '@frontend/views/client/agreement',
        ],          
        'agreement-payment' => [
            'class' => 'frontend\controllers\client\AgreementPaymentController',
            'viewPath' => '@frontend/views/client/agreement-payment',
        ],          
        'agreement-history' => [
            'class' => 'frontend\controllers\client\AgreementHistoryController',
            'viewPath' => '@frontend/views/client/agreement-history',
        ],         
        'project' => [
            'class' => 'frontend\controllers\client\ProjectController',
            'viewPath' => '@frontend/views/client/project',
        ],          
        'product' => [
            'class' => 'backend\controllers\ProductController',
            'viewPath' => '@backend/views/product',
        ],          
        'measure' => [
            'class' => 'backend\controllers\MeasureController',
            'viewPath' => '@backend/views/measure',
        ],          
        'bill' => [
            'class' => 'frontend\controllers\bill\BillController',
            'viewPath' => '@frontend/views/bill/bill',
        ],          
        'bill-template' => [
            'class' => 'frontend\controllers\bill\BillTemplateController',
            'viewPath' => '@frontend/views/bill/bill-template',
        ],          
        'bill-history' => [
            'class' => 'frontend\controllers\bill\BillHistoryController',
            'viewPath' => '@frontend/views/bill/bill-history',
        ],          
        'payment-order' => [
            'class' => 'frontend\controllers\bill\PaymentOrderController',
            'viewPath' => '@frontend/views/bill/payment-order',
        ],          
        'payment-confirm' => [
            'class' => 'frontend\controllers\bill\PaymentConfirmController',
            'viewPath' => '@frontend/views/bill/payment-confirm',
        ],          
        'bill-payment' => [
            'class' => 'frontend\controllers\bill\BillPaymentController',
            'viewPath' => '@frontend/views/bill/bill-payment',
        ],          
        'bill-confirm' => [
            'class' => 'frontend\controllers\bill\BillConfirmController',
            'viewPath' => '@frontend/views/bill/bill-confirm',
        ],          
        'convention' => [
            'class' => 'frontend\controllers\bill\ConventionController',
            'viewPath' => '@frontend/views/bill/convention',
        ],          
        'reg-doc-type' => [
            'class' => 'backend\controllers\RegDocTypeController',
            'viewPath' => '@backend/views/reg-doc-type',
        ],          
        'client-reg-doc' => [
            'class' => 'frontend\controllers\client\ClientRegDocController',
            'viewPath' => '@frontend/views/client/reg-doc',
        ],         
        'client-invoice-pdf' => [
            'class' => 'frontend\controllers\client\ClientInvoicePdfController',
            'viewPath' => '@frontend/views/client/client-invoice-pdf',
        ],
        'client-history' => [
            'class' => 'frontend\controllers\client\ClientHistoryController',
            'viewPath' => '@frontend/views/client/client-history',
        ],
        'expense-type' => [
            'class' => 'backend\controllers\ExpenseTypeController',
            'viewPath' => '@backend/views/expense-type',
        ],          
        'expense' => [
            'class' => 'frontend\controllers\bill\ExpenseController',
            'viewPath' => '@frontend/views/bill/expense',
        ],          
        'base-report' => [
            'class' => 'frontend\controllers\report\BaseReportController',
            'viewPath' => '@frontend/views/report',
        ],      
        'event' => [
            'class' => 'frontend\controllers\event\EventController',
            'viewPath' => '@frontend/views/event',
        ],         
        'event-resurs' => [
            'class' => 'backend\controllers\EventResursController',
            'viewPath' => '@backend/views/event-resurs',
        ],
        'dashboard' => [
            'class' => 'frontend\controllers\dashboard\DashboardController',
            'viewPath' => '@frontend/views/dashboard',
        ],        
        'tax-payment' => [
            'class' => 'frontend\controllers\bill\TaxPaymentController',
            'viewPath' => '@frontend/views/bill/tax-payment',
        ],        
    ],
];
