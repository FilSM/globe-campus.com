<?php

namespace common\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

use common\controllers\FilSMController;
use common\models\user\FSMUser;

/**
 * Controller implements the CRUD actions for model.
 */
class AdminListController extends FilSMController {
    
    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors = ArrayHelper::merge(
            $behaviors,
                [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'controllers' => [
                                'language', 
                                'bank', 
                                'valuta', 
                                'measure', 
                                'product', 
                                'person-position', 
                                'action',
                                'reg-doc-type',
                                'event-resurs',
                                'account-map',
                            ],
                            'allow' => FSMUser::getIsPortalAdmin() || Yii::$app->user->can('showBackend'),
                            //'roles' => ['superuser', 'portal_admin', 'admin', 'boss', 'manager'],
                        ],
                    ],
                ],
            ]
        );
        return $behaviors;
    }
    
}
