<?php

namespace common\controllers;

use Yii;
use yii\base\ViewContextInterface;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;

use common\traits\AjaxValidationTrait;
use common\assets\ButtonDeleteAsset;

/**
 * Controller implements the CRUD actions for model.
 */
class FilSMController extends Controller  implements ViewContextInterface {
    
    use AjaxValidationTrait;
    
    protected $defaultModel = null;
    protected $defaultSearchModel = null;
    protected $_viewPath;
    protected $pjaxIndex = false;

    public function behaviors() {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    // allow authenticated users
                    [
                        'roles' => ['@'],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
                            'ajax-get-model', 
                            'sms-login',
                        ],
                        'allow' => true,
                    ],                     
                ],
            ],         
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        if($action->id == 'delete'){
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }
    
    /**
     * Lists all models.
     * @return mixed
     */
    public function actionIndex() {
        ButtonDeleteAsset::register(Yii::$app->getView());
        
        if(isset($this->defaultSearchModel)){
            $searchModel = new $this->defaultSearchModel;
            if(!$searchModel){
                $className = $this->className();
                throw new Exception("For the {$className} defaultSearchModel not defined.");
            }        
            $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]);
        }else{
            return $this->render('index');
        }
    }
    
    /**
     * Displays a single model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        ButtonDeleteAsset::register(Yii::$app->getView());
        
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new $this->defaultModel;

        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxValidation($model);
        }

        if ($model->load($requestPost) && $model->save()) {
            return $this->redirect('index');
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxValidation($model);
        }

        if ($model->load($requestPost) && $model->save()) {
            return $this->redirectToBackUrl($id);              
        } else {
            $this->rememberBackUrl($model->backURL, $id);            
            
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    /**
     * Deletes an existing single model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            $model = $this->findModel($id);
            $backUrl = $model->backURL;
            $backUrl = is_array($backUrl) ? $backUrl[0] : $backUrl;
            $pjax = $this->pjaxIndex && (strpos($backUrl, 'view') === false);
            $model->delete();
            $transaction->commit();            
        } catch (\Exception $e) {
            $transaction->rollBack();
            $message = $e->getMessage();
            if(!$pjax){
                Yii::$app->getSession()->setFlash('error', $message);
            }            
            Yii::error($message, __METHOD__);
        } finally {
            if(!$pjax){
                return $this->redirect(['index']);
            }else{
                return;
            }
        }         
    }

    /**
     * Finds the single model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return The loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $withScope = false) {
        $className = $this->className();
        $defaultModel = $this->defaultModel;
        if($defaultModel){
            if (($model = $defaultModel::findOne($id, $withScope)) !== null) {
                return $model;
            } else {
                throw new Exception('The requested page does not exist.');
            }
        }else{
            throw new Exception("For the {$className} findModel function not defined.");
        }
    }
    
    public function actionAjaxGetModel($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (empty($id)) {
            return [];
        }
        $model = $this->findModel($id);
        return $model;
    }    

    public function actionAjaxNameList($q = null) {
        $q = trim($q);
        $args = $_GET;
        if (isset($args['q'])) {
            unset($args['q']);
        }
        $out = [];
        if (isset($this->defaultModel)) {
            $model = new $this->defaultModel;
            $data = $model::getNameList($q, $args);
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $out[] = ['id' => $key, 'name' => $value];
                }
            }
        }
        echo Json::encode($out);
        return false;
    }

    public function actionAjaxSelect2NameList($q = null) {
        $q = trim($q);
        $args = $_GET;
        if (isset($args['q'])) {
            unset($args['q']);
        }
        $out = [];
        $out['results'][] = ['id' => '', 'text' => ''];
        if (isset($this->defaultModel)) {
            $model = new $this->defaultModel;
            $data = $model::getNameList($q, $args);
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $out['results'][] = ['id' => $key, 'text' => $value]; // !!! 'text' is needed for Select2 templateResult & templateSelection functions
                }
            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $out;
    }    
    
    public function actionAjaxModalNameList($param = [])
    {
        if(!isset($this->defaultModel)){
            return [];
        }
        
        $model = new $this->defaultModel;
        
        $selectedId = null;
        if(isset($param['selected_id'])){
            $selectedId = $param['selected_id'];
            unset($param['selected_id']);
        }
        $param = !empty($param) ? $param : null;
        
        if($model->hasAttribute('deleted') && !isset($param['deleted'])){
            $param['deleted'] = false;
        }
        
        // JSON response is expected in case of successful save
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = $model::getNameArr($param, $model::$nameField, $model::$keyField, $model::$nameField);
        unset($model);
        
        $result = [];
        foreach ($data as $key => $item) {
            $result[] = [
                'id' => $key,
                'text' => $item,
            ];
        }
        
        return [
            'success' => true,
            'selected' => $selectedId,
            'data' => $result,
        ];        
    }
    
    public function rememberBackUrl($url, $id)
    {
        Url::remember($url, get_class($this).'_'.$id);
    }
    
    public function redirectToBackUrl($id)
    {
        $backUrl = Url::previous(get_class($this).'_'.$id);
        Yii::$app->getSession()->set(get_class($this).'_'.$id, null);
        return $this->redirect($backUrl);
    }

    public function actionAttachmentDelete($deleted_id)
    {
        $session = Yii::$app->session;
        $deletedIds = (array)($session->has('file_delete') ? $session['file_delete'] : []);
        if(!in_array($deleted_id, $deletedIds)){
            array_push($deletedIds, $deleted_id);
        }
        $session->set('file_delete', $deletedIds);
        echo Json::encode(['output' => true]);
        return false;
    }     
}
