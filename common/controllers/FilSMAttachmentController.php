<?php

namespace common\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\base\Exception;

use common\controllers\FilSMController;
use common\models\Files;

class FilSMAttachmentController extends FilSMController
{
    protected $attachmentDir = 'attachment';
    protected $attachmentClass;
    protected $foreignKeyField;
    protected $attachmentField = 'uploaded_file_id';
    
    protected function createAttachment($model, $filesModel)
    {
        $attachmentClass = $this->attachmentClass;
        $foreignKeyField = $this->foreignKeyField;
        $attachemntField = $this->attachmentField;

        try {
            $files = UploadedFile::getInstances($filesModel[0], 'file');
            foreach ($files as $uploadedFile) {
                $filesModel = new Files();
                $file = $filesModel->uploadFileMultiply($uploadedFile, $this->attachmentDir.'/'.$model->id);
                if (!empty($file)) {
                    if(!$filesModel->save()){
                        throw new Exception('Can not save the file! '.$filesModel->errorMessage);
                    }

                    $attachment = new $attachmentClass();
                    $attachment->$foreignKeyField = $model->id;
                    $attachment->$attachemntField = $filesModel->id;
                    if(!$attachment->save()){
                        throw new Exception('Can not save the Attachment! '.$attachment->errorMessage);
                    }
                }
            }        
        } catch (Exception $e) {
            $message = $e->getMessage();
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
            return;
        }

        return true;
    }
    
    protected function updateAttacment($model, $filesModel)
    {
        $attachmentClass = $this->attachmentClass;
        $foreignKeyField = $this->foreignKeyField;
        $attachemntField = $this->attachmentField;

        try {
            $session = Yii::$app->session;
            $deletedIDs = (array)($session->has('file_delete') ? $session['file_delete'] : []);
            if(!empty($deletedIDs)){
                $attachmentList = $model->attachments;
                foreach ($attachmentList as $attachment) {
                    if(in_array($attachment->$attachemntField, $deletedIDs)){
                        $attachment->delete();
                    }
                }
            }

            $files = UploadedFile::getInstances($filesModel[0], 'file');
            foreach ($files as $uploadedFile) {
                $filesModel = new Files();
                $file = $filesModel->uploadFileMultiply($uploadedFile, $this->attachmentDir.'/'.$model->id);
                if (!empty($file)) {
                    if(!$filesModel->save()){
                        throw new Exception('Can not save the file! '.$filesModel->errorMessage);
                    }
                    $attachment = new $attachmentClass();
                    $attachment->$foreignKeyField = $model->id;
                    $attachment->$attachemntField = $filesModel->id;
                    if(!$attachment->save()){
                        throw new Exception('Can not save the Attachment! '.$attachment->errorMessage);
                    }
                }
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
            return;
        }
        
        return true;
    }    
}
