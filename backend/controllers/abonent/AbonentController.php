<?php

namespace backend\controllers\abonent;

use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Response;

use common\controllers\FilSMController;
use common\models\mainclass\FSMBaseModel;
use common\models\user\FSMUser;
use common\models\user\FSMProfile;
use common\models\Bank;
use common\models\Valuta;
use common\models\Language;
use common\models\client\PersonPosition;
use common\models\client\Client;
use common\models\client\ClientGroup;
use common\models\address\Country;
use common\models\client\ClientBank;
use common\models\client\ClientContact;
use common\models\Files;
use common\models\abonent\Abonent;
use common\models\abonent\AbonentClient;
use frontend\assets\ClientUIAsset;
use common\assets\ButtonDeleteAsset;

/**
 * AbonentController implements the CRUD actions for Abonent model.
 */
class AbonentController extends FilSMController
{

    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\abonent\Abonent';
        $this->defaultSearchModel = 'common\models\abonent\search\AbonentSearch';
        $this->pjaxIndex = true;
    }

    public function actionAjaxGetProjectList()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        $selected = null;
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            if (empty($id) || !is_numeric($id)) {
                return ['output' => '', 'selected' => $selected];
            }
            $list = $this->findModel($id)->projects;
            if (count($list) > 0) {
                foreach ($list as $i => $item) {
                    $out[] = ['id' => $item->id, 'name' => $item->name];
                }
            }
            $selected = (count($out) == 1 ? strval($out[0]['id']) : '');
        }
        // Shows how you can preselect a value  
        return ['output' => $out, 'selected' => $selected];
    }

    /**
     * Lists all models.
     * @return mixed
     */
    public function actionIndex()
    {
        ButtonDeleteAsset::register(Yii::$app->getView());

        $searchModel = new $this->defaultSearchModel;
        if (!$searchModel) {
            $className = $this->className();
            throw new Exception("For the {$className} defaultSearchModel not defined.");
        }
        $params = Yii::$app->request->getQueryParams();
        $params['deleted'] = (empty($params) || empty($params['AbonentSearch'])) ? 0 : (int) !empty($params['AbonentSearch']['deleted']);

        $dataProvider = $searchModel->search($params);

        $currentAbonentId = Yii::$app->session->get('user_current_abonent_id');
        $mainClientId = Abonent::findOne($currentAbonentId)->mainClient->id;
        $isAdmin = FSMUser::getIsPortalAdmin();
        $managerList = FSMProfile::getProfileListByRole([
            FSMUser::USER_ROLE_ADMIN,
            FSMUser::USER_ROLE_OPERATOR,
            FSMUser::USER_ROLE_BOOKER,
            FSMUser::USER_ROLE_COORDINATOR,
            FSMUser::USER_ROLE_MANAGER,
            FSMUser::USER_ROLE_TEHNICAL,
            FSMUser::USER_ROLE_LEGAL_TAX,
        ]);
        $profile = Yii::$app->user->identity->profile;
        $profileId = isset($profile) ? $profile->id : null;
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'managerList' => $managerList,
            'isAdmin' => $isAdmin,
            'profileId' => $profileId,
            'mainClientId' => $mainClientId,
        ]);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new $this->defaultModel;
        $clientModel = new Client();
        $clientModel->scenario = 'abonent';
        $abonentModel = new AbonentClient();
        $filesModel = new Files();
        $mainContactModel = new ClientContact();
        $mainContactModel->scenario = 'client';

        $modelArr = [
            'Abonent' => $model,
            'Client' => $clientModel,
            'AbonentClient' => $abonentModel,
        ];

        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            // ajax validation
            $ajaxModelArr = $modelArr;
            $clientBankModel = FSMBaseModel::createMultiple(ClientBank::class);
            FSMBaseModel::loadMultiple($clientBankModel, $requestPost);
            foreach ($clientBankModel as $index => $clientBank) {
                if (empty($clientBank->bank_id) && empty($clientBank->account) && empty($clientBank->name)) {
                    unset($clientBankModel[$index]);
                    continue;
                }
            }
            if (!empty($clientBankModel)) {
                $ajaxModelArr = ArrayHelper::merge($modelArr, ['ClientBank' => $clientBankModel]);
            }
            return $this->performAjaxMultipleValidation($ajaxModelArr);
        }

        $modelArr['ClientContact'] = $mainContactModel;

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {

            $transaction = Yii::$app->getDb()->beginTransaction();
            try {

                if (!empty($_POST['use_legal_address'])) {
                    $clientModel->office_address = $clientModel->legal_address;
                    $clientModel->office_country_id = $clientModel->legal_country_id;
                }

                $file = $filesModel->uploadFile('logo');
                if (!empty($file)) {
                    $filesModel->save();
                }
                $clientModel->uploaded_file_id = $filesModel->id;

                if ($existClient = Client::find()->notDeleted()->andWhere(['or',
                            ['reg_number' => $clientModel->reg_number],
                            ['vat_number' => $clientModel->vat_number],
                        ])->one()) {
                    $clientModel->id = $existClient->id;
                    $clientModel->setIsNewRecord(false);
                }

                if (!$clientModel->save()) {
                    throw new Exception('Unable to save data! ' . $clientModel->errorMessage);
                } else {
                    $model->main_client_id = $clientModel->id;
                    $model->manager_id = $abonentModel->manager_id;
                    $model->subscription_end_date = !empty($model->subscription_end_date) ? date('Y-m-d', strtotime($model->subscription_end_date)) : null;
                    if (!$model->save()) {
                        throw new Exception('Unable to save data! ' . $model->errorMessage);
                    } else {
                        if (!empty($abonentModel->parent_id)) {
                            $parent = $abonentModel->parent;
                            $abonentModel->it_is = (!empty($parent->abonents) ? Client::CLIENT_IT_IS_ABONENT : $abonentModel->it_is);
                        } else {
                            $abonentModel->it_is = Client::CLIENT_IT_IS_ABONENT;
                        }
                        $abonentModel->abonent_id = $model->id;
                        $abonentModel->client_id = $clientModel->id;
                        if (!$abonentModel->save()) {
                            throw new Exception('Unable to save data! ' . $abonentModel->errorMessage);
                        }

                        $clientModel->saveMainContact($mainContactModel, $model->id);

                        $clientBankModel = FSMBaseModel::createMultiple(ClientBank::class);
                        FSMBaseModel::loadMultiple($clientBankModel, $requestPost);
                        foreach ($clientBankModel as $index => $clientBank) {
                            if (empty($clientBank->bank_id) && empty($clientBank->account) && empty($clientBank->name)) {
                                unset($clientBankModel[$index]);
                                continue;
                            }
                            $clientBank->abonent_id = $model->id;
                            $clientBank->client_id = $clientModel->id;
                        }

                        if (!empty($clientBankModel)) {
                            if (FSMBaseModel::validateMultiple($clientBankModel)) {
                                foreach ($clientBankModel as $clientBank) {
                                    if (!$clientBank->save(false)) {
                                        throw new Exception('Unable to save data! ' . $clientBank->errorMessage);
                                    }
                                }
                            } else {
                                Exception('Unable to save data!');
                            }
                        }
                    }
                    $transaction->commit();
                }
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirect('index');
            }
        } else {
            ClientUIAsset::register(Yii::$app->getView());

            $clientModel->tax = Client::CLIENT_DEFAULT_VAT_TAX;
            $abonentModel->status = Client::CLIENT_STATUS_ACTIVE;
            $abonentModel->it_is = Client::CLIENT_IT_IS_ABONENT;
            $abonentModel->debit_valuta_id = Valuta::VALUTA_DEFAULT;

            $clientBankModel = [new ClientBank()];

            $isAdmin = FSMUser::getIsPortalAdmin();
            $managerList = FSMProfile::getProfileListByRole([
                FSMUser::USER_ROLE_ADMIN,
                FSMUser::USER_ROLE_OPERATOR,
                FSMUser::USER_ROLE_BOOKER,
                FSMUser::USER_ROLE_COORDINATOR,
                FSMUser::USER_ROLE_MANAGER,
                FSMUser::USER_ROLE_TEHNICAL,
                FSMUser::USER_ROLE_LEGAL_TAX,
            ]);
            $profileId = Yii::$app->user->identity->profile->id;
            if (array_key_exists($profileId, $managerList)) {
                $abonentModel->manager_id = $profileId;
            }

            $countryList = Country::getNameArr();
            $clientGroupList = ClientGroup::getNameArr(['enabled' => true]);
            $bankList = Bank::getNameArr(['enabled' => true], '', '', function($bank) {
                return $bank['name'] . (!empty($bank['swift']) ? ' | ' . $bank['swift'] : '');
            });
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            $languageList = Language::getEnabledLanguageList();
            return $this->render('create', [
                'model' => $model,
                'clientModel' => $clientModel,
                'abonentModel' => $abonentModel,
                'clientBankModel' => $clientBankModel,
                'filesModel' => $filesModel,
                'mainContactModel' => $mainContactModel,
                'clientGroupList' => $clientGroupList,
                'countryList' => $countryList,
                'languageList' => $languageList,
                'bankList' => $bankList,
                'valutaList' => $valutaList,
                'managerList' => $managerList,
                'isAdmin' => $isAdmin,
            ]);
        }
    }

    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $clientModel = $model->mainClient;
        $clientModel = (!empty($clientModel) ? $clientModel : new Client());

        $abonentModel = AbonentClient::findOne(['abonent_id' => $model->id, 'client_id' => $clientModel->id]);
        $abonentModel = !empty($abonentModel) ? $abonentModel : new AbonentClient();
        $clientBankModel = $clientModel->clientBanks;
        $clientBankModel = !empty($clientBankModel) ? $clientBankModel : [new ClientBank()];
        $filesModel = $oldFileModel = $clientModel->logo;
        $filesModel = (!empty($filesModel) ? $filesModel : new Files());
        $mainContactModel = ClientContact::findOne(['client_id' => $clientModel->id, 'main' => 1]);
        $mainContactModel = !empty($mainContactModel) ? $mainContactModel : new ClientContact();

        $clientModel->scenario = 'abonent';
        $mainContactModel->scenario = 'client';

        $modelArr = [
            'Abonent' => $model,
            'Client' => $clientModel,
            'AbonentClient' => $abonentModel,
        ];

        // ajax validation
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && $isAjax) {
            $ajaxModelArr = $modelArr;
            $clientBankModel = FSMBaseModel::createMultiple(ClientBank::class);
            FSMBaseModel::loadMultiple($clientBankModel, $requestPost);
            foreach ($clientBankModel as $index => $clientBank) {
                if (empty($clientBank->bank_id) && empty($clientBank->account) && empty($clientBank->name)) {
                    unset($clientBankModel[$index]);
                    continue;
                }
            }
            if (!empty($clientBankModel)) {
                $ajaxModelArr = ArrayHelper::merge($modelArr, ['ClientBank' => $clientBankModel]);
            }
            return $this->performAjaxMultipleValidation($ajaxModelArr);
        }

        $modelArr['ClientContact'] = $mainContactModel;

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $clientModel->saveMainContact($mainContactModel);

                if (!empty($_POST['use_legal_address'])) {
                    $clientModel->office_address = $clientModel->legal_address;
                    $clientModel->office_country_id = $clientModel->legal_country_id;
                }

                $session = Yii::$app->session;
                $deletedIDs = (array) ($session->has('file_delete') ? $session['file_delete'] : []);
                if (!empty($deletedIDs) && (in_array($filesModel->id, $deletedIDs))) {
                    $filesModel->delete();
                    $filesModel = new Files();
                    $clientModel->uploaded_file_id = null;
                }

                $file = $filesModel->uploadFile('logo');
                if (!empty($file)) {
                    $filesModel->save();
                    $clientModel->uploaded_file_id = $filesModel->id;
                }

                if (!$clientModel->save()) {
                    throw new Exception('Unable to save data! ' . $clientModel->errorMessage);
                } else {
                    if (!empty($abonentModel->parent_id)) {
                        $parent = $abonentModel->parent;
                        $abonentModel->it_is = (!empty($parent->abonents) ? Client::CLIENT_IT_IS_ABONENT : $abonentModel->it_is);
                    }
                    if (!$abonentModel->save()) {
                        throw new Exception('Unable to save data! ' . $abonentModel->errorMessage);
                    }

                    $model->manager_id = $abonentModel->manager_id;
                    $model->deleted = $clientModel->deleted;
                    $model->subscription_end_date = !empty($model->subscription_end_date) ? date('Y-m-d', strtotime($model->subscription_end_date)) : null;
                    if (!$model->save()) {
                        throw new Exception('Unable to save data! ' . $model->errorMessage);
                    } else {
                        $oldBankIDs = isset($clientBankModel[0]) && !empty($clientBankModel[0]->id) ? ArrayHelper::map($clientBankModel, 'id', 'id') : [];

                        $clientBankModel = FSMBaseModel::createMultiple(ClientBank::class, $clientBankModel);
                        FSMBaseModel::loadMultiple($clientBankModel, $requestPost);
                        $deletedBankIDs = array_diff($oldBankIDs, array_filter(ArrayHelper::map($clientBankModel, 'id', 'id')));
                        if (!empty($deletedBankIDs)) {
                            if (!ClientBank::deleteByIDs($deletedBankIDs)) {
                                throw new Exception('Unable to save data! ' . $clientBank->errorMessage);
                            }
                        }

                        foreach ($clientBankModel as $index => $clientBank) {
                            if (empty($clientBank->bank_id) && empty($clientBank->account) && empty($clientBank->name)) {
                                unset($clientBankModel[$index]);
                                continue;
                            }
                            $clientBank->client_id = $clientModel->id;
                        }
                        if (!empty($clientBankModel)) {
                            if (FSMBaseModel::validateMultiple($clientBankModel)) {
                                foreach ($clientBankModel as $clientBank) {
                                    if (!$clientBank->save(false)) {
                                        throw new Exception('Unable to save data! ' . $clientBank->errorMessage);
                                    }
                                }
                            } else {
                                throw new Exception('Unable to save data!');
                            }
                        }
                    }
                }
                $transaction->commit();

                //$model->syncData(Abonent::ABONENT_SYNC_SET_ITEM, $modelArr);
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirectToBackUrl($id);
            }
        } else {
            $this->rememberBackUrl($model->backURL, $id);
            ClientUIAsset::register(Yii::$app->getView());

            $model->subscription_end_date = !empty($model->subscription_end_date) ? date('d-m-Y', strtotime($model->subscription_end_date)) : null;

            $isAdmin = FSMUser::getIsPortalAdmin();
            $clientGroupList = ClientGroup::getNameArr(['enabled' => true]);
            $countryList = Country::getNameArr();
            $bankList = Bank::getNameArr(['enabled' => true], '', '', function($bank) {
                return $bank['name'] . (!empty($bank['swift']) ? ' | ' . $bank['swift'] : '');
            });
            $valutaList = Valuta::getNameArr(['enabled' => true]);
            $languageList = Language::getEnabledLanguageList();
            $managerList = FSMProfile::getProfileListByRole([
                FSMUser::USER_ROLE_ADMIN,
                FSMUser::USER_ROLE_OPERATOR,
                FSMUser::USER_ROLE_BOOKER,
                FSMUser::USER_ROLE_COORDINATOR,
                FSMUser::USER_ROLE_MANAGER,
                FSMUser::USER_ROLE_TEHNICAL,
                FSMUser::USER_ROLE_LEGAL_TAX,
            ]);
            return $this->render('update', [
                'model' => $model,
                'clientModel' => $clientModel,
                'abonentModel' => $abonentModel,
                'clientBankModel' => $clientBankModel,
                'filesModel' => $filesModel,
                'mainContactModel' => $mainContactModel,
                'clientGroupList' => $clientGroupList,
                'countryList' => $countryList,
                'languageList' => $languageList,
                'bankList' => $bankList,
                'valutaList' => $valutaList,
                'managerList' => $managerList,
                'isAdmin' => $isAdmin,
                'isModal' => false,
            ]);
        }
    }

    /**
     * Displays a single model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        ButtonDeleteAsset::register(Yii::$app->getView());

        return $this->render('view', [
            'model' => $this->findModel($id),
            'isAdmin' => FSMUser::getIsPortalAdmin(),
        ]);
    }

    public function actionSwitchAbonent()
    {
        $data = Yii::$app->request->post();
        if (!empty($data['abonent_id'])) {
            Yii::$app->session->set('user_current_abonent_id', $data['abonent_id']);
        }
        return $this->goHome();
    }

    //public function actionSyncApi(){
    public function actionSyncApi($action, $id = null, $data = [])
    {
        if (empty($id)) {
            $model = new $this->defaultModel;
        } else {
            $model = $this->findModel($id);
        }
        $this->rememberBackUrl($model->backURL, $id);
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            $result = $model->syncData($action, $data);
            $transaction->commit();
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $transaction->rollBack();
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
        } finally {
            return $this->redirectToBackUrl($id);
        }
    }

}
