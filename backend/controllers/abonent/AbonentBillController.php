<?php

namespace backend\controllers\abonent;

use Yii;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;

use common\controllers\FilSMController;
use common\models\user\FSMUser;
use common\models\bill\Bill;
use common\models\client\Project;
use common\models\abonent\Abonent;
use common\models\abonent\AbonentAgreement;
use common\models\abonent\AbonentBill;
use common\models\abonent\AbonentProduct;
use common\models\abonent\AbonentProject;

/**
 * AbonentBillController implements the CRUD actions for AbonentBill model.
 */
class AbonentBillController extends FilSMController
{

    public function behaviors() {
        $behaviors = [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'view', 'delete'],
                        'allow' => false,
                    ],
                    [
                        'allow' => true,
                        'actions' => ['send-other-abonent', 'receive-other-abonent'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
        return $behaviors;
    }
    
    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\abonent\AbonentBill';
        $this->defaultSearchModel = 'common\models\abonent\search\AbonentBillSearch';
    }
    
    public function actionSendOtherAbonent($id) {
        $model = new $this->defaultModel;
        $this->rememberBackUrl($model->backURL, $id);
        
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            $billModel = Bill::findOne($id);
            $agreementModel = $billModel->agreement;
            $billProductModel = $billModel->billProducts;

            $abonentModel = AbonentAgreement::find()
                ->andWhere(['agreement_id' => $agreementModel->id])
                ->andWhere(['not', ['abonent_id' => $billModel->userAbonentId]])
                ->one();

            if(empty($abonentModel)){
                throw new Exception('Agreement for another abonent was not defined!');
            }

            $model->abonent_id = $abonentModel->abonent_id;
            $model->bill_id = $id;
            $model->project_id = $abonentModel->project_id;
            $model->primary_abonent = 0;
            if($model->save()){
                foreach ($billProductModel as $billProduct) {
                    $abonentModel = AbonentProduct::findOne(['abonent_id' => $model->abonent_id, 'product_id' => $billProduct->product_id]);
                    if(empty($abonentModel)){
                        $abonentModel = new AbonentProduct();
                        $abonentModel->abonent_id = $model->abonent_id;
                        $abonentModel->product_id = $billProduct->product_id;
                        $abonentModel->save();
                    }
                }

                $billModel->updateAttributes(['transfer_status' => Bill::BILL_TRANSFER_STATUS_SENT]);

                $transaction->commit();
                Yii::$app->getSession()->setFlash('success', Yii::t('bill', 'Invoice #{doc_number} sent to another abonent.', ['doc_number' => $billModel->doc_number]));
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $transaction->rollBack();
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
        } finally {
            return $this->redirectToBackUrl($id);              
        }
    }    

    public function actionReceiveOtherAbonent($id) {
        $model = new $this->defaultModel;
        $this->rememberBackUrl($model->backURL, $id);

        try {
            $billModel = Bill::findOne($id);
            $billModel->updateAttributes(['transfer_status' => Bill::BILL_TRANSFER_STATUS_RECEIVED]);
            Yii::$app->getSession()->setFlash('success', Yii::t('bill', 'Invoice #{doc_number} received from another abonent.', ['doc_number' => $billModel->doc_number]));
        } catch (\Exception $e) {
            $message = $e->getMessage();
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
        } finally {
            return $this->redirectToBackUrl($id);              
        }
    }
}