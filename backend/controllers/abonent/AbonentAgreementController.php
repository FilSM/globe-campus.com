<?php

namespace backend\controllers\abonent;

use Yii;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;

use common\controllers\FilSMController;
use common\models\user\FSMUser;
use common\models\client\Agreement;
use common\models\client\Project;
use common\models\abonent\Abonent;
use common\models\abonent\AbonentAgreement;
use common\models\abonent\AbonentClient;
use common\models\abonent\AbonentProject;

/**
 * AbonentBillController implements the CRUD actions for AbonentBill model.
 */
class AbonentAgreementController extends FilSMController
{

    public function behaviors() {
        $behaviors = [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'view', 'delete'],
                        'allow' => false,
                    ],
                    [
                        'allow' => true,
                        'actions' => ['send-other-abonent', 'receive-other-abonent', 'return'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
        return $behaviors;
    }
    
    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\abonent\AbonentAgreement';
        $this->defaultSearchModel = 'common\models\abonent\search\AbonentAgreementSearch';
    }
    
    public function actionSendOtherAbonent($id) {
        $model = new $this->defaultModel;
        
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxValidation($model);
        }
        
        if ($model->load($requestPost)) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                $agreementModel = Agreement::findOne($id);

                $projectModel = Project::find()->notDeleted()
                    ->andWhere(['abonent_id' => $model->abonent_id])
                    ->andWhere(['name' => 'Undefined'])
                    ->one();
                if(empty($projectModel)){
                    $projectModel = new Project();
                    $projectModel->abonent_id = $model->abonent_id;
                    $projectModel->name = 'Undefined';
                    $projectModel->save();
                }
                
                $model->project_id = $projectModel->id;
                $model->primary_abonent = 0;
                if($model->save()){
                    $abonentModel = AbonentClient::findOne(['abonent_id' => $model->abonent_id, 'client_id' => $agreementModel->first_client_id]);
                    if(empty($abonentModel)){
                        $abonentModel = new AbonentClient();
                        $abonentModel->abonent_id = $model->abonent_id;
                        $abonentModel->client_id = $agreementModel->first_client_id;
                        $abonentModel->save();
                        
                        $clientContactList = \common\models\client\ClientContact::find(true)->andWhere(['client_id' => $agreementModel->first_client_id])->all();
                        foreach ($clientContactList as $clientContact) {
                            $clientContact->id = null;
                            $clientContact->setIsNewRecord(true);                            
                            $clientContact->abonent_id = $model->abonent_id;
                            $clientContact->save();
                        }
                        
                        $clientRegDocList = \common\models\client\ClientRegDoc::find(true)->andWhere(['client_id' => $agreementModel->first_client_id])->all();
                        foreach ($clientRegDocList as $clientRegDoc) {
                            $clientRegDoc->id = null;
                            $clientRegDoc->setIsNewRecord(true);                            
                            $clientRegDoc->abonent_id = $model->abonent_id;
                            $clientRegDoc->save();
                        }
                    }
                    
                    $abonentModel = AbonentClient::findOne(['abonent_id' => $model->abonent_id, 'client_id' => $agreementModel->second_client_id]);
                    if(empty($abonentModel)){
                        $abonentModel = new AbonentClient();
                        $abonentModel->abonent_id = $model->abonent_id;
                        $abonentModel->client_id = $agreementModel->second_client_id;
                        $abonentModel->save();
                        
                        $clientContactList = \common\models\client\ClientContact::find(true)->andWhere(['client_id' => $agreementModel->second_client_id])->all();
                        foreach ($clientContactList as $clientContact) {
                            $clientContact->id = null;
                            $clientContact->setIsNewRecord(true);                            
                            $clientContact->abonent_id = $model->abonent_id;
                            $clientContact->save();
                        }
                        
                        $clientRegDocList = \common\models\client\ClientRegDoc::find(true)->andWhere(['client_id' => $agreementModel->second_client_id])->all();
                        foreach ($clientRegDocList as $clientRegDoc) {
                            $clientRegDoc->id = null;
                            $clientRegDoc->setIsNewRecord(true);                            
                            $clientRegDoc->abonent_id = $model->abonent_id;
                            $clientRegDoc->save();
                        }
                    }
                    
                    $agreementModel->transfer_status = Agreement::AGREEMENT_TRANSFER_STATUS_SENT;
                    $agreementModel->save();
                    
                    $transaction->commit();
                }
                Yii::$app->getSession()->setFlash('success', Yii::t('agreement', 'Agreement #{doc_number} sent to another abonent.', ['doc_number' => $agreementModel->number]));
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirectToBackUrl($id);              
            }
        } else {
            $this->rememberBackUrl($model->backURL, $id);
            
            $model->agreement_id = $id;
            
            $isAdmin = FSMUser::getIsPortalAdmin();
            $abonentList = ArrayHelper::map(
                Abonent::find()->notDeleted()
                    ->select(['id', 'name'])
                    ->andWhere(['not', ['abonent.id' => $model->userAbonentId]])
                    ->orderBy('name')
                    ->asArray()
                    ->all(), 
                'id', 'name');
            return $this->renderAjax('send', [
                'model' => $model,
                'abonentList' => $abonentList,
                'isAdmin' => $isAdmin,
                'isModal' => true,
            ]);
        }
    }    

    public function actionReceiveOtherAbonent($id) {
        $agreementModel = Agreement::findOne($id);
        $model = AbonentAgreement::findOne(['abonent_id' => $agreementModel->userAbonentId, 'agreement_id' => $agreementModel->id]);;
        
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxValidation($model);
        }
        
        if ($model->load($requestPost)) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                if($model->save()){
                    $agreementModel->transfer_status = Agreement::AGREEMENT_TRANSFER_STATUS_RECEIVED;
                    $agreementModel->save();
                    
                    $transaction->commit();
                    Yii::$app->getSession()->setFlash('success', Yii::t('agreement', 'Agreement #{doc_number} received from another abonent.', ['doc_number' => $agreementModel->number]));
                }
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirectToBackUrl($id);              
            }
        } else {
            $this->rememberBackUrl($model->backURL, $id);
            
            $isAdmin = FSMUser::getIsPortalAdmin();
            $projectList = ArrayHelper::map(
                Project::find(true)->notDeleted()
                    ->select(['id', 'name'])
                    ->orderBy('name')
                    ->asArray()
                    ->all(), 
                'id', 'name');
            return $this->renderAjax('receive', [
                'model' => $model,
                'projectList' => $projectList,
                'isAdmin' => $isAdmin,
                'isModal' => true,
            ]);
        }
    }    

    public function actionReturn($id) {
        $agreementModel = Agreement::findOne($id);
        $model = AbonentAgreement::findOne(['abonent_id' => $agreementModel->userAbonentId, 'agreement_id' => $agreementModel->id]);;
        
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            $agreementModel->updateAttributes(['transfer_status' => null]);
            if($model->delete()){
                $transaction->commit();
                Yii::$app->getSession()->setFlash('success', Yii::t('agreement', 'Agreement #{doc_number} returned .', ['doc_number' => $agreementModel->number]));
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $transaction->rollBack();
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
        } finally {
            return $this->goBack(Yii::$app->request->referrer);
        }
    } 
}