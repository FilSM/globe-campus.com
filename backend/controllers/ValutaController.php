<?php

namespace backend\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\web\Response;

use common\controllers\AdminListController;
use common\models\ValutaRate;

/**
 * ValutaController implements the CRUD actions for Valuta model.
 */
class ValutaController extends AdminListController
{
    
    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors = ArrayHelper::merge(
            $behaviors, [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'actions' => [
                                'ajax-get-currency-rate',
                            ],
                            'allow' => true,
                        ],                        
                    ],
                ],                
            ]
        );
        return $behaviors;
    }
    
    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\Valuta';
        $this->defaultSearchModel = 'common\models\search\ValutaSearch';
    }

    public function actionAjaxGetCurrencyRate($code, $date = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (empty($code)) {
            return [];
        }elseif (strtoupper($code) == 'EUR') {
            return '1.0000';
        }
        
        $date = isset($date) ? date('Y-m-d', strtotime($date)) : date('Y-m-d');
        $model = ValutaRate::getByDate($code, $date);
        if(!empty($model)){
            return number_format($model->rate, 4, '.', '');
        }else{
            return '0.0000';
        }
    }

}