<?php

namespace backend\controllers\mdm;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

use mdm\admin\controllers\RuleController;

use common\models\user\FSMUser;

/**
 * Controller implements the CRUD actions for model.
 */
class FSMRuleController extends RuleController
{
    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors = ArrayHelper::merge(
            $behaviors, [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'roles' => ['@'],
                            'allow' => FSMUser::getIsPortalAdmin() || Yii::$app->user->can('showBackend'),
                        ],
                    ],
                ],
            ]
        );
        return $behaviors;
    }
}
