<?php

namespace backend\controllers\address;

use Yii;
use yii\helpers\Json; 
use yii\web\Response;

use common\controllers\FilSMController;
use common\models\address\Country;

/**
 * CountryController implements the CRUD actions for Country model.
 */
class CountryController extends FilSMController {
    
    /**
     * Initializes the controller.
     */
    public function init() {
        parent::init();
        $this->defaultModel = 'common\models\address\Country';
        $this->defaultSearchModel = 'common\models\address\search\CountrySearch';
    }

    public function actionAjaxGetRegions() {
        Yii::$app->response->format = Response::FORMAT_JSON;        
        $out = [];  
        $selected = null;  
        if (isset($_POST['depdrop_parents'])) {  
            $id = end($_POST['depdrop_parents']);  
            if(empty($id) || !is_numeric($id)){  
                return ['output' => '', 'selected' => $selected];  
            }  
            $list = $this->findModel($id)->regions;            
            if (count($list) > 0) {  
                foreach ($list as $i => $item) {  
                    $out[] = ['id' => $item['id'], 'name' => $item['name']];  
                }  
            }  
            $selected = (count($list) == 1 ? strval($list[0]['id']) : '');
        }  
        // Shows how you can preselect a value  
        return ['output' => $out, 'selected' => $selected];  
    }  

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new $this->defaultModel;

        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxValidation($model);
        }

        if ($model->load($requestPost) && $model->save()) {
            if ($isPjax) {
                return $this->actionAjaxModalNameList(['selected_id' => $model->id]);
            } else {
                return $this->redirect('index');
            }            
        } else {
            $data = [
                'model' => $model,
                'isModal' => $isAjax,
            ];
            if ($isAjax) {
                return $this->renderAjax('create', $data);
            } else {
                return $this->render('create', $data);
            }
        }
    } 
}