<?php

namespace backend\controllers\address;

use common\controllers\FilSMController;

/**
 * SubregionController implements the CRUD actions for Subregion model.
 */
class SubregionController extends FilSMController
{

    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\address\Subregion';
        $this->defaultSearchModel = 'common\models\address\search\SubregionSearch';
    }

}