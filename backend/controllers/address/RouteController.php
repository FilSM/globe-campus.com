<?php

namespace admin\controllers\address;

use common\controllers\FilSMController;

/**
 * RouteController implements the CRUD actions for Route model.
 */
class RouteController extends FilSMController
{

    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\address\Route';
        $this->defaultSearchModel = 'common\models\address\search\RouteSearch';
    }

}