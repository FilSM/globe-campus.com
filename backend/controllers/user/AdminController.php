<?php

namespace backend\controllers\user;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Response;
use yii\base\Exception;

use dektrium\user\controllers\AdminController as BaseAdminController;

use common\models\mainclass\FSMBaseModel;
use common\models\user\FSMUser;
use common\models\user\FSMProfile;
use common\models\user\ProfileClient;
use common\models\user\search\FSMUserSearch;
use common\models\client\Client;
use common\assets\UIAsset;
use common\assets\ButtonMultiActionAsset;

class AdminController extends BaseAdminController
{
    /**
     * Performs ajax validation.
     * @param $model (array) Array of Model 
     * @throws \yii\base\ExitException
     */
    protected function performAjaxMultipleValidation($models) {
        $models = (array) $models;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if ($isAjax && FSMBaseModel::loadMultiple($models, $requestPost, '')) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $result = self::validateMultiple($models);
        }
    }
    
    protected static function validateMultiple($models, $attributes = null) {
        $result = [];
        /* @var $model Model */
        foreach ($models as $i => $model) {
            if(is_array($model)){
                $arrResult = self::validateMultiple($model, $attributes);
                $result = ArrayHelper::merge($result, $arrResult);
                continue;
            }            
            $model->validate($attributes);
            foreach ($model->getErrors() as $attribute => $errors) {
                if (is_numeric($i)) {
                    $result[Html::getInputId($model, "[$i]" . $attribute)] = $errors;
                } else {
                    $result[Html::getInputId($model, $attribute)] = $errors;
                }
            }
        }
        return $result;
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors = ArrayHelper::merge(
            $behaviors,
            [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@'],
                            'matchCallback' => function () {
                                $userId = Yii::$app->user->identity->getId();
                                return FSMUser::getIsSystemAdmin($userId);
                            }
                        ],
                    ]
                ],
                'afterImpersonate' => [
                    'class' => \common\behaviors\UserModelBehavior::class
                ]
            ]
        );
        return $behaviors;
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        Url::remember('', 'actions-redirect');
        UIAsset::register(Yii::$app->getView());
        ButtonMultiActionAsset::register(Yii::$app->getView());

        $searchModel = Yii::createObject(FSMUserSearch::class);
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $dataFilterRoles = Yii::$app->authManager->getRoleList([
            FSMUser::USER_ROLE_USER,
        ]);
        $itIsList = Client::getClientItIsList();
        $isAdmin = FSMUser::getIsPortalAdmin();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'dataFilterRoles' => $dataFilterRoles,
            'itIsList' => $itIsList,
            'isAdmin' => $isAdmin,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate()
    {
        /** @var User $user */
        $user = Yii::createObject([
            'class' => FSMUser::class,
            'scenario' => 'create',
        ]);
        $event = $this->getUserEvent($user);

        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            $this->performAjaxValidation($user);
        }
        $this->trigger(self::EVENT_BEFORE_CREATE, $event);
        if ($user->load($requestPost) && $user->create()) {
            if ($isPjax) {
                // JSON response is expected in case of successful save
                Yii::$app->response->format = Response::FORMAT_JSON;
                return 'reload';
            } else {
                Yii::$app->getSession()->setFlash('success', Yii::t('user', 'User has been created'));
                $this->trigger(self::EVENT_AFTER_CREATE, $event);
                return $this->redirect(['/user/profile/show', 'id' => $user->id]);
            }
        }

        if ($isAjax) {
            return $this->renderAjax('create', [
                'user' => $user,
                'isModal' => true,
            ]);
        } else {
            return $this->render('create', [
                'user' => $user
            ]);
        }
    }

    public function actionCreateFrom($className, $fromId)
    {
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;

        $fromModel = Yii::createObject([
            'class' => $className,
        ]);
        
        if(!$fromModel){
            throw new Exception("{$className} not defined.");
        }
        
        $fromModel = $fromModel::findOne($fromId);
        if(!$fromModel){
            throw new Exception("For the {$className} can`t find model with ID ".$fromId);
        }  

        $userModel = Yii::createObject([
            'class' => FSMUser::class,
            'scenario' => 'create-from',
        ]);
        
        $profileModel = new FSMProfile();
        $clientModel = new ProfileClient();
        $clientId = $fromModel->client_id;
                       
        $event = $this->getUserEvent($userModel);
        
        $modelArr = [
            'FSMUser' => $userModel,
            'FSMProfile' => $profileModel,
        ];
        
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxMultipleValidation($modelArr); 
        }
        
        $this->trigger(self::EVENT_BEFORE_CREATE, $event);
        
        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')){
            
            $transaction = Yii::$app->getDb()->beginTransaction(); 
            try {
                if (!$userModel->create()) {
                    throw new Exception('Unable to save data! ' . $userModel->errorMessage);                    
                }
                
                if(!$userClient = ProfileClient::findOne([
                    'profile_id' => $userModel->profile->id,
                    'client_id' => $fromModel->client_id,
                    'abonent_id' => Yii::$app->session->get('user_current_abonent_id'),
                ])){
                    $userClient = new ProfileClient();
                    $userClient->profile_id = $userModel->profile->id;
                    $userClient->client_id = $fromModel->client_id;
                    $userClient->abonent_id = Yii::$app->session->get('user_current_abonent_id');
                    if (!$userClient->save(false)) {
                        throw new Exception('Unable to save data! '.$userClient->errorMessage);
                    }
                }  
                
                $fromModel->updateAttributes(['user_id' => $userModel->id]);
                
                Yii::$app->getSession()->setFlash('success', Yii::t('user', 'User has been created'));
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                if ($isPjax) {
                    // JSON response is expected in case of successful save
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return 'reload';
                } else {
                    $this->trigger(self::EVENT_AFTER_CREATE, $event);
                    return $this->redirect(['/user/profile/show', 'id' => $userModel->id]);
                }
            }        
        }else{
            //$userModel->username = mb_convert_encoding(trim($fromModel->first_name).'_'.trim($fromModel->last_name), "ASCII");
            setlocale(LC_ALL, 'en_US.UTF-8');
            $userModel->username = iconv('UTF-8', 'ASCII//TRANSLIT', trim($fromModel->first_name).'_'.trim($fromModel->last_name));
            $userModel->email = $fromModel->email;
            if(strpos($className, 'ClientStaff')){
                $userModel->role = [FSMUser::USER_ROLE_REPORTING];
            }
                

            $profileModel->name = trim($fromModel->first_name).' '.trim($fromModel->last_name);
            $profileModel->phone = $fromModel->phone;

            if(FSMUser::getIsPortalAdmin()){
                $roleList = Yii::$app->authManager->getRoleList();
            }else{
                $roleList = Yii::$app->authManager->getRoleList([
                    FSMUser::USER_ROLE_SUPERUSER,
                    FSMUser::USER_ROLE_PORTALADMIN,
                ]);
            }

            $data = [
                'userModel' => $userModel,
                'profileModel' => $profileModel,
                'roleList' => $roleList,
                'isModal' => $isAjax,
            ];

            if ($isAjax) {
                return $this->renderAjax('create-from', $data);
            } else {
                return $this->render('create-from', $data);
            }
        }        
    }
    
    public function actionLinkFrom($className, $fromId)
    {
        $fromModel = Yii::createObject([
            'class' => $className,
        ]);
        
        if(!$fromModel){
            throw new Exception("{$className} not defined.");
        }
        
        $fromModel = $fromModel::findOne($fromId);
        if(!$fromModel){
            throw new Exception("For the {$className} can`t find model with ID ".$fromId);
        }  

        if(!empty($fromModel->email)){
            if($userModel = FSMUser::findOne(['email' => $fromModel->email])){

                $auth = Yii::$app->authManager;
                $role = $auth->getRole(FSMUser::USER_ROLE_REPORTING);
                if(isset($role)){
                    $auth->assign($role, $userModel->id);
                }else{
                    throw new Exception('User role '.FSMUser::USER_ROLE_REPORTING.' not exist!');
                }
                
                if(!$userClient = ProfileClient::findOne([
                    'profile_id' => $userModel->profile->id,
                    'client_id' => $fromModel->client_id,
                    'abonent_id' => Yii::$app->session->get('user_current_abonent_id'),
                ])){
                    $userClient = new ProfileClient();
                    $userClient->profile_id = $userModel->profile->id;
                    $userClient->client_id = $fromModel->client_id;
                    $userClient->abonent_id = Yii::$app->session->get('user_current_abonent_id');
                    if (!$userClient->save(false)) {
                        throw new Exception('Unable to save data! '.$userClient->errorMessage);
                    }
                }  
                $fromModel->updateAttributes(['user_id' => $userModel->id]);
                
                Yii::$app->getSession()->setFlash('success', Yii::t('user', 'User has been created'));
                return $this->redirect($fromModel->backURL);
            }else{
                throw new Exception("For the {$className} can`t find user model by email ".$fromModel->email);
            }
        }else{
            throw new Exception("{$className} doesn't have email.");
        }
    }
    
    public function actionDeleteSelected($ids)
    {
        $backUrl = \common\models\mainclass\FSMBaseModel::getBackURL('index');
        if (empty($ids)) {
            return $this->redirect($backUrl);
        }
        $idsArr = explode(',', $ids);
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            $result = true;
            //$result = FSMUser::deleteAll(['id' => $idsArr]);
            foreach ($idsArr as $id) {
                $model = $this->findModel($id);
                $event = $this->getUserEvent($model);
                $this->trigger(self::EVENT_BEFORE_DELETE, $event);
                if (!$result = $model->delete()) {
                    break;
                }
                $this->trigger(self::EVENT_AFTER_DELETE, $event);
            }
            if ($result) {
                $transaction->commit();
                Yii::$app->getSession()->setFlash('success', Yii::t('common', '{count, number} {count, plural, =1 {user} other {users}} removed!.', ['count' => count($idsArr)]));
            } else {
                $transaction->rollBack();
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $transaction->rollBack();
            Yii::$app->getSession()->setFlash('error', $message);
            Yii::error($message, __METHOD__);
        } finally {
            return $this->redirect($backUrl);
        }
    }

}
