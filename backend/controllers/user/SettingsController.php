<?php

namespace backend\controllers\user;

use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\filters\AccessControl;
//use yii\web\GroupUrlRule;
use yii\web\Response;

use dektrium\user\controllers\SettingsController as BaseSettingsController;

use common\components\FSMAccessHelper;
use common\traits\AjaxValidationTrait;
use common\models\user\FSMUser;
use common\models\user\ProfileClient;
use common\models\mainclass\FSMBaseModel;
use common\models\client\Client;

class SettingsController extends BaseSettingsController {
    
    use AjaxValidationTrait;

    /**
     * Displays page where user can update account settings (username, email or password).
     * @return string|\yii\web\Response
     */
    public function actionAccount() {
        $userId = !isset($_GET['id']) ? Yii::$app->user->identity->getId() : $_GET['id'];

        $user = $this->finder->findUserById($userId);
        $user->role = Yii::$app->authManager->getArrRolesByUser($userId);
        $arrForCreate = [
            'class' => \common\models\user\FSMSettingsForm::class,
            'user'  => $user,
        ];
        
        if(FSMUser::getIsSystemAdmin() || FSMAccessHelper::can('administrateUser')){
            $arrForCreate['scenario'] = 'admin_update';
        }
                
        /** @var SettingsForm $model */
        $model = Yii::createObject($arrForCreate); 
        
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxValidation($model);
        }        

        if ($model->load($requestPost) && $model->save()) {
            if ($isPjax) {
                // JSON response is expected in case of successful save
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }else{
                Yii::$app->session->setFlash('success', Yii::t('user', 'Your account details have been updated'));
                return $this->refresh();
            }
        }
        
        $model['role'] = $user->role;
        $profile = $this->finder->findProfileById($userId);
        if(FSMUser::getIsPortalAdmin()){
            $roleList = Yii::$app->authManager->getRoleList();
        }else{
            $roleList = Yii::$app->authManager->getRoleList([
                FSMUser::USER_ROLE_SUPERUSER,
                FSMUser::USER_ROLE_PORTALADMIN,
            ]);
        }
        
        $model->sms_auth = $profile->sms_auth;
        
        $accountDeleteUrl = Url::to(isset($_GET['id']) && ($_GET['id'] != Yii::$app->user->identity->getId()) ? ['delete', 'id' => $_GET['id']] : ['delete']);
        if ($isAjax) {
            return $this->renderAjax('create', [
                'model' => $model,
                'profile' => $profile,
                'roleList' => $roleList,
                'isModal' => true,
            ]);
        }else{
            return $this->render('account', [
                'model' => $model,
                'profile' => $profile,
                'roleList' => $roleList,
                'accountDeleteUrl' => $accountDeleteUrl,
            ]);
        }
    }
    
    public function actionNetworks() {
        if (!isset($_GET['id'])) {
            $userId = Yii::$app->user->identity->getId();
        } else {
            $userId = $_GET['id'];
        }

        $model = $this->finder->findProfileById($userId);
        $user = $model->user;
        return $this->render('networks', [
            'user' => $user,
            'profile' => $model,
        ]);
    }
    
    /**
     * Shows profile settings form.
     *
     * @return string|Yii\web\Response
     */
    public function actionProfile() {
        if (!isset($_GET['id'])) {
            $userId = Yii::$app->user->identity->getId();
        } else {
            $userId = $_GET['id'];
        }

        if(!$model = $this->finder->findProfileById($userId)){
            throw new Exception('The requested page does not exist.');
        }
        $clientModel = $model->profileClients;

        $modelArr = [
            'FSMProfile' => $model,
        ];
        
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && $isAjax) {
            return $this->performAjaxMultipleValidation($modelArr);            
        }       

        if (FSMBaseModel::loadMultiple($modelArr, $requestPost, '')){
            
            $transaction = Yii::$app->getDb()->beginTransaction(); 
            try {
                if(!$model->save()){
                    throw new Exception('Unable to save data! '.$model->errorMessage);
                }
                $oldIDs = isset($clientModel[0]) && !empty($clientModel[0]->client_id) ? ArrayHelper::map($clientModel, 'client_id', 'client_id') : [];
                $clientList = $requestPost['ProfileClient']['client_id'];
                $clientList = !empty($clientList) ? (array)$clientList : [];
                $deletedIDs = array_diff($oldIDs, $clientList);
                if (!empty($deletedIDs)) {
                    ProfileClient::deleteAll(['profile_id' => $model->id, 'client_id' => $deletedIDs]);
                }                        
                $profileClientList = [];
                foreach ($clientList as $index => $client_id) {
                    if(in_array($client_id, $oldIDs)){
                        continue;
                    }
                    $userClient = new ProfileClient();  
                    $userClient->profile_id = $model->id;
                    $userClient->client_id = $client_id;
                    $userClient->abonent_id = Yii::$app->session->get('user_current_abonent_id');
                    $profileClientList[] = $userClient;
                }

                if(!empty($profileClientList)){
                    foreach ($profileClientList as $profileClient) {
                        if (!$profileClient->save(false)) {
                            throw new Exception('Unable to save data! '.$profileClient->errorMessage);
                        }
                    }
                }

                $transaction->commit();
                Yii::$app->getSession()->setFlash('success', Yii::t('user', 'Profile updated successfully'));
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->refresh();                
            }        
        }else{
            $isAdmin = FSMUser::getIsPortalAdmin();
            $languageList = \common\models\Language::getEnabledLanguageList();
            
            $query = Client::find(true)
                ->select(['client.id', 'client.name'])
                ->orderBy('client.name')
                ->notDeleted();
                    
            if(Yii::$app->authManager->hasRole($userId, [
                    FSMUser::USER_ROLE_CONTRAGENT, 
                ])
            ){
                $query
                    ->leftJoin(['a' => 'agreement'], '(a.first_client_id = client.id) OR (a.second_client_id = client.id) OR (a.third_client_id = client.id)')
                    ->leftJoin(['bill' => 'bill'], 'bill.agreement_id = a.id')
                    ->andWhere(['not', ['bill.id' => null]]);
            }elseif(Yii::$app->authManager->hasRole($userId, [
                    FSMUser::USER_ROLE_CLIENT,
                ])
            ){
                //
            }else{
                /*
                $clientArr = \common\models\client\Client::getClientListByItIs([
                    \common\models\client\Client::CLIENT_IT_IS_OWNER,
                    \common\models\client\Client::CLIENT_IT_IS_ABONENT,
                ]);       
                 * 
                 */     
                //$clientList = \common\models\client\Client::getNameArr(['client.deleted' => 0], '', '', '', false);

                $query
                    ->leftJoin(['ac' => 'abonent_client'], 'ac.client_id = client.id AND ac.abonent_id = '. FSMBaseModel::getUserAbonentId())
                    ->andWhere(['or',
                        ['not', ['ac.client_group_id' => null]],
                        ['ac.it_is' => Client::CLIENT_IT_IS_OWNER],
                        ['ac.it_is' => Client::CLIENT_IT_IS_ABONENT],
                    ]);
            }
            $list = $query
                ->asArray()
                ->all();
            $clientList = ArrayHelper::map($list, 'id', 'name');            
            
            return $this->render('profile', [
                'model' => $model,
                'clientList' => $clientList,
                'languageList' => $languageList,
                'clientModel' => !empty($clientModel) ? $clientModel : [new ProfileClient()],
                'isAdmin' => $isAdmin,
            ]);
        }
   }

    /**
     * Shows email settings form.
     *
     * @return string|Yii\web\Response
     */
    public function actionEmail() {
        if (!isset($_GET['id'])) {
            $userId = Yii::$app->user->identity->getId();
        } else {
            $userId = $_GET['id'];
        }

        $model = $this->finder->findUserById($userId);
        $profile = $this->finder->findProfileById($userId);
        $model->scenario = 'update_email';

        if ($model->load(Yii::$app->getRequest()->post()) && $model->updateEmail()) {
            return $this->refresh();
        }

        return $this->render('email', [
                    'model' => $model,
                    'profile' => $profile,
        ]);
    }

    /**
     * Shows password settings form.
     *
     * @return string|Yii\web\Response
     */
    public function actionPassword() {
        if (!isset($_GET['id'])) {
            $userId = Yii::$app->user->identity->getId();
        } else {
            $userId = $_GET['id'];
        }

        $model = $this->finder->findUser(['id' => $userId])->one();
        $profile = $this->finder->findProfileById($userId);
        $model->scenario = 'update_password';

        if ($model->load(Yii::$app->getRequest()->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('settings_saved', Yii::t('user', 'Password has been changed'));
            return $this->refresh();
        }

        return $this->render('password', [
                    'model' => $model,
                    'profile' => $profile,
        ]);
    }

    /**
     * Completely deletes user's account.
     *
     * @return \yii\web\Response
     * @throws \Exception
     */
    public function actionDelete($id = null)
    {
        if(empty($id)){
            return parent::actionDelete();
        }
        
        if ($id == Yii::$app->user->getId()) {
            Yii::$app->getSession()->setFlash('danger', Yii::t('user', 'You can not remove your own account'));
        } else {
            $model = $this->finder->findUserById($id);
            $event = $this->getUserEvent($model);
            $this->trigger(self::EVENT_BEFORE_DELETE, $event);
            $model->delete();
            $this->trigger(self::EVENT_AFTER_DELETE, $event);
            Yii::$app->getSession()->setFlash('success', Yii::t('user', 'User has been deleted'));
        }

        return $this->redirect(['/user/admin/index']);
    }
}
