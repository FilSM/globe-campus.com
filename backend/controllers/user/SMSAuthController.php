<?php

namespace backend\controllers\user;

use Yii;
use yii\helpers\ArrayHelper;

use dektrium\user\traits\EventTrait;
use dektrium\user\controllers\SecurityController as BaseSecurityController;

use common\models\user\SMSAuthForm;

class SMSAuthController extends BaseSecurityController
{
    use EventTrait;
    
    public function behaviors() {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    // allow authenticated users
                    [
                        'roles' => ['@'],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
                            'cancel-sms-auth',
                        ],
                        'allow' => true,
                    ],                     
                ],
            ],         
        ];
    }
    
    /**
     * Displays the SMS Auth page.
     *
     * @return string|Response
     */
    public function actionLogin()
    {
        $user = Yii::$app->user->identity;
        if (!isset($user)) {
            $this->redirect(['/user/login']);
        }

        /** @var FSMLoginForm $model */
        $model = \Yii::createObject(SMSAuthForm::class);

        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxValidation($model);
        }
        
        if ($model->load($requestPost) && $model->login()) {
            Yii::$app->session->set('sms-auth-complete', true);
            return $this->goHome();
        }

        return $this->render('sms-login', [
            'model' => $model,
        ]);
    }

    public function actionCancelSmsAuth()
    {
        $user = Yii::$app->user->identity;
        if (!isset($user)) {
            $this->goHome();
        }
        Yii::$app->getSession()->remove('sms-auth-complete');
        $profile = $user->profile;
        $profile->sms_code = null; 
        $profile->sms_auth_exp = null;
        $profile->save();

        $event = $this->getUserEvent(\Yii::$app->user->identity);
        $this->trigger(SecurityController::EVENT_BEFORE_LOGOUT, $event);
        Yii::$app->user->logout();
        $this->trigger(SecurityController::EVENT_AFTER_LOGOUT, $event);
        return $this->goHome();
    }

}
