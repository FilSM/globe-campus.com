<?php

namespace backend\controllers\user;

use Yii;
use yii\helpers\ArrayHelper;

use dektrium\user\controllers\SecurityController as BaseSecurityController;

use common\models\user\FSMLoginForm;

class SecurityController extends BaseSecurityController {

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors = ArrayHelper::merge(
            $behaviors,
            [
                'FSMSecurityBehavior' => array(
                    'class' => \common\behaviors\FSMSecurityBehavior::class,
                ),
            ]
        );
        return $behaviors;        
    }

    /**
     * Displays the login page.
     *
     * @return string|Response
     */
    public function actionLogin()
    {
        if (!empty(Yii::$app->params['SMS_AUTH']) && !\Yii::$app->user->isGuest) {
            $user = Yii::$app->user->identity;
            if(isset($user) && ($profile = $user->profile)){
                if($profile->sms_auth){
                    if(!empty($profile->sms_auth_exp) && ($profile->sms_auth_exp > date('Y-m-d H:i:s'))){
                        Yii::$app->session->set('sms-auth-complete', false);
                        return $this->redirect(['/sms-auth/login']);
                    }else{
                        return $this->redirect(['/sms-auth/cancel-sms-auth']);
                    }
                }
            }
            $this->goHome();
        }

        /** @var FSMLoginForm $model */
        $model = \Yii::createObject(FSMLoginForm::class);
        $event = $this->getFormEvent($model);

        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxValidation($model);
        }

        $this->trigger(self::EVENT_BEFORE_LOGIN, $event);

        if ($model->load($requestPost) && $model->login()) {
            
            $this->trigger(self::EVENT_AFTER_LOGIN, $event);
            
            $user = Yii::$app->user->identity;
            if (!empty(Yii::$app->params['STRONG_PASSWORD_CONTROL'])) {
                if(!$user->passwordControl()){
                    Yii::$app->getSession()->setFlash('error', Yii::t('user', 'You need to change your password!'));
                    return $this->redirect(['/user/settings/account', 'id' => $user->id]);
                }
            }
            
            if(!empty(Yii::$app->params['SMS_AUTH'])){
                if($user){
                    if(($profile = $user->profile) && $profile->sms_auth){
                        if(!empty($profile->phone)){
                            $profile->sendSMSCode();
                            Yii::$app->session->set('sms-auth-complete', false);
                            return $this->redirect(['/sms-auth/login']);
                        }else{
                            Yii::$app->user->logout();
                            Yii::$app->getSession()->setFlash('error', Yii::t('fsmuser', 'User does not have a phone number'));
                        }
                    }
                }
            }
            return $this->goBack();
        }

        return $this->render('login', [
            'model'  => $model,
            'module' => $this->module,
        ]);
    }    
}
