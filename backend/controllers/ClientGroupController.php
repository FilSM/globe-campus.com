<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Json; 
use yii\web\Response;

use common\controllers\FilSMController;

/**
 * ClientGroupController implements the CRUD actions for ClientGroup model.
 */
class ClientGroupController extends FilSMController
{

    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\client\ClientGroup';
        $this->defaultSearchModel = 'common\models\client\search\ClientGroupSearch';
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new $this->defaultModel;

        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxValidation($model);
        }

        if ($model->load($requestPost) && $model->save()) {
            if ($isPjax) {
                return $this->actionAjaxModalNameList(['selected_id' => $model->id]);
            } else {
                return $this->redirect('index');
            }            
        } else {
            $model->abonent_id = $model->userAbonentId;
            $data = [
                'model' => $model,
                'isModal' => $isAjax,
            ];
            if ($isAjax) {
                return $this->renderAjax('create', $data);
            } else {
                return $this->render('create', $data);
            }
        }
    }
}