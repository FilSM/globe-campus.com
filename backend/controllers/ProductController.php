<?php
namespace backend\controllers;

use Yii;
use yii\helpers\Url;
use yii\helpers\Json; 
use yii\web\Response;

use common\models\user\FSMUser;
use common\models\abonent\Abonent;
use common\models\abonent\AbonentProduct;
use common\models\Measure;

use common\controllers\AdminListController;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends AdminListController
{

    /**
     * Initializes the controller.
     */
    public function init()
    {
        parent::init();
        $this->defaultModel = 'common\models\Product';
        $this->defaultSearchModel = 'common\models\search\ProductSearch';
    }
    
    public function actionAjaxGetMeasure($id = null)
    {
        $id = !$id ? (!empty($_POST['id']) ? $_POST['id'] : null) : $id;
        if (!$id) {
            return false;
        }
        $model = $this->findModel($id);
        $out[] = [
            'measure' => !empty($model) ? ['id' => $model->measure_id, 'name' => $model->measure->name] : [],
        ];
        echo Json::encode($out);
        return false;
    }
    
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new $this->defaultModel;

        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxValidation($model);
        }

        if ($model->load($requestPost)) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {            
                if(!$model->save()){
                    $this->refresh();
                }
                $abonentModel = new AbonentProduct();
                $abonentModel->abonent_id = $model->userAbonentId;
                $abonentModel->product_id = $model->id;
                $abonentModel->save();            
                
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                if ($isPjax) {
                    return $this->actionAjaxModalNameList(['selected_id' => $model->id]);
                } else {
                    return $this->redirect('index');
                }              
            }  
        } else {
            $isAdmin = FSMUser::getIsPortalAdmin();
            $measuraList = Measure::getNameArr();
            if ($isAjax) {
                return $this->renderAjax('create', [
                    'model' => $model,
                    'measuraList' => $measuraList,
                    'isAdmin' => $isAdmin,
                    'isModal' => true,
                ]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'measuraList' => $measuraList,
                    'isAdmin' => $isAdmin,
                    'isModal' => false,
                ]);
            }            
        }
    }
    
    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        
        $isPjax = Yii::$app->request->isPjax;
        $isAjax = Yii::$app->request->isAjax;
        $requestPost = Yii::$app->request->post();
        if (!empty($requestPost) && !$isPjax && $isAjax) {
            return $this->performAjaxValidation($model);
        }

        if ($model->load($requestPost)) {
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {            
                if(!$model->save()){
                    $this->refresh();
                }
                $abonentModel = AbonentProduct::findOne(['abonent_id' => $model->userAbonentId, 'product_id' => $model->id]);
                if(empty($abonentModel)){
                    $abonentModel->abonent_id = $model->userAbonentId;
                    $abonentModel->product_id = $model->id;
                    $abonentModel->save();
                }            
                $transaction->commit();
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', $message);
                Yii::error($message, __METHOD__);
            } finally {
                return $this->redirectToBackUrl($id);
            }             
        } else {
            $this->rememberBackUrl($model->backURL, $id);
            
            $isAdmin = FSMUser::getIsPortalAdmin();
            $measuraList = Measure::getNameArr();
            return $this->render('update', [
                'model' => $model,
                'measuraList' => $measuraList,
                'isAdmin' => $isAdmin,
            ]);
        }
    }    
}