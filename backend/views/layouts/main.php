<?php

/* @var $this \yii\web\View */
/* @var $content string */

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

use kartik\helpers\Html;
use kartik\widgets\AlertBlock;
use kartik\widgets\Select2;

use backend\assets_b\AppAsset;
//use common\widgets\languageSelection\FSMLanguageSelection;
use common\models\user\FSMUser;
use common\components\FSMNavX;
use common\components\FSMAccessHelper;
/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$this->registerLinkTag(['rel' => 'shortcut icon', 'type' => 'image/png', 'href' => Url::home().'../favicon.png?v='. time()]);

$user = Yii::$app->user->identity;
$isGuest = !isset($user) || $user->isGuest;
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
    	<meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Yii::$app->params['brandLabel'].' v.'.Yii::$app->params['brandVersion']; ?></title>
        <!--title><?= Html::encode($this->title) ?></title-->
        <?php $this->head() ?>
    </head>
    <body id="backend-body">
        <?php $this->beginBody() ?>
        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => Yii::$app->params['brandLabel'].' v.'.Yii::$app->params['brandVersion'],
                'brandUrl' => Yii::$app->urlManager->homeUrl,
                'options' => [
                    'class' => 'navbar-default navbar-fixed-top',
                    'style' => 'z-index: 1003;'
                ],
            ]);
            
            $menuLeftItems = [
                /*
                FSMLanguageSelection::widget([
                    //'language' => ['lv', 'en-us', 'ru'],
                    //'languageParam' => 'language',
                    //'container' => 'div', // li for navbar, div for sidebar or footer example
                    //'classContainer' =>  'btn btn-default dropdown-toggle' // btn btn-default dropdown-toggle
                ]),
                 * 
                 */
                ['label' => Yii::t('admin', 'Admin'),
                    'items' => [
                        ['label' => Yii::t('action', 'History'), 
                            'items' => [
                                ['label' => Yii::t('bill', 'Invoices history'), 'url' => ['/bill-history/index']],
                                ['label' => Yii::t('agreement', 'Agreement history'), 'url' => ['/agreement-history/index']],
                            ],
                        ],
                        ['label' => Yii::t('bill', 'Payments'), 
                            'items' => [
                                ['label' => Yii::t('bill', 'Check delayed invoices'), 'url' => ['/bill/check-delayed', 'isCron' => false]],
                            ],
                        ],
                        [
                            'label' => '<li class="divider"></li>', 
                            'visible' => FSMUser::getIsPortalAdmin(),
                        ],    
                        [
                            'label' => Yii::t('abonent', 'Abonents'), 
                            'url' => ['/abonent'],
                            'visible' => FSMUser::getIsPortalAdmin(),
                        ],      
                        
                    ],
                    'visible' => !Yii::$app->user->isGuest,
                ],
                
                ['label' => Yii::t('admin', 'Dictionaries'), 
                    'items' => [
                        ['label' => \common\models\Language::modelTitle(2), 
                            'items' => [
                                ['label' => Yii::t('languages', 'Communication language'), 'url' => ['/language']],
                                ['label' => Yii::t('languages', 'Translation'), 
                                    'items' => [
                                        ['label' => Yii::t('language', 'Language'), 
                                            'items' => [
                                                ['label' => Yii::t('language', 'List of languages'), 'url' => ['/translatemanager/language/list']],
                                                ['label' => Yii::t('language', 'Create'), 'url' => ['/translatemanager/language/create']],
                                            ]
                                        ],
                                        ['label' => Yii::t('language', 'Scan'), 'url' => ['/translatemanager/language/scan']],
                                        ['label' => Yii::t('language', 'Optimize'), 'url' => ['/translatemanager/language/optimizer']],
                                    ],
                                ],
                            ],
                        ],
                        ['label' => Bank::modelTitle(2), 'url' => ['/bank']],
                        ['label' => Valuta::modelTitle(2), 'url' => ['/valuta']],
                        ['label' => Measure::modelTitle(2), 'url' => ['/measure']],
                        ['label' => Product::modelTitle(2), 'url' => ['/product']],
                        ['label' => client\ClientGroup::modelTitle(2), 'url' => ['/client-group']],
                        ['label' => client\ClientRole::modelTitle(2), 'url' => ['/client-role']],
                        ['label' => client\PersonPosition::modelTitle(2), 'url' => ['/person-position']],
                        ['label' => client\RegDocType::modelTitle(2), 'url' => ['/reg-doc-type']],
                        ['label' => bill\ExpenseType::modelTitle(2), 'url' => ['/expense-type']],
                        ['label' => event\EventResurs::modelTitle(2), 'url' => ['/event-resurs']],
                        //['label' => bill\BillObject::modelTitle(2), 'url' => ['/bill-object']],
                        //['label' => bill\AccountMap::modelTitle(2), 'url' => ['/account-map']],
                        ['label' => Yii::t('common', 'Locations'), 
                            'items' => [
                                ['label' => \common\models\address\Country::modelTitle(2), 'url' => ['/country']],
                                ['label' => \common\models\address\Region::modelTitle(2), 'url' => ['/region']],
                                ['label' => \common\models\address\Subregion::modelTitle(2), 'url' => ['/subregion']],
                                ['label' => \common\models\address\City::modelTitle(2), 'url' => ['/city']],
                                ['label' => \common\models\address\District::modelTitle(2), 'url' => ['/district']],
                                '<li class="divider"></li>',
                                ['label' => \common\models\address\Address::modelTitle(2), 'url' => ['/address']],
                            ],
                        ],
                    ],
                    'visible' => !Yii::$app->user->isGuest,
                ],

                ['label' => Yii::t('admin', 'RBAC'), 
                    'items' => [
                        ['label' => Yii::t('admin', 'Assignments'), 'url' => ['/admin/assignment']],
                        ['label' => Yii::t('admin', 'Roles'), 'url' => ['/admin/role']],
                        ['label' => Yii::t('admin', 'Permissions'), 'url' => ['/admin/permission']],
                        ['label' => Yii::t('admin', 'Routes'), 'url' => ['/admin/route']],
                        //['label' => Yii::t('admin', 'Rules'), 'url' => ['/admin/rule']],
                        //['label' => Yii::t('admin', 'Menu'), 'url' => ['/admin/menu']],
                    ],
                    'visible' => FSMUser::getIsPortalAdmin(),
                ],
                
                [
                    'label' => Yii::t('user', 'Users'), 
                    'url' => ['/user/admin/index'],
                    'visible' => FSMUser::getIsPortalAdmin()|| FSMAccessHelper::can('administrateUser'),
                ],
            ];
            
            $menuRightItems = [
                ['label' => '',
                    'url' => ['/user/login'],
                    'linkOptions' => [
                        'title' => Yii::t('common', 'Login'),
                        'class' => 'glyphicon glyphicon-off'
                    ],
                    'visible' => Yii::$app->user->isGuest,
                ],                
            ];
            if(!Yii::$app->user->isGuest){
                $countUserAbonent = Yii::$app->session->get('user_abonent_id');
                if(isset($countUserAbonent) && (count($countUserAbonent) > 1)){
                    $dataAbonent = array_combine(Yii::$app->session->get('user_abonent_id'), Yii::$app->session->get('user_abonent_name'));
                    asort($dataAbonent);
                    $currentAbonentId = (int)Yii::$app->session->get('user_current_abonent_id');
                    $selectAbonent = Select2::widget([
                        'name' => 'abonent_id',
                        'data' => $dataAbonent,
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'value' => $currentAbonentId,
                        'options' => [
                            'placeholder' => 'Select a abonent...',
                        ],
                        'pluginEvents' => [
                            "change" => "function() {
                                var form = $('#switch-abonent-form');
                                form.submit();
                            }",
                        ]
                    ]);
                    $swithAbonentForm = 
                        Html::beginForm(['/abonent/switch-abonent'], 'post', ['class' => 'navbar-form', 'id' => 'switch-abonent-form', 'style' => 'min-width: 200px;'])
                        . $selectAbonent
                        . Html::endForm();
                    $menuRightItems = ArrayHelper::merge($menuRightItems, [
                        'label' => '<li>'.$swithAbonentForm.'</li>',
                        //'visible' => count(Yii::$app->session->get('user_abonent_id')) > 1,
                    ]);
                }
                
                $user = Yii::$app->user->identity;
                $gravatar_id = $user->profile->gravatar_id;
                if(!$gravatar_id){
                    $linkOptions = [
                        'title' => Yii::t('user', 'Profile') . ' (' . Yii::$app->user->identity->username . ')', 
                        'class' => 'glyphicon glyphicon-user',
                        'style' => 'font-size: 1.7em;'
                    ];
                }else{
                    $linkOptions = [
                        //'title' => Yii::t('user', 'Profile') . ' (' . Yii::$app->user->identity->username . ')', 
                        'class' => 'gravatar',
                        //'style' => 'font-size: 1.5em;'
                    ];                    
                }
                $menuRightItems = ArrayHelper::merge($menuRightItems, [
                    ['label' => $gravatar_id ? "<img src='http://gravatar.com/avatar/{$gravatar_id}>?s=28' alt='{$user->username}'/> {$user->profile->name}" : '',
                        'items' => [
                            [
                                'label' => 'Profile', 
                                //'label' => Yii::t('user', 'Profile'), 
                                'url' => ['/user/profile/index'],
                            ],
                            [
                                'label' => Yii::t('fsmuser', 'My company'), 
                                'url' => ['/client/view', 'id' => (isset($user, $user->firstClient) ? $user->firstClient->id : null)],
                                'visible' => (isset($user, $user->firstClient) ? true : false)
                            ],
                            '<li class="divider"></li>',
                            [
                                'label' => Html::icon('cog').'&nbsp;'.Yii::t('common', 'Application'),
                                'url' => \yii\helpers\Url::to('@web').'/..',
                            ],
                            [
                                'label' => Html::icon('cog').'&nbsp;'.Yii::t('admin', 'Run Daily procedure'),
                                'url' => ['/daemon/daily'],
                                'linkOptions' => ['target' => '_blank', 'data-pjax' => 0],
                                'visible' => FSMUser::getIsPortalAdmin(),
                            ],
                            [
                                'label' => Html::icon('cog').'&nbsp;'.Yii::t('admin', 'Run Frequent procedure'),
                                'url' => ['/daemon/frequent'],
                                'linkOptions' => ['target' => '_blank', 'data-pjax' => 0],
                                'visible' => FSMUser::getIsPortalAdmin(),
                            ],  
                            [
                                'label' => Html::icon('log-out').'&nbsp;'.Yii::t('common', 'Log Out'),
                                'url' => ['/user/security/logout'],
                                'linkOptions' => [
                                    'data-method' => 'post'
                                ],
                            ],                            
                        ],
                        'linkOptions' => $linkOptions,
                    ]
                ]);
            }            
            
            echo FSMNavX::widget([
                'options' => ['class' => 'navbar-nav navbar-left'],
                'items' => $menuLeftItems,
                'activateParents' => true,
                'encodeLabels' => false
            ]);
            
            echo FSMNavX::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuRightItems,
                'activateParents' => true,
                'encodeLabels' => false
            ]);
                        
            NavBar::end();
            ?>

            <div class="container container-type-<?= strtolower($this->context->id)?>">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
                ?>
                <?= AlertBlock::widget([
                    'delay' => 0,
                    //'delay' => 5000,
                    'useSessionFlash' => true
                ]);
                ?>            
                <?= $content ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; <?= Yii::$app->params['brandLabel'].' v.'.Yii::$app->params['brandVersion'].' '.date('Y') ?></p>
                <p class="pull-right"><?= Yii::powered() ?></p>
            </div>
        </footer>

        <?= $this->render('@frontend/views/layouts/modalWindow'); ?>
        
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
