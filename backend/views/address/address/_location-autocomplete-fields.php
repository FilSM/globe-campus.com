<?php

use yii\helpers\Url;

use kartik\widgets\Typeahead;
use kartik\helpers\Html;

use common\models\address\Address;

$prefix = (isset($prefix) ? $prefix : 'address');
$prefixName = (isset($prefixName) ? $prefixName : 'Address');
$formGroup = (isset($formGroup) ? $formGroup : true);
$fieldList = (isset($fieldList) ? $fieldList : null);
?>

<?php if(in_array(Address::ADDRESS_FIELD_POSTAL_CODE, $fieldList)):
    $fieldName = Address::ADDRESS_FIELD_POSTAL_CODE;
    echo $form->field($addressModel, $fieldName, [
        'options' => ['id' => $prefix.'-'.$fieldName.'-input', 'class' => (!empty($formGroup) ? ' form-group' : '')],
        //'addon' => ['append' => ['content' => Html::icon('map-marker'),]]
        ])->widget(Typeahead::class, [
        'options' => [
            'id' =>$prefix.'-'.$fieldName,
            'name' => $prefixName.'['.$fieldName.']',
            //'placeholder' => Yii::t('common', 'Start typing to get a list of possible matches.'),
        ],
        'scrollable' => true,
        'pluginOptions' => [
            'highlight' => true,
            'minLength' => 3,
        ],        
        'dataset' => [
            [
                'displayKey' => $fieldName,
                'remote' => [
                    'url' => Url::to(['address/ajax-postal-code-list']).'?q=%QUERY&limit=10',
                    'wildcard' => '%QUERY',
                ],
                'limit' => 10,
            ],
        ],
        'pluginEvents' => [
            "typeahead:selected" => "function(_event, _object, _dataset) {
                var form = $('#{$form->id}');
                    
                var districtVal = form.find('input#{$prefix}-district').val();
                if(empty(districtVal)){
                    form.find('input#{$prefix}-district').typeahead('val',
                        _object.district ? _object.district : 
                            (_object.political ? _object.political :
                                (_object.sublocality_level_1 ? _object.sublocality_level_1 :
                                    (_object.sublocality ? _object.sublocality : '')
                                )
                            )
                        );
                }

                var countryVal = form.find('input#{$prefix}-country').val();
                if(empty(countryVal)){
                    var initUrl = {$prefix}_administrative_area_level_1_data_1.remote.initUrl;
                    if(!initUrl){
                        initUrl = {$prefix}_administrative_area_level_1_data_1.remote.url;
                        {$prefix}_administrative_area_level_1_data_1.remote.initUrl = initUrl;
                    }
                    if(_object.country_id){
                        {$prefix}_administrative_area_level_1_data_1.remote.url = initUrl + '+&country_id=' + _object.country_id;
                    }
                    form.find('input#{$prefix}-country').typeahead('val', _object.country);
                }

                var regionVal = form.find('input#{$prefix}-administrative_area_level_1').val();
                if(empty(regionVal)){
                    initUrl = {$prefix}_locality_data_1.remote.initUrl;
                    if(!initUrl){
                        initUrl = {$prefix}_locality_data_1.remote.url;
                        {$prefix}_locality_data_1.remote.initUrl = initUrl;
                    }
                    if(_object.region_id){
                        {$prefix}_locality_data_1.remote.url = initUrl + '+&region_id=' + _object.region_id;
                    }
                    form.find('input#{$prefix}-administrative_area_level_1').typeahead('val', _object.administrative_area_level_1);
                }

                var subregionVal = form.find('input#{$prefix}-administrative_area_level_2').val();
                if(empty(subregionVal)){
                    form.find('input#{$prefix}-administrative_area_level_2').typeahead('val', _object.administrative_area_level_2);
                }
                
                var cityVal = form.find('input#{$prefix}-locality').val();
                if(empty(cityVal)){
                    initUrl = {$prefix}_district_data_1.remote.initUrl;
                    if(!initUrl){
                        initUrl = {$prefix}_district_data_1.remote.url;
                        {$prefix}_district_data_1.remote.initUrl = initUrl;
                    }
                    if(_object.city_id){
                        {$prefix}_district_data_1.remote.url = initUrl + '+&city_id=' + _object.city_id;
                    }
                    form.find('input#{$prefix}-locality').typeahead('val', _object.locality);
                }

                var routeVal = form.find('input#{$prefix}-route').val();
                if(empty(routeVal)){
                    initUrl = {$prefix}_route_data_1.remote.initUrl;
                    if(!initUrl){
                        initUrl = {$prefix}_route_data_1.remote.url;
                        {$prefix}_route_data_1.remote.initUrl = initUrl;
                    }
                    if(_object.district_id){
                        {$prefix}_route_data_1.remote.url = initUrl + '+&district_id=' + _object.district_id;
                    }
                }
                
                checkRequiredInputs(form);

                //alert('selected id = ' +_object.id);
            }",
        ],
    ]);
endif;?>

<?php if(in_array(Address::ADDRESS_FIELD_COUNTRY, $fieldList)):
    $fieldName = Address::ADDRESS_FIELD_COUNTRY;
    echo $form->field($addressModel, $fieldName, [
        'options' => ['id' => $prefix.'-'.$fieldName.'-input', 'class' => (!empty($formGroup) ? ' form-group' : '')],
        //'addon' => ['append' => ['content' => Html::icon('map-marker'),]]
    ])->widget(Typeahead::class, [
        'options' => [
            'id' =>$prefix.'-'.$fieldName,
            'name' => $prefixName.'['.$fieldName.']',
            //'placeholder' => Yii::t('common', 'Start typing to get a list of possible matches.'),
        ],
        'scrollable' => true,
        'pluginOptions' => [
            'highlight' => true,
            'minLength' => 3,
        ],        
        'dataset' => [
            [
                'displayKey' => 'name',
                'remote' => [
                    'url' => Url::to(['country/ajax-name-list']).'?q=%QUERY',
                    'wildcard' => '%QUERY',
                ],
                'limit' => 10,
            ],
        ],
        'pluginEvents' => [
            "typeahead:active" => "function() {
                var initUrl = {$prefix}_administrative_area_level_1_data_1.remote.initUrl;
                if(!initUrl){
                    initUrl = {$prefix}_administrative_area_level_1_data_1.remote.url;
                    {$prefix}_administrative_area_level_1_data_1.remote.initUrl = initUrl;
                }
            }",
            "typeahead:selected" => "function(_event, _object, _dataset) {
                var initUrl = {$prefix}_administrative_area_level_1_data_1.remote.initUrl;
                {$prefix}_administrative_area_level_1_data_1.remote.url = initUrl + '+&country_id=' + _object.id;

                //alert('selected id = ' +_object.id);
            }",
        ],
    ]);
endif;?>

<?php if(in_array(Address::ADDRESS_FIELD_REGION, $fieldList)):
    $fieldName = Address::ADDRESS_FIELD_REGION;
    echo $form->field($addressModel, $fieldName, [
        'options' => ['id' => $prefix.'-region-input', 'class' => ''.(!empty($formGroup) ? ' form-group' : '')],
        //'addon' => ['append' => ['content' => Html::icon('map-marker'),]]
    ])->widget(Typeahead::class, [
    'options' => [
        'id' =>$prefix.'-'.$fieldName,
        'name' => $prefixName.'['.$fieldName.']',
        //'placeholder' => Yii::t('common', 'Start typing to get a list of possible matches.'),
    ],
    'scrollable' => true,
    'pluginOptions' => [
        'highlight' => true,
        'minLength' => 3,
    ],        
    'dataset' => [
        [
            'displayKey' => 'name',
            'remote' => [
                'url' => Url::to(['region/ajax-name-list']).'?q=%QUERY',
                'wildcard' => '%QUERY',
            ],
            'limit' => 10,
        ],
    ],
    'pluginEvents' => [
        "typeahead:active" => "function() {
            var initUrl = {$prefix}_administrative_area_level_2_data_1.remote.initUrl;
            if(!initUrl){
                initUrl = {$prefix}_administrative_area_level_2_data_1.remote.url;
                {$prefix}_administrative_area_level_2_data_1.remote.initUrl = initUrl;
            }            

            initUrl = {$prefix}_locality_data_1.remote.initUrl;
            if(!initUrl){
                initUrl = {$prefix}_locality_data_1.remote.url;
                {$prefix}_locality_data_1.remote.initUrl = initUrl;
            }
        }",
        "typeahead:selected" => "function(_event, _object, _dataset) {
            var initUrl = {$prefix}_administrative_area_level_2_data_1.remote.initUrl;
            {$prefix}_administrative_area_level_2_data_1.remote.url = initUrl + '+&region_id=' + _object.id;
                
            initUrl = {$prefix}_locality_data_1.remote.initUrl;
            {$prefix}_locality_data_1.remote.url = initUrl + '+&region_id=' + _object.id;

            //alert('selected id = ' +_object.id);
        }",
    ],
]);
endif;?>

<?php if(in_array(Address::ADDRESS_FIELD_SUBREGION, $fieldList)):
    $fieldName = Address::ADDRESS_FIELD_SUBREGION;
    echo $form->field($addressModel, $fieldName, [
        'options' => ['id' => $prefix.'-subregion-input', 'class' => ''.(!empty($formGroup) ? ' form-group' : '')],
        //'addon' => ['append' => ['content' => Html::icon('map-marker'),]]
    ])->widget(Typeahead::class, [
    'options' => [
        'id' =>$prefix.'-'.$fieldName,
        'name' => $prefixName.'['.$fieldName.']',
        //'placeholder' => Yii::t('common', 'Start typing to get a list of possible matches.'),
    ],
    'scrollable' => true,
    'pluginOptions' => [
        'highlight' => true,
        'minLength' => 3,
    ],        
    'dataset' => [
        [
            'displayKey' => 'name',
            'remote' => [
                'url' => Url::to(['subregion/ajax-name-list']).'?q=%QUERY',
                'wildcard' => '%QUERY',
            ],
            'limit' => 10,
        ],
    ],
        /*
    'pluginEvents' => [
        "typeahead:active" => "function() {
            var initUrl = {$prefix}_locality_data_1.remote.initUrl;
            if(!initUrl){
                initUrl = {$prefix}_locality_data_1.remote.url;
                {$prefix}_locality_data_1.remote.initUrl = initUrl;
            }
        }",
        "typeahead:selected" => "function(_event, _object, _dataset) {
            var initUrl = {$prefix}_locality_data_1.remote.initUrl;
            {$prefix}_locality_data_1.remote.url = initUrl + '+&subregion_id=' + _object.id;

            //alert('selected id = ' +_object.id);
        }",
    ],
         * 
         */
]);
endif;?>

<?php if(in_array(Address::ADDRESS_FIELD_CITY, $fieldList)):
    $fieldName = Address::ADDRESS_FIELD_CITY;
    echo $form->field($addressModel, $fieldName, [
        'options' => ['id' => $prefix.'-city-input', 'class' => (!empty($formGroup) ? ' form-group' : '')],
        //'addon' => ['append' => ['content' => Html::icon('map-marker'),]]
    ])->widget(Typeahead::class, [
    'options' => [
        'id' =>$prefix.'-'.$fieldName,
        'name' => $prefixName.'['.$fieldName.']',
        //'placeholder' => Yii::t('common', 'Start typing to get a list of possible matches.'),
    ],
    'scrollable' => true,
    'pluginOptions' => [
        'highlight' => true,
        'minLength' => 3,
    ],        
    'dataset' => [
        [
            'displayKey' => 'name',
            'remote' => [
                'url' => Url::to(['city/ajax-name-list']).'?q=%QUERY',
                'wildcard' => '%QUERY',
            ],
            'limit' => 10,
        ],
    ],
    'pluginEvents' => [
        "typeahead:active" => "function() {
            var initUrl = {$prefix}_district_data_1.remote.initUrl;
            if(!initUrl){
                initUrl = {$prefix}_district_data_1.remote.url;
                {$prefix}_district_data_1.remote.initUrl = initUrl;
            }
        }",
        "typeahead:selected" => "function(_event, _object, _dataset) {
            var initUrl = {$prefix}_district_data_1.remote.initUrl;
            {$prefix}_district_data_1.remote.url = initUrl + '+&city_id=' + _object.id;

            //alert('selected id = ' +_object.id);
        }",
    ],
]);
endif;?>

<?php if(in_array(Address::ADDRESS_FIELD_DISTRICT, $fieldList)):
    $fieldName = Address::ADDRESS_FIELD_DISTRICT;
    echo $form->field($addressModel, $fieldName, [
        'options' => ['id' => $prefix.'-'.$fieldName.'-input', 'class' => ''.(!empty($formGroup) ? ' form-group' : '')],
        //'addon' => ['append' => ['content' => Html::icon('map-marker'),]]
    ])->widget(Typeahead::class, [
    'options' => [
        'id' =>$prefix.'-'.$fieldName,
        'name' => $prefixName.'['.$fieldName.']',
        //'placeholder' => Yii::t('common', 'Start typing to get a list of possible matches.'),
    ],
    'scrollable' => true,
    'pluginOptions' => [
        'highlight' => true,
        'minLength' => 3,
    ],        
    'dataset' => [
        [
            'displayKey' => 'name',
            'remote' => [
                'url' => Url::to(['district/ajax-name-list']).'?q=%QUERY',
                'wildcard' => '%QUERY',
            ],
            'limit' => 10,
        ],
    ],
    'pluginEvents' => [
        "typeahead:active" => "function() {
            var initUrl = {$prefix}_route_data_1.remote.initUrl;
            if(!initUrl){
                initUrl = {$prefix}_route_data_1.remote.url;
                {$prefix}_route_data_1.remote.initUrl = initUrl;
            }
        }",
        "typeahead:selected" => "function(_event, _object, _dataset) {
            var initUrl = {$prefix}_route_data_1.remote.initUrl;
            {$prefix}_route_data_1.remote.url = initUrl + '+&district_id=' + _object.id;

            //alert('selected id = ' +_object.id);
        }",
    ],
]);
endif;?>

<?php if(in_array(Address::ADDRESS_FIELD_STREET, $fieldList)):
    $fieldName = Address::ADDRESS_FIELD_STREET;
    echo $form->field($addressModel, $fieldName, [
        'options' => ['id' => $prefix.'-'.$fieldName.'-input', 'class' => (!empty($formGroup) ? ' form-group' : '')],
        //'addon' => ['append' => ['content' => Html::icon('map-marker'),]]
    ])->widget(Typeahead::class, [
    'options' => [
        'id' =>$prefix.'-'.$fieldName,
        'name' => $prefixName.'['.$fieldName.']',
        //'placeholder' => Yii::t('common', 'Start typing to get a list of possible matches.'),
    ],
    'scrollable' => true,
    'pluginOptions' => [
        'highlight' => true,
        'minLength' => 3,
    ],        
    'dataset' => [
        [
            'displayKey' => 'name',
            'remote' => [
                'url' => Url::to(['route/ajax-name-list']).'?q=%QUERY',
                'wildcard' => '%QUERY',
            ],
            'limit' => 10,
        ],
    ],
]);
endif;?>

<?php if(in_array(Address::ADDRESS_FIELD_STREET_NUMBER, $fieldList)):
    $fieldName = Address::ADDRESS_FIELD_STREET_NUMBER;
    echo $form->field($addressModel, $fieldName, [
    'options' => ['id' => $prefix.'-'.$fieldName, 'class' => (!empty($formGroup) ? ' form-group' : '')], 
    //'addon' => ['append' => ['content' => Html::icon('map-marker'),]]
    ])->textInput(['name' => $prefixName.'['.$fieldName.']']);
endif;?>

<?php if(in_array(Address::ADDRESS_FIELD_FLAT_NUMBER, $fieldList)):
    $fieldName = Address::ADDRESS_FIELD_FLAT_NUMBER;
    echo $form->field($addressModel, $fieldName, [
    'options' => [
        'id' => $prefix.'-'.$fieldName, 
        'class' => (!empty($formGroup) ? ' form-group' : ''),
        //'style' => 'display: none;',
    ], 
    //'addon' => ['append' => ['content' => Html::icon('map-marker'),]]
    ])->textInput(['name' => $prefixName.'['.$fieldName.']']);
endif;?>
