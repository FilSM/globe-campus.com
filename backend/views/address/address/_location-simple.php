<?php

?>

<?= $this->render('@admin/views/address/address/_location-gMap', [
    'form' => $form,
    'addressModel' => $addressModel,
]) ?>

<?= $this->render('@admin/views/address/address/_location-fields', [
    'model' => $model,
    'addressModel' => $addressModel,
    'form' => $form,
]) ?>