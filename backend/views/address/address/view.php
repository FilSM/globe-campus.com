<?php

use kartik\helpers\Html;
use kartik\detail\DetailView;

use common\models\user\FSMUser;

/* @var $this yii\web\View */
/* @var $model common\models\address\Address */

$this->title = $model->customer_address;;
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

if(!empty($model->latitude) && !empty($model->longitude)){
    $coord = new \dosamigos\google\maps\LatLng(['lat' => $model->latitude, 'lng' => $model->longitude]);
    $map = new \dosamigos\google\maps\Map([
        'center' => $coord,
        'zoom' => 14,
        'width' => '100%',
        'height' => 300,
    ]);

    $shortAddress = !empty($model->route) ? $model->route.(!empty($model->street_number) ? ' '.$model->street_number : '') : '';
    $marker = new \dosamigos\google\maps\overlays\Marker([
        'position' => $coord,
        'title' => $shortAddress,
        'animation' => 'google.maps.Animation.DROP',
    ]);

    $infoWindowContent = 
        "<div id='gm-info-window'>".
            "<strong>{$shortAddress}</strong>".
            //"<br>" + address.formated_address +
            "<hr style='margin-top: 5px; margin-bottom: 5px;'>".
            "Latitude: {$model->latitude}<br/>Longitude: {$model->longitude}".
        "</div>";
    $infoWindow = new \dosamigos\google\maps\overlays\InfoWindow([
        'content' => $infoWindowContent, 
    ]);
    $marker->attachInfoWindow($infoWindow);

    // Add marker to the map
    $map->addOverlay($marker);
    //echo $map->display();
}
?>
<div class="address-view">

    <h1><?= Html::encode($model->customer_address) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= \common\components\FSMBtnDialog::button(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
        ]); ?>        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'address_type',
            'customer_address',
            'postal_code',
            [
                'attribute' => 'country_id',
                'value' => !empty($model->country_id) ? Html::a(Html::encode($model->country0->name), ['country/view', 'id' => $model->country_id]) : null,
                'format' => 'raw',
            ],
            [
                'attribute' => 'region_id',
                'value' => !empty($model->region_id) ? Html::a(Html::encode($model->region->name), ['region/view', 'id' => $model->region_id]) : null,
                'format' => 'raw',
            ],
            [
                'attribute' => 'city_id',
                'value' => !empty($model->city_id) ? Html::a(Html::encode($model->city->name), ['city/view', 'id' => $model->city_id]) : null,
                'format' => 'raw',
            ],
            [
                'attribute' => 'district_id',
                'value' => !empty($model->district_id) ? Html::a(Html::encode($model->district->name), ['/district/view', 'id' => $model->district_id]) : null,
                'format' => 'raw',
            ],
            'street_number',
            'route',
            'district',
            /*
            'political',
            'sublocality_level_1',
            'sublocality',
            'locality',
            'administrative_area_level_1',
            'country',
             * 
             */
            'latitude',
            'longitude',
            'formated_address:ntext',
            
            [
                'label' => Yii::t('estate', 'Map'),
                'value' => $map->display(),
                'format' => 'raw',
                'visible' => isset($map),
            ],
            
            'company_name',
            'contact_person',
            'contact_phone',
            'contact_email:email',
            
            [
                'attribute' => 'deleted',
                'format' => 'boolean',
                'visible' => FSMUser::getIsPortalAdmin(),
            ],
            /*
            'create_time',
            'create_user_id',
            'update_time',
            'update_user_id',
             * 
             */
        ],
    ]) ?>

</div>