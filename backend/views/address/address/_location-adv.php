<?php
$fieldList = (isset($fieldList) ? $fieldList : null);
?>

<?= $this->render('@admin/views/address/address/_location-gMap', [
    'form' => $form,
    'addressModel' => $addressModel,
]) ?>

<?= $this->render('@admin/views/address/address/_location-autocomplete-fields', [
    'form' => $form,
    'addressModel' => $addressModel,
    'fieldList' => $fieldList,
]) ?>