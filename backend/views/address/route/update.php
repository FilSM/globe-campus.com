<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\address\Route */

$this->title = Yii::t($model->tableName(), 'Update a '.$model->modelTitle(1, false)) . ': #' . $model->name;
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Edit');

$isModal = !empty($isModal);
?>
<div class="route-update">

    <?php if(!$isModal): ?> 
    <?= Html::pageHeader(Html::encode($this->title)); ?>
    <?php endif; ?>    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
