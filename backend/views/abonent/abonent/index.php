<?php
namespace common\models;

use Yii;
//use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\grid\GridView;

use kartik\helpers\Html;
use kartik\grid\GridView;
use kartik\daterange\DateRangePicker;

use common\components\FSMAccessHelper;
use common\components\FSMHelper;
use common\models\abonent\Abonent;

/* @var $this yii\web\View */
/* @var $searchModel common\models\abonent\AbonentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $searchModel->modelTitle(2);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="abonent-index">
    <?php /*= Html::pageHeader(Html::encode($this->title)); */?>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Html::icon('plus').'&nbsp;'.$searchModel->modelTitle(), ['create'], ['class' => 'btn btn-success']); ?>
        <?php /* if (FSMAccessHelper::can('canAPI')): ?>
        <?= FSMHelper::aButton(null, [
            'label' => Yii::t('bill', 'Sync list'),
            'title' => Yii::t('bill', 'Sync list'),
            'controller' => 'abonent',
            'action' => 'sync-api',
            'data' => [
                'action' => Abonent::ABONENT_SYNC_GET_LIST,
            ],
            'icon' => 'cloud-download',
            'class' => 'info',
        ]);?>
        <?php endif;*/ ?>
    </p>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsive' => false,
        //'striped' => false,
        'hover' => true,      
        'floatHeader' => true,
        'pjax' => true,
        'columns' => [
        //['class' => '\kartik\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'width' => '75px',
                'hAlign' => 'center',
            ],
            [
                'attribute'=>'name',
                //'headerOptions' => ['class'=>'td-mw-200'],
                'value' => function ($model) {
                    return Html::a($model->name, ['abonent/view', 'id' => $model->id], ['target' => '_blank', 'data-pjax' => 0]);
                },                         
                'format'=>'raw',
            ],                            
            [
                'attribute'=>'client_name',
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) use($mainClientId) {
                    //return !empty($model->main_client_id) ? $model->client_name : null;
                    return (!empty($model->main_client_id) ? 
                        (FSMAccessHelper::can('viewClient', $model) && ($model->main_client_id == $mainClientId) ?
                            Html::a($model->client_name, ['/client/view', 'id' => $model->main_client_id], ['target' => '_blank', 'data-pjax' => 0]) :
                            $model->client_name
                        )
                        : null);
                },                         
                'format'=>'raw',
            ],                          
            [
                'attribute' => 'subscription_end_date',
                'headerOptions' => [
                    'class' => 'td-mw-75',
                ],
                'value' => function ($model) {
                    return !empty($model->subscription_end_date) ? date('d-M-Y', strtotime($model->subscription_end_date)) : null;
                },                 
            ],                             
            [
                'attribute' => 'subscription_type',
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) {
                    return isset($model->subscription_type) ? $model->abonentTypeList[$model->subscription_type] : null;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $searchModel->abonentTypeList,
                'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                'filterInputOptions' => ['placeholder' => '...'],
            ],                            
            [
                'attribute'=>'manager_name',
                'headerOptions' => ['class'=>'td-mw-150'],
                'value' => function ($model) {
                    return !empty($model->manager_id) ? Html::a($model->manager_name, ['/user/profile/show', 'id' => (isset($model->manager_user_id) ? $model->manager_user_id : null)], ['target' => '_blank']) : null;
                },                         
                'format'=>'raw',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $managerList,
                'filterWidgetOptions' => ['pluginOptions' => ['allowClear' => true],],
                'filterInputOptions' => ['placeholder' => '...'],
            ], 
            [
                'attribute' => "deleted",                
                'class' => '\kartik\grid\BooleanColumn',
                'trueLabel' => 'Yes', 
                'falseLabel' => 'No',
                'width' => '100px',
                'visible' => $isAdmin,
            ],
            [
                'class' => '\common\components\FSMActionColumn',
                'headerOptions' => ['class' => 'td-mw-150'],
                'dropdown' => true,
                'template' => '{view} {update} {delete}',
            ],
        ],
    ]); ?>

</div>