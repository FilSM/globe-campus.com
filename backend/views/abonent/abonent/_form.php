<?php
namespace common\models;

use Yii;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\DatePicker;

use common\widgets\EnumInput;

/* @var $this yii\web\View */
/* @var $model common\models\abonent\Abonent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="abonent-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'id' => 'client-form',
        'formConfig' => [
            'labelSpan' => 3,
        ],
        'fieldConfig' => [
            'showHints' => true,
        ],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]); ?>

    <?= $form->field($model, 'subscription_end_date')->widget(DatePicker::class, [
            'pluginOptions' => Yii::$app->params['DatePickerPluginOptions'],
        ]); 
    ?>

    <?= $form->field($model, 'subscription_type')->widget(EnumInput::class, [
            'type' => EnumInput::TYPE_RADIOBUTTON,
            'options' => [
                'translate' => $model->abonentTypeList,
            ],
        ]); 
    ?>  

    <?= $form->field($model, 'simple_workflow')->widget(EnumInput::class, [
            'data' => [
                1 => Yii::t('common', 'Yes'),
                0 => Yii::t('common', 'No'),
            ],
            'type' => EnumInput::TYPE_RADIOBUTTON,
        ]); 
    ?>     

    <?= $form->field($model, 'send_all_attachment')->widget(EnumInput::class, [
            'data' => [
                1 => Yii::t('common', 'Yes'),
                0 => Yii::t('common', 'No'),
            ],
            'type' => EnumInput::TYPE_RADIOBUTTON,
        ]); 
    ?> 
    
    <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>
    
    <?= $this->render('@frontend/views/client/client/_form', [
        'form' => $form,
        'model' => $clientModel,
        'abonentModel' => $abonentModel,
        'clientBankModel' => $clientBankModel,
        'filesModel' => $filesModel,
        'mainContactModel' => $mainContactModel,
        'clientGroupList' => $clientGroupList,
        'countryList' => $countryList,
        'languageList' => $languageList,
        'bankList' => $bankList,
        'valutaList' => $valutaList,
        'managerList' => $managerList,
        'isAdmin' => $isAdmin,
        'isModal' => false,
        'registerAction' => false,
        'fromAbonent' => true,
    ]) ?>
    
    <div class="form-group clearfix double-line-top">
        <div class="col-md-offset-8 col-md-4" style="text-align: right;">
            <?= $model->SubmitButton; ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>
