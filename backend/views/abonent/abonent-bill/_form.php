<?php
namespace common\models;

use Yii;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\abonent\AbonentBill */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="abonent-bill-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => $model->tableName().'-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>

    <?= Html::activeHiddenInput($model, 'bill_id', ['id' => 'bill-id', 'value' => $model->bill_id]); ?>    
    <?= Html::activeHiddenInput($model, 'project_id', ['id' => 'project-id', 'value' => $model->project_id]); ?>    
    
    <?= $form->field($model, 'abonent_id')->widget(Select2::class, [
        'data' => $abonentList,
        'options' => [
            'id' => 'abonent-id',
            'placeholder' => '...',
        ],
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]);?>
    
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10" style="text-align: right;">
            <?= $model->getSubmitButton('send'); ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
