<?php
namespace common\models;

use Yii;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\abonent\AbonentAgreement */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="abonent-agreement-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => $model->tableName().'-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>

    <?= Html::activeHiddenInput($model, 'abonent_id', ['id' => 'abonent-id', 'value' => $model->abonent_id]); ?>    
    <?= Html::activeHiddenInput($model, 'agreement_id', ['id' => 'agreement-id', 'value' => $model->agreement_id]); ?>    
    
    <?= $form->field($model, 'project_id')->widget(Select2::class, [
        'data' => $projectList,
        'options' => [
            'id' => 'project-id',
            'placeholder' => '...',
        ],
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]);?>
    
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10" style="text-align: right;">
            <?= $model->getSubmitButton('send'); ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
