<?php

use kartik\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\abonent\AbonentAgreement */

$this->title = Yii::t($model->tableName(), 'Create a new '.$model->modelTitle(1, false));
$this->params['breadcrumbs'][] = ['label' => $model->modelTitle(2), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="abonent-agreement-create">

    <?= $this->render('_form_receive', [
        'model' => $model,
        'projectList' => $projectList,
        'isAdmin' => $isAdmin,        
        'isModal' => true,
    ]) ?>
    
</div>
