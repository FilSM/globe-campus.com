<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\bootstrap\ActiveForm;

use kartik\helpers\Html;
use kartik\checkbox\CheckboxX;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <?= Html::pageHeader(Html::encode($this->title));?>

    <p>Please fill out the following fields to login:</p>

    <div class="row">
        <div class="col-md-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>
            
                <?=
                $form->field($model, 'rememberMe')->widget(CheckboxX::class, [
                    'autoLabel' => true,
                    'pluginOptions' => ['threeState' => false, 'tabindex' => '4'],
                    'labelSettings' => ['options' => ['class' => 'fsm-label']],
                ])->label(false);
                ?>
            
                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
