<?php

use yii\widgets\MaskedInput;

use kartik\password\PasswordInput;

/**
 * @var yii\widgets\ActiveForm    $form
 * @var dektrium\user\models\User $user
 */

?>

<?= $form->field($user, 'username')
    ->textInput(['maxlength' => 25])
    ->hint(isset($user) && ($user->scenario == 'from-client') ? Yii::t('fsmuser', 'Leave it blank and we will generate your username from of your email address.') : null) ?>

<?= $form->field($user, 'email')->widget(MaskedInput::class, [
        'clientOptions' => [
            'alias' => 'email',
        ],
    ]); 
?>  

<?= $form->field($user, 'password')
    ->widget(PasswordInput::class)
    ->hint(Yii::t('fsmuser', 
        'Password should contain at least 8 characters. '.
        'From them, at least one upper case character, one lowercase character and at least one numeric character.')
    );
?> 