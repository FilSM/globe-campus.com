<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\MaskedInput;

use kartik\widgets\ActiveForm;
use kartik\password\PasswordInput;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;

use common\components\FSMAccessHelper;
use common\components\FSMHelper;
use common\models\user\FSMUser;

/**
 * @var $this  yii\web\View
 * @var $form  yii\widgets\ActiveForm
 * @var $model common\models\user\FSMSettingsForm
 */

$this->title = Yii::t('user', 'Account settings');
if(FSMUser::getIsSystemAdmin() || FSMAccessHelper::can('administrateUser')){
    $this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['/user/admin/index']];
}
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-2">
        <?= $this->render('_menu',[
            'profile' => $profile,
        ]) ?>
    </div>
    
    <div class="col-md-10">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="panel-body">
                
                <?php $form = ActiveForm::begin([
                    'id' => 'account-form',
                    'type' => ActiveForm::TYPE_HORIZONTAL,
                    'fieldConfig' => [
                        //'template' => "{label}\n<div class=\"col-md-9\">{input}</div>\n<div class=\"col-sm-offset-3 col-md-9\">\n{hint}\n{error}</div>",
                        //'labelOptions' => ['class' => 'col-md-3 control-label'],
                        'showHints' => true,
                    ],
                    'enableAjaxValidation'   => true,
                    'enableClientValidation' => false,
                ]); ?>
                
                <?php
                    if(FSMUser::getIsSystemAdmin() || FSMAccessHelper::can('administrateUser')){
                        echo $form->field($model, 'role')->widget(Select2::class, [
                            'data' => $roleList,
                            'options' => [
                                'placeholder' => '...',
                                'multiple' => true,
                            ],
                        ]); 
                    }
                ?>
                
                <?= $form->field($model, 'email')->widget(MaskedInput::class, [
                        'clientOptions' => [
                            'alias' => 'email',
                        ],
                    ]); 
                ?>                 

                <?= $form->field($model, 'username') ?>

                <?= $form->field($model, 'new_password')->widget(PasswordInput::class, 
                    ['options' => [
                        'autocomplete' => "new-password",
                    ]])
                    ->hint(Yii::t('fsmuser', 'Password should contain at least 9 characters. '.
                            'From them, at least one upper case character, one lowercase character, one numeric and at least one special characters.')) ?>

                <?php if(!empty(Yii::$app->params['SMS_AUTH']) && (FSMUser::getIsSystemAdmin() || FSMAccessHelper::can('administrateUser'))){
                    echo $form->field($model, 'sms_auth', [
                        ])->widget(SwitchInput::class, [
                        'pluginOptions' => [
                            'onText' => Yii::t('common', 'Yes'),
                            'offText' => Yii::t('common', 'No'),
                        ],
                    ]); 
                }?>
                
                <hr/>
                
                <?php if(($model->scenario != 'admin_update') && ($model->user->id == Yii::$app->user->id)): 
                    echo $form->field($model, 'current_password')->passwordInput();
                 endif; ?>                

                <div class="form-group clearfix">
                    <div class="col-md-offset-9 col-md-3" style="text-align: right;">
                        <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-block btn-success']) ?><br>
                    </div>
                </div>
                
                <?php ActiveForm::end(); ?>
            </div>
        </div>

        <?php if (FSMUser::getIsPortalAdmin() && $model->module->enableAccountDelete): 
            
        ?>
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Yii::t('user', 'Delete account') ?></h3>
                </div>
                <div class="panel-body">
                    <p>
                        <?= Yii::t('user', 'Once you delete your account, there is no going back') ?>.
                        <?= Yii::t('user', 'It will be deleted forever') ?>.
                        <?= Yii::t('user', 'Please be certain') ?>.
                    </p>
                    <?= FSMHelper::aButton(null, [
                        'label' => Yii::t('user', 'Delete account'),
                        'url' => (!empty($accountDeleteUrl) ? $accountDeleteUrl : null),
                        'action' => (!empty($accountDeleteUrl) ? null : 'delete'),
                        'class' => 'danger',
                        'message' => Yii::t('user', 'Are you sure? There is no going back'),
                    ]) ?>
                </div>
            </div>
        <?php endif ?>
    </div>
</div>
