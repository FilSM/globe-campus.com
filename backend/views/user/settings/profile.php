<?php
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace common\models;

use Yii;
use yii\widgets\MaskedInput;
use yii\helpers\Url;
use yii\web\JsExpression;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;

use common\components\FSMAccessHelper;
use common\widgets\EnumInput;
//use common\widgets\GeocodeInput;
//use common\widgets\GMapInput;
use common\widgets\AutocompleteGMapInput;
use common\models\user\FSMUser;
use common\models\client\Client;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\Profile $profile
 */
$this->title = Yii::t('user', 'Profile settings');
if($isAdmin || Yii::$app->user->can('showBackend')){
    $this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['/user/admin/index']];
}
$this->params['breadcrumbs'][] = $this->title;
$username = empty($model->name) ? Html::encode($model->user->username) : Html::encode($model->name);
?>

<div class="row">

    <div class="col-md-2">
        <?= $this->render('_menu',[
            'profile' => $model,
            'activeItem' => 'profile',
            'clientModel' => !empty($clientModel) ? $clientModel : null,
            'isAdmin' => $isAdmin,
        ]) ?>
    </div>

    <div class="col-md-10">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="panel-body">
                
                <?php $form = ActiveForm::begin([
                    'type' => ActiveForm::TYPE_HORIZONTAL,
                    'id' => 'profile-form',
                    'formConfig' => [
                        'labelSpan' => 3,
                    ],
                    'fieldConfig' => [
                        //'template' => "{label}\n<div class=\"col-md-9\">{input}</div>\n<div class=\"col-sm-offset-3 col-md-9\">\n{hint}\n{error}</div>",
                        //'labelOptions' => ['class' => 'col-md-3 control-label'],
                        'showHints' => true,
                    ],
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'validateOnBlur' => false,
                ]); ?>                

                <?php
                    if($isAdmin || FSMAccessHelper::can('administrateUser')){
                        $values = [];
                        foreach ($clientModel as $client) {
                            $values[$client->client_id] = $client->client_id;
                        }
                        $clientModel[0]->client_id = $values;
                        echo $form->field($clientModel[0], 'client_id')->widget(Select2::class, [
                            'data' => $clientList,
                            'options' => [
                                'placeholder' => '...',
                                'multiple' => true,
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],            
                        ])->label(Yii::t('fsmuser', 'My company')); 
                    }else{
                        if(!empty($clientModel) && !empty($clientModel[0]->client)){
                            foreach ($clientModel as $key => $client) {
                                echo Html::activeHiddenInput($clientModel[0], 'client_id', ['value' => $client->client_id]);
                                $myCompany = (FSMAccessHelper::can('viewClient', $client->client) ?
                                        Html::a($client->client->name, ['/client/view', 'id' => $client->client_id], ['target' => '_blank']) :
                                    $client->client->name);
                                echo $form->field($client, 'id[]', [
                                    'staticValue' => $myCompany,
                                ])->staticInput([
                                    'class' => 'form-control',
                                ])->label(($key == 0 ? Yii::t('fsmuser', 'My company') : ''));
                            }
                        }
                    }
                ?>
                
                <?= $form->field($model, 'name')->textInput([
                    'enableAjaxValidation' => true,
                    'maxlength' => 255,
                ]) ?>
                
                <?= $form->field($model, 'short_name')->textInput([
                    'maxlength' => 5,
                ]) ?>
                
                <?= $form->field($model, 'language_id')->widget(Select2::class, [
                    'data' => $languageList, 
                    'options' => [
                        'placeholder' => '...',
                    ],
                ]); ?>
                
                <?= $form->field($model, 'phone', [
                    'inputOptions' => [
                        'value' => !empty($model->phone) ? $model->phone : 
                            (!empty(Yii::$app->params['defaultPhoneCode']) ? Yii::$app->params['defaultPhoneCode'] : ''),
                    ]
                ])->widget(MaskedInput::class, [
                    'clientOptions' => [
                        'greedy' => false,
                    ],
                    'mask' => '(+9{1,4})9{8,15}',
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => Yii::t('common', 'Enter as') . ' (+9999)9999999999...',
                    ],
                ]);
                ?>
                
                <?= $form->field($model, 'bio')->textarea() ?>

                <?= $form->field($model, 'gravatar_email')->widget(MaskedInput::class, [
                    'clientOptions' => [
                        'alias' => 'email',
                    ],
                    ])->hint(Html::a(Yii::t('user', 'Change your avatar at Gravatar.com'), 'http://gravatar.com')) ?>

                <?php if(!empty(Yii::$app->params['ENABLE_GOOGLE_CALENDAR'])) : 
                    echo $form->field($model, 'gcalendar_id')->textInput([
                        'maxlength' => 100,
                    ]);
                    endif;
                ?>
                
                <?php /*
                  <?= $form->field($model, 'public_email')->widget(MaskedInput::class, [
                  'clientOptions' => [
                  'alias' => 'email',
                  ],
                  ]);
                  ?>

                  <?= $form->field($model, 'website')->widget(MaskedInput::class, [
                  'clientOptions' => [
                  'alias' => 'url',
                  ],
                  ]);
                  ?>
                 * 
                 */ ?>

                <?php if(!empty($model->id) && $isAdmin){
                    echo $form->field($model, 'deleted', [
                        ])->widget(SwitchInput::class, [
                        'pluginOptions' => [
                            'onText' => Yii::t('common', 'Yes'),
                            'offText' => Yii::t('common', 'No'),
                        ],
                    ]); 
                }?>

                <div class="form-group clearfix double-line-top">
                    <div class="col-md-offset-9 col-md-3">
                        <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-block btn-success']) ?><br>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
