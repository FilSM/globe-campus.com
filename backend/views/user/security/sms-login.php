<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var dektrium\user\Module $module
 */

$this->title = Yii::t('user', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'sms-auth-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                ]); ?>                

                <?= $form->field($model, 'sms_code', [
                    'options' => [
                        'placeholder' => 'Type secret code',
                    ],                    
                    'inputOptions' => [
                        'autofocus' => 'autofocus', 
                        'class' => 'form-control', 
                        'tabindex' => '1'
                    ]
                ])->hint(Yii::t('fsmuser', 'Secret code was sent to your phone number.'));
                ?>

                <div class="form-group clearfix">
                    <div style="text-align: right;">
                        <?= \common\components\FSMHelper::vButton(null, [
                            'label' => Yii::t('user', 'Login'),
                            'class' => 'primary btn-lg',
                            'options' => [
                                'type' => 'submit',
                                'tabindex' => '2',
                            ],
                        ]); ?>
                        <?= \common\components\FSMHelper::aButton(null, [
                            'label' => Yii::t('common', 'Cancel'),
                            //'controller' => 'abonent-bill',
                            'action' => 'cancel-sms-auth',
                            'class' => 'default btn-lg',
                            'options' => [
                                'tabindex' => '3',
                            ],
                        ]); ?>
                    </div>
                </div>                

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
