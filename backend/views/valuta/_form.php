<?php
namespace common\models;

use Yii;
use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;

/**
 * @var yii\web\View $this
 * @var common\models\Valuta $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="valuta-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => 64]) ?>

    <?= $form->field($model, 'int_0')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'int_1')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'int_2')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'floor_0')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'floor_1')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'floor_2')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'enabled')->widget(SwitchInput::class, [
        'pluginOptions' => [
            'onText' => Yii::t('common', 'Yes'),
            'offText' => Yii::t('common', 'No'),
        ],
    ]);
    ?>   
    
    <div class="form-group">
        <div class="col-md-offset-9 col-md-3" style="text-align: right;">
            <?= $model->SubmitButton; ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>