<?php
namespace common\models;

use Yii;
use yii\widgets\Pjax;

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\ColorInput;

/* @var $this yii\web\View */
/* @var $model common\models\event\EventResurs */
/* @var $form yii\widgets\ActiveForm */
$isModal = !empty($isModal);
?>

<div class="event-resurs-form">
    <?php if($isModal) : Pjax::begin(Yii::$app->params['PjaxModalOptions']); endif; ?>
    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => $model->tableName().'-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'options' => [
            'data-pjax' => $isModal,
        ],          
    ]); ?>

    <?= $form->field($model, 'name')->textInput([
        'maxlength' => true,
        'autofocus' => true, 
        'tabindex' => 1,
    ]) ?>
    
    <?= $form->field($model, 'color_code')->widget(ColorInput::class, [
        'options' => ['placeholder' => 'Select color ...'],
    ]); ?>    

    <div class="form-group <?php if($isModal) : echo 'modal-button-group'; endif; ?>">
        <div class="col-md-offset-2 col-md-10" style="text-align: right;">
            <?= $model->SubmitButton; ?>
            <?= $model->CancelButton; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <?php if($isModal) : Pjax::end(); endif; ?>
</div>