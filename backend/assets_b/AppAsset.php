<?php

namespace backend\assets_b;

use yii\helpers\ArrayHelper;

use common\components\FSMAssetBundle;

/**
 * Main backend application asset bundle.
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @since 2.0
 */
class AppAsset extends FSMAssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/assets_b';

    /**
     * @inheritdoc
     */
    public function init() {
        //require_once('backend/assets_b/js/globalJSVars.php');

        $this->setSourcePath('@webroot/assets_b');
        $this->setupAssets('css', [
            'css/site',
            'css/backend',
        ]);
        $this->setupAssets('js', [
            //'js/filJSBackend',
        ]);
        
        $this->depends = ArrayHelper::merge(
            $this->depends, [
                'common\assets\CommonAsset',
            ]
        );
        
        $this->checkNeedReloadAssets();
        
        parent::init();
    }    
}
